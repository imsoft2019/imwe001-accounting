﻿WEACCTAPP.controller('businessprofilecontroller', function ($scope, $http, $timeout, GlobalVar) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessProfile = [];
    $scope.table = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.ReLoad = 1;
            $scope.table.binding = 1;
            $http.get(baseURL + "Setting/GetBusinessProfile?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                            $scope.BusinessProfile = response.data.responsedata;
                            $scope.table.binding = 0;
                            $scope.DeleteImages = undefined;
                            $scope.DeleteImagesRabber = undefined;
                            //$("#company_thumnail").addClass('dropify');
                            //$("#company_thumnail").attr("data-default-file", );

                            /************Logo**************/
                            $("#company_thumnail").addClass('dropify');
                            var publicpath_identity_picture = $scope.BusinessProfile.ThumnailLogo != '' && $scope.BusinessProfile.ThumnailLogo != undefined ? baseURL + 'uploads/company/' + $scope.BusinessProfile.ThumnailLogo : '';

                            var drEvent = $('.dropify').dropify();
                            //drEvent.on('dropify.afterClear', function (event, element) {
                            //    $scope.DeleteImages = 1;
                            //});
                            if ($scope.ReLoad == 1) {
                            drEvent = drEvent.data('dropify');
                            drEvent.resetPreview();
                            drEvent.clearElement();
                            drEvent.settings.defaultFile = publicpath_identity_picture;
                            drEvent.destroy();

                            drEvent.init();

                           
                                drEvent = $('.dropify').dropify();
                                drEvent.on('dropify.afterClear', function (event, element) {
                                    $scope.DeleteImages = 1;
                                });
                            }

                            $('.dropify#company_thumnail').dropify({
                                defaultFile: publicpath_identity_picture,
                            });

                            $('.dropify').dropify();

                            /************Rabber**************/
                            $("#rabber_thumnail").addClass('dropify1');
                            var rabberpath_identity_picture = $scope.BusinessProfile.RabberStampLogo != '' && $scope.BusinessProfile.RabberStampLogo != undefined ? baseURL + 'uploads/company/' + $scope.BusinessProfile.RabberStampLogo : '';

                            var drEventRabber = $('.dropify1').dropify();
                            //});
                            if ($scope.ReLoad == 1) {
                            drEventRabber = drEventRabber.data('dropify');
                            drEventRabber.resetPreview();
                            drEventRabber.clearElement();
                            drEventRabber.settings.defaultFile = rabberpath_identity_picture;
                            drEventRabber.destroy();

                            drEventRabber.init();

                         
                                drEventRabber = $('.dropify1').dropify();
                                drEventRabber.on('dropify.afterClear', function (event, element) {
                                    $scope.DeleteImagesRabber = 1;
                                });
                            }

                            $('.dropify1#rabber_thumnail').dropify({
                                defaultFile: rabberpath_identity_picture,
                            });

                            $('.dropify1').dropify();
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickSave = function () {
        try {
            if ($scope.BusinessProfile.BusinessName == undefined || $scope.BusinessProfile.BusinessName == "")
                throw "กรุณากรอกชื่อธุรกิจ";
            else {
                var data = {
                    UID: $scope.BusinessProfile.UID,
                    UserType: $scope.BusinessProfile.UserType,
                    UserTypeName: $scope.BusinessProfile.UserType == 'C' ? 'บริษัท' : $scope.BusinessProfile.UserType == 'O' ? 'ห้างหุ้นส่วนจำกัด' : 'บุคคลธรรมดา/ฟรีแลนซ์',
                    BusinessName: $scope.BusinessProfile.BusinessName,
                    Address: $scope.BusinessProfile.Address,
                    TaxID: $scope.BusinessProfile.TaxID,
                    BranchType: $scope.BusinessProfile.BranchType,
                    BranchTypeName: $scope.BusinessProfile.BranchType == 'H' ? 'สำนักงานใหญ่' : 'สาขา',
                    OfficePhone: $scope.BusinessProfile.OfficePhone,
                    MobilePhone: $scope.BusinessProfile.MobilePhone,
                    Fax: $scope.BusinessProfile.Fax,
                    WebsiteURL: $scope.BusinessProfile.WebsiteURL,
                    BranchCode: $scope.BusinessProfile.BranchType == 'B' ? $scope.BusinessProfile.BranchCode : '',
                    BranchName: $scope.BusinessProfile.BranchType == 'B' ? $scope.BusinessProfile.BranchName : '',
                    IsRegisterTax: $scope.BusinessProfile.IsRegisterTax,
                    IsRegisterTaxTypeName: $scope.BusinessProfile.IsRegisterTax == 'V' ? 'จดภาษีมูลค่าเพิ่มแล้ว' : 'ยังไม่เข้าระบบภาษีมูลค่าเพิ่ม',
                    ThumnailLogo: $scope.DeleteImages == 1 ? '' : $scope.BusinessProfile.ThumnailLogo,
                    RabberStampLogo: $scope.DeleteImagesRabber == 1 ? '' : $scope.BusinessProfile.RabberStampLogo,
                    LogoPath: $scope.DeleteImages == 1 ? '' : $scope.BusinessProfile.LogoPath
                };
                $http.post(baseURL + "Setting/PostBusinessProfile", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    if (($scope.SelectedFiles != undefined && $scope.Upload == 1) || ($scope.SelectedFilesRabber != undefined && $scope.UploadRabber == 1)) {
                                        try {
                                            var input = document.getElementById("company_thumnail");
                                            var input1 = document.getElementById("rabber_thumnail");
                                            var files = input.files;
                                            var rabbes = input1.files;
                                            var formData = new FormData();

                                            var v = files.length == 1 && rabbes.length == 1 ? 1 : rabbes.length == 1 ? 1 : files.length == 1 ? 1 : 0;

                                            for (var i = 0; i != v; i++) {
                                                if ($scope.SelectedFiles != undefined && $scope.Upload == 1)
                                                    formData.append("files", files[i]);
                                                if ($scope.SelectedFilesRabber != undefined && $scope.UploadRabber == 1)
                                                    formData.append("rabbers", rabbes[i]);
                                            }

                                            $.ajax(
                                                {
                                                    url: baseURL + "Setting/UploadCompanyProfile",
                                                    data: formData,
                                                    processData: false,
                                                    contentType: false,
                                                    type: "POST",
                                                    success: function (data) {
                                                        $scope.init();
                                                        $scope.ReLoad = 0;
                                                        showSuccessToast();
                                                    },
                                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                        showErrorToast(textStatus);
                                                    }
                                                }
                                            );
                                        }
                                        catch (ex) {
                                            showErrorToast(ex);
                                        }
                                    }
                                    else {
                                        $scope.ReLoad = 0;
                                        $scope.init();
                                        showSuccessToast();
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    /******* Start Upload *****/
    $scope.UploadFiles = function (files) {
        $scope.Upload = 1;
        $scope.SelectedFiles = files;
    };

    $scope.UploadFilesRabber = function (files) {
        $scope.UploadRabber = 1;
        $scope.SelectedFilesRabber = files;
    };

    /******* End Upload *****/

});


//if ($scope.SelectedFiles != undefined) {
//   
//}