﻿WEACCTAPP.controller('businessdocumentremarkcontroller', function ($scope, $http, $timeout, GlobalVar) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessDocument = [];
    $scope.table = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $http.get(baseURL + "Setting/GetDocumentRemark?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                            $scope.BusinessDocument = response.data.responsedata;
                            $scope.table.binding = 0;
                        }
                        else if(response.data.responsecode == '200')
                        {
                            $scope.table.binding = 0;
                        }
                        else if (response.data.responsecode == '400') {
                            $scope.table.binding = 0;
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.ChecklistDocument = function (val) {
        var checklist = _.where($scope.BusinessDocument, { DocumentType: val })[0];
        if (checklist != undefined)
            return 1;
        else
            return 0;
    }

    $scope.OnClickAdd = function () {
        $scope.BusinessDocumentRemarkAdd = [];
        $scope.BusinessDocumentRemarkAdd.updated = 0;
        $('#modaldoc-add').modal('show');
    };

    $scope.OnClickSave = function () {
        try {
            if ($scope.BusinessDocumentRemarkAdd.DocumentType == undefined)
                throw "กรุณาเลือกเอกสาร";
            if ($scope.BusinessDocumentRemarkAdd.DocumentText == undefined || $scope.BusinessDocumentRemarkAdd.DocumentText == "")
                throw "กรุณากรอกหมายเหตุ";
            else {

                var doctype;
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'ALL')
                    doctype = 'เอกสารซื้อและขายทั้งหมดในระบบ';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'QT')
                    doctype = 'ใบเสนอราคา';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'BL')
                    doctype = 'ใบวางบิล';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'INV')
                    doctype = 'ใบแจ้งหนี้';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'RE')
                    doctype = 'ใบเสร็จรับเงิน';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'CA')
                    doctype = 'ใบเสร็จรับเงิน (เงินสด)';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'CN')
                    doctype = 'ใบลดหนี้';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'DN')
                    doctype = 'ใบเพิ่มหนี้';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'PO')
                    doctype = 'ใบสั่งซื้อ';
                if ($scope.BusinessDocumentRemarkAdd.DocumentType == 'RI')
                    doctype = 'ใบรับสินค้า';

                var data = {
                    DocumentKey: $scope.BusinessDocumentRemarkAdd.DocumentKey,
                    DocumentType: $scope.BusinessDocumentRemarkAdd.DocumentType,
                    DocumentText: $scope.BusinessDocumentRemarkAdd.DocumentText,
                    DocumentTypeName: doctype
                };

                $http.post(baseURL + "Setting/PostDocumentRemark", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    $scope.BusinessDocumentRemarkAdd = [];
                                    $scope.init();
                                    $('#modaldoc-add').modal('hide');
                                    showSuccessToast();
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickUpdate = function (val) {
        var filter = _.where($scope.BusinessDocument, { DocumentKey: val})[0];

        if (filter != undefined) {
            $scope.BusinessDocumentRemarkAdd = [];
            $scope.BusinessDocumentRemarkAdd = angular.copy(filter);
            $scope.BusinessDocumentRemarkAdd.updated = 1;
            $('#modaldoc-add').modal('show');
        }
    }

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.BusinessDocument, { DocumentKey: val })[0];
        if (filter != undefined) {
            $scope.BusinessDocumentRemarkAdd = [];
            $scope.BusinessDocumentRemarkAdd = angular.copy(filter);
            $('#modaldocu-del').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {
        $http.get(baseURL + "Setting/DeleteDocumentRemark?dockey=" + $scope.BusinessDocumentRemarkAdd.DocumentKey + "&key=" + makeid())
            .then(function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $scope.init();
                            $scope.BusinessDocumentRemarkAdd = [];
                            $('#modaldocu-del').modal('hide');
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.itemsPerPage = 30;
});


