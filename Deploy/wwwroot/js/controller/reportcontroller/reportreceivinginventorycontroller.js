﻿WEACCTAPP.controller('reportreceivinginventorycontroller', function ($scope, $http, $timeout, GlobalVar) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessDocument = [];
    $scope.BusinessDocumentMain = [];
    $scope.table = [];
    $scope.Search = [];
    $scope.BusinessHeader = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.Selectionfilter = '2';
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputToMonth = GetDatetimeNowMonth();
            $scope.table.binding = 0;
            //$scope.OnClickSearch();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnSelection = function () {
        if ($scope.Search.Selectionfilter == '2')
            $scope.Search.InputToMonth = undefined;
        else if ($scope.Search.Selectionfilter == '4')
            $scope.Search.InputToYear = undefined;
        else if ($scope.Search.Selectionfilter == '5')
            $scope.Search.InputFromDate = $scope.Search.InputToDate = undefined;
        else {
            $scope.OnClickSearch();
        }
    };

    $scope.OnChangeDate = function () {
        if ($scope.Search.Selectionfilter == '2') {
            if ($scope.Search.InputToMonth != undefined && $scope.Search.InputToMonth != '')
                $scope.OnClickSearch();
        }
        else if ($scope.Search.Selectionfilter == '4') {
            if ($scope.Search.InputToYear != undefined && $scope.Search.InputToYear != '')
                $scope.OnClickSearch();
        }
        else if ($scope.Search.Selectionfilter == '5') {
            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != '' && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != '')
                $scope.OnClickSearch();
        }
    };

    $scope.OnChangeStatus = function () {
        $scope.table.binding = 1;
        $scope.BusinessDocument = $scope.BusinessDocumentMain;
        if ($scope.Search.SelectStatus != '0') {
            $scope.BusinessDocument = _.where($scope.BusinessDocumentMain, { DocumentStatus: $scope.Search.SelectStatus });
        }

        var amount = 0;
        var vat = 0;
        _.each($scope.BusinessDocument, function (item) {
            if (item.DocumentStatus != '3') {
                amount = amount + item.Total;
                vat = vat + item.VAT;
            }
        });
        $scope.BusinessHeader.Amount = AFormatNumber(amount, 2);
        $scope.BusinessHeader.VAT = AFormatNumber(vat, 2);
        $scope.currentPage = 0;
        $scope.retpage = [];
        $scope.range();
        $scope.table.binding = 0;
    };

    $scope.OnClickSearch = function () {
        $scope.table.binding = 1;
        $scope.Search.SelectStatus = '0';
        var data = {
            typedate: $scope.Search.Selectionfilter,
            startdate: $scope.Search.Selectionfilter == '2' ? ToJsonDate2('01-' + $scope.Search.InputToMonth)
                : $scope.Search.Selectionfilter == '4' ? ToJsonDate2('01-01-' + $scope.Search.InputToYear)
                    : $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputFromDate) : undefined,
            enddate: $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputToDate) : undefined
        };

        $http.post(baseURL + "Report/PostReportReceivingInventory", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.BusinessDocumentMain = [];
                            $scope.BusinessDocumentMain = response.data.responsedata;

                            var amount = 0;
                            var vat = 0;
                            _.each($scope.BusinessDocumentMain, function (item) {
                                item.DocumentDate = formatDate(item.DocumentDate);

                                if (item.DocumentStatus != '3') {

                                    amount = amount + item.Total;
                                    vat = vat + item.VAT;
                                }

                                item.TotalText = AFormatNumber(item.Total, 2);
                                item.VATText = AFormatNumber(item.VAT, 2);

                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);

                                if (item.ReceiptInfo != undefined) {
                                    item.ReceiptInfo.PaymentDate = formatDate(item.ReceiptInfo.PaymentDate);
                                    item.ReceiptInfo.PaymentAmount = AFormatNumber(item.ReceiptInfo.PaymentAmount, 2);
                                    item.ReceiptInfo.NetPaymentAmount = AFormatNumber(item.ReceiptInfo.NetPaymentAmount, 2);
                                    item.ReceiptInfo.WithholdingAmount = item.ReceiptInfo.IsWithholdingTax == '1' ? AFormatNumber(item.ReceiptInfo.WithholdingAmount, 2) : '0.00';
                                    item.ReceiptInfo.DiscountAmount = item.ReceiptInfo.IsDiscount == '1' ? AFormatNumber(item.ReceiptInfo.DiscountAmount, 2) : '0.00';
                                    item.ReceiptInfo.ChequeDate = item.ReceiptInfo.ChequeDate != undefined ? formatDate(item.ReceiptInfo.ChequeDate) : undefined;
                                    item.ReceiptInfo.Balance = AFormatNumber(item.ReceiptInfo.Balance, 2);

                                    var htmltag;
                                    htmltag = "<div class='title' style='width:220px;' >รายละเอียดการชำระ </div> <hr class='mb-2'> ";
                                    htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ยอดที่เรียกเก็บ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.PaymentAmount + " บาท</div></div>";
                                    if (item.ReceiptInfo.IsDiscount == '1')
                                        htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.DiscountTypeName + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.DiscountAmount + " บาท</div></div> ";
                                    if (item.ReceiptInfo.IsWithholdingTax == '1')
                                        htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ภาษีหัก ณ ที่จ่าย </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.WithholdingAmount + " บาท</div> </div>";
                                    if (item.ReceiptInfo.BalanceType == 'Lost')
                                        htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.CauseTypeName.replace('/เงินเกิน', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.Balance + " บาท</div> </div>";
                                    if (item.ReceiptInfo.BalanceType == 'Over')
                                        htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.CauseTypeName.replace('เงินขาด/', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.Balance + " บาท</div> </div>";
                                    htmltag = htmltag + " <hr class='mb-2'><div class='row'> <div class='col-lg-6'> ยอดรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.NetPaymentAmount + " บาท</div> </div>";
                                    htmltag = htmltag + " <div class='row'> <div class='col-lg-6'> วิธีการรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.PaymentTypeName + "</div> </div>";
                                    item.ReceiptInfo.TagHtml = htmltag;
                                }
                            });

                            $scope.BusinessDocument = $scope.BusinessDocumentMain;

                            $scope.retpage = [];
                            $scope.range();

                            var condition = response.data.responsecondition;
                            $scope.BusinessHeader.DateNow = formatFullDate(GetDatetimeNow());
                            $scope.BusinessHeader.StartDate = formatShortDate(condition.startdate);
                            $scope.BusinessHeader.EndDate = formatShortDate(condition.enddate);
                            $scope.BusinessHeader.Amount = AFormatNumber(amount, 2);
                            $scope.BusinessHeader.VAT = AFormatNumber(vat, 2);
                            if ($scope.BusinessDocument != undefined && $scope.BusinessDocument.length > 0) {
                                $scope.BusinessHeader.DocumentNo = $scope.BusinessDocument[0].DocumentNo + ' - ' + $scope.BusinessDocument[$scope.BusinessDocument.length - 1].DocumentNo;
                            }
                            else
                                $scope.BusinessHeader.BillingNo = '-';

                            $scope.table.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });

    };

    $scope.OnClickExportExcel = function () {
        $scope.table.binding = 1;
        $scope.Search.SelectStatus = '0';
        var data = {
            typedate: $scope.Search.Selectionfilter,
            startdate: $scope.Search.Selectionfilter == '2' ? ToJsonDate2('01-' + $scope.Search.InputToMonth)
                : $scope.Search.Selectionfilter == '4' ? ToJsonDate2('01-01-' + $scope.Search.InputToYear)
                    : $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputFromDate) : undefined,
            enddate: $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputToDate) : undefined
        };

        $http.post(baseURL + "Report/PostXlsxReportReceivingInventory", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.BusinessDocumentMain = [];
                            $scope.BusinessDocumentMain = response.data.responsedata;
                            showSuccessText('ดาวน์โหลดสำเร็จ');
                            window.location = baseURL + "Report/DownloadFromPath?file=" + response.data.responsedata;

                            $scope.table.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickReport = function (dockey, docno, doctype) {
        $('#modal-report').modal('show');
        $scope.document = [];
        $scope.document.bindding = 1;
        $scope.document.DocNo = docno;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: doctype,
                DocumentNo: docno,
                TypeAction: 'Report',
                Content: 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function (dockey, docno, doctype) {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: doctype,
                DocumentNo: docno,
                TypeAction: 'Download',
                Content: 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickUpdate = function (key, type) {
        if (type == 'RI')
            window.location.href = baseURL + "DocumentBuy/ReceivingInventoryAdd?key=" + key + "&ref_report=Reportreceivinginventory";
    };

    $scope.itemsPerPage = 20;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'DocumentKey';
    $scope.reverseSort = true;


    $scope.pageCount = function () {
        return Math.ceil($scope.BusinessDocument.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessDocument.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

});


