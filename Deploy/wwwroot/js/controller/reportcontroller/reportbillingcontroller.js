﻿WEACCTAPP.controller('reportbillingcontroller', function ($scope, $http, $timeout, GlobalVar) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessBilling = [];
    $scope.BusinessBillingMain = [];
    $scope.table = [];
    $scope.Search = [];
    $scope.BusinessHeader = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.Selectionfilter = '2';
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputToMonth =GetDatetimeNowMonth();
            $scope.table.binding = 0;
            //$scope.OnClickSearch();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnSelection = function () {
        if ($scope.Search.Selectionfilter == '2')
            $scope.Search.InputToMonth = undefined;
        else if ($scope.Search.Selectionfilter == '4')
            $scope.Search.InputToYear = undefined;
        else if ($scope.Search.Selectionfilter == '5')
            $scope.Search.InputFromDate = $scope.Search.InputToDate = undefined;
        else {
            $scope.OnClickSearch();
        }
    };

    $scope.OnChangeDate = function () {
        if ($scope.Search.Selectionfilter == '2') {
            if ($scope.Search.InputToMonth != undefined && $scope.Search.InputToMonth != '')
                $scope.OnClickSearch();
        }
        else if ($scope.Search.Selectionfilter == '4') {
            if ($scope.Search.InputToYear != undefined && $scope.Search.InputToYear != '')
                $scope.OnClickSearch();
        }
        else if ($scope.Search.Selectionfilter == '5') {
            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != '' && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != '')
                $scope.OnClickSearch();
        }
    };

    $scope.OnChangeStatus = function () {
        $scope.table.binding = 1;
        $scope.BusinessBilling = $scope.BusinessBillingMain;
        if ($scope.Search.SelectStatus != '0') {
            $scope.BusinessBilling = _.where($scope.BusinessBillingMain, { DocumentStatus : $scope.Search.SelectStatus });
        }

        var amount = 0;
        var vat = 0;
        _.each($scope.BusinessBilling, function (item) {
            if (item.DocumentStatus != '3') {

                if (item.DocumentType == 'CN') {
                    amount = amount - item.Total;
                    vat = vat - item.VAT;
                }
                else {
                    amount = amount + item.Total;
                    vat = vat + item.VAT;
                }
            }
        });
        $scope.BusinessHeader.Amount = AFormatNumber(amount, 2);
        $scope.BusinessHeader.VAT = AFormatNumber(vat, 2);
        $scope.currentPage = 0;
        $scope.retpage = [];
        $scope.range();
        $scope.table.binding = 0;
    };

    $scope.OnClickSearch = function () {
        $scope.table.binding = 1;
        $scope.Search.SelectStatus = '0';

        var data = {
            typedate: $scope.Search.Selectionfilter,
            startdate: $scope.Search.Selectionfilter == '2' ? ToJsonDate2('01-' + $scope.Search.InputToMonth)
                : $scope.Search.Selectionfilter == '4' ? ToJsonDate2('01-01-' + $scope.Search.InputToYear)
                : $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputFromDate) : undefined,
            enddate: $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputToDate) : undefined
        };

        $http.post(baseURL + "Report/PostReportBilling", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.BusinessBillingMain = [];
                            $scope.BusinessBillingMain = response.data.responsedata;

                            var amount = 0;
                            var vat = 0;
                            _.each($scope.BusinessBillingMain, function (item) {
                                item.DocumentDate = formatDate(item.DocumentDate);

                                if (item.DocumentStatus != '3') {

                                    if (item.DocumentType == 'CN') {
                                        amount = amount - item.Total;
                                        vat = vat - item.VAT;
                                    }
                                    else {
                                        amount = amount + item.Total;
                                        vat = vat + item.VAT;
                                    }
                                }

                                item.TotalText = AFormatNumber(item.Total, 2);
                                item.VATText = AFormatNumber(item.VAT, 2);

                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                            });

                            $scope.BusinessBilling = $scope.BusinessBillingMain;

                            $scope.retpage = [];
                            $scope.range();

                            var condition = response.data.responsecondition;
                            $scope.BusinessHeader.DateNow = formatFullDate(GetDatetimeNow());
                            $scope.BusinessHeader.StartDate = formatShortDate(condition.startdate);
                            $scope.BusinessHeader.EndDate = formatShortDate(condition.enddate);
                            $scope.BusinessHeader.Amount = AFormatNumber(amount, 2);
                            $scope.BusinessHeader.VAT = AFormatNumber(vat, 2);
                            if ($scope.BusinessBilling != undefined && $scope.BusinessBilling.length > 0) {
                                $scope.BusinessHeader.DocumentNo = $scope.BusinessBilling[0].DocumentNo + ' - ' + $scope.BusinessBilling[$scope.BusinessBilling.length - 1].DocumentNo;
                            }
                            else
                                $scope.BusinessHeader.BillingNo = '-';

                            $scope.table.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });

    };

    $scope.OnClickExportExcel = function () {
        $scope.table.binding = 1;
        $scope.Search.SelectStatus = '0';
        var data = {
            typedate: $scope.Search.Selectionfilter,
            startdate: $scope.Search.Selectionfilter == '2' ? ToJsonDate2('01-' + $scope.Search.InputToMonth)
                : $scope.Search.Selectionfilter == '4' ? ToJsonDate2('01-01-' + $scope.Search.InputToYear)
                    : $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputFromDate) : undefined,
            enddate: $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputToDate) : undefined
        };

        $http.post(baseURL + "Report/PostXlsxReportBilling", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.BusinessDocumentMain = [];
                            $scope.BusinessDocumentMain = response.data.responsedata;
                            showSuccessText('ดาวน์โหลดสำเร็จ');
                            window.location = baseURL + "Report/DownloadFromPath?file=" + response.data.responsedata;

                            $scope.table.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickUpdate = function (key, type) {
        if (type == 'BL')
            window.location.href = baseURL + "DocumentSell/BillingNoteAdd?BillingKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'CBL')
            window.location.href = baseURL + "DocumentSell/BillingNoteCumulativeAdd?BillingKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'QT')
            window.location.href = baseURL + "DocumentSell/QuotationAdd?QuotationKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'INV')
            window.location.href = baseURL + "DocumentSell/InvoiceAdd?InvoiceKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'CN')
            window.location.href = baseURL + "DocumentSell/CreditNoteAdd?CreditNoteKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'DN')
            window.location.href = baseURL + "DocumentSell/DebitNoteAdd?DebitNoteKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'RE')
            window.location.href = baseURL + "DocumentSell/ReceiptAdd?ReceiptKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'CRE')
            window.location.href = baseURL + "DocumentSell/ReceiptCumulativeAdd?ReceiptKey=" + key + "&ref_report=Reportbillingnote";
        else if (type == 'CA')
            window.location.href = baseURL + "DocumentSell/CashSaleAdd?CashSaleKey=" + key + "&ref_report=Reportbillingnote";
    };

    $scope.OnClickReportContent = function (dockey, docno, type, doctype) {
        $scope.document = [];
        $scope.document.DocKey = dockey;
        $scope.document.DocNo = docno;
        $scope.document.original = $scope.document.copy = true;
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $scope.document.documenttype = doctype;
        $('#modal-content-report').modal('show');
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        $scope.document.DocNo = $scope.document.DocNo;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: $scope.document.documenttype,
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: $scope.document.documenttype,
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Download',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.itemsPerPage = 20;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'DocumentNo';
    $scope.reverseSort = true;


    $scope.pageCount = function () {
        return Math.ceil($scope.BusinessBilling.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessBilling.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

});


