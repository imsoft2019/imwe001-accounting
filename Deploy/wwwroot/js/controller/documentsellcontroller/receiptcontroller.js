﻿WEACCTAPP.controller('receiptcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, $q) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    const headerdoc = 'RE';
    $scope.BusinessReceipt = [];
    $scope.BusinessReceiptMain = [];
    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];
    $scope.Parameter.BizBank = [];
    $scope.BusinessProfile = [];
    $scope.BusinessEmployee = [];
    $scope.EmailAdd = [];
    var gEmp = paramService.getProfile();
    var gProfile = paramService.getBusinessProfile();

    var gParamQuotation = paramService.getParameterQuotation();
    var gBizBank = paramService.getBankBusiness();

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
            $scope.Search.InputToDate = GetDatetimeNowLastMonth();

            var qq1 = $q.all([gProfile, gEmp]).then(function (data) {
                try {
                    /****** GetDocument NO ******/
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BusinessProfile = data[0].data.responsedata; }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Customer ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.BusinessEmployee = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });

            var qq = $q.all([gParamQuotation, gBizBank]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                            $scope.Parameter.Param_Quotation = [];
                            $scope.Parameter.Param_Quotation = data[0].data.responsedata;
                        }
                        else if (data[0].data.responsecode == '000') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Business Bank ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.Parameter.BizBank = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                    }


                    $http.get(baseURL + "DocumentSell/GetBusinessReceipt?key=" + makeid())
                        .then(function (response) {

                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {
                                    $scope.BusinessReceiptMain = response.data.responsedata;

                                    var grandtotal = 0;
                                    _.each($scope.BusinessReceiptMain, function (item) {
                                        item.ReceiptDate = formatDate(item.ReceiptDate);
                                        if (item.ReceiptStatus != '2')
                                            grandtotal = grandtotal + item.GrandAmount;
                                        item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                                        item.TotalAfterDiscountAmount = AFormatNumber(item.TotalAfterDiscountAmount, 2);
                                        item.TotalBeforeVatAmount = AFormatNumber(item.TotalBeforeVatAmount, 2);

                                        if (item.ReceiptInfo != undefined) {
                                            item.ReceiptInfo.PaymentDate = formatDate(item.ReceiptInfo.PaymentDate);
                                            item.ReceiptInfo.PaymentAmount = AFormatNumber(item.ReceiptInfo.PaymentAmount, 2);
                                            item.ReceiptInfo.NetPaymentAmount = AFormatNumber(item.ReceiptInfo.NetPaymentAmount, 2);
                                            item.ReceiptInfo.WithholdingAmount = item.ReceiptInfo.IsWithholdingTax == '1' ? AFormatNumber(item.ReceiptInfo.WithholdingAmount, 2) : '0.00';
                                            item.ReceiptInfo.DiscountAmount = item.ReceiptInfo.IsDiscount == '1' ? AFormatNumber(item.ReceiptInfo.DiscountAmount, 2) : '0.00';
                                            item.ReceiptInfo.ChequeDate = item.ReceiptInfo.ChequeDate != undefined ? formatDate(item.ReceiptInfo.ChequeDate) : undefined;
                                            item.ReceiptInfo.Balance = AFormatNumber(item.ReceiptInfo.Balance, 2);

                                            var htmltag;
                                            htmltag = "<div class='title' style='width:220px;' >รายละเอียดการชำระ </div> <hr class='mb-2'> ";
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ยอดที่เรียกเก็บ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.PaymentAmount + " บาท</div></div>";
                                            if (item.ReceiptInfo.IsDiscount == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.DiscountTypeName + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.DiscountAmount + " บาท</div></div> ";
                                            if (item.ReceiptInfo.IsWithholdingTax == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ภาษีหัก ณ ที่จ่าย </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.WithholdingAmount + " บาท</div> </div>";
                                            if (item.ReceiptInfo.BalanceType == 'Lost')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.CauseTypeName.replace('/เงินเกิน', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.Balance + " บาท</div> </div>";
                                            if (item.ReceiptInfo.BalanceType == 'Over')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.CauseTypeName.replace('เงินขาด/', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.Balance + " บาท</div> </div>";
                                            htmltag = htmltag + " <hr class='mb-2'><div class='row'> <div class='col-lg-6'> ยอดรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.NetPaymentAmount + " บาท</div> </div>";
                                            htmltag = htmltag + " <div class='row'> <div class='col-lg-6'> วิธีการรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.PaymentTypeName + "</div> </div>";
                                            item.ReceiptInfo.TagHtml = htmltag;
                                        }
                                    });
                                    $scope.ReceiptSummary = AFormatNumber(grandtotal, 2);
                                    $scope.BusinessReceipt = $scope.BusinessReceiptMain;
                                    $scope.Search.ReceiptType = 'ALL';

                                    $scope.retpage = [];
                                    $scope.range();
                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickSelectType = function (val) {

        if (val == 'ALL')
            $scope.BusinessReceipt = $scope.BusinessReceiptMain;
        else if (val == 'RE') // ใบวางบิล
            $scope.BusinessReceipt = _.where($scope.BusinessReceiptMain, { ReceiptType: 'RE' });
        else if (val == 'CRE') //ใบวางบิลรวม
            $scope.BusinessReceipt = _.where($scope.BusinessReceiptMain, { ReceiptType: 'CRE' });

        $scope.Search.ReceiptType = val;
    };

    $scope.OnClickSearch = function () {
        $scope.BusinessReceipt = $scope.BusinessReceiptMain;
        $scope.table.binding = 1;

        $scope.BusinessReceipt = _.filter($scope.BusinessReceiptMain, function (item) {

            var c1 = true, c2 = true, c3 = true, c4 = true, c5 = true;

            if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.CustomerName != undefined && (item.CustomerName).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.CustomerName == undefined || (item.CustomerName).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ReceiptNo != undefined && (item.ReceiptNo).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.ReceiptNo == undefined || (item.ReceiptNo).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProjectName != undefined && (item.ProjectName).indexOf($scope.Search.InputFilter) > -1)
                    c3 = true;
                else if (item.ProjectName == undefined || (item.ProjectName).indexOf($scope.Search.InputFilter) < 0)
                    c3 = false;


            }

            if ($scope.Search.SelectStatus == '0') {
                c4 = true;
            }
            else if (item.ReceiptStatus == $scope.Search.SelectStatus) {
                c4 = true;
            }
            else
                c4 = false;

            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != "" && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != "") {

                var fromarry = $scope.Search.InputFromDate.split('-');

                var quotationdatearry = item.ReceiptDate.split('-');

                var from = new Date(fromarry[2], fromarry[1] - 1, fromarry[0]);

                var quotationdate = new Date(quotationdatearry[2], quotationdatearry[1] - 1, quotationdatearry[0]);

                var toarry = $scope.Search.InputToDate.split('-');

                var to = new Date(toarry[2], toarry[1] - 1, toarry[0]);
                //alert(quotationdate); alert(from); alert(from);
                if (quotationdate >= from && quotationdate <= to)
                    c5 = true;
                else
                    c5 = false;
            }


            if ((c1 || c2 || c3) && c4 && c5) {
                return item;
            }
        });
        $scope.table.binding = 0;
    };

    $scope.OnClickClear = function () {
        $scope.Search = [];
        $scope.Search.SelectStatus = '0';
        $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
        $scope.Search.InputToDate = GetDatetimeNowLastMonth();
        $scope.BusinessReceipt = $scope.BusinessReceiptMain;
    };

    $scope.OnClickCumulativeAdd = function () {
        window.location.href = baseURL + "DocumentSell/ReceiptCumulativeAdd";
    };

    $scope.OnClickUpdate = function (key, type) {
        if (type == 'RE')
            window.location.href = baseURL + "DocumentSell/ReceiptAdd?ReceiptKey=" + key;
        else if (type == 'CRE')
            window.location.href = baseURL + "DocumentSell/ReceiptCumulativeAdd?ReceiptKey=" + key;
    };

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.BusinessReceipt, { ReceiptKey: val })[0];
        if (filter != undefined) {
            $scope.ReceiptAdd = [];
            $scope.ReceiptAdd = angular.copy(filter);
            if ($scope.ReceiptAdd.ReceiptStatus == '1')
                $('#modalreceipt-del').modal('show');
            else
                $('#modalreceipt-alert').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            ReceiptKey: $scope.ReceiptAdd.ReceiptKey
        };

        $http.post(baseURL + "DocumentSell/DeleteBusinessReceipt", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalreceipt-del').modal('hide');
                            $scope.init();
                            $scope.ReceiptAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickNextWorkflow = function (receiptkey, tostatus) {
        try {
            var data = {
                ReceiptKey: receiptkey,
                ReceiptStatus: tostatus
            };
            $http.post(baseURL + "DocumentSell/PostBusinessReceiptNextStep", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.init();
                                showSuccessWorkToast();
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickPaymentAdd = function (val) {
        var filter = _.where($scope.BusinessReceipt, { ReceiptKey: val })[0];
        if (filter != undefined) {
            $scope.ReceiptAdd = [];
            $scope.ReceiptAdd = angular.copy(filter);

            $scope.ReceiptAdd.IsTaxHeader = $scope.ReceiptAdd.IsTaxHeader == '1' ? true : false;

            $scope.ReceiptAdd.ReceiptInfo = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push({ ID: '0', Name: 'จำนวนเงิน', Rate: 0 });
            _.each($scope.Parameter.Param_Quotation.Param_WithholdingRate, function (item) { $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push(item); });


            $scope.ReceiptAdd.ReceiptInfo.DocumentKey = $scope.ReceiptAdd.ReceiptKey;
            $scope.ReceiptAdd.ReceiptInfo.DocumentNo = $scope.ReceiptAdd.ReceiptNo;
            $scope.ReceiptAdd.ReceiptInfo.DocumentType = headerdoc;
            $scope.ReceiptAdd.ReceiptInfo.CustomerName = $scope.ReceiptAdd.CustomerName;
            $scope.ReceiptAdd.ReceiptInfo.PaymentAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            $scope.ReceiptAdd.ReceiptInfo.PaymentDate = GetDatetimeNow();
            $('#inputreceivedate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.PaymentDate);
            $scope.ReceiptAdd.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax == '1' ? true : false;

            $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax;
            if ($scope.ReceiptAdd.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.WithholdingKey; });
                if (type.length > 0)
                    $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];

                $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.WithholdingAmount, 2) : AFormatNumber(0, 2);
            }

            if ($scope.ReceiptAdd.IsTaxHeader) {

                $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax = false;
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
                if (type.length > 0)
                    $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];

                $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            }

            $scope.ReceiptAdd.IsAdjustDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.IsDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.DiscountType = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined;
            $scope.ReceiptAdd.ReceiptInfo.DiscountTypeName = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountTypeName : undefined;
            $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : AFormatNumber(0, 2);

            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = $scope.ReceiptAdd.IsAdjustDiscount || $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.PaymentAmount, 2) : AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);

            $scope.ReceiptAdd.ReceiptInfo.PaymentType = "CA";
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(0, 2);
            $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
            $scope.ReceiptAdd.ReceiptInfo.IsPayment = true;

            $('#modalreceipt-receive').modal('show');
        }
    };

    $scope.OnChangeIsAdjustReceive = function () {
        $scope.ReceiptAdd.ReceiptInfo.DiscountType = "1";
        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = AFormatNumber(0, 2);
    };

    $scope.OnChangeIsWithhollding = function () {
        
        if ($scope.ReceiptAdd.IsTaxHeader == '1' || $scope.ReceiptAdd.ReceiptType == 'CRE') {
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
        }
        else {
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '1'; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
        }
        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    };

    $scope.OnChangeWithholldingReceipt = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID == '0') {
            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            $scope.OnChangeReceiveCalculate();
        }
        else {
            $scope.OnChangeReceiveCalculate();
        }
    };

    $scope.OnChangePaymentType = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR') {
            var type = $scope.Parameter.BizBank.filter(function (item) { return item.UID == $scope.Parameter.BizBank[0].UID; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer = type[0];
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
            $scope.ReceiptAdd.ReceiptInfo.ChequeDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiptInfo.ChequeNo = '';
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
        }
    };

    $scope.OnClickIsPayment = function () {
        $scope.ReceiptAdd.ReceiptInfo.IsPayment = false;
        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
        if (type.length > 0)
            $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
    };

    $scope.OnChangeReceiveCalculate = function () {
        try {
            var grandtotal = 0, netpayment = 0, discount = 0, withholding = 0, netgrand = 0, pricediscount = 0, pricevat, balance = 0;
            grandtotal = $scope.ReceiptAdd.ReceiptInfo.PaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.PaymentAmount); // Discount Percenter
            pricediscount = $scope.ReceiptAdd.TotalAfterDiscountAmount == undefined || $scope.ReceiptAdd.TotalAfterDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalAfterDiscountAmount); // Discount Percenter
            pricevat = $scope.ReceiptAdd.TotalBeforeVatAmount == undefined || $scope.ReceiptAdd.TotalBeforeVatAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalBeforeVatAmount); // Discount Percenter
            netpayment = $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount); // Discount Percenter
            discount = $scope.ReceiptAdd.ReceiptInfo.DiscountAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.DiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.DiscountAmount); // Discount Percenter
            withholding = $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.WithholdingAmount); // Discount Percenter


            if ($scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID; });
                if (type.length > 0)
                    withholdingrate = type[0].Rate;

                if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID == '0')
                    withholding = withholding;
                else {
                    if ($scope.ReceiptAdd.TaxType == 'N')
                        withholding = pricediscount * withholdingrate / 100;
                    else if ($scope.ReceiptAdd.TaxType == 'V')
                        withholding = pricevat * withholdingrate / 100;
                }

                grandtotal = grandtotal - withholding;
            }
            else
                grandtotal = grandtotal;

            grandtotal = grandtotal - discount;

            if (!$scope.ReceiptAdd.ReceiptInfo.IsPayment) {
                balance = netpayment - grandtotal;
            }
            else
                netpayment = grandtotal;

            if (balance < 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินขาด';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Lost';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
                balance = balance * (-1);
            }
            else if (balance > 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินเกิน';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Over';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }
            else {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
                $scope.ReceiptAdd.ReceiptInfo.IsText = '';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Equal';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }

            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(withholding, 2);//
            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber(netpayment, 2);//
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(balance, 2);//
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickReceiptPayment = function () {
        try {
            if (!$scope.ReceiptAdd.ReceiptInfo.IsCauseType && $scope.ReceiptAdd.ReceiptInfo.CauseType == undefined)
                showWariningToast("กรุณาระบุสาเหตุ เงินขาด/เงินเกิน");
            else {
                var adjusttype;
                if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '1')
                    adjusttype = 'ส่วนลดพิเศษ';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '2')
                    adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '3')
                    adjusttype = 'ค่าดำเนินการ';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '4')
                    adjusttype = 'ปัดเศษ';

                var paymenttype;
                if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CA')
                    paymenttype = 'เงินสด';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR')
                    paymenttype = 'โอนเงิน';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ')
                    paymenttype = 'เช็ค';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC')
                    paymenttype = 'บัตรเครดิต';

                var cause;
                if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '1')
                    cause = 'ค่าธรรมเนียม';
                else if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '2')
                    cause = 'เงินขาด/เงินเกิน';

                var data = {
                    DocumentKey: $scope.ReceiptAdd.ReceiptKey,
                    DocumentNo: $scope.ReceiptAdd.ReceiptNo,
                    DocumentType: headerdoc,
                    CustomerName: $scope.ReceiptAdd.ReceiptInfo.CustomerName,
                    PaymentAmount: $scope.ReceiptAdd.ReceiptInfo.PaymentAmount,
                    PaymentDate: ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.PaymentDate),
                    NetPaymentAmount: ConvertToDecimal($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount),
                    IsWithholdingTax: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount : undefined,
                    IsDiscount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? '1' : '0',
                    DiscountType: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountType : undefined,
                    DiscountTypeName: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? adjusttype : undefined,
                    DiscountAmount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountAmount : undefined,
                    PaymentType: $scope.ReceiptAdd.ReceiptInfo.PaymentType,
                    PaymentTypeName: paymenttype,
                    BankAccountID: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.UID : undefined,
                    BankCode: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBank.ID : undefined,
                    BankName: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.BankName :
                        $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' || $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBank.Name : undefined,
                    BankAccountNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined :  $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.AccountNo : undefined,
                    BankAccountType: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.DepositTypeName : undefined,
                    ChequeDate: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.ChequeDate) : undefined,
                    ChequeNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.ChequeNo : undefined,
                    Comment: $scope.ReceiptAdd.ReceiptInfo.Comment,
                    Balance: $scope.ReceiptAdd.ReceiptInfo.Balance,
                    BalanceType: $scope.ReceiptAdd.ReceiptInfo.BalanceType,
                    CauseType: $scope.ReceiptAdd.ReceiptInfo.CauseType,
                    CauseTypeName: cause
                };

                $http.post(baseURL + "DocumentSell/PostBusinessReceiptPayment", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    showSuccessToast();
                                    $('#modalreceipt-receive').modal('hide');
                                    $scope.init();
                                    $scope.ReceiptAdd = [];
                                }
                                else showErrorToast(response.data.errormessage);
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) { showErrorToast(err); }
    };


    $scope.OnClickReportContent = function (dockey, docno, type) {
        $scope.document = [];
        $scope.document.DocKey = dockey;
        $scope.document.DocNo = docno;
        $scope.document.original = $scope.document.copy = true;
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $('#modal-content-report').modal('show');
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        $scope.document.DocNo = $scope.document.DocNo;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'RE',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'RE',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Download',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickSendMail = function (dockey, docno) {
        var quo = _.where($scope.BusinessReceipt, { ReceiptKey: dockey })[0];
        if (quo != undefined) {
            $scope.EmailAdd = [];
            $scope.EmailAdd.DocKey = dockey;
            $scope.EmailAdd.DocNo = docno;
            $scope.EmailAdd.MailToMe = $scope.EmailAdd.Content1 = $scope.EmailAdd.Content2 = true;
            $scope.EmailAdd.MailFrom = $scope.BusinessEmployee.Email;
            $scope.EmailAdd.MailTo = quo.CustomerContactEmail;
            $scope.EmailAdd.Subject = 'เอกสาร ' + docno;
            $scope.EmailAdd.Message = 'เรียน ' + quo.CustomerName + '\n\n' + $scope.BusinessProfile.BusinessName + ' เลขที่ ' + docno + ' มาให้ในอีเมลนี้\n\nกรุณาคลิกที่ไฟล์แนบ เพื่อดาวน์โหลดเอกสารของคุณ\n\nด้วยความเคารพ\n' + $scope.BusinessEmployee.EmployeeName + '\n' + $scope.BusinessProfile.BusinessName;
            $('#modalemail-send').modal('show');
        }

    };

    $scope.OnClickConfirmEmail = function () {
        $('#modalemail-send').modal('hide');
        var data = {
            MailFrom: $scope.EmailAdd.MailFrom,
            MailTo: $scope.EmailAdd.MailTo,
            MailCC: $scope.EmailAdd.MailCC,
            MailIsMe: $scope.EmailAdd.MailToMe ? "1" : "0",
            Subject: $scope.EmailAdd.Subject,
            Message: $scope.EmailAdd.Message,
            DocumentKey: $scope.EmailAdd.DocKey,
            DocumentNo: $scope.EmailAdd.DocNo,
            DocumentType: 'RE',
            Content: $scope.EmailAdd.Content1 == true && $scope.EmailAdd.Content2 == true ? "NORMAL" : $scope.EmailAdd.Content1 == true ? "ORIGINAL" : $scope.EmailAdd.Content2 == true ? "COPY" : "ORIGINAL",
        };

        $http.post(baseURL + "DocumentSell/PostEmailToSending", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {

                            showSuccessText(response.data.responsedata);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };
    /****************************/

    $scope.itemsPerPage = 20;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'ReceiptKey';
    $scope.reverseSort = true;


    $scope.pageCount = function () {
        return Math.ceil($scope.BusinessReceipt.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessReceipt.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

});


