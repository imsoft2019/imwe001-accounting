﻿HeaderConfig = { headers: { "Content-Type": "application/json; charset=utf-8", "dataType": "json" } };

WEACCTAPP.factory('paramService', ['$http', '$q', function ($http, $q) {
    var baseURL = $("base")[0].href;
    return {
        getDocumentNo: function (data) {
            return $http.post(baseURL + "Setting/PostGetDocumentNo", data, HeaderConfig, { cache: false });
        },
        getCustomer: function () {
            return $http.get(baseURL + "ContactBook/GetBusinessContactCustom?key=" + makeid(), { cache: false });
        },
        getProduct: function () {
            return $http.get(baseURL + "Product/GetBusinessProducts?key=" + makeid(), { cache: false });
        },
        getEmployee: function () {
            return $http.get(baseURL + "Setting/GetUserProfiles?key=" + makeid(), { cache: false });
        },
        getProfile: function () {
            return $http.get(baseURL + "Setting/GetIProfile?key=" + makeid(), { cache: false });
        },
        getParameterQuotation: function () {
            return $http.get(baseURL + "DocumentSell/GetParameterQuotation?key=" + makeid(), { cache: false });
        },
        getDocumentRemark: function (documenttype) {
            return $http.get(baseURL + "Setting/GetDocumentRemarkByType?key=" + makeid() + "&DocumentType=" + documenttype, { cache: false });
        },
        getBankBusiness: function () {
            return $http.get(baseURL + "Setting/GetBusinessBankAccount?key=" + makeid(), { cache: false });
        },
        getExpenseCatagory: function () {
            return $http.get(baseURL + "DocumentExpenses/GetExpenseCatagory?key=" + makeid(), { cache: false });
        },
        getSupplier: function () {
            return $http.get(baseURL + "ContactBook/GetBusinessContactSupplier?key=" + makeid(), { cache: false });
        },
        getBusinessProfile: function () {
            return $http.get(baseURL + "Setting/GetBusinessProfile?key=" + makeid(), { cache: false });
        },
    };
}]);

WEACCTAPP.factory('contactService', ['$http', '$q', function ($http, $q) {
    var baseURL = $("base")[0].href;
    return {
        filterContact: function (filter, list, mlist) {
            if (filter != undefined && filter != "") {
                list = _.filter(mlist, function (item) {

                    var c1 = true, c2 = true, c3 = true;
                    if (filter != undefined && filter != "" && item.ContactCode != undefined && (item.ContactCode).indexOf(filter) > -1)
                        c1 = true;
                    else if (item.ContactCode == undefined || (item.ContactCode).indexOf(filter) < 0)
                        c1 = false;

                    if (filter != undefined && filter != "" && item.BusinessName != undefined && (item.BusinessName).indexOf(filter) > -1)
                        c2 = true;
                    else if (item.BusinessName == undefined || (item.BusinessName).indexOf(filter) < 0)
                        c2 = false;

                    if (filter != undefined && filter != "" && item.TaxID != undefined && (item.TaxID).indexOf(filter) > -1)
                        c3 = true;
                    else if (item.TaxID == undefined || (item.TaxID).indexOf(filter) < 0)
                        c3 = false;

                    if (c1 || c2 || c3) {
                        return item;
                    }
                });
                return list;
            }
            else
                return mlist;
        },
        filterRevenue: function (filter) {
            var data = { NID: filter, Name: filter };
            return $http.post(baseURL + "ContactBook/GetRevenurevalue", data, HeaderConfig, { cache: false });
        },
        postNewContact: function (values) {
            var code, desc;
            if (values.ContactType1 && values.ContactType2) { code = 'A'; desc = 'ผู้จำหน่าย/ลูกค้า'; }
            else if (values.ContactType1) { code = 'C'; desc = 'ลูกค้า'; }
            else if (values.ContactType2) { code = 'S'; desc = 'ผู้จำหน่าย'; }
            var data = {
                ContactKey: values.ContactKey,
                BusinessType: values.BusinessType,
                BusinessTypeName: values.BusinessType == 'C' ? 'นิติบุคคล' : 'บุคคลธรรมดา',
                ContactType: code,
                ContactTypeName: desc,
                ContactCode: values.ContactCode,
                BusinessName: values.BusinessName,
                TaxID: values.TaxID,
                BranchType: values.BranchType,
                BranchTypeName: values.BranchType == 'H' ? 'สำนักงานใหญ่' : 'สาขา',
                BranchCode: values.BranchType == 'B' ? values.BranchCode : '',
                BranchName: values.BranchType == 'B' ? values.BranchName : 'สำนักงานใหญ่',
                Address: values.Address,
                ShippingAddress: values.ShippingAddress,
                OfficeNumber: values.OfficeNumber,
                FaxNumber: values.FaxNumber,
                WebSite: values.WebSite,
                CreditDate: values.CreditDate,
                ContactName: values.ContactName,
                ContactEmail: values.ContactEmail,
                ContactMobile: values.ContactMobile,
                BankCode: values.SelectBank != undefined ? values.SelectBank.ID : '',
                BankName: values.SelectBank != undefined ? values.SelectBank.Name : '',
                AccountType: values.AccountType,
                AccountTypeName: values.DepositType == 'S' ? 'ออมทรัพย์' : values.DepositType == 'C' ? 'กระแสรายวัน' : 'ฝากประจำ',
                AccountNo: values.AccountNo,
                AccountBranch: values.AccountBranch,
                Note: values.Note,
                RevenueFlag: values.RevenueFlag
            };
            return $http.post(baseURL + "ContactBook/PostBusinessContactReply", data, HeaderConfig, { cache: false });
        },
    };
}]);


WEACCTAPP.factory('productService', ['$http', '$q', function ($http, $q) {
    var baseURL = $("base")[0].href;
    return {
        filterProduct: function (filter, list, mlist) {
            if (filter != undefined && filter != "") {
                list = _.filter(mlist, function (item) {

                    var c1 = true, c2 = true;
                    if (filter != undefined && filter != "" && item.ProductCode != undefined && item.ProductCode != undefined && (item.ProductCode).indexOf(filter) > -1)
                        c1 = true;
                    else if (item.ProductCode == undefined || (item.ProductCode).indexOf(filter) < 0)
                        c1 = false;

                    if (filter != undefined && filter != "" && item.ProductName != undefined && (item.ProductName).indexOf(filter) > -1)
                        c2 = true;
                    else if (item.ProductName == undefined || (item.ProductName).indexOf(filter) < 0)
                        c2 = false;

                    if (c1 || c2) {
                        return item;
                    }
                });
                return list;
            }
            else
                return mlist;
        },
        postNewProduct: function (values, vat, unit, category) {

            var sell = 0, selltax = 0, selllast = 0;
            if (values.SellTaxType == 2) {
                sell = ConvertToDecimal(values.SellPrice == undefined ? 0 : values.SellPrice);
                selltax = sell - (sell / (parseInt(100) + parseInt(vat))) * 100;
                selllast = sell - selltax;
            }
            else if (values.SellTaxType == 1) {
                sell = ConvertToDecimal(values.SellPrice == undefined ? 0 : values.SellPrice);
                selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                selllast = sell + selltax;
            }
            else {
                sell = values.SellPrice == undefined ? 0 : values.SellPrice;
                selltax = 0;
                selllast = values.SellPrice == undefined ? 0 : values.SellPrice;
            }

            var buy = 0, buytax = 0, buylast = 0;
            if (values.BuyTaxType == 2) {
                buy = ConvertToDecimal(values.BuyPrice == undefined ? 0 : values.BuyPrice);
                buytax = buy - (buy / (parseInt(100) + parseInt(vat))) * 100;
                buylast = buy - buytax;
            }
            if (values.BuyTaxType == 1) {
                buy = ConvertToDecimal(values.BuyPrice == undefined ? 0 : values.BuyPrice);
                buytax = (buy * (parseInt(100) + parseInt(vat))) / 100 - buy;
                buylast = buy + buytax;
            }
            else {
                buy = values.BuyPrice == undefined ? 0 : values.BuyPrice;
                buytax = 0;
                buylast = values.BuyPrice == undefined ? 0 : values.BuyPrice;
            }

            var selltaxtype;
            if (values.SellTaxType == "1")
                selltaxtype = 'ราคาไม่รวม VAT';
            if (values.SellTaxType == "2")
                selltaxtype = 'ราคารวม VAT แล้ว';
            if (values.SellTaxType == "3")
                selltaxtype = 'VAT 0%';
            if (values.SellTaxType == "4")
                selltaxtype = 'สินค้าได้รับยกเว้นภาษี (N/A)';

            var buytaxtype;
            if (values.BuyTaxType == "1")
                buytaxtype = 'ราคาไม่รวม VAT';
            if (values.BuyTaxType == "2")
                buytaxtype = 'ราคารวม VAT แล้ว';
            if (values.BuyTaxType == "3")
                buytaxtype = 'VAT 0%';
            if (values.BuyTaxType == "4")
                buytaxtype = 'สินค้าได้รับยกเว้นภาษี (N/A)';



            var data = {
                ProductKey: values.ProductKey,
                ProductType: values.ProductType,
                ProductTypeName: values.ProductType == 'S' ? 'บริการ' : 'สินค้า',
                ProductCode: values.ProductCode,
                ProductName: values.ProductName,
                ProductDescription: values.ProductDescription,
                UnitTypeName: unit,
                CategoryName: category,
                SellPrice: ConvertToDecimal(sell),
                SellTaxPrice: ConvertToDecimal(selltax),
                SellAfterTaxPrice: ConvertToDecimal(selllast),
                SellTaxType: values.SellTaxType,
                SellTaxTypeName: selltaxtype,
                BuyPrice: values.ProductType == 'S' ? '' : ConvertToDecimal(buy),
                BuyTaxPrice: values.ProductType == 'S' ? '' : ConvertToDecimal(buytax),
                BuyAfterTaxPrice: values.ProductType == 'S' ? '' : ConvertToDecimal(buylast),
                BuyTaxType: values.ProductType == 'S' ? '' : values.BuyTaxType,
                BuyTaxTypeName: values.ProductType == 'S' ? '' : buytaxtype,
                BuyDescription: values.ProductType == 'S' ? '' : values.BuyDescription,
                ProductBarCode: values.ProductBarCode,
                ThumbnailPath: values.ThumbnailPath,
                PicturePath: values.PicturePath
            };

            return $http.post(baseURL + "Product/PostBusinessProductsReply", data, HeaderConfig, { cache: false });

        }
    };
}]);

