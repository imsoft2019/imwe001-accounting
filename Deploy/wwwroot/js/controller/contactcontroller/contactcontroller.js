﻿WEACCTAPP.controller('contactcontroller', function ($scope, $http, $timeout, GlobalVar) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessContact = [];
    $scope.BusinessRevenue = [];
    $scope.BusinessRevenue.List = [];
    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            if ($scope.LoadFirst == undefined) {
                $http.get(baseURL + "Setting/GetParameterBank?key=" + makeid())
                    .then(function (response) {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                                $scope.Parameter.Param_Bank = [];
                                $scope.Parameter.Param_Bank = response.data.responsedata;
                            }
                            else if (response.data.responsecode == '400') {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    });
                $scope.LoadFirst = 0;
            }

            $http.get(baseURL + "ContactBook/GetBusinessContact?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            $scope.BusinessContactMain = $scope.BusinessContact = response.data.responsedata;
                            $scope.Search.ContactType = 'ALL';

                            $scope.retpage = [];
                            $scope.range();
                            $scope.table.binding = 0;
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickSelectType = function (val) {
        
        if (val == 'ALL')
            $scope.BusinessContact = $scope.BusinessContactMain;
        else if (val == 'C') //client
            $scope.BusinessContact = _.where($scope.BusinessContactMain, { ContactType: 'C' });
        else if (val == 'S') //supplier
            $scope.BusinessContact = _.where($scope.BusinessContactMain, { ContactType: 'S' });
        else if (val == 'A') //supplier
            $scope.BusinessContact = _.where($scope.BusinessContactMain, { ContactType: 'A' });

        $scope.Search.ContactType = val;
        $scope.retpage = [];
        $scope.range();
    };

    $scope.OnClickSearch = function () {
        $scope.BusinessContact = $scope.BusinessContactMain;

        $scope.OnClickSelectType($scope.Search.ContactType);

        if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {
            $scope.BusinessContact = _.filter($scope.BusinessContactMain, function (item) {

                var c1 = true, c2 = true;
                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ContactCode != undefined && (item.ContactCode).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.ContactCode == undefined || (item.ContactCode).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;
                
                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.BusinessName != undefined && (item.BusinessName).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.BusinessName == undefined || (item.BusinessName).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;
                
                if (c1 || c2) {
                    if ($scope.Search.ContactType != 'ALL' && item.ContactType == $scope.Search.ContactType)
                        return item;
                    else if ($scope.Search.ContactType == 'ALL')
                        return item;
                }
            });
        }

    };

    $scope.OnClickAdd = function () {
        $scope.BusinessContactAdd = [];

        $scope.BusinessContactAdd.BusinessType = 'C';
        $scope.BusinessContactAdd.ContactType1 = true;
        $scope.BusinessContactAdd.ContactType2 = false;
        $scope.BusinessContactAdd.BranchType = "H";
        $scope.BusinessContactAdd.AccountType = 'S';

        var type = $scope.Parameter.Param_Bank.filter(function (item) { return item.ID == undefined; });
        if (type.length > 0)
            $scope.BusinessContactAdd.SelectBank = type[0];

        $('#modalcontact-add').modal('show');
    };

    $scope.OnClickRevenue = function () {
        $scope.BusinessRevenue = [];
        $scope.BusinessRevenue.List = [];
        $scope.revenue = [];
        $scope.revenue.binding = 0;
        $scope.revitemsCount = 0;
        $('#modalrevenute-list').modal('show');
    };

    $scope.OnClickFilterRevenue = function () {
        if ($scope.BusinessRevenue.InputText != undefined && $scope.BusinessRevenue.InputText != "") {
            $scope.revenue.binding = 1;
            var data = {
                NID: $scope.BusinessRevenue.InputText,
                Name: $scope.BusinessRevenue.InputText
            };

            $http.post(baseURL + "ContactBook/GetRevenurevalue", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.BusinessRevenue.List = [];
                                $scope.BusinessRevenue.List = response.data.responsedata;
                                $scope.revretpage = [];
                                $scope.revrange();
                                $scope.revenue.binding = 0;
                            }
                            else {
                                $scope.revenue.binding = 0;
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        $scope.revenue.binding = 0;
                        showErrorToast(err);
                    }
                });
        }
    };

    $scope.OnClickChooseRevenue = function (index) {
        try {
            var revenue = angular.copy($scope.BusinessRevenue.List[index]);
            $scope.BusinessContactAdd.BusinessName = revenue.CompanyName;
            $scope.BusinessContactAdd.TaxID = revenue.NID;
            if (revenue.BranchNo == "00000") {
                $scope.BusinessContactAdd.BranchType = "H";
                $scope.BusinessContactAdd.BranchCode = "";
            }
            else {
                $scope.BusinessContactAdd.BranchType = "B";
                $scope.BusinessContactAdd.BranchCode = revenue.BranchNo;
            }
            $scope.BusinessContactAdd.Address = revenue.Address;
            $('#modalrevenute-list').modal('hide');
        }
        catch (err) {
            showErrorToast(response.data.errormessage);
        }
    };

    $scope.OnClickUpdate = function (key) {
        var filter = _.where($scope.BusinessContact, { ContactKey: key })[0];
        if (filter != undefined) {
            $scope.BusinessContactAdd = [];
            $scope.BusinessContactAdd = angular.copy(filter);

            if ($scope.BusinessContactAdd.BankCode != undefined) {
                var type = $scope.Parameter.Param_Bank.filter(function (item) { return item.ID == $scope.BusinessContactAdd.BankCode; });
                if (type.length > 0)
                    $scope.BusinessContactAdd.SelectBank = type[0];

                if ($scope.BusinessContactAdd.ContactType == 'A') {
                    $scope.BusinessContactAdd.ContactType1 = $scope.BusinessContactAdd.ContactType2 = true;
                }
                else if ($scope.BusinessContactAdd.ContactType == 'C')
                    $scope.BusinessContactAdd.ContactType1 = true;
                else if ($scope.BusinessContactAdd.ContactType == 'S')
                    $scope.BusinessContactAdd.ContactType2 = true;
            }

            $('#modalcontact-add').modal('show');
        }
    };

    $scope.OnClickDuplicate = function (key) {
        var filter = _.where($scope.BusinessContact, { ContactKey: key })[0];
        if (filter != undefined) {
            $scope.BusinessContactAdd = [];
            $scope.BusinessContactAdd = angular.copy(filter);
            if ($scope.BusinessContactAdd != undefined) {
                if ($scope.BusinessContactAdd.BankCode != undefined) {
                    var type = $scope.Parameter.Param_Bank.filter(function (item) { return item.ID == $scope.BusinessContactAdd.BankCode; });
                    if (type.length > 0)
                        $scope.BusinessContactAdd.SelectBank = type[0];

                    if ($scope.BusinessContactAdd.ContactType == 'A') {
                        $scope.BusinessContactAdd.ContactType1 = $scope.BusinessContactAdd.ContactType2 = true;
                    }
                    else if ($scope.BusinessContactAdd.ContactType == 'C')
                        $scope.BusinessContactAdd.ContactType1 = true;
                    else if ($scope.BusinessContactAdd.ContactType == 'S')
                        $scope.BusinessContactAdd.ContactType2 = true;
                }

                $scope.BusinessContactAdd.ContactKey = undefined;
                $scope.BusinessContactAdd.RevenueFlag = undefined;

                $('#modalcontact-add').modal('show');
            }
        }
    };

    $scope.OnClickSave = function () {
        try {
            if ($scope.BusinessContactAdd.BusinessName == undefined || $scope.BusinessContactAdd.BusinessName == "")
                throw "กรุณาระบุชื่อธุรกิจ";
            else if ($scope.BusinessContactAdd.ContactType1 == false && $scope.BusinessContactAdd.ContactType2 == false)
                throw "กรุณาระบุประเภท";
            else if ($scope.BusinessContactAdd.ContactEmail != undefined && $scope.BusinessContactAdd.ContactEmail != "" && !ValidateEmail($scope.BusinessContactAdd.ContactEmail))
                throw "กรุณาแก้ไขอีเมล เนื่องจากรูปแบบอีเมลไม่ถูกต้อง";
            else {

                var code, desc;
                if ($scope.BusinessContactAdd.ContactType1 && $scope.BusinessContactAdd.ContactType2) { code = 'A'; desc = 'ผู้จำหน่าย/ลูกค้า'; }
                else if ($scope.BusinessContactAdd.ContactType1) { code = 'C'; desc = 'ลูกค้า'; }
                else if ($scope.BusinessContactAdd.ContactType2) { code = 'S'; desc = 'ผู้จำหน่าย'; }
                var data = {
                    ContactKey: $scope.BusinessContactAdd.ContactKey,
                    BusinessType: $scope.BusinessContactAdd.BusinessType,
                    BusinessTypeName: $scope.BusinessContactAdd.BusinessType == 'C' ? 'นิติบุคคล' : 'บุคคลธรรมดา',
                    ContactType: code,
                    ContactTypeName: desc,
                    ContactCode: $scope.BusinessContactAdd.ContactCode,
                    BusinessName: $scope.BusinessContactAdd.BusinessName,
                    TaxID: $scope.BusinessContactAdd.TaxID,
                    BranchType: $scope.BusinessContactAdd.BranchType,
                    BranchTypeName: $scope.BusinessContactAdd.BranchType == 'H' ? 'สำนักงานใหญ่' : 'สาขา',
                    BranchCode: $scope.BusinessContactAdd.BranchType == 'B' ? $scope.BusinessContactAdd.BranchCode : '',
                    BranchName: $scope.BusinessContactAdd.BranchType == 'B' ? $scope.BusinessContactAdd.BranchName : 'สำนักงานใหญ่',
                    Address: $scope.BusinessContactAdd.Address,
                    ShippingAddress: $scope.BusinessContactAdd.ShippingAddress,
                    OfficeNumber: $scope.BusinessContactAdd.OfficeNumber,
                    FaxNumber: $scope.BusinessContactAdd.FaxNumber,
                    WebSite: $scope.BusinessContactAdd.WebSite,
                    CreditDate: $scope.BusinessContactAdd.CreditDate,
                    ContactName: $scope.BusinessContactAdd.ContactName,
                    ContactEmail: $scope.BusinessContactAdd.ContactEmail,
                    ContactMobile: $scope.BusinessContactAdd.ContactMobile,
                    BankCode: $scope.BusinessContactAdd.SelectBank != undefined ? $scope.BusinessContactAdd.SelectBank.ID : '',
                    BankName: $scope.BusinessContactAdd.SelectBank != undefined ? $scope.BusinessContactAdd.SelectBank.Name : '',
                    AccountType: $scope.BusinessContactAdd.AccountType,
                    AccountTypeName: $scope.BusinessContactAdd.DepositType == 'S' ? 'ออมทรัพย์' : $scope.BusinessContactAdd.DepositType == 'C' ? 'กระแสรายวัน' : 'ฝากประจำ',
                    AccountNo: $scope.BusinessContactAdd.AccountNo,
                    AccountBranch: $scope.BusinessContactAdd.AccountBranch,
                    Note: $scope.BusinessContactAdd.Note,
                    RevenueFlag: $scope.BusinessContactAdd.RevenueFlag
                };

                $http.post(baseURL + "ContactBook/PostBusinessContact", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    $scope.init();
                                    $('#modalcontact-add').modal('hide');
                                    showSuccessToast();
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.BusinessContact, { ContactKey: val })[0];
        if (filter != undefined) {
            $scope.BusinessContactAdd = [];
            $scope.BusinessContactAdd = angular.copy(filter);
            $('#modalcontact-del').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            ContactKey: $scope.BusinessContactAdd.ContactKey
        };

        $http.post(baseURL + "ContactBook/DeleteBusinessContact", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalcontact-del').modal('hide');
                            $scope.init();
                            $scope.BusinessContactAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    /****************************/

    $scope.itemsPerPage = 10;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'ContactKey';
    $scope.reverseSort = false;

    $scope.pageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.BusinessContact.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessContact.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };


    /****************************/
    $scope.revcurrentPage = 0;

    $scope.revLimitFirst = 0;
    $scope.revLimitPage = 5;
    $scope.revitemsPerPage = 10;

    $scope.revpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.BusinessRevenue.List.length / $scope.revitemsPerPage) - 1;
    };

    $scope.revrange = function () {
        $scope.revitemsCount = $scope.BusinessRevenue.List.length;
        $scope.revpageshow = $scope.revpageCount() > $scope.revLimitPage && $scope.revpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.revpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.revpageshowdata = 1;
        $scope.revretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.revprevPage = function () {
        if ($scope.revcurrentPage > 0) {
            $scope.revcurrentPage--;
        }

        if ($scope.revcurrentPage < $scope.revLimitFirst && $scope.revcurrentPage >= 1) {
            $scope.revLimitFirst = $scope.revLimitFirst - 5;
            $scope.revLimitPage = $scope.revLimitPage - 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }
    };

    $scope.revprevPageDisabled = function () {
        return $scope.revcurrentPage === 0 ? "disabled" : "";
    };

    $scope.revnextPage = function () {
        if ($scope.revcurrentPage < $scope.revpageCount()) {
            $scope.revcurrentPage++;
        }

        if ($scope.revcurrentPage >= $scope.revLimitPage && $scope.revcurrentPage <= $scope.revpageCount()) {
            $scope.revLimitFirst = $scope.revLimitFirst + 5;
            $scope.revLimitPage = $scope.revLimitPage + 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }

    };

    $scope.revnextPageDisabled = function () {
        return $scope.revcurrentPage === $scope.revpageCount() ? "disabled" : "";
    };

    $scope.revsetPage = function (n) {
        $scope.revcurrentPage = n;
    };



});


