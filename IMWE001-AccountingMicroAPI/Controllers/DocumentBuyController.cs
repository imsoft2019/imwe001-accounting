﻿using System; 
using IMWE001_AccountingMicroAPI.Manager; 
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.DocumentSell; 
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/documentbuy")]
    [ApiController]
    public class DocumentBuyController : ManagerBaseController
    {
        #region PO (ใบสั่งซื้อ)
        [HttpPost("postpo/{businessid}")]
        public IActionResult postpo(Document_PurchaseOrder request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.SavePO(request))); }

        [HttpGet("getpo/{businessid}")]
        public IActionResult getListPO(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentBuyInterface.GetPOList(businessid))); }

        [HttpGet("getpobykey/{businessid}/{key}")]
        public IActionResult getPO(string businessid, int key)
        { addlog("businessid=" + businessid + " key=" + key); return ApiSync(() => Ok(base.IDocumentBuyInterface.GetPO(businessid, key))); }

        [HttpPost("poststatuspo/{businessid}")]
        public IActionResult updatestatuspo(Document_PurchaseOrder request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.UpdateStatusPO(request))); }

        [HttpPost("postdeletepo/{businessid}")]
        public IActionResult deletePO(Document_PurchaseOrder request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.DeletePO(request))); }
        #endregion

        #region RI (ใบรับสินค้า)
        [HttpPost("postri/{businessid}")]
        public IActionResult postRI(Document_ReceiveInventory request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.SaveRI(request))); }

        [HttpGet("getri/{businessid}")]
        public IActionResult getListRI(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentBuyInterface.GetRIList(businessid))); }

        [HttpGet("getribykey/{businessid}/{key}")]
        public IActionResult getRI(string businessid, int key)
        { addlog("businessid=" + businessid + " key=" + key); return ApiSync(() => Ok(base.IDocumentBuyInterface.GetRI(businessid, key))); }

        [HttpPost("poststatusri/{businessid}")]
        public IActionResult updatestatusri(Document_ReceiveInventory request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.UpdateStatusRI(request))); }

        [HttpPost("postdeleteri/{businessid}")]
        public IActionResult deleteRI(Document_ReceiveInventory request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.DeleteRI(request))); }

        [HttpPost("postreceiptpayment/{businessid}")]
        public IActionResult postreceiptpayment(Document_ReceiveInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.postreceiveinfobusinessid(request))); }


        [HttpPost("postreceipttax/{businessid}")]
        public IActionResult postreceipttax(Document_ReceiveTaxInvoiceInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.SaveReceiveTaxInfo(request))); }

        [HttpPost("postdeletereceipttax/{businessid}")]
        public IActionResult postdeletereceipttax(Document_ReceiveTaxInvoiceInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentBuyInterface.DeleteReceiveTax(request))); }
        #endregion
    }
}