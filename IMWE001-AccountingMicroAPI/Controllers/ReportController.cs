﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMWE001_AccountingMicroAPI.Manager;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/report")]
    [ApiController]
    public class ReportController : ManagerBaseController
    {

        #region Report
        [HttpPost("reportbilling/{businessid}")]
        public IActionResult postbillingreport(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportbilling(request))); }

        [HttpPost("reportsalessummary/{businessid}")]
        public IActionResult postsalesreport(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportsalessummary(request))); }

        [HttpPost("reportsalesbycustomer/{businessid}")]
        public IActionResult postsalecustomerreprt(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportsalesbycustomer(request))); }

        [HttpPost("reportreceipt/{businessid}")]
        public IActionResult postreportreceipt(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportreceipt(request))); }

        [HttpPost("reportpurchase/{businessid}")]
        public IActionResult postreportpurchase(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportpurchase(request))); }

        [HttpPost("reportreceivinginventory/{businessid}")]
        public IActionResult postreportreceivinginventory(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportreceivinginventory(request))); }

        [HttpPost("reportexpense/{businessid}")]
        public IActionResult postreportexpense(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportexpense(request))); }

        [HttpPost("reportsalestax/{businessid}")]
        public IActionResult postreportsalestax(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportsalestax(request))); }

        [HttpPost("reportpurchasevat/{businessid}")]
        public IActionResult postreportpurchasevat(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportpurchasevat(request))); }

        [HttpPost("reportwithholdingtax/{businessid}")]
        public IActionResult postreportwithholdingtax(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportwithholdingtax(request))); }

        [HttpPost("reportreceivable/{businessid}")]
        public IActionResult postreportreceivable(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportreceivable(request))); }

        [HttpPost("reportpayable/{businessid}")]
        public IActionResult postreportpayable(GetReportWithDate request)
        { addlog(request); return ApiSync(() => Ok(base.IReportInterface.getreportpayable(request))); }

        #endregion

    }
}