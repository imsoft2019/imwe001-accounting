﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMWE001_AccountingMicroAPI.Manager;
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/parameter")]
    [ApiController]
    public class ParameterController : ManagerBaseController
    {
        // GET api/values
        [HttpGet("get/bank")]
        public IActionResult getparambank(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IParameterInterface.getparambank())); }

        [HttpGet("get/product/{businessid}")]
        public IActionResult getparamproduct(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IParameterInterface.getparamproducts(businessid))); }

        [HttpGet("get/quotation/{businessid}")]
        public IActionResult getparamquotation(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IParameterInterface.getparamquotation(businessid))); }

        [HttpGet("get/position")]
        public IActionResult getposition()
        { return ApiSync(() => Ok(base.IParameterInterface.getposition())); }

    }
}
