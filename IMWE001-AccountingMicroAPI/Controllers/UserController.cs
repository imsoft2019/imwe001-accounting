﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using IMWE001_AccountingMicroAPI.Manager;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.User;
using log4net;
using log4net.Core; 
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/user")]
    [ApiController]
    public class UserController : ManagerBaseController
    {
        [HttpGet("profile")]
        public IActionResult get(string uid, string email)
        {
            addlog("uid=" + uid + " email=" + email);
            if (string.IsNullOrEmpty(email))
                return ApiSync(() => Ok(base.IUserInterface.get(new System.Guid(uid))));
            else
                return ApiSync(() => Ok(base.IUserInterface.get(new System.Guid(uid), email)));
        }

        [HttpGet("profiles")]
        public IActionResult get(string businessid)
        {
            addlog("businessid=" + businessid);
            return ApiSync(() => Ok(base.IUserInterface.getbyBusinessID(new System.Guid(businessid))));
        }


        [HttpPost]
        public IActionResult post(UserProfile request)
        {
            addlog(request);
            return ApiSync(() => Ok(base.IUserInterface.save(request)));
        }



        [HttpPost("Login")]
        public IActionResult Login(LoginViewModel request)
        {
            addlog(request);
            return ApiSync(() => Ok(base.IUserInterface.Login(request)));
        }

    }
}