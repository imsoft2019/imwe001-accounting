﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMWE001_AccountingMicroAPI.Manager;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/business")]
    [ApiController]
    public class BusinessController : ManagerBaseController
    {
        #region Profile
        // GET api/values
        [HttpGet("getprofile/{businessid}")]
        public IActionResult getprofilebusinessbyid(string businessid)
        {
            addlog("businessid=" + businessid);
            return ApiSync(() => Ok(base.IBusinessInterface.getbusinessprofilebyid(businessid)));
        }

        [HttpPost("postprofile/{businessid}")]
        public IActionResult postprofilebusinessbyid(BusinessProfile request)
        {
            addlog(request);
            return ApiSync(() => Ok(base.IBusinessInterface.postbusinessprofilebyid(request)));
        }

        [HttpPost("postprofileupload/{businessid}")]
        public IActionResult postprofileuploadbusinessbyid(BusinessProfile request)
        {
            addlog(request);
            return ApiSync(() => Ok(base.IBusinessInterface.postprofileuploadbusinessbyid(request)));
        }
        #endregion

        #region Account Bank
        [HttpGet("getbank/{businessid}")]
        public IActionResult getbankaccountbyid(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IBusinessInterface.getbankaccountbyid(businessid))); }

        [HttpPost("postbank/{businessid}")]
        public IActionResult postbankaccount(BusinessBankAccount request)
        { addlog(request); return ApiSync(() => Ok(base.IBusinessInterface.postbankaccount(request))); }

        [HttpGet("deletebank/{businessid}")]
        public IActionResult deletebankaccount(string uid)
        { addlog("uid=" + uid); return ApiSync(() => Ok(base.IBusinessInterface.deletebankaccount(uid))); }
        #endregion

        #region Document Remark
        [HttpGet("getdocumentremark/{businessid}")]
        public IActionResult getdocumentremark(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IBusinessInterface.getdocumentremarkbyid(businessid))); }

        [HttpGet("getdocumentremarkbytype/{businessid}")]
        public IActionResult getdocumentremarkbytype(string businessid, string documenttype)
        { addlog("businessid=" + businessid + " documenttype=" + documenttype); return ApiSync(() => Ok(base.IBusinessInterface.getdocumentremarkbytype(businessid, documenttype))); }

        [HttpPost("postdocumentremark/{businessid}")]
        public IActionResult postdocumentremark(DocumentRemark request)
        { addlog(request); return ApiSync(() => Ok(base.IBusinessInterface.postdocumentremark(request))); }

        [HttpGet("deletedocumentremark/{businessid}")]
        public IActionResult deletedocumentremark(string dockey)
        { addlog("dockey=" + dockey); return ApiSync(() => Ok(base.IBusinessInterface.deletedocumentremark(dockey))); }
        #endregion

        #region Docuemnt Runing
        [HttpGet("getdocumentparameter/{businessid}")]
        public IActionResult getdocumentparameter(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IBusinessInterface.getbusinessparameneterbyid(businessid))); }

        [HttpGet("getdocumentparameterheader/{businessid}")]
        public IActionResult getdocumentparameterheader(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IBusinessInterface.getbusinessparameneterdocumentbyid(businessid))); }


        [HttpPost("postdocumentparameter/{businessid}")]
        public IActionResult postdocumentparameter(DocumentRuning reqeust)
        { addlog(reqeust); return ApiSync(() => Ok(base.IBusinessInterface.postbusinessparameter(reqeust))); }

        [HttpPost("postgetdocumentno/{businessid}")]
        public IActionResult postgetdocumentno(GetDocumentNo reqeust)
        { addlog(reqeust); return ApiSync(() => Ok(base.IBusinessInterface.postgetdocumentno(reqeust))); }
        #endregion

        #region Activity
        [HttpGet("getactivity/{businessid}")]
        public IActionResult getactivity(string businessid)
        { return ApiSync(() => Ok(base.IBusinessInterface.getactivitylog(businessid))); }
        #endregion
    }
}