﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMWE001_AccountingMicroAPI.Manager;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Response;
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/contactbook")]
    [ApiController]
    public class ContactController : ManagerBaseController
    {

        #region Product
        [HttpGet("getcontact/{businessid}")]
        public IActionResult getbusinesscontactbyid(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IContactInterface.getcontactbusinessbyid(businessid))); }

        [HttpGet("getcontact/custom/{businessid}")]
        public IActionResult getbusinesscontactcustombyid(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IContactInterface.getcontactcustombusinessbyid(businessid))); }

        [HttpGet("getcontact/supplier/{businessid}")]
        public IActionResult getbusinesscontactsupplierbyid(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IContactInterface.getcontactsupplierbusinessbyid(businessid))); }

        [HttpPost("postcontact/{businessid}")]
        public IActionResult postcontact(Contact request)
        { addlog(request); return ApiSync(() => Ok(base.IContactInterface.postcontactbusinessid(request))); }

        [HttpPost("postcontactreply/{businessid}")]
        public IActionResult postcontactreply(Contact request)
        { addlog(request); return ApiSync(() => Ok(base.IContactInterface.postcontactreplybusinessid(request))); }

        [HttpPost("deletecontact/{businessid}")]
        public IActionResult deletecontact(Contact request)
        { addlog(request); return ApiSync(() => Ok(base.IContactInterface.deletecontactbusinessbyid(request))); }
        #endregion

    }
}