﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMWE001_AccountingMicroAPI.Manager;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/documentsell")]
    [ApiController]
    public class DocumentSellController : ManagerBaseController
    {
        #region Quotation
        [HttpPost("postquotation/{businessid}")]
        public IActionResult postquotation(Document_Quotation request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postquotationbusinessid(request))); }

        [HttpGet("getquotation/{businessid}")]
        public IActionResult getbusinessquotationbyid(string businessid, string sid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentSellInterface.getquotationbusinessbyid(businessid, sid))); }

        [HttpGet("getquotationbykey/{businessid}/{quotationkey}")]
        public IActionResult getquotationbykey(string businessid, int quotationkey)
        { addlog("businessid=" + businessid + " key=" + quotationkey); return ApiSync(() => Ok(base.IDocumentSellInterface.getquotationquotationkey(businessid, quotationkey))); }

        [HttpPost("deletequotation/{businessid}")]
        public IActionResult deletequotation(Document_Quotation request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deletequotationbusinessbyid(request))); }

        [HttpPost("postquotationnextflow/{businessid}")]
        public IActionResult postquotationnextworkflow(Document_Quotation request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postquotationnextworkflowbusinessid(request))); }
        #endregion

        #region Billing
        [HttpPost("postbilling/{businessid}")]
        public IActionResult postbilling(Document_Billing request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postbillingbusinessid(request))); }

        [HttpPost("postbillingcumulative/{businessid}")]
        public IActionResult postbillingcumulative(Document_Billing request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postbillingcumulativebusinessid(request))); }

        [HttpGet("getbilling/{businessid}")]
        public IActionResult getbusinessbillingbyid(string businessid, string sid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentSellInterface.getbillingbusinessbyid(businessid, sid))); }

        [HttpGet("getbillingbykey/{businessid}/{billingkey}")]
        public IActionResult getbillingbykey(string businessid, int billingkey)
        { addlog("businessid=" + businessid + " key=" + billingkey); return ApiSync(() => Ok(base.IDocumentSellInterface.getbillingbybillingkey(businessid, billingkey))); }

        [HttpPost("deletebilling/{businessid}")]
        public IActionResult deletebilling(Document_Billing request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deletebillingbusinessbyid(request))); }

        [HttpPost("postbillingnextflow/{businessid}")]
        public IActionResult postbillingnextworkflow(Document_Billing request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postbillingnextworkflowbusinessid(request))); }
        #endregion

        #region Invoice
        [HttpPost("postinvoice/{businessid}")]
        public IActionResult postinvoice(Document_Invoice request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postinvoicebusinessid(request))); }

        [HttpGet("getinvoice/{businessid}")]
        public IActionResult getbusinessinvoicebyid(string businessid, string sid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentSellInterface.getinvoicebusinessbyid(businessid, sid))); }

        [HttpGet("getinvoicebykey/{businessid}/{invoicekey}")]
        public IActionResult getinvoicebykey(string businessid, int invoicekey)
        { addlog("businessid=" + businessid + " key=" + invoicekey); return ApiSync(() => Ok(base.IDocumentSellInterface.getinvoicebyinvoicekey(businessid, invoicekey))); }

        [HttpPost("deleteinvoice/{businessid}")]
        public IActionResult deleteinvoice(Document_Invoice request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deleteinvoicebusinessbyid(request))); }

        [HttpPost("postinvoicenextflow/{businessid}")]
        public IActionResult postinvoicenextworkflow(Document_Invoice request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postinvoicenextworkflowbusinessid(request))); }
        #endregion

        #region Receipt
        [HttpPost("postreceipt/{businessid}")]
        public IActionResult postreceipt(Document_Receipt request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postreceiptbusinessid(request))); }

        [HttpPost("postreceiptcumulative/{businessid}")]
        public IActionResult postreceiptcumulative(Document_Receipt request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postreceiptcumulativebusinessid(request))); }

        [HttpGet("getreceipt/{businessid}")]
        public IActionResult getbusinessreceiptbyid(string businessid, string sid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentSellInterface.getreceiptbusinessbyid(businessid, sid))); }

        [HttpGet("getreceiptbykey/{businessid}/{receiptkey}")]
        public IActionResult getreceiptbykey(string businessid, int receiptkey)
        { addlog("businessid=" + businessid + " key=" + receiptkey); return ApiSync(() => Ok(base.IDocumentSellInterface.getreceiptbyreceiptkey(businessid, receiptkey))); }

        [HttpPost("deletereceipt/{businessid}")]
        public IActionResult deletereceipt(Document_Receipt request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deletereceiptbusinessbyid(request))); }

        [HttpPost("postreceiptnextflow/{businessid}")]
        public IActionResult postreceiptnextworkflow(Document_Receipt request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postreceiptnextworkflowbusinessid(request))); }

        [HttpPost("postreceiptpayment/{businessid}")]
        public IActionResult postreceiptpayment(Document_ReceiveInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postreceiveinfobusinessid(request))); }
        #endregion

        #region CreditNote
        [HttpPost("postcreditnote/{businessid}")]
        public IActionResult postCreditNote(Document_CreditNote request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postCreditNotebusinessid(request))); }

        [HttpGet("getcreditnote/{businessid}")]
        public IActionResult getbusinessCreditNotebyid(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentSellInterface.getCreditNotebusinessbyid(businessid))); }

        [HttpGet("getcreditnotebykey/{businessid}/{creditnotekey}")]
        public IActionResult getCreditNotebykey(string businessid, int creditnotekey)
        { addlog("businessid=" + businessid + " key=" + creditnotekey); return ApiSync(() => Ok(base.IDocumentSellInterface.getCreditNotebyCreditNotekey(businessid, creditnotekey))); }

        [HttpPost("deletecreditnote/{businessid}")]
        public IActionResult deleteCreditNote(Document_CreditNote request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deleteCreditNotebusinessbyid(request))); }

        [HttpPost("postcreditnotenextflow/{businessid}")]
        public IActionResult postCreditNotenextworkflow(Document_CreditNote request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postCreditNotenextworkflowbusinessid(request))); }
        #endregion

        #region DebitNote
        [HttpPost("postdebitnote/{businessid}")]
        public IActionResult postdebitnote(Document_DebitNote request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postDebitNotebusinessid(request))); }

        [HttpGet("getdebitnote/{businessid}")]
        public IActionResult getbusinessdebitnotebyid(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentSellInterface.getDebitNotebusinessbyid(businessid))); }

        [HttpGet("getdebitnotebykey/{businessid}/{debitnotekey}")]
        public IActionResult getdebitnotebykey(string businessid, int debitnotekey)
        { addlog("businessid=" + businessid + " key=" + debitnotekey); return ApiSync(() => Ok(base.IDocumentSellInterface.getDebitNotebyDebitNotekey(businessid, debitnotekey))); }

        [HttpPost("deletedebitnote/{businessid}")]
        public IActionResult deletedebitnote(Document_DebitNote request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deleteDebitNotebusinessbyid(request))); }

        [HttpPost("postdebitnotenextflow/{businessid}")]
        public IActionResult postdebitnotenextworkflow(Document_DebitNote request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postDebitNotenextworkflowbusinessid(request))); }
        #endregion

        #region Cumulative
        [HttpPost("getdocumentcumulative/{businessid}")]
        public IActionResult getdocumentcumulative(string businessid, List<GetDocumentCumulative> request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.getdocumentcumulative(businessid, request))); }
        #endregion

        #region CashSale
        [HttpPost("postcashsale/{businessid}")]
        public IActionResult postcashsale(Document_CashSale request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postCashSalebusinessid(request))); }

        [HttpGet("getcashsale/{businessid}")]
        public IActionResult getbusinesscashsalebyid(string businessid, string sid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentSellInterface.getCashSalebusinessbyid(businessid, sid))); }

        [HttpGet("getcashsalebykey/{businessid}/{cashsalekey}")]
        public IActionResult getcashsalebykey(string businessid, int cashsalekey)
        { addlog("businessid=" + businessid + " key=" + cashsalekey); return ApiSync(() => Ok(base.IDocumentSellInterface.getCashSalebyCashSalekey(businessid, cashsalekey))); }

        [HttpPost("deletecashsale/{businessid}")]
        public IActionResult deletecashsale(Document_CashSale request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deleteCashSalebusinessbyid(request))); }

        [HttpPost("postcashsalenextflow/{businessid}")]
        public IActionResult postcashsalenextworkflow(Document_CashSale request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postCashSalenextworkflowbusinessid(request))); }

        [HttpPost("postcashsalepayment/{businessid}")]
        public IActionResult postcashsalepayment(Document_ReceiveInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postcashsalereceiveinfobusinessid(request))); }
        #endregion

        #region Document Attach 

        [HttpPost("postdocumentattach/{businessid}")]
        public IActionResult postdocumentattach(Document_Attach request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.postDocumentAttach(request))); }

        [HttpGet("getdocumentattach/{businessid}/{documentkey}/{documenttype}")]
        public IActionResult getdocumentattach(string businessid, int documentkey, string documenttype)
        { addlog("businessid=" + businessid + " key=" + documentkey + " type=" + documenttype); return ApiSync(() => Ok(base.IDocumentSellInterface.getDocumentAttach(businessid, documentkey, documenttype))); }

        [HttpPost("deletedocumentattach/{businessid}")]
        public IActionResult deletedocumentattach(Document_Attach request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentSellInterface.deleteDocumentAttach(request))); }


        #endregion
    }
}