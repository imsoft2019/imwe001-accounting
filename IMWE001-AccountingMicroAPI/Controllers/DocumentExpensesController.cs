﻿using System;
using IMWE001_AccountingMicroAPI.Manager;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.DocumentExpenses;
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/documentexpenses")]
    [ApiController]
    public class DocumentExpensesController : ManagerBaseController
    {
        #region EX (ค่าใช้จ่าย)

        [HttpPost("postex/{businessid}")]
        public IActionResult postEX(Document_Expenses request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.SaveEX(request))); }

        [HttpGet("getex/{businessid}")]
        public IActionResult getListEX(string businessid,string sid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentExpensesInterface.GetEXList(businessid,sid))); }

        [HttpGet("getexbykey/{businessid}/{key}")]
        public IActionResult getEX(string businessid, int key)
        { addlog("businessid=" + businessid + " key=" + key); return ApiSync(() => Ok(base.IDocumentExpensesInterface.GetEX(businessid, key))); }

        [HttpPost("poststatusex/{businessid}")]
        public IActionResult updatestatusex(Document_Expenses request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.UpdateStatusEX(request))); }

        [HttpPost("postdeleteex/{businessid}")]
        public IActionResult deleteEX(Document_Expenses request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.DeleteEX(request))); }

        [HttpPost("postreceiptpayment/{businessid}")]
        public IActionResult postreceiptpayment(Document_ReceiveInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.postreceiveinfobusinessid(request))); }


        [HttpPost("postreceipttax/{businessid}")]
        public IActionResult postreceipttax(Document_ReceiveTaxInvoiceInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.SaveReceiveTaxInfo(request))); }

        [HttpPost("postdeletereceipttax/{businessid}")]
        public IActionResult postdeletereceipttax(Document_ReceiveTaxInvoiceInfo request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.DeleteReceiveTax(request))); }

        #endregion

        #region WT (หักภาษี ณ.ที่จ่าย)

        [HttpPost("postwt/{businessid}")]
        public IActionResult postwt(Document_WithholdingTax request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.SaveWT(request))); }

        [HttpGet("getwt/{businessid}")]
        public IActionResult getListWT(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IDocumentExpensesInterface.GetWTList(businessid))); }

        [HttpGet("getwtbykey/{businessid}/{key}")]
        public IActionResult getWT(string businessid, int key)
        { addlog("businessid=" + businessid + " key=" + key); return ApiSync(() => Ok(base.IDocumentExpensesInterface.GetWT(businessid, key))); }

        [HttpPost("poststatuswt/{businessid}")]
        public IActionResult updatestatuswt(Document_WithholdingTax request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.UpdateStatusWT(request))); }

        [HttpPost("postdeletewt/{businessid}")]
        public IActionResult deleteWT(Document_WithholdingTax request)
        { addlog(request); return ApiSync(() => Ok(base.IDocumentExpensesInterface.DeleteWT(request))); }


        #endregion
    }
}