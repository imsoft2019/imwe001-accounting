﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMWE001_AccountingMicroAPI.Manager;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Response;
using Microsoft.AspNetCore.Mvc;

namespace IMWE001_AccountingMicroAPI.Controllers
{
    [Route("api/v1/products")]
    [ApiController]
    public class ProductController : ManagerBaseController
    {

        #region Product
        [HttpGet("getproduct/{businessid}")]
        public IActionResult getbusinessproductbyid(string businessid)
        { addlog("businessid=" + businessid); return ApiSync(() => Ok(base.IProductManager.getproducbusinessbyid(businessid))); }

        [HttpPost("postproduct/{businessid}")]
        public IActionResult postproduct(Product request)
        { addlog(request); return ApiSync(() => Ok(base.IProductManager.postproductbusinessid(request))); }

        [HttpPost("postproductreply/{businessid}")]
        public IActionResult postproductreply(Product request)
        { addlog(request); return ApiSync(() => Ok(base.IProductManager.postproductreplybusinessid(request))); }

        [HttpPost("postproductupload/{businessid}")]
        public IActionResult postproductuploadbusinessbyid(Product request)
        { addlog(request); return ApiSync(() => Ok(base.IProductManager.postproductuploadbusinessbyid(request))); }

        [HttpPost("deleteproduct/{businessid}")]
        public IActionResult deleteproduct(Product request)
        { addlog(request); return ApiSync(() => Ok(base.IProductManager.deleteproductbusinessbyid(request))); }
        #endregion

    }
}