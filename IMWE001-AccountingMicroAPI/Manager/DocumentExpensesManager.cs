﻿using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.DocumentExpenses;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingRepository.Repository.DocumentExpenses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public interface IDocumentExpensesManager : IDisposable
    {
        #region EX (ค่าใช้จ่าย)

        Document_Expenses SaveEX(Document_Expenses input);
        string UpdateStatusEX(Document_Expenses input);
        List<Document_Expenses> GetEXList(string buinessid ,string sid);
        Document_Expenses GetEX(string buinessid, int key);
        string DeleteEX(Document_Expenses input);
        Document_ReceiveInfo postreceiveinfobusinessid(Document_ReceiveInfo request);
        Document_ReceiveTaxInvoiceInfo SaveReceiveTaxInfo(Document_ReceiveTaxInvoiceInfo input);

        Document_ReceiveTaxInvoiceInfo DeleteReceiveTax(Document_ReceiveTaxInvoiceInfo input);

        #endregion

        #region WT (หักภาษี ณ.ที่จ่าย)

        Document_WithholdingTax SaveWT(Document_WithholdingTax input);
        string UpdateStatusWT(Document_WithholdingTax input);
        List<Document_WithholdingTax> GetWTList(string buinessid);
        Document_WithholdingTax GetWT(string buinessid, int key);
        string DeleteWT(Document_WithholdingTax input);

        #endregion
    }
    public class DocumentExpensesManager : IDocumentExpensesManager
    {
        int isflagdelete = 0;
        int isactive = 1;

        #region EX (ค่าใช้จ่าย)

        public Document_Expenses SaveEX(Document_Expenses input)
        {
            using (var PORep = new Document_ExpensesRepository())
                PORep.Save(input);

            return input;
        }

        public string UpdateStatusEX(Document_Expenses input)
        {
            try
            {
                using (var doc = new Document_ExpensesRepository())
                    doc.UpdateStatus(input, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_Expenses> GetEXList(string buinessid,string sid)
        {
            List<Document_Expenses> result = null;
            using (var PORep = new Document_ExpensesRepository())
                result = PORep.GetWithRef(buinessid,sid);

            return result;
        }


        public Document_Expenses GetEX(string buinessid, int key)
        {
            Document_Expenses result = null;

            using (var PORep = new Document_ExpensesRepository())
                result = PORep.Get(buinessid, key);

            return result;
        }

        public string DeleteEX(Document_Expenses input)
        {
            try
            {
                string response = string.Empty;

                var val = new Document_Expenses();
                using (var poRep = new Document_ExpensesRepository())
                {
                    val = poRep.SelectFirstDataWithCondition(a => a.ExpensesKey == input.ExpensesKey && a.BusinessID == input.BusinessID);

                    if (val == null)
                        throw new Exception("ไม่พบข้อมูลใบรับสินค้า");

                    val.isdelete = isflagdelete;
                    val.UpdateBy = input.UpdateBy;
                    val.UpdateDate = input.UpdateDate;

                    poRep.Delete(input);
                }

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_ReceiveInfo postreceiveinfobusinessid(Document_ReceiveInfo request)
        {
            try
            {
                Document_ReceiveInfo response = new Document_ReceiveInfo();

                ValidateReceiver(request);

                using (var RIRep = new Document_ExpensesRepository())
                    response = RIRep.SaveReceivePayment(request);

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ValidateReceiver(Document_ReceiveInfo request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลบันทึกชำระเงิน");
            else if (!request.DocumentKey.HasValue)
                throw new Exception("กรุณากรอกข้อมูลรหัสเอกสาร");
        }

        public Document_ReceiveTaxInvoiceInfo SaveReceiveTaxInfo(Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveTaxInvoiceInfo result = null;

                ValidateReceiveTax(input);

                using (var RIRep = new Document_ExpensesRepository())
                    RIRep.SaveReceiveTax(input);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Document_ReceiveTaxInvoiceInfo DeleteReceiveTax(Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveTaxInvoiceInfo result = null;

                // ValidateReceiveTax(input);

                using (var RIRep = new Document_ExpensesRepository())
                    RIRep.DeleteReceiveTax(input);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ValidateReceiveTax(Document_ReceiveTaxInvoiceInfo input)
        {
            if (input == null)
                throw new Exception("กรุณากรอกข้อมูลใบกำกับภาษี");
            else if (string.IsNullOrEmpty(input.InvoiceNo))
                throw new Exception("กรุณากรอกข้อมูลหมายเลขใบกำกับภาษี");
            else if (!input.InvoiceDate.HasValue)
                throw new Exception("กรุณากรอกข้อมูลวันที่ใบกำกับภาษี");
            else if (string.IsNullOrEmpty(input.SupplierName))
                throw new Exception("กรุณากรอกข้อมูชื่อผู้จำหน่าย");
            else if (string.IsNullOrEmpty(input.SupplierTaxID))
                throw new Exception("กรุณากรอกข้อมูลหมายเลขผู้เสียยภาษี");
            else if (string.IsNullOrEmpty(input.SupplierBranch))
                throw new Exception("กรุณากรอกข้อมูลสำนักงาน/สาขา");

        }


        #endregion

        #region WT (หักภาษี ณ.ที่จ่าย)

        public Document_WithholdingTax SaveWT(Document_WithholdingTax  input)
        {
            using (var PORep = new Document_WithholdingTaxRepository())
            {
                PORep.Save(input);
            }

            return input;
        }

        public string UpdateStatusWT(Document_WithholdingTax input)
        {
            try
            {
                using (var doc = new Document_WithholdingTaxRepository())
                    doc.UpdateStatus(input, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_WithholdingTax> GetWTList(string buinessid)
        {
            List<Document_WithholdingTax> result = null;
            using (var PORep = new Document_WithholdingTaxRepository())
                result = PORep.GetWithRef(buinessid);

            return result;
        }

        public Document_WithholdingTax GetWT(string buinessid, int key)
        {
            Document_WithholdingTax result = null;

            using (var PORep = new Document_WithholdingTaxRepository())
                result = PORep.Get(buinessid, key);

            return result;
        }

        public string DeleteWT(Document_WithholdingTax input)
        {
            try
            {
                string response = string.Empty;

                var val = new Document_WithholdingTax();
                using (var poRep = new Document_WithholdingTaxRepository())
                {
                    val = poRep.SelectFirstDataWithCondition(a => a.WithholdingKey == input.WithholdingKey && a.BusinessID == input.BusinessID);

                    if (val == null)
                        throw new Exception("ไม่พบข้อมูลใบเสนอราคา");

                    val.isdelete = isflagdelete;
                    val.UpdateBy = input.UpdateBy;
                    val.UpdateDate = input.UpdateDate;

                    poRep.Delete(input);
                }

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        public void Dispose()
        {
            //throw new NotImplementedException();
        }
    }
}
