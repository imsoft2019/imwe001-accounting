﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingRepository.Repository.User;
using IMWE001_AccountingMicroAPI.Interfaces;
using IMWE001_AccountingRepository.Repository.Parameter;
using IMWE001_AccountingModels.Parameter;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class UserManager : IUserManager
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public UserProfile Login(LoginViewModel model)
        {
            UserProfile result = null;

            using (UserProfileRepository userRep = new UserProfileRepository())
                result = userRep.SelectDataWithCondition(S => S.Email == model.UserName).FirstOrDefault();

            if (result == null)
                throw new Exception("ไม่พบข้อมูลผู้ใช้งานในระบบ");
            else if (result.Pwd != model.PWD)
                throw new Exception("รหัสผ่านไม่ถูกต้อง");
            else if (!result.IsActive)
                throw new Exception("ท่านไม่มีสิทธิเข้าใช้งานระบบ");

            Param_Position position = new Param_Position();
            using (Param_PositionRepository Param_PositionRepository = new Param_PositionRepository())
                position = Param_PositionRepository.SelectFirstDataWithCondition(a => a.ID == result.Position);

            if (position != null)
                result.PositionName = position.Name;

                return result;
        }

        public UserProfile get(Guid uid)
        {
            UserProfile result = null;

            using (UserProfileRepository userRep = new UserProfileRepository())
                result = userRep.SelectDataWithCondition(S => S.UID == uid).FirstOrDefault();

            return result;
        }
        //getbyBusinessID
        public List<UserProfile> getbyBusinessID(Guid bussinessid)
        {
            List<UserProfile> result = null;

            using (UserProfileRepository userRep = new UserProfileRepository())
                result = userRep.SelectDataWithCondition(S => S.BusinessID == bussinessid && S.IsDelete == 1).OrderBy(c => c.CreateDate).ToList();

            return result;
        }

        public UserProfile get(Guid uid, string email)
        {
            UserProfile result = null;

            using (UserProfileRepository userRep = new UserProfileRepository())
                result = userRep.SelectFirstDataWithCondition(S => S.UID == uid && S.Email == email);

                return null;
        }

        public string save(UserProfile user)
        {
            try
            {
                VerifyUser(user);

                using (UserProfileRepository userRep = new UserProfileRepository())
                {
                    userRep.Save(user);
                }

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void VerifyUser(UserProfile user)
        {
            if (user == null)
                throw new Exception("กรุณากรอกข้อมูลผุ้ใช้งาน");
            else if (string.IsNullOrEmpty(user.Email))
                throw new Exception("กรุณากรอกข้อมูล email ผุ้ใช้งาน");
            else if (string.IsNullOrEmpty(user.Name))
                throw new Exception("กรุณากรอกข้อมูล ชื่อ ผุ้ใช้งาน");
            else if (string.IsNullOrEmpty(user.Surname))
                throw new Exception("กรุณากรอกข้อมูล นามสกุล ผุ้ใช้งาน");
            //else if (user.UID == new Guid())
            //    throw new Exception("ไม่พบข้อมูลบริษัทที่จะสังกัด");
            else if (string.IsNullOrEmpty(user.Position))
                throw new Exception("กรุณาระบุตำแหน่ง");
        }

    }
}
