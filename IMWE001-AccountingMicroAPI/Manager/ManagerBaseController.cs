﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.Web.Http;
using Newtonsoft.Json;
using IMWE001_AccountingMicroAPI.Interfaces;
using System.Diagnostics;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class ManagerBaseController : ControllerBase
    {
        public class UserValidationException : System.Exception { public UserValidationException(string Code, string Message) : base(Message) { ResCode = Code; } public string ResCode { get; set; } }
        public class RequestTimeoutException : System.Exception { public RequestTimeoutException(string Code, string Message) : base(Message) { ResCode = Code; } public string ResCode { get; set; } }

        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected IBusinessManager IBusinessInterface;
        protected IParameterManager IParameterInterface;
        protected IProductManager IProductManager;
        protected IUserManager IUserInterface;
        protected IContactManager IContactInterface;
        protected IDocumentSellManager IDocumentSellInterface;
        protected IDocumentBuyManager IDocumentBuyInterface;
        protected IDocumentExpensesManager IDocumentExpensesInterface;
        protected IReportManager IReportInterface;
        protected IExcelManager IExcelInterface;

        private string strToken = "Weacct-Apim-Subscription-Key";
        public string Subscriptkey
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Request.Headers[strToken]))
                    return string.Empty;

                return Request.Headers[strToken];
            }
        }

        public void addlog(string Param)
        {
            StackTrace stackTrace = new StackTrace();
            var Method = stackTrace.GetFrame(1).GetMethod().DeclaringType.ToString() + "." + stackTrace.GetFrame(1).GetMethod().Name;
            addlog(Method, Param);
        }

        protected void addlog(object Param)
        {
            StackTrace stackTrace = new StackTrace();
            var Method = stackTrace.GetFrame(1).GetMethod().DeclaringType.ToString() + "." + stackTrace.GetFrame(1).GetMethod().Name;
            addlog(Method, Param);
        }

        private void addlog(string Method, string Param)
        {
            log.InfoFormat("Method:{0}\nParam:{1}", Method, Param);
        }

        private void addlog(string Method, object Param)
        {
            log.InfoFormat("Method:{0}\nParam:{1}", Method, JsonConvert.SerializeObject(Param));
        }

        protected IActionResult ApiSync(Func<IActionResult> work)
        {
            try
            {

                if (string.IsNullOrEmpty(Subscriptkey))
                    throw new Exception("please input tokenid.");

                if (IBusinessInterface == null)
                    IBusinessInterface = new BusinessManager();

                if (IParameterInterface == null)
                    IParameterInterface = new ParameterManager();

                if (IProductManager == null)
                    IProductManager = new ProductManager();

                if (IUserInterface == null)
                    IUserInterface = new UserManager();

                if (IContactInterface == null)
                    IContactInterface = new ContactManager();

                if (IDocumentSellInterface == null)
                    IDocumentSellInterface = new DocumentSellManager();

                if (IDocumentBuyInterface == null)
                    IDocumentBuyInterface = new DocumentBuyManager();

                if (IDocumentExpensesInterface == null)
                    IDocumentExpensesInterface = new DocumentExpensesManager();

                if (IReportInterface == null)
                    IReportInterface = new ReportManager();

                if (IExcelInterface == null)
                    IExcelInterface = new ExcelManager();

                // string logInfoFormat = "Method : {0} \n Target : {1}";
                // string logmsg = string.Format(logInfoFormat, work.Method.Name, JsonConvert.SerializeObject(work.Target));
                /////log.Info(work.Method.Name)

                var result = work();

                var parentmethodname = work.Method.ReflectedType.ToString().Replace("+" + work.Method.ReflectedType.Name, "") + "." + work.Method.Name;

                //  StackTrace stackTrace = new StackTrace();
                //   var parentmethodname = stackTrace.GetFrame(1).GetMethod().DeclaringType.ToString() + "." + stackTrace.GetFrame(1).GetMethod().Name;
                // addlog(result);
                addlog(parentmethodname, result);
                return result;
            }
            catch (UserValidationException uex)
            {
                log.Warn("validate", uex);
                var msg = JsonConvert.SerializeObject(new { ResponseCode = uex.ResCode, ResponseMessage = uex.Message });
                return this.NotFound(msg);
            }
            catch (Exception ex)
            {
                log.Error("", ex);
                var msg = JsonConvert.SerializeObject(new { ResponseCode = "999", ResponseMessage = ex.Message });
                return this.NotFound(msg);
            }
        }
    }
}
