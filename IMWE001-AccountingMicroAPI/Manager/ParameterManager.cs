﻿using IMWE001_AccountingMicroAPI.Interfaces;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Parameter;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingRepository.Repository.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class ParameterManager : IParameterManager
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<Param_Bank> getparambank()
        {
            try
            {
                List<Param_Bank> response = new List<Param_Bank>();
                using (Param_BankRepository Param_BankRepository = new Param_BankRepository())
                    response = Param_BankRepository.GetDataAll();

                if (response == null)
                    throw new Exception("ไม่พบข้อมูลพารามิเตอร์ธนาคาร");

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public RParameter getparamproducts(string businessid)
        {
            try
            {
                RParameter response = new RParameter();

                List<Param_Category> category = new List<Param_Category>();
                using (Param_CategoryRepository Param_CategoryRepository = new Param_CategoryRepository())
                    category = Param_CategoryRepository.GetDataAll();

                List<Param_UnitType> unittype = new List<Param_UnitType>();
                using (Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository())
                    unittype = Param_UnitTypeRepository.GetDataAll();

                List<BusinessParameter> biz_param = new List<BusinessParameter>();
                using (BusinessParameterRepository BusinessParameterRepository = new BusinessParameterRepository())
                    biz_param = BusinessParameterRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper());

                response.Param_Category = category;
                response.Param_UnitType = unittype;
                response.Biz_Param = biz_param;

                if (response == null)
                    throw new Exception("ไม่พบข้อมูลพารามิเตอร์สินค้า");

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public RParameter getparamquotation(string businessid)
        {
            try
            {
                RParameter response = new RParameter();

                List<Param_Category> category = new List<Param_Category>();
                using (Param_CategoryRepository Param_CategoryRepository = new Param_CategoryRepository())
                    category = Param_CategoryRepository.GetDataAll();

                List<Param_UnitType> unittype = new List<Param_UnitType>();
                using (Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository())
                    unittype = Param_UnitTypeRepository.GetDataAll();

                List<BusinessParameter> biz_param = new List<BusinessParameter>();
                using (BusinessParameterRepository BusinessParameterRepository = new BusinessParameterRepository())
                    biz_param = BusinessParameterRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper());

                List<Param_Bank> bank = new List<Param_Bank>();
                using (Param_BankRepository Param_BankRepository = new Param_BankRepository())
                    bank = Param_BankRepository.GetDataAll();

                List<Param_WithholdingRate> withholding = new List<Param_WithholdingRate>();
                using (Param_WithholdingRateRepository Param_WithholdingRateRepository = new Param_WithholdingRateRepository())
                    withholding = Param_WithholdingRateRepository.GetDataAll();

                response.Param_Category = category;
                response.Param_UnitType = unittype;
                response.Param_Bank = bank;
                response.Biz_Param = biz_param;
                response.Param_WithholdingRate = withholding;

                if (response == null)
                    throw new Exception("ไม่พบข้อมูลพารามิเตอร์ใบเสนอราคา");

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Param_Position> getposition()
        {
            try
            {
                List<Param_Position> response = new List<Param_Position>();
                using (Param_PositionRepository positionRep = new Param_PositionRepository())
                    response = positionRep.GetDataAll();

                if (response == null)
                    throw new Exception("ไม่พบข้อมูลพารามิเตอร์ตำแหน่ง");

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
