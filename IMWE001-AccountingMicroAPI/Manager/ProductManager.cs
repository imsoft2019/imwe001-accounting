﻿using IMWE001_AccountingMicroAPI.Interfaces;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingRepository.Repository.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class ProductManager : IProductManager
    {
        int isactive = 1;
        int isflagdelete = 0;
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region Products
        public List<Product> getproducbusinessbyid(string businessid)
        {
            try
            {
                List<Product> response = new List<Product>();
                using (ProductRepository ProductRepository = new ProductRepository())
                    response = ProductRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderBy(a => a.ProductKey).ToList();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Product> getallproducbusinessbyid(string businessid)
        {
            try
            {
                List<Product> response = new List<Product>();
                using (ProductRepository ProductRepository = new ProductRepository())
                    response = ProductRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper()).OrderBy(a => a.ProductKey).ToList();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postproductbusinessid(Product request)
        {
            try
            {
                string response = string.Empty;

                if (request == null)
                    throw new Exception("กรุณากรอกข้อสินค้า");
                else if (string.IsNullOrEmpty(request.ProductName))
                    throw new Exception("กรุณากรอกชื่อสินค้า/บริการ");

                using (ProductRepository ProductRepository = new ProductRepository())
                    ProductRepository.Save(request);

                return request.ProductKey.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Product postproductreplybusinessid(Product request)
        {
            try
            {
                Product response = new Product();

                if (request == null)
                    throw new Exception("กรุณากรอกข้อสินค้า");
                else if (string.IsNullOrEmpty(request.ProductName))
                    throw new Exception("กรุณากรอกชื่อสินค้า/บริการ");

                using (ProductRepository ProductRepository = new ProductRepository())
                    response = ProductRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postproductuploadbusinessbyid(Product request)
        {
            try
            {
                string response = string.Empty;

                ValidateProduct(request);

                Product val = new Product();
                using (ProductRepository ProductRepository = new ProductRepository())
                    val = ProductRepository.SelectFirstDataWithCondition(a => a.ProductKey == request.ProductKey);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลสินค้า/บริการ");

                val.PicturePath = request.PicturePath;
                val.ThumbnailPath = request.ThumbnailPath;

                using (ProductRepository ProductRepository = new ProductRepository())
                    ProductRepository.Save(val);

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deleteproductbusinessbyid(Product request)
        {
            try
            {
                string response = string.Empty;

                ValidateProduct(request);

                Product val = new Product();
                using (ProductRepository ProductRepository = new ProductRepository())
                    val = ProductRepository.SelectFirstDataWithCondition(a => a.ProductKey == request.ProductKey);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลสินค้า/บริการ");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (ProductRepository ProductRepository = new ProductRepository())
                    ProductRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateProduct(Product request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลธุรกิจ");
            else if (!request.ProductKey.HasValue)
                throw new Exception("กรุณากรอกรหัสสินค้า/บริการ");
        }
        #endregion

    }
}
