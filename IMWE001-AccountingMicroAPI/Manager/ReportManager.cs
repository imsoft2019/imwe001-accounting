﻿using IMWE001_AccountingMicroAPI.Interfaces;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingRepository.Repository.ContactBook;
using IMWE001_AccountingRepository.Repository.DocumentSell;
using IMWE001_AccountingRepository.Repository.ReportPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class ReportManager : IReportManager
    {
        int isactive = 1;
        int isflagdelete = 0;
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region Report
        public List<DocumentReport> getreportbilling(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportBilling(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportsalessummary(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportSalesSummary(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportreceipt(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportReceipt(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportsalesbycustomer(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportSalesSummaryByCustomer(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportpurchase(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportPurchase(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportreceivinginventory(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportReceivingInventory(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportexpense(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportExpense(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportsalestax(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportSalesTax(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportpurchasevat(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportPurchaseVat(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportwithholdingtax(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportWithholdingTax(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportreceivable(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportAccountreceivable(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> getreportpayable(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (ReportRepository ReportRepository = new ReportRepository())
                    response = ReportRepository.GetReportAccountpayable(input);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}
