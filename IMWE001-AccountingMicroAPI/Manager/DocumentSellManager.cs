﻿using IMWE001_AccountingMicroAPI.Interfaces;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingRepository.Repository.ContactBook;
using IMWE001_AccountingRepository.Repository.DocumentSell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class DocumentSellManager : IDocumentSellManager
    {
        int isactive = 1;
        int isflagdelete = 0;
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region Document Quotation
        public Document_Quotation postquotationbusinessid(Document_Quotation request)
        {
            try
            {
                Document_Quotation response = new Document_Quotation();

                ValidateQuotation(request);

                using (Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository())
                    response = Document_QuotationRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_Quotation> getquotationbusinessbyid(string businessid,string sid)
        {
            try
            {
                List<Document_Quotation> response = new List<Document_Quotation>();
                using (Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository())
                    //response = Document_QuotationRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.QuotationKey).ToList();
                    response = Document_QuotationRepository.GetWithRef(businessid, sid);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Quotation getquotationquotationkey(string businessid, int QuotationKey)
        {
            try
            {
                Document_Quotation response = new Document_Quotation();
                using (Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository())
                    response = Document_QuotationRepository.Get(businessid, QuotationKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deletequotationbusinessbyid(Document_Quotation request)
        {
            try
            {
                string response = string.Empty;

                Document_Quotation val = new Document_Quotation();
                using (Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository())
                    val = Document_QuotationRepository.SelectFirstDataWithCondition(a => a.QuotationKey == request.QuotationKey && a.BusinessID == request.BusinessID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบเสนอราคา");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository())
                    Document_QuotationRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postquotationnextworkflowbusinessid(Document_Quotation request)
        {
            try
            {
                Document_Quotation response = new Document_Quotation();

                using (Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository())
                    Document_QuotationRepository.SaveWorkflow(request, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateQuotation(Document_Quotation request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลใบเสนอราคา");
            else if (string.IsNullOrEmpty(request.CustomerName))
                throw new Exception("กรุณากรอกชื่อลูกค้า");
        }
        #endregion

        #region Document Billing
        public Document_Billing postbillingbusinessid(Document_Billing request)
        {
            try
            {
                Document_Billing response = new Document_Billing();

                ValidateBilling(request);

                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    response = Document_BillingRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Billing postbillingcumulativebusinessid(Document_Billing request)
        {
            try
            {
                Document_Billing response = new Document_Billing();

                ValidateBilling(request);

                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    response = Document_BillingRepository.SaveCumulative(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_Billing> getbillingbusinessbyid(string businessid, string sid)
        {
            try
            {
                List<Document_Billing> response = new List<Document_Billing>();
                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    //response = Document_BillingRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.BillingKey).ToList();
                    response = Document_BillingRepository.GetWithRef(businessid, sid);


                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Billing getbillingbybillingkey(string businessid, int BillingKey)
        {
            try
            {
                Document_Billing response = new Document_Billing();
                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    response = Document_BillingRepository.Get(businessid, BillingKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deletebillingbusinessbyid(Document_Billing request)
        {
            try
            {
                string response = string.Empty;

                Document_Billing val = new Document_Billing();
                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    val = Document_BillingRepository.SelectFirstDataWithCondition(a => a.BillingKey == request.BillingKey && a.BusinessID == request.BusinessID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบวางบิล");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    Document_BillingRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postbillingnextworkflowbusinessid(Document_Billing request)
        {
            try
            {
                Document_Billing response = new Document_Billing();

                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    Document_BillingRepository.SaveWorkflow(request, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateBilling(Document_Billing request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลใบวางบิล");
            else if (string.IsNullOrEmpty(request.CustomerName))
                throw new Exception("กรุณากรอกชื่อลูกค้า");
        }
        #endregion

        #region Document Invoice
        public Document_Invoice postinvoicebusinessid(Document_Invoice request)
        {
            try
            {
                Document_Invoice response = new Document_Invoice();

                Validateinvoice(request);

                using (Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository())
                    response = Document_InvoiceRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_Invoice> getinvoicebusinessbyid(string businessid, string sid)
        {
            try
            {
                List<Document_Invoice> response = new List<Document_Invoice>();
                using (Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository())
                    //response = Document_InvoiceRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.InvoiceKey).ToList();
                    response = Document_InvoiceRepository.GetWithRef(businessid, sid);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Invoice getinvoicebyinvoicekey(string businessid, int invoiceKey)
        {
            try
            {
                Document_Invoice response = new Document_Invoice();
                using (Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository())
                    response = Document_InvoiceRepository.Get(businessid, invoiceKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deleteinvoicebusinessbyid(Document_Invoice request)
        {
            try
            {
                string response = string.Empty;

                Document_Invoice val = new Document_Invoice();
                using (Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository())
                    val = Document_InvoiceRepository.SelectFirstDataWithCondition(a => a.InvoiceKey == request.InvoiceKey && a.BusinessID == request.BusinessID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository())
                    Document_InvoiceRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postinvoicenextworkflowbusinessid(Document_Invoice request)
        {
            try
            {
                Document_Invoice response = new Document_Invoice();

                using (Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository())
                    Document_InvoiceRepository.SaveWorkflow(request, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Validateinvoice(Document_Invoice request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลใบแจ้งหนี้");
            else if (string.IsNullOrEmpty(request.CustomerName))
                throw new Exception("กรุณากรอกชื่อลูกค้า");
        }
        #endregion

        #region Document Receipt
        public Document_Receipt postreceiptbusinessid(Document_Receipt request)
        {
            try
            {
                Document_Receipt response = new Document_Receipt();

                ValidateReceipt(request);

                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    response = Document_ReceiptRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_Receipt> getreceiptbusinessbyid(string businessid, string sid)
        {
            try
            {
                List<Document_Receipt> response = new List<Document_Receipt>();
                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    //response = Document_ReceiptRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.ReceiptKey).ToList();
                    response = Document_ReceiptRepository.GetWithRef(businessid, sid);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Receipt getreceiptbyreceiptkey(string businessid, int ReceiptKey)
        {
            try
            {
                Document_Receipt response = new Document_Receipt();
                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    response = Document_ReceiptRepository.Get(businessid, ReceiptKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deletereceiptbusinessbyid(Document_Receipt request)
        {
            try
            {
                string response = string.Empty;

                Document_Receipt val = new Document_Receipt();
                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    val = Document_ReceiptRepository.SelectFirstDataWithCondition(a => a.ReceiptKey == request.ReceiptKey && a.BusinessID == request.BusinessID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    Document_ReceiptRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postreceiptnextworkflowbusinessid(Document_Receipt request)
        {
            try
            {
                Document_Receipt response = new Document_Receipt();

                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    Document_ReceiptRepository.SaveWorkflow(request, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Receipt postreceiptcumulativebusinessid(Document_Receipt request)
        {
            try
            {
                Document_Receipt response = new Document_Receipt();

                ValidateReceipt(request);

                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    response = Document_ReceiptRepository.SaveCumulative(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_ReceiveInfo postreceiveinfobusinessid(Document_ReceiveInfo request)
        {
            try
            {
                Document_ReceiveInfo response = new Document_ReceiveInfo();

                ValidateReceiver(request);

                using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                    response = Document_ReceiptRepository.SaveReceivePayment(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateReceipt(Document_Receipt request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลใบเสร็จรับเงิน");
            else if (string.IsNullOrEmpty(request.CustomerName))
                throw new Exception("กรุณากรอกชื่อลูกค้า");
        }

        public void ValidateReceiver(Document_ReceiveInfo request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลบันทึกชำระเงิน");
            else if (!request.DocumentKey.HasValue)
                throw new Exception("กรุณากรอกข้อมูลรหัสเอกสาร");
        }
        #endregion

        #region Document CreditNote
        public Document_CreditNote postCreditNotebusinessid(Document_CreditNote request)
        {
            try
            {
                Document_CreditNote response = new Document_CreditNote();

                ValidateCreditNote(request);

                using (Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository())
                    response = Document_CreditNoteRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_CreditNote> getCreditNotebusinessbyid(string businessid)
        {
            try
            {
                List<Document_CreditNote> response = new List<Document_CreditNote>();
                using (Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository())
                    //response = Document_CreditNoteRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.CreditNoteKey).ToList();
                    response = Document_CreditNoteRepository.GetWithRef(businessid);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_CreditNote getCreditNotebyCreditNotekey(string businessid, int CreditNoteKey)
        {
            try
            {
                Document_CreditNote response = new Document_CreditNote();
                using (Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository())
                    response = Document_CreditNoteRepository.Get(businessid, CreditNoteKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deleteCreditNotebusinessbyid(Document_CreditNote request)
        {
            try
            {
                string response = string.Empty;

                Document_CreditNote val = new Document_CreditNote();
                using (Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository())
                    val = Document_CreditNoteRepository.SelectFirstDataWithCondition(a => a.CreditNoteKey == request.CreditNoteKey && a.BusinessID == request.BusinessID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบลดหนี้");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository())
                    Document_CreditNoteRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postCreditNotenextworkflowbusinessid(Document_CreditNote request)
        {
            try
            {
                Document_CreditNote response = new Document_CreditNote();

                using (Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository())
                    Document_CreditNoteRepository.SaveWorkflow(request, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateCreditNote(Document_CreditNote request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลใบลดหนี้");
            else if (!request.InvoiceKey.HasValue)
                throw new Exception("กรุณาเลือกใบแจ้งหนี้");
        }
        #endregion

        #region Document DebitNote
        public Document_DebitNote postDebitNotebusinessid(Document_DebitNote request)
        {
            try
            {
                Document_DebitNote response = new Document_DebitNote();

                ValidateDebitNote(request);

                using (Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository())
                    response = Document_DebitNoteRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_DebitNote> getDebitNotebusinessbyid(string businessid)
        {
            try
            {
                List<Document_DebitNote> response = new List<Document_DebitNote>();
                using (Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository())
                    //response = Document_DebitNoteRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.DebitNoteKey).ToList();
                    response = Document_DebitNoteRepository.GetWithRef(businessid);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_DebitNote getDebitNotebyDebitNotekey(string businessid, int DebitNoteKey)
        {
            try
            {
                Document_DebitNote response = new Document_DebitNote();
                using (Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository())
                    response = Document_DebitNoteRepository.Get(businessid, DebitNoteKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deleteDebitNotebusinessbyid(Document_DebitNote request)
        {
            try
            {
                string response = string.Empty;

                Document_DebitNote val = new Document_DebitNote();
                using (Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository())
                    val = Document_DebitNoteRepository.SelectFirstDataWithCondition(a => a.DebitNoteKey == request.DebitNoteKey && a.BusinessID == request.BusinessID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบเพิ่มหนี้");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository())
                    Document_DebitNoteRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postDebitNotenextworkflowbusinessid(Document_DebitNote request)
        {
            try
            {
                Document_DebitNote response = new Document_DebitNote();

                using (Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository())
                    Document_DebitNoteRepository.SaveWorkflow(request, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateDebitNote(Document_DebitNote request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลใบเพิ่มหนี้");
            else if (!request.InvoiceKey.HasValue)
                throw new Exception("กรุณาเลือกใบแจ้งหนี้");
        }
        #endregion

        #region Documnent Cumulative
        public DocumentCumulative getdocumentcumulative(string businessid, List<GetDocumentCumulative> request)
        {
            try
            {
                DocumentCumulative response = new DocumentCumulative();
                using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                    response = Document_BillingRepository.GetDocumentCumulativeWithkey(businessid, request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Document CashSale
        public Document_CashSale postCashSalebusinessid(Document_CashSale request)
        {
            try
            {
                Document_CashSale response = new Document_CashSale();

                ValidateCashSale(request);

                using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                    response = Document_CashSaleRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_CashSale> getCashSalebusinessbyid(string businessid, string sid)
        {
            try
            {
                List<Document_CashSale> response = new List<Document_CashSale>();
                using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                    //response = Document_CashSaleRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.CashSaleKey).ToList();
                    response = Document_CashSaleRepository.GetWithRef(businessid, sid);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_CashSale getCashSalebyCashSalekey(string businessid, int CashSaleKey)
        {
            try
            {
                Document_CashSale response = new Document_CashSale();
                using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                    response = Document_CashSaleRepository.Get(businessid, CashSaleKey);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deleteCashSalebusinessbyid(Document_CashSale request)
        {
            try
            {
                string response = string.Empty;

                Document_CashSale val = new Document_CashSale();
                using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                    val = Document_CashSaleRepository.SelectFirstDataWithCondition(a => a.CashSaleKey == request.CashSaleKey && a.BusinessID == request.BusinessID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน (เงินสด)");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                    Document_CashSaleRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postCashSalenextworkflowbusinessid(Document_CashSale request)
        {
            try
            {
                Document_CashSale response = new Document_CashSale();

                using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                    Document_CashSaleRepository.SaveWorkflow(request, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_ReceiveInfo postcashsalereceiveinfobusinessid(Document_ReceiveInfo request)
        {
            try
            {
                Document_ReceiveInfo response = new Document_ReceiveInfo();

                ValidateReceiver(request);

                using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                    response = Document_CashSaleRepository.SaveReceivePayment(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateCashSale(Document_CashSale request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลใบเสร็จรับเงิน (เงินสด)");
            else if (string.IsNullOrEmpty(request.CustomerName))
                throw new Exception("กรุณากรอกชื่อลูกค้า");
        }
        #endregion

        #region Document Attach

        public List<Document_Attach> getDocumentAttach(string businessid, int documentkey , string documenttype)
        {
            try
            {
                List<Document_Attach> response = new List<Document_Attach>();
                using (Document_AttachRepository Document_AttachRepository = new Document_AttachRepository())
                    response = Document_AttachRepository.Get(businessid, documentkey , documenttype);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Attach postDocumentAttach(Document_Attach request)
        {
            try
            {
                Document_Attach response = new Document_Attach();

                using (Document_AttachRepository Document_AttachRepository = new Document_AttachRepository())
                    response = Document_AttachRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deleteDocumentAttach(Document_Attach request)
        {
            try
            {
                string response = string.Empty;

                using (Document_AttachRepository Document_AttachRepository = new Document_AttachRepository())
                    Document_AttachRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
