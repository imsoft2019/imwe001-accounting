﻿using IMWE001_AccountingMicroAPI.Interfaces;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingRepository.Repository.EventLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class BusinessManager : IBusinessManager
    {
        int isactive = 1;
        int isflagdelete = 0;
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region Business Profile
        public BusinessProfile getbusinessprofilebyid(string businessid)
        {
            try
            {
                BusinessProfile response = new BusinessProfile();
                using (BusinessProfileRepository BusinessProfileRepositor = new BusinessProfileRepository())
                    response = BusinessProfileRepositor.SelectFirstDataWithCondition(a => a.UID.ToString().ToUpper() == businessid.ToString().ToUpper());

                if (response == null)
                    throw new Exception("ไม่พบข้อมูลธุรกิจ");

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postbusinessprofilebyid(BusinessProfile request)
        {
            try
            {
                string response = string.Empty;

                if (request == null)
                    throw new Exception("กรุณากรอกข้อมูลธุรกิจ");
                else if (string.IsNullOrEmpty(request.BusinessName))
                    throw new Exception("กรุณากรอกชื่อธุรกิจ");

                using (BusinessProfileRepository BusinessProfileRepositor = new BusinessProfileRepository())
                    BusinessProfileRepositor.Save(request);

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postprofileuploadbusinessbyid(BusinessProfile request)
        {
            try
            {
                string response = string.Empty;

                if (request == null)
                    throw new Exception("กรุณากรอกข้อมูลธุรกิจ");
                else if (!request.UID.HasValue)
                    throw new Exception("กรุณากรอกรหัสธุรกิจ");

                BusinessProfile val = new BusinessProfile();
                using (BusinessProfileRepository BusinessProfileRepositor = new BusinessProfileRepository())
                    val = BusinessProfileRepositor.SelectFirstDataWithCondition(a => a.UID == request.UID);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลธุรกิจ");

                if (!string.IsNullOrEmpty(request.ThumnailLogo))
                {
                    val.LogoPath = request.LogoPath;
                    val.ThumnailLogo = request.ThumnailLogo;
                }
                if (!string.IsNullOrEmpty(request.RabberStampLogo))
                    val.RabberStampLogo = request.RabberStampLogo;

                using (BusinessProfileRepository BusinessProfileRepositor = new BusinessProfileRepository())
                    BusinessProfileRepositor.Save(val);

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Account bank
        public List<BusinessBankAccount> getbankaccountbyid(string businessid)
        {
            try
            {
                List<BusinessBankAccount> response = new List<BusinessBankAccount>();
                using (BusinessBankAccountRepository BusinessBankAccountRepository = new BusinessBankAccountRepository())
                    response = BusinessBankAccountRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postbankaccount(BusinessBankAccount request)
        {
            try
            {
                string response = string.Empty;

                if (request == null)
                    throw new Exception("กรุณากรอกข้อมูลธนาคาร");
                else if (string.IsNullOrEmpty(request.AccountNo))
                    throw new Exception("กรุณากรอกเลขที่บัญชี");
                else if (string.IsNullOrEmpty(request.AccountName))
                    throw new Exception("กรุณากรอกชื่อบัญชี");

                using (BusinessBankAccountRepository BusinessBankAccountRepository = new BusinessBankAccountRepository())
                    BusinessBankAccountRepository.Save(request);

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deletebankaccount(string uid)
        {
            try
            {
                string response = string.Empty;

                if (string.IsNullOrEmpty(uid))
                    throw new Exception("กรุณากรอกเลขบัญชีที่ต้องการลบ");

                using (BusinessBankAccountRepository BusinessBankAccountRepository = new BusinessBankAccountRepository())
                    BusinessBankAccountRepository.Delete(uid);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Document Remark
        public List<DocumentRemark> getdocumentremarkbyid(string businessid)
        {
            try
            {
                List<DocumentRemark> response = new List<DocumentRemark>();
                using (DocumentRemarkRepository DocumentRemarkRepository = new DocumentRemarkRepository())
                    response = DocumentRemarkRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.UpdateDate).ToList();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DocumentRemark getdocumentremarkbytype(string businessid, string documenttype)
        {
            try
            {
                DocumentRemark response = new DocumentRemark();
                using (DocumentRemarkRepository DocumentRemarkRepository = new DocumentRemarkRepository())
                    response = DocumentRemarkRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.DocumentType == documenttype && a.isdelete == isactive).FirstOrDefault();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string postdocumentremark(DocumentRemark request)
        {
            try
            {
                string response = string.Empty;

                if (request == null)
                    throw new Exception("กรุณากรอกข้อมูลหมายเหตุเอกสาร");
                else if (string.IsNullOrEmpty(request.DocumentType))
                    throw new Exception("กรุณาเลือกประเภทเอกสารหมายเหตุ");
                else if (string.IsNullOrEmpty(request.DocumentText))
                    throw new Exception("กรุณากรอกหมายเหตุเอกสาร");

                using (DocumentRemarkRepository DocumentRemarkRepository = new DocumentRemarkRepository())
                    DocumentRemarkRepository.Save(request);

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deletedocumentremark(string dockey)
        {
            try
            {
                string response = string.Empty;

                if (string.IsNullOrEmpty(dockey))
                    throw new Exception("กรุณากรอกเลขหมายเหตุเอกสาร");

                using (DocumentRemarkRepository DocumentRemarkRepository = new DocumentRemarkRepository())
                    DocumentRemarkRepository.Delete(dockey);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Document Runing
        public DocumentRuning getbusinessparameneterbyid(string businessid)
        {
            try
            {
                List<BusinessParameter> bizpara = new List<BusinessParameter>();
                using (BusinessParameterRepository BusinessParameterRepository = new BusinessParameterRepository())
                    bizpara = BusinessParameterRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper()).OrderByDescending(a => a.UpdateDate).ToList();

                List<DocumentPrefix> prefixpara = new List<DocumentPrefix>();
                //BusinessParameter runtype = new BusinessParameter();
                //runtype = bizpara.Where(a => a.ParameterField == "RunningType").FirstOrDefault();
                //string format = runtype.ParameterValue == "YY" ? DateTime.Now.Year.ToString() :  string.Format("{0}-{1}", DateTime.Now.Date.Year , DateTime.Now.Date.Month.ToString().PadLeft(2,'0'));

                using (DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository())
                    prefixpara = DocumentPrefixRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper()).ToList();// && a.Prefix == runtype.ParameterValue && a.Suffix == format).ToList();

                DocumentRuning response = new DocumentRuning();
                response.BizParam = bizpara;
                response.BizRun = prefixpara;

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public string postbusinessparameter(DocumentRuning request)
        {
            try
            {
                string response = string.Empty;

                if (request == null)
                    throw new Exception("กรุณากรอกข้อมูลรันนิ่งเอกสาร");

                using (BusinessParameterRepository BusinessParameterRepository = new BusinessParameterRepository())
                    BusinessParameterRepository.Save(request.BizParam);

                if (request.BizRun != null && request.BizRun.Count > 0)
                    using (DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository())
                        DocumentPrefixRepository.Save(request.BizRun);

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postgetdocumentno(GetDocumentNo request)
        {
            try
            {
                string response = string.Empty;

                if (request == null)
                    throw new Exception("กรุณากรอกข้อมูลดึงเลขรันนิ่ง");
                else if (string.IsNullOrEmpty(request.asofdate))
                    throw new Exception("กรุณากรอกข้อมูลกรอกวันที่");
                else if (string.IsNullOrEmpty(request.documenttype))
                    throw new Exception("กรุณากรอกข้อมูลกรอกรหัสหัวเอกสาร");

                using (DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository())
                    response = DocumentPrefixRepository.GetStaticDocumentNo(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<BusinessParameter> getbusinessparameneterdocumentbyid(string businessid)
        {
            try
            {
                List<BusinessParameter> resposne = new List<BusinessParameter>();
                using (BusinessParameterRepository BusinessParameterRepository = new BusinessParameterRepository())
                    resposne = BusinessParameterRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper()).OrderByDescending(a => a.UpdateDate).ToList();

                return resposne;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region Activity Log
        public List<Log_Activity> getactivitylog(string businessid)
        {
            try
            {
                List<Log_Activity> response = new List<Log_Activity>();
                using (Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository())
                    response = Log_ActivityRepository.GetActivity(businessid);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion
    }
}
