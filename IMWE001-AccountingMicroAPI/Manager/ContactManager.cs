﻿using IMWE001_AccountingMicroAPI.Interfaces;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingRepository.Repository.ContactBook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Manager
{
    public class ContactManager : IContactManager
    {
        int isactive = 1;
        int isflagdelete = 0;
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region Contact
        public List<Contact> getcontactbusinessbyid(string businessid)
        {
            try
            {
                List<Contact> response = new List<Contact>();
                using (ContactRepository ContactRepository = new ContactRepository())
                    response = ContactRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderBy(a => a.ContactKey).ToList();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Contact> getcontactcustombusinessbyid(string businessid)
        {
            try
            {
                List<Contact> response = new List<Contact>();
                using (ContactRepository ContactRepository = new ContactRepository())
                    response = ContactRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive && a.ContactType != "S").OrderBy(a => a.ContactKey).ToList();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Contact> getcontactsupplierbusinessbyid(string businessid)
        {
            try
            {
                List<Contact> response = new List<Contact>();
                using (ContactRepository ContactRepository = new ContactRepository())
                    response = ContactRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive && a.ContactType != "C").OrderBy(a => a.ContactKey).ToList();

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string postcontactbusinessid(Contact request)
        {
            try
            {
                string response = string.Empty;

                ValidateContact(request);

                using (ContactRepository ContactRepository = new ContactRepository())
                    ContactRepository.Save(request);

                return "บันทึกข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Contact postcontactreplybusinessid(Contact request)
        {
            try
            {
                Contact response = new Contact();

                ValidateContact(request);

                using (ContactRepository ContactRepository = new ContactRepository())
                    response = ContactRepository.Save(request);

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string deletecontactbusinessbyid(Contact request)
        {
            try
            {
                string response = string.Empty;

                Contact val = new Contact();
                using (ContactRepository ContactRepository = new ContactRepository())
                    val = ContactRepository.SelectFirstDataWithCondition(a => a.ContactKey == request.ContactKey);

                if (val == null)
                    throw new Exception("ไม่พบข้อมูลสินค้า/บริการ");

                val.isdelete = isflagdelete;
                val.UpdateBy = request.UpdateBy;
                val.UpdateDate = request.UpdateDate;

                using (ContactRepository ContactRepository = new ContactRepository())
                    ContactRepository.Delete(request);

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateContact(Contact request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อผู้ติดต่อ");
            else if (string.IsNullOrEmpty(request.ContactType))
                throw new Exception("กรุณากรอกประเภทผู้ติดต่อ");
            else if (string.IsNullOrEmpty(request.BusinessName))
                throw new Exception("กรุณากรอกชื่อธุรกิจ หรือชื่อนามสกุลผู้ติดต่อ");
        }
        #endregion

    }
}
