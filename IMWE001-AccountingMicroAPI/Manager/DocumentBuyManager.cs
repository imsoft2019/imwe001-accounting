﻿using System;
using System.Collections.Generic;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingRepository.Repository.DocumentBuy;
using IMWE001_AccountingRepository.Repository.DocumentSell;
namespace IMWE001_AccountingMicroAPI.Manager
{
    public class DocumentBuyManager : IMWE001_AccountingMicroAPI.Interfaces.IDocumentBuyManager
    {
        int isflagdelete = 0;
        int isactive = 1;

        #region PO (ใบสั่งซื้อ)

        public Document_PurchaseOrder SavePO(Document_PurchaseOrder input)
        {
            using (Document_PurchaseOrderRepository PORep = new Document_PurchaseOrderRepository())
            {
                PORep.Save(input);
            }

            return input;
        }

        public string UpdateStatusPO(Document_PurchaseOrder input)
        {
            try
            {
                using (var doc = new Document_PurchaseOrderRepository())
                    doc.UpdateStatus(input, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_PurchaseOrder> GetPOList(string buinessid)
        {
            List<Document_PurchaseOrder> result = null;
            using (Document_PurchaseOrderRepository PORep = new Document_PurchaseOrderRepository())
                result = PORep.GetWithRef(buinessid);

            return result;
        }


        public Document_PurchaseOrder GetPO(string buinessid, int key)
        {
            Document_PurchaseOrder result = null;

            using (Document_PurchaseOrderRepository PORep = new Document_PurchaseOrderRepository())
                result = PORep.Get(buinessid, key);

            return result;
        }

        public string DeletePO(Document_PurchaseOrder input)
        {
            try
            {
                string response = string.Empty;

                var val = new Document_PurchaseOrder();
                using (var poRep = new Document_PurchaseOrderRepository())
                {
                    val = poRep.SelectFirstDataWithCondition(a => a.PurchaseOrderKey == input.PurchaseOrderKey && a.BusinessID == input.BusinessID);

                    if (val == null)
                        throw new Exception("ไม่พบข้อมูลใบเสนอราคา");

                    val.isdelete = isflagdelete;
                    val.UpdateBy = input.UpdateBy;
                    val.UpdateDate = input.UpdateDate;

                    poRep.Delete(input);
                }

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region RI (ใบรับสินค้า)

        public Document_ReceiveInventory SaveRI(Document_ReceiveInventory input)
        {
            using (var PORep = new Document_ReceiveInventoryRepository())
                PORep.Save(input);

            return input;
        }

        public string UpdateStatusRI(Document_ReceiveInventory input)
        {
            try
            {
                using (var doc = new Document_ReceiveInventoryRepository())
                    doc.UpdateStatus(input, "");

                return "เปลี่ยนสถานะสำเร็จ";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_ReceiveInventory> GetRIList(string buinessid)
        {
            List<Document_ReceiveInventory> result = null;
            using (var PORep = new Document_ReceiveInventoryRepository())
                result = PORep.GetWithRef(buinessid);

            return result;
        }


        public Document_ReceiveInventory GetRI(string buinessid, int key)
        {
            Document_ReceiveInventory result = null;

            using (var PORep = new Document_ReceiveInventoryRepository())
                result = PORep.Get(buinessid, key);

            return result;
        }

        public string DeleteRI(Document_ReceiveInventory input)
        {
            try
            {
                string response = string.Empty;

                var val = new Document_ReceiveInventory();
                using (var poRep = new Document_ReceiveInventoryRepository())
                {
                    val = poRep.SelectFirstDataWithCondition(a => a.ReceiveInventoryKey == input.ReceiveInventoryKey && a.BusinessID == input.BusinessID);

                    if (val == null)
                        throw new Exception("ไม่พบข้อมูลใบรับสินค้า");

                    val.isdelete = isflagdelete;
                    val.UpdateBy = input.UpdateBy;
                    val.UpdateDate = input.UpdateDate;

                    poRep.Delete(input);
                }

                return "ลบข้อมูลเรียบร้อย";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_ReceiveInfo postreceiveinfobusinessid(Document_ReceiveInfo request)
        {
            try
            {
                Document_ReceiveInfo response = new Document_ReceiveInfo();

                ValidateReceiver(request);

                using (var RIRep = new Document_ReceiveInventoryRepository())
                    response = RIRep.SaveReceivePayment(request);

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ValidateReceiver(Document_ReceiveInfo request)
        {
            if (request == null)
                throw new Exception("กรุณากรอกข้อมูลบันทึกชำระเงิน");
            else if (!request.DocumentKey.HasValue)
                throw new Exception("กรุณากรอกข้อมูลรหัสเอกสาร");
        }

        public Document_ReceiveTaxInvoiceInfo SaveReceiveTaxInfo(Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveTaxInvoiceInfo result = null;

                ValidateReceiveTax(input);

                using (var RIRep = new Document_ReceiveInventoryRepository())
                    RIRep.SaveReceiveTax(input);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Document_ReceiveTaxInvoiceInfo DeleteReceiveTax(Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveTaxInvoiceInfo result = null;

               // ValidateReceiveTax(input);

                using (var RIRep = new Document_ReceiveInventoryRepository())
                    RIRep.DeleteReceiveTax(input);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ValidateReceiveTax(Document_ReceiveTaxInvoiceInfo input)
        {
            if (input == null)
                throw new Exception("กรุณากรอกข้อมูลใบกำกับภาษี");
            else if(string.IsNullOrEmpty(input.InvoiceNo))
                throw new Exception("กรุณากรอกข้อมูลหมายเลขใบกำกับภาษี");
            else if (!input.InvoiceDate.HasValue)
                throw new Exception("กรุณากรอกข้อมูลวันที่ใบกำกับภาษี");
            else if (string.IsNullOrEmpty(input.SupplierName))
                throw new Exception("กรุณากรอกข้อมูชื่อผู้จำหน่าย");
            else if (string.IsNullOrEmpty(input.SupplierTaxID))
                throw new Exception("กรุณากรอกข้อมูลหมายเลขผู้เสียยภาษี");
            else if (string.IsNullOrEmpty(input.SupplierBranch))
                throw new Exception("กรุณากรอกข้อมูลสำนักงาน/สาขา");
         
        }


        #endregion

        public void Dispose()
        {
            //throw new NotImplementedException();
        }
    }
}