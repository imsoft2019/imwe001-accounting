﻿using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IProductManager : IDisposable
    {
        List<Product> getproducbusinessbyid(string businessid);

        string postproductbusinessid(Product request);

        Product postproductreplybusinessid(Product request);

        string postproductuploadbusinessbyid(Product request);

        string deleteproductbusinessbyid(Product request);
    }
}
