﻿using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IContactManager : IDisposable
    {
        List<Contact> getcontactbusinessbyid(string businessid);

        List<Contact> getcontactcustombusinessbyid(string businessid);

        List<Contact> getcontactsupplierbusinessbyid(string businessid);
        string postcontactbusinessid(Contact request);

        Contact postcontactreplybusinessid(Contact request);

        string deletecontactbusinessbyid(Contact request);
    }
}
