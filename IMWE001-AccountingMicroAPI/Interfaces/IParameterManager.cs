﻿using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IParameterManager : IDisposable
    {
        List<Param_Bank> getparambank();
        List<Param_Position> getposition();
        RParameter getparamproducts(string businessid);
        RParameter getparamquotation(string businessid);

    }
}
