﻿using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IBusinessManager : IDisposable
    {
        BusinessProfile getbusinessprofilebyid(string businessid);

        string postbusinessprofilebyid(BusinessProfile request);

        string postprofileuploadbusinessbyid(BusinessProfile request);

        List<BusinessBankAccount> getbankaccountbyid(string businessid);

        string postbankaccount(BusinessBankAccount request);

        string deletebankaccount(string uid);

        string postdocumentremark(DocumentRemark request);

        List<DocumentRemark> getdocumentremarkbyid(string businessid);

        DocumentRemark getdocumentremarkbytype(string businessid, string documenttype);

        string deletedocumentremark(string dockey);

        DocumentRuning getbusinessparameneterbyid(string businessid);

        List<BusinessParameter> getbusinessparameneterdocumentbyid(string businessid);

        string postbusinessparameter(DocumentRuning request);

        string postgetdocumentno(GetDocumentNo request);

        List<Log_Activity> getactivitylog(string businessid);

    }
}
