﻿using System;
using System.Collections.Generic;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IDocumentBuyManager : IDisposable
    {
        #region PO (ใบสั่งซื้อ)
        Document_PurchaseOrder SavePO(Document_PurchaseOrder input);
        List<Document_PurchaseOrder> GetPOList(string buinessid);
        Document_PurchaseOrder GetPO(string buinessid, int key);
        string UpdateStatusPO(Document_PurchaseOrder input);
        string DeletePO(Document_PurchaseOrder input);
        #endregion

        #region RI (ใบรับสินค้า)
        Document_ReceiveInventory SaveRI(Document_ReceiveInventory input);
        string UpdateStatusRI(Document_ReceiveInventory input);
        List<Document_ReceiveInventory> GetRIList(string buinessid);
        Document_ReceiveInventory GetRI(string buinessid, int key);
        string DeleteRI(Document_ReceiveInventory input);
        Document_ReceiveInfo postreceiveinfobusinessid(Document_ReceiveInfo request);
        Document_ReceiveTaxInvoiceInfo SaveReceiveTaxInfo(Document_ReceiveTaxInvoiceInfo input);

        Document_ReceiveTaxInvoiceInfo DeleteReceiveTax(Document_ReceiveTaxInvoiceInfo input);

        #endregion
    }
}
