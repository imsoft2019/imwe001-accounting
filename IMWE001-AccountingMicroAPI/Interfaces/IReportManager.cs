﻿using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IReportManager : IDisposable
    {
        List<DocumentReport> getreportbilling(GetReportWithDate input);

        List<DocumentReport> getreportsalessummary(GetReportWithDate input);

        List<DocumentReport> getreportreceipt(GetReportWithDate input);

        List<DocumentReport> getreportsalesbycustomer(GetReportWithDate input);

        List<DocumentReport> getreportpurchase(GetReportWithDate input);

        List<DocumentReport> getreportreceivinginventory(GetReportWithDate input);

        List<DocumentReport> getreportexpense(GetReportWithDate input);

        List<DocumentReport> getreportsalestax(GetReportWithDate input);

        List<DocumentReport> getreportpurchasevat(GetReportWithDate input);

        List<DocumentReport> getreportwithholdingtax(GetReportWithDate input);

        List<DocumentReport> getreportreceivable(GetReportWithDate input);

        List<DocumentReport> getreportpayable(GetReportWithDate input);
    }
}
