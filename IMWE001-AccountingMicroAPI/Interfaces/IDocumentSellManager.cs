﻿using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IDocumentSellManager : IDisposable
    {
        #region Quotation
        Document_Quotation postquotationbusinessid(Document_Quotation request);

        List<Document_Quotation> getquotationbusinessbyid(string businessid, string sid);

        Document_Quotation getquotationquotationkey(string businessid, int QuotationKey);

        string deletequotationbusinessbyid(Document_Quotation request);

        string postquotationnextworkflowbusinessid(Document_Quotation request);
        #endregion

        #region Billing
        Document_Billing postbillingbusinessid(Document_Billing request);

        Document_Billing postbillingcumulativebusinessid(Document_Billing request);

        List<Document_Billing> getbillingbusinessbyid(string businessid, string sid);

        Document_Billing getbillingbybillingkey(string businessid, int BillingKey);

        string deletebillingbusinessbyid(Document_Billing request);

        string postbillingnextworkflowbusinessid(Document_Billing request);
        #endregion

        #region Invoice
        Document_Invoice postinvoicebusinessid(Document_Invoice request);

        List<Document_Invoice> getinvoicebusinessbyid(string businessid, string sid);

        Document_Invoice getinvoicebyinvoicekey(string businessid, int invoiceKey);

        string deleteinvoicebusinessbyid(Document_Invoice request);

        string postinvoicenextworkflowbusinessid(Document_Invoice request);
        #endregion

        #region Receipt
        Document_Receipt postreceiptbusinessid(Document_Receipt request);

        Document_Receipt postreceiptcumulativebusinessid(Document_Receipt request);

        List<Document_Receipt> getreceiptbusinessbyid(string businessid, string sid);

        Document_Receipt getreceiptbyreceiptkey(string businessid, int ReceiptKey);

        string deletereceiptbusinessbyid(Document_Receipt request);

        string postreceiptnextworkflowbusinessid(Document_Receipt request);

        Document_ReceiveInfo postreceiveinfobusinessid(Document_ReceiveInfo request);

        #endregion

        #region CreditNote
        Document_CreditNote postCreditNotebusinessid(Document_CreditNote request);

        List<Document_CreditNote> getCreditNotebusinessbyid(string businessid);

        Document_CreditNote getCreditNotebyCreditNotekey(string businessid, int CreditNoteKey);

        string deleteCreditNotebusinessbyid(Document_CreditNote request);

        string postCreditNotenextworkflowbusinessid(Document_CreditNote request);
        #endregion

        #region DebitNote
        Document_DebitNote postDebitNotebusinessid(Document_DebitNote request);

        List<Document_DebitNote> getDebitNotebusinessbyid(string businessid);

        Document_DebitNote getDebitNotebyDebitNotekey(string businessid, int DebitNoteKey);

        string deleteDebitNotebusinessbyid(Document_DebitNote request);

        string postDebitNotenextworkflowbusinessid(Document_DebitNote request);
        #endregion

        #region Cumulative
        DocumentCumulative getdocumentcumulative(string businessid, List<GetDocumentCumulative> request);
        #endregion

        #region CashSale
        Document_CashSale postCashSalebusinessid(Document_CashSale request);

        List<Document_CashSale> getCashSalebusinessbyid(string businessid, string sid);

        Document_CashSale getCashSalebyCashSalekey(string businessid, int CashSaleKey);

        string deleteCashSalebusinessbyid(Document_CashSale request);

        string postCashSalenextworkflowbusinessid(Document_CashSale request);

        Document_ReceiveInfo postcashsalereceiveinfobusinessid(Document_ReceiveInfo request);

        #endregion

        #region Document Attach 

        List<Document_Attach> getDocumentAttach(string businessid, int documentkey, string documenttype);

        Document_Attach postDocumentAttach(Document_Attach request);

        string deleteDocumentAttach(Document_Attach request);
        #endregion
    }
}
