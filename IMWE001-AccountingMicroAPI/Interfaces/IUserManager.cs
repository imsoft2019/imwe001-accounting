﻿using IMWE001_AccountingModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_AccountingMicroAPI.Interfaces
{
    public interface IUserManager : IDisposable
    {
        UserProfile Login(LoginViewModel model);
        UserProfile get(Guid uid);

        List<UserProfile> getbyBusinessID(Guid BusinessID);
        UserProfile get(Guid uid, string email);
        string save(UserProfile user);

    }
}
