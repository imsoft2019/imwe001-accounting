﻿WEACCTAPP.controller('reportsalesbycustomercontroller', function ($scope, $http, $timeout, GlobalVar, paramService, $q) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.Parameter = [];
    $scope.Parameter.Param_Contact = [];
    $scope.Parameter.Param_ContactFilter = [];
    $scope.BusinessDocumentByCustomer = [];
    $scope.BusinessDocumentMain = [];
    $scope.table = [];
    $scope.Search = [];
    $scope.BusinessHeader = [];

    var gCustomer = paramService.getCustomer();

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.Selectionfilter = '2';
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputToMonth = GetDatetimeNowMonth();
            $scope.Search.SelectTab = undefined;

            var qq = $q.all([gCustomer]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                            $scope.SearchContact = [];
                            $scope.Parameter.Param_Contact = [];

                            $scope.Parameter.Param_Contact = data[0].data.responsedata;

                            $scope.Parameter.Param_ContactFilter.unshift({ ContactKey: '0', BusinessName: 'แสดงทั้งหมด' });
                            var type = $scope.Parameter.Param_ContactFilter.filter(function (item) { return item.ContactKey == '0'; });
                            if (type.length > 0)
                                $scope.Search.SelectCustomer = type[0];

                            $scope.table.binding = 0;
                        }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                    //$scope.OnClickSearch();
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnSelection = function () {
        if ($scope.Search.Selectionfilter == '2')
            $scope.Search.InputToMonth = undefined;
        else if ($scope.Search.Selectionfilter == '4')
            $scope.Search.InputToYear = undefined;
        else if ($scope.Search.Selectionfilter == '5')
            $scope.Search.InputFromDate = $scope.Search.InputToDate = undefined;
        else {
            $scope.OnClickSearch();
        }
    };

    $scope.OnChangeDate = function () {
        if ($scope.Search.Selectionfilter == '2') {
            if ($scope.Search.InputToMonth != undefined && $scope.Search.InputToMonth != '')
                $scope.OnClickSearch();
        }
        else if ($scope.Search.Selectionfilter == '4') {
            if ($scope.Search.InputToYear != undefined && $scope.Search.InputToYear != '')
                $scope.OnClickSearch();
        }
        else if ($scope.Search.Selectionfilter == '5') {
            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != '' && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != '')
                $scope.OnClickSearch();
        }
    };

    $scope.OnChangeStatus = function () {

        if ($scope.BusinessDocumentByCustomerMain != undefined && $scope.BusinessDocumentByCustomerMain.length > 0) {
            $scope.table.binding = 1;

            var businessvalue = angular.copy($scope.BusinessDocumentByCustomerMain);
            if ($scope.Search.SelectCustomer.ContactKey != '0') {
                businessvalue = _.where(businessvalue, { ContactKey: $scope.Search.SelectCustomer.ContactKey });
            }

            if ($scope.Search.SelectStatus != '0') {
                var total = 0;
                _.each(businessvalue, function (cus) {

                    var docver = _.where(cus.Document, { DocumentStatus: $scope.Search.SelectStatus });
                    cus.Document = [];
                    cus.Document = docver;

                    var amount = 0;

                    _.each(cus.Document, function (inv) {

                        if (inv.DocumentStatus != '3') {

                            if (inv.DocumentType == 'CN') {
                                total = total - inv.Total;
                                amount = amount - inv.Total;
                            }
                            else {
                                total = total + inv.Total;
                                amount = amount + inv.Total;
                            }
                        }
                    });
                    cus.Total = AFormatNumber(amount, 2);
                });
            }

            $scope.Search.SelectTab = businessvalue[0].ContactKey;
            $scope.BusinessDocumentByCustomer = businessvalue;

            if ($scope.Search.SelectStatus == '0')
                $scope.BusinessHeader.Amount = $scope.BusinessHeader.AmountFix;
            else
                $scope.BusinessHeader.Amount = AFormatNumber(total, 2);
            $scope.table.binding = 0;
        }
    };

    $scope.OnClickCustomer = function (val) {
        if ($scope.Search.SelectTab == val)
            $scope.Search.SelectTab = undefined;
        else
            $scope.Search.SelectTab = val;
    };

    $scope.OnClickSearch = function () {
        $scope.table.binding = 1;
        $scope.Search.SelectStatus = '0';
        var data = {
            typedate: $scope.Search.Selectionfilter,
            startdate: $scope.Search.Selectionfilter == '2' ? ToJsonDate2('01-' + $scope.Search.InputToMonth)
                : $scope.Search.Selectionfilter == '4' ? ToJsonDate2('01-01-' + $scope.Search.InputToYear)
                    : $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputFromDate) : undefined,
            enddate: $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputToDate) : undefined
        };

        $http.post(baseURL + "Report/PostReportSalesCustomer", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.BusinessDocumentMain = [];
                            $scope.BusinessDocumentMain = response.data.responsedata;

                            $scope.BusinessDocument = $scope.BusinessDocumentMain;

                            $scope.BusinessDocumentByCustomerMain = $scope.BusinessDocumentByCustomer = [];
                            $scope.Parameter.Param_ContactFilter = [];
                            var total = 0;

                            _.each($scope.Parameter.Param_Contact, function (cus) {
                                var i = 1;
                                if (cus.CustomerKey != '0') {
                                    var cusfilter = _.where($scope.BusinessDocumentMain, { CustomerKey: cus.ContactKey });
                                    if (cusfilter != undefined && cusfilter.length > 0) {

                                        var customer = { ContactKey: cus.ContactKey, BusinessName: cus.BusinessName, Total: AFormatNumber(0, 2) };
                                        var amount = 0;
                                        customer.Document = [];
                                        _.each(cusfilter, function (inv) {
                                            if (i <= 30) {
                                                inv.DocumentDate = formatDate(inv.DocumentDate);
                                                inv.DueDate = formatDate(inv.DueDate);

                                                if (inv.DocumentStatus != '3') {

                                                    if (inv.DocumentType == 'CN') {
                                                        total = total - inv.Total;
                                                        amount = amount - inv.Total;
                                                    }
                                                    else {
                                                        total = total + inv.Total;
                                                        amount = amount + inv.Total;
                                                    }
                                                }
                                                inv.TotalText = AFormatNumber(inv.Total, 2);
                                                customer.Document.push(inv);
                                                i++;
                                            }
                                        });
                                        customer.Total = AFormatNumber(amount, 2);
                                        $scope.BusinessDocumentByCustomer.push(customer);
                                        $scope.Parameter.Param_ContactFilter.push({ ContactKey: cus.ContactKey, BusinessName: cus.BusinessName });
                                    }
                                }
                            });

                            if ($scope.Parameter.Param_ContactFilter != undefined && $scope.Parameter.Param_ContactFilter.length > 0)
                                $scope.Search.SelectTab = $scope.Parameter.Param_ContactFilter[0].ContactKey;

                            $scope.Parameter.Param_ContactFilter.unshift({ ContactKey: '0', BusinessName: 'แสดงทั้งหมด' });
                            var type = $scope.Parameter.Param_ContactFilter.filter(function (item) { return item.ContactKey == '0'; });
                            if (type.length > 0)
                                $scope.Search.SelectCustomer = type[0];

                            $scope.BusinessDocumentByCustomer = $scope.BusinessDocumentByCustomerMain;

                            var condition = response.data.responsecondition;
                            $scope.BusinessHeader.DateNow = formatFullDate(GetDatetimeNow());
                            $scope.BusinessHeader.StartDate = formatShortDate(condition.startdate);
                            $scope.BusinessHeader.EndDate = formatShortDate(condition.enddate);
                            $scope.BusinessHeader.Amount = AFormatNumber(total, 2);
                            $scope.BusinessHeader.AmountFix = AFormatNumber(total, 2);

                            $scope.table.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });

    };

    $scope.OnClickExportExcel = function () {
        $scope.table.binding = 1;
        $scope.Search.SelectStatus = '0';
        var data = {
            typedate: $scope.Search.Selectionfilter,
            startdate: $scope.Search.Selectionfilter == '2' ? ToJsonDate2('01-' + $scope.Search.InputToMonth)
                : $scope.Search.Selectionfilter == '4' ? ToJsonDate2('01-01-' + $scope.Search.InputToYear)
                    : $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputFromDate) : undefined,
            enddate: $scope.Search.Selectionfilter == '5' ? ToJsonDate2($scope.Search.InputToDate) : undefined
        };

        $http.post(baseURL + "Report/PostXlsxReportSalesCustomer", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.BusinessDocumentMain = [];
                            $scope.BusinessDocumentMain = response.data.responsedata;
                            showSuccessText('ดาวน์โหลดสำเร็จ');
                            window.location = baseURL + "Report/DownloadFromPath?file=" + response.data.responsedata;

                            $scope.table.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickUpdate = function (key, type) {
        if (type == 'BL')
            window.location.href = baseURL + "DocumentSell/BillingNoteAdd?BillingKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'CBL')
            window.location.href = baseURL + "DocumentSell/BillingNoteCumulativeAdd?BillingKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'QT')
            window.location.href = baseURL + "DocumentSell/QuotationAdd?QuotationKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'INV')
            window.location.href = baseURL + "DocumentSell/InvoiceAdd?InvoiceKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'CN')
            window.location.href = baseURL + "DocumentSell/CreditNoteAdd?CreditNoteKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'DN')
            window.location.href = baseURL + "DocumentSell/DebitNoteAdd?DebitNoteKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'RE')
            window.location.href = baseURL + "DocumentSell/ReceiptAdd?ReceiptKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'CRE')
            window.location.href = baseURL + "DocumentSell/ReceiptCumulativeAdd?ReceiptKey=" + key + "&ref_report=Reportsalebycustomer";
        else if (type == 'CA')
            window.location.href = baseURL + "DocumentSell/CashSaleAdd?CashSaleKey=" + key + "&ref_report=Reportsalebycustomer";
    };

    $scope.OnClickReportContent = function (dockey, docno, type, doctype) {
        $scope.document = [];
        $scope.document.DocKey = dockey;
        $scope.document.DocNo = docno;
        $scope.document.original = $scope.document.copy = true;
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $scope.document.documenttype = doctype;
        $('#modal-content-report').modal('show');
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        $scope.document.DocNo = $scope.document.DocNo;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: $scope.document.documenttype,
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: $scope.document.documenttype,
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Download',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.itemsPerPage = 20;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'DocumentNo';
    $scope.reverseSort = true;


    $scope.pageCount = function () {
        return Math.ceil($scope.BusinessDocumentByCustomer.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessDocumentByCustomer.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

});


