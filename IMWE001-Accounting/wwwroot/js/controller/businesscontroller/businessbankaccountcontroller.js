﻿WEACCTAPP.controller('businessbankaccountcontroller', function ($scope, $http, $timeout, GlobalVar) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessAccount = [];
    $scope.Parameter = [];
    $scope.table = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;        
            if ($scope.LoadFirst == undefined) {
                $http.get(baseURL + "Setting/GetParameterBank?key=" + makeid())
                    .then(function (response) {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                                $scope.Parameter.Param_Bank = [];
                                $scope.Parameter.Param_Bank = response.data.responsedata;
                            }
                            else if (response.data.responsecode == '400') {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    });
                $scope.LoadFirst = 0;
            }

            $http.get(baseURL + "Setting/GetBusinessBankAccount?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                            $scope.BusinessAccount = response.data.responsedata;
                            $scope.retpage = [];
                            $scope.range();
                            $scope.table.binding = 0;      
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickAdd = function () {
        $scope.BusinessAccountAdd = [];
        $scope.BusinessAccountAdd.SelectBank = undefined;
        //var type = $scope.Parameter.Param_Bank.filter(function (item) { return item.ID == $scope.Parameter.Param_Bank[0].ID; });
        //if (type.length > 0)
        //    $scope.BusinessAccountAdd.SelectBank = type[0];

        $scope.BusinessAccountAdd.DepositType = 'S';

        $('#modalbank-add').modal('show');
    };

    $scope.OnClickSave = function () {
        try {
            if ($scope.BusinessAccountAdd.SelectBank == undefined)
                throw "กรุณากรอกเลือกธนาคาร";
            else if ($scope.BusinessAccountAdd.AccountNo == undefined || $scope.BusinessAccountAdd.AccountNo == "")
                throw "กรุณากรอกเลขที่บัญชี";
            else if ($scope.BusinessAccountAdd.AccountNo != undefined && $scope.BusinessAccountAdd.AccountNo != "" && $scope.BusinessAccountAdd.AccountNo.length < 10)
                throw "กรุณากรอกเลขที่บัญชีอย่างน้อย 10 หลัก";
            else if ($scope.BusinessAccountAdd.AccountName == undefined || $scope.BusinessAccountAdd.AccountName == "")
                throw "กรุณากรอกชื่อบัญชี";
            else {

                var data = {
                    BankCode: $scope.BusinessAccountAdd.SelectBank.ID,
                    AccountNo: $scope.BusinessAccountAdd.AccountNo,
                    BranchName: $scope.BusinessAccountAdd.BranchName,
                    AccountName: $scope.BusinessAccountAdd.AccountName,
                    DepositType: $scope.BusinessAccountAdd.DepositType,
                    BankName: $scope.BusinessAccountAdd.SelectBank.Name,
                    DepositTypeName: $scope.BusinessAccountAdd.DepositType == 'S' ? 'ออมทรัพย์' : $scope.BusinessAccountAdd.DepositType == 'C' ? 'กระแสรายวัน' : 'ฝากประจำ'
                };

                $http.post(baseURL + "Setting/PostBusinessBankAccount", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    $scope.init();
                                    $('#modalbank-add').modal('hide');
                                    showSuccessToast();
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnclickDelete = function (uid) {
        var filter = _.where($scope.BusinessAccount, { UID: uid })[0];
        if (filter != undefined) {
            $scope.BusinessAccountAdd = [];
            $scope.BusinessAccountAdd = angular.copy(filter);
            $('#modalbank-del').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {
        $http.get(baseURL + "Setting/DeleteBusinessBankAccount?uid=" + $scope.BusinessAccountAdd.UID + "&key=" + makeid())
            .then(function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $scope.init();
                            $scope.BusinessAccountAdd = [];
                            $('#modalbank-del').modal('hide');
                        }
                        else {
                            $('#modalbank-del').modal('hide');
                            $('#modalbank-alert').modal('show');
                            //showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.itemsPerPage = 10;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.pageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.BusinessAccount.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessAccount.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
});


