﻿WEACCTAPP.controller('businessdocumentrunningcontroller', function ($scope, $http, $timeout, GlobalVar, Upload) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessRuning = [];
    $scope.table = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $http.get(baseURL + "Setting/GetDocumentRuning?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {

                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!
                            var yyyy = today.getFullYear();
                            $scope.FixedYM = yyyy + (mm < 10 ? '0' + mm : mm) + '0001';
                            $scope.FixedYY = yyyy + '000001';

                            /**Document Type Runing */
                            $scope.BusinessRuning = response.data.responsedata;
                            $scope.BusinessParameter = [];

                            $scope.BusinessParameter.RunningType = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'RunningType' })[0];
                            $scope.BusinessParameter.RunningType.Disabled = 1;
                            $scope.OnChangeOptionDatetime();

                            /**Document Header*/
                            $scope.BusinessParameter.DocumentQuotation = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentQuotation' })[0];
                            $scope.BusinessParameter.DocumentPurchase = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentPurchase' })[0];
                            $scope.BusinessParameter.DocumentBilling = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentBilling' })[0];
                            $scope.BusinessParameter.DocumentDelivery = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentDelivery' })[0];
                            $scope.BusinessParameter.DocumentReceipt = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentReceipt' })[0];
                            $scope.BusinessParameter.DocumentDebit = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentDebit' })[0];
                            $scope.BusinessParameter.DocumentCredit = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentCredit' })[0];
                            $scope.BusinessParameter.DocumentCash = _.where($scope.BusinessRuning.BizParam, { ParameterField: 'DocumentCash' })[0];

                            $scope.OnClickChangeDocumentType();

                            $scope.table.binding = 0;
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickSave = function () {
        try {
            $scope.UpdateDocumentParameter = [];
            if ($scope.BusinessParameter.RunningType.Disabled == 0)
                $scope.UpdateDocumentParameter.push({ ParameterField: 'RunningType', ParameterValue: $scope.BusinessParameter.RunningType.ParameterValue, ParameterDescription: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 'ปี' : 'ปี/เดือน') });

            var quotationdesc;
            if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '1')
                quotationdesc = 'สัญญาโฆษณา | Advertising Contract';
            else if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '2')
                quotationdesc = 'ใบส่งของ | Delivery Note';
            else if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '3')
                quotationdesc = 'ใบสั่งงาน | Job order';
            else if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '4')
                quotationdesc = 'ใบเสนอราคา | Price Quotation';
            else if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '5')
                quotationdesc = 'ใบเสนอราคา | Quotation';
            else if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '6')
                quotationdesc = 'ใบสั่งขาย | Sales / Job order';
            else if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '7')
                quotationdesc = 'สัญญาบริการ | Sevice Contract';
            else if ($scope.BusinessParameter.DocumentQuotation.ParameterValue == '8')
                quotationdesc = 'ใบส่งมอบบริการ | Summary Timesheet';

            var billingnotedesc;
            if ($scope.BusinessParameter.DocumentBilling.ParameterValue == '1')
                billingnotedesc = 'ใบวางบิล | Billing Note';
            else if ($scope.BusinessParameter.DocumentBilling.ParameterValue == '2')
                billingnotedesc = 'ใบส่งสินค้าชั่วคราว | Delivery Bill';

            var deliverydesc;
            if ($scope.BusinessParameter.DocumentDelivery.ParameterValue == '1')
                deliverydesc = 'ใบส่งสินค้า | Delivery Note';
            else if ($scope.BusinessParameter.DocumentDelivery.ParameterValue == '2')
                deliverydesc = 'ใบส่งสินค้า / ใบแจ้งหนี้ | Delivery Note / Invoice';
            else if ($scope.BusinessParameter.DocumentDelivery.ParameterValue == '3')
                deliverydesc = 'ใบแจ้งหนี้ | Invoice';

            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentQuotation', ParameterValue: $scope.BusinessParameter.DocumentQuotation.ParameterValue, ParameterDescription: quotationdesc });
            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentBilling', ParameterValue: $scope.BusinessParameter.DocumentBilling.ParameterValue, ParameterDescription: billingnotedesc });
            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentDelivery', ParameterValue: $scope.BusinessParameter.DocumentDelivery.ParameterValue, ParameterDescription: deliverydesc });
            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentReceipt', ParameterValue: $scope.BusinessParameter.DocumentReceipt.ParameterValue, ParameterDescription: 'ใบเสร็จรับเงิน | Receipt' });
            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentCredit', ParameterValue: $scope.BusinessParameter.DocumentCredit.ParameterValue, ParameterDescription: 'ใบลดหนี้ | Credit Note' });
            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentDebit', ParameterValue: $scope.BusinessParameter.DocumentDebit.ParameterValue, ParameterDescription: 'ใบเพิ่มหนี้ | Debit Note' });
            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentCash', ParameterValue: $scope.BusinessParameter.DocumentCash.ParameterValue, ParameterDescription: 'ใบเสร็จรับเงิน (เงินสด) | Receipt (Cash)' });
            $scope.UpdateDocumentParameter.push({ ParameterField: 'DocumentPurchase', ParameterValue: $scope.BusinessParameter.DocumentPurchase.ParameterValue, ParameterDescription: 'ใบสั่งซื้อ | Purchase Order' });

            $scope.UpdateDocumentRuning = [];
            if ($scope.BusinessDocumentRuning.QuotationDisabled == 0) {
                if ($scope.BusinessDocumentRuning.QuotationSuffix == "" || $scope.BusinessDocumentRuning.QuotationSuffix == undefined ||
                    $scope.BusinessDocumentRuning.QuotationNextNumber == "" || $scope.BusinessDocumentRuning.QuotationNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบเสนอราคาให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'QT', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.QuotationSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.QuotationNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.BillingDisabled == 0) {
                if ($scope.BusinessDocumentRuning.BillingSuffix == "" || $scope.BusinessDocumentRuning.BillingSuffix == undefined ||
                    $scope.BusinessDocumentRuning.BillingNextNumber == "" || $scope.BusinessDocumentRuning.BillingNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบวางบิลให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'BL', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.BillingSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.BillingNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.DeliveryNoteDisabled == 0) {
                if ($scope.BusinessDocumentRuning.DeliveryNoteSuffix == "" || $scope.BusinessDocumentRuning.DeliveryNoteSuffix == undefined ||
                    $scope.BusinessDocumentRuning.DeliveryNoteNextNumber == "" || $scope.BusinessDocumentRuning.DeliveryNoteNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบแจ้งหนี้ให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'INV', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.DeliveryNoteSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.DeliveryNoteNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.CashDisabled == 0) {
                if ($scope.BusinessDocumentRuning.CashSuffix == "" || $scope.BusinessDocumentRuning.CashSuffix == undefined ||
                    $scope.BusinessDocumentRuning.CashNextNumber == "" || $scope.BusinessDocumentRuning.CashNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารขายเงินสดให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'CA', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.CashSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.CashNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.ReceiptDisabled == 0) {
                if ($scope.BusinessDocumentRuning.ReceiptSuffix == "" || $scope.BusinessDocumentRuning.ReceiptSuffix == undefined ||
                    $scope.BusinessDocumentRuning.ReceiptNextNumber == "" || $scope.BusinessDocumentRuning.ReceiptNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบเสร็จรับเงินให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'RE', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.ReceiptSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.ReceiptNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.CreditDisabled == 0) {
                if ($scope.BusinessDocumentRuning.CreditSuffix == "" || $scope.BusinessDocumentRuning.CreditSuffix == undefined ||
                    $scope.BusinessDocumentRuning.CreditNextNumber == "" || $scope.BusinessDocumentRuning.CreditNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบลดหนี้ให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'CN', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.CreditSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.CreditNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.DebitDisabled == 0) {
                if ($scope.BusinessDocumentRuning.DebitSuffix == "" || $scope.BusinessDocumentRuning.DebitSuffix == undefined ||
                    $scope.BusinessDocumentRuning.DebitNextNumber == "" || $scope.BusinessDocumentRuning.DebitNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบเพิ่มหนี้ให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'CN', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.DebitSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.DebitNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.PurchaseDisabled == 0) {
                if ($scope.BusinessDocumentRuning.PurchaseSuffix == "" || $scope.BusinessDocumentRuning.PurchaseSuffix == undefined ||
                    $scope.BusinessDocumentRuning.PurchaseNextNumber == "" || $scope.BusinessDocumentRuning.PurchaseNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบสั่งซื้อให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'PO', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.PurchaseSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.PurchaseNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }

            if ($scope.BusinessDocumentRuning.ReceivingDisabled == 0) {
                if ($scope.BusinessDocumentRuning.ReceivingSuffix == "" || $scope.BusinessDocumentRuning.ReceivingSuffix == undefined ||
                    $scope.BusinessDocumentRuning.ReceivingNextNumber == "" || $scope.BusinessDocumentRuning.ReceivingNextNumber == undefined)
                    throw "กรุณากรอกตั้งค่าเริ่มต้นเอกสารใบรับสินค้าให้ครบถ้วน";

                $scope.UpdateDocumentRuning.push({
                    DocumentType: 'RI', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: $scope.BusinessDocumentRuning.ReceivingSuffix,
                    NextNumber: $scope.BusinessDocumentRuning.ReceivingNextNumber, Len: ($scope.BusinessParameter.RunningType.ParameterValue == 'YY' ? 6 : 4)
                });
            }


            var data = {
                BizParam: $scope.UpdateDocumentParameter,
                BizRun: $scope.UpdateDocumentRuning
            };

            $http.post(baseURL + "Setting/PostDocumentParameter", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.init();
                                showSuccessToast();
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickChangeDocumentType = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        /*Document Runing*/
        var ymsuffix = yyyy + '-' + (mm < 10 ? '0' + mm : mm);
        var yysuffix = yyyy;
        var selsuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
        $scope.BusinessDocumentRuning = [];
        var qt = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'QT', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        $scope.BusinessDocumentRuning.QuotationDisabled = 1;
        if (qt != undefined) {
            $scope.BusinessDocumentRuning.QuotationSuffix = qt.Suffix;
            $scope.BusinessDocumentRuning.QuotationNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? qt.NextNumber.toString().padStart(4, '0') : qt.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.QuotationSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.QuotationNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }

        var bl = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'BL', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (bl != undefined) {
            $scope.BusinessDocumentRuning.BillingSuffix = bl.Suffix;
            $scope.BusinessDocumentRuning.BillingNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? bl.NextNumber.toString().padStart(4, '0') : bl.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.BillingSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.BillingNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }
        $scope.BusinessDocumentRuning.BillingDisabled = 1;

        var inv = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'INV', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (inv != undefined) {
            $scope.BusinessDocumentRuning.DeliveryNoteSuffix = inv.Suffix;
            $scope.BusinessDocumentRuning.DeliveryNoteNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? inv.NextNumber.toString().padStart(4, '0') : inv.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.DeliveryNoteSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.DeliveryNoteNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }
        $scope.BusinessDocumentRuning.DeliveryNoteDisabled = 1;

        var ca = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'CA', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (ca != undefined) {
            $scope.BusinessDocumentRuning.CashSuffix = ca.Suffix;
            $scope.BusinessDocumentRuning.CashNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ca.NextNumber.toString().padStart(4, '0') : ca.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.CashSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.CashNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }
        $scope.BusinessDocumentRuning.CashDisabled = 1;

        var re = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'RE', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (re != undefined) {
            $scope.BusinessDocumentRuning.ReceiptSuffix = re.Suffix;
            $scope.BusinessDocumentRuning.ReceiptNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? re.NextNumber.toString().padStart(4, '0') : re.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.ReceiptSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.ReceiptNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }
        $scope.BusinessDocumentRuning.ReceiptDisabled = 1;

        var cn = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'CN', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (cn != undefined) {
            $scope.BusinessDocumentRuning.CreditSuffix = cn.Suffix;
            $scope.BusinessDocumentRuning.CreditNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? cn.NextNumber.toString().padStart(4, '0') : cn.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.CreditSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.CreditNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }
        $scope.BusinessDocumentRuning.CreditDisabled = 1;

        var dn = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'DN', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (dn != undefined) {
            $scope.BusinessDocumentRuning.DebitSuffix = dn.Suffix;
            $scope.BusinessDocumentRuning.DebitNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? dn.NextNumber.toString().padStart(4, '0') : dn.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.DebitSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.DebitNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }

        $scope.BusinessDocumentRuning.DebitDisabled = 1;

        var ri = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'RI', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (ri != undefined) {
            $scope.BusinessDocumentRuning.PurchaseSuffix = ri.Suffix;
            $scope.BusinessDocumentRuning.PurchaseNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ri.NextNumber.toString().padStart(4, '0') : ri.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.PurchaseSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.PurchaseNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }
        $scope.BusinessDocumentRuning.PurchaseDisabled = 1;

        var po = _.where($scope.BusinessRuning.BizRun, { DocumentType: 'PO', Prefix: $scope.BusinessParameter.RunningType.ParameterValue, Suffix: selsuffix.toString() })[0];
        if (po != undefined) {
            $scope.BusinessDocumentRuning.ReceivingSuffix = po.Suffix;
            $scope.BusinessDocumentRuning.ReceivingNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? po.NextNumber.toString().padStart(4, '0') : po.NextNumber.toString().padStart(6, '0');
        }
        else {
            $scope.BusinessDocumentRuning.ReceivingSuffix = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? ymsuffix : yysuffix;
            $scope.BusinessDocumentRuning.ReceivingNextNumber = $scope.BusinessParameter.RunningType.ParameterValue == "YM" ? "1".padStart(4, '0') : "1".padStart(6, '0');
        }
        $scope.BusinessDocumentRuning.ReceivingDisabled = 1;
    };

    $scope.OnClickChangeDisabled = function (type) {
        $scope.OnChangeOptionDatetime();
        if (type == 1) {
            $scope.BusinessParameter.RunningType.Disabled = $scope.BusinessParameter.RunningType.Disabled == 1 ? 0 : 1;
            if ($scope.BusinessParameter.RunningType.Disabled == 0)
                $scope.BusinessParameter.RunningType.DumpValue = $scope.BusinessParameter.RunningType.ParameterValue;
            else if ($scope.BusinessParameter.RunningType.Disabled == 1) {
                $scope.BusinessParameter.RunningType.ParameterValue = $scope.BusinessParameter.RunningType.DumpValue;
                $scope.OnChangeDocumentTypeRadio();
            }

        }
        else if (type == 2) {
            $scope.BusinessDocumentRuning.QuotationDisabled = $scope.BusinessDocumentRuning.QuotationDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.QuotationDisabled == 0) {
                $scope.BusinessDocumentRuning.QuotationSuffixDump = $scope.BusinessDocumentRuning.QuotationSuffix;
                $scope.BusinessDocumentRuning.QuotationNextNumberDump = $scope.BusinessDocumentRuning.QuotationNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.QuotationDisabled == 1) {
                $scope.BusinessDocumentRuning.QuotationSuffix = $scope.BusinessDocumentRuning.QuotationSuffixDump;
                $scope.BusinessDocumentRuning.QuotationNextNumber = $scope.BusinessDocumentRuning.QuotationNextNumberDump;
            }
        }
        else if (type == 3) {
            $scope.BusinessDocumentRuning.BillingDisabled = $scope.BusinessDocumentRuning.BillingDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.BillingDisabled == 0) {
                $scope.BusinessDocumentRuning.BillingSuffixDump = $scope.BusinessDocumentRuning.BillingSuffix;
                $scope.BusinessDocumentRuning.BillingNextNumberDump = $scope.BusinessDocumentRuning.BillingNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.BillingDisabled == 1) {
                $scope.BusinessDocumentRuning.BillingSuffix = $scope.BusinessDocumentRuning.BillingSuffixDump;
                $scope.BusinessDocumentRuning.BillingNextNumber = $scope.BusinessDocumentRuning.BillingNextNumberDump;
            }
        }
        else if (type == 4) {
            $scope.BusinessDocumentRuning.DeliveryNoteDisabled = $scope.BusinessDocumentRuning.DeliveryNoteDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.DeliveryNoteDisabled == 0) {
                $scope.BusinessDocumentRuning.DeliveryNoteSuffixDump = $scope.BusinessDocumentRuning.DeliveryNoteSuffix;
                $scope.BusinessDocumentRuning.DeliveryNoteNextNumberDump = $scope.BusinessDocumentRuning.DeliveryNoteNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.DeliveryNoteDisabled == 1) {
                $scope.BusinessDocumentRuning.DeliveryNoteSuffix = $scope.BusinessDocumentRuning.DeliveryNoteSuffixDump;
                $scope.BusinessDocumentRuning.DeliveryNoteNextNumber = $scope.BusinessDocumentRuning.DeliveryNoteNextNumberDump;
            }

        }
        else if (type == 5) {
            $scope.BusinessDocumentRuning.CashDisabled = $scope.BusinessDocumentRuning.CashDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.CashDisabled == 0) {
                $scope.BusinessDocumentRuning.CashSuffixDump = $scope.BusinessDocumentRuning.CashSuffix;
                $scope.BusinessDocumentRuning.CashNextNumberDump = $scope.BusinessDocumentRuning.CashNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.CashDisabled == 1) {
                $scope.BusinessDocumentRuning.CashSuffix = $scope.BusinessDocumentRuning.CashSuffixDump;
                $scope.BusinessDocumentRuning.CashNextNumber = $scope.BusinessDocumentRuning.CashNextNumberDump;
            }
        }

        else if (type == 6) {
            $scope.BusinessDocumentRuning.ReceiptDisabled = $scope.BusinessDocumentRuning.ReceiptDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.ReceiptDisabled == 0) {
                $scope.BusinessDocumentRuning.ReceiptSuffixDump = $scope.BusinessDocumentRuning.ReceiptSuffix;
                $scope.BusinessDocumentRuning.ReceiptNextNumberDump = $scope.BusinessDocumentRuning.ReceiptNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.ReceiptDisabled == 1) {
                $scope.BusinessDocumentRuning.ReceiptSuffix = $scope.BusinessDocumentRuning.ReceiptSuffixDump;
                $scope.BusinessDocumentRuning.ReceiptNextNumber = $scope.BusinessDocumentRuning.ReceiptNextNumberDump;
            }
        }

        else if (type == 7) {
            $scope.BusinessDocumentRuning.CreditDisabled = $scope.BusinessDocumentRuning.CreditDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.CreditDisabled == 0) {
                $scope.BusinessDocumentRuning.CreditSuffixDump = $scope.BusinessDocumentRuning.CreditSuffix;
                $scope.BusinessDocumentRuning.CreditNextNumberDump = $scope.BusinessDocumentRuning.CreditNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.CreditDisabled == 1) {
                $scope.BusinessDocumentRuning.CreditSuffix = $scope.BusinessDocumentRuning.CreditSuffixDump;
                $scope.BusinessDocumentRuning.CreditNextNumber = $scope.BusinessDocumentRuning.CreditNextNumberDump;
            }
        }

        else if (type == 8) {
            $scope.BusinessDocumentRuning.DebitDisabled = $scope.BusinessDocumentRuning.DebitDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.DebitDisabled == 0) {
                $scope.BusinessDocumentRuning.DebitSuffixDump = $scope.BusinessDocumentRuning.DebitSuffix;
                $scope.BusinessDocumentRuning.DebitNextNumberDump = $scope.BusinessDocumentRuning.DebitNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.DebitDisabled == 1) {
                $scope.BusinessDocumentRuning.DebitSuffix = $scope.BusinessDocumentRuning.DebitSuffixDump;
                $scope.BusinessDocumentRuning.DebitNextNumber = $scope.BusinessDocumentRuning.DebitNextNumberDump;
            }
        }

        else if (type == 9) {
            $scope.BusinessDocumentRuning.PurchaseDisabled = $scope.BusinessDocumentRuning.PurchaseDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.PurchaseDisabled == 0) {
                $scope.BusinessDocumentRuning.PurchaseSuffixDump = $scope.BusinessDocumentRuning.PurchaseSuffix;
                $scope.BusinessDocumentRuning.PurchaseNextNumberDump = $scope.BusinessDocumentRuning.PurchaseNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.PurchaseDisabled == 1) {
                $scope.BusinessDocumentRuning.PurchaseSuffix = $scope.BusinessDocumentRuning.PurchaseSuffixDump;
                $scope.BusinessDocumentRuning.PurchaseNextNumber = $scope.BusinessDocumentRuning.PurchaseNextNumberDump;
            }
        }

        else if (type == 10) {
            $scope.BusinessDocumentRuning.ReceivingDisabled = $scope.BusinessDocumentRuning.ReceivingDisabled == 1 ? 0 : 1;
            if ($scope.BusinessDocumentRuning.ReceivingDisabled == 0) {
                $scope.BusinessDocumentRuning.ReceivingSuffixDump = $scope.BusinessDocumentRuning.ReceivingSuffix;
                $scope.BusinessDocumentRuning.ReceivingNextNumberDump = $scope.BusinessDocumentRuning.ReceivingNextNumber;
            }
            else if ($scope.BusinessDocumentRuning.ReceivingDisabled == 1) {
                $scope.BusinessDocumentRuning.ReceivingSuffix = $scope.BusinessDocumentRuning.ReceivingSuffixDump;
                $scope.BusinessDocumentRuning.ReceivingNextNumber = $scope.BusinessDocumentRuning.ReceivingNextNumberDump;
            }
        }
    };

    $scope.OnChangeOptionDatetime = function () {
        $('#datepicker-popup').datepicker('destroy'); $('#datepicker-popup-1').datepicker('destroy'); $('#datepicker-popup-2').datepicker('destroy'); $('#datepicker-popup-3').datepicker('destroy'); $('#datepicker-popup-4').datepicker('destroy');
        $('#datepicker-popup-5').datepicker('destroy'); $('#datepicker-popup-6').datepicker('destroy'); $('#datepicker-popup-7').datepicker('destroy'); $('#datepicker-popup-8').datepicker('destroy');
        if ($scope.BusinessParameter.RunningType.ParameterValue == "YM") {
            $('#datepicker-popup').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-1').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-2').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-3').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-4').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-5').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-6').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-7').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
            $('#datepicker-popup-8').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months",
                language: 'th-th'
            });
        }
        else if ($scope.BusinessParameter.RunningType.ParameterValue == "YY") {
            $('#datepicker-popup').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-1').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-2').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-3').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-4').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-5').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-6').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-7').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('#datepicker-popup-8').datepicker({
                enableOnReadonly: true,
                todayHighlight: true,
                autoclose: true,
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
        }
    };

    $scope.OnChangeDocumentTypeRadio = function () {
        $scope.OnChangeOptionDatetime();
        $scope.OnClickChangeDocumentType();
    };
});

