﻿WEACCTAPP.controller('manageuseraccountcontroller', function ($scope, $http, $timeout, $q, GlobalVar) {

    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.Parameter = [];
    $scope.Parameter.Positions = [];
    $scope.table = [];
    $scope.userprofileList = [];
    $scope.userprofile = {};
    //  $scope.userprofile.Positions = "aa";
    // $scope.bell = {};
    // $scope.bell.aaa = "ddd";

    function inti() {
        try {
            $scope.table.binding = 1;

            var paramPositionRequest = $http.get(baseURL + "Setting/GetParameterPosition?key=" + makeid(), { cache: false });
            var userListRequest = $http.get(baseURL + "Setting/GetUserProfiles?key=" + makeid(), { cache: false });
            var userRequest = $http.get(baseURL + "Setting/GetUserProfile?key=" + makeid(), { cache: false });

            $q.all([paramPositionRequest, userListRequest, userRequest]).then(function (data) {
                $scope.Parameter.Positions = data[0].data.responsedata;
                tmpUsers = data[1].data.responsedata;
                for (var i = 0; i < tmpUsers.length; i++) {
                    if (tmpUsers[i].IsActive)
                        tmpUsers[i].StatusName = "Active";
                    else
                        tmpUsers[i].StatusName = "Not Active";

                    for (var j = 0; j < $scope.Parameter.Positions.length; j++) {
                        if (tmpUsers[i].Position == $scope.Parameter.Positions[j].ID) {
                            tmpUsers[i].PositionName = $scope.Parameter.Positions[j].Name;
                            break;
                        }
                    }
                }
                $scope.userprofileList = tmpUsers;
                $scope.retpage = [];
                $scope.range();
                $scope.table.binding = 0;

                $scope.userprofile = data[2].data.responsedata;

                $("#user_thumnail").addClass('dropify');
                if ($scope.userprofile.ThumbnailSignature != undefined && $scope.userprofile.ThumbnailSignature != "")
                    $("#user_thumnail").attr("data-default-file", baseURL + 'uploads/user/' + $scope.userprofile.ThumbnailSignature);

                $('.dropify').dropify();

                var drEvent = $('.dropify').dropify();
                drEvent.on('dropify.afterClear', function (event, element) {
                    $scope.DeleteImages = 1;
                });

            });
        }
        catch (err) {
            showErrorToast(err);
        }
    }

    $scope.init = function () {
        inti();
    };

    $scope.UploadFiles = function (files) {
        $scope.Upload = 1;
        $scope.SelectedFiles = files;
    };

    $scope.OpenAddUserDialog = function (type, uid) {
        $scope.readonlyUserDialog = false;

        
        if (type == "new") {
            ClearUserProfile(); $('#AddUserDialog').modal('show');
        }
        else if (type == "edit") {
            ClearUserProfile();
            SetData(uid);

            $http.get(baseURL + "Setting/GetUserProfile?key=" + makeid(), { cache: false }).then(
                function (response) {
                    if (response.data.responsedata.Position == "001")
                        $scope.readonlyUserDialog = true;
                }
            )
            $('#AddUserDialog').modal('show');
        } //DeleteUserDialog
        else if (type == "delete")
        {
            ClearUserProfile();
            SetData(uid); $scope.userprofile.IsDelete = 0;
            $('#DeleteUserDialog').modal('show');
        }
    }

    $scope.SaveUserProfile = function () {
        try {

           

            var data = GetData();

            if (data.Name == undefined || data.Name == null || data.Name == "")
                showWariningToast("กรุณาระบุชื่อ");
            else if (data.Surname == undefined || data.Surname == null || data.Surname == "")
                showWariningToast("กรุณาระบุนามสกุล");
            else if (data.Telephone == undefined || data.Telephone == null || data.Telephone == "")
                showWariningToast("กรุณาระบุเบอร์ติดต่อ");
            else if (data.Position == undefined || data.Position == null || data.Position == "")
                showWariningToast("กรุณาระบุตำแหน่ง");
            else if (data.Email == undefined || data.Email == null || data.Email == "")
                showWariningToast("กรุณาระบุอีเมล");
            else if (!ValidateEmail(data.Email))
                showWariningToast("กรุณาแก้ไขอีเมล เนื่องจากรูปแบบอีเมลไม่ถูกต้อง");
            else if (data.newPwd == undefined || data.newPwd == null || data.newPwd == "")
                showWariningToast("กรุณาระบุรหัสผ่าน");
            else if (data.newPwd.length < 6)
                showWariningToast("กรุณาระบุรหัสผ่านอย่างน้อย 6 หลัก");
            else
            {
                if (data.UID == "")
                    data.UID = " 00000000-0000-0000-0000-000000000000";

                $http.post(baseURL + "Setting/PostUserProfile", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    ClearUserProfile(); $scope.init();
                                    $('#AddUserDialog').modal('hide');
                                    $('#DeleteUserDialog').modal('hide');
                                    showSuccessToast();
                                }
                                else
                                    showErrorToast(response.data.errormessage);
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    }

    $scope.DeleteUserProfile = function () {
        try {



            var data = GetData();
            data.IsDelete = 0;

            $http.post(baseURL + "Setting/PostUserProfile", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                ClearUserProfile(); $scope.init();
                                $('#AddUserDialog').modal('hide');
                                $('#DeleteUserDialog').modal('hide');
                                showSuccessText('ลบข้อมูลสำเร็จ');
                            }
                            else
                                showErrorToast(response.data.errormessage);
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });

        }
        catch (err) {
            showWariningToast(err);
        }
    }

    $scope.SaveMyUserProfile = function () {
        try {
            var data = GetData();

            if ($scope.replacePWD.OldPWD != undefined && $scope.replacePWD.OldPWD != "") {
                validatePWD();
                data.newPwd = $scope.replacePWD.NewPWD;
                data.oldPwd = $scope.replacePWD.OldPWD;
            }

            if ($scope.SelectedFiles != undefined && $scope.Upload == 1) {
                var input = document.getElementById("user_thumnail");
                var files = input.files;
                var formData = new FormData();

                for (var i = 0; i != files.length; i++) {
                    formData.append("files", files[i]);
                }

                $.ajax(
                    {
                        url: baseURL + "Setting/UploadUserProfile",
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: "POST",
                        success: function (d) {
                            data.SignaturePath = d.fullname;
                            data.ThumbnailSignature = d.filename;
                            saveUserProfile(data);
                           // $scope.init();
                          //  showSuccessToast();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            showErrorToast(textStatus);
                        }
                    }
                );
            }
            else
                saveUserProfile(data);



            //$http.post(baseURL + "Setting/PostUserProfile", data, config).then(
            //    function (response) {
            //        try {
            //            if (response != undefined && response != "") {
            //                if (response.data.responsecode == 200) {
            //                    ClearUserProfile(); $scope.init();
            //                    $('#AddUserDialog').modal('hide');
            //                    $('#DeleteUserDialog').modal('hide');
            //                    showSuccessToast();
            //                }
            //                else
            //                    showErrorToast(response.data.errormessage);
            //            }
            //        }
            //        catch (err) {
            //            showErrorToast(err);
            //        }
            //    });
        }
        catch(err)
        {
            showWariningToast(err);
        }
    }

    function saveUserProfile(data)
    {
         $http.post(baseURL + "Setting/PostUserProfile", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                ClearUserProfile(); $scope.init();
                                $('#AddUserDialog').modal('hide');
                                $('#DeleteUserDialog').modal('hide');
                                showSuccessToast();
                            }
                            else
                                showErrorToast(response.data.errormessage);
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
    }

    $scope.replacePWD = {};
    function validatePWD()
    {
        if ($scope.replacePWD.NewPWD == undefined || $scope.replacePWD.NewPWD == "")
            throw "กรุณาระบุ new password";
        else if ($scope.replacePWD.NewConfirmPWD == undefined || $scope.replacePWD.NewConfirmPWD == "")
            throw "กรุณาระบุ confirm password";
        if ($scope.replacePWD.NewPWD != $scope.replacePWD.NewConfirmPWD)
            throw "passsword ไม่ตรงกัน";
    }

    function SetData(uid)
    {
        var items = $scope.userprofileList;

        for (var i = 0; i < items.length; i++)
        {
            if (items[i].UID == uid)
            {
                data = items[i];

                $scope.userprofile.UID = data.UID;
                $scope.userprofile.Email = data.Email;
                $scope.userprofile.Pwd = data.Pwd;
                $scope.userprofile.Name = data.Name;
                $scope.userprofile.Surname = data.Surname;
                $scope.userprofile.Position = data.Position;
                $scope.userprofile.Telephone = data.Telephone;
                $scope.userprofile.ThumbnailSignature = data.ThumbnailSignature;
                $scope.userprofile.SignaturePath = data.SignaturePath;
                $scope.userprofile.IsActive = data.IsActive;
                break;
            }
        }
    }

    function GetData()
    {
        var data = {
            UID: $scope.userprofile.UID,
            Email: $scope.userprofile.Email,
            Pwd: $scope.userprofile.Pwd,
            Name: $scope.userprofile.Name,
            Surname: $scope.userprofile.Surname,
            Position: $scope.userprofile.Position,
            Telephone: $scope.userprofile.Telephone,
            ThumbnailSignature: $scope.userprofile.ThumbnailSignature,
            SignaturePath: $scope.userprofile.SignaturePath,
            IsActive: $scope.userprofile.IsActive,
            IsDelete: $scope.userprofile.IsDelete,
            newPwd: $scope.userprofile.newPwd,
        };

        return data;
    }

    function ClearUserProfile()
    {
        $scope.userprofile = {};
        $scope.userprofile.UID = "";
        $scope.userprofile.Email = "";
        $scope.userprofile.Pwd = "";
        $scope.userprofile.Name = "";
        $scope.userprofile.Surname = "";
        $scope.userprofile.Position = $scope.Parameter.Positions[0].ID;
        $scope.userprofile.Telephone = "";
        $scope.userprofile.ThumbnailSignature = "";
        $scope.userprofile.SignaturePath = "";
        $scope.userprofile.IsActive = true;
        $scope.userprofile.IsDelete = 1;

        $scope.replacePWD = {};
    }


    $scope.itemsPerPage = 10;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'ProductKey';
    $scope.reverseSort = false;

    $scope.pageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.userprofileList.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.userprofileList.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };



});