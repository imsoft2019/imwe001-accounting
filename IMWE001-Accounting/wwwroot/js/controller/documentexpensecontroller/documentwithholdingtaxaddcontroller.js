﻿WEACCTAPP.controller('documentwithholdingtaxaddcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, contactService, productService, $q, Upload) {

    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'WT';

    $scope.PurchaseOrderAdd = [];
    $scope.DocumentAttach = [];
    $scope.Parameter = [];
    $scope.UserProfile = [];
    $scope.Parameter.Param_Contact = [];
    $scope.Parameter.Param_Product = [];
    $scope.Parameter.Param_Employee = [];
    $scope.Parameter.Param_Revenue = [];
    $scope.Parameter.BizBank = [];
    $scope.table = [];

    var gCustomer = paramService.getSupplier();
    var gProduct = paramService.getProduct();
    var gEmployee = paramService.getEmployee();
    var gParamQuotation = paramService.getParameterQuotation();
    var gProfile = paramService.getProfile();
    var gRemark = paramService.getDocumentRemark(headerdoc);
    var gBizBank = paramService.getBankBusiness();
    var gCatagory = paramService.getExpenseCatagory();

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        $scope.table.binding = 1;

        var prefix = { asofdate: GetDatetimeNow(), documenttype: headerdoc };

        var qq = $q.all([paramService.getDocumentNo(prefix), gCustomer, gProduct, gEmployee, gParamQuotation, gProfile, gRemark, gBizBank, gCatagory]).then(function (data) {
            try {
                /****** GetDocument NO ******/
                if (data[0] != undefined && data[0] != "") {
                    if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.PurchaseOrderAdd.PurchaseOrderNo = data[0].data.responsedata; }
                    else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                }

                /****** Get Customer ******/
                if (data[1] != undefined && data[1] != "") {
                    if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                        $scope.SearchContact = [];
                        $scope.Parameter.Param_Contact = [];
                        $scope.Parameter.Param_ContactMain = [];
                        $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                    }
                    else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                }

                /****** Get Product ******/
                if (data[2] != undefined && data[2] != "") {
                    if (data[2].data.responsecode == '200' && data[2].data.responsedata != undefined && data[2].data.responsedata != "") {
                        $scope.SearchProduct = [];
                        $scope.Parameter.Param_Product = [];
                        $scope.Parameter.Param_ProductMain = [];

                        $scope.Parameter.Param_ProductMain = data[2].data.responsedata;

                        _.each($scope.Parameter.Param_ProductMain, function (item) {
                            item.SellPrice = AFormatNumber(item.SellPrice, 2);
                            item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                            item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                            item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                            item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                        });

                        $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;
                    }
                    else if (data[2].data.responsecode == '400') { showErrorToast(data[2].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[3] != undefined && data[3] != "") {
                    if (data[3].data.responsecode == '200' && data[3].data.responsedata != undefined && data[3].data.responsedata != "") {
                        $scope.Parameter.Param_Employee = [];
                        $scope.Parameter.Param_Employee = data[3].data.responsedata;
                    }
                    else if (data[3].data.responsecode == '400') { showErrorToast(data[3].data.errormessage); }
                }

                /****** Get Parameter Invoice ******/
                if (data[4] != undefined && data[4] != "") {
                    if (data[4].data.responsecode == '200' && data[4].data.responsedata != undefined && data[4].data.responsedata != "") {
                        $scope.Parameter.Param_Quotation = [];
                        $scope.Parameter.Param_Quotation = data[4].data.responsedata;

                        var substringMatcher = function (strs) {
                            return function findMatches(q, cb) {
                                if (q == '') {
                                    cb(strs);
                                }
                                else {
                                    var matches, substringRegex;

                                    // an array that will be populated with substring matches
                                    matches = [];

                                    // regex used to determine if a string contains the substring `q`
                                    var substrRegex = new RegExp(q, 'i');

                                    // iterate through the pool of strings and for any string that
                                    // contains the substring `q`, add it to the `matches` array
                                    for (var i = 0; i < strs.length; i++) {
                                        if (substrRegex.test(strs[i])) {
                                            matches.push(strs[i]);
                                        }
                                    }

                                    cb(matches);
                                }
                            };
                        };

                        var unittype = [];
                        _.each($scope.Parameter.Param_Quotation.Param_UnitType, function (item) { unittype.push(item.Name); });

                        $('#productunit .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0
                        }, {
                                name: 'unittype',
                                limit: 20,
                                source: substringMatcher(unittype)
                            });


                        var category = [];
                        _.each($scope.Parameter.Param_Quotation.Param_Category, function (item) { category.push(item.Name); });
                        $('#productcategory .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0,
                        }, {
                                name: 'category',
                                limit: 20,
                                source: substringMatcher(category)
                            });
                    }
                    else if (data[4].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[5] != undefined && data[5] != "") {
                    if (data[5].data.responsecode == '200' && data[5].data.responsedata != undefined && data[5].data.responsedata != "") {
                        $scope.UserProfile = [];
                        $scope.UserProfile = data[5].data.responsedata;
                    }
                    else if (data[5].data.responsecode == '400') { showErrorToast(data[5].data.errormessage); }
                }

                $scope.table.binding = 0;
                /****** Get Remark ******/
                //if (data[6] != undefined && data[6] != "") {
                //    if (data[6].data.responsecode == '200' && data[6].data.responsedata != undefined && data[6].data.responsedata != "") {
                //        $scope.ReceiptAdd.Remark = data[6].data.responsedata.DocumentText;
                //    }
                //    else if (data[6].data.responsecode == '400') { showErrorToast(data[6].data.errormessage); }
                //}

                /****** Get Business Bank ******/
                if (data[7] != undefined && data[7] != "") {
                    if (data[7].data.responsecode == '200' && data[7].data.responsedata != undefined && data[7].data.responsedata != "") {
                        $scope.Parameter.BizBank = data[7].data.responsedata;
                    }
                    else if (data[7].data.responsecode == '400') { showErrorToast(data[7].data.errormessage); }
                }

                if (data[8] != undefined && data[8] != "") {
                    if (data[7].data.responsecode == '200' && data[8].data.responsedata != undefined && data[8].data.responsedata != "") {
                        $scope.Parameter.Param_Catagory = data[8].data.responsedata;
                    }
                    else if (data[7].data.responsecode == '400') { showErrorToast(data[8].data.errormessage); }
                }


                if (QueryString('key') != undefined) {

                    $http.get(baseURL + "DocumentExpenses/GetBusinessWTkey?POKey=" + QueryString('key') + "&key=" + makeid())
                        .then(function (response) {
                            var a = response;
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.PurchaseOrderAdd = [];
                                    $scope.PurchaseOrderAdd = response.data.responsedata;

                                    if ($scope.PurchaseOrderAdd.ReceiveInfo != undefined) {

                                        $scope.PurchaseOrderAdd.ReceiveInfo.PaymentDate = formatDate($scope.PurchaseOrderAdd.ReceiveInfo.PaymentDate);
                                        $scope.PurchaseOrderAdd.ReceiptInfo = $scope.PurchaseOrderAdd.ReceiveInfo;
                                    }


                                    if ($scope.PurchaseOrderAdd.ReceiveTaxInfo != undefined) {
                                        $scope.PurchaseOrderAdd.ReceiveTaxInfo.InvoiceDate = formatDate($scope.PurchaseOrderAdd.ReceiveTaxInfo.InvoiceDate);

                                        var imgURL = baseURL + 'uploads/tax/' + $scope.PurchaseOrderAdd.ReceiveTaxInfo.ThumbnailDocument;
                                        if ($scope.PurchaseOrderAdd.ReceiveTaxInfo.ThumbnailDocument != undefined && $scope.PurchaseOrderAdd.ReceiveTaxInfo.ThumbnailDocument != "") {
                                            $("#tax_thumnail").addClass('dropify');
                                            $("#tax_thumnail").attr("data-default-file", imgURL);
                                            $('.dropify').dropify();

                                            $("#imgtax").attr("src", imgURL);
                                        }
                                    }

                                    $scope.PurchaseOrderAdd.PurchaseOrderDate = formatDate($scope.PurchaseOrderAdd.PurchaseOrderDate);
                                    /*Credit */
                                    $scope.PurchaseOrderAdd.DueDate = $scope.PurchaseOrderAdd.DueDate != undefined && $scope.PurchaseOrderAdd.DueDate != "" ? formatDate($scope.PurchaseOrderAdd.DueDate) : "";

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.PurchaseOrderAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.PurchaseOrderAdd.SelectSale = type[0];

                                    $scope.PurchaseOrderAdd.IsTaxHeader = $scope.PurchaseOrderAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                    $scope.PurchaseOrderAdd.IsDiscountHeader = $scope.PurchaseOrderAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                    $scope.PurchaseOrderAdd.IsTaxSummary = $scope.PurchaseOrderAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                    //// หักภาษี ณ ที่จ่าย
                                    $scope.PurchaseOrderAdd.IsWithholdingTax = $scope.PurchaseOrderAdd.IsWithholdingTax == '1' ? true : false;
                                    if ($scope.PurchaseOrderAdd.IsWithholdingTax) {

                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.PurchaseOrderAdd.WithholdingKey; });
                                        if (type.length > 0)
                                            $scope.PurchaseOrderAdd.SelectWithholding = type[0];

                                        $scope.PurchaseOrderAdd.WithholdingAmount = AFormatNumber($scope.PurchaseOrderAdd.WithholdingAmount, 2);
                                        $scope.PurchaseOrderAdd.GrandAmountAfterWithholding = AFormatNumber($scope.PurchaseOrderAdd.GrandAmountAfterWithholding, 2);
                                    }
                                    else {
                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                        if (type.length > 0)
                                            $scope.PurchaseOrderAdd.SelectWithholding = type[0];

                                    }

                                    //   $scope.PurchaseOrderAdd.TotalAmount = $scope.PurchaseOrderAdd.TotalAmount;
                                    //   $scope.PurchaseOrderAdd.DiscountPercent = AFormatNumber($scope.PurchaseOrderAdd.DiscountPercent, 2);
                                    //    $scope.PurchaseOrderAdd.DiscountAmount = AFormatNumber($scope.PurchaseOrderAdd.DiscountAmount, 2);
                                    //    $scope.PurchaseOrderAdd.TotalAfterDiscountAmount = AFormatNumber($scope.PurchaseOrderAdd.TotalAfterDiscountAmount, 2);
                                    //    $scope.PurchaseOrderAdd.ExemptAmount = $scope.PurchaseOrderAdd.ExemptAmount;
                                    //    $scope.PurchaseOrderAdd.VatableAmount = AFormatNumber($scope.PurchaseOrderAdd.VatableAmount, 2);
                                    //   $scope.PurchaseOrderAdd.VAT = AFormatNumber($scope.PurchaseOrderAdd.VAT, 2);
                                    //   $scope.PurchaseOrderAdd.TotalBeforeVatAmount = AFormatNumber($scope.PurchaseOrderAdd.TotalBeforeVatAmount, 2);
                                    //   $scope.PurchaseOrderAdd.GrandAmount = AFormatNumber($scope.PurchaseOrderAdd.GrandAmount, 2);

                                    $scope.PurchaseOrderAdd.Signature == $scope.PurchaseOrderAdd.Signature == '1' ? true : false;

                                    _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {
                                        item.Quantity = AFormatNumber(item.Quantity, 2);
                                        item.Price = AFormatNumber(item.Price, 2);
                                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.Discount = AFormatNumber(item.Discount, 2);
                                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                        item.VatRate = AFormatNumber(item.VatRate, 2);
                                        item.Vat = AFormatNumber(item.Vat, 2);
                                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                        item.Total = AFormatNumber(item.Total, 2);
                                    });

                                    //   $scope.OnClickClearDetail();

                                    /**  duppllicate */
                                    if (QueryString('dup_ref') != undefined && QueryString('dup_ref') == '1') {
                                        $scope.PurchaseOrderAdd.PurchaseOrderNo = data[0].data.responsedata;
                                        $scope.PurchaseOrderAdd.PurchaseOrderKey = undefined;
                                        $scope.PurchaseOrderAdd.PurchaseOrderStatus = undefined;
                                        $scope.PurchaseOrderAdd.PurchaseOrderStatusName = undefined;
                                        $scope.PurchaseOrderAdd.DueDate = $scope.PurchaseOrderAdd.PurchaseOrderDate = GetDatetimeNow();
                                        $scope.OnChangeCredit();
                                        var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                        if (type.length > 0)
                                            $scope.PurchaseOrderAdd.SelectSale = type[0];

                                        _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {
                                            item.PurchaseOrderKey = undefined;
                                            item.PurchaseOrderDetailKey = undefined;
                                        });
                                    }

                                    $scope.OnLoadAttach();

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else if (QueryString('ref_key') != undefined && QueryString('ref_info') != undefined) {
                    //    // New document With Quotation
                    //    $http.get(baseURL + "DocumentSell/GetBusinessDcoumentReference?DocumentKey=" + QueryString('ref_key') + "&DocumentType=" + QueryString('ref_info') + "&key=" + makeid())
                    //        .then(function (response) {
                    //            if (response != undefined && response != "") {
                    //                if (response.data.responsecode == '200') {

                    // $scope.InvoiceAdd = [];
                    // $scope.InvoiceAdd = response.data.responsedata;
                    //                    $scope.PurchaseOrderAdd.InvoiceDetail = [];

                    //                    /** QuotationValues*/
                    //                    if (QueryString('ref_info') == 'QT') {
                    //                        $scope.PurchaseOrderAdd.DocumentKey = $scope.PurchaseOrderAdd.QuotationKey;
                    //                        $scope.PurchaseOrderAdd.DocumentNo = $scope.PurchaseOrderAdd.QuotationNo;
                    //                        $scope.PurchaseOrderAdd.ReferenceNo = $scope.PurchaseOrderAdd.QuotationNo;

                    //                        $scope.PurchaseOrderAdd.InvoiceStatus = "1";
                    //                        $scope.PurchaseOrderAdd.InvoiceStatusName = "รอดำเนินการ";

                    //                        _.each($scope.PurchaseOrderAdd.QuotationDetail, function (item) {
                    //                            item.InvoiceKey = undefined;
                    //                            item.InvoiceDetailKey = undefined;
                    //                            $scope.PurchaseOrderAdd.PurchaseOrderDetail.push(item);
                    //                        });

                    //                    }
                    //                    else if (QueryString('ref_info') == 'BL') {
                    //                        $scope.PurchaseOrderAdd.DocumentKey = $scope.PurchaseOrderAdd.BillingKey;
                    //                        $scope.PurchaseOrderAdd.DocumentNo = $scope.PurchaseOrderAdd.BillingNo;
                    //                        $scope.PurchaseOrderAdd.ReferenceNo = $scope.PurchaseOrderAdd.BillingNo;

                    //                        $scope.PurchaseOrderAdd.InvoiceStatus = "2";
                    //                        $scope.PurchaseOrderAdd.InvoiceStatusName = "รอเก็บเงิน";

                    //                        _.each($scope.PurchaseOrderAdd.BillingDetail, function (item) {
                    //                            item.InvoiceKey = undefined;
                    //                            item.InvoiceDetailKey = undefined;
                    //                            $scope.PurchaseOrderAdd.PurchaseOrderDetail.push(item);
                    //                        });

                    //                    }
                    //                    $scope.PurchaseOrderAdd.DocumentOwner = QueryString('ref_info');

                    //                    /**  clear values */
                    //                    $scope.PurchaseOrderAdd.InvoiceNo = data[0].data.responsedata;
                    //                    $scope.PurchaseOrderAdd.InvoiceKey = undefined;

                    //                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                    //                    if (type.length > 0)
                    //                        $scope.PurchaseOrderAdd.SelectSale = type[0];


                    //                    $scope.PurchaseOrderAdd.InvoiceDate = GetDatetimeNow();

                    //                    /*Credit */
                    //                    /*หาก เครดิตเป็นเงินสด เปลี่ยนเป็น เครดิต (วัน)**/
                    //                    if ($scope.PurchaseOrderAdd.CreditType == 'CA')
                    //                        $scope.PurchaseOrderAdd.CreditType = 'CD';
                    //                    if ($scope.PurchaseOrderAdd.CreditType == 'CD')
                    //                        $scope.OnChangeCredit();


                    //                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.PurchaseOrderAdd.SaleID; });
                    //                    if (type.length > 0)
                    //                        $scope.PurchaseOrderAdd.SelectSale = type[0];

                    //                    $scope.PurchaseOrderAdd.IsTaxHeader = $scope.PurchaseOrderAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                    //                    $scope.PurchaseOrderAdd.IsDiscountHeader = $scope.PurchaseOrderAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                    //                    $scope.PurchaseOrderAdd.IsTaxSummary = $scope.PurchaseOrderAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                    //                    // หักภาษี ณ ที่จ่าย
                    //                    $scope.PurchaseOrderAdd.IsWithholdingTax = $scope.PurchaseOrderAdd.IsWithholdingTax == '1' ? true : false;
                    //                    if ($scope.PurchaseOrderAdd.IsWithholdingTax) {

                    //                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.PurchaseOrderAdd.WithholdingKey; });
                    //                        if (type.length > 0)
                    //                            $scope.PurchaseOrderAdd.SelectWithholding = type[0];

                    //                        $scope.PurchaseOrderAdd.IsWithholdingTax.WithholdingAmount = AFormatNumber($scope.PurchaseOrderAdd.IsWithholdingTax.WithholdingAmount, 2);
                    //                        $scope.PurchaseOrderAdd.IsWithholdingTax.GrandAmountAfterWithholding = AFormatNumber($scope.PurchaseOrderAdd.IsWithholdingTax.GrandAmountAfterWithholding, 2);
                    //                    }
                    //                    else {
                    //                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                    //                        if (type.length > 0)
                    //                            $scope.PurchaseOrderAdd.SelectWithholding = type[0];

                    //                    }

                    //                    $scope.PurchaseOrderAdd.TotalAmount = AFormatNumber($scope.PurchaseOrderAdd.TotalAmount, 2);
                    //                    $scope.PurchaseOrderAdd.DiscountPercent = AFormatNumber($scope.PurchaseOrderAdd.DiscountPercent, 2);
                    //                    $scope.PurchaseOrderAdd.DiscountAmount = AFormatNumber($scope.PurchaseOrderAdd.DiscountAmount, 2);
                    //                    $scope.PurchaseOrderAdd.TotalAfterDiscountAmount = AFormatNumber($scope.PurchaseOrderAdd.TotalAfterDiscountAmount, 2);
                    //                    $scope.PurchaseOrderAdd.ExemptAmount = AFormatNumber($scope.PurchaseOrderAdd.ExemptAmount, 2);
                    //                    $scope.PurchaseOrderAdd.VatableAmount = AFormatNumber($scope.PurchaseOrderAdd.VatableAmount, 2);
                    //                    $scope.PurchaseOrderAdd.VAT = AFormatNumber($scope.PurchaseOrderAdd.VAT, 2);
                    //                    $scope.PurchaseOrderAdd.TotalBeforeVatAmount = AFormatNumber($scope.PurchaseOrderAdd.TotalBeforeVatAmount, 2);
                    //                    $scope.PurchaseOrderAdd.GrandAmount = AFormatNumber($scope.PurchaseOrderAdd.GrandAmount, 2);
                    //                    $scope.ReceiptAdd.WithholdingAmount = AFormatNumber($scope.PurchaseOrderAdd.WithholdingAmount, 2);//

                    //                    $scope.PurchaseOrderAdd.Signature == $scope.PurchaseOrderAdd.Signature == '1' ? true : false;

                    //                    _.each($scope.PurchaseOrderAdd.InvoiceDetail, function (item) {
                    //                        item.Quantity = AFormatNumber(item.Quantity, 2);
                    //                        item.Price = AFormatNumber(item.Price, 2);
                    //                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                    //                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                    //                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                    //                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                    //                        item.Discount = AFormatNumber(item.Discount, 2);
                    //                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                    //                        item.VatRate = AFormatNumber(item.VatRate, 2);
                    //                        item.Vat = AFormatNumber(item.Vat, 2);
                    //                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                    //                        item.Total = AFormatNumber(item.Total, 2);
                    //                    });

                    //                    $scope.OnClickClearDetail();
                    //                    $scope.table.binding = 0;
                    //                }
                    //                else if (response.data.responsecode == '400') {
                    //                    showErrorToast(response.data.errormessage);
                    //                }
                    //            }
                    //            else {
                    //                showErrorToast(response.data.errormessage);
                    //            }
                    //        });
                }
                else {
                    /******* Initail ********/
                    $scope.PurchaseOrderAdd.DueDate = $scope.PurchaseOrderAdd.PurchaseOrderDate = GetDatetimeNow();
                    $scope.PurchaseOrderAdd.CreditType = 'CD';
                    $scope.PurchaseOrderAdd.TaxType = 'N';
                    $scope.OnChangeCreditType();

                    $scope.PurchaseOrderAdd.IsTaxHeader = false; //แยกรายการภาษี
                    $scope.PurchaseOrderAdd.IsDiscountHeader = false; //แยกรายการาส่วนลด
                    $scope.PurchaseOrderAdd.IsWithholdingTax = false; // หักภาษี ณ ที่จ่าย

                    $scope.PurchaseOrderAdd.IsTaxSummary = true; // หักภาษี 7 % รวม
                    $scope.PurchaseOrderAdd.IsDiscountHeaderType = '%'; // ส่วนลดแยกรายการแบบ Percent

                    $scope.PurchaseOrderAdd.ExemptAmount = $scope.PurchaseOrderAdd.VatableAmount = $scope.PurchaseOrderAdd.TotalAmount
                        = $scope.PurchaseOrderAdd.WithholdingAmount = $scope.PurchaseOrderAdd.GrandAmount = 0.00;

                    $scope.PurchaseOrderAdd.WithholdingTaxForm = "";

                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                    if (type.length > 0)
                        $scope.PurchaseOrderAdd.SelectSale = type[0];

                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                    if (type.length > 0)
                        $scope.PurchaseOrderAdd.SelectWithholding = type[0];

                    $scope.OnClickClearDetail();

                    // $scope.PurchaseOrderAdd.InvoiceDetail = [];
                    $scope.PurchaseOrderAdd.PurchaseOrderDetails = [];
                    $scope.OnChangeDetailCalculate();


                    $scope.table.binding = 0;
                }
            }
            catch (err) {
                showErrorToast(err);
            }
        });
    };


    $scope.OnClickCancel = function () {
        window.location.href = baseURL + "DocumentExpenses/WithholdingTax";
    };

    $scope.OnDisabled = function () {
        if ($scope.PurchaseOrderAdd.PurchaseOrderStatus == undefined || $scope.PurchaseOrderAdd.PurchaseOrderStatus == '1' || $scope.PurchaseOrderAdd.PurchaseOrderStatus == '2')
            return false;
        else
            return true;
    };

    $scope.OnClickReport = function () {
        $scope.Print = "1";
        $scope.OnClickSave();
    };

    $scope.OnClickDownload = function () {
        $scope.Print = "2";
        $scope.OnClickSave();
    };


    /******************Due Date *********** */
    $scope.OnChangeCreditType = function () {
        if ($scope.PurchaseOrderAdd.CreditType == 'CD') {
            $scope.OnChangeCredit();
        }
        if ($scope.PurchaseOrderAdd.CreditType == 'CA') { $scope.PurchaseOrderAdd.CreditDay = 0; }
    };

    $scope.OnChangeCredit = function () {

        if ($scope.PurchaseOrderAdd.CreditType == 'CD') {
            if ($scope.PurchaseOrderAdd.CreditDay == undefined || $scope.PurchaseOrderAdd.CreditDay == '') {
                $scope.PurchaseOrderAdd.PurchaseOrderDate = $scope.PurchaseOrderAdd.DueDate = GetDatetimeNow();
                $scope.PurchaseOrderAdd.CreditDay = 0;
            }
            else {
                $scope.PurchaseOrderAdd.DueDate = SetDatetimeDay($scope.PurchaseOrderAdd.PurchaseOrderDate, $scope.PurchaseOrderAdd.CreditDay);
            }
        }
    };

    $scope.OnChangeDateDue = function () {
        if ($scope.PurchaseOrderAdd.CreditType == 'CD') {
            if ($scope.PurchaseOrderAdd.DueDate == undefined || $scope.PurchaseOrderAdd.DueDate == '') {
                $scope.PurchaseOrderAdd.PurchaseOrderDate = GetDatetimeNow();
                $scope.PurchaseOrderAdd.CreditDay = 0;
            }
            else {
                var datedue = $scope.PurchaseOrderAdd.DueDate;
                var days = DatetimeLenof($scope.PurchaseOrderAdd.PurchaseOrderDate, $scope.PurchaseOrderAdd.DueDate);
                if (days < 0) {
                    $scope.PurchaseOrderAdd.PurchaseOrderDate = SetDatetimeDay($scope.PurchaseOrderAdd.DueDate, days);
                    $scope.PurchaseOrderAdd.DueDate = datedue;
                    $scope.PurchaseOrderAdd.CreditDay = (days * (-1));
                }
                else
                    $scope.PurchaseOrderAdd.CreditDay = days;
            }
        }
    };

    $scope.OnChangeInvoiceDate = function () {
        if ($scope.PurchaseOrderAdd.CreditType == 'CD') {
            if ($scope.PurchaseOrderAdd.PurchaseOrderDate == undefined || $scope.PurchaseOrderAdd.PurchaseOrderDate == '') {
                $scope.PurchaseOrderAdd.DueDate = $scope.PurchaseOrderAdd.PurchaseOrderDate = GetDatetimeNow();
                $scope.PurchaseOrderAdd.CreditDay = 0;
            }
            else {
                var days = $scope.PurchaseOrderAdd.CreditDay == undefined || $scope.PurchaseOrderAdd.CreditDay == "" ? 0 : $scope.PurchaseOrderAdd.CreditDay;
                $scope.PurchaseOrderAdd.DueDate = SetDatetimeDay($scope.PurchaseOrderAdd.PurchaseOrderDate, $scope.PurchaseOrderAdd.CreditDay);
            }
        }
        var prefix = { asofdate: $scope.PurchaseOrderAdd.PurchaseOrderDate, documenttype: headerdoc };
        var qq = $q.all([paramService.getDocumentNo(prefix)]).then(function (data) {
            /****** GetDocument NO ******/
            if (data[0] != undefined && data[0] != "") {
                if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.PurchaseOrderAdd.PurchaseOrderNo = data[0].data.responsedata; }
                else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
            }
        });
    };

    /*********** Header Setting Discount , Tax**************/
    $scope.OnChangeIsTaxHeader = function () {
        if ($scope.PurchaseOrderAdd.IsTaxHeader) {
            $scope.PurchaseOrderAdd.IsDiscountHeader = true;
        }
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeIsDiscountHeader = function () {
        $scope.PurchaseOrderAdd.DiscountPercent = '0';
        $scope.PurchaseOrderAdd.DiscountAmount = '0';
        $scope.OnClickDiscountType($scope.PurchaseOrderAdd.IsDiscountHeaderType);
    };


    /******************* Detail And Calculate *************************/
    var vatParam = 7;
    $scope.ProductDetailAdd = [];
    $scope.ProductDetailAdd.TaxType = 'N';

    function CaluculateTaxRecord(input) {
        if (input.TaxPercentType != undefined && input.TaxPercentType != "" &&
            input.Price != undefined && input.Price != "" &&
            input.TaxType != undefined && input.TaxType != "") {
            var taxPercnet = ConvertToDecimal(input.TaxPercentType);
            var price = ConvertToDecimal(input.Price);
            var PriceAfterTax = price;
            var Vat = 0;

            var pricetax = 0;
            if (input.PriceTax != undefined && input.PriceTax != "" && input.TaxPercentType == "0")
                pricetax = ConvertToDecimal(input.PriceTax);

            if (input.TaxType == "V") {

                Vat = (price * vatParam) / (100 + vatParam);
                PriceAfterTax = price - Vat;

                if (pricetax == 0)
                    pricetax = (PriceAfterTax * taxPercnet) / 100;
            }
            else {
                if (input.TaxType == "N")
                    Vat = (price * vatParam) / 100;

                if (pricetax == 0)
                    pricetax = (price * taxPercnet) / 100;
            }

            input.Vat = AFormatNumber(Vat, 2);
            input.PriceAfterTax = AFormatNumber(PriceAfterTax, 2);
            input.PriceTax = AFormatNumber(pricetax, 2);

        }
        return input;
    }

    $scope.OnChangeTaxPercent = function () {
        $scope.ProductDetailAdd = CaluculateTaxRecord($scope.ProductDetailAdd);
    }

    $scope.CaculateAll = function () {
        var taxform = $scope.PurchaseOrderAdd;

        var ExemptAmount = 0;
        var VatableAmount = 0;
        var TotalAmount = 0;
        var WithholdingAmount = 0;
        var GrandAmount = 0;
        var TotalAmount = 0;

        _.each(taxform.PurchaseOrderDetails, function (item) {
            item = CaluculateTaxRecord(item);

            ExemptAmount += ConvertToDecimal(item.PriceAfterTax);
            VatableAmount += ConvertToDecimal(item.Vat);
            // TotalAmount += parseFloat(item.Price);
            WithholdingAmount += ConvertToDecimal(item.PriceTax);


        });

        taxform.ExemptAmount = ExemptAmount;
        taxform.VatableAmount = VatableAmount;
        taxform.TotalAmount = ExemptAmount + VatableAmount;
        taxform.WithholdingAmount = WithholdingAmount;
        taxform.GrandAmount = (ExemptAmount + VatableAmount) - WithholdingAmount;

    }

    $scope.OnClickProductDetailAdd = function () {
        try {

            var valid = _.where($scope.PurchaseOrderAdd.PurchaseOrderDetails, { WithholdingTaxCatagory: $scope.ProductDetailAdd.WithholdingTaxCatagory })[0];


            if ($scope.ProductDetailAdd.WithholdingTaxCatagory == undefined || $scope.ProductDetailAdd.WithholdingTaxCatagory == "")
                showWariningToast("กรุณาเลือกประเภทเงินได้ที่จ่าย");
            else if ($scope.ProductDetailAdd.TaxPercentType == undefined || $scope.ProductDetailAdd.TaxPercentType == "")
                showWariningToast("กรุณาเลือกอัตราภาษีที่หัก");
            else if ($scope.ProductDetailAdd.PriceTax == undefined || $scope.ProductDetailAdd.PriceTax == "")
                showWariningToast("กรุณากรอกจำนวนเงินที่หัก");
            else if ($scope.ProductDetailAdd.Price == undefined || $scope.ProductDetailAdd.Price == "")
                showWariningToast("กรุณากรอกจำนวนเงิน");//TaxType
            else if ($scope.ProductDetailAdd.TaxType == undefined || $scope.ProductDetailAdd.TaxType == "")
                showWariningToast("กรุณาเลือกภาษีมูลค่าเพิ่ม");
            else if (valid != undefined)
                showWariningToast("ประเภทการหักภาษีไม่สามารถซ้ำกันได้");
            else {
                $scope.OnChangeTaxPercent();
                $scope.ProductDetailAdd.Sequence = $scope.PurchaseOrderAdd.PurchaseOrderDetails.length + 1;
                $scope.ProductDetailAdd.PriceTax = AFormatNumber($scope.ProductDetailAdd.PriceTax, 2);
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);

                $scope.PurchaseOrderAdd.PurchaseOrderDetails.push($scope.ProductDetailAdd);
                $scope.CaculateAll()
                $scope.ProductDetailAdd = [];
                $scope.ProductDetailAdd.TaxType = 'N';

            }


            //if ($scope.ProductDetailAdd.Name == undefined || $scope.ProductDetailAdd.Name == "")
            //    throw "กรุณากรอกชื่อสินค้า";
            //if ($scope.ProductDetailAdd.Price == undefined || $scope.ProductDetailAdd.Price == "")
            //    throw "กรุณากรอกราคาต่อหน่วย";
            //else if ($scope.ProductDetailAdd.Quantity == undefined || $scope.ProductDetailAdd.Quantity == "")
            //    throw "กรุณากรอกจำนวน";
            //else {
            //    $scope.ProductDetailAdd.Sequence = $scope.PurchaseOrderAdd.PurchaseOrderDetails.length + 1;
            //    $scope.ProductDetailAdd.DiscountType = $scope.PurchaseOrderAdd.IsDiscountHeaderType;

            //    if ($scope.ProductDetailAdd.ProductKey == undefined) {
            //        $scope.ProductDetailAdd.DiscountType = $scope.PurchaseOrderAdd.IsDiscountHeaderType;
            //        $scope.ProductDetailAdd.DiscountPercent = '0.00';
            //        $scope.ProductDetailAdd.Discount = '0.00';
            //        $scope.ProductDetailAdd.VatType = '2';

            //        //var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
            //        var sell = 0, selltax = 0, selllast = 0;
            //        sell = ConvertToDecimal($scope.ProductDetailAdd.Price == undefined ? 0 : $scope.ProductDetailAdd.Price);
            //        //selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
            //        //selllast = sell + selltax;

            //        $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
            //        $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
            //        $scope.ProductDetailAdd.PriceTax = AFormatNumber(0, 2);
            //    }

            //    if ($scope.PurchaseOrderAdd.TaxType == 'N')
            //        $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
            //    else if ($scope.PurchaseOrderAdd.TaxType == 'V')
            //        $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

            //    $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            //    $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            //}

            //$scope.PurchaseOrderAdd.PurchaseOrderDetails.push($scope.ProductDetailAdd);
            //$scope.OnClickClearDetail();
            //$scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickProductDetailDelete = function (index) {
        try {
            $scope.PurchaseOrderAdd.PurchaseOrderDetails.splice(index, 1);
            $scope.CaculateAll();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickClearDetail = function () {
        $scope.ProductDetailAdd = [];
        $scope.ProductDetailAdd.DiscountType = $scope.PurchaseOrderAdd.IsDiscountHeaderType;
        $scope.ProductDetailAdd.VatType = '2';
    };

    $scope.OnClickDiscountType = function (val) {
        $scope.PurchaseOrderAdd.IsDiscountHeaderType = val;
        _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {
            item.DiscountType = val;
            item.DiscountPercent = item.Discount = AFormatNumber(0, 2);
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountAmount = function () {
        $scope.PurchaseOrderAdd.DiscountPercent = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickNoTaxSummary = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickWithholdingTax = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeProductTaxType = function () {
        _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {
            if ($scope.PurchaseOrderAdd.TaxType == 'N')
                item.Price = item.PriceBeforeTax;
            else if ($scope.PurchaseOrderAdd.TaxType == 'V')
                item.Price = item.PriceAfterTax;
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountPercent = function () {
        if ($scope.PurchaseOrderAdd.DiscountPercent == undefined || $scope.PurchaseOrderAdd.DiscountPercent === '' || $scope.PurchaseOrderAdd.DiscountPercent == 0)
            $scope.PurchaseOrderAdd.DiscountAmount = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDetailCalculate = function () {
        try {
            _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {
                var amt, qty, total, vatrate, vatamt, discountper, discountminus, discount, discountafter, vatafter = 0, totalamount, vatcal = 0;
                // Get Values
                amt = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                vat = item.VatType == '1' ? 0 : item.VatType == '2' ? 7 : 0; // dropdown vat หน้าจอ
                discountper = item.DiscountPercent == undefined || item.DiscountPercent === '' ? '0' : detectFloat(item.DiscountPercent); //ส่วนลด % จากหน้าจอ
                discount = item.Discount == undefined || item.Discount === '' ? '0' : detectFloat(item.Discount); //ส่วนลดจากหน้าจอ
                discountminus = vatafter = 0; // ราคาส่วนลด
                discountafter = 0; // หลังหักส่วนลด
                vatafter = 0;

                total = amt * qty;
                // Calculate
                if ($scope.PurchaseOrderAdd.IsDiscountHeader == true) { //เงื่อนไขหักส่วนลด
                    if (item.DiscountType == '%') {
                        discountminus = total * discountper / 100;
                        discountafter = detectFloat(AFormatNumber(total - discountminus, 2));
                    }
                    else if (item.DiscountType == '฿') {
                        discountminus = discount;
                        discountafter = total - discount;
                    }
                }
                else {
                    discountafter = total;
                    discountper = discount = 0;
                }

                if ($scope.PurchaseOrderAdd.IsTaxHeader == true) { //คำนวณหาภาษี ของแต่ละรายการ
                    if (item.VatType == '2') {
                        if ($scope.PurchaseOrderAdd.TaxType == 'N') {

                            vatcal = vat > 0 ? ((discountafter * (100 + vat) / 100)) : 0;
                            vatamt = detectFloat(AFormatNumber(vatcal, 2)) - discountafter;
                            vatafter = discountafter + vatamt;
                        }
                        else if ($scope.PurchaseOrderAdd.TaxType == 'V') {
                            vatcal = vat > 0 ? ((discountafter / (100 + vat) * 100)) : 0;
                            vatamt = discountafter - detectFloat(AFormatNumber(vatcal, 2));
                            vatafter = discountafter - vatamt;
                        }
                    }
                    else
                        vatamt = 0;
                }
                else {
                    item.VatType = '2';
                    vatamt = 0;
                }

                totalamount = discountafter;

                // Set Value
                item.Discount = AFormatNumber(discountminus, 2);
                item.DiscountAfter = AFormatNumber(discountafter, 2);
                item.VatRate = AFormatNumber(vat, 2);
                item.Vat = AFormatNumber(vatamt, 2);
                item.VatAfter = AFormatNumber(vatafter, 2);
                item.Total = AFormatNumber(totalamount, 2);
            });

            /******* หาค่ารวม Summary ***********/
            var price = 0, priceedis = 0, pricevat = 0, qty = 0, totalprice = 0, total = 0, discounthper = 0, discounthamt = 0, priceafterdiscount = 0, discounth = 0, grandtotal = 0, exemptamt = 0, vatableamt = 0, vat = 0, withholdingrate = 0, withholdingprice = 0, withholdingafter = 0, vatafter = 0;


            discounthper = $scope.PurchaseOrderAdd.DiscountPercent == undefined || $scope.PurchaseOrderAdd.DiscountPercent === '' ? 0 : detectFloat($scope.PurchaseOrderAdd.DiscountPercent); // Discount Percenter
            discounthamt = $scope.PurchaseOrderAdd.DiscountAmount == undefined || $scope.PurchaseOrderAdd.DiscountAmount === '' ? 0 : $scope.PurchaseOrderAdd.IsTaxHeader == false ? detectFloat($scope.PurchaseOrderAdd.DiscountAmount) : 0; // Discount Amount

            if ($scope.PurchaseOrderAdd.PurchaseOrderDetails != undefined && $scope.PurchaseOrderAdd.PurchaseOrderDetails.length > 0) {
                _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {

                    var amt, qty, total, vatrate, vatamt, discountper, discountminus, discount, discountafter, vatafter = 0, totalamount, vatcal = 0;
                    // Get Values
                    amt = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                    qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                    vat = item.VatType == '1' ? 0 : item.VatType == '2' ? 7 : 0; // dropdown vat หน้าจอ
                    discountper = item.DiscountPercent == undefined || item.DiscountPercent === '' ? '0' : detectFloat(item.DiscountPercent); //ส่วนลด % จากหน้าจอ
                    discount = item.Discount == undefined || item.Discount === '' ? '0' : detectFloat(item.Discount); //ส่วนลดจากหน้าจอ
                    discountminus = vatafter = 0; // ราคาส่วนลด
                    discountafter = 0; // หลังหักส่วนลด
                    vatafter = 0;

                    total = amt * qty;

                    if ($scope.PurchaseOrderAdd.IsTaxHeader) { // หักภาษี แบบทีละรายการ
                        if (item.VatType == '1' || item.VatType == '3') //0% ยกเว้น
                            exemptamt = exemptamt + totalprice - priceedis;
                        else if (item.VatType == '2') // 7%
                            vatableamt = (vatableamt + totalprice - priceedis) - ($scope.PurchaseOrderAdd.TaxType == 'V' ? pricevat : 0); //ถ้าเลือกรวมภาษีแล้ว ให้หัก ภาษีออก

                        vat = vat + pricevat;
                    }

                    // Calculate
                    if ($scope.PurchaseOrderAdd.IsDiscountHeader == true) { //เงื่อนไขหักส่วนลด
                        if (item.DiscountType == '%') {
                            discountminus = total * discountper / 100;
                            discountafter = detectFloat(AFormatNumber(total - discountminus, 2));
                        }
                        else if (item.DiscountType == '฿') {
                            discountminus = discount;
                            discountafter = total - discount;
                        }
                    }
                    else {
                        discountafter = total;
                        discountper = discount = 0;
                    }

                    if ($scope.PurchaseOrderAdd.IsTaxHeader == true) { //คำนวณหาภาษี ของแต่ละรายการ
                        if (item.VatType == '2') {
                            if ($scope.PurchaseOrderAdd.TaxType == 'N') {

                                vatcal = vat > 0 ? ((discountafter * (100 + vat) / 100)) : 0;
                                vatamt = detectFloat(AFormatNumber(vatcal, 2)) - discountafter;
                                vatafter = discountafter + vatamt;
                            }
                            else if ($scope.PurchaseOrderAdd.TaxType == 'V') {
                                vatcal = vat > 0 ? ((discountafter / (100 + vat) * 100)) : 0;
                                vatamt = discountafter - detectFloat(AFormatNumber(vatcal, 2));
                                vatafter = discountafter - vatamt;
                            }
                        }
                        else
                            vatamt = 0;
                    }
                    else {
                        item.VatType = '2';
                        vatamt = 0;
                    }

                    totalamount = discountafter;

                    // Set Value
                    item.Discount = AFormatNumber(discountminus, 2);
                    item.DiscountAfter = AFormatNumber(discountafter, 2);
                    item.VatRate = AFormatNumber(vat, 2);
                    item.Vat = AFormatNumber(vatamt, 2);
                    item.VatAfter = AFormatNumber(vatafter, 2);
                    item.Total = AFormatNumber(totalamount, 2);
                });

                /******* หาค่ารวม Summary ***********/
                var price = 0, priceedis = 0, pricevat = 0, qty = 0, totalprice = 0, total = 0, discounthper = 0, discounthamt = 0, priceafterdiscount = 0, discounth = 0, grandtotal = 0, exemptamt = 0, vatableamt = 0, vat = 0, withholdingrate = 0, withholdingprice = 0, withholdingafter = 0, vatafter = 0;


                discounthper = $scope.PurchaseOrderAdd.DiscountPercent == undefined || $scope.PurchaseOrderAdd.DiscountPercent === '' ? 0 : detectFloat($scope.PurchaseOrderAdd.DiscountPercent); // Discount Percenter
                discounthamt = $scope.PurchaseOrderAdd.DiscountAmount == undefined || $scope.PurchaseOrderAdd.DiscountAmount === '' ? 0 : $scope.PurchaseOrderAdd.IsTaxHeader == false ? detectFloat($scope.PurchaseOrderAdd.DiscountAmount) : 0; // Discount Amount

                if ($scope.PurchaseOrderAdd.PurchaseOrderDetails != undefined && $scope.PurchaseOrderAdd.PurchaseOrderDetails.length > 0) {
                    _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {

                        price = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                        priceedis = item.Discount == undefined || item.Discount === '' ? 0 : detectFloat(item.Discount); // ส่วนลดคำนวณ
                        qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                        pricevat = item.Vat == undefined || item.Vat === '' ? 0 : detectFloat(item.Vat); // จำนวน จากหน้าจอ

                        totalprice = totalprice + price * qty;//รวมเป็นเงิน
                        discounth = discounth + priceedis; //ส่วนลดรวม

                        if ($scope.PurchaseOrderAdd.IsTaxHeader) { // หักภาษี แบบทีละรายการ
                            if (item.VatType == '1' || item.VatType == '3') //0% ยกเว้น
                                exemptamt = exemptamt + totalprice - priceedis;
                            else if (item.VatType == '2') // 7%
                                vatableamt = (vatableamt + price - priceedis) - ($scope.PurchaseOrderAdd.TaxType == 'V' ? pricevat : 0); //ถ้าเลือกรวมภาษีแล้ว ให้หัก ภาษีออก

                            vat = vat + pricevat;
                        }
                    });

                    /**** หักแบบ Summary ***/
                    if ($scope.PurchaseOrderAdd.IsDiscountHeader == false) {
                        if (discounthper > 0)
                            discounthamt = totalprice * discounthper / 100;
                        discounth = discounthamt;
                    }

                    priceafterdiscount = totalprice - discounth; //รวมเป็นเงิน - หักส่วนลด = ราคาหลังหักส่วนลด

                    /*** หักภาษีแบบ Summary **/
                    if (!$scope.PurchaseOrderAdd.IsTaxHeader && $scope.PurchaseOrderAdd.IsTaxSummary) {
                        if ($scope.PurchaseOrderAdd.TaxType == 'N') {
                            vatcal = (priceafterdiscount * (100 + 7) / 100);
                            vatamt = vatcal - priceafterdiscount;
                            vatafter = priceafterdiscount + vatamt;
                        }
                        else if ($scope.PurchaseOrderAdd.TaxType == 'V') {
                            vatcal = (priceafterdiscount / (100 + 7) * 100);
                            vatamt = priceafterdiscount - vatcal;
                            vatafter = priceafterdiscount - vatamt;
                        }
                        vat = vatamt;
                    }

                    grandtotal = totalprice - discounth + ($scope.PurchaseOrderAdd.TaxType == 'N' ? vat : 0); //จำนวนเงินรวมทั้งสิ้น

                    if ($scope.PurchaseOrderAdd.IsWithholdingTax) {
                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.PurchaseOrderAdd.SelectWithholding.ID; });
                        if (type.length > 0)
                            withholdingrate = type[0].Rate;

                        if ($scope.PurchaseOrderAdd.TaxType == 'N')
                            withholdingprice = priceafterdiscount * withholdingrate / 100;
                        else if ($scope.PurchaseOrderAdd.TaxType == 'V')
                            withholdingprice = vatafter * withholdingrate / 100;

                        withholdingafter = $scope.PurchaseOrderAdd.IsTaxSummary ? (grandtotal - withholdingprice) : (priceafterdiscount - withholdingprice);
                    }

                    $scope.PurchaseOrderAdd.TotalAmount = AFormatNumber(totalprice, 2);
                    $scope.PurchaseOrderAdd.DiscountAmount = AFormatNumber(discounth, 2);
                    $scope.PurchaseOrderAdd.TotalAfterDiscountAmount = AFormatNumber(priceafterdiscount, 2);
                    $scope.PurchaseOrderAdd.ExemptAmount = AFormatNumber(exemptamt, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                    $scope.PurchaseOrderAdd.VatableAmount = AFormatNumber(vatableamt, 2);//มูลค่าที่คำนวณภาษี
                    $scope.PurchaseOrderAdd.VAT = AFormatNumber(vat, 2);//ภาษีมูลค่าเพิ่ม
                    $scope.PurchaseOrderAdd.TotalBeforeVatAmount = AFormatNumber(vatafter, 2);//จำนวนเงินรวมภาษี
                    $scope.PurchaseOrderAdd.GrandAmount = AFormatNumber(grandtotal, 2);//
                    $scope.PurchaseOrderAdd.WithholdingAmount = AFormatNumber(withholdingprice, 2);//
                    $scope.PurchaseOrderAdd.GrandAmountAfterWithholding = AFormatNumber(withholdingafter, 2);//
                }
                else {
                    $scope.PurchaseOrderAdd.TotalAmount = AFormatNumber(0, 2);
                    $scope.PurchaseOrderAdd.DiscountPercent = AFormatNumber(0, 2);
                    $scope.PurchaseOrderAdd.DiscountAmount = AFormatNumber(0, 2);
                    $scope.PurchaseOrderAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                    $scope.PurchaseOrderAdd.ExemptAmount = AFormatNumber(0, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                    $scope.PurchaseOrderAdd.VatableAmount = AFormatNumber(0, 2);//มูลค่าที่คำนวณภาษี
                    $scope.PurchaseOrderAdd.VAT = AFormatNumber(0, 2);//ภาษีมูลค่าเพิ่ม
                    $scope.PurchaseOrderAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);//จำนวนเงินรวมภาษี
                    $scope.PurchaseOrderAdd.GrandAmount = AFormatNumber(0, 2);//
                    $scope.PurchaseOrderAdd.WithholdingAmount = AFormatNumber(0, 2);//
                    $scope.PurchaseOrderAdd.GrandAmountAfterWithholding = AFormatNumber(0, 2);//
                }
            }
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickSave = function (exit) {
        try {
            if ($scope.PurchaseOrderAdd.CustomerName == undefined || $scope.PurchaseOrderAdd.CustomerName == "")
                throw "กรุณาเลือกผู้ติดต่อ";
            else if ($scope.PurchaseOrderAdd.WithholdingTaxForm == undefined || $scope.PurchaseOrderAdd.WithholdingTaxForm == '')
                throw "กรุณาเลือกประเภทแบบฟอร์ม";
            else if ($scope.PurchaseOrderAdd.PurchaseOrderDetails == undefined || $scope.PurchaseOrderAdd.PurchaseOrderDetails.length <= 0)
                throw "กรุณาเลือกประเภทเงินได้ที่จ่าย";
            else {

                var credittype;
                if ($scope.PurchaseOrderAdd.CreditType == 'CD')
                    credittype = 'เครดิต(วัน)';
                else if ($scope.PurchaseOrderAdd.CreditType == 'CA')
                    credittype = 'เงินสด';
                else if ($scope.PurchaseOrderAdd.CreditType == 'CN')
                    credittype = 'เครดิต (ไม่แสดงวันที่)';

                var withform;
                if ($scope.PurchaseOrderAdd.WithholdingTaxForm == '1') withform = 'ภงด 3';
                else if ($scope.PurchaseOrderAdd.WithholdingTaxForm == '2') withform = 'ภงด 53';
                else if ($scope.PurchaseOrderAdd.WithholdingTaxForm == '3') withform = 'ภงด 1ก';
                else if ($scope.PurchaseOrderAdd.WithholdingTaxForm == '4') withform = 'ภงด 1ก(พิเศษ)';
                else if ($scope.PurchaseOrderAdd.WithholdingTaxForm == '5') withform = 'ภงด 2';
                else if ($scope.PurchaseOrderAdd.WithholdingTaxForm == '6') withform = 'ภงด 2ก';
                else if ($scope.PurchaseOrderAdd.WithholdingTaxForm == '7') withform = 'ภงด 3ก';

                var data = {
                    PurchaseOrderKey: $scope.PurchaseOrderAdd.PurchaseOrderKey,
                    PurchaseOrderNo: $scope.PurchaseOrderAdd.PurchaseOrderNo,
                    PurchaseOrderStatus: $scope.PurchaseOrderAdd.PurchaseOrderStatus,
                    PurchaseOrderStatusName: $scope.PurchaseOrderAdd.PurchaseOrderStatusName,
                    CustomerKey: $scope.PurchaseOrderAdd.CustomerKey,
                    CustomerName: $scope.PurchaseOrderAdd.CustomerName,
                    CustomerTaxID: $scope.PurchaseOrderAdd.CustomerTaxID,
                    CustomerContactName: $scope.PurchaseOrderAdd.CustomerContactName,
                    CustomerContactPhone: $scope.PurchaseOrderAdd.CustomerContactPhone,
                    CustomerContactEmail: $scope.PurchaseOrderAdd.CustomerContactEmail,
                    CustomerAddress: $scope.PurchaseOrderAdd.CustomerAddress,
                    CustomerBranch: $scope.PurchaseOrderAdd.CustomerBranch,
                    PurchaseOrderDate: ToJsonDate2($scope.PurchaseOrderAdd.PurchaseOrderDate),
                    CreditType: $scope.PurchaseOrderAdd.CreditType,
                    CreditTypeName: credittype,
                    CreditDay: $scope.PurchaseOrderAdd.CreditDay,
                    DueDate: $scope.PurchaseOrderAdd.CreditType == 'CD' ? ToJsonDate2($scope.PurchaseOrderAdd.DueDate) : null,
                    SaleID: $scope.PurchaseOrderAdd.SelectSale.UID,
                    SaleName: $scope.PurchaseOrderAdd.SelectSale.EmployeeName,
                    ProjectName: $scope.PurchaseOrderAdd.ProjectName,
                    ReferenceNo: $scope.PurchaseOrderAdd.ReferenceNo,
                    TaxType: $scope.PurchaseOrderAdd.TaxType,
                    TaxTypeName: $scope.PurchaseOrderAdd.TaxType == 'N' ? 'ราคาไม่รวมภาษี' : $scope.PurchaseOrderAdd.TaxType == 'V' ? 'ราคารวมภาษี' : '',
                    TotalAmount: ConvertToDecimal($scope.PurchaseOrderAdd.TotalAmount),
                    IsDiscountHeader: $scope.PurchaseOrderAdd.IsDiscountHeader ? '1' : '0',
                    IsDiscountHeaderType: $scope.PurchaseOrderAdd.IsDiscountHeaderType,
                    DiscountPercent: $scope.PurchaseOrderAdd.DiscountPercent,
                    DiscountAmount: $scope.PurchaseOrderAdd.DiscountAmount,
                    TotalAfterDiscountAmount: $scope.PurchaseOrderAdd.TotalAfterDiscountAmount,
                    IsTaxHeader: $scope.PurchaseOrderAdd.IsTaxHeader ? '1' : '0',
                    IsTaxSummary: $scope.PurchaseOrderAdd.IsTaxSummary ? '1' : '0',
                    ExemptAmount: $scope.PurchaseOrderAdd.ExemptAmount,
                    VatableAmount: $scope.PurchaseOrderAdd.VatableAmount,
                    VAT: $scope.PurchaseOrderAdd.VAT,
                    TotalBeforeVatAmount: $scope.PurchaseOrderAdd.TotalBeforeVatAmount,
                    IsWithholdingTax: $scope.PurchaseOrderAdd.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.PurchaseOrderAdd.IsWithholdingTax ? $scope.PurchaseOrderAdd.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.PurchaseOrderAdd.IsWithholdingTax ? $scope.PurchaseOrderAdd.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.PurchaseOrderAdd.WithholdingAmount,
                    GrandAmountAfterWithholding: $scope.PurchaseOrderAdd.IsWithholdingTax ? $scope.PurchaseOrderAdd.GrandAmountAfterWithholding : undefined,
                    GrandAmount: $scope.PurchaseOrderAdd.GrandAmount,
                    Remark: $scope.PurchaseOrderAdd.Remark,
                    Signature: $scope.PurchaseOrderAdd.Signature ? '1' : '0',
                    Noted: $scope.PurchaseOrderAdd.Noted,
                    DocumentKey: $scope.PurchaseOrderAdd.DocumentKey,
                    DocumentNo: $scope.PurchaseOrderAdd.DocumentNo,
                    DocumentOwner: $scope.PurchaseOrderAdd.DocumentOwner,
                    WithholdingTaxForm: $scope.PurchaseOrderAdd.WithholdingTaxForm,
                    WithholdingTaxFormName: withform,
                    PayerMethod: $scope.PurchaseOrderAdd.PayerMethod,
                    PayerDesc: $scope.PurchaseOrderAdd.PayerDesc,
                    SSONumber: $scope.PurchaseOrderAdd.SSONumber,
                    SSOPrice: $scope.PurchaseOrderAdd.SSOPrice,
                    SSOFundPrice: $scope.PurchaseOrderAdd.SSOFundPrice
                };

                data.PurchaseOrderDetails = [];
                _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {

                    var categoryname;
                    if (item.WithholdingTaxCatagory == '100')
                        categoryname = '1. เงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัส ฯลฯ ตามมาตรา 40 (1)';
                    else if (item.WithholdingTaxCatagory == '200')
                        categoryname = '2.  ค่าธรรมเนียม ค่านายหน้า ฯลฯ ตามมาตรา 40 (2)';
                    else if (item.WithholdingTaxCatagory == '300')
                        categoryname = '3. ค่าแห่งลิขสิทธิ์ ฯลฯ ตามมาตรา 40(3)';
                    else if (item.WithholdingTaxCatagory == '400')
                        categoryname = '4. (ก) ค่าดอกเบี้ย ฯลฯ ตามมาตรา 40(4) (ก)';
                    else if (item.WithholdingTaxCatagory == '411')
                        categoryname = '4. (ข)(1.1) กิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราร้อยละ 25 ของกำไรสุทธิ';
                    else if (item.WithholdingTaxCatagory == '412')
                        categoryname = '4. (ข)(1.2) กิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราร้อยละ 20 ของกำไรสุทธิ';
                    else if (item.WithholdingTaxCatagory == '413')
                        categoryname = '4. (ข)(1.3) กิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราอื่น ของกำไรสุทธิ (กรุณาระบุ)';
                    else if (item.WithholdingTaxCatagory == '421')
                        categoryname = '4. (ข)(2.1) กำไรสุทธิของกิจการที่ได้รับยกเว้นเงินได้นิติบุคคล';
                    else if (item.WithholdingTaxCatagory == '422')
                        categoryname = '4. (ข)(2.2) เงินปันผลหรือเงินส่วนแบ่งของกำไรที่ได้รับยกเว้น...';
                    else if (item.WithholdingTaxCatagory == '423')
                        categoryname = '4. (ข)(2.3) กำไรสุทธิส่วนที่ได้หักผลขาดทุนสิทธิยกมาไม่เกิน 5 ปี...';
                    else if (item.WithholdingTaxCatagory == '424')
                        categoryname = '4. (ข)(2.4) กำไรที่รับรู้ทางบัญชีโดยวิธีส่วนได้เสีย (equity method)';
                    else if (item.WithholdingTaxCatagory == '425')
                        categoryname = '4. (ข)(2.5) อื่นๆ (กรุณาระบุ)';
                    else if (item.WithholdingTaxCatagory == '500')
                        categoryname = '5. การจ่ายเงินได้ที่ต้องหักภาษี ณ ที่จ่ายตามคำสั่งกรมสรรพากร....';
                    else if (item.WithholdingTaxCatagory == '600')
                        categoryname = '6. อื่นๆ (กรุณาระบุ)';

                    var tmpQT = {
                        Discount: ConvertToDecimal(item.Discount),
                        DiscountAfter: ConvertToDecimal(item.DiscountAfter),
                        DiscountPercent: ConvertToDecimal(item.DiscountPercent),
                        DiscountType: item.DiscountType,
                        Name: item.Name,
                        Unit: item.Unit,
                        Description: item.Description,
                        Price: ConvertToDecimal(item.Price),
                        PriceAfterTax: ConvertToDecimal(item.PriceAfterTax),
                        PriceBeforeTax: ConvertToDecimal(item.PriceBeforeTax),
                        PriceTax: ConvertToDecimal(item.PriceTax),
                        ProductKey: parseInt(item.ProductKey),
                        Quantity: ConvertToDecimal(item.Quantity),
                        Sequence: parseInt(item.Sequence),
                        Total: ConvertToDecimal(item.Total),
                        Vat: ConvertToDecimal(item.Vat),
                        VatAfter: ConvertToDecimal(item.VatAfter),
                        VatRate: ConvertToDecimal(item.VatRate),
                        VatType: item.VatType,
                        CatagoryID: item.CatagoryID,
                        WithholdingTaxCatagory: item.WithholdingTaxCatagory,
                        WithholdingTaxCatagoryName: categoryname,
                        TaxPercent: item.TaxPercent,
                        TaxType: item.TaxType,
                        TaxPercentType: item.TaxPercentType
                    };


                    data.PurchaseOrderDetails.push(tmpQT);
                });

                $scope.table.binding = 1;
                $http.post(baseURL + "DocumentExpenses/PostWithholdingTax", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseInvoice = response.data.responsedata;
                                    if (responseInvoice != undefined) {

                                        if (exit == 1)
                                            window.location.href = baseURL + "DocumentExpenses/WithholdingTax";
                                        else {
                                            $scope.PurchaseOrderAdd.PurchaseOrderKey = responseInvoice.PurchaseOrderKey;
                                            $scope.PurchaseOrderAdd.PurchaseOrderStatus = responseInvoice.PurchaseOrderStatus;
                                            $scope.PurchaseOrderAdd.PurchaseOrderStatusName = responseInvoice.PurchaseOrderStatusName;
                                            $scope.PurchaseOrderAdd.CustomerKey = responseInvoice.CustomerKey;

                                            _.each($scope.PurchaseOrderAdd.PurchaseOrderDetails, function (item) {
                                                var detail = _.where(responseInvoice.PurchaseOrderDetails, { Sequence: item.Sequence })[0];
                                                if (detail != null) {
                                                    item.ProductKey = detail.ProductKey;
                                                    item.PurchaseOrderKey = detail.PurchaseOrderKey;
                                                    item.PurchaseOrderDetailKey = detail.PurchaseOrderDetailKey;
                                                }
                                            });

                                            $scope.table.binding = 0;
                                            showSuccessToast();

                                            if ($scope.PurchaseOrderAdd.PurchaseOrderKey != undefined && $scope.Print == "1") {
                                                $('#modal-report').modal('show');
                                                $scope.document = [];
                                                $scope.document.bindding = 1;
                                                $scope.document.DocNo = $scope.PurchaseOrderAdd.PurchaseOrderNo;
                                                try {
                                                    var data = {
                                                        DocumentKey: $scope.PurchaseOrderAdd.PurchaseOrderKey,
                                                        DocumentType: 'WT',
                                                        DocumentNo: $scope.document.DocNo,
                                                        TypeAction: 'Report',
                                                        Content: 'ORIGINAL'
                                                    };
                                                    $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                                                        function (response) {
                                                            try {
                                                                if (response != undefined && response != "") {
                                                                    if (response.data.responsecode == 200) {

                                                                        PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                                                        $scope.document.bindding = 0;
                                                                        $scope.Print = "0";
                                                                    }
                                                                    else {
                                                                        showErrorToast(response.data.errormessage);
                                                                        $scope.document.bindding = 0;
                                                                        $scope.Print = "0";
                                                                    }
                                                                }
                                                            }
                                                            catch (err) {
                                                                showErrorToast(err);
                                                            }
                                                        });
                                                }
                                                catch (err) {
                                                    showWariningToast(err);
                                                }
                                            }
                                            else if ($scope.PurchaseOrderAdd.PurchaseOrderKey != undefined && $scope.Print == "2") {
                                                $scope.table.binding = 1;
                                                try {
                                                    var data = {
                                                        DocumentKey: $scope.PurchaseOrderAdd.PurchaseOrderKey,
                                                        DocumentType: 'WT',
                                                        DocumentNo: $scope.PurchaseOrderAdd.PurchaseOrderNo,
                                                        TypeAction: 'Download',
                                                        Content: 'ORIGINAL'
                                                    };
                                                    $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                                                        function (response) {
                                                            try {
                                                                if (response != undefined && response != "") {
                                                                    if (response.data.responsecode == 200) {
                                                                        showSuccessText('ดาวน์โหลดสำเร็จ');
                                                                        window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                                                        $scope.table.binding = 0;
                                                                    }
                                                                    else {
                                                                        showErrorToast(response.data.errormessage);
                                                                        $scope.table.binding = 0;
                                                                    }
                                                                }
                                                            }
                                                            catch (err) {
                                                                showErrorToast(err);
                                                            }
                                                        });
                                                }
                                                catch (err) {
                                                    showWariningToast(err);
                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    $scope.table.binding = 0;
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };


    // #region contactbook

    /**************Select ContactBook **************/
    $scope.OnClearContact = function () {
        $scope.PurchaseOrderAdd.CustomerKey = $scope.PurchaseOrderAdd.CustomerName = $scope.PurchaseOrderAdd.CustomerTaxID =
            $scope.PurchaseOrderAdd.CustomerContactName = $scope.PurchaseOrderAdd.CustomerContactPhone = $scope.PurchaseOrderAdd.CustomerContactEmail =
            $scope.PurchaseOrderAdd.CustomerAddress = $scope.PurchaseOrderAdd.CustomerBranch = $scope.PurchaseOrderAdd.CreditDay = undefined;
    };

    $scope.OnClickContactPopup = function () {
        $scope.SearchContact.InputFilter = [];
        $scope.contactcurrentPage = 0;
        $scope.contact = [];
        $scope.contact.binding = 0;
        $scope.contactretpage = [];
        $scope.contactrange();
        $('#modalcontact-list').modal('show');
    };

    $scope.OnClickContactSearch = function () {
        $scope.Parameter.Param_Contact = contactService.filterContact($scope.SearchContact.InputFilter, $scope.Parameter.Param_Contact, $scope.Parameter.Param_ContactMain);
        if ($scope.Parameter.Param_Contact == undefined)
            $scope.Parameter.Param_Contact = [];
    };

    $scope.OnClickContactSelection = function (contactkey) {
        var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: contactkey })[0];
        if (contact != undefined) {
            $scope.OnClearContact();
            $scope.PurchaseOrderAdd.CustomerKey = contact.ContactKey;
            $scope.PurchaseOrderAdd.CustomerName = contact.BusinessName;
            $scope.PurchaseOrderAdd.CustomerTaxID = contact.TaxID;
            $scope.PurchaseOrderAdd.CustomerContactName = contact.ContactName;
            $scope.PurchaseOrderAdd.CustomerContactPhone = contact.ContactMobile;
            $scope.PurchaseOrderAdd.CustomerContactEmail = contact.ContactEmail;
            $scope.PurchaseOrderAdd.CustomerAddress = contact.Address;
            $scope.PurchaseOrderAdd.CustomerBranch = contact.BranchName;
            $scope.PurchaseOrderAdd.CreditDay = contact.CreditDate;
            $scope.OnChangeCredit();
            $('#modalcontact-list').modal('hide');
        }
    };

    $scope.OnClickContactAdd = function () {
        $scope.BusinessContactAdd = [];
        $scope.BusinessContactAdd.BusinessType = 'C';
        $scope.BusinessContactAdd.ContactType1 = false;
        $scope.BusinessContactAdd.ContactType2 = true;
        $scope.BusinessContactAdd.BranchType = "H";
        $scope.BusinessContactAdd.AccountType = 'S';

        var type = $scope.Parameter.Param_Quotation.Param_Bank.filter(function (item) { return item.ID == undefined; });
        if (type.length > 0)
            $scope.BusinessContactAdd.SelectBank = type[0];

        $('#modalcontact-add').modal('show');
    };

    $scope.OnClickContactSave = function () {
        try {
            if ($scope.BusinessContactAdd.BusinessName == undefined || $scope.BusinessContactAdd.BusinessName == "")
                throw "กรุณาระบุชื่อธุรกิจ";
            else if ($scope.BusinessContactAdd.ContactType1 == false && $scope.BusinessContactAdd.ContactType2 == false)
                throw "กรุณาระบุประเภท";
            else if ($scope.BusinessContactAdd.ContactEmail != undefined && $scope.BusinessContactAdd.ContactEmail != "" && !ValidateEmail($scope.BusinessContactAdd.ContactEmail))
                throw "กรุณาแก้ไขอีเมล เนื่องจากรูปแบบอีเมลไม่ถูกต้อง";
            else {
                var qq = $q.all([contactService.postNewContact($scope.BusinessContactAdd)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var contact = data[0].data.responsedata;
                                if (contact != undefined) {
                                    $scope.OnClearContact();
                                    $scope.PurchaseOrderAdd.CustomerKey = contact.ContactKey;
                                    $scope.PurchaseOrderAdd.CustomerName = contact.BusinessName;
                                    $scope.PurchaseOrderAdd.CustomerTaxID = contact.TaxID;
                                    $scope.PurchaseOrderAdd.CustomerContactName = contact.ContactName;
                                    $scope.PurchaseOrderAdd.CustomerContactPhone = contact.ContactMobile;
                                    $scope.PurchaseOrderAdd.CustomerContactEmail = contact.ContactEmail;
                                    $scope.PurchaseOrderAdd.CustomerAddress = contact.Address;
                                    $scope.PurchaseOrderAdd.CustomerBranch = contact.BranchName;
                                    $scope.PurchaseOrderAdd.CreditDay = contact.CreditDate;
                                    $scope.OnChangeCredit();
                                }
                                $('#modalcontact-add').modal('hide');
                                showSuccessToast();

                                var q1 = $q.all([paramService.getCustomer()]).then(function (data) {
                                    if (data[0] != undefined && data[0] != "") {
                                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                            $scope.SearchContact = [];
                                            $scope.Parameter.Param_Contact = [];
                                            $scope.Parameter.Param_ContactMain = [];
                                            $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[0].data.responsedata;
                                        }
                                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                    }
                                });

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(response.data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.contactcurrentPage = 0;

    $scope.contactLimitFirst = 0;
    $scope.contactLimitPage = 5;
    $scope.contactitemsPerPage = 10;

    $scope.contactpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Contact.length / $scope.contactitemsPerPage) - 1;
    };

    $scope.contactrange = function () {
        $scope.contactitemsCount = $scope.Parameter.Param_Contact.length;
        $scope.contactpageshow = $scope.contactpageCount() > $scope.contactLimitPage && $scope.contactpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.contactpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.contactpageshowdata = 1;
        $scope.contactretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.contactpcontactPage = function () {
        if ($scope.contactcurrentPage > 0) {
            $scope.contactcurrentPage--;
        }

        if ($scope.contactcurrentPage < $scope.contactLimitFirst && $scope.contactcurrentPage >= 1) {
            $scope.contactLimitFirst = $scope.contactLimitFirst - 5;
            $scope.contactLimitPage = $scope.contactLimitPage - 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }
    };

    $scope.contactpcontactPageDisabled = function () {
        return $scope.contactcurrentPage === 0 ? "disabled" : "";
    };

    $scope.contactnextPage = function () {
        if ($scope.contactcurrentPage < $scope.contactpageCount()) {
            $scope.contactcurrentPage++;
        }

        if ($scope.contactcurrentPage >= $scope.contactLimitPage && $scope.contactcurrentPage <= $scope.contactpageCount()) {
            $scope.contactLimitFirst = $scope.contactLimitFirst + 5;
            $scope.contactLimitPage = $scope.contactLimitPage + 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }

    };

    $scope.contactnextPageDisabled = function () {
        return $scope.contactcurrentPage === $scope.contactpageCount() ? "disabled" : "";
    };

    $scope.contactsetPage = function (n) {
        $scope.contactcurrentPage = n;
    };


    // #endregion


    // #region Revenue

    /**************Select Revenue **************/

    $scope.OnClickRevenuePopup = function () {
        $scope.SearchRevenue = [];
        $scope.Parameter.Param_Revenue = [];
        $scope.revenue = [];
        $scope.revenue.binding = 0;
        $scope.revitemsCount = 0;
        $('#modalrevenute-list').modal('show');
    };

    $scope.OnClickFilterRevenue = function () {
        if ($scope.SearchRevenue.InputText != undefined && $scope.SearchRevenue.InputText != "") {
            $scope.revenue.binding = 1;
            var qq = $q.all([contactService.filterRevenue($scope.SearchRevenue.InputText)]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == 200) {
                            if (data[0].data.responsedata == undefined)
                                $scope.Parameter.Param_Revenue = [];
                            else {
                                $scope.Parameter.Param_Revenue = data[0].data.responsedata;
                                $scope.revretpage = [];
                                $scope.revrange();
                                $scope.revenue.binding = 0;
                            }
                        }
                        else {
                            $scope.revenue.binding = 0;
                            showErrorToast(data[0].data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(response.data.errormessage);
                }
            });
        }
    };

    $scope.OnClickRevenueSelection = function (index) {
        try {
            var revenue = angular.copy($scope.Parameter.Param_Revenue[index]);
            $scope.OnClearContact();
            $scope.PurchaseOrderAdd.CustomerName = revenue.CompanyName;
            $scope.PurchaseOrderAdd.CustomerTaxID = revenue.NID;
            $scope.PurchaseOrderAdd.CustomerAddress = revenue.Address;
            $scope.PurchaseOrderAdd.CustomerBranch = revenue.BranchNoName;
            $scope.PurchaseOrderAdd.CreditDay = 0;
            $('#modalrevenute-list').modal('hide');
        }
        catch (err) {
            showErrorToast(response.data.errormessage);
        }
    };

    $scope.revcurrentPage = 0;

    $scope.revLimitFirst = 0;
    $scope.revLimitPage = 5;
    $scope.revitemsPerPage = 10;

    $scope.revpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Revenue.length / $scope.revitemsPerPage) - 1;
    };

    $scope.revrange = function () {
        $scope.revitemsCount = $scope.Parameter.Param_Revenue.length;
        $scope.revpageshow = $scope.revpageCount() > $scope.revLimitPage && $scope.revpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.revpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.revpageshowdata = 1;
        $scope.revretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.revprevPage = function () {
        if ($scope.revcurrentPage > 0) {
            $scope.revcurrentPage--;
        }

        if ($scope.revcurrentPage < $scope.revLimitFirst && $scope.revcurrentPage >= 1) {
            $scope.revLimitFirst = $scope.revLimitFirst - 5;
            $scope.revLimitPage = $scope.revLimitPage - 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }
    };

    $scope.revprevPageDisabled = function () {
        return $scope.revcurrentPage === 0 ? "disabled" : "";
    };

    $scope.revnextPage = function () {
        if ($scope.revcurrentPage < $scope.revpageCount()) {
            $scope.revcurrentPage++;
        }

        if ($scope.revcurrentPage >= $scope.revLimitPage && $scope.revcurrentPage <= $scope.revpageCount()) {
            $scope.revLimitFirst = $scope.revLimitFirst + 5;
            $scope.revLimitPage = $scope.revLimitPage + 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }

    };

    $scope.revnextPageDisabled = function () {
        return $scope.revcurrentPage === $scope.revpageCount() ? "disabled" : "";
    };

    $scope.revsetPage = function (n) {
        $scope.revcurrentPage = n;
    };

    // #endregion


    // #region product

    /**************Select Product **************/

    $scope.OnClickProductPopup = function () {
        $scope.SearchProduct.InputFilter = [];
        $scope.productcurrentPage = 0;
        $scope.product = [];
        $scope.product.binding = 0;
        $scope.productretpage = [];
        $scope.productrange();
        $('#modalproduct-list').modal('show');
    };

    $scope.OnClickProductSearch = function () {
        $scope.Parameter.Param_Product = productService.filterProduct($scope.SearchProduct.InputFilter, $scope.Parameter.Param_Product, $scope.Parameter.Param_ProductMain);
        if ($scope.Parameter.Param_Product == undefined)
            $scope.Parameter.Param_Product = [];
    };

    $scope.OnClickProductSelection = function (productkey) {
        var product = _.where($scope.Parameter.Param_Product, { ProductKey: productkey })[0];
        if (product != undefined) {
            $scope.OnClickClearDetail();
            $scope.ProductDetailAdd.ProductKey = product.ProductKey;
            $scope.ProductDetailAdd.Name = product.ProductName;
            $scope.ProductDetailAdd.Description = product.ProductDescription;
            $scope.ProductDetailAdd.Quantity = '1.00';
            $scope.ProductDetailAdd.Unit = product.UnitTypeName;
            $scope.ProductDetailAdd.DiscountType = $scope.PurchaseOrderAdd.IsDiscountHeaderType;
            $scope.ProductDetailAdd.DiscountPercent = '0.00';
            $scope.ProductDetailAdd.Discount = '0.00';
            $scope.ProductDetailAdd.VatType = '2';

            if (product.SellTaxType == '1' || product.SellTaxType == '3' || product.SellTaxType == '4') {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var sell = 0, selltax = 0, selllast = 0;

                if (product.ProductType == 'P')
                    sell = ConvertToDecimal(product.BuyPrice == undefined ? 0 : product.BuyPrice);
                else
                    sell = ConvertToDecimal(product.SellPrice == undefined ? 0 : product.SellPrice);

                selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                selllast = sell + selltax;



                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(selllast, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(selltax, 2);

                if (product.SellTaxType == '3') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '1';
                }
                else if (product.SellTaxType == '4') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '3';
                }
            }
            else if (product.SellTaxType == '2') {

                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(product.BuyAfterTaxPrice, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(product.BuyPrice, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(product.BuyTaxPrice, 2);
            }

            if ($scope.PurchaseOrderAdd.TaxType == 'N')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
            else if ($scope.PurchaseOrderAdd.TaxType == 'V')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

            $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $('#modalproduct-list').modal('hide');
        }
    };



    $scope.OnClickProductAdd = function () {
        $scope.BusinessProductAdd = [];
        $scope.BusinessProductAdd.ProductType = 'S';
        $scope.BusinessProductAdd.SellTaxType = '1';
        $scope.BusinessProductAdd.BuyTaxType = '1';
        initdropify('');

        $('#modalproduct-add').modal('show');
    };

    function initdropify(path) {
        $("#product_thumnail").addClass('dropify');
        var publicpath_identity_picture = path;

        var drEvent = $('.dropify').dropify();
        drEvent.on('dropify.afterClear', function (event, element) {
            $scope.DeleteImages = 1;
        });
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = publicpath_identity_picture;
        drEvent.destroy();

        drEvent.init();

        $('.dropify#identity_picture').dropify({
            defaultFile: publicpath_identity_picture
        });

        $('.dropify').dropify();
    }

    $scope.OnClickProductSave = function () {
        try {
            if ($scope.BusinessProductAdd.ProductName == undefined || $scope.BusinessProductAdd.ProductName == "")
                throw "กรุณาระบุชื่อสินค้า/บริการ";
            else {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var unit = document.getElementById('input_unittype').value;
                var category = document.getElementById('input_categorytype').value;


                var qq = $q.all([productService.postNewProduct($scope.BusinessProductAdd, vat, unit, category)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var product = data[0].data.responsedata;

                                if (product != undefined) {
                                    //$scope.ProductDetailAdd.ProductKey = product.ProductKey;
                                    //$scope.ProductDetailAdd.Name = product.ProductName;
                                    //$scope.ProductDetailAdd.Description = product.ProductDescription;
                                    //$scope.ProductDetailAdd.Quantity = '1.00';
                                    //$scope.ProductDetailAdd.Unit = product.UnitTypeName;
                                    //$scope.ProductDetailAdd.Price = AFormatNumber($scope.PurchaseOrderAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);
                                    //$scope.ProductDetailAdd.Total = AFormatNumber($scope.PurchaseOrderAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);

                                    if ($scope.SelectedFiles != undefined && $scope.Upload == 1) {
                                        try {

                                            var input = document.getElementById("product_thumnail");
                                            var files = input.files;
                                            var formData = new FormData();

                                            for (var i = 0; i != files.length; i++) {
                                                formData.append("files", files[i]);
                                            }

                                            $.ajax(
                                                {
                                                    url: baseURL + "Product/UploadPictureProducts?productkey=" + product.ProductKey,
                                                    data: formData,
                                                    processData: false,
                                                    contentType: false,
                                                    type: "POST",
                                                    success: function (data) {
                                                        $('#modalproduct-add').modal('hide');
                                                        showSuccessToast();
                                                    },
                                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                        showErrorToast(textStatus);
                                                    }
                                                }
                                            );
                                        }
                                        catch (ex) {
                                            showErrorToast(ex);
                                        }
                                    }
                                    else {
                                        $('#modalproduct-add').modal('hide');
                                        showSuccessToast();
                                    }


                                    var q1 = $q.all([paramService.getProduct()]).then(function (data) {
                                        if (data[0] != undefined && data[0] != "") {
                                            if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                                $scope.SearchProduct = [];
                                                $scope.Parameter.Param_Product = [];
                                                $scope.Parameter.Param_ProductMain = [];

                                                $scope.Parameter.Param_ProductMain = data[0].data.responsedata;

                                                _.each($scope.Parameter.Param_ProductMain, function (item) {
                                                    item.SellPrice = AFormatNumber(item.SellPrice, 2);
                                                    item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                                                    item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                                                    item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                                                    item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                                                    item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                                                });

                                                $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;

                                                OnClickProductSelection(product.ProductKey);
                                            }
                                            else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                        }
                                    });
                                }

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(data[0].data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.productcurrentPage = 0;

    $scope.productLimitFirst = 0;
    $scope.productLimitPage = 5;
    $scope.productitemsPerPage = 10;

    $scope.productpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Product.length / $scope.productitemsPerPage) - 1;
    };

    $scope.productrange = function () {
        $scope.productitemsCount = $scope.Parameter.Param_Product.length;
        $scope.productpageshow = $scope.productpageCount() > $scope.productLimitPage && $scope.productpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.productpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.productpageshowdata = 1;
        $scope.productretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.productpproductPage = function () {
        if ($scope.productcurrentPage > 0) {
            $scope.productcurrentPage--;
        }

        if ($scope.productcurrentPage < $scope.productLimitFirst && $scope.productcurrentPage >= 1) {
            $scope.productLimitFirst = $scope.productLimitFirst - 5;
            $scope.productLimitPage = $scope.productLimitPage - 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }
    };

    $scope.productpproductPageDisabled = function () {
        return $scope.productcurrentPage === 0 ? "disabled" : "";
    };

    $scope.productnextPage = function () {
        if ($scope.productcurrentPage < $scope.productpageCount()) {
            $scope.productcurrentPage++;
        }

        if ($scope.productcurrentPage >= $scope.productLimitPage && $scope.productcurrentPage <= $scope.productpageCount()) {
            $scope.productLimitFirst = $scope.productLimitFirst + 5;
            $scope.productLimitPage = $scope.productLimitPage + 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }

    };

    $scope.productnextPageDisabled = function () {
        return $scope.productcurrentPage === $scope.productpageCount() ? "disabled" : "";
    };

    $scope.productsetPage = function (n) {
        $scope.productcurrentPage = n;
    };

    $scope.UploadFiles = function (files) {
        $scope.Upload = 1;
        $scope.SelectedFiles = files;

        // var input = document.getElementById("tax_thumnail");
        // var a = input.files;
    };

    // #endregion


    // #region Payment Popup
    $scope.OnClickPaymentAdd = function () {

        var filter = $scope.PurchaseOrderAdd;// _.where($scope.PurchaseOrderAdd, { PurchaseOrderKey: $scope.PurchaseOrderAdd.PurchaseOrderKey })[0];
        if (filter != undefined) {
            $scope.ReceiptAdd = [];
            $scope.ReceiptAdd = angular.copy(filter);

            $scope.ReceiptAdd.ReceiptInfo = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push({ ID: '0', Name: 'จำนวนเงิน', Rate: 0 });
            _.each($scope.Parameter.Param_Quotation.Param_WithholdingRate, function (item) { $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push(item); });


            $scope.ReceiptAdd.ReceiptInfo.DocumentKey = $scope.ReceiptAdd.PurchaseOrderKey;
            $scope.ReceiptAdd.ReceiptInfo.DocumentNo = $scope.ReceiptAdd.PurchaseOrderNo;
            $scope.ReceiptAdd.ReceiptInfo.DocumentType = headerdoc;
            $scope.ReceiptAdd.ReceiptInfo.CustomerName = $scope.ReceiptAdd.CustomerName;
            $scope.ReceiptAdd.ReceiptInfo.PaymentAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            $scope.ReceiptAdd.ReceiptInfo.PaymentDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            //$scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = $scope.ReceiptAdd.IsAdjustDiscount || $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.PaymentAmount, 2) : AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            $scope.ReceiptAdd.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax;
            // $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = '0';
            if ($scope.ReceiptAdd.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.WithholdingKey; });
                if (type.length > 0)
                    $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[1];

                $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.WithholdingAmount, 2) : AFormatNumber(0, 2);
            }

            $scope.ReceiptAdd.IsAdjustDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.IsDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.DiscountType = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined;
            $scope.ReceiptAdd.ReceiptInfo.DiscountTypeName = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountTypeName : undefined;
            $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : AFormatNumber(0, 2);
            $scope.ReceiptAdd.ReceiptInfo.PaymentType = "CA";
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(0, 2);
            $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
            $scope.ReceiptAdd.ReceiptInfo.IsPayment = true;

            $('#modalreceipt-receive').modal('show');
        }
    };

    $scope.OnClickChangePaymentAmount = function () {
        $scope.ReceiptAdd.ReceiptInfo.IsPayment = false
        $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = '0';
        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    }

    $scope.OnChangeIsAdjustReceive = function () {
        $scope.ReceiptAdd.ReceiptInfo.DiscountType = "1";
        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = AFormatNumber(0, 2);
    };

    $scope.OnChangeFee = function () {
        $scope.ReceiptAdd.ReceiptInfo.Fee = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.Fee, 2);
    }

    $scope.OnChangeIsWithhollding = function () {

        var type = [];
        if ($scope.ReceiptAdd.ReceiptInfo.IsPayment)
            type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '1'; });
        else
            type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });

        if (type.length > 0)
            $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0].ID;
        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    };

    $scope.OnChangeWithholldingReceipt = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding == '0') {
            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            $scope.OnChangeReceiveCalculate();
        }
        else {
            $scope.OnChangeReceiveCalculate();
        }
    };

    $scope.OnChangePaymentType = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR') {
            var type = $scope.Parameter.BizBank.filter(function (item) { return item.UID == $scope.Parameter.BizBank[0].UID; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer = type[0];
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
            $scope.ReceiptAdd.ReceiptInfo.ChequeDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiptInfo.ChequeNo = '';
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
        }
    };

    $scope.OnChangeReceiveCalculate = function () {
        try {
            var grandtotal = 0, netpayment = 0, discount = 0, withholding = 0, netgrand = 0, pricediscount = 0, pricevat, balance = 0;
            grandtotal = $scope.ReceiptAdd.ReceiptInfo.PaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.PaymentAmount); // Discount Percenter
            pricediscount = $scope.ReceiptAdd.TotalAfterDiscountAmount == undefined || $scope.ReceiptAdd.TotalAfterDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalAfterDiscountAmount); // Discount Percenter
            pricevat = $scope.ReceiptAdd.TotalBeforeVatAmount == undefined || $scope.ReceiptAdd.TotalBeforeVatAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalBeforeVatAmount); // Discount Percenter
            netpayment = $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount); // Discount Percenter
            discount = $scope.ReceiptAdd.ReceiptInfo.DiscountAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.DiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.DiscountAmount); // Discount Percenter
            withholding = $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.WithholdingAmount); // Discount Percenter


            if ($scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.ReceiptInfo.SelectWithholding; });
                if (type.length > 0)
                    withholdingrate = type[0].Rate;

                if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding == '0')
                    withholding = withholding;
                else {
                    if ($scope.ReceiptAdd.TaxType == 'N')
                        withholding = pricediscount * withholdingrate / 100;
                    else if ($scope.ReceiptAdd.TaxType == 'V')
                        withholding = pricevat * withholdingrate / 100;
                }

                grandtotal = grandtotal - withholding;
            }
            else
                grandtotal = grandtotal;

            grandtotal = grandtotal - discount;

            if (!$scope.ReceiptAdd.ReceiptInfo.IsPayment) {
                balance = netpayment - grandtotal;
            }
            else
                netpayment = grandtotal;

            if (balance < 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินขาด';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Lost';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
                balance = balance * (-1);
            }
            else if (balance > 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินเกิน';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Over';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }
            else {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
                $scope.ReceiptAdd.ReceiptInfo.IsText = '';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Equal';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }

            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(withholding, 2);//
            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber(netpayment, 2);//
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(balance, 2);//
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickReceiptPayment = function () {
        try {

            var adjusttype;
            if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '1')
                adjusttype = 'ส่วนลดพิเศษ';
            else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '2')
                adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
            else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '3')
                adjusttype = 'ค่าดำเนินการ';
            else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '4')
                adjusttype = 'ปัดเศษ';

            var paymenttype;
            if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CA')
                paymenttype = 'เงินสด';
            else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR')
                paymenttype = 'โอนเงิน';
            else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ')
                paymenttype = 'เช็ค';
            else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC')
                paymenttype = 'บัตรเครดิต';

            var cause;
            if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '1')
                cause = 'ค่าธรรมเนียม';
            else if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '2')
                cause = 'เงินขาด/เงินเกิน';

            var data = {
                DocumentKey: $scope.ReceiptAdd.PurchaseOrderKey,
                DocumentNo: $scope.ReceiptAdd.PurchaseOrderNo,
                DocumentType: headerdoc,
                CustomerName: $scope.ReceiptAdd.ReceiptInfo.CustomerName,
                PaymentAmount: $scope.ReceiptAdd.ReceiptInfo.PaymentAmount,
                PaymentDate: ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.PaymentDate),
                NetPaymentAmount: ConvertToDecimal($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount),
                IsWithholdingTax: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? '1' : '0',
                WithholdingKey: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID : undefined,
                WithholdingRate: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.Rate : undefined,
                WithholdingAmount: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount : undefined,
                IsDiscount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? '1' : '0',
                DiscountType: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountType : undefined,
                DiscountTypeName: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? adjusttype : undefined,
                DiscountAmount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountAmount : undefined,
                PaymentType: $scope.ReceiptAdd.ReceiptInfo.PaymentType,
                PaymentTypeName: paymenttype,
                BankAccountID: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.UID : undefined,
                BankCode: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank.ID : undefined,
                BankName: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.BankName : $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' || $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank.Name : undefined,
                BankAccountNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.AccountNo : undefined,
                BankAccountType: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.DepositTypeName : undefined,
                ChequeDate: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.ChequeDate) : undefined,
                ChequeNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.ChequeNo : undefined,
                Comment: $scope.ReceiptAdd.ReceiptInfo.Comment,
                Balance: $scope.ReceiptAdd.ReceiptInfo.Balance,
                BalanceType: $scope.ReceiptAdd.ReceiptInfo.BalanceType,
                CauseType: $scope.ReceiptAdd.ReceiptInfo.CauseType,
                CauseTypeName: cause,
                PaymentFee: $scope.ReceiptAdd.ReceiptInfo.Fee
            };

            $http.post(baseURL + "DocumentExpenses/PostBusinessReceiptPayment", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessToast();
                                $('#modalreceipt-receive').modal('hide');
                                $scope.init();
                                $scope.ReceiptAdd = [];
                            }
                            else showErrorToast(response.data.errormessage);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    catch (err) {
                        $scope.table.binding = 0;
                        showErrorToast(err);
                    }
                });

        }
        catch (err) { showErrorToast(err); }
    };
    // #endregion


    // #region ReceiveTax
    function SaveReceiptTax() {
        if ($scope.RTaxAdd.InvoiceNo == undefined || $scope.RTaxAdd.InvoiceNo == "")
            showErrorToast("กรุณากรอกข้อมูล เลขที่ใบกำกับภาษี ");
        else if ($scope.RTaxAdd.InvoiceDate == undefined || $scope.RTaxAdd.InvoiceDate == "")
            showErrorToast("กรุณากรอกข้อมูล วันที่ใบกำกับภาษี");
        else if ($scope.RTaxAdd.SupplierName == undefined || $scope.RTaxAdd.SupplierName == "")
            showErrorToast("กรุณากรอกข้อมูล ชื่อผู้จำหน่าย");
        else if ($scope.RTaxAdd.SupplierTaxID == undefined || $scope.RTaxAdd.SupplierTaxID == "")
            showErrorToast("กรุณากรอกข้อมูล เลขประจำตัวผู้เสียภาษี");
        else if ($scope.RTaxAdd.SupplierBranch == undefined || $scope.RTaxAdd.SupplierBranch == "")
            showErrorToast("กรุณากรอกข้อมูล สำนักงาน/สาขา");
        else {
            var data = {
                DocumentKey: $scope.PurchaseOrderAdd.PurchaseOrderKey,
                DocumentNo: $scope.PurchaseOrderAdd.PurchaseOrderNo,
                DocumentType: "RI",
                InvoiceNo: $scope.RTaxAdd.InvoiceNo,
                InvoiceDate: ToJsonDate2($scope.RTaxAdd.InvoiceDate),
                SupplierName: $scope.RTaxAdd.SupplierName,
                SupplierTaxID: $scope.RTaxAdd.SupplierTaxID,
                SupplierBranch: $scope.RTaxAdd.SupplierBranch,
                DocumentPath: $scope.RTaxAdd.DocumentPath,
                ThumbnailDocument: $scope.RTaxAdd.ThumbnailDocument,
                isdelete: 1
            };
            $scope.table.binding = 1;

            $http.post(baseURL + "DocumentExpenses/PostReceiptTax", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessToast();
                                $('#modaltax-receive').modal('hide');
                                $scope.init();
                                $scope.RTaxAdd = {};
                            }
                            else showErrorToast(response.data.errormessage);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    catch (err) {
                        $scope.table.binding = 0;
                        showErrorToast(err);
                    }
                });
        }
    }

    $scope.OnClickSaveReceiptTax = function () {

        if ($scope.SelectedFiles != undefined && $scope.Upload == 1) {
            var input = document.getElementById("tax_thumnail");
            var files = input.files;
            var formData = new FormData();

            for (var i = 0; i != files.length; i++) {
                formData.append("files", files[i]);
            }

            $.ajax(
                {
                    url: baseURL + "DocumentExpenses/UploadTax",
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    success: function (d) {
                        $scope.RTaxAdd.DocumentPath = d.fullname;
                        $scope.RTaxAdd.ThumbnailDocument = d.filename;
                        SaveReceiptTax()
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        showErrorToast(textStatus);
                    }
                }
            );
        }
        else
            SaveReceiptTax()
    }

    $scope.OnClickTaxReceiptAdd = function () {
        var tmp = $scope.PurchaseOrderAdd;// _.where($scope.PurchaseOrderAdd, { PurchaseOrderKey: $scope.PurchaseOrderAdd.PurchaseOrderKey })[0];

        $scope.RTaxAdd = {};
        $scope.RTaxAdd.InvoiceNo = "";
        $scope.RTaxAdd.InvoiceDate = GetDatetimeNow();
        $scope.RTaxAdd.SupplierName = tmp.CustomerName
        $scope.RTaxAdd.SupplierTaxID = tmp.CustomerTaxID;

        if (tmp.CustomerBranch == undefined || tmp.CustomerBranch == "")
            $scope.RTaxAdd.SupplierBranch = "0000";
        else
            $scope.RTaxAdd.SupplierBranch = tmp.CustomerBranch;

        $scope.RTaxAdd.DocumentPath = "";
        $scope.RTaxAdd.ThumbnailDocument = "";

        $("#tax_thumnail").addClass('dropify');
        $("#tax_thumnail").removeAttr('data-default-file');//.attr("data-default-file", imgURL);
        $('.dropify').dropify();

        $('.dropify-clear').click();

        $('#modaltax-receive').modal('show');
    };

    $scope.OnClickTaxReceiptEdit = function () {
        $scope.RTaxAdd = $scope.PurchaseOrderAdd.ReceiveTaxInfo;



        $('#modaltax-receive').modal('show');
    }

    $scope.OnClickTaxReceiptDelete = function () { $('#modalPO-del').modal('show'); }

    $scope.OnClickTaxReceiptCancelDelete = function () { $('#modalPO-del').modal('hide'); }

    $scope.OnClickTaxReceiptConfirmDelete = function () {
        var data = {
            DocumentKey: $scope.PurchaseOrderAdd.PurchaseOrderKey,
            DocumentNo: $scope.PurchaseOrderAdd.PurchaseOrderNo,
            DocumentType: "RI",
            isdelete: 0
        };
        $scope.table.binding = 1;

        $http.post(baseURL + "DocumentExpenses/PostdeleteReceiptTax", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showSuccessToast();
                            $('#modalPO-del').modal('hide');
                            $scope.init();
                            $scope.RTaxAdd = {};
                        }
                        else {
                            $scope.table.binding = 0;
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });
    }

    $scope.CloseModal = function (name) { $(name).modal('hide'); }
    // #endregion

    $scope.OnLoadAttach = function () {
        try {
            $http.get(baseURL + "DocumentSell/GetDocumentAttach?documentkey=" + $scope.PurchaseOrderAdd.PurchaseOrderKey + "&documenttype=" + headerdoc + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.DocumentAttach = [];
                            $scope.DocumentAttach = response.data.responsedata;
                        }
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnUploadAttach = function (files) {
        try {
            if ($scope.PurchaseOrderAdd.PurchaseOrderKey == undefined || $scope.PurchaseOrderAdd.PurchaseOrderKey == "")
                throw "กรุณากดปุ่ม บันทึก ก่อนแนบไฟล์เอกสาร";
            else if ($scope.DocumentAttach.length > 3)
                throw "สามารถแนบไฟล์สูงสุดเพียง 3 ไฟล์ เท่านั้น";
            else if (files.length > 0) {
                var data = {
                    DocumentKey: $scope.PurchaseOrderAdd.PurchaseOrderKey,
                    DocumentNo: $scope.PurchaseOrderAdd.PurchaseOrderNo,
                    DocumentType: headerdoc,
                    AttachName: files[0].name,
                    AttachExtension: getFileExtension(files[0].name)
                };
                $http.post(baseURL + "DocumentSell/PostDocumentAttach", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    try {
                                        var attachfile = response.data.responsedata;
                                        var input = document.getElementById("document_attach");
                                        var files = input.files;
                                        var formData = new FormData();

                                        for (var i = 0; i != files.length; i++) {
                                            formData.append("files", files[i]);
                                        }

                                        $.ajax(
                                            {
                                                url: baseURL + "DocumentSell/UploadDocumentAttach?attachkey=" + attachfile.AttachKey,
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                type: "POST",
                                                success: function (data) {

                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();

                                                    $scope.OnLoadAttach();
                                                    showSuccessText('แนบไฟล์สำเร็จ');
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();
                                                    showErrorToast(textStatus);
                                                }
                                            }
                                        );
                                    }
                                    catch (ex) {
                                        var drEvent = $('.dropify').dropify();
                                        drEvent = drEvent.data('dropify');
                                        drEvent.resetPreview();
                                        drEvent.clearElement();
                                        drEvent.destroy();

                                        drEvent.init();
                                        showErrorToast(ex);
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            var drEvent = $('.dropify').dropify();
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.destroy();

            drEvent.init();
            showWariningToast(err);
        }
    };

    $scope.OnClickDeleteAttach = function (attachkey) {
        var data = {
            DocumentKey: $scope.PurchaseOrderAdd.PurchaseOrderKey,
            DocumentNo: $scope.PurchaseOrderAdd.PurchaseOrderNo,
            DocumentType: headerdoc,
            AttachKey: attachkey
        };
        $http.post(baseURL + "DocumentSell/DeleteDocumentAttach", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            try {
                                showDeleteSuccessToast();
                                $scope.OnLoadAttach();
                            }
                            catch (err) {
                                showErrorToast(err);
                            }
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickShowAttach = function (attachkey) {
        var attactfile = _.where($scope.DocumentAttach, { AttachKey: attachkey })[0];
        if (attactfile != undefined) {
            $scope.DocumentAttachView = [];
            $scope.DocumentAttachView = attactfile;

            if (attactfile.AttachExtension == 'pdf') {
                $scope.DocumentAttachView.AttachHeight = '850px';
                PDFObject.embed(baseURL + 'uploads/attach/' + attactfile.AttachPath, "#exampleattach");
            }
            else {
                $scope.DocumentAttachView.AttachHeight = 'auto';
                $scope.DocumentAttachView.AttachShow = baseURL + 'uploads/attach/' + attactfile.AttachPath;
            }
            $('#modal-attach').modal('show');
        }
    };
});