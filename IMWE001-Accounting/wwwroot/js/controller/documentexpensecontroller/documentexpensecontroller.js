﻿WEACCTAPP.controller('documentexpensecontroller', function ($scope, $http, $timeout, GlobalVar, paramService, $q) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'PV';

    $scope.PurchaseOrder = [];
    $scope.PurchaseOrderMain = [];

    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];
    $scope.BusinessProfile = [];
    $scope.BusinessEmployee = [];
    $scope.EmailAdd = [];

    var gEmp = paramService.getProfile();
    var gProfile = paramService.getBusinessProfile();
    //DocumentExpenses / ExpensesList

    var gParamQuotation = paramService.getParameterQuotation();
    var gBizBank = paramService.getBankBusiness();


    var DocumentAddURL = "DocumentExpenses/ExpensesAdd";


    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
            $scope.Search.InputToDate = GetDatetimeNowLastMonth();

            var qq1 = $q.all([gProfile, gEmp]).then(function (data) {
                try {
                    /****** GetDocument NO ******/
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BusinessProfile = data[0].data.responsedata; }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Customer ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.BusinessEmployee = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });

            var qq = $q.all([gParamQuotation, gBizBank]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                            $scope.Parameter.Param_Quotation = [];
                            $scope.Parameter.Param_Quotation = data[0].data.responsedata;
                        }
                        else if (data[0].data.responsecode == '000') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Business Bank ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.Parameter.BizBank = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                    }

                    $http.get(baseURL + "DocumentExpenses/GetBusinessEX?key=" + makeid())
                        .then(function (response) {

                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {
                                    $scope.PurchaseOrderMain = response.data.responsedata;

                                    var grandtotal = 0;
                                    _.each($scope.PurchaseOrderMain, function (item) {
                                        item.PurchaseOrderDate = formatDate(item.PurchaseOrderDate);
                                        if (item.CreditType == 'CD')
                                            item.DueDate = formatDate(item.DueDate);
                                        if (item.PurchaseOrderStatus != '3')
                                            grandtotal = grandtotal + item.GrandAmount;
                                        item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);

                                        if (item.ReceiveInfo != undefined) {
                                            item.ReceiveInfo.PaymentDate = formatDate(item.ReceiveInfo.PaymentDate);
                                            item.ReceiveInfo.PaymentAmount = AFormatNumber(item.ReceiveInfo.PaymentAmount, 2);
                                            item.ReceiveInfo.NetPaymentAmount = AFormatNumber(item.ReceiveInfo.NetPaymentAmount, 2);
                                            item.ReceiveInfo.WithholdingAmount = item.ReceiveInfo.IsWithholdingTax == '1' ? AFormatNumber(item.ReceiveInfo.WithholdingAmount, 2) : '0.00';
                                            item.ReceiveInfo.DiscountAmount = item.ReceiveInfo.IsDiscount == '1' ? AFormatNumber(item.ReceiveInfo.DiscountAmount, 2) : '0.00';
                                            item.ReceiveInfo.ChequeDate = item.ReceiveInfo.ChequeDate != undefined ? formatDate(item.ReceiveInfo.ChequeDate) : undefined;
                                            item.ReceiveInfo.Balance = AFormatNumber(item.ReceiveInfo.Balance, 2);

                                            var htmltag;
                                            htmltag = "<div class='title' style='width:220px;' >รายละเอียดการชำระ </div> <hr class='mb-2'> ";
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ยอดที่เรียกเก็บ </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.PaymentAmount + " บาท</div></div>";
                                            if (item.ReceiveInfo.IsDiscount == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiveInfo.DiscountTypeName + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.DiscountAmount + " บาท</div></div> ";
                                            if (item.ReceiveInfo.IsWithholdingTax == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ภาษีหัก ณ ที่จ่าย </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.WithholdingAmount + " บาท</div> </div>";
                                            if (item.ReceiveInfo.BalanceType == 'Lost')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiveInfo.CauseTypeName.replace('/เงินเกิน', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.Balance + " บาท</div> </div>";
                                            if (item.ReceiveInfo.BalanceType == 'Over')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiveInfo.CauseTypeName.replace('เงินขาด/', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.Balance + " บาท</div> </div>";
                                            htmltag = htmltag + " <hr class='mb-2'><div class='row'> <div class='col-lg-6'> ยอดรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.NetPaymentAmount + " บาท</div> </div>";
                                            htmltag = htmltag + " <div class='row'> <div class='col-lg-6'> วิธีการรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.PaymentTypeName + "</div> </div>";
                                            item.ReceiveInfo.TagHtml = htmltag;
                                        }
                                    });
                                    $scope.BillingSummary = AFormatNumber(grandtotal, 2);
                                    $scope.PurchaseOrder = $scope.PurchaseOrderMain;


                                    $scope.Search.BillingType = 'ALL';
                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                catch (err) { showErrorToast(err); }
            });
        }
        catch (err) {
            showErrorToast(err);
        }
    };


    $scope.OnClickAdd = function () {
        window.location.href = baseURL + DocumentAddURL;
    };

    $scope.OnClickUpdate = function (key) {
        window.location.href = baseURL + DocumentAddURL + "?key=" + key;
    };

    $scope.OnClickNextWorkflow = function (quotationkey, tostatus) {
        try {
            var data = {
                PurchaseOrderKey: quotationkey,
                PurchaseOrderStatus: tostatus
            };
            $http.post(baseURL + "DocumentExpenses/UpdateStatusEX", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.init();
                                showSuccessWorkToast();
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickDuplicate = function (key) {
        window.location.href = baseURL + DocumentAddURL + "?key=" + key + "&dup_ref=1";

    };

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.PurchaseOrder, { PurchaseOrderKey: val })[0];
        if (filter != undefined) {
            $scope.PurchaseOrderAdd = [];
            $scope.PurchaseOrderAdd = angular.copy(filter);
            if ($scope.PurchaseOrderAdd.PurchaseOrderStatus == '1')
                $('#modalPO-del').modal('show');
            else
                $('#modalPO-alert').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            PurchaseOrderKey: $scope.PurchaseOrderAdd.PurchaseOrderKey
        };

        $http.post(baseURL + "DocumentExpenses/DeleteEX", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalPO-del').modal('hide');
                            $scope.init();
                            $scope.QuotationAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickReport = function (dockey, docno) {
        $('#modal-report').modal('show');
        $scope.document = [];
        $scope.document.bindding = 1;
        $scope.document.DocNo = docno;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: 'EX',
                DocumentNo: docno,
                TypeAction: 'Report',
                Content: 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function (dockey, docno) {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: 'EX',
                DocumentNo: docno,
                TypeAction: 'Download',
                Content: 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickSendMail = function (dockey, docno) {
        var quo = _.where($scope.PurchaseOrder, { PurchaseOrderKey: dockey })[0];
        if (quo != undefined) {
            $scope.EmailAdd = [];
            $scope.EmailAdd.DocKey = dockey;
            $scope.EmailAdd.DocNo = docno;
            $scope.EmailAdd.MailToMe = true;
            $scope.EmailAdd.MailFrom = $scope.BusinessEmployee.Email;
            $scope.EmailAdd.MailTo = quo.CustomerContactEmail;
            $scope.EmailAdd.Subject = 'เอกสาร ' + docno;
            $scope.EmailAdd.Message = 'เรียน ' + quo.CustomerName + '\n\n' + $scope.BusinessProfile.BusinessName + ' เลขที่ ' + docno + ' มาให้ในอีเมลนี้\n\nกรุณาคลิกที่ไฟล์แนบ เพื่อดาวน์โหลดเอกสารของคุณ\n\nด้วยความเคารพ\n' + $scope.BusinessEmployee.EmployeeName + '\n' + $scope.BusinessProfile.BusinessName;
            $('#modalemail-send').modal('show');
        }

    };

    $scope.OnClickConfirmEmail = function () {
        $('#modalemail-send').modal('hide');
        var data = {
            MailFrom: $scope.EmailAdd.MailFrom,
            MailTo: $scope.EmailAdd.MailTo,
            MailCC: $scope.EmailAdd.MailCC,
            MailIsMe: $scope.EmailAdd.MailToMe ? "1" : "0",
            Subject: $scope.EmailAdd.Subject,
            Message: $scope.EmailAdd.Message,
            DocumentKey: $scope.EmailAdd.DocKey,
            DocumentNo: $scope.EmailAdd.DocNo,
            DocumentType: 'EX',
            Content: 'ORIGINAL'
        };

        $http.post(baseURL + "DocumentSell/PostEmailToSending", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {

                            showSuccessText(response.data.responsedata);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    // #region Payment Popup
    $scope.OnClickPaymentAdd = function (val) {

        var filter = _.where($scope.PurchaseOrder, { PurchaseOrderKey: val })[0];
        if (filter != undefined) {
            $scope.ReceiptAdd = [];
            $scope.ReceiptAdd = angular.copy(filter);

            $scope.ReceiptAdd.ReceiptInfo = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push({ ID: '0', Name: 'จำนวนเงิน', Rate: 0 });
            _.each($scope.Parameter.Param_Quotation.Param_WithholdingRate, function (item) { $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push(item); });


            $scope.ReceiptAdd.ReceiptInfo.DocumentKey = $scope.ReceiptAdd.PurchaseOrderKey;
            $scope.ReceiptAdd.ReceiptInfo.DocumentNo = $scope.ReceiptAdd.PurchaseOrderNo;
            $scope.ReceiptAdd.ReceiptInfo.DocumentType = headerdoc;
            $scope.ReceiptAdd.ReceiptInfo.CustomerName = $scope.ReceiptAdd.CustomerName;
            $scope.ReceiptAdd.ReceiptInfo.PaymentAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            $scope.ReceiptAdd.ReceiptInfo.PaymentDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiptInfo.ChequeDate = GetDatetimeNow();
            $('#inputreceivedate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.PaymentDate);
            $('#inputreceivechaque-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.ChequeDate);
            
            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            //$scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = $scope.ReceiptAdd.IsAdjustDiscount || $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.PaymentAmount, 2) : AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            $scope.ReceiptAdd.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax;
            // $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = '0';
            if ($scope.ReceiptAdd.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.WithholdingKey; });
                if (type.length > 0)
                    $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];

                $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.WithholdingAmount, 2) : AFormatNumber(0, 2);
            }

            $scope.ReceiptAdd.IsAdjustDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.IsDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiptInfo.DiscountType = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined;
            $scope.ReceiptAdd.ReceiptInfo.DiscountTypeName = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountTypeName : undefined;
            $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : AFormatNumber(0, 2);
            $scope.ReceiptAdd.ReceiptInfo.PaymentType = "CA";
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(0, 2);
            $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
            $scope.ReceiptAdd.ReceiptInfo.IsPayment = true;

            $('#modalreceipt-receive').modal('show');
        }
    };

    $scope.OnChangeIsAdjustReceive = function () {
        $scope.ReceiptAdd.ReceiptInfo.DiscountType = "1";
        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = AFormatNumber(0, 2);
    };

    $scope.OnChangeFee = function () {
        $scope.ReceiptAdd.ReceiptInfo.Fee = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.Fee, 2);
    }

    $scope.OnChangeIsWithhollding = function () {

        var type = [];
        
        if ($scope.ReceiptAdd.ReceiptInfo.IsPayment)
            type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '1'; });
        else
            type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });

        if (type.length > 0)
            $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    };

    $scope.OnChangeWithholldingReceipt = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding == '0') {
            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            $scope.OnChangeReceiveCalculate();
        }
        else {
            $scope.OnChangeReceiveCalculate();
        }
    };

    $scope.OnClickChangePaymentAmount = function () {
        $scope.ReceiptAdd.ReceiptInfo.IsPayment = false
        $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = '0';
        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    }

    $scope.OnChangePaymentType = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR') {
            var type = $scope.Parameter.BizBank.filter(function (item) { return item.UID == $scope.Parameter.BizBank[0].UID; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer = type[0];
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
            $scope.ReceiptAdd.ReceiptInfo.ChequeDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiptInfo.ChequeNo = '';
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
        }
    };

    $scope.OnChangeReceiveCalculate = function () {
        try {
            var grandtotal = 0, netpayment = 0, discount = 0, withholding = 0, netgrand = 0, pricediscount = 0, pricevat, balance = 0;
            grandtotal = $scope.ReceiptAdd.ReceiptInfo.PaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.PaymentAmount); // Discount Percenter
            pricediscount = $scope.ReceiptAdd.TotalAfterDiscountAmount == undefined || $scope.ReceiptAdd.TotalAfterDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalAfterDiscountAmount); // Discount Percenter
            pricevat = $scope.ReceiptAdd.TotalBeforeVatAmount == undefined || $scope.ReceiptAdd.TotalBeforeVatAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalBeforeVatAmount); // Discount Percenter
            netpayment = $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount); // Discount Percenter
            discount = $scope.ReceiptAdd.ReceiptInfo.DiscountAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.DiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.DiscountAmount); // Discount Percenter
            withholding = $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.WithholdingAmount); // Discount Percenter


            if ($scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax) {
                var withholdingrate = 0;
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID; }); 
                if (type.length > 0)
                    withholdingrate = type[0].Rate;
                
                if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding == '0')
                    withholding = withholding;
                else {
                    if ($scope.ReceiptAdd.TaxType == 'N')
                        withholding = pricediscount * withholdingrate / 100;
                    else if ($scope.ReceiptAdd.TaxType == 'V')
                        withholding = pricevat * withholdingrate / 100;
                }

                grandtotal = grandtotal - withholding;
            }
            else
                grandtotal = grandtotal;

            grandtotal = grandtotal - discount;

            if (!$scope.ReceiptAdd.ReceiptInfo.IsPayment) {
                balance = netpayment - grandtotal;
            }
            else
                netpayment = grandtotal;

            if (balance < 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินขาด';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Lost';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
                balance = balance * (-1);
            }
            else if (balance > 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินเกิน';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Over';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }
            else {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
                $scope.ReceiptAdd.ReceiptInfo.IsText = '';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Equal';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }

            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(withholding, 2);//
            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber(netpayment, 2);//
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(balance, 2);//
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickReceiptPayment = function () {
        try {

            var adjusttype;
            if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '1')
                adjusttype = 'ส่วนลดพิเศษ';
            else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '2')
                adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
            else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '3')
                adjusttype = 'ค่าดำเนินการ';
            else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '4')
                adjusttype = 'ปัดเศษ';

            var paymenttype;
            if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CA')
                paymenttype = 'เงินสด';
            else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR')
                paymenttype = 'โอนเงิน';
            else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ')
                paymenttype = 'เช็ค';
            else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC')
                paymenttype = 'บัตรเครดิต';

            var cause;
            if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '1')
                cause = 'ค่าธรรมเนียม';
            else if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '2')
                cause = 'เงินขาด/เงินเกิน';

            var data = {
                DocumentKey: $scope.ReceiptAdd.PurchaseOrderKey,
                DocumentNo: $scope.ReceiptAdd.PurchaseOrderNo,
                DocumentType: headerdoc,
                CustomerName: $scope.ReceiptAdd.ReceiptInfo.CustomerName,
                PaymentAmount: $scope.ReceiptAdd.ReceiptInfo.PaymentAmount,
                PaymentDate: ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.PaymentDate),
                NetPaymentAmount: ConvertToDecimal($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount),
                IsWithholdingTax: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? '1' : '0',
                WithholdingKey: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID : undefined,
                WithholdingRate: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.Rate : undefined,
                WithholdingAmount: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount : undefined,
                IsDiscount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? '1' : '0',
                DiscountType: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountType : undefined,
                DiscountTypeName: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? adjusttype : undefined,
                DiscountAmount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountAmount : undefined,
                PaymentType: $scope.ReceiptAdd.ReceiptInfo.PaymentType,
                PaymentTypeName: paymenttype,
                BankAccountID: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.UID : undefined,
                BankCode: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank.ID : undefined,
                BankName: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.BankName : $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' || $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank.Name : undefined,
                BankAccountNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.AccountNo : undefined,
                BankAccountType: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.DepositTypeName : undefined,
                ChequeDate: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.ChequeDate) : undefined,
                ChequeNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.ChequeNo : undefined,
                CustomerBankCode: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? '' :  $scope.ReceiptAdd.ReceiptInfo.SelectBank.ID : undefined,
                CustomerBankName: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? '' : $scope.ReceiptAdd.ReceiptInfo.SelectBank.Name : undefined,
                CustomerAccountNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.CustomerAccountNo : undefined,
                Comment: $scope.ReceiptAdd.ReceiptInfo.Comment,
                Balance: $scope.ReceiptAdd.ReceiptInfo.Balance,
                BalanceType: $scope.ReceiptAdd.ReceiptInfo.BalanceType,
                CauseType: $scope.ReceiptAdd.ReceiptInfo.CauseType,
                CauseTypeName: cause,
                PaymentFee: $scope.ReceiptAdd.ReceiptInfo.Fee
            };

            $http.post(baseURL + "DocumentExpenses/PostBusinessReceiptPayment", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessToast();
                                $('#modalreceipt-receive').modal('hide');
                                $scope.init();
                                $scope.ReceiptAdd = [];
                            }
                            else showErrorToast(response.data.errormessage);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    catch (err) {
                        $scope.table.binding = 0;
                        showErrorToast(err);
                    }
                });

        }
        catch (err) { showErrorToast(err); }
    };
    // #endregion

    $scope.OnClickClear = function () {
        $scope.Search = [];
        $scope.Search.SelectStatus = '0';
        $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
        $scope.Search.InputToDate = GetDatetimeNowLastMonth();
        $scope.PurchaseOrder = $scope.PurchaseOrderMain;
    };

    $scope.OnClickSearch = function () {
        $scope.PurchaseOrder = $scope.PurchaseOrderMain;
        $scope.table.binding = 1;

        $scope.PurchaseOrder = _.filter($scope.PurchaseOrderMain, function (item) {

            var c1 = true, c2 = true, c3 = true, c4 = true, c5 = true;

            if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.CustomerName != undefined && (item.CustomerName).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.CustomerName == undefined || (item.CustomerName).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.PurchaseOrderNo != undefined && (item.PurchaseOrderNo).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.PurchaseOrderNo == undefined || (item.PurchaseOrderNo).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProjectName != undefined && (item.ProjectName).indexOf($scope.Search.InputFilter) > -1)
                    c3 = true;
                else if (item.ProjectName == undefined || (item.ProjectName).indexOf($scope.Search.InputFilter) < 0)
                    c3 = false;


            }

            if ($scope.Search.SelectStatus == '0') {
                c4 = true;
            }
            else if (item.PurchaseOrderStatus == $scope.Search.SelectStatus) {
                c4 = true;
            }
            else
                c4 = false;

            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != "" && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != "") {

                var fromarry = $scope.Search.InputFromDate.split('-');

                var quotationdatearry = item.PurchaseOrderDate.split('-');

                var from = new Date(fromarry[2], fromarry[1] - 1, fromarry[0]);

                var quotationdate = new Date(quotationdatearry[2], quotationdatearry[1] - 1, quotationdatearry[0]);

                var toarry = $scope.Search.InputToDate.split('-');

                var to = new Date(toarry[2], toarry[1] - 1, toarry[0]);
                //alert(quotationdate); alert(from); alert(from);
                if (quotationdate >= from && quotationdate <= to)
                    c5 = true;
                else
                    c5 = false;
            }


            if ((c1 || c2 || c3) && c4 && c5) {
                return item;
            }
        });
        $scope.table.binding = 0;
    };

    $scope.OnClickSelectType = function (type) {
        if (type == 'ALL')
            $scope.init();
    }
});