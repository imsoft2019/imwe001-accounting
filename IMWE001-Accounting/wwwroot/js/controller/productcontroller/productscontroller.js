﻿WEACCTAPP.controller('productscontroller', function ($scope, $http, $timeout, GlobalVar, Upload) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessProduct = [];
    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            if ($scope.LoadFirst == undefined) {
                $http.get(baseURL + "Product/GetParameterProducts?key=" + makeid())
                    .then(function (response) {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                                $scope.Parameter = [];
                                $scope.Parameter = response.data.responsedata;

                                var substringMatcher = function (strs) {
                                    return function findMatches(q, cb) {
                                        if (q == '') {
                                            cb(strs);

                                        }
                                        else {
                                            var matches, substringRegex;

                                            // an array that will be populated with substring matches
                                            matches = [];

                                            // regex used to determine if a string contains the substring `q`
                                            var substrRegex = new RegExp(q, 'i');

                                            // iterate through the pool of strings and for any string that
                                            // contains the substring `q`, add it to the `matches` array
                                            for (var i = 0; i < strs.length; i++) {
                                                if (substrRegex.test(strs[i])) {
                                                    matches.push(strs[i]);
                                                }
                                            }

                                            cb(matches);
                                        }
                                    };
                                };

                                var unittype = [];
                                _.each($scope.Parameter.Param_UnitType, function (item) { unittype.push(item.Name); });

                                $('#productunit .typeahead').typeahead({
                                    hint: true,
                                    highlight: true,
                                    minLength: 0
                                }, {
                                        name: 'unittype',
                                        limit: 20,
                                        source: substringMatcher(unittype)
                                    });


                                var category = [];
                                _.each($scope.Parameter.Param_Category, function (item) { category.push(item.Name); });
                                $('#productcategory .typeahead').typeahead({
                                    hint: false,
                                    highlight: true,
                                    minLength: 0,
                                }, {
                                        name: 'category',
                                        limit: 20,
                                        source: substringMatcher(category)
                                    });

                            }
                            else if (response.data.responsecode == '400') {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    });
                $scope.LoadFirst = 0;
            }

            $http.get(baseURL + "Product/GetBusinessProducts?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            $scope.BusinessProductMain = response.data.responsedata;
                            _.each($scope.BusinessProductMain, function (item) {
                                item.SellPrice = AFormatNumber(item.SellPrice, 2);
                                item.SellPriceOrder = item.SellTaxType != '2' ? ConvertToDecimal(item.SellPrice) : ConvertToDecimal(item.SellAfterTaxPrice);
                                item.SellPriceText = item.SellTaxType != '2' ? AFormatNumber(item.SellPrice, 2) : AFormatNumber(item.SellAfterTaxPrice, 2);
                                item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                                item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                                item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                                item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                                item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                            });

                            $scope.BusinessProduct = $scope.BusinessProductMain;
                            $scope.Search.ProductType = 'ALL';

                            var drEvent = $('.dropify').dropify();


                            $scope.retpage = [];
                            $scope.range();
                            $scope.table.binding = 0;
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickSelectType = function (val) {

        if (val == 'ALL')
            $scope.BusinessProduct = $scope.BusinessProductMain;
        else if (val == 'S')
            $scope.BusinessProduct = _.where($scope.BusinessProductMain, { ProductType: 'S' });
        else if (val == 'P')
            $scope.BusinessProduct = _.where($scope.BusinessProductMain, { ProductType: 'P' });

        $scope.Search.ProductType = val;
        $scope.retpage = [];
        $scope.range();
    };

    $scope.OnClickSearch = function () {
        $scope.BusinessProduct = $scope.BusinessProductMain;

        if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {
            $scope.BusinessProduct = _.filter($scope.BusinessProductMain, function (item) {

                var c1 = true, c2 = true, c3 = true;
                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProductCode != undefined && (item.ProductCode).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.ProductCode == undefined || (item.ProductCode).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProductName != undefined && (item.ProductName).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.ProductName == undefined || (item.ProductName).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProductBarCode != undefined && (item.ProductBarCode).indexOf($scope.Search.InputFilter) > -1)
                    c3 = true;
                else if (item.ProductBarCode == undefined || (item.ProductBarCode).indexOf($scope.Search.InputFilter) < 0)
                    c3 = false;

                if (c1 || c2 || c3) {
                    if ($scope.Search.ProductType != 'ALL' && item.ProductType == $scope.Search.ProductType)
                        return item;
                    else if ($scope.Search.ProductType == 'ALL')
                        return item;
                }
            });
        }

    };

    function initdropify(path) {
        $("#product_thumnail").addClass('dropify');
        var publicpath_identity_picture = path;

        var drEvent = $('.dropify').dropify();
        //drEvent.on('dropify.afterClear', function (event, element) {
        //    $scope.DeleteImages = 1;
        //});
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = publicpath_identity_picture;
        drEvent.destroy();

        drEvent.init();

        drEvent = $('.dropify').dropify();
        drEvent.on('dropify.afterClear', function (event, element) {
            $scope.DeleteImages = 1;
        });

        $('.dropify#identity_picture').dropify({
            defaultFile: publicpath_identity_picture,
        });



        $('.dropify').dropify();
    }

    $scope.OnClickAdd = function () {
        $scope.BusinessProductAdd = [];

        $scope.BusinessProductAdd.ProductType = 'S';
        $scope.BusinessProductAdd.SellTaxType = '1';
        $scope.BusinessProductAdd.BuyTaxType = '1';
        initdropify('');

        $('#modalproduct-add').modal('show');
    };

    $scope.OnClickUpdate = function (key) {
        var filter = _.where($scope.BusinessProduct, { ProductKey: key })[0];
        if (filter != undefined) {
            $scope.BusinessProductAdd = [];
            $scope.BusinessProductAdd = angular.copy(filter);

            if ($scope.BusinessProductAdd.ThumbnailPath != undefined && $scope.BusinessProductAdd.ThumbnailPath != "")
                initdropify(baseURL + 'uploads/product/' + $scope.BusinessProductAdd.ThumbnailPath);
            else
                initdropify('');

            $('#modalproduct-add').modal('show');
        }
    };

    $scope.OnClickDuplicate = function (key) {
        var filter = _.where($scope.BusinessProduct, { ProductKey: key })[0];
        if (filter != undefined) {
            $scope.BusinessProductAdd = [];
            $scope.BusinessProductAdd = angular.copy(filter);

            if ($scope.BusinessProductAdd.ThumbnailPath != undefined)
                initdropify(baseURL + 'uploads/product/' + $scope.BusinessProductAdd.ThumbnailPath);
            else
                initdropify('');

            $scope.BusinessProductAdd.ProductKey = undefined;

            $('#modalproduct-add').modal('show');
        }
    };

    $scope.OnClickSave = function () {
        try {
            if ($scope.BusinessProductAdd.ProductName == undefined || $scope.BusinessProductAdd.ProductName == "")
                throw "กรุณาระบุชื่อสินค้า/บริการ";
            else {
                var vat = _.where($scope.Parameter.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;

                var sell = 0, selltax = 0, selllast = 0;
                if ($scope.BusinessProductAdd.SellTaxType == 1) {
                    sell = ConvertToDecimal($scope.BusinessProductAdd.SellPrice == undefined ? 0 : $scope.BusinessProductAdd.SellPrice);
                    selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                    selllast = sell + selltax;
                }
                else if ($scope.BusinessProductAdd.SellTaxType == 2) {
                    sell = ConvertToDecimal($scope.BusinessProductAdd.SellPrice == undefined ? 0 : $scope.BusinessProductAdd.SellPrice);
                    selltax = sell - (sell / (parseInt(100) + parseInt(vat))) * 100;
                    selllast = sell - selltax;
                }
                else {
                    sell = $scope.BusinessProductAdd.SellPrice == undefined ? 0 : $scope.BusinessProductAdd.SellPrice;
                    selltax = 0;
                    selllast = $scope.BusinessProductAdd.SellPrice == undefined ? 0 : $scope.BusinessProductAdd.SellPrice;
                }

                var buy = 0, buytax = 0, buylast = 0;
                if ($scope.BusinessProductAdd.BuyTaxType == 1) {
                    buy = ConvertToDecimal($scope.BusinessProductAdd.BuyPrice == undefined ? 0 : $scope.BusinessProductAdd.BuyPrice);
                    buytax = (buy * (parseInt(100) + parseInt(vat))) / 100 - buy;
                    buylast = buy + buytax;
                }
                else if ($scope.BusinessProductAdd.BuyTaxType == 2) {
                    buy = ConvertToDecimal($scope.BusinessProductAdd.BuyPrice == undefined ? 0 : $scope.BusinessProductAdd.BuyPrice);
                    buytax = buy - (buy / (parseInt(100) + parseInt(vat))) * 100;
                    buylast = buy - buytax;
                }
                else {
                    buy = $scope.BusinessProductAdd.BuyPrice == undefined ? 0 : $scope.BusinessProductAdd.BuyPrice;
                    buytax = 0;
                    buylast = $scope.BusinessProductAdd.BuyPrice == undefined ? 0 : $scope.BusinessProductAdd.BuyPrice;
                }

                var selltaxtype;
                if ($scope.BusinessProductAdd.SellTaxType == "1")
                    selltaxtype = 'ราคาไม่รวม VAT';
                if ($scope.BusinessProductAdd.SellTaxType == "2")
                    selltaxtype = 'ราคารวม VAT แล้ว';
                if ($scope.BusinessProductAdd.SellTaxType == "3")
                    selltaxtype = 'VAT 0%';
                if ($scope.BusinessProductAdd.SellTaxType == "4")
                    selltaxtype = 'สินค้าได้รับยกเว้นภาษี (N/A)';

                var buytaxtype;
                if ($scope.BusinessProductAdd.BuyTaxType == "1")
                    buytaxtype = 'ราคาไม่รวม VAT';
                if ($scope.BusinessProductAdd.BuyTaxType == "2")
                    buytaxtype = 'ราคารวม VAT แล้ว';
                if ($scope.BusinessProductAdd.BuyTaxType == "3")
                    buytaxtype = 'VAT 0%';
                if ($scope.BusinessProductAdd.BuyTaxType == "4")
                    buytaxtype = 'สินค้าได้รับยกเว้นภาษี (N/A)';

                var unit = document.getElementById('input_unittype').value;
                var category = document.getElementById('input_categorytype').value;

                var data = {
                    ProductKey: $scope.BusinessProductAdd.ProductKey,
                    ProductType: $scope.BusinessProductAdd.ProductType,
                    ProductTypeName: $scope.BusinessProductAdd.ProductType == 'S' ? 'บริการ' : 'สินค้า',
                    ProductCode: $scope.BusinessProductAdd.ProductCode,
                    ProductName: $scope.BusinessProductAdd.ProductName,
                    ProductDescription: $scope.BusinessProductAdd.ProductDescription,
                    UnitTypeName: unit,
                    CategoryName: category,
                    SellPrice: ConvertToDecimal(sell),
                    SellTaxPrice: ConvertToDecimal(selltax),
                    SellAfterTaxPrice: ConvertToDecimal(selllast),
                    SellTaxType: $scope.BusinessProductAdd.SellTaxType,
                    SellTaxTypeName: selltaxtype,
                    BuyPrice: $scope.BusinessProductAdd.ProductType == 'S' ? ConvertToDecimal(sell) : ConvertToDecimal(buy),
                    BuyTaxPrice: $scope.BusinessProductAdd.ProductType == 'S' ? ConvertToDecimal(selltax) : ConvertToDecimal(buytax),
                    BuyAfterTaxPrice: $scope.BusinessProductAdd.ProductType == 'S' ? ConvertToDecimal(selllast) : ConvertToDecimal(buylast),
                    BuyTaxType: $scope.BusinessProductAdd.ProductType == 'S' ? $scope.BusinessProductAdd.SellTaxType : $scope.BusinessProductAdd.BuyTaxType,
                    BuyTaxTypeName: $scope.BusinessProductAdd.ProductType == 'S' ? selltaxtype : buytaxtype,
                    BuyDescription: $scope.BusinessProductAdd.ProductType == 'S' ? $scope.BusinessProductAdd.ProductDescription : $scope.BusinessProductAdd.BuyDescription,
                    ProductBarCode: $scope.BusinessProductAdd.ProductBarCode,
                    ThumbnailPath: $scope.DeleteImages == 1 ? '' : $scope.BusinessProductAdd.ThumbnailPath,
                    PicturePath: $scope.DeleteImages == 1 ? '' : $scope.BusinessProductAdd.PicturePath
                };

                $http.post(baseURL + "Product/PostBusinessProducts", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    if ($scope.SelectedFiles != undefined && $scope.Upload == 1) {
                                        try {

                                            var input = document.getElementById("product_thumnail");
                                            var files = input.files;
                                            var formData = new FormData();

                                            for (var i = 0; i != files.length; i++) {
                                                formData.append("files", files[i]);
                                            }

                                            $.ajax(
                                                {
                                                    url: baseURL + "Product/UploadPictureProducts?productkey=" + response.data.responsedata,
                                                    data: formData,
                                                    processData: false,
                                                    contentType: false,
                                                    type: "POST",
                                                    success: function (data) {
                                                        $('#modalproduct-add').modal('hide');
                                                        $scope.init();
                                                        showSuccessToast();
                                                    },
                                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                        showErrorToast(textStatus);
                                                    }
                                                }
                                            );
                                        }
                                        catch (ex) {
                                            showErrorToast(ex);
                                        }
                                    }
                                    else {
                                        $('#modalproduct-add').modal('hide');
                                        $scope.init();
                                        showSuccessToast();
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.BusinessProduct, { ProductKey: val })[0];
        if (filter != undefined) {
            $scope.BusinessProductAdd = [];
            $scope.BusinessProductAdd = angular.copy(filter);
            $('#modalproduct-del').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            ProductKey: $scope.BusinessProductAdd.ProductKey,
        };

        $http.post(baseURL + "Product/DeleteBusinessProducts", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalproduct-del').modal('hide');
                            $scope.init();
                            $scope.BusinessProductAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };


    $scope.itemsPerPage = 10;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'ProductKey';
    $scope.reverseSort = false;

    $scope.pageCount = function () {
        return Math.ceil($scope.BusinessProduct.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessProduct.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

    $scope.UploadFiles = function (files) {
        $scope.Upload = 1;
        $scope.SelectedFiles = files;
    };


});


