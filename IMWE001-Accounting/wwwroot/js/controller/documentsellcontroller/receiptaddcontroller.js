﻿WEACCTAPP.controller('receiptaddcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, contactService, productService, $q, Upload) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'RE';

    $scope.DocumentAttach = [];
    $scope.ReceiptAdd = [];
    $scope.Parameter = [];
    $scope.UserProfile = [];
    $scope.Parameter.Param_Contact = [];
    $scope.Parameter.Param_Product = [];
    $scope.Parameter.Param_Employee = [];
    $scope.Parameter.Param_Revenue = [];
    $scope.Parameter.BizBank = [];
    $scope.table = [];

    var gCustomer = paramService.getCustomer();
    var gProduct = paramService.getProduct();
    var gEmployee = paramService.getEmployee();
    var gParamQuotation = paramService.getParameterQuotation();
    var gProfile = paramService.getProfile();
    var gRemark = paramService.getDocumentRemark(headerdoc);
    var gBizBank = paramService.getBankBusiness();

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        $scope.table.binding = 1;

        var prefix = { asofdate: GetDatetimeNow(), documenttype: headerdoc };

        var qq = $q.all([paramService.getDocumentNo(prefix), gCustomer, gProduct, gEmployee, gParamQuotation, gProfile, gRemark, gBizBank]).then(function (data) {
            try {
                /****** GetDocument NO ******/
                if (data[0] != undefined && data[0] != "") {
                    if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.ReceiptAdd.ReceiptNo = data[0].data.responsedata; }
                    else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                }

                /****** Get Customer ******/
                if (data[1] != undefined && data[1] != "") {
                    if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                        $scope.SearchContact = [];
                        $scope.Parameter.Param_Contact = [];
                        $scope.Parameter.Param_ContactMain = [];
                        $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                    }
                    else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                }

                /****** Get Product ******/
                if (data[2] != undefined && data[2] != "") {
                    if (data[2].data.responsecode == '200' && data[2].data.responsedata != undefined && data[2].data.responsedata != "") {
                        $scope.SearchProduct = [];
                        $scope.Parameter.Param_Product = [];
                        $scope.Parameter.Param_ProductMain = [];

                        $scope.Parameter.Param_ProductMain = data[2].data.responsedata;

                        _.each($scope.Parameter.Param_ProductMain, function (item) {
                            item.SellPrice = AFormatNumber(item.SellPrice, 2);
                            item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                            item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                            item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                            item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                        });

                        $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;
                    }
                    else if (data[2].data.responsecode == '400') { showErrorToast(data[2].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[3] != undefined && data[3] != "") {
                    if (data[3].data.responsecode == '200' && data[3].data.responsedata != undefined && data[3].data.responsedata != "") {
                        $scope.Parameter.Param_Employee = [];
                        $scope.Parameter.Param_Employee = data[3].data.responsedata;
                    }
                    else if (data[3].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Parameter Receipt ******/
                if (data[4] != undefined && data[4] != "") {
                    if (data[4].data.responsecode == '200' && data[4].data.responsedata != undefined && data[4].data.responsedata != "") {
                        $scope.Parameter.Param_Quotation = [];
                        $scope.Parameter.Param_Quotation = data[4].data.responsedata;

                        var substringMatcher = function (strs) {
                            return function findMatches(q, cb) {
                                if (q == '') {
                                    cb(strs);
                                }
                                else {
                                    var matches, substringRegex;

                                    // an array that will be populated with substring matches
                                    matches = [];

                                    // regex used to determine if a string contains the substring `q`
                                    var substrRegex = new RegExp(q, 'i');

                                    // iterate through the pool of strings and for any string that
                                    // contains the substring `q`, add it to the `matches` array
                                    for (var i = 0; i < strs.length; i++) {
                                        if (substrRegex.test(strs[i])) {
                                            matches.push(strs[i]);
                                        }
                                    }

                                    cb(matches);
                                }
                            };
                        };

                        var unittype = [];
                        _.each($scope.Parameter.Param_Quotation.Param_UnitType, function (item) { unittype.push(item.Name); });

                        $('#productunit .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0
                        }, {
                                name: 'unittype',
                                limit: 20,
                                source: substringMatcher(unittype)
                            });


                        var category = [];
                        _.each($scope.Parameter.Param_Quotation.Param_Category, function (item) { category.push(item.Name); });
                        $('#productcategory .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0,
                        }, {
                                name: 'category',
                                limit: 20,
                                source: substringMatcher(category)
                            });
                    }
                    else if (data[4].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Profile ******/
                if (data[5] != undefined && data[5] != "") {
                    if (data[5].data.responsecode == '200' && data[5].data.responsedata != undefined && data[5].data.responsedata != "") {
                        $scope.UserProfile = [];
                        $scope.UserProfile = data[5].data.responsedata;
                    }
                    else if (data[5].data.responsecode == '400') { showErrorToast(data[5].data.errormessage); }
                }

                /****** Get Remark ******/
                if (data[6] != undefined && data[6] != "") {
                    if (data[6].data.responsecode == '200' && data[6].data.responsedata != undefined && data[6].data.responsedata != "") {
                        $scope.ReceiptAdd.Remark = data[6].data.responsedata.DocumentText;
                    }
                    else if (data[6].data.responsecode == '400') { showErrorToast(data[6].data.errormessage); }
                }

                /****** Get Business Bank ******/
                if (data[7] != undefined && data[7] != "") {
                    if (data[7].data.responsecode == '200' && data[7].data.responsedata != undefined && data[7].data.responsedata != "") {
                        $scope.Parameter.BizBank = data[7].data.responsedata;
                    }
                    else if (data[7].data.responsecode == '400') { showErrorToast(data[7].data.errormessage); }
                }


                if (QueryString('ReceiptKey') != undefined) {

                    $http.get(baseURL + "DocumentSell/GetBusinessReceiptbykey?ReceiptKey=" + QueryString('ReceiptKey') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.ReceiptAdd = [];
                                    $scope.ReceiptAdd = response.data.responsedata;

                                    $scope.ReceiptAdd.ReceiptDate = formatDate($scope.ReceiptAdd.ReceiptDate);
                                    $('#inputreceiptdate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptDate);

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.ReceiptAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.ReceiptAdd.SelectSale = type[0];

                                    $scope.ReceiptAdd.IsTaxHeader = $scope.ReceiptAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                    $scope.ReceiptAdd.IsDiscountHeader = $scope.ReceiptAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                    $scope.ReceiptAdd.IsTaxSummary = $scope.ReceiptAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                    // หักภาษี ณ ที่จ่าย

                                    $scope.ReceiptAdd.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax == '1' ? true : false;
                                    var fixedwith = $scope.ReceiptAdd.IsWithholdingTax;
                                    $scope.ReceiptAdd.IsWithholdingTaxFix = fixedwith;

                                    if ($scope.ReceiptAdd.IsWithholdingTax) {

                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.ReceiptAdd.WithholdingKey; });
                                        if (type.length > 0)
                                            $scope.ReceiptAdd.SelectWithholding = type[0];

                                        $scope.ReceiptAdd.WithholdingAmount = AFormatNumber($scope.ReceiptAdd.WithholdingAmount, 2);
                                        $scope.ReceiptAdd.PaymentAmount = AFormatNumber($scope.ReceiptAdd.PaymentAmount, 2);//

                                    }
                                    else {
                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                        if (type.length > 0)
                                            $scope.ReceiptAdd.SelectWithholding = type[0];

                                    }

                                    $scope.ReceiptAdd.IsAdjustDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false; // เพิ่มรายการปรับลด
                                    var fixeddis = $scope.ReceiptAdd.IsAdjustDiscount;
                                    $scope.ReceiptAdd.IsAdjustDiscountFix = fixeddis;
                                    if ($scope.ReceiptAdd.IsAdjustDiscount) {
                                        $scope.ReceiptAdd.AdjustDiscountAmount = AFormatNumber($scope.ReceiptAdd.AdjustDiscountAmount, 2);//
                                        $scope.ReceiptAdd.PaymentAmount = AFormatNumber($scope.ReceiptAdd.PaymentAmount, 2);//
                                    }

                                    $scope.ReceiptAdd.TotalAmount = AFormatNumber($scope.ReceiptAdd.TotalAmount, 2);
                                    $scope.ReceiptAdd.DiscountPercent = AFormatNumber($scope.ReceiptAdd.DiscountPercent, 2);
                                    $scope.ReceiptAdd.DiscountAmount = AFormatNumber($scope.ReceiptAdd.DiscountAmount, 2);
                                    $scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber($scope.ReceiptAdd.TotalAfterDiscountAmount, 2);
                                    $scope.ReceiptAdd.ExemptAmount = AFormatNumber($scope.ReceiptAdd.ExemptAmount, 2);
                                    $scope.ReceiptAdd.VatableAmount = AFormatNumber($scope.ReceiptAdd.VatableAmount, 2);
                                    $scope.ReceiptAdd.VAT = AFormatNumber($scope.ReceiptAdd.VAT, 2);
                                    $scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber($scope.ReceiptAdd.TotalBeforeVatAmount, 2);
                                    $scope.ReceiptAdd.GrandAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);

                                    $scope.ReceiptAdd.Signature = $scope.ReceiptAdd.Signature == '1' ? true : false;

                                    _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {
                                        item.Quantity = AFormatNumber(item.Quantity, 2);
                                        item.Price = AFormatNumber(item.Price, 2);
                                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.Discount = AFormatNumber(item.Discount, 2);
                                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                        item.VatRate = AFormatNumber(item.VatRate, 2);
                                        item.Vat = AFormatNumber(item.Vat, 2);
                                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                        item.Total = AFormatNumber(item.Total, 2);
                                    });

                                    $scope.OnClickClearDetail();

                                    if ($scope.ReceiptAdd.ReceiptInfo != undefined) {
                                        $scope.ReceiptAdd.ReceiptInfo.PaymentDate = formatDate($scope.ReceiptAdd.ReceiptInfo.PaymentDate);
                                        $('#inputreceivedate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.PaymentDate);
                                        $scope.ReceiptAdd.ReceiptInfo.PaymentAmount = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.PaymentAmount, 2);
                                        $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount, 2);
                                        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax == '1' ? AFormatNumber($scope.ReceiptAdd.ReceiptInfo.WithholdingAmount, 2) : '0.00';
                                        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = $scope.ReceiptAdd.ReceiptInfo.IsDiscount == '1' ? AFormatNumber($scope.ReceiptAdd.ReceiptInfo.DiscountAmount, 2) : '0.00';
                                        $scope.ReceiptAdd.ReceiptInfo.ChequeDate = $scope.ReceiptAdd.ReceiptInfo.ChequeDate != undefined ? formatDate($scope.ReceiptAdd.ReceiptInfo.ChequeDate) : undefined;
                                        if ($scope.ReceiptAdd.ReceiptInfo.ChequeDate != undefined)
                                            $('#inputreceivechaque-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.ChequeDate);
                                        $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.Balance, 2);

                                        var htmltag;
                                        htmltag = "<div class='title' style='width:220px;' >รายละเอียดการชำระ </div> <hr class='mb-2'> ";
                                        htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ยอดที่เรียกเก็บ </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.PaymentAmount + " บาท</div></div>";
                                        if ($scope.ReceiptAdd.ReceiptInfo.IsDiscount == '1')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + $scope.ReceiptAdd.ReceiptInfo.DiscountTypeName + " </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.DiscountAmount + " บาท</div></div> ";
                                        if ($scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax == '1')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ภาษีหัก ณ ที่จ่าย </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount + " บาท</div> </div>";
                                        if ($scope.ReceiptAdd.ReceiptInfo.BalanceType == 'Lost')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + $scope.ReceiptAdd.ReceiptInfo.CauseTypeName.replace('/เงินเกิน', '') + " </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.Balance + " บาท</div> </div>";
                                        if ($scope.ReceiptAdd.ReceiptInfo.BalanceType == 'Over')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + $scope.ReceiptAdd.ReceiptInfo.CauseTypeName.replace('เงินขาด/', '') + " </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.Balance + " บาท</div> </div>";
                                        htmltag = htmltag + " <hr class='mb-2'><div class='row'> <div class='col-lg-6'> ยอดรับชำระ </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount + " บาท</div> </div>";
                                        $scope.ReceiptAdd.ReceiptInfo.TagHtml = htmltag;
                                    }

                                    if (QueryString('succes_redic') != undefined && QueryString('succes_redic') == '1')
                                        showSuccessToast();

                                    $scope.OnLoadAttach();

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else if (QueryString('ref_key') != undefined && QueryString('ref_info') != undefined) {
                    // New document With Quotation
                    $http.get(baseURL + "DocumentSell/GetBusinessDcoumentReference?DocumentKey=" + QueryString('ref_key') + "&DocumentType=" + QueryString('ref_info') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.ReceiptAdd = [];
                                    $scope.ReceiptAdd = response.data.responsedata;
                                    $scope.ReceiptAdd.ReceiptDetail = [];

                                    if (QueryString('ref_info') == 'INV') {
                                        $scope.ReceiptAdd.DocumentKey = $scope.ReceiptAdd.InvoiceKey;
                                        $scope.ReceiptAdd.DocumentNo = $scope.ReceiptAdd.InvoiceNo;
                                        $scope.ReceiptAdd.ReferenceNo = $scope.ReceiptAdd.InvoiceNo;

                                        $scope.ReceiptAdd.ReceiptStatus = "1";
                                        $scope.ReceiptAdd.ReceiptStatusName = "รอดำเนินการ";

                                        _.each($scope.ReceiptAdd.InvoiceDetail, function (item) {
                                            item.ReceiptKey = undefined;
                                            item.ReceiptDetailKey = undefined;
                                            $scope.ReceiptAdd.ReceiptDetail.push(item);
                                        });

                                    }
                                    $scope.ReceiptAdd.DocumentOwner = QueryString('ref_info');

                                    /**  clear values */
                                    $scope.ReceiptAdd.ReceiptNo = data[0].data.responsedata;
                                    $scope.ReceiptAdd.ReceiptKey = undefined;

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                    if (type.length > 0)
                                        $scope.ReceiptAdd.SelectSale = type[0];


                                    $scope.ReceiptAdd.ReceiptDate = GetDatetimeNow();
                                    $('#inputreceiptdate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptDate);

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.ReceiptAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.ReceiptAdd.SelectSale = type[0];

                                    $scope.ReceiptAdd.IsTaxHeader = $scope.ReceiptAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                    $scope.ReceiptAdd.IsDiscountHeader = $scope.ReceiptAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                    $scope.ReceiptAdd.IsTaxSummary = $scope.ReceiptAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                    // หักภาษี ณ ที่จ่าย
                                    $scope.ReceiptAdd.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax == '1' ? true : false;
                                    if ($scope.ReceiptAdd.IsWithholdingTax) {

                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.ReceiptAdd.WithholdingKey; });
                                        if (type.length > 0)
                                            $scope.ReceiptAdd.SelectWithholding = type[0];

                                        $scope.ReceiptAdd.IsWithholdingTax.WithholdingAmount = AFormatNumber($scope.ReceiptAdd.IsWithholdingTax.WithholdingAmount, 2);
                                        $scope.ReceiptAdd.IsWithholdingTax.GrandAmountAfterWithholding = AFormatNumber($scope.ReceiptAdd.IsWithholdingTax.GrandAmountAfterWithholding, 2);
                                    }
                                    else {
                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                        if (type.length > 0)
                                            $scope.ReceiptAdd.SelectWithholding = type[0];

                                    }

                                    $scope.ReceiptAdd.IsAdjustDiscount = false; // ส่วนลดเพิ่มเติม

                                    $scope.ReceiptAdd.TotalAmount = AFormatNumber($scope.ReceiptAdd.TotalAmount, 2);
                                    $scope.ReceiptAdd.DiscountPercent = AFormatNumber($scope.ReceiptAdd.DiscountPercent, 2);
                                    $scope.ReceiptAdd.DiscountAmount = AFormatNumber($scope.ReceiptAdd.DiscountAmount, 2);
                                    $scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber($scope.ReceiptAdd.TotalAfterDiscountAmount, 2);
                                    $scope.ReceiptAdd.ExemptAmount = AFormatNumber($scope.ReceiptAdd.ExemptAmount, 2);
                                    $scope.ReceiptAdd.VatableAmount = AFormatNumber($scope.ReceiptAdd.VatableAmount, 2);
                                    $scope.ReceiptAdd.VAT = AFormatNumber($scope.ReceiptAdd.VAT, 2);
                                    $scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber($scope.ReceiptAdd.TotalBeforeVatAmount, 2);
                                    $scope.ReceiptAdd.GrandAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);

                                    $scope.ReceiptAdd.Signature == $scope.ReceiptAdd.Signature == '1' ? true : false;

                                    _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {
                                        item.Quantity = AFormatNumber(item.Quantity, 2);
                                        item.Price = AFormatNumber(item.Price, 2);
                                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.Discount = AFormatNumber(item.Discount, 2);
                                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                        item.VatRate = AFormatNumber(item.VatRate, 2);
                                        item.Vat = AFormatNumber(item.Vat, 2);
                                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                        item.Total = AFormatNumber(item.Total, 2);
                                    });

                                    $scope.OnClickClearDetail();
                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else {
                    /******* Initail ********/
                    $scope.ReceiptAdd.ReceiptDate = GetDatetimeNow();
                    $scope.ReceiptAdd.TaxType = 'N';

                    $('#inputreceiptdate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptDate);

                    
                    $scope.ReceiptAdd.IsTaxHeader = false; //แยกรายการภาษี
                    $scope.ReceiptAdd.IsDiscountHeader = false; //แยกรายการาส่วนลด
                    $scope.ReceiptAdd.IsWithholdingTax = false; // หักภาษี ณ ที่จ่าย
                    $scope.ReceiptAdd.IsAdjustDiscount = false; // ส่วนลดเพิ่มเติม

                    $scope.ReceiptAdd.IsTaxSummary = true; // หักภาษี 7 % รวม
                    $scope.ReceiptAdd.IsDiscountHeaderType = '%'; // ส่วนลดแยกรายการแบบ Percent

                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                    if (type.length > 0)
                        $scope.ReceiptAdd.SelectSale = type[0];

                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                    if (type.length > 0)
                        $scope.ReceiptAdd.SelectWithholding = type[0];

                    $scope.OnClickClearDetail();

                    $scope.ReceiptAdd.ReceiptDetail = [];
                    $scope.OnChangeDetailCalculate();

                    $scope.table.binding = 0;
                }
            }
            catch (err) {
                showErrorToast(err);
            }
        });
    };

    $scope.OnClickCancel = function () {
        if (QueryString('ref_info') == 'QT')
            window.location.href = baseURL + "DocumentSell/Quotation";
        else if (QueryString('ref_info') == 'INV')
            window.location.href = baseURL + "DocumentSell/Invoice";
        else if (QueryString('ref_report') != undefined)
            window.location.href = baseURL + "Report/" + QueryString('ref_report');
        else
            window.location.href = baseURL + "DocumentSell/Receipt";
    };

    $scope.OnDisabled = function () {
        if ($scope.ReceiptAdd.ReceiptStatus == undefined || $scope.ReceiptAdd.ReceiptStatus == '1')
            return false;
        else
            return true;
    };

    $scope.OnChangeReceiptDate = function () {
        var prefix = { asofdate: $scope.ReceiptAdd.ReceiptDate, documenttype: headerdoc };
        var qq = $q.all([paramService.getDocumentNo(prefix)]).then(function (data) {
            /****** GetDocument NO ******/
            if (data[0] != undefined && data[0] != "") {
                if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.ReceiptAdd.ReceiptNo = data[0].data.responsedata; }
                else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
            }
        });
    };

    $scope.OnClickReportContent = function (type) {
        $scope.Print = "1";
        $scope.document = [];
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $scope.OnClickSave();
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'RE',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {

        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'RE',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    /*********** Header Setting Discount , Tax**************/
    $scope.OnChangeIsTaxHeader = function () {
        if ($scope.ReceiptAdd.IsTaxHeader) {
            $scope.ReceiptAdd.IsDiscountHeader = true;
        }
        $scope.OnChangeDetailCalculate();
    };
    $scope.OnChangeIsDiscountHeader = function () {
        $scope.ReceiptAdd.DiscountPercent = '0';
        $scope.ReceiptAdd.DiscountAmount = '0';
        $scope.OnClickDiscountType($scope.ReceiptAdd.IsDiscountHeaderType);
    };
    /******************* Detail And Calculate *************************/
    $scope.OnClickProductDetailAdd = function () {
        try {
            if ($scope.ProductDetailAdd.Name == undefined || $scope.ProductDetailAdd.Name == "")
                throw "กรุณากรอกชื่อสินค้า";
            else if ($scope.ProductDetailAdd.Price == undefined || $scope.ProductDetailAdd.Price == "")
                throw "กรุณากรอกราคาต่อหน่วย";
            else if ($scope.ProductDetailAdd.Quantity == undefined || $scope.ProductDetailAdd.Quantity == "")
                throw "กรุณากรอกจำนวน";
            else {
                $scope.ProductDetailAdd.Sequence = $scope.ReceiptAdd.ReceiptDetail.length + 1;
                $scope.ProductDetailAdd.DiscountType = $scope.ReceiptAdd.IsDiscountHeaderType;

                if ($scope.ProductDetailAdd.ProductKey == undefined) {
                    $scope.ProductDetailAdd.DiscountType = $scope.ReceiptAdd.IsDiscountHeaderType;
                    $scope.ProductDetailAdd.DiscountPercent = '0.00';
                    $scope.ProductDetailAdd.Discount = '0.00';
                    $scope.ProductDetailAdd.VatType = '2';

                    //var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                    var sell = 0, selltax = 0, selllast = 0;
                    sell = ConvertToDecimal($scope.ProductDetailAdd.Price == undefined ? 0 : $scope.ProductDetailAdd.Price);
                    //selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                    //selllast = sell + selltax;

                    $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.PriceTax = AFormatNumber(0, 2);
                }

                if ($scope.ReceiptAdd.TaxType == 'N')
                    $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
                else if ($scope.ReceiptAdd.TaxType == 'V')
                    $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
                $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            }

            $scope.ReceiptAdd.ReceiptDetail.push($scope.ProductDetailAdd);
            $scope.OnClickClearDetail();
            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickProductDetailDelete = function (index) {
        try {
            $scope.ReceiptAdd.ReceiptDetail.splice(index, 1);
            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickClearDetail = function () {
        $scope.ProductDetailAdd = [];
        $scope.ProductDetailAdd.DiscountType = $scope.ReceiptAdd.IsDiscountHeaderType;
        $scope.ProductDetailAdd.VatType = '2';
    };

    $scope.OnClickDiscountType = function (val) {
        $scope.ReceiptAdd.IsDiscountHeaderType = val;
        _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {
            item.DiscountType = val;
            item.DiscountPercent = item.Discount = AFormatNumber(0, 2);
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountAmount = function () {
        $scope.ReceiptAdd.DiscountPercent = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickNoTaxSummary = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickWithholdingTax = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickAdjustDiscount = function () {
        $scope.ReceiptAdd.AdjustDiscountType = '1';
        $scope.ReceiptAdd.AdjustDiscountAmount = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeProductTaxType = function () {
        _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {
            if ($scope.ReceiptAdd.TaxType == 'N')
                item.Price = item.PriceBeforeTax;
            else if ($scope.ReceiptAdd.TaxType == 'V')
                item.Price = item.PriceAfterTax;
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountPercent = function () {
        if ($scope.ReceiptAdd.DiscountPercent == undefined || $scope.ReceiptAdd.DiscountPercent === '' || $scope.ReceiptAdd.DiscountPercent == 0)
            $scope.ReceiptAdd.DiscountAmount = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDetailCalculate = function () {
        try {
            _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {
                var amt, qty, total, vatrate, vatamt, discountper, discountminus, discount, discountafter, vatafter = 0, totalamount, vatcal = 0;
                // Get Values
                amt = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                vat = item.VatType == '1' ? 0 : item.VatType == '2' ? 7 : 0; // dropdown vat หน้าจอ
                discountper = item.DiscountPercent == undefined || item.DiscountPercent === '' ? '0' : detectFloat(item.DiscountPercent); //ส่วนลด % จากหน้าจอ
                discount = item.Discount == undefined || item.Discount === '' ? '0' : detectFloat(item.Discount); //ส่วนลดจากหน้าจอ
                discountminus = vatafter = 0; // ราคาส่วนลด
                discountafter = 0; // หลังหักส่วนลด
                vatafter = 0;

                total = amt * qty;
                // Calculate
                if ($scope.ReceiptAdd.IsDiscountHeader == true) { //เงื่อนไขหักส่วนลด
                    if (item.DiscountType == '%') {
                        discountminus = total * discountper / 100;
                        discountafter = detectFloat(AFormatNumber(total - discountminus, 2));
                    }
                    else if (item.DiscountType == '฿') {
                        discountminus = discount;
                        discountafter = total - discount;
                    }
                }
                else {
                    discountafter = total;
                    discountper = discount = 0;
                }

                if ($scope.ReceiptAdd.IsTaxHeader == true) { //คำนวณหาภาษี ของแต่ละรายการ
                    if (item.VatType == '2') {
                        if ($scope.ReceiptAdd.TaxType == 'N') {

                            vatcal = vat > 0 ? ((discountafter * (100 + vat) / 100)) : 0;
                            vatamt = detectFloat(AFormatNumber(vatcal, 2)) - discountafter;
                            vatafter = discountafter + vatamt;
                        }
                        else if ($scope.ReceiptAdd.TaxType == 'V') {
                            vatcal = vat > 0 ? ((discountafter / (100 + vat) * 100)) : 0;
                            vatamt = discountafter - detectFloat(AFormatNumber(vatcal, 2));
                            vatafter = discountafter - vatamt;
                        }
                    }
                    else
                        vatamt = 0;
                }
                else {
                    item.VatType = '2';
                    vatamt = 0;
                }

                totalamount = discountafter;

                // Set Value
                item.Discount = AFormatNumber(discountminus, 2);
                item.DiscountAfter = AFormatNumber(discountafter, 2);
                item.VatRate = AFormatNumber(vat, 2);
                item.Vat = AFormatNumber(vatamt, 2);
                item.VatAfter = AFormatNumber(vatafter, 2);
                item.Total = AFormatNumber(totalamount, 2);
            });

            /******* หาค่ารวม Summary ***********/
            var price = 0, priceedis = 0, pricevat = 0, qty = 0, totalprice = 0, total = 0, discounthper = 0, discounthamt = 0, priceafterdiscount = 0, priceqty = 0,
                discounth = 0, grandtotal = 0 ,exemptamt = 0, vatableamt = 0, vat = 0, withholdingrate = 0, withholdingprice = 0, withholdingafter = 0, vatafter = 0, adjustamount = 0, adjustafter = 0, paymantamount = 0;


            discounthper = $scope.ReceiptAdd.DiscountPercent == undefined || $scope.ReceiptAdd.DiscountPercent === '' ? 0 : detectFloat($scope.ReceiptAdd.DiscountPercent); // Discount Percenter
            discounthamt = $scope.ReceiptAdd.DiscountAmount == undefined || $scope.ReceiptAdd.DiscountAmount === '' ? 0 : $scope.ReceiptAdd.IsTaxHeader == false ? detectFloat($scope.ReceiptAdd.DiscountAmount) : 0; // Discount Amount

            if ($scope.ReceiptAdd.ReceiptDetail != undefined && $scope.ReceiptAdd.ReceiptDetail.length > 0) {
                _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {

                    price = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                    priceedis = item.Discount == undefined || item.Discount === '' ? 0 : detectFloat(item.Discount); // ส่วนลดคำนวณ
                    qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                    pricevat = item.Vat == undefined || item.Vat === '' ? 0 : detectFloat(item.Vat); // จำนวน จากหน้าจอ

                    totalprice = totalprice + price * qty;//รวมเป็นเงิน
                    discounth = discounth + priceedis; //ส่วนลดรวม
                    priceqty = price * qty;

                    if ($scope.ReceiptAdd.IsTaxHeader) { // หักภาษี แบบทีละรายการ
                        if (item.VatType == '1' || item.VatType == '3') //0% ยกเว้น
                            exemptamt = exemptamt + priceqty - priceedis;
                        else if (item.VatType == '2') // 7%
                            vatableamt = (vatableamt + priceqty - priceedis) - ($scope.ReceiptAdd.TaxType == 'V' ? pricevat : 0); //ถ้าเลือกรวมภาษีแล้ว ให้หัก ภาษีออก

                        vat = vat + pricevat;
                    }
                });

                /**** หักแบบ Summary ***/
                if ($scope.ReceiptAdd.IsDiscountHeader == false) {
                    if (discounthper > 0)
                        discounthamt = totalprice * discounthper / 100;
                    discounth = discounthamt;
                }

                priceafterdiscount = totalprice - discounth; //รวมเป็นเงิน - หักส่วนลด = ราคาหลังหักส่วนลด

                /*** หักภาษีแบบ Summary **/
                if (!$scope.ReceiptAdd.IsTaxHeader && $scope.ReceiptAdd.IsTaxSummary) {
                    if ($scope.ReceiptAdd.TaxType == 'N') {
                        vatcal = (priceafterdiscount * (100 + 7) / 100);
                        vatamt = vatcal - priceafterdiscount;
                        vatafter = priceafterdiscount + vatamt;
                    }
                    else if ($scope.ReceiptAdd.TaxType == 'V') {
                        vatcal = (priceafterdiscount / (100 + 7) * 100);
                        vatamt = priceafterdiscount - vatcal;
                        vatafter = priceafterdiscount - vatamt;
                    }
                    vat = vatamt;
                }

                grandtotal = totalprice - discounth + ($scope.ReceiptAdd.TaxType == 'N' ? vat : 0); //จำนวนเงินรวมทั้งสิ้น

                if ($scope.ReceiptAdd.IsWithholdingTax && !$scope.ReceiptAdd.IsTaxHeader) {

                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.ReceiptAdd.SelectWithholding.ID; });
                    if (type.length > 0)
                        withholdingrate = type[0].Rate;

                    if ($scope.ReceiptAdd.TaxType == 'N')
                        withholdingprice = priceafterdiscount * withholdingrate / 100;
                    else if ($scope.ReceiptAdd.TaxType == 'V')
                        withholdingprice = vatafter * withholdingrate / 100;

                    withholdingafter = $scope.ReceiptAdd.IsTaxSummary ? (grandtotal - withholdingprice) : (priceafterdiscount - withholdingprice);
                    paymantamount = withholdingafter;
                }

                if ($scope.ReceiptAdd.IsAdjustDiscount) {
                    discounthper = $scope.ReceiptAdd.AdjustDiscountAmount == undefined || $scope.ReceiptAdd.AdjustDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.AdjustDiscountAmount); // Discount Percenter
                    adjustafter = $scope.ReceiptAdd.IsWithholdingTax ? (paymantamount - discounthper) : (grandtotal - discounthper);
                    paymantamount = adjustafter;
                }

                $scope.ReceiptAdd.TotalAmount = AFormatNumber(totalprice, 2);
                $scope.ReceiptAdd.DiscountAmount = AFormatNumber(discounth, 2);
                $scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber(priceafterdiscount, 2);
                $scope.ReceiptAdd.ExemptAmount = AFormatNumber(exemptamt, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.ReceiptAdd.VatableAmount = AFormatNumber(vatableamt, 2);//มูลค่าที่คำนวณภาษี
                $scope.ReceiptAdd.VAT = AFormatNumber(vat, 2);//ภาษีมูลค่าเพิ่ม
                $scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber(vatafter, 2);//จำนวนเงินรวมภาษี
                $scope.ReceiptAdd.GrandAmount = AFormatNumber(grandtotal, 2);//
                $scope.ReceiptAdd.WithholdingAmount = AFormatNumber(withholdingprice, 2);//
                $scope.ReceiptAdd.PaymentAmount = AFormatNumber(paymantamount, 2);//
            }
            else {
                $scope.ReceiptAdd.TotalAmount = AFormatNumber(0, 2);
                $scope.ReceiptAdd.DiscountPercent = AFormatNumber(0, 2);
                $scope.ReceiptAdd.DiscountAmount = AFormatNumber(0, 2);
                $scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                $scope.ReceiptAdd.ExemptAmount = AFormatNumber(0, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.ReceiptAdd.VatableAmount = AFormatNumber(0, 2);//มูลค่าที่คำนวณภาษี
                $scope.ReceiptAdd.VAT = AFormatNumber(0, 2);//ภาษีมูลค่าเพิ่ม
                $scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);//จำนวนเงินรวมภาษี
                $scope.ReceiptAdd.GrandAmount = AFormatNumber(0, 2);//
                $scope.ReceiptAdd.WithholdingAmount = AFormatNumber(0, 2);//
                $scope.ReceiptAdd.PaymentAmount = AFormatNumber(0, 2);//
            }


        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickSave = function (exit) {
        try {
            if ($scope.ReceiptAdd.CustomerName == undefined || $scope.ReceiptAdd.CustomerName == "")
                throw "กรุณาเลือกผู้ติดต่อ";
            else {

                if ($scope.ReceiptAdd.IsAdjustDiscount) {
                    var adjust = $scope.ReceiptAdd.PaymentAmount == undefined || $scope.ReceiptAdd.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.PaymentAmount); // Discount Percenter
                    if (adjust < 0)
                        throw "ยอดชำระติดลบ กรุณาตรวจสอบยอดชำระอีกครั้ง";
                }

                var adjusttype;
                if ($scope.ReceiptAdd.AdjustDiscountType == '1')
                    adjusttype = 'ส่วนลดพิเศษ';
                else if ($scope.ReceiptAdd.AdjustDiscountType == '2')
                    adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
                else if ($scope.ReceiptAdd.AdjustDiscountType == '3')
                    adjusttype = 'ค่าดำเนินการ';
                else if ($scope.ReceiptAdd.AdjustDiscountType == '4')
                    adjusttype = 'ปัดเศษ';

                if ($scope.ReceiptAdd.AdjustDiscountAmount <= 0)
                    $scope.ReceiptAdd.IsAdjustDiscount = false;

                var data = {
                    ReceiptKey: $scope.ReceiptAdd.ReceiptKey,
                    ReceiptNo: $scope.ReceiptAdd.ReceiptNo,
                    ReceiptStatus: $scope.ReceiptAdd.ReceiptStatus,
                    ReceiptStatusName: $scope.ReceiptAdd.ReceiptStatusName,
                    ReceiptType: 'RE',
                    ReceiptTypeName: 'ใบเสร็จรับเงิน',
                    CustomerKey: $scope.ReceiptAdd.CustomerKey,
                    CustomerName: $scope.ReceiptAdd.CustomerName,
                    CustomerTaxID: $scope.ReceiptAdd.CustomerTaxID,
                    CustomerContactName: $scope.ReceiptAdd.CustomerContactName,
                    CustomerContactPhone: $scope.ReceiptAdd.CustomerContactPhone,
                    CustomerContactEmail: $scope.ReceiptAdd.CustomerContactEmail,
                    CustomerAddress: $scope.ReceiptAdd.CustomerAddress,
                    CustomerBranch: $scope.ReceiptAdd.CustomerBranch,
                    ReceiptDate: ToJsonDate2($scope.ReceiptAdd.ReceiptDate),
                    SaleID: $scope.ReceiptAdd.SelectSale.UID,
                    SaleName: $scope.ReceiptAdd.SelectSale.EmployeeName,
                    ProjectName: $scope.ReceiptAdd.ProjectName,
                    ReferenceNo: $scope.ReceiptAdd.ReferenceNo,
                    TaxType: $scope.ReceiptAdd.TaxType,
                    TaxTypeName: $scope.ReceiptAdd.TaxType == 'N' ? 'ราคาไม่รวมภาษี' : $scope.ReceiptAdd.TaxType == 'V' ? 'ราคารวมภาษี' : '',
                    TotalAmount: ConvertToDecimal($scope.ReceiptAdd.TotalAmount),
                    IsDiscountHeader: $scope.ReceiptAdd.IsDiscountHeader ? '1' : '0',
                    IsDiscountHeaderType: $scope.ReceiptAdd.IsDiscountHeaderType,
                    DiscountPercent: $scope.ReceiptAdd.DiscountPercent,
                    DiscountAmount: $scope.ReceiptAdd.DiscountAmount,
                    TotalAfterDiscountAmount: $scope.ReceiptAdd.TotalAfterDiscountAmount,
                    IsTaxHeader: $scope.ReceiptAdd.IsTaxHeader ? '1' : '0',
                    IsTaxSummary: $scope.ReceiptAdd.IsTaxSummary ? '1' : '0',
                    ExemptAmount: $scope.ReceiptAdd.ExemptAmount,
                    VatableAmount: $scope.ReceiptAdd.VatableAmount,
                    VAT: $scope.ReceiptAdd.VAT,
                    TotalBeforeVatAmount: $scope.ReceiptAdd.TotalBeforeVatAmount,
                    IsWithholdingTax: $scope.ReceiptAdd.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.ReceiptAdd.IsWithholdingTax ? $scope.ReceiptAdd.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.ReceiptAdd.IsWithholdingTax ? $scope.ReceiptAdd.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.ReceiptAdd.IsWithholdingTax ? $scope.ReceiptAdd.WithholdingAmount : undefined,
                    IsAdjustDiscount: $scope.ReceiptAdd.IsAdjustDiscount ? '1' : '0',
                    AdjustDiscountType: $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined,
                    AdjustDiscountTypeName: $scope.ReceiptAdd.IsAdjustDiscount ? adjusttype : undefined,
                    AdjustDiscountAmount: $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : undefined,
                    PaymentAmount: $scope.ReceiptAdd.IsWithholdingTax || $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.PaymentAmount : undefined,
                    GrandAmount: $scope.ReceiptAdd.GrandAmount,
                    Remark: $scope.ReceiptAdd.Remark,
                    Signature: $scope.ReceiptAdd.Signature ? '1' : '0',
                    Noted: $scope.ReceiptAdd.Noted,
                    DocumentKey: $scope.ReceiptAdd.DocumentKey,
                    DocumentNo: $scope.ReceiptAdd.DocumentNo,
                    DocumentOwner: $scope.ReceiptAdd.DocumentOwner
                };

                data.ReceiptDetail = [];
                _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {

                    var tmpQT = {
                        Discount: ConvertToDecimal(item.Discount),
                        DiscountAfter: ConvertToDecimal(item.DiscountAfter),
                        DiscountPercent: ConvertToDecimal(item.DiscountPercent),
                        DiscountType: item.DiscountType,
                        Name: item.Name,
                        Unit: item.Unit,
                        Description: item.Description,
                        Price: ConvertToDecimal(item.Price),
                        PriceAfterTax: ConvertToDecimal(item.PriceAfterTax),
                        PriceBeforeTax: ConvertToDecimal(item.PriceBeforeTax),
                        PriceTax: ConvertToDecimal(item.PriceTax),
                        ProductKey: parseInt(item.ProductKey),
                        Quantity: ConvertToDecimal(item.Quantity),
                        Sequence: parseInt(item.Sequence),
                        Total: ConvertToDecimal(item.Total),
                        Vat: ConvertToDecimal(item.Vat),
                        VatAfter: ConvertToDecimal(item.VatAfter),
                        VatRate: ConvertToDecimal(item.VatRate),
                        VatType: item.VatType
                    };


                    data.ReceiptDetail.push(tmpQT);
                });

                $scope.table.binding = 1;
                $http.post(baseURL + "DocumentSell/PostBusinessReceipt", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseReceipt = response.data.responsedata;
                                    if (responseReceipt != undefined) {

                                        if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'QT' && exit == 1)
                                            window.location.href = baseURL + "DocumentSell/Quotation";
                                        else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'INV' && exit == 1)
                                            window.location.href = baseURL + "DocumentSell/Invoice";
                                        else if (QueryString('ref_key') != undefined && (QueryString('ref_info') == 'QT' || QueryString('ref_info') == 'INV'))
                                            window.location.href = baseURL + "DocumentSell/ReceiptAdd?ReceiptKey=" + responseReceipt.ReceiptKey + "&succes_redic=1";
                                        else if (exit == 1)
                                            window.location.href = baseURL + "DocumentSell/Receipt";
                                        else {
                                            $scope.ReceiptAdd.ReceiptKey = responseReceipt.ReceiptKey;
                                            $scope.ReceiptAdd.ReceiptStatus = responseReceipt.ReceiptStatus;
                                            $scope.ReceiptAdd.ReceiptStatusName = responseReceipt.ReceiptStatusName;
                                            $scope.ReceiptAdd.CustomerKey = responseReceipt.CustomerKey;

                                            _.each($scope.ReceiptAdd.ReceiptDetail, function (item) {
                                                var detail = _.where(responseReceipt.ReceiptDetail, { Sequence: item.Sequence })[0];
                                                if (detail != null) {
                                                    item.ProductKey = detail.ProductKey;
                                                    item.ReceiptKey = detail.ReceiptKey;
                                                    item.ReceiptDetailKey = detail.ReceiptDetailKey;
                                                }
                                            });

                                            $scope.table.binding = 0;
                                            showSuccessToast();

                                            if ($scope.ReceiptAdd.ReceiptKey != undefined && $scope.Print == "1") {

                                                $scope.document.DocKey = $scope.ReceiptAdd.ReceiptKey;
                                                $scope.document.DocNo = $scope.ReceiptAdd.ReceiptNo;
                                                $scope.document.original = $scope.document.copy = true;
                                                $('#modal-content-report').modal('show');
                                            }
                                            $scope.Print = "0";
                                        }
                                    }
                                }
                                else {
                                    $scope.table.binding = 0;
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickPaymentAdd = function () {

        if ($scope.ReceiptAdd.IsWithholdingTax != $scope.ReceiptAdd.IsWithholdingTaxFix || $scope.ReceiptAdd.IsAdjustDiscount != $scope.ReceiptAdd.IsAdjustDiscountFix)
            $scope.OnClickSave('');

        $scope.ReceiptAdd.ReceiptInfo = [];
        $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt = [];
        $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push({ ID: '0', Name: 'จำนวนเงิน', Rate: 0 });
        _.each($scope.Parameter.Param_Quotation.Param_WithholdingRate, function (item) { $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push(item); });


        $scope.ReceiptAdd.ReceiptInfo.DocumentKey = $scope.ReceiptAdd.ReceiptKey;
        $scope.ReceiptAdd.ReceiptInfo.DocumentNo = $scope.ReceiptAdd.ReceiptNo;
        $scope.ReceiptAdd.ReceiptInfo.DocumentType = headerdoc;
        $scope.ReceiptAdd.ReceiptInfo.CustomerName = $scope.ReceiptAdd.CustomerName;
        $scope.ReceiptAdd.ReceiptInfo.PaymentAmount = $scope.ReceiptAdd.GrandAmount;
        $scope.ReceiptAdd.ReceiptInfo.PaymentDate = GetDatetimeNow();

        $('#inputreceivedate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.PaymentDate);
        $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.Balance, 2);

        $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = $scope.ReceiptAdd.IsAdjustDiscount || $scope.ReceiptAdd.IsWithholdingTax ? $scope.ReceiptAdd.PaymentAmount : $scope.ReceiptAdd.GrandAmount;
        $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax;
        if ($scope.ReceiptAdd.IsWithholdingTax) {
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.SelectWithholding.ID; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];

            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.WithholdingAmount, 2) : AFormatNumber(0, 2);
        }

        if ($scope.ReceiptAdd.IsTaxHeader) {
            
            $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax = false;
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
            
            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        }

        $scope.ReceiptAdd.ReceiptInfo.IsDiscount = $scope.ReceiptAdd.IsAdjustDiscount;
        $scope.ReceiptAdd.ReceiptInfo.DiscountType = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined;
        $scope.ReceiptAdd.ReceiptInfo.DiscountTypeName = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountTypeName : undefined;
        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : AFormatNumber(0, 2);
        $scope.ReceiptAdd.ReceiptInfo.PaymentType = "CA";
        $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(0, 2);
        $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
        $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
        $scope.ReceiptAdd.ReceiptInfo.IsPayment = true;

        $('#modalreceipt-receive').modal('show');
    };

    $scope.OnChangeIsAdjustReceive = function () {
        $scope.ReceiptAdd.ReceiptInfo.DiscountType = '1';
        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = AFormatNumber(0, 2);
    };

    $scope.OnChangeIsWithhollding = function () {
        if ($scope.ReceiptAdd.IsTaxHeader == '1') {
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
        }
        else {
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '1'; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
        }
        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    };

    $scope.OnChangeWithholldingReceipt = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID == '0') {
            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            $scope.OnChangeReceiveCalculate();
        }
        else {
            $scope.OnChangeReceiveCalculate();
        }
    };

    $scope.OnChangePaymentType = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR') {
            var type = $scope.Parameter.BizBank.filter(function (item) { return item.UID == $scope.Parameter.BizBank[0].UID; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer = type[0];
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
            $scope.ReceiptAdd.ReceiptInfo.ChequeDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiptInfo.ChequeNo = '';
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
        }
    };

    $scope.OnClickIsPayment = function () {
        $scope.ReceiptAdd.ReceiptInfo.IsPayment = false;
        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
        if (type.length > 0)
            $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
    };

    $scope.OnChangeReceiveCalculate = function () {
        try {
            var grandtotal = 0, netpayment = 0, discount = 0, withholding = 0, netgrand = 0, pricediscount = 0, pricevat, balance = 0;
            grandtotal = $scope.ReceiptAdd.ReceiptInfo.PaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.PaymentAmount); // Discount Percenter
            pricediscount = $scope.ReceiptAdd.TotalAfterDiscountAmount == undefined || $scope.ReceiptAdd.TotalAfterDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalAfterDiscountAmount); // Discount Percenter
            pricevat = $scope.ReceiptAdd.TotalBeforeVatAmount == undefined || $scope.ReceiptAdd.TotalBeforeVatAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalBeforeVatAmount); // Discount Percenter
            netpayment = $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount); // Discount Percenter
            discount = $scope.ReceiptAdd.ReceiptInfo.DiscountAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.DiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.DiscountAmount); // Discount Percenter
            withholding = $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.WithholdingAmount); // Discount Percenter


            if ($scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID; });
                if (type.length > 0)
                    withholdingrate = type[0].Rate;

                if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID == '0')
                    withholding = withholding;
                else {
                    if ($scope.ReceiptAdd.TaxType == 'N')
                        withholding = pricediscount * withholdingrate / 100;
                    else if ($scope.ReceiptAdd.TaxType == 'V')
                        withholding = pricevat * withholdingrate / 100;
                }

                grandtotal = grandtotal - withholding;
            }
            else
                grandtotal = grandtotal;

            grandtotal = grandtotal - discount;

            if (!$scope.ReceiptAdd.ReceiptInfo.IsPayment) {
                balance = netpayment - grandtotal;
            }
            else
                netpayment = grandtotal;

            if (balance < 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินขาด';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Lost';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
                balance = balance * (-1);
            }
            else if (balance > 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินเกิน';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Over';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }
            else {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
                $scope.ReceiptAdd.ReceiptInfo.IsText = '';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Equal';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }

            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(withholding, 2);//
            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber(netpayment, 2);//
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(balance, 2);//
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickReceiptPayment = function () {
        try {
            if (!$scope.ReceiptAdd.ReceiptInfo.IsCauseType && $scope.ReceiptAdd.ReceiptInfo.CauseType == undefined)
                showWariningToast("กรุณาระบุสาเหตุ เงินขาด/เงินเกิน");
            else {
                var adjusttype;
                if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '1')
                    adjusttype = 'ส่วนลดพิเศษ';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '2')
                    adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '3')
                    adjusttype = 'ค่าดำเนินการ';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '4')
                    adjusttype = 'ปัดเศษ';

                var paymenttype;
                if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CA')
                    paymenttype = 'เงินสด';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR')
                    paymenttype = 'โอนเงิน';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ')
                    paymenttype = 'เช็ค';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC')
                    paymenttype = 'บัตรเครดิต';

                var cause;
                if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '1')
                    cause = 'ค่าธรรมเนียม';
                else if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '2')
                    cause = 'เงินขาด/เงินเกิน';

                var data = {
                    DocumentKey: $scope.ReceiptAdd.ReceiptKey,
                    DocumentNo: $scope.ReceiptAdd.ReceiptNo,
                    DocumentType: headerdoc,
                    CustomerName: $scope.ReceiptAdd.ReceiptInfo.CustomerName,
                    PaymentAmount: $scope.ReceiptAdd.ReceiptInfo.PaymentAmount,
                    PaymentDate: ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.PaymentDate),
                    NetPaymentAmount: ConvertToDecimal($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount),
                    IsWithholdingTax: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount : undefined,
                    IsDiscount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? '1' : '0',
                    DiscountType: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountType : undefined,
                    DiscountTypeName: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? adjusttype : undefined,
                    DiscountAmount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountAmount : undefined,
                    PaymentType: $scope.ReceiptAdd.ReceiptInfo.PaymentType,
                    PaymentTypeName: paymenttype,
                    BankAccountID: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.UID : undefined,
                    BankCode: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBank.ID : undefined,
                    BankName: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.BankName :
                        $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' || $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBank.Name : undefined,
                    BankAccountNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.AccountNo : undefined,
                    BankAccountType: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.DepositTypeName : undefined,
                    ChequeDate: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.ChequeDate) : undefined,
                    ChequeNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.ChequeNo : undefined,
                    Comment: $scope.ReceiptAdd.ReceiptInfo.Comment,
                    Balance: $scope.ReceiptAdd.ReceiptInfo.Balance,
                    BalanceType: $scope.ReceiptAdd.ReceiptInfo.BalanceType,
                    CauseType: $scope.ReceiptAdd.ReceiptInfo.CauseType,
                    CauseTypeName: cause
                };

                $http.post(baseURL + "DocumentSell/PostBusinessReceiptPayment", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseReceipt = response.data.responsedata;
                                    if (responseReceipt != undefined) {
                                        window.location.href = baseURL + "DocumentSell/ReceiptAdd?ReceiptKey=" + $scope.ReceiptAdd.ReceiptKey + "&succes_redic=1";
                                    }
                                }
                                else showErrorToast(response.data.errormessage);
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) { showErrorToast(err); }
    };

    /**************Select ContactBook **************/
    $scope.OnClearContact = function () {
        $scope.ReceiptAdd.CustomerKey = $scope.ReceiptAdd.CustomerName = $scope.ReceiptAdd.CustomerTaxID =
            $scope.ReceiptAdd.CustomerContactName = $scope.ReceiptAdd.CustomerContactPhone = $scope.ReceiptAdd.CustomerContactEmail =
            $scope.ReceiptAdd.CustomerAddress = $scope.ReceiptAdd.CustomerBranch = $scope.ReceiptAdd.CreditDay = undefined;
    };

    $scope.OnClickContactPopup = function () {
        $scope.SearchContact.InputFilter = [];
        $scope.contactcurrentPage = 0;
        $scope.contact = [];
        $scope.contact.binding = 0;
        $scope.contactretpage = [];
        $scope.contactrange();
        $('#modalcontact-list').modal('show');
    };

    $scope.OnClickContactSearch = function () {
        $scope.Parameter.Param_Contact = contactService.filterContact($scope.SearchContact.InputFilter, $scope.Parameter.Param_Contact, $scope.Parameter.Param_ContactMain);
        if ($scope.Parameter.Param_Contact == undefined)
            $scope.Parameter.Param_Contact = [];
    };

    $scope.OnClickContactSelection = function (contactkey) {
        var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: contactkey })[0];
        if (contact != undefined) {
            $scope.OnClearContact();
            $scope.ReceiptAdd.CustomerKey = contact.ContactKey;
            $scope.ReceiptAdd.CustomerName = contact.BusinessName;
            $scope.ReceiptAdd.CustomerTaxID = contact.TaxID;
            $scope.ReceiptAdd.CustomerContactName = contact.ContactName;
            $scope.ReceiptAdd.CustomerContactPhone = contact.ContactMobile;
            $scope.ReceiptAdd.CustomerContactEmail = contact.ContactEmail;
            $scope.ReceiptAdd.CustomerAddress = contact.Address;
            $scope.ReceiptAdd.CustomerBranch = contact.BranchName;
            $scope.ReceiptAdd.CreditDay = contact.CreditDate;
            $scope.OnChangeCredit();
            $('#modalcontact-list').modal('hide');
        }
    };

    $scope.OnClickContactAdd = function () {
        $scope.BusinessContactAdd = [];
        $scope.BusinessContactAdd.BusinessType = 'C';
        $scope.BusinessContactAdd.ContactType1 = true;
        $scope.BusinessContactAdd.ContactType2 = false;
        $scope.BusinessContactAdd.BranchType = "H";
        $scope.BusinessContactAdd.AccountType = 'S';

        var type = $scope.Parameter.Param_Quotation.Param_Bank.filter(function (item) { return item.ID == undefined; });
        if (type.length > 0)
            $scope.BusinessContactAdd.SelectBank = type[0];

        $('#modalcontact-add').modal('show');
    };

    $scope.OnClickContactSave = function () {
        try {
            if ($scope.BusinessContactAdd.BusinessName == undefined || $scope.BusinessContactAdd.BusinessName == "")
                throw "กรุณาระบุชื่อธุรกิจ";
            else if ($scope.BusinessContactAdd.ContactType1 == false && $scope.BusinessContactAdd.ContactType2 == false)
                throw "กรุณาระบุประเภท";
            else if ($scope.BusinessContactAdd.ContactEmail != undefined && $scope.BusinessContactAdd.ContactEmail != "" && !ValidateEmail($scope.BusinessContactAdd.ContactEmail))
                throw "กรุณาแก้ไขอีเมล เนื่องจากรูปแบบอีเมลไม่ถูกต้อง";
            else {
                var qq = $q.all([contactService.postNewContact($scope.BusinessContactAdd)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var contact = data[0].data.responsedata;
                                if (contact != undefined) {
                                    $scope.OnClearContact();
                                    $scope.ReceiptAdd.CustomerKey = contact.ContactKey;
                                    $scope.ReceiptAdd.CustomerName = contact.BusinessName;
                                    $scope.ReceiptAdd.CustomerTaxID = contact.TaxID;
                                    $scope.ReceiptAdd.CustomerContactName = contact.ContactName;
                                    $scope.ReceiptAdd.CustomerContactPhone = contact.ContactMobile;
                                    $scope.ReceiptAdd.CustomerContactEmail = contact.ContactEmail;
                                    $scope.ReceiptAdd.CustomerAddress = contact.Address;
                                    $scope.ReceiptAdd.CustomerBranch = contact.BranchName;
                                    $scope.ReceiptAdd.CreditDay = contact.CreditDate;
                                    $scope.OnChangeCredit();
                                }
                                $('#modalcontact-add').modal('hide');
                                showSuccessToast();

                                var q1 = $q.all([paramService.getCustomer()]).then(function (data) {
                                    if (data[0] != undefined && data[0] != "") {
                                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                            $scope.SearchContact = [];
                                            $scope.Parameter.Param_Contact = [];
                                            $scope.Parameter.Param_ContactMain = [];
                                            $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[0].data.responsedata;
                                        }
                                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                    }
                                });

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(response.data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.contactcurrentPage = 0;

    $scope.contactLimitFirst = 0;
    $scope.contactLimitPage = 5;
    $scope.contactitemsPerPage = 10;

    $scope.contactpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Contact.length / $scope.contactitemsPerPage) - 1;
    };

    $scope.contactrange = function () {
        $scope.contactitemsCount = $scope.Parameter.Param_Contact.length;
        $scope.contactpageshow = $scope.contactpageCount() > $scope.contactLimitPage && $scope.contactpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.contactpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.contactpageshowdata = 1;
        $scope.contactretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.contactpcontactPage = function () {
        if ($scope.contactcurrentPage > 0) {
            $scope.contactcurrentPage--;
        }

        if ($scope.contactcurrentPage < $scope.contactLimitFirst && $scope.contactcurrentPage >= 1) {
            $scope.contactLimitFirst = $scope.contactLimitFirst - 5;
            $scope.contactLimitPage = $scope.contactLimitPage - 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }
    };

    $scope.contactpcontactPageDisabled = function () {
        return $scope.contactcurrentPage === 0 ? "disabled" : "";
    };

    $scope.contactnextPage = function () {
        if ($scope.contactcurrentPage < $scope.contactpageCount()) {
            $scope.contactcurrentPage++;
        }

        if ($scope.contactcurrentPage >= $scope.contactLimitPage && $scope.contactcurrentPage <= $scope.contactpageCount()) {
            $scope.contactLimitFirst = $scope.contactLimitFirst + 5;
            $scope.contactLimitPage = $scope.contactLimitPage + 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }

    };

    $scope.contactnextPageDisabled = function () {
        return $scope.contactcurrentPage === $scope.contactpageCount() ? "disabled" : "";
    };

    $scope.contactsetPage = function (n) {
        $scope.contactcurrentPage = n;
    };

    /**************Select Revenue **************/

    $scope.OnClickRevenuePopup = function () {
        $scope.SearchRevenue = [];
        $scope.Parameter.Param_Revenue = [];
        $scope.revenue = [];
        $scope.revenue.binding = 0;
        $scope.revitemsCount = 0;
        $('#modalrevenute-list').modal('show');
    };

    $scope.OnClickFilterRevenue = function () {
        if ($scope.SearchRevenue.InputText != undefined && $scope.SearchRevenue.InputText != "") {
            $scope.revenue.binding = 1;
            var qq = $q.all([contactService.filterRevenue($scope.SearchRevenue.InputText)]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == 200) {
                            if (data[0].data.responsedata == undefined)
                                $scope.Parameter.Param_Revenue = [];
                            else {
                                $scope.Parameter.Param_Revenue = data[0].data.responsedata;
                                $scope.revretpage = [];
                                $scope.revrange();
                                $scope.revenue.binding = 0;
                            }
                        }
                        else {
                            $scope.revenue.binding = 0;
                            showErrorToast(data[0].data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(response.data.errormessage);
                }
            });
        }
    };

    $scope.OnClickRevenueSelection = function (index) {
        try {
            var revenue = angular.copy($scope.Parameter.Param_Revenue[index]);
            $scope.OnClearContact();
            $scope.ReceiptAdd.CustomerName = revenue.CompanyName;
            $scope.ReceiptAdd.CustomerTaxID = revenue.NID;
            $scope.ReceiptAdd.CustomerAddress = revenue.Address;
            $scope.ReceiptAdd.CustomerBranch = revenue.BranchNoName;
            $scope.ReceiptAdd.CreditDay = 0;
            $('#modalrevenute-list').modal('hide');
        }
        catch (err) {
            showErrorToast(response.data.errormessage);
        }
    };

    $scope.revcurrentPage = 0;

    $scope.revLimitFirst = 0;
    $scope.revLimitPage = 5;
    $scope.revitemsPerPage = 10;

    $scope.revpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Revenue.length / $scope.revitemsPerPage) - 1;
    };

    $scope.revrange = function () {
        $scope.revitemsCount = $scope.Parameter.Param_Revenue.length;
        $scope.revpageshow = $scope.revpageCount() > $scope.revLimitPage && $scope.revpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.revpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.revpageshowdata = 1;
        $scope.revretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.revprevPage = function () {
        if ($scope.revcurrentPage > 0) {
            $scope.revcurrentPage--;
        }

        if ($scope.revcurrentPage < $scope.revLimitFirst && $scope.revcurrentPage >= 1) {
            $scope.revLimitFirst = $scope.revLimitFirst - 5;
            $scope.revLimitPage = $scope.revLimitPage - 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }
    };

    $scope.revprevPageDisabled = function () {
        return $scope.revcurrentPage === 0 ? "disabled" : "";
    };

    $scope.revnextPage = function () {
        if ($scope.revcurrentPage < $scope.revpageCount()) {
            $scope.revcurrentPage++;
        }

        if ($scope.revcurrentPage >= $scope.revLimitPage && $scope.revcurrentPage <= $scope.revpageCount()) {
            $scope.revLimitFirst = $scope.revLimitFirst + 5;
            $scope.revLimitPage = $scope.revLimitPage + 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }

    };

    $scope.revnextPageDisabled = function () {
        return $scope.revcurrentPage === $scope.revpageCount() ? "disabled" : "";
    };

    $scope.revsetPage = function (n) {
        $scope.revcurrentPage = n;
    };

    /**************Select Product **************/

    $scope.OnClickProductPopup = function () {
        $scope.SearchProduct.InputFilter = [];
        $scope.productcurrentPage = 0;
        $scope.product = [];
        $scope.product.binding = 0;
        $scope.productretpage = [];
        $scope.productrange();
        $('#modalproduct-list').modal('show');
    };

    $scope.OnClickProductSearch = function () {
        $scope.Parameter.Param_Product = productService.filterProduct($scope.SearchProduct.InputFilter, $scope.Parameter.Param_Product, $scope.Parameter.Param_ProductMain);
        if ($scope.Parameter.Param_Product == undefined)
            $scope.Parameter.Param_Product = [];
    };

    $scope.OnClickProductSelection = function (productkey) {
        var product = _.where($scope.Parameter.Param_Product, { ProductKey: productkey })[0];
        if (product != undefined) {
            $scope.OnClickClearDetail();
            $scope.ProductDetailAdd.ProductKey = product.ProductKey;
            $scope.ProductDetailAdd.Name = product.ProductName;
            $scope.ProductDetailAdd.Description = product.ProductDescription;
            $scope.ProductDetailAdd.Quantity = '1.00';
            $scope.ProductDetailAdd.Unit = product.UnitTypeName;
            $scope.ProductDetailAdd.DiscountType = $scope.ReceiptAdd.IsDiscountHeaderType;
            $scope.ProductDetailAdd.DiscountPercent = '0.00';
            $scope.ProductDetailAdd.Discount = '0.00';
            $scope.ProductDetailAdd.VatType = '2';

            if (product.SellTaxType == '1' || product.SellTaxType == '3' || product.SellTaxType == '4') {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var sell = 0, selltax = 0, selllast = 0;
                sell = ConvertToDecimal(product.SellPrice == undefined ? 0 : product.SellPrice);
                selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                selllast = sell + selltax;

                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(selllast, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(selltax, 2);

                if (product.SellTaxType == '3') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '1';
                }
                else if (product.SellTaxType == '4') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '3';
                }
            }
            else if (product.SellTaxType == '2') {

                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(product.SellAfterTaxPrice, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(product.SellPrice, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(product.SellTaxPrice, 2);
            }

            if ($scope.ReceiptAdd.TaxType == 'N')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
            else if ($scope.ReceiptAdd.TaxType == 'V')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

            $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $('#modalproduct-list').modal('hide');
        }
    };

    $scope.OnClickProductAdd = function () {
        $scope.BusinessProductAdd = [];
        $scope.BusinessProductAdd.ProductType = 'S';
        $scope.BusinessProductAdd.SellTaxType = '1';
        $scope.BusinessProductAdd.BuyTaxType = '1';
        initdropify('');

        $('#modalproduct-add').modal('show');
    };

    function initdropify(path) {
        $("#product_thumnail").addClass('dropify');
        var publicpath_identity_picture = path;

        var drEvent = $('.dropify').dropify();
        drEvent.on('dropify.afterClear', function (event, element) {
            $scope.DeleteImages = 1;
        });
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = publicpath_identity_picture;
        drEvent.destroy();

        drEvent.init();

        $('.dropify#identity_picture').dropify({
            defaultFile: publicpath_identity_picture
        });

        $('.dropify').dropify();
    }

    $scope.OnClickProductSave = function () {
        try {
            if ($scope.BusinessProductAdd.ProductName == undefined || $scope.BusinessProductAdd.ProductName == "")
                throw "กรุณาระบุชื่อสินค้า/บริการ";
            else {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var unit = document.getElementById('input_unittype').value;
                var category = document.getElementById('input_categorytype').value;


                var qq = $q.all([productService.postNewProduct($scope.BusinessProductAdd, vat, unit, category)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var product = data[0].data.responsedata;

                                if (product != undefined) {
                                    //$scope.ProductDetailAdd.ProductKey = product.ProductKey;
                                    //$scope.ProductDetailAdd.Name = product.ProductName;
                                    //$scope.ProductDetailAdd.Description = product.ProductDescription;
                                    //$scope.ProductDetailAdd.Quantity = '1.00';
                                    //$scope.ProductDetailAdd.Unit = product.UnitTypeName;
                                    //$scope.ProductDetailAdd.Price = AFormatNumber($scope.ReceiptAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);
                                    //$scope.ProductDetailAdd.Total = AFormatNumber($scope.ReceiptAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);

                                    if ($scope.SelectedFiles != undefined && $scope.Upload == 1) {
                                        try {

                                            var input = document.getElementById("product_thumnail");
                                            var files = input.files;
                                            var formData = new FormData();

                                            for (var i = 0; i != files.length; i++) {
                                                formData.append("files", files[i]);
                                            }

                                            $.ajax(
                                                {
                                                    url: baseURL + "Product/UploadPictureProducts?productkey=" + product.ProductKey,
                                                    data: formData,
                                                    processData: false,
                                                    contentType: false,
                                                    type: "POST",
                                                    success: function (data) {
                                                        $('#modalproduct-add').modal('hide');
                                                        showSuccessToast();
                                                    },
                                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                        showErrorToast(textStatus);
                                                    }
                                                }
                                            );
                                        }
                                        catch (ex) {
                                            showErrorToast(ex);
                                        }
                                    }
                                    else {
                                        $('#modalproduct-add').modal('hide');
                                        showSuccessToast();
                                    }


                                    var q1 = $q.all([paramService.getProduct()]).then(function (data) {
                                        if (data[0] != undefined && data[0] != "") {
                                            if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                                $scope.SearchProduct = [];
                                                $scope.Parameter.Param_Product = [];
                                                $scope.Parameter.Param_ProductMain = [];

                                                $scope.Parameter.Param_ProductMain = data[0].data.responsedata;

                                                _.each($scope.Parameter.Param_ProductMain, function (item) {
                                                    item.SellPrice = AFormatNumber(item.SellPrice, 2);
                                                    item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                                                    item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                                                    item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                                                    item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                                                    item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                                                });

                                                $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;

                                                $scope.OnClickProductSelection(product.ProductKey);
                                            }
                                            else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                        }
                                    });
                                }

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(data[0].data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.productcurrentPage = 0;

    $scope.productLimitFirst = 0;
    $scope.productLimitPage = 5;
    $scope.productitemsPerPage = 10;

    $scope.productpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Product.length / $scope.productitemsPerPage) - 1;
    };

    $scope.productrange = function () {
        $scope.productitemsCount = $scope.Parameter.Param_Product.length;
        $scope.productpageshow = $scope.productpageCount() > $scope.productLimitPage && $scope.productpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.productpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.productpageshowdata = 1;
        $scope.productretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.productpproductPage = function () {
        if ($scope.productcurrentPage > 0) {
            $scope.productcurrentPage--;
        }

        if ($scope.productcurrentPage < $scope.productLimitFirst && $scope.productcurrentPage >= 1) {
            $scope.productLimitFirst = $scope.productLimitFirst - 5;
            $scope.productLimitPage = $scope.productLimitPage - 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }
    };

    $scope.productpproductPageDisabled = function () {
        return $scope.productcurrentPage === 0 ? "disabled" : "";
    };

    $scope.productnextPage = function () {
        if ($scope.productcurrentPage < $scope.productpageCount()) {
            $scope.productcurrentPage++;
        }

        if ($scope.productcurrentPage >= $scope.productLimitPage && $scope.productcurrentPage <= $scope.productpageCount()) {
            $scope.productLimitFirst = $scope.productLimitFirst + 5;
            $scope.productLimitPage = $scope.productLimitPage + 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }

    };

    $scope.productnextPageDisabled = function () {
        return $scope.productcurrentPage === $scope.productpageCount() ? "disabled" : "";
    };

    $scope.productsetPage = function (n) {
        $scope.productcurrentPage = n;
    };

    $scope.UploadFiles = function (files) {
        $scope.Upload = 1;
        $scope.SelectedFiles = files;
    };

    /****************** Document Attach ************************/
    $scope.OnLoadAttach = function () {
        try {
            $http.get(baseURL + "DocumentSell/GetDocumentAttach?documentkey=" + $scope.ReceiptAdd.ReceiptKey + "&documenttype=" + headerdoc + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.DocumentAttach = [];
                            $scope.DocumentAttach = response.data.responsedata;
                        }
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnUploadAttach = function (files) {
        try {
            if ($scope.ReceiptAdd.ReceiptKey == undefined || $scope.ReceiptAdd.ReceiptKey == "")
                throw "กรุณากดปุ่ม บันทึก ก่อนแนบไฟล์เอกสาร";
            else if ($scope.DocumentAttach.length > 3)
                throw "สามารถแนบไฟล์สูงสุดเพียง 3 ไฟล์ เท่านั้น";
            else if (files.length > 0) {
                var data = {
                    DocumentKey: $scope.ReceiptAdd.ReceiptKey,
                    DocumentNo: $scope.ReceiptAdd.ReceiptNo,
                    DocumentType: headerdoc,
                    AttachName: files[0].name,
                    AttachExtension: getFileExtension(files[0].name)
                };
                $http.post(baseURL + "DocumentSell/PostDocumentAttach", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    try {
                                        var attachfile = response.data.responsedata;
                                        var input = document.getElementById("document_attach");
                                        var files = input.files;
                                        var formData = new FormData();

                                        for (var i = 0; i != files.length; i++) {
                                            formData.append("files", files[i]);
                                        }

                                        $.ajax(
                                            {
                                                url: baseURL + "DocumentSell/UploadDocumentAttach?attachkey=" + attachfile.AttachKey,
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                type: "POST",
                                                success: function (data) {

                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();

                                                    $scope.OnLoadAttach();
                                                    showSuccessText('แนบไฟล์สำเร็จ');
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();
                                                    showErrorToast(textStatus);
                                                }
                                            }
                                        );
                                    }
                                    catch (ex) {
                                        var drEvent = $('.dropify').dropify();
                                        drEvent = drEvent.data('dropify');
                                        drEvent.resetPreview();
                                        drEvent.clearElement();
                                        drEvent.destroy();

                                        drEvent.init();
                                        showErrorToast(ex);
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            var drEvent = $('.dropify').dropify();
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.destroy();

            drEvent.init();
            showWariningToast(err);
        }
    };

    $scope.OnClickDeleteAttach = function (attachkey) {
        var data = {
            DocumentKey: $scope.ReceiptAdd.ReceiptKey,
            DocumentNo: $scope.ReceiptAdd.ReceiptNo,
            DocumentType: headerdoc,
            AttachKey: attachkey
        };
        $http.post(baseURL + "DocumentSell/DeleteDocumentAttach", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            try {
                                showDeleteSuccessToast();
                                $scope.OnLoadAttach();
                            }
                            catch (err) {
                                showErrorToast(err);
                            }
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickShowAttach = function (attachkey) {
        var attactfile = _.where($scope.DocumentAttach, { AttachKey: attachkey })[0];
        if (attactfile != undefined) {
            $scope.DocumentAttachView = [];
            $scope.DocumentAttachView = attactfile;

            if (attactfile.AttachExtension == 'pdf') {
                $scope.DocumentAttachView.AttachHeight = '850px';
                PDFObject.embed(baseURL + 'uploads/attach/' + attactfile.AttachPath, "#exampleattach");
            }
            else {
                $scope.DocumentAttachView.AttachHeight = 'auto';
                $scope.DocumentAttachView.AttachShow = baseURL + 'uploads/attach/' + attactfile.AttachPath;
            }
            $('#modal-attach').modal('show');
        }
    };
});


