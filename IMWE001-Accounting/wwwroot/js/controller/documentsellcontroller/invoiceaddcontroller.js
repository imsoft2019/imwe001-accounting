﻿WEACCTAPP.controller('invoiceaddcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, contactService, productService, $q, Upload) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'INV';

    $scope.DocumentAttach = [];
    $scope.InvoiceAdd = [];
    $scope.Parameter = [];
    $scope.UserProfile = [];
    $scope.Parameter.Param_Contact = [];
    $scope.Parameter.Param_Product = [];
    $scope.Parameter.Param_Employee = [];
    $scope.Parameter.Param_Revenue = [];
    $scope.table = [];

    var gCustomer = paramService.getCustomer();
    var gProduct = paramService.getProduct();
    var gEmployee = paramService.getEmployee();
    var gParamQuotation = paramService.getParameterQuotation();
    var gProfile = paramService.getProfile();
    var gRemark = paramService.getDocumentRemark(headerdoc);

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        $scope.table.binding = 1;

        var prefix = { asofdate: GetDatetimeNow(), documenttype: headerdoc };

        var qq = $q.all([paramService.getDocumentNo(prefix), gCustomer, gProduct, gEmployee, gParamQuotation, gProfile, gRemark]).then(function (data) {
            try {
                /****** GetDocument NO ******/
                if (data[0] != undefined && data[0] != "") {
                    if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.InvoiceAdd.InvoiceNo = data[0].data.responsedata; }
                    else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                }

                /****** Get Customer ******/
                if (data[1] != undefined && data[1] != "") {
                    if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                        $scope.SearchContact = [];
                        $scope.Parameter.Param_Contact = [];
                        $scope.Parameter.Param_ContactMain = [];
                        $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                    }
                    else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                }

                /****** Get Product ******/
                if (data[2] != undefined && data[2] != "") {
                    if (data[2].data.responsecode == '200' && data[2].data.responsedata != undefined && data[2].data.responsedata != "") {
                        $scope.SearchProduct = [];
                        $scope.Parameter.Param_Product = [];
                        $scope.Parameter.Param_ProductMain = [];

                        $scope.Parameter.Param_ProductMain = data[2].data.responsedata;

                        _.each($scope.Parameter.Param_ProductMain, function (item) {
                            item.SellPrice = AFormatNumber(item.SellPrice, 2);
                            item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                            item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.SellPriceText = item.SellTaxType != '2' ? AFormatNumber(item.SellPrice, 2) : AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                            item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                            item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                        });

                        $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;
                    }
                    else if (data[2].data.responsecode == '400') { showErrorToast(data[2].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[3] != undefined && data[3] != "") {
                    if (data[3].data.responsecode == '200' && data[3].data.responsedata != undefined && data[3].data.responsedata != "") {
                        $scope.Parameter.Param_Employee = [];
                        $scope.Parameter.Param_Employee = data[3].data.responsedata;
                    }
                    else if (data[3].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Parameter Invoice ******/
                if (data[4] != undefined && data[4] != "") {
                    if (data[4].data.responsecode == '200' && data[4].data.responsedata != undefined && data[4].data.responsedata != "") {
                        $scope.Parameter.Param_Quotation = [];
                        $scope.Parameter.Param_Quotation = data[4].data.responsedata;

                        var substringMatcher = function (strs) {
                            return function findMatches(q, cb) {
                                if (q == '') {
                                    cb(strs);
                                }
                                else {
                                    var matches, substringRegex;

                                    // an array that will be populated with substring matches
                                    matches = [];

                                    // regex used to determine if a string contains the substring `q`
                                    var substrRegex = new RegExp(q, 'i');

                                    // iterate through the pool of strings and for any string that
                                    // contains the substring `q`, add it to the `matches` array
                                    for (var i = 0; i < strs.length; i++) {
                                        if (substrRegex.test(strs[i])) {
                                            matches.push(strs[i]);
                                        }
                                    }

                                    cb(matches);
                                }
                            };
                        };

                        var unittype = [];
                        _.each($scope.Parameter.Param_Quotation.Param_UnitType, function (item) { unittype.push(item.Name); });

                        $('#productunit .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0
                        }, {
                                name: 'unittype',
                                limit: 20,
                                source: substringMatcher(unittype)
                            });


                        var category = [];
                        _.each($scope.Parameter.Param_Quotation.Param_Category, function (item) { category.push(item.Name); });
                        $('#productcategory .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0,
                        }, {
                                name: 'category',
                                limit: 20,
                                source: substringMatcher(category)
                            });
                    }
                    else if (data[4].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[5] != undefined && data[5] != "") {
                    if (data[5].data.responsecode == '200' && data[5].data.responsedata != undefined && data[5].data.responsedata != "") {
                        $scope.UserProfile = [];
                        $scope.UserProfile = data[5].data.responsedata;
                    }
                    else if (data[5].data.responsecode == '400') { showErrorToast(data[5].data.errormessage); }
                }

                /****** Get Remark ******/
                if (data[6] != undefined && data[6] != "") {
                    if (data[6].data.responsecode == '200' && data[6].data.responsedata != undefined && data[6].data.responsedata != "") {
                        $scope.InvoiceAdd.Remark = data[6].data.responsedata.DocumentText;
                    }
                    else if (data[6].data.responsecode == '400') { showErrorToast(data[6].data.errormessage); }
                }

                if (QueryString('InvoiceKey') != undefined) {

                    $http.get(baseURL + "DocumentSell/GetBusinessInvoicebykey?InvoiceKey=" + QueryString('InvoiceKey') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.InvoiceAdd = [];
                                    $scope.InvoiceAdd = response.data.responsedata;

                                    $scope.InvoiceAdd.InvoiceDate = formatDate($scope.InvoiceAdd.InvoiceDate);
                                    /*Credit */
                                    $scope.InvoiceAdd.DueDate = $scope.InvoiceAdd.DueDate != undefined && $scope.InvoiceAdd.DueDate != "" ? formatDate($scope.InvoiceAdd.DueDate) : "";

                                    $('#inputquotationdate-popup').datepicker('setDate', $scope.InvoiceAdd.InvoiceDate);
                                    $('#inputdatedue-popup').datepicker('setDate', $scope.InvoiceAdd.DueDate);

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.InvoiceAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.InvoiceAdd.SelectSale = type[0];

                                    $scope.InvoiceAdd.IsTaxHeader = $scope.InvoiceAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                    $scope.InvoiceAdd.IsDiscountHeader = $scope.InvoiceAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                    $scope.InvoiceAdd.IsTaxSummary = $scope.InvoiceAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                    // หักภาษี ณ ที่จ่าย
                                    $scope.InvoiceAdd.IsWithholdingTax = $scope.InvoiceAdd.IsWithholdingTax == '1' ? true : false;
                                    if ($scope.InvoiceAdd.IsWithholdingTax) {

                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.InvoiceAdd.WithholdingKey; });
                                        if (type.length > 0)
                                            $scope.InvoiceAdd.SelectWithholding = type[0];

                                        $scope.InvoiceAdd.WithholdingAmount = AFormatNumber($scope.InvoiceAdd.WithholdingAmount, 2);
                                        $scope.InvoiceAdd.GrandAmountAfterWithholding = AFormatNumber($scope.InvoiceAdd.GrandAmountAfterWithholding, 2);
                                    }
                                    else {
                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                        if (type.length > 0)
                                            $scope.InvoiceAdd.SelectWithholding = type[0];

                                    }

                                    $scope.InvoiceAdd.TotalAmount = AFormatNumber($scope.InvoiceAdd.TotalAmount, 2);
                                    $scope.InvoiceAdd.DiscountPercent = AFormatNumber($scope.InvoiceAdd.DiscountPercent, 2);
                                    $scope.InvoiceAdd.DiscountAmount = AFormatNumber($scope.InvoiceAdd.DiscountAmount, 2);
                                    $scope.InvoiceAdd.TotalAfterDiscountAmount = AFormatNumber($scope.InvoiceAdd.TotalAfterDiscountAmount, 2);
                                    $scope.InvoiceAdd.ExemptAmount = AFormatNumber($scope.InvoiceAdd.ExemptAmount, 2);
                                    $scope.InvoiceAdd.VatableAmount = AFormatNumber($scope.InvoiceAdd.VatableAmount, 2);
                                    $scope.InvoiceAdd.VAT = AFormatNumber($scope.InvoiceAdd.VAT, 2);
                                    $scope.InvoiceAdd.TotalBeforeVatAmount = AFormatNumber($scope.InvoiceAdd.TotalBeforeVatAmount, 2);
                                    $scope.InvoiceAdd.GrandAmount = AFormatNumber($scope.InvoiceAdd.GrandAmount, 2);

                                    $scope.InvoiceAdd.Signature = $scope.InvoiceAdd.Signature == '1' ? true : false;

                                    _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {
                                        item.Quantity = AFormatNumber(item.Quantity, 2);
                                        item.Price = AFormatNumber(item.Price, 2);
                                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.Discount = AFormatNumber(item.Discount, 2);
                                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                        item.VatRate = AFormatNumber(item.VatRate, 2);
                                        item.Vat = AFormatNumber(item.Vat, 2);
                                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                        item.Total = AFormatNumber(item.Total, 2);
                                    });

                                    $scope.OnClickClearDetail();

                                    /**  duppllicate */
                                    if (QueryString('dup_ref') != undefined && QueryString('dup_ref') == '1') {
                                        $scope.InvoiceAdd.InvoiceNo = data[0].data.responsedata;
                                        $scope.InvoiceAdd.InvoiceKey = undefined;
                                        $scope.InvoiceAdd.InvoiceStatus = undefined;
                                        $scope.InvoiceAdd.InvoiceStatusName = undefined;
                                        $scope.InvoiceAdd.DueDate = $scope.InvoiceAdd.InvoiceDate = GetDatetimeNow();

                                        $('#inputquotationdate-popup').datepicker('setDate', $scope.InvoiceAdd.InvoiceDate);
                                        $('#inputdatedue-popup').datepicker('setDate', $scope.InvoiceAdd.DueDate);

                                        $scope.OnChangeCredit();
                                        var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                        if (type.length > 0)
                                            $scope.InvoiceAdd.SelectSale = type[0];

                                        _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {
                                            item.InvoiceKey = undefined;
                                            item.InvoiceDetailKey = undefined;
                                        });
                                    }

                                    $scope.OnLoadAttach();

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else if (QueryString('ref_key') != undefined && QueryString('ref_info') != undefined ) {
                    // New document With Quotation
                    $http.get(baseURL + "DocumentSell/GetBusinessDcoumentReference?DocumentKey=" + QueryString('ref_key') + "&DocumentType=" + QueryString('ref_info') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.InvoiceAdd = [];
                                    $scope.InvoiceAdd = response.data.responsedata;
                                    $scope.InvoiceAdd.InvoiceDetail = [];

                                    /** QuotationValues*/
                                    if (QueryString('ref_info') == 'QT') {
                                        $scope.InvoiceAdd.DocumentKey = $scope.InvoiceAdd.QuotationKey;
                                        $scope.InvoiceAdd.DocumentNo = $scope.InvoiceAdd.QuotationNo;
                                        $scope.InvoiceAdd.ReferenceNo = $scope.InvoiceAdd.QuotationNo;

                                        $scope.InvoiceAdd.InvoiceStatus = "1";
                                        $scope.InvoiceAdd.InvoiceStatusName = "รอดำเนินการ";

                                        _.each($scope.InvoiceAdd.QuotationDetail, function (item) {
                                            item.InvoiceKey = undefined;
                                            item.InvoiceDetailKey = undefined;
                                            $scope.InvoiceAdd.InvoiceDetail.push(item);
                                        });

                                    }
                                    else if (QueryString('ref_info') == 'BL') {
                                        $scope.InvoiceAdd.DocumentKey = $scope.InvoiceAdd.BillingKey;
                                        $scope.InvoiceAdd.DocumentNo = $scope.InvoiceAdd.BillingNo;
                                        $scope.InvoiceAdd.ReferenceNo = $scope.InvoiceAdd.BillingNo;

                                        $scope.InvoiceAdd.InvoiceStatus = "2"; 
                                        $scope.InvoiceAdd.InvoiceStatusName= "รอเก็บเงิน";

                                        _.each($scope.InvoiceAdd.BillingDetail, function (item) {
                                            item.InvoiceKey = undefined;
                                            item.InvoiceDetailKey = undefined;
                                            $scope.InvoiceAdd.InvoiceDetail.push(item);
                                        });

                                    }
                                    $scope.InvoiceAdd.DocumentOwner = QueryString('ref_info');

                                    /**  clear values */
                                    $scope.InvoiceAdd.InvoiceNo = data[0].data.responsedata;
                                    $scope.InvoiceAdd.InvoiceKey = undefined;

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                    if (type.length > 0)
                                        $scope.InvoiceAdd.SelectSale = type[0];

                                    
                                    $scope.InvoiceAdd.InvoiceDate = GetDatetimeNow();
                                    
                                    /*Credit */
                                    /*หาก เครดิตเป็นเงินสด เปลี่ยนเป็น เครดิต (วัน)**/
                                    if ($scope.InvoiceAdd.CreditType == 'CA')
                                        $scope.InvoiceAdd.CreditType = 'CD';
                                    if ($scope.InvoiceAdd.CreditType == 'CD')
                                        $scope.OnChangeCredit();

                                    $('#inputquotationdate-popup').datepicker('setDate', $scope.InvoiceAdd.InvoiceDate);
                                    $('#inputdatedue-popup').datepicker('setDate', $scope.InvoiceAdd.DueDate);


                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.InvoiceAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.InvoiceAdd.SelectSale = type[0];

                                    $scope.InvoiceAdd.IsTaxHeader = $scope.InvoiceAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                    $scope.InvoiceAdd.IsDiscountHeader = $scope.InvoiceAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                    $scope.InvoiceAdd.IsTaxSummary = $scope.InvoiceAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                    // หักภาษี ณ ที่จ่าย
                                    $scope.InvoiceAdd.IsWithholdingTax = $scope.InvoiceAdd.IsWithholdingTax == '1' ? true : false;
                                    if ($scope.InvoiceAdd.IsWithholdingTax) {

                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.InvoiceAdd.WithholdingKey; });
                                        if (type.length > 0)
                                            $scope.InvoiceAdd.SelectWithholding = type[0];

                                        $scope.InvoiceAdd.IsWithholdingTax.WithholdingAmount = AFormatNumber($scope.InvoiceAdd.IsWithholdingTax.WithholdingAmount, 2);
                                        $scope.InvoiceAdd.IsWithholdingTax.GrandAmountAfterWithholding = AFormatNumber($scope.InvoiceAdd.IsWithholdingTax.GrandAmountAfterWithholding, 2);
                                    }
                                    else {
                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                        if (type.length > 0)
                                            $scope.InvoiceAdd.SelectWithholding = type[0];

                                        $scope.InvoiceAdd.WithholdingAmount = AFormatNumber(0, 2);//
                                    }

                                    $scope.InvoiceAdd.TotalAmount = AFormatNumber($scope.InvoiceAdd.TotalAmount, 2);
                                    $scope.InvoiceAdd.DiscountPercent = AFormatNumber($scope.InvoiceAdd.DiscountPercent, 2);
                                    $scope.InvoiceAdd.DiscountAmount = AFormatNumber($scope.InvoiceAdd.DiscountAmount, 2);
                                    $scope.InvoiceAdd.TotalAfterDiscountAmount = AFormatNumber($scope.InvoiceAdd.TotalAfterDiscountAmount, 2);
                                    $scope.InvoiceAdd.ExemptAmount = AFormatNumber($scope.InvoiceAdd.ExemptAmount, 2);
                                    $scope.InvoiceAdd.VatableAmount = AFormatNumber($scope.InvoiceAdd.VatableAmount, 2);
                                    $scope.InvoiceAdd.VAT = AFormatNumber($scope.InvoiceAdd.VAT, 2);
                                    $scope.InvoiceAdd.TotalBeforeVatAmount = AFormatNumber($scope.InvoiceAdd.TotalBeforeVatAmount, 2);
                                    $scope.InvoiceAdd.GrandAmount = AFormatNumber($scope.InvoiceAdd.GrandAmount, 2);

                                    $scope.InvoiceAdd.Signature == $scope.InvoiceAdd.Signature == '1' ? true : false;

                                    _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {
                                        item.Quantity = AFormatNumber(item.Quantity, 2);
                                        item.Price = AFormatNumber(item.Price, 2);
                                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.Discount = AFormatNumber(item.Discount, 2);
                                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                        item.VatRate = AFormatNumber(item.VatRate, 2);
                                        item.Vat = AFormatNumber(item.Vat, 2);
                                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                        item.Total = AFormatNumber(item.Total, 2);
                                    });

                                    $scope.OnClickClearDetail();
                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else {
                    /******* Initail ********/
                    $scope.InvoiceAdd.DueDate = $scope.InvoiceAdd.InvoiceDate = GetDatetimeNow();
                    $scope.InvoiceAdd.CreditType = 'CD';
                    $scope.InvoiceAdd.TaxType = 'N';
                    $scope.OnChangeCreditType();

                    $('#inputquotationdate-popup').datepicker('setDate', $scope.InvoiceAdd.InvoiceDate);
                    $('#inputdatedue-popup').datepicker('setDate', $scope.InvoiceAdd.DueDate);

                    $scope.InvoiceAdd.IsTaxHeader = false; //แยกรายการภาษี
                    $scope.InvoiceAdd.IsDiscountHeader = false; //แยกรายการาส่วนลด
                    $scope.InvoiceAdd.IsWithholdingTax = false; // หักภาษี ณ ที่จ่าย

                    $scope.InvoiceAdd.IsTaxSummary = true; // หักภาษี 7 % รวม
                    $scope.InvoiceAdd.IsDiscountHeaderType = '%'; // ส่วนลดแยกรายการแบบ Percent

                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                    if (type.length > 0)
                        $scope.InvoiceAdd.SelectSale = type[0];

                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                    if (type.length > 0)
                        $scope.InvoiceAdd.SelectWithholding = type[0];

                    $scope.OnClickClearDetail();

                    $scope.InvoiceAdd.InvoiceDetail = [];
                    $scope.OnChangeDetailCalculate();

                    $scope.table.binding = 0;
                }
            }
            catch (err) {
                showErrorToast(err);
            }
        });
    };

    $scope.OnClickCancel = function () {
        if (QueryString('ref_info') == 'QT')
            window.location.href = baseURL + "DocumentSell/Quotation";
        else if (QueryString('ref_info') == 'BL')
            window.location.href = baseURL + "DocumentSell/BillingNote";
        else if (QueryString('ref_report') != undefined)
            window.location.href = baseURL + "Report/" + QueryString('ref_report');
        else
        window.location.href = baseURL + "DocumentSell/Invoice";
    };

    $scope.OnDisabled = function () {
        if ($scope.InvoiceAdd.InvoiceStatus == undefined || $scope.InvoiceAdd.InvoiceStatus == '1' || QueryString('ref_info') != undefined)
            return false;
        else
            return true;
    };

    $scope.OnClickReportContent = function (type) {
        $scope.Print = "1";
        $scope.document = [];
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $scope.OnClickSave();
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'INV',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {

        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'INV',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };
    /******************Due Date *********** */
    $scope.OnChangeCreditType = function () {
        if ($scope.InvoiceAdd.CreditType == 'CD') {
            $scope.OnChangeCredit();
        }
        if ($scope.InvoiceAdd.CreditType == 'CA') { $scope.InvoiceAdd.CreditDay = 0; }
    };
    $scope.OnChangeCredit = function () {

        if ($scope.InvoiceAdd.CreditType == 'CD') {
            if ($scope.InvoiceAdd.CreditDay == undefined || $scope.InvoiceAdd.CreditDay == '') {
                $scope.InvoiceAdd.InvoiceDate = $scope.InvoiceAdd.DueDate = GetDatetimeNow();
                $scope.InvoiceAdd.CreditDay = 0;
            }
            else {
                $scope.InvoiceAdd.DueDate = SetDatetimeDay($scope.InvoiceAdd.InvoiceDate, $scope.InvoiceAdd.CreditDay);
            }
        }
    };
    $scope.OnChangeDateDue = function () {
        if ($scope.InvoiceAdd.CreditType == 'CD') {
            if ($scope.InvoiceAdd.DueDate == undefined || $scope.InvoiceAdd.DueDate == '') {
                $scope.InvoiceAdd.InvoiceDate = GetDatetimeNow();
                $scope.InvoiceAdd.CreditDay = 0;
            }
            else {
                var datedue = $scope.InvoiceAdd.DueDate;
                var days = DatetimeLenof($scope.InvoiceAdd.InvoiceDate, $scope.InvoiceAdd.DueDate);
                if (days < 0) {
                    $scope.InvoiceAdd.InvoiceDate = SetDatetimeDay($scope.InvoiceAdd.DueDate, days);
                    $scope.InvoiceAdd.DueDate = datedue;
                    $scope.InvoiceAdd.CreditDay = (days * (-1));
                }
                else
                    $scope.InvoiceAdd.CreditDay = days;
            }
        }
    };
    $scope.OnChangeInvoiceDate = function () {
        if ($scope.InvoiceAdd.CreditType == 'CD') {
            if ($scope.InvoiceAdd.InvoiceDate == undefined || $scope.InvoiceAdd.InvoiceDate == '') {
                $scope.InvoiceAdd.DueDate = $scope.InvoiceAdd.InvoiceDate = GetDatetimeNow();
                $scope.InvoiceAdd.CreditDay = 0;
            }
            else {
                var days = $scope.InvoiceAdd.CreditDay == undefined || $scope.InvoiceAdd.CreditDay == "" ? 0 : $scope.InvoiceAdd.CreditDay;
                $scope.InvoiceAdd.DueDate = SetDatetimeDay($scope.InvoiceAdd.InvoiceDate, $scope.InvoiceAdd.CreditDay);
            }
        }
        var prefix = { asofdate: $scope.InvoiceAdd.InvoiceDate, documenttype: headerdoc };
        var qq = $q.all([paramService.getDocumentNo(prefix)]).then(function (data) {
            /****** GetDocument NO ******/
            if (data[0] != undefined && data[0] != "") {
                if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.InvoiceAdd.InvoiceNo = data[0].data.responsedata; }
                else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
            }
        });
    };

    /*********** Header Setting Discount , Tax**************/
    $scope.OnChangeIsTaxHeader = function () {
        if ($scope.InvoiceAdd.IsTaxHeader) {
            $scope.InvoiceAdd.IsDiscountHeader = true;
        }
        $scope.OnChangeDetailCalculate();
    };
    $scope.OnChangeIsDiscountHeader = function () {
        $scope.InvoiceAdd.DiscountPercent = '0';
        $scope.InvoiceAdd.DiscountAmount = '0';
        $scope.OnClickDiscountType($scope.InvoiceAdd.IsDiscountHeaderType);
    };
    /******************* Detail And Calculate *************************/
    $scope.OnClickProductDetailAdd = function () {
        try {
            if ($scope.ProductDetailAdd.Name == undefined || $scope.ProductDetailAdd.Name == "")
                throw "กรุณากรอกชื่อสินค้า";
            else if ($scope.ProductDetailAdd.Price == undefined || $scope.ProductDetailAdd.Price == "")
                throw "กรุณากรอกราคาต่อหน่วย";
            else if ($scope.ProductDetailAdd.Quantity == undefined || $scope.ProductDetailAdd.Quantity == "")
                throw "กรุณากรอกจำนวน";
            else {
                $scope.ProductDetailAdd.Sequence = $scope.InvoiceAdd.InvoiceDetail.length + 1;
                $scope.ProductDetailAdd.DiscountType = $scope.InvoiceAdd.IsDiscountHeaderType;

                if ($scope.ProductDetailAdd.ProductKey == undefined) {
                    $scope.ProductDetailAdd.DiscountType = $scope.InvoiceAdd.IsDiscountHeaderType;
                    $scope.ProductDetailAdd.DiscountPercent = '0.00';
                    $scope.ProductDetailAdd.Discount = '0.00';
                    $scope.ProductDetailAdd.VatType = '2';

                    //var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                    var sell = 0, selltax = 0, selllast = 0;
                    sell = ConvertToDecimal($scope.ProductDetailAdd.Price == undefined ? 0 : $scope.ProductDetailAdd.Price);
                    //selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                    //selllast = sell + selltax;

                    $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.PriceTax = AFormatNumber(0, 2);
                }

                if ($scope.InvoiceAdd.TaxType == 'N')
                    $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
                else if ($scope.InvoiceAdd.TaxType == 'V')
                    $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
                $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            }

            $scope.InvoiceAdd.InvoiceDetail.push($scope.ProductDetailAdd);
            $scope.OnClickClearDetail();
            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickProductDetailDelete = function (index) {
        try {
            $scope.InvoiceAdd.InvoiceDetail.splice(index, 1);
            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickClearDetail = function () {
        $scope.ProductDetailAdd = [];
        $scope.ProductDetailAdd.DiscountType = $scope.InvoiceAdd.IsDiscountHeaderType;
        $scope.ProductDetailAdd.VatType = '2';
    };

    $scope.OnClickDiscountType = function (val) {
        $scope.InvoiceAdd.IsDiscountHeaderType = val;
        _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {
            item.DiscountType = val;
            item.DiscountPercent = item.Discount = AFormatNumber(0, 2);
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountAmount = function () {
        $scope.InvoiceAdd.DiscountPercent = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickNoTaxSummary = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickWithholdingTax = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeProductTaxType = function () {
        _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {
            if ($scope.InvoiceAdd.TaxType == 'N')
                item.Price = item.PriceBeforeTax;
            else if ($scope.InvoiceAdd.TaxType == 'V')
                item.Price = item.PriceAfterTax;
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountPercent = function () {
        if ($scope.InvoiceAdd.DiscountPercent == undefined || $scope.InvoiceAdd.DiscountPercent === '' || $scope.InvoiceAdd.DiscountPercent == 0)
            $scope.InvoiceAdd.DiscountAmount = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDetailCalculate = function () {
        try {
            _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {
                var amt, qty, total, vatrate, vatamt, discountper, discountminus, discount, discountafter, vatafter = 0, totalamount, vatcal = 0;
                // Get Values
                amt = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                vat = item.VatType == '1' ? 0 : item.VatType == '2' ? 7 : 0; // dropdown vat หน้าจอ
                discountper = item.DiscountPercent == undefined || item.DiscountPercent === '' ? '0' : detectFloat(item.DiscountPercent); //ส่วนลด % จากหน้าจอ
                discount = item.Discount == undefined || item.Discount === '' ? '0' : detectFloat(item.Discount); //ส่วนลดจากหน้าจอ
                discountminus = vatafter = 0; // ราคาส่วนลด
                discountafter = 0; // หลังหักส่วนลด
                vatafter = 0;

                total = amt * qty;
                // Calculate
                if ($scope.InvoiceAdd.IsDiscountHeader == true) { //เงื่อนไขหักส่วนลด
                    if (item.DiscountType == '%') {
                        discountminus = total * discountper / 100;
                        discountafter = detectFloat(AFormatNumber(total - discountminus, 2));
                    }
                    else if (item.DiscountType == '฿') {
                        discountminus = discount;
                        discountafter = total - discount;
                    }
                }
                else {
                    discountafter = total;
                    discountper = discount = 0;
                }

                if ($scope.InvoiceAdd.IsTaxHeader == true) { //คำนวณหาภาษี ของแต่ละรายการ
                    if (item.VatType == '2') {
                        if ($scope.InvoiceAdd.TaxType == 'N') {

                            vatcal = vat > 0 ? ((discountafter * (100 + vat) / 100)) : 0;
                            vatamt = detectFloat(AFormatNumber(vatcal, 2)) - discountafter;
                            vatafter = discountafter + vatamt;
                        }
                        else if ($scope.InvoiceAdd.TaxType == 'V') {
                            vatcal = vat > 0 ? ((discountafter / (100 + vat) * 100)) : 0;
                            vatamt = discountafter - detectFloat(AFormatNumber(vatcal, 2));
                            vatafter = discountafter - vatamt;
                        }
                    }
                    else
                        vatamt = 0;
                }
                else {
                    item.VatType = '2';
                    vatamt = 0;
                }

                totalamount = discountafter;

                // Set Value
                item.Discount = AFormatNumber(discountminus, 2);
                item.DiscountAfter = AFormatNumber(discountafter, 2);
                item.VatRate = AFormatNumber(vat, 2);
                item.Vat = AFormatNumber(vatamt, 2);
                item.VatAfter = AFormatNumber(vatafter, 2);
                item.Total = AFormatNumber(totalamount, 2);
            });

            /******* หาค่ารวม Summary ***********/
            var price = 0, priceedis = 0, pricevat = 0, qty = 0, totalprice = 0, total = 0, discounthper = 0, discounthamt = 0, priceafterdiscount = 0, priceqty = 0,
                discounth = 0, grandtotal = 0, exemptamt = 0, vatableamt = 0, vat = 0, withholdingrate = 0, withholdingprice = 0, withholdingafter = 0, vatafter = 0;


            discounthper = $scope.InvoiceAdd.DiscountPercent == undefined || $scope.InvoiceAdd.DiscountPercent === '' ? 0 : detectFloat($scope.InvoiceAdd.DiscountPercent); // Discount Percenter
            discounthamt = $scope.InvoiceAdd.DiscountAmount == undefined || $scope.InvoiceAdd.DiscountAmount === '' ? 0 : $scope.InvoiceAdd.IsTaxHeader == false ? detectFloat($scope.InvoiceAdd.DiscountAmount) : 0; // Discount Amount

            if ($scope.InvoiceAdd.InvoiceDetail != undefined && $scope.InvoiceAdd.InvoiceDetail.length > 0) {
                _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {

                    price = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                    priceedis = item.Discount == undefined || item.Discount === '' ? 0 : detectFloat(item.Discount); // ส่วนลดคำนวณ
                    qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                    pricevat = item.Vat == undefined || item.Vat === '' ? 0 : detectFloat(item.Vat); // จำนวน จากหน้าจอ

                    totalprice = totalprice + price * qty;//รวมเป็นเงิน
                    discounth = discounth + priceedis; //ส่วนลดรวม
                    priceqty = price * qty;

                    if ($scope.InvoiceAdd.IsTaxHeader) { // หักภาษี แบบทีละรายการ
                        if (item.VatType == '1' || item.VatType == '3') //0% ยกเว้น
                            exemptamt = exemptamt + priceqty - priceedis;
                        else if (item.VatType == '2') // 7%
                            vatableamt = (vatableamt + priceqty - priceedis) - ($scope.InvoiceAdd.TaxType == 'V' ? pricevat : 0); //ถ้าเลือกรวมภาษีแล้ว ให้หัก ภาษีออก

                        vat = vat + pricevat;
                    }
                });

                /**** หักแบบ Summary ***/
                if ($scope.InvoiceAdd.IsDiscountHeader == false) {
                    if (discounthper > 0)
                        discounthamt = totalprice * discounthper / 100;
                    discounth = discounthamt;
                }

                priceafterdiscount = totalprice - discounth; //รวมเป็นเงิน - หักส่วนลด = ราคาหลังหักส่วนลด

                /*** หักภาษีแบบ Summary **/
                if (!$scope.InvoiceAdd.IsTaxHeader && $scope.InvoiceAdd.IsTaxSummary) {
                    if ($scope.InvoiceAdd.TaxType == 'N') {
                        vatcal = (priceafterdiscount * (100 + 7) / 100);
                        vatamt = vatcal - priceafterdiscount;
                        vatafter = priceafterdiscount + vatamt;
                    }
                    else if ($scope.InvoiceAdd.TaxType == 'V') {
                        vatcal = (priceafterdiscount / (100 + 7) * 100);
                        vatamt = priceafterdiscount - vatcal;
                        vatafter = priceafterdiscount - vatamt;
                    }
                    vat = vatamt;
                }

                grandtotal = totalprice - discounth + ($scope.InvoiceAdd.TaxType == 'N' ? vat : 0); //จำนวนเงินรวมทั้งสิ้น

                if ($scope.InvoiceAdd.IsWithholdingTax && !$scope.InvoiceAdd.IsTaxHeader) {
                    
                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.InvoiceAdd.SelectWithholding.ID; });
                    if (type.length > 0)
                        withholdingrate = type[0].Rate;

                    if ($scope.InvoiceAdd.TaxType == 'N')
                        withholdingprice = priceafterdiscount * withholdingrate / 100;
                    else if ($scope.InvoiceAdd.TaxType == 'V')
                        withholdingprice = vatafter * withholdingrate / 100;

                    withholdingafter = $scope.InvoiceAdd.IsTaxSummary ? (grandtotal - withholdingprice) : (priceafterdiscount - withholdingprice);
                }
                
                $scope.InvoiceAdd.TotalAmount = AFormatNumber(totalprice, 2);
                $scope.InvoiceAdd.DiscountAmount = AFormatNumber(discounth, 2);
                $scope.InvoiceAdd.TotalAfterDiscountAmount = AFormatNumber(priceafterdiscount, 2);
                $scope.InvoiceAdd.ExemptAmount = AFormatNumber(exemptamt, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.InvoiceAdd.VatableAmount = AFormatNumber(vatableamt, 2);//มูลค่าที่คำนวณภาษี
                $scope.InvoiceAdd.VAT = AFormatNumber(vat, 2);//ภาษีมูลค่าเพิ่ม
                $scope.InvoiceAdd.TotalBeforeVatAmount = AFormatNumber(vatafter, 2);//จำนวนเงินรวมภาษี
                $scope.InvoiceAdd.GrandAmount = AFormatNumber(grandtotal, 2);//
                $scope.InvoiceAdd.WithholdingAmount = AFormatNumber(withholdingprice, 2);//
                $scope.InvoiceAdd.GrandAmountAfterWithholding = AFormatNumber(withholdingafter, 2);//
            }
            else {
                $scope.InvoiceAdd.TotalAmount = AFormatNumber(0, 2);
                $scope.InvoiceAdd.DiscountPercent = AFormatNumber(0, 2);
                $scope.InvoiceAdd.DiscountAmount = AFormatNumber(0, 2);
                $scope.InvoiceAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                $scope.InvoiceAdd.ExemptAmount = AFormatNumber(0, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.InvoiceAdd.VatableAmount = AFormatNumber(0, 2);//มูลค่าที่คำนวณภาษี
                $scope.InvoiceAdd.VAT = AFormatNumber(0, 2);//ภาษีมูลค่าเพิ่ม
                $scope.InvoiceAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);//จำนวนเงินรวมภาษี
                $scope.InvoiceAdd.GrandAmount = AFormatNumber(0, 2);//
                $scope.InvoiceAdd.WithholdingAmount = AFormatNumber(0, 2);//
                $scope.InvoiceAdd.GrandAmountAfterWithholding = AFormatNumber(0, 2);//
            }


        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickSave = function (exit) {
        try {
            if ($scope.InvoiceAdd.CustomerName == undefined || $scope.InvoiceAdd.CustomerName == "")
                throw "กรุณาเลือกผู้ติดต่อ";
            else if (detectFloat($scope.InvoiceAdd.GrandAmount) < 0)
                throw "ยอดรวมเอกสารติดลบ กรุณาตรวจสอบยอดรวมเอกสารอีกครั้ง";
            else {

                var credittype;
                if ($scope.InvoiceAdd.CreditType == 'CD')
                    credittype = 'เครดิต(วัน)';
                else if ($scope.InvoiceAdd.CreditType == 'CA')
                    credittype = 'เงินสด';
                else if ($scope.InvoiceAdd.CreditType == 'CN')
                    credittype = 'เครดิต (ไม่แสดงวันที่)';

                var data = {
                    InvoiceKey: $scope.InvoiceAdd.InvoiceKey,
                    InvoiceNo: $scope.InvoiceAdd.InvoiceNo,
                    InvoiceStatus: $scope.InvoiceAdd.InvoiceStatus,
                    InvoiceStatusName: $scope.InvoiceAdd.InvoiceStatusName,
                    CustomerKey: $scope.InvoiceAdd.CustomerKey,
                    CustomerName: $scope.InvoiceAdd.CustomerName,
                    CustomerTaxID: $scope.InvoiceAdd.CustomerTaxID,
                    CustomerContactName: $scope.InvoiceAdd.CustomerContactName,
                    CustomerContactPhone: $scope.InvoiceAdd.CustomerContactPhone,
                    CustomerContactEmail: $scope.InvoiceAdd.CustomerContactEmail,
                    CustomerAddress: $scope.InvoiceAdd.CustomerAddress,
                    CustomerBranch: $scope.InvoiceAdd.CustomerBranch,
                    InvoiceDate: ToJsonDate2($scope.InvoiceAdd.InvoiceDate),
                    CreditType: $scope.InvoiceAdd.CreditType,
                    CreditTypeName: credittype,
                    CreditDay: $scope.InvoiceAdd.CreditDay,
                    DueDate: $scope.InvoiceAdd.CreditType == 'CD' ? ToJsonDate2($scope.InvoiceAdd.DueDate) : null,
                    SaleID: $scope.InvoiceAdd.SelectSale.UID,
                    SaleName: $scope.InvoiceAdd.SelectSale.EmployeeName,
                    ProjectName: $scope.InvoiceAdd.ProjectName,
                    ReferenceNo: $scope.InvoiceAdd.ReferenceNo,
                    TaxType: $scope.InvoiceAdd.TaxType,
                    TaxTypeName: $scope.InvoiceAdd.TaxType == 'N' ? 'ราคาไม่รวมภาษี' : $scope.InvoiceAdd.TaxType == 'V' ? 'ราคารวมภาษี' : '',
                    TotalAmount: ConvertToDecimal($scope.InvoiceAdd.TotalAmount),
                    IsDiscountHeader: $scope.InvoiceAdd.IsDiscountHeader ? '1' : '0',
                    IsDiscountHeaderType: $scope.InvoiceAdd.IsDiscountHeaderType,
                    DiscountPercent: $scope.InvoiceAdd.DiscountPercent,
                    DiscountAmount: $scope.InvoiceAdd.DiscountAmount,
                    TotalAfterDiscountAmount: $scope.InvoiceAdd.TotalAfterDiscountAmount,
                    IsTaxHeader: $scope.InvoiceAdd.IsTaxHeader ? '1' : '0',
                    IsTaxSummary: $scope.InvoiceAdd.IsTaxSummary ? '1' : '0',
                    ExemptAmount: $scope.InvoiceAdd.ExemptAmount,
                    VatableAmount: $scope.InvoiceAdd.VatableAmount,
                    VAT: $scope.InvoiceAdd.VAT,
                    TotalBeforeVatAmount: $scope.InvoiceAdd.TotalBeforeVatAmount,
                    IsWithholdingTax: $scope.InvoiceAdd.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.InvoiceAdd.IsWithholdingTax ? $scope.InvoiceAdd.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.InvoiceAdd.IsWithholdingTax ? $scope.InvoiceAdd.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.InvoiceAdd.IsWithholdingTax ? $scope.InvoiceAdd.WithholdingAmount : undefined,
                    GrandAmountAfterWithholding: $scope.InvoiceAdd.IsWithholdingTax ? $scope.InvoiceAdd.GrandAmountAfterWithholding : undefined,
                    GrandAmount: $scope.InvoiceAdd.GrandAmount,
                    Remark: $scope.InvoiceAdd.Remark,
                    Signature: $scope.InvoiceAdd.Signature ? '1' : '0',
                    Noted: $scope.InvoiceAdd.Noted,
                    DocumentKey: $scope.InvoiceAdd.DocumentKey,
                    DocumentNo: $scope.InvoiceAdd.DocumentNo,
                    DocumentOwner: $scope.InvoiceAdd.DocumentOwner
                };

                data.InvoiceDetail = [];
                _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {

                    var tmpQT = {
                        Discount: ConvertToDecimal(item.Discount),
                        DiscountAfter: ConvertToDecimal(item.DiscountAfter),
                        DiscountPercent: ConvertToDecimal(item.DiscountPercent),
                        DiscountType: item.DiscountType,
                        Name: item.Name,
                        Unit: item.Unit,
                        Description: item.Description,
                        Price: ConvertToDecimal(item.Price),
                        PriceAfterTax: ConvertToDecimal(item.PriceAfterTax),
                        PriceBeforeTax: ConvertToDecimal(item.PriceBeforeTax),
                        PriceTax: ConvertToDecimal(item.PriceTax),
                        ProductKey: parseInt(item.ProductKey),
                        Quantity: ConvertToDecimal(item.Quantity),
                        Sequence: parseInt(item.Sequence),
                        Total: ConvertToDecimal(item.Total),
                        Vat: ConvertToDecimal(item.Vat),
                        VatAfter: ConvertToDecimal(item.VatAfter),
                        VatRate: ConvertToDecimal(item.VatRate),
                        VatType: item.VatType
                    };


                    data.InvoiceDetail.push(tmpQT);
                });

                $scope.table.binding = 1;
                $http.post(baseURL + "DocumentSell/PostBusinessInvoice", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseInvoice = response.data.responsedata;
                                    if (responseInvoice != undefined) {

                                        if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'QT' && exit == 1)
                                            window.location.href = baseURL + "DocumentSell/Quotation";
                                        else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'BL' && exit == 1)
                                            window.location.href = baseURL + "DocumentSell/BillingNote";
                                        else if (QueryString('ref_key') != undefined && (QueryString('ref_info') == 'QT' || QueryString('ref_info') == 'BL'))
                                            window.location.href = baseURL + "DocumentSell/InvoiceAdd?InvoiceKey=" + responseInvoice.InvoiceKey;
                                        else if (exit == 1)
                                            window.location.href = baseURL + "DocumentSell/Invoice";
                                        else {
                                            $scope.InvoiceAdd.InvoiceKey = responseInvoice.InvoiceKey;
                                            $scope.InvoiceAdd.InvoiceStatus = responseInvoice.InvoiceStatus;
                                            $scope.InvoiceAdd.InvoiceStatusName = responseInvoice.InvoiceStatusName;
                                            $scope.InvoiceAdd.CustomerKey = responseInvoice.CustomerKey;

                                            _.each($scope.InvoiceAdd.InvoiceDetail, function (item) {
                                                var detail = _.where(responseInvoice.InvoiceDetail, { Sequence: item.Sequence })[0];
                                                if (detail != null) {
                                                    item.ProductKey = detail.ProductKey;
                                                    item.InvoiceKey = detail.InvoiceKey;
                                                    item.InvoiceDetailKey = detail.InvoiceDetailKey;
                                                }
                                            });

                                            $scope.ProductDetailAdd = [];
                                            $scope.table.binding = 0;
                                            showSuccessToast();

                                            if ($scope.InvoiceAdd.InvoiceKey != undefined && $scope.Print == "1") {
                                                $scope.document.DocKey = $scope.InvoiceAdd.InvoiceKey;
                                                $scope.document.DocNo = $scope.InvoiceAdd.InvoiceNo;
                                                $scope.document.original = $scope.document.copy = true;
                                                $('#modal-content-report').modal('show');
                                            }
                                            $scope.Print = "0";
                                        }
                                    }
                                }
                                else {
                                    $scope.table.binding = 0;
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    /**************Select ContactBook **************/
    $scope.OnClearContact = function () {
        $scope.InvoiceAdd.CustomerKey = $scope.InvoiceAdd.CustomerName = $scope.InvoiceAdd.CustomerTaxID =
            $scope.InvoiceAdd.CustomerContactName = $scope.InvoiceAdd.CustomerContactPhone = $scope.InvoiceAdd.CustomerContactEmail =
            $scope.InvoiceAdd.CustomerAddress = $scope.InvoiceAdd.CustomerBranch = $scope.InvoiceAdd.CreditDay = undefined;
    };

    $scope.OnClickContactPopup = function () {
        $scope.SearchContact.InputFilter = [];
        $scope.contactcurrentPage = 0;
        $scope.contact = [];
        $scope.contact.binding = 0;
        $scope.contactretpage = [];
        $scope.contactrange();
        $('#modalcontact-list').modal('show');
    };

    $scope.OnClickContactSearch = function () {
        $scope.Parameter.Param_Contact = contactService.filterContact($scope.SearchContact.InputFilter, $scope.Parameter.Param_Contact, $scope.Parameter.Param_ContactMain);
        if ($scope.Parameter.Param_Contact == undefined)
            $scope.Parameter.Param_Contact = [];
    };

    $scope.OnClickContactSelection = function (contactkey) {
        var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: contactkey })[0];
        if (contact != undefined) {
            $scope.OnClearContact();
            $scope.InvoiceAdd.CustomerKey = contact.ContactKey;
            $scope.InvoiceAdd.CustomerName = contact.BusinessName;
            $scope.InvoiceAdd.CustomerTaxID = contact.TaxID;
            $scope.InvoiceAdd.CustomerContactName = contact.ContactName;
            $scope.InvoiceAdd.CustomerContactPhone = contact.ContactMobile;
            $scope.InvoiceAdd.CustomerContactEmail = contact.ContactEmail;
            $scope.InvoiceAdd.CustomerAddress = contact.Address;
            $scope.InvoiceAdd.CustomerBranch = contact.BranchName;
            $scope.InvoiceAdd.CreditDay = contact.CreditDate;
            $scope.OnChangeCredit();
            $('#modalcontact-list').modal('hide');
        }
    };

    $scope.OnClickContactAdd = function () {
        $scope.BusinessContactAdd = [];
        $scope.BusinessContactAdd.BusinessType = 'C';
        $scope.BusinessContactAdd.ContactType1 = true;
        $scope.BusinessContactAdd.ContactType2 = false;
        $scope.BusinessContactAdd.BranchType = "H";
        $scope.BusinessContactAdd.AccountType = 'S';

        var type = $scope.Parameter.Param_Quotation.Param_Bank.filter(function (item) { return item.ID == undefined; });
        if (type.length > 0)
            $scope.BusinessContactAdd.SelectBank = type[0];

        $('#modalcontact-add').modal('show');
    };

    $scope.OnClickContactSave = function () {
        try {
            if ($scope.BusinessContactAdd.BusinessName == undefined || $scope.BusinessContactAdd.BusinessName == "")
                throw "กรุณาระบุชื่อธุรกิจ";
            else if ($scope.BusinessContactAdd.ContactType1 == false && $scope.BusinessContactAdd.ContactType2 == false)
                throw "กรุณาระบุประเภท";
            else if ($scope.BusinessContactAdd.ContactEmail != undefined && $scope.BusinessContactAdd.ContactEmail != "" && !ValidateEmail($scope.BusinessContactAdd.ContactEmail))
                throw "กรุณาแก้ไขอีเมล เนื่องจากรูปแบบอีเมลไม่ถูกต้อง";
            else {
                var qq = $q.all([contactService.postNewContact($scope.BusinessContactAdd)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var contact = data[0].data.responsedata;
                                if (contact != undefined) {
                                    $scope.OnClearContact();
                                    $scope.InvoiceAdd.CustomerKey = contact.ContactKey;
                                    $scope.InvoiceAdd.CustomerName = contact.BusinessName;
                                    $scope.InvoiceAdd.CustomerTaxID = contact.TaxID;
                                    $scope.InvoiceAdd.CustomerContactName = contact.ContactName;
                                    $scope.InvoiceAdd.CustomerContactPhone = contact.ContactMobile;
                                    $scope.InvoiceAdd.CustomerContactEmail = contact.ContactEmail;
                                    $scope.InvoiceAdd.CustomerAddress = contact.Address;
                                    $scope.InvoiceAdd.CustomerBranch = contact.BranchName;
                                    $scope.InvoiceAdd.CreditDay = contact.CreditDate;
                                    $scope.OnChangeCredit();
                                }
                                $('#modalcontact-add').modal('hide');
                                showSuccessToast();

                                var q1 = $q.all([paramService.getCustomer()]).then(function (data) {
                                    if (data[0] != undefined && data[0] != "") {
                                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                            $scope.SearchContact = [];
                                            $scope.Parameter.Param_Contact = [];
                                            $scope.Parameter.Param_ContactMain = [];
                                            $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[0].data.responsedata;
                                        }
                                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                    }
                                });

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(response.data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.contactcurrentPage = 0;

    $scope.contactLimitFirst = 0;
    $scope.contactLimitPage = 5;
    $scope.contactitemsPerPage = 10;

    $scope.contactpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Contact.length / $scope.contactitemsPerPage) - 1;
    };

    $scope.contactrange = function () {
        $scope.contactitemsCount = $scope.Parameter.Param_Contact.length;
        $scope.contactpageshow = $scope.contactpageCount() > $scope.contactLimitPage && $scope.contactpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.contactpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.contactpageshowdata = 1;
        $scope.contactretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.contactpcontactPage = function () {
        if ($scope.contactcurrentPage > 0) {
            $scope.contactcurrentPage--;
        }

        if ($scope.contactcurrentPage < $scope.contactLimitFirst && $scope.contactcurrentPage >= 1) {
            $scope.contactLimitFirst = $scope.contactLimitFirst - 5;
            $scope.contactLimitPage = $scope.contactLimitPage - 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }
    };

    $scope.contactpcontactPageDisabled = function () {
        return $scope.contactcurrentPage === 0 ? "disabled" : "";
    };

    $scope.contactnextPage = function () {
        if ($scope.contactcurrentPage < $scope.contactpageCount()) {
            $scope.contactcurrentPage++;
        }

        if ($scope.contactcurrentPage >= $scope.contactLimitPage && $scope.contactcurrentPage <= $scope.contactpageCount()) {
            $scope.contactLimitFirst = $scope.contactLimitFirst + 5;
            $scope.contactLimitPage = $scope.contactLimitPage + 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }

    };

    $scope.contactnextPageDisabled = function () {
        return $scope.contactcurrentPage === $scope.contactpageCount() ? "disabled" : "";
    };

    $scope.contactsetPage = function (n) {
        $scope.contactcurrentPage = n;
    };

    /**************Select Revenue **************/

    $scope.OnClickRevenuePopup = function () {
        $scope.SearchRevenue = [];
        $scope.Parameter.Param_Revenue = [];
        $scope.revenue = [];
        $scope.revenue.binding = 0;
        $scope.revitemsCount = 0;
        $('#modalrevenute-list').modal('show');
    };

    $scope.OnClickFilterRevenue = function () {
        if ($scope.SearchRevenue.InputText != undefined && $scope.SearchRevenue.InputText != "") {
            $scope.revenue.binding = 1;
            var qq = $q.all([contactService.filterRevenue($scope.SearchRevenue.InputText)]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == 200) {
                            if (data[0].data.responsedata == undefined)
                                $scope.Parameter.Param_Revenue = [];
                            else {
                                $scope.Parameter.Param_Revenue = data[0].data.responsedata;
                                $scope.revretpage = [];
                                $scope.revrange();
                                $scope.revenue.binding = 0;
                            }
                        }
                        else {
                            $scope.revenue.binding = 0;
                            showErrorToast(data[0].data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(response.data.errormessage);
                }
            });
        }
    };

    $scope.OnClickRevenueSelection = function (index) {
        try {
            var revenue = angular.copy($scope.Parameter.Param_Revenue[index]);
            $scope.OnClearContact();
            $scope.InvoiceAdd.CustomerName = revenue.CompanyName;
            $scope.InvoiceAdd.CustomerTaxID = revenue.NID;
            $scope.InvoiceAdd.CustomerAddress = revenue.Address;
            $scope.InvoiceAdd.CustomerBranch = revenue.BranchNoName;
            $scope.InvoiceAdd.CreditDay = 0;
            $('#modalrevenute-list').modal('hide');
        }
        catch (err) {
            showErrorToast(response.data.errormessage);
        }
    };

    $scope.revcurrentPage = 0;

    $scope.revLimitFirst = 0;
    $scope.revLimitPage = 5;
    $scope.revitemsPerPage = 10;

    $scope.revpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Revenue.length / $scope.revitemsPerPage) - 1;
    };

    $scope.revrange = function () {
        $scope.revitemsCount = $scope.Parameter.Param_Revenue.length;
        $scope.revpageshow = $scope.revpageCount() > $scope.revLimitPage && $scope.revpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.revpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.revpageshowdata = 1;
        $scope.revretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.revprevPage = function () {
        if ($scope.revcurrentPage > 0) {
            $scope.revcurrentPage--;
        }

        if ($scope.revcurrentPage < $scope.revLimitFirst && $scope.revcurrentPage >= 1) {
            $scope.revLimitFirst = $scope.revLimitFirst - 5;
            $scope.revLimitPage = $scope.revLimitPage - 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }
    };

    $scope.revprevPageDisabled = function () {
        return $scope.revcurrentPage === 0 ? "disabled" : "";
    };

    $scope.revnextPage = function () {
        if ($scope.revcurrentPage < $scope.revpageCount()) {
            $scope.revcurrentPage++;
        }

        if ($scope.revcurrentPage >= $scope.revLimitPage && $scope.revcurrentPage <= $scope.revpageCount()) {
            $scope.revLimitFirst = $scope.revLimitFirst + 5;
            $scope.revLimitPage = $scope.revLimitPage + 5;
            for (var i = 1; i <= $scope.revretpage.length; i++) {
                if (i >= $scope.revLimitFirst && i <= $scope.revLimitPage) {
                    $scope.revretpage[i - 1].show = 1;
                }
                else
                    $scope.revretpage[i - 1].show = 0;
            }
        }

    };

    $scope.revnextPageDisabled = function () {
        return $scope.revcurrentPage === $scope.revpageCount() ? "disabled" : "";
    };

    $scope.revsetPage = function (n) {
        $scope.revcurrentPage = n;
    };

    /**************Select Product **************/

    $scope.OnClickProductPopup = function () {
        $scope.SearchProduct.InputFilter = [];
        $scope.productcurrentPage = 0;
        $scope.product = [];
        $scope.product.binding = 0;
        $scope.productretpage = [];
        $scope.productrange();
        $('#modalproduct-list').modal('show');
    };

    $scope.OnClickProductSearch = function () {
        $scope.Parameter.Param_Product = productService.filterProduct($scope.SearchProduct.InputFilter, $scope.Parameter.Param_Product, $scope.Parameter.Param_ProductMain);
        if ($scope.Parameter.Param_Product == undefined)
            $scope.Parameter.Param_Product = [];
    };

    $scope.OnClickProductSelection = function (productkey) {
        var product = _.where($scope.Parameter.Param_Product, { ProductKey: productkey })[0];
        if (product != undefined) {
            $scope.OnClickClearDetail();
            $scope.ProductDetailAdd.ProductKey = product.ProductKey;
            $scope.ProductDetailAdd.Name = product.ProductName;
            $scope.ProductDetailAdd.Description = product.ProductDescription;
            $scope.ProductDetailAdd.Quantity = '1.00';
            $scope.ProductDetailAdd.Unit = product.UnitTypeName;
            $scope.ProductDetailAdd.DiscountType = $scope.InvoiceAdd.IsDiscountHeaderType;
            $scope.ProductDetailAdd.DiscountPercent = '0.00';
            $scope.ProductDetailAdd.Discount = '0.00';
            $scope.ProductDetailAdd.VatType = '2';

            if (product.SellTaxType == '1' || product.SellTaxType == '3' || product.SellTaxType == '4') {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var sell = 0, selltax = 0, selllast = 0;
                sell = ConvertToDecimal(product.SellPrice == undefined ? 0 : product.SellPrice);
                selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                selllast = sell + selltax;

                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(selllast, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(selltax, 2);

                if (product.SellTaxType == '3') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '1';
                }
                else if (product.SellTaxType == '4') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '3';
                }
            }
            else if (product.SellTaxType == '2') {

                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(product.SellAfterTaxPrice, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(product.SellPrice, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(product.SellTaxPrice, 2);
            }

            if ($scope.InvoiceAdd.TaxType == 'N')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
            else if ($scope.InvoiceAdd.TaxType == 'V')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

            $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $('#modalproduct-list').modal('hide');
        }
    };

    $scope.OnClickProductAdd = function () {
        $scope.BusinessProductAdd = [];
        $scope.BusinessProductAdd.ProductType = 'S';
        $scope.BusinessProductAdd.SellTaxType = '1';
        $scope.BusinessProductAdd.BuyTaxType = '1';
        initdropify('');

        $('#modalproduct-add').modal('show');
    };

    function initdropify(path) {
        $("#product_thumnail").addClass('dropify');
        var publicpath_identity_picture = path;

        var drEvent = $('.dropify').dropify();
        drEvent.on('dropify.afterClear', function (event, element) {
            $scope.DeleteImages = 1;
        });
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = publicpath_identity_picture;
        drEvent.destroy();

        drEvent.init();

        $('.dropify#identity_picture').dropify({
            defaultFile: publicpath_identity_picture
        });

        $('.dropify').dropify();
    }

    $scope.OnClickProductSave = function () {
        try {
            if ($scope.BusinessProductAdd.ProductName == undefined || $scope.BusinessProductAdd.ProductName == "")
                throw "กรุณาระบุชื่อสินค้า/บริการ";
            else {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var unit = document.getElementById('input_unittype').value;
                var category = document.getElementById('input_categorytype').value;


                var qq = $q.all([productService.postNewProduct($scope.BusinessProductAdd, vat, unit, category)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var product = data[0].data.responsedata;

                                if (product != undefined) {
                                    //$scope.ProductDetailAdd.ProductKey = product.ProductKey;
                                    //$scope.ProductDetailAdd.Name = product.ProductName;
                                    //$scope.ProductDetailAdd.Description = product.ProductDescription;
                                    //$scope.ProductDetailAdd.Quantity = '1.00';
                                    //$scope.ProductDetailAdd.Unit = product.UnitTypeName;
                                    //$scope.ProductDetailAdd.Price = AFormatNumber($scope.InvoiceAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);
                                    //$scope.ProductDetailAdd.Total = AFormatNumber($scope.InvoiceAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);

                                    if ($scope.SelectedFiles != undefined && $scope.Upload == 1) {
                                        try {

                                            var input = document.getElementById("product_thumnail");
                                            var files = input.files;
                                            var formData = new FormData();

                                            for (var i = 0; i != files.length; i++) {
                                                formData.append("files", files[i]);
                                            }

                                            $.ajax(
                                                {
                                                    url: baseURL + "Product/UploadPictureProducts?productkey=" + product.ProductKey,
                                                    data: formData,
                                                    processData: false,
                                                    contentType: false,
                                                    type: "POST",
                                                    success: function (data) {
                                                        $('#modalproduct-add').modal('hide');
                                                        showSuccessToast();
                                                    },
                                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                        showErrorToast(textStatus);
                                                    }
                                                }
                                            );
                                        }
                                        catch (ex) {
                                            showErrorToast(ex);
                                        }
                                    }
                                    else {
                                        $('#modalproduct-add').modal('hide');
                                        showSuccessToast();
                                    }


                                    var q1 = $q.all([paramService.getProduct()]).then(function (data) {
                                        if (data[0] != undefined && data[0] != "") {
                                            if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                                $scope.SearchProduct = [];
                                                $scope.Parameter.Param_Product = [];
                                                $scope.Parameter.Param_ProductMain = [];

                                                $scope.Parameter.Param_ProductMain = data[0].data.responsedata;

                                                _.each($scope.Parameter.Param_ProductMain, function (item) {
                                                    item.SellPrice = AFormatNumber(item.SellPrice, 2);
                                                    item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                                                    item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                                                    item.SellPriceText = item.SellTaxType != '2' ? AFormatNumber(item.SellPrice, 2) : AFormatNumber(item.SellAfterTaxPrice, 2);
                                                    item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                                                    item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                                                    item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                                                });

                                                $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;

                                                $scope.OnClickProductSelection(product.ProductKey);
                                            }
                                            else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                        }
                                    });
                                }

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(data[0].data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.productcurrentPage = 0;

    $scope.productLimitFirst = 0;
    $scope.productLimitPage = 5;
    $scope.productitemsPerPage = 10;

    $scope.productpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Product.length / $scope.productitemsPerPage) - 1;
    };

    $scope.productrange = function () {
        $scope.productitemsCount = $scope.Parameter.Param_Product.length;
        $scope.productpageshow = $scope.productpageCount() > $scope.productLimitPage && $scope.productpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.productpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.productpageshowdata = 1;
        $scope.productretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.productpproductPage = function () {
        if ($scope.productcurrentPage > 0) {
            $scope.productcurrentPage--;
        }

        if ($scope.productcurrentPage < $scope.productLimitFirst && $scope.productcurrentPage >= 1) {
            $scope.productLimitFirst = $scope.productLimitFirst - 5;
            $scope.productLimitPage = $scope.productLimitPage - 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }
    };

    $scope.productpproductPageDisabled = function () {
        return $scope.productcurrentPage === 0 ? "disabled" : "";
    };

    $scope.productnextPage = function () {
        if ($scope.productcurrentPage < $scope.productpageCount()) {
            $scope.productcurrentPage++;
        }

        if ($scope.productcurrentPage >= $scope.productLimitPage && $scope.productcurrentPage <= $scope.productpageCount()) {
            $scope.productLimitFirst = $scope.productLimitFirst + 5;
            $scope.productLimitPage = $scope.productLimitPage + 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }

    };

    $scope.productnextPageDisabled = function () {
        return $scope.productcurrentPage === $scope.productpageCount() ? "disabled" : "";
    };

    $scope.productsetPage = function (n) {
        $scope.productcurrentPage = n;
    };

    $scope.UploadFiles = function (files) {
        $scope.Upload = 1;
        $scope.SelectedFiles = files;
    };

     /****************** Document Attach ************************/
    $scope.OnLoadAttach = function () {
        try {
            $http.get(baseURL + "DocumentSell/GetDocumentAttach?documentkey=" + $scope.InvoiceAdd.InvoiceKey + "&documenttype=" + headerdoc + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.DocumentAttach = [];
                            $scope.DocumentAttach = response.data.responsedata;
                        }
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnUploadAttach = function (files) {
        try {
            if ($scope.InvoiceAdd.InvoiceKey == undefined || $scope.InvoiceAdd.InvoiceKey == "")
                throw "กรุณากดปุ่ม บันทึก ก่อนแนบไฟล์เอกสาร";
            else if ($scope.DocumentAttach.length > 3)
                throw "สามารถแนบไฟล์สูงสุดเพียง 3 ไฟล์ เท่านั้น";
            else if (files.length > 0) {
                var data = {
                    DocumentKey: $scope.InvoiceAdd.InvoiceKey,
                    DocumentNo: $scope.InvoiceAdd.InvoiceNo,
                    DocumentType: headerdoc,
                    AttachName: files[0].name,
                    AttachExtension: getFileExtension(files[0].name)
                };
                $http.post(baseURL + "DocumentSell/PostDocumentAttach", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    try {
                                        var attachfile = response.data.responsedata;
                                        var input = document.getElementById("document_attach");
                                        var files = input.files;
                                        var formData = new FormData();

                                        for (var i = 0; i != files.length; i++) {
                                            formData.append("files", files[i]);
                                        }

                                        $.ajax(
                                            {
                                                url: baseURL + "DocumentSell/UploadDocumentAttach?attachkey=" + attachfile.AttachKey,
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                type: "POST",
                                                success: function (data) {

                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();

                                                    $scope.OnLoadAttach();
                                                    showSuccessText('แนบไฟล์สำเร็จ');
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();
                                                    showErrorToast(textStatus);
                                                }
                                            }
                                        );
                                    }
                                    catch (ex) {
                                        var drEvent = $('.dropify').dropify();
                                        drEvent = drEvent.data('dropify');
                                        drEvent.resetPreview();
                                        drEvent.clearElement();
                                        drEvent.destroy();

                                        drEvent.init();
                                        showErrorToast(ex);
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            var drEvent = $('.dropify').dropify();
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.destroy();

            drEvent.init();
            showWariningToast(err);
        }
    };

    $scope.OnClickDeleteAttach = function (attachkey) {
        var data = {
            DocumentKey: $scope.InvoiceAdd.InvoiceKey,
            DocumentNo: $scope.InvoiceAdd.InvoiceNo,
            DocumentType: headerdoc,
            AttachKey: attachkey
        };
        $http.post(baseURL + "DocumentSell/DeleteDocumentAttach", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            try {
                                showDeleteSuccessToast();
                                $scope.OnLoadAttach();
                            }
                            catch (err) {
                                showErrorToast(err);
                            }
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickShowAttach = function (attachkey) {
        var attactfile = _.where($scope.DocumentAttach, { AttachKey: attachkey })[0];
        if (attactfile != undefined) {
            $scope.DocumentAttachView = [];
            $scope.DocumentAttachView = attactfile;

            if (attactfile.AttachExtension == 'pdf') {
                $scope.DocumentAttachView.AttachHeight = '850px';
                PDFObject.embed(baseURL + 'uploads/attach/' + attactfile.AttachPath, "#exampleattach");
            }
            else {
                $scope.DocumentAttachView.AttachHeight = 'auto';
                $scope.DocumentAttachView.AttachShow = baseURL + 'uploads/attach/' + attactfile.AttachPath;
            }
            $('#modal-attach').modal('show');
        }
    };
});


