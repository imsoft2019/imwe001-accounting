﻿WEACCTAPP.controller('receiptcumulativeaddcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, contactService, productService, $q, Upload) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'RE';

    $scope.DocumentAttach = [];
    $scope.ReceiptAdd = [];
    $scope.ReceiptAdd.InvoiceDetail = [];
    $scope.Parameter = [];
    $scope.UserProfile = [];
    $scope.Parameter.Param_Contact = [];
    $scope.Parameter.Param_Document = [];
    $scope.Parameter.Param_Product = [];
    $scope.Parameter.Param_Employee = [];
    $scope.Parameter.Param_Revenue = [];
    $scope.table = [];

    var gCustomer = paramService.getCustomer();
    var gProduct = paramService.getProduct();
    var gEmployee = paramService.getEmployee();
    var gParamQuotation = paramService.getParameterQuotation();
    var gProfile = paramService.getProfile();
    var gRemark = paramService.getDocumentRemark(headerdoc);
    var gBizBank = paramService.getBankBusiness();

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        $scope.table.binding = 1;
        $scope.CheckingAll = false;
        var prefix = { asofdate: GetDatetimeNow(), documenttype: headerdoc };

        var qq = $q.all([paramService.getDocumentNo(prefix), gCustomer, gProduct, gEmployee, gParamQuotation, gProfile, gRemark, gBizBank]).then(function (data) {
            try {
                /****** GetDocument NO ******/
                if (data[0] != undefined && data[0] != "") {
                    if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.ReceiptAdd.ReceiptNo = data[0].data.responsedata; }
                    else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                }

                /****** Get Customer ******/
                if (data[1] != undefined && data[1] != "") {
                    if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                        $scope.SearchContact = [];
                        $scope.Parameter.Param_Contact = [];
                        $scope.Parameter.Param_ContactMain = [];
                        $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                    }
                    else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                }

                /****** Get Product ******/
                if (data[2] != undefined && data[2] != "") {
                    if (data[2].data.responsecode == '200' && data[2].data.responsedata != undefined && data[2].data.responsedata != "") {
                        $scope.SearchProduct = [];
                        $scope.Parameter.Param_Product = [];
                        $scope.Parameter.Param_ProductMain = [];

                        $scope.Parameter.Param_ProductMain = data[2].data.responsedata;

                        _.each($scope.Parameter.Param_ProductMain, function (item) {
                            item.SellPrice = AFormatNumber(item.SellPrice, 2);
                            item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                            item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                            item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                            item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                        });

                        $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;
                    }
                    else if (data[2].data.responsecode == '400') { showErrorToast(data[2].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[3] != undefined && data[3] != "") {
                    if (data[3].data.responsecode == '200' && data[3].data.responsedata != undefined && data[3].data.responsedata != "") {
                        $scope.Parameter.Param_Employee = [];
                        $scope.Parameter.Param_Employee = data[3].data.responsedata;
                    }
                    else if (data[3].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Parameter Receipt ******/
                if (data[4] != undefined && data[4] != "") {
                    if (data[4].data.responsecode == '200' && data[4].data.responsedata != undefined && data[4].data.responsedata != "") {
                        $scope.Parameter.Param_Quotation = [];
                        $scope.Parameter.Param_Quotation = data[4].data.responsedata;

                        var substringMatcher = function (strs) {
                            return function findMatches(q, cb) {
                                if (q == '') {
                                    cb(strs);
                                }
                                else {
                                    var matches, substringRegex;

                                    // an array that will be populated with substring matches
                                    matches = [];

                                    // regex used to determine if a string contains the substring `q`
                                    var substrRegex = new RegExp(q, 'i');

                                    // iterate through the pool of strings and for any string that
                                    // contains the substring `q`, add it to the `matches` array
                                    for (var i = 0; i < strs.length; i++) {
                                        if (substrRegex.test(strs[i])) {
                                            matches.push(strs[i]);
                                        }
                                    }

                                    cb(matches);
                                }
                            };
                        };

                        var unittype = [];
                        _.each($scope.Parameter.Param_Quotation.Param_UnitType, function (item) { unittype.push(item.Name); });

                        $('#productunit .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0
                        }, {
                                name: 'unittype',
                                limit: 20,
                                source: substringMatcher(unittype)
                            });


                        var category = [];
                        _.each($scope.Parameter.Param_Quotation.Param_Category, function (item) { category.push(item.Name); });
                        $('#productcategory .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0,
                        }, {
                                name: 'category',
                                limit: 20,
                                source: substringMatcher(category)
                            });
                    }
                    else if (data[4].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[5] != undefined && data[5] != "") {
                    if (data[5].data.responsecode == '200' && data[5].data.responsedata != undefined && data[5].data.responsedata != "") {
                        $scope.UserProfile = [];
                        $scope.UserProfile = data[5].data.responsedata;
                    }
                    else if (data[5].data.responsecode == '400') { showErrorToast(data[5].data.errormessage); }
                }

                /****** Get Remark ******/
                if (data[6] != undefined && data[6] != "") {
                    if (data[6].data.responsecode == '200' && data[6].data.responsedata != undefined && data[6].data.responsedata != "") {
                        $scope.ReceiptAdd.Remark = data[6].data.responsedata.DocumentText;
                    }
                    else if (data[6].data.responsecode == '400') { showErrorToast(data[6].data.errormessage); }
                }

                /****** Get Business Bank ******/
                if (data[7] != undefined && data[7] != "") {
                    if (data[7].data.responsecode == '200' && data[7].data.responsedata != undefined && data[7].data.responsedata != "") {
                        $scope.Parameter.BizBank = data[7].data.responsedata;
                    }
                    else if (data[7].data.responsecode == '400') { showErrorToast(data[7].data.errormessage); }
                }

                if (QueryString('ReceiptKey') != undefined) {

                    $http.get(baseURL + "DocumentSell/GetBusinessReceiptbykey?ReceiptKey=" + QueryString('ReceiptKey') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.ReceiptAdd = [];
                                    $scope.ReceiptAdd = response.data.responsedata;

                                    $scope.ReceiptAdd.ReceiptDate = formatDate($scope.ReceiptAdd.ReceiptDate);
                                    $('#inputreceiptdate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptDate);
                                    /*Credit */
                                    $scope.ReceiptAdd.DueDate = $scope.ReceiptAdd.DueDate != undefined && $scope.ReceiptAdd.DueDate != "" ? formatDate($scope.ReceiptAdd.DueDate) : "";

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.ReceiptAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.ReceiptAdd.SelectSale = type[0];

                                    $scope.ReceiptAdd.IsAdjustDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false; // เพิ่มรายการปรับลด
                                    var fixeddis = $scope.ReceiptAdd.IsAdjustDiscount;
                                    $scope.ReceiptAdd.IsAdjustDiscountFix = fixeddis;
                                    if ($scope.ReceiptAdd.IsAdjustDiscount) {
                                        $scope.ReceiptAdd.AdjustDiscountAmount = AFormatNumber($scope.ReceiptAdd.AdjustDiscountAmount, 2);//
                                        $scope.ReceiptAdd.PaymentAmount = AFormatNumber($scope.ReceiptAdd.PaymentAmount, 2);//
                                    }


                                    $scope.ReceiptAdd.TotalQTY = $scope.ReceiptAdd.ReceiptCumulativeDetail.length;
                                    $scope.ReceiptAdd.TotalAmount = $scope.ReceiptAdd.TaxType == 'Y' ? AFormatNumber($scope.ReceiptAdd.TotalBeforeVatAmount, 2) : AFormatNumber($scope.ReceiptAdd.TotalAmount, 2);
                                    $scope.ReceiptAdd.DiscountPercent = AFormatNumber($scope.ReceiptAdd.DiscountPercent, 2);
                                    $scope.ReceiptAdd.DiscountAmount = AFormatNumber($scope.ReceiptAdd.DiscountAmount, 2);
                                    $scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber($scope.ReceiptAdd.TotalAfterDiscountAmount, 2);
                                    $scope.ReceiptAdd.ExemptAmount = AFormatNumber($scope.ReceiptAdd.ExemptAmount, 2);
                                    $scope.ReceiptAdd.VatableAmount = AFormatNumber($scope.ReceiptAdd.VatableAmount, 2);
                                    $scope.ReceiptAdd.VAT = AFormatNumber($scope.ReceiptAdd.VAT, 2);
                                    $scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber($scope.ReceiptAdd.TotalBeforeVatAmount, 2);
                                    $scope.ReceiptAdd.GrandAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);

                                    $scope.ReceiptAdd.Signature = $scope.ReceiptAdd.Signature == '1' ? true : false;

                                    _.each($scope.ReceiptAdd.ReceiptCumulativeDetail, function (item) {
                                        item.DocumentDate = formatDate(item.DocumentDate);
                                        item.DueDate = formatDate(item.DueDate);
                                        item.TotalAmount = AFormatNumber(item.TotalAmount, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.DiscountAmount = AFormatNumber(item.DiscountAmount, 2);
                                        item.TotalAfterDiscountAmount = AFormatNumber(item.TotalAfterDiscountAmount, 2);
                                        item.ExemptAmount = AFormatNumber(item.ExemptAmount, 2);
                                        item.VatableAmount = AFormatNumber(item.VatableAmount, 2);
                                        item.VAT = AFormatNumber(item.VAT, 2);
                                        item.TotalBeforeVatAmount = AFormatNumber(item.TotalBeforeVatAmount, 2);
                                        item.WithholdingRate = AFormatNumber(item.WithholdingRate, 2);
                                        item.WithholdingAmount = AFormatNumber(item.WithholdingAmount, 2);
                                        item.GrandAmountAfterWithholding = AFormatNumber(item.GrandAmountAfterWithholding, 2);
                                        item.GrandAmount = AFormatNumber(item.GrandAmount, 2);
                                    });

                                    if ($scope.ReceiptAdd.ReceiptInfo != undefined) {
                                        $scope.ReceiptAdd.ReceiptInfo.PaymentDate = formatDate($scope.ReceiptAdd.ReceiptInfo.PaymentDate);
                                        $('#inputreceivedate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.PaymentDate);
                                        $scope.ReceiptAdd.ReceiptInfo.PaymentAmount = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.PaymentAmount, 2);
                                        $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount, 2);
                                        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax == '1' ? AFormatNumber($scope.ReceiptAdd.ReceiptInfo.WithholdingAmount, 2) : '0.00';
                                        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = $scope.ReceiptAdd.ReceiptInfo.IsDiscount == '1' ? AFormatNumber($scope.ReceiptAdd.ReceiptInfo.DiscountAmount, 2) : '0.00';
                                        $scope.ReceiptAdd.ReceiptInfo.ChequeDate = $scope.ReceiptAdd.ReceiptInfo.ChequeDate != undefined ? formatDate($scope.ReceiptAdd.ReceiptInfo.ChequeDate) : undefined;
                                        $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber($scope.ReceiptAdd.ReceiptInfo.Balance, 2);

                                        var htmltag;
                                        htmltag = "<div class='title' style='width:220px;' >รายละเอียดการชำระ </div> <hr class='mb-2'> ";
                                        htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ยอดที่เรียกเก็บ </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.PaymentAmount + " บาท</div></div>";
                                        if ($scope.ReceiptAdd.ReceiptInfo.IsDiscount == '1')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + $scope.ReceiptAdd.ReceiptInfo.DiscountTypeName + " </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.DiscountAmount + " บาท</div></div> ";
                                        if ($scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax == '1')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ภาษีหัก ณ ที่จ่าย </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount + " บาท</div> </div>";
                                        if ($scope.ReceiptAdd.ReceiptInfo.BalanceType == 'Lost')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + $scope.ReceiptAdd.ReceiptInfo.CauseTypeName.replace('/เงินเกิน', '') + " </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.Balance + " บาท</div> </div>";
                                        if ($scope.ReceiptAdd.ReceiptInfo.BalanceType == 'Over')
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + $scope.ReceiptAdd.ReceiptInfo.CauseTypeName.replace('เงินขาด/', '') + " </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.Balance + " บาท</div> </div>";
                                        htmltag = htmltag + " <hr class='mb-2'><div class='row'> <div class='col-lg-6'> ยอดรับชำระ </div>  <div class='col-lg-6 text-right'> " + $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount + " บาท</div> </div>";
                                        $scope.ReceiptAdd.ReceiptInfo.TagHtml = htmltag;
                                    }

                                    if (QueryString('succes_redic') != undefined && QueryString('succes_redic') == '1')
                                        showSuccessToast();

                                    $scope.OnLoadAttach();

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else if (QueryString('ref_key') != undefined && QueryString('ref_info') != undefined) {
                    // New document With Invoice
                    $http.get(baseURL + "DocumentSell/GetBusinessDcoumentReference?DocumentKey=" + QueryString('ref_key') + "&DocumentType=" + QueryString('ref_info') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.ReceiptAdd = [];
                                    $scope.ReceiptAdd = response.data.responsedata;
                                    $scope.ReceiptAdd.ReceiptCumulativeDetail = [];

                                    /** Credit Note Values*/
                                    if (QueryString('ref_info') == 'CN') {
                                        $scope.ReceiptAdd.DocumentKey = $scope.ReceiptAdd.CreditNoteKey;
                                        $scope.ReceiptAdd.DocumentNo = $scope.ReceiptAdd.CreditNoteNo;
                                        $scope.ReceiptAdd.ReferenceNo = '';
                                        $scope.ReceiptAdd.ProjectName = '';
                                    }
                                    else if (QueryString('ref_info') == 'INV') {
                                        $scope.ReceiptAdd.DocumentKey = $scope.ReceiptAdd.InvoiceKey;
                                        $scope.ReceiptAdd.DocumentNo = $scope.ReceiptAdd.InvoiceNo;
                                        $scope.ReceiptAdd.ReferenceNo = '';
                                        $scope.ReceiptAdd.ProjectName = '';
                                    }
                                    else if (QueryString('ref_info') == 'DN') {
                                        $scope.ReceiptAdd.DocumentKey = $scope.ReceiptAdd.DebitNoteKey;
                                        $scope.ReceiptAdd.DocumentNo = $scope.ReceiptAdd.DebitNoteNo;
                                        $scope.ReceiptAdd.ReferenceNo = '';
                                        $scope.ReceiptAdd.ProjectName = '';
                                    }
                                    else if (QueryString('ref_info') == 'BL') {
                                        $scope.ReceiptAdd.DocumentKey = $scope.ReceiptAdd.BillingKey;
                                        $scope.ReceiptAdd.DocumentNo = $scope.ReceiptAdd.BillingNo;
                                        $scope.ReceiptAdd.BillingNoteCumulativeKey = $scope.ReceiptAdd.BillingKey;
                                        $scope.ReceiptAdd.BillingNoteCumulativeNo = $scope.ReceiptAdd.BillingNo;
                                        $scope.ReceiptAdd.ReferenceNo = $scope.ReceiptAdd.BillingNo;
                                    }


                                    $scope.ReceiptAdd.ReceiptStatus = "1";
                                    $scope.ReceiptAdd.ReceiptStatusName = "รอดำเนินการ";

                                    if (QueryString('ref_info') == 'CN' || QueryString('ref_info') == 'DN' || QueryString('ref_info') == 'INV') {
                                        var dock = { documentkey: QueryString('ref_key'), documentype: QueryString('ref_info') };
                                        var values = [];
                                        values.push(dock);

                                        $http.post(baseURL + "DocumentSell/GetBusinessCumulative", values, config).then(
                                            function (response) {
                                                try {
                                                    if (response != undefined && response != "") {
                                                        if (response.data.responsecode == 200) {
                                                            var responseDcoument = response.data.responsedata;
                                                            if (responseDcoument != undefined) {

                                                                var i = 1;
                                                                $scope.ReceiptAdd.ReceiptCumulativeDetail = [];
                                                                _.each(responseDcoument.Document_Invoice, function (item) {

                                                                    if (item.CreditType == 'CA')
                                                                        item.DueDate = GetDatetimeNow();
                                                                    else if (item.CreditType == 'CN')
                                                                        item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                                                    else if (item.CreditType == 'CD')
                                                                        item.DueDate = formatDate(item.DueDate);

                                                                    $scope.ReceiptAdd.ReceiptCumulativeDetail.push({
                                                                        Sequence: i,
                                                                        DocumentKey: item.InvoiceKey,
                                                                        DocumentNo: item.InvoiceNo,
                                                                        DocumentType: 'INV',
                                                                        DocumentCategoryKey: item.InvoiceKey,
                                                                        DocumentDate: formatDate(item.InvoiceDate),
                                                                        DueDate: item.DueDate,
                                                                        TaxType: item.TaxType,
                                                                        TaxTypeName: item.TaxTypeName,
                                                                        TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                                        IsDiscountHeader: item.IsDiscountHeader,
                                                                        IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                                        DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                                        DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                                        TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                                        IsTaxHeader: item.IsTaxHeader,
                                                                        IsTaxSummary: item.IsTaxSummary,
                                                                        ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                                        VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                                        VAT: AFormatNumber(item.VAT, 2),
                                                                        TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                                        IsWithholdingTax: item.IsWithholdingTax,
                                                                        WithholdingKey: item.WithholdingKey,
                                                                        WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                                        WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                                        GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                                        GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                                                    });
                                                                    i++;
                                                                });

                                                                _.each(responseDcoument.Document_CreditNote, function (item) {

                                                                    if (item.CreditType == 'CA')
                                                                        item.DueDate = GetDatetimeNow();
                                                                    else if (item.CreditType == 'CN')
                                                                        item.DueDate = SetDatetimeDay(formatDate(item.CreditNoteDate), item.CreditDay);
                                                                    else if (item.CreditType == 'CD')
                                                                        item.DueDate = formatDate(item.DueDate);

                                                                    $scope.ReceiptAdd.ReceiptCumulativeDetail.push({
                                                                        Sequence: i,
                                                                        DocumentKey: item.CreditNoteKey,
                                                                        DocumentNo: item.CreditNoteNo,
                                                                        DocumentType: 'CN',
                                                                        DocumentCategoryKey: item.InvoiceKey,
                                                                        DocumentDate: formatDate(item.CreditNoteDate),
                                                                        DueDate: item.DueDate,
                                                                        TaxType: item.TaxType,
                                                                        TaxTypeName: item.TaxTypeName,
                                                                        TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                                        IsDiscountHeader: item.IsDiscountHeader,
                                                                        IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                                        DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                                        DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                                        TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                                        IsTaxHeader: item.IsTaxHeader,
                                                                        IsTaxSummary: item.IsTaxSummary,
                                                                        ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                                        VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                                        VAT: AFormatNumber(item.VAT, 2),
                                                                        TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                                        IsWithholdingTax: item.IsWithholdingTax,
                                                                        WithholdingKey: item.WithholdingKey,
                                                                        WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                                        WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                                        GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                                        GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                                                    });
                                                                    i++;
                                                                });

                                                                _.each(responseDcoument.Document_DebitNote, function (item) {

                                                                    if (item.CreditType == 'CA')
                                                                        item.DueDate = GetDatetimeNow();
                                                                    else if (item.CreditType == 'CN')
                                                                        item.DueDate = SetDatetimeDay(formatDate(item.DebitNoteDate), item.CreditDay);
                                                                    else if (item.CreditType == 'CD')
                                                                        item.DueDate = formatDate(item.DueDate);

                                                                    $scope.ReceiptAdd.ReceiptCumulativeDetail.push({
                                                                        Sequence: i,
                                                                        DocumentKey: item.DebitNoteKey,
                                                                        DocumentNo: item.DebitNoteNo,
                                                                        DocumentType: 'DN',
                                                                        DocumentCategoryKey: item.InvoiceKey,
                                                                        DocumentDate: formatDate(item.DebitNoteDate),
                                                                        DueDate: item.DueDate,
                                                                        TaxType: item.TaxType,
                                                                        TaxTypeName: item.TaxTypeName,
                                                                        TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                                        IsDiscountHeader: item.IsDiscountHeader,
                                                                        IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                                        DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                                        DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                                        TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                                        IsTaxHeader: item.IsTaxHeader,
                                                                        IsTaxSummary: item.IsTaxSummary,
                                                                        ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                                        VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                                        VAT: AFormatNumber(item.VAT, 2),
                                                                        TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                                        IsWithholdingTax: item.IsWithholdingTax,
                                                                        WithholdingKey: item.WithholdingKey,
                                                                        WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                                        WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                                        GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                                        GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                                                    });
                                                                    i++;
                                                                });
                                                                $scope.OnChangeDetailCalculate();
                                                            }
                                                        }
                                                        else {
                                                            $scope.table.binding = 0;
                                                            showErrorToast(response.data.errormessage);
                                                        }
                                                    }
                                                }
                                                catch (err) {
                                                    $scope.table.binding = 0;
                                                    showErrorToast(err);
                                                }
                                            });
                                    }
                                    else if (QueryString('ref_info') == 'BL') {
                                        _.each($scope.ReceiptAdd.BillingCumulativeDetail, function (item) {
                                            item.DocumentDate = formatDate(item.DocumentDate);
                                            item.DueDate = formatDate(item.DueDate);
                                            item.TotalAmount = AFormatNumber(item.TotalAmount, 2);
                                            item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                            item.DiscountAmount = AFormatNumber(item.DiscountAmount, 2);
                                            item.TotalAfterDiscountAmount = AFormatNumber(item.TotalAfterDiscountAmount, 2);
                                            item.ExemptAmount = AFormatNumber(item.ExemptAmount, 2);
                                            item.VatableAmount = AFormatNumber(item.VatableAmount, 2);
                                            item.VAT = AFormatNumber(item.VAT, 2);
                                            item.TotalBeforeVatAmount = AFormatNumber(item.TotalBeforeVatAmount, 2);
                                            item.WithholdingRate = AFormatNumber(item.WithholdingRate, 2);
                                            item.WithholdingAmount = AFormatNumber(item.WithholdingAmount, 2);
                                            item.GrandAmountAfterWithholding = AFormatNumber(item.GrandAmountAfterWithholding, 2);
                                            item.GrandAmount = AFormatNumber(item.GrandAmount, 2);
                                            $scope.ReceiptAdd.ReceiptCumulativeDetail.push(item);
                                        });

                                        $scope.OnChangeDetailCalculate();

                                    }

                                    $scope.ReceiptAdd.DocumentOwner = QueryString('ref_info');

                                    /**  clear values */
                                    $scope.ReceiptAdd.ReceiptNo = data[0].data.responsedata;
                                    $scope.ReceiptAdd.ReceiptKey = undefined;


                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                    if (type.length > 0)
                                        $scope.ReceiptAdd.SelectSale = type[0];


                                    $scope.ReceiptAdd.ReceiptDate = GetDatetimeNow();
                                    $('#inputreceiptdate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptDate);

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.ReceiptAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.ReceiptAdd.SelectSale = type[0];

                                    $scope.ReceiptAdd.TaxType = 'N';
                                    $scope.ReceiptAdd.IsTaxHeader = false; //แยกรายการภาษี
                                    $scope.ReceiptAdd.IsDiscountHeader = false; //แยกรายการาส่วนลด
                                    $scope.ReceiptAdd.IsWithholdingTax = false; // หักภาษี ณ ที่จ่าย
                                    $scope.ReceiptAdd.IsAdjustDiscount = false; // ส่วนลดเพิ่มเติม
                                    $scope.ReceiptAdd.IsTaxSummary = true; // หักภาษี 7 % รวม
                                    $scope.ReceiptAdd.IsDiscountHeaderType = '%'; // ส่วนลดแยกรายการแบบ Percent

                                    //$scope.ReceiptAdd.TotalAmount = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.DiscountPercent = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.DiscountAmount = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.ExemptAmount = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.VatableAmount = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.VAT = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);
                                    //$scope.ReceiptAdd.GrandAmount = AFormatNumber(0, 2);

                                    $scope.ReceiptAdd.Signature = $scope.ReceiptAdd.Signature == '1' ? true : false;

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else {
                    /******* Initail ********/
                    $scope.SelectInvoice = '1';
                    $scope.ReceiptAdd.ReceiptDate = GetDatetimeNow();
                    $('#inputreceiptdate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptDate);
                    $scope.ReceiptAdd.TaxType = 'N';
                    $scope.ReceiptAdd.IsTaxHeader = false; //แยกรายการภาษี
                    $scope.ReceiptAdd.IsDiscountHeader = false; //แยกรายการาส่วนลด
                    $scope.ReceiptAdd.IsWithholdingTax = false; // หักภาษี ณ ที่จ่าย
                    $scope.ReceiptAdd.IsAdjustDiscount = false; // ส่วนลดเพิ่มเติม

                    $scope.ReceiptAdd.IsTaxSummary = true; // หักภาษี 7 % รวม
                    $scope.ReceiptAdd.IsDiscountHeaderType = '%'; // ส่วนลดแยกรายการแบบ Percent

                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                    if (type.length > 0)
                        $scope.ReceiptAdd.SelectSale = type[0];

                    $scope.ReceiptAdd.ReceiptCumulativeDetail = [];

                    $scope.document = [];
                    $scope.document.len = 0;
                    $scope.document.total = '0.00';
                    $('#modalinvoice-list').modal('show');

                    $scope.table.binding = 0;
                }
            }
            catch (err) {
                showErrorToast(err);
            }
        });
    };

    $scope.OnClickCancel = function () {
        if (QueryString('ref_info') == 'INV')
            window.location.href = baseURL + "DocumentSell/Invoice";
        else if (QueryString('ref_info') == 'CN')
            window.location.href = baseURL + "DocumentSell/CreditNote";
        else if (QueryString('ref_info') == 'DN')
            window.location.href = baseURL + "DocumentSell/DebitNote";
        else if (QueryString('ref_info') == 'BL')
            window.location.href = baseURL + "DocumentSell/BillingNote";
        else if (QueryString('ref_report') != undefined)
            window.location.href = baseURL + "Report/" + QueryString('ref_report');
        else
            window.location.href = baseURL + "DocumentSell/Receipt";
    };

    $scope.OnDisabled = function () {
        if ($scope.ReceiptAdd.ReceiptStatus == undefined || $scope.ReceiptAdd.ReceiptStatus == '1')
            return false;
        else
            return true;
    };

    $scope.OnClickReportContent = function (type) {
        $scope.Print = "1";
        $scope.document = [];
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $scope.OnClickSave();
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'RE',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {

        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'RE',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };
    /******************Date *********** */
    $scope.OnChangeReceiptDate = function () {
        var prefix = { asofdate: $scope.ReceiptAdd.ReceiptDate, documenttype: headerdoc };
        var qq = $q.all([paramService.getDocumentNo(prefix)]).then(function (data) {
            /****** GetDocument NO ******/
            if (data[0] != undefined && data[0] != "") {
                if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.ReceiptAdd.ReceiptNo = data[0].data.responsedata; }
                else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
            }
        });
    };

    /*********** Header Setting Discount , Tax**************/
    $scope.OnChangeIsTaxHeader = function () {
        if ($scope.ReceiptAdd.IsTaxHeader) {
            $scope.ReceiptAdd.IsDiscountHeader = true;
        }
        $scope.OnChangeDetailCalculate();
    };
    $scope.OnChangeIsDiscountHeader = function () {
        $scope.ReceiptAdd.DiscountPercent = '0';
        $scope.ReceiptAdd.DiscountAmount = '0';
        $scope.OnClickDiscountType($scope.ReceiptAdd.IsDiscountHeaderType);
    };
    /******************* Detail And Calculate *************************/
    $scope.OnClickDocumentDetailDelete = function (categorykey) {
        try {
            var invoice = _.where($scope.ReceiptAdd.ReceiptCumulativeDetail, { DocumentCategoryKey: categorykey });
            _.each(invoice, function (del) {
                var a = $scope.ReceiptAdd.ReceiptCumulativeDetail.indexOf(del);
                $scope.ReceiptAdd.ReceiptCumulativeDetail.splice(a, 1);
            });
            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickAdjustDiscount = function () {
        $scope.ReceiptAdd.AdjustDiscountType = '1';
        $scope.ReceiptAdd.AdjustDiscountAmount = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDetailCalculate = function () {
        try {
            /******* หาค่ารวม Summary ***********/
            var sumtotalafterdiscount = 0, sumvat = 0, sumexemptamount = 0, sumvatableamount = 0, sumgrandamount = 0, sumtotalbeforevatamount = 0, discounthper = 0, adjustafter = 0, paymantamount = 0;

            if ($scope.ReceiptAdd.ReceiptCumulativeDetail != undefined && $scope.ReceiptAdd.ReceiptCumulativeDetail.length > 0) {
                _.each($scope.ReceiptAdd.ReceiptCumulativeDetail, function (item) {

                    totalafterdiscount = item.TotalAfterDiscountAmount == undefined || item.TotalAfterDiscountAmount === '' ? 0 : detectFloat(item.TotalAfterDiscountAmount); // ราคาจากหน้าจอ
                    vat = item.VAT == undefined || item.VAT === '' ? 0 : detectFloat(item.VAT); // ส่วนลดคำนวณ
                    exemptamount = item.ExemptAmount == undefined || item.ExemptAmount === '' ? 0 : detectFloat(item.ExemptAmount); // จำนวน จากหน้าจอ
                    vatableamount = item.VatableAmount == undefined || item.VatableAmount === '' ? 0 : detectFloat(item.VatableAmount); // จำนวน จากหน้าจอ
                    grandamount = item.GrandAmount == undefined || item.GrandAmount === '' ? 0 : detectFloat(item.GrandAmount); // จำนวน จากหน้าจอ
                    totalbeforevatamount = item.TotalBeforeVatAmount == undefined || item.TotalBeforeVatAmount === '' ? 0 : detectFloat(item.TotalBeforeVatAmount); // จำนวน จากหน้าจอ

                    if (item.DocumentType == 'CN') {
                        totalafterdiscount = totalafterdiscount * (-1);
                        vat = vat * (-1);
                        exemptamount = exemptamount * (-1);
                        vatableamount = vatableamount * (-1);
                        grandamount = grandamount * (-1);
                    }

                    // กร๊ไม่มี Vat 7% ของรวม


                    if (item.IsTaxHeader == '1') { // หักภาษี แบบทีละรายการ
                        sumexemptamount = sumexemptamount + exemptamount; // ยกเว้น
                        sumvatableamount = sumvatableamount + vatableamount; //คำนวณภาษี
                        sumvat = sumvat + vat;
                    }
                    else {
                        if (item.IsTaxHeader == '0' && item.IsTaxSummary == '0')
                            sumexemptamount = sumexemptamount + totalafterdiscount; // ยกเว้น
                        else
                            sumvatableamount = sumvatableamount + totalafterdiscount;
                        sumvat = sumvat + vat;
                    }

                    sumtotalbeforevatamount = sumtotalbeforevatamount + totalbeforevatamount;
                    sumtotalafterdiscount = sumtotalafterdiscount + totalafterdiscount;
                    sumgrandamount = sumgrandamount + grandamount;
                });

                if ($scope.ReceiptAdd.IsAdjustDiscount) {
                    discounthper = $scope.ReceiptAdd.AdjustDiscountAmount == undefined || $scope.ReceiptAdd.AdjustDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.AdjustDiscountAmount); // Discount Percenter
                    adjustafter = sumgrandamount - discounthper;
                    paymantamount = adjustafter;
                }

                $scope.ReceiptAdd.TotalQTY = $scope.ReceiptAdd.ReceiptCumulativeDetail.length;
                $scope.ReceiptAdd.TotalAmount = AFormatNumber(sumtotalafterdiscount, 2);
                $scope.ReceiptAdd.DiscountAmount = AFormatNumber(0, 2);
                $scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber(sumtotalafterdiscount, 2);
                $scope.ReceiptAdd.ExemptAmount = AFormatNumber(sumexemptamount, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.ReceiptAdd.VatableAmount = AFormatNumber(sumvatableamount, 2);//มูลค่าที่คำนวณภาษี
                $scope.ReceiptAdd.VAT = AFormatNumber(sumvat, 2);//ภาษีมูลค่าเพิ่ม
                $scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber(totalbeforevatamount, 2);//จำนวนเงินรวมภาษี
                $scope.ReceiptAdd.GrandAmount = AFormatNumber(sumgrandamount, 2);//
                $scope.ReceiptAdd.WithholdingAmount = AFormatNumber(0, 2);//
                $scope.ReceiptAdd.PaymentAmount = AFormatNumber(paymantamount, 2);//
            }
            else {
                $scope.ReceiptAdd.TotalQTY = 0;
                $scope.ReceiptAdd.TotalAmount = AFormatNumber(0, 2);
                $scope.ReceiptAdd.DiscountPercent = AFormatNumber(0, 2);
                $scope.ReceiptAdd.DiscountAmount = AFormatNumber(0, 2);
                $scope.ReceiptAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                $scope.ReceiptAdd.ExemptAmount = AFormatNumber(0, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.ReceiptAdd.VatableAmount = AFormatNumber(0, 2);//มูลค่าที่คำนวณภาษี
                $scope.ReceiptAdd.VAT = AFormatNumber(0, 2);//ภาษีมูลค่าเพิ่ม
                $scope.ReceiptAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);//จำนวนเงินรวมภาษี
                $scope.ReceiptAdd.GrandAmount = AFormatNumber(0, 2);//
                $scope.ReceiptAdd.WithholdingAmount = AFormatNumber(0, 2);//
                $scope.ReceiptAdd.PaymentAmount = AFormatNumber(0, 2);//
            }


        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickSave = function (exit) {
        try {
            if ($scope.ReceiptAdd.IsAdjustDiscount) {
                var adjust = $scope.ReceiptAdd.PaymentAmount == undefined || $scope.ReceiptAdd.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.PaymentAmount); // Discount Percenter
                if (adjust < 0)
                    throw "ยอดชำระติดลบ กรุณาตรวจสอบยอดชำระอีกครั้ง";
            }

            var adjusttype;
            if ($scope.ReceiptAdd.AdjustDiscountType == '1')
                adjusttype = 'ส่วนลดพิเศษ';
            else if ($scope.ReceiptAdd.AdjustDiscountType == '2')
                adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
            else if ($scope.ReceiptAdd.AdjustDiscountType == '3')
                adjusttype = 'ค่าดำเนินการ';
            else if ($scope.ReceiptAdd.AdjustDiscountType == '4')
                adjusttype = 'ปัดเศษ';

            if ($scope.ReceiptAdd.AdjustDiscountAmount <= 0)
                $scope.ReceiptAdd.IsAdjustDiscount = false;

            var data = {
                ReceiptKey: $scope.ReceiptAdd.ReceiptKey,
                ReceiptNo: $scope.ReceiptAdd.ReceiptNo,
                ReceiptStatus: $scope.ReceiptAdd.ReceiptStatus,
                ReceiptStatusName: $scope.ReceiptAdd.ReceiptStatusName,
                ReceiptType: 'CRE',
                ReceiptTypeName: 'ใบเสร็จรวม',
                CustomerKey: $scope.ReceiptAdd.CustomerKey,
                CustomerName: $scope.ReceiptAdd.CustomerName,
                CustomerTaxID: $scope.ReceiptAdd.CustomerTaxID,
                CustomerContactName: $scope.ReceiptAdd.CustomerContactName,
                CustomerContactPhone: $scope.ReceiptAdd.CustomerContactPhone,
                CustomerContactEmail: $scope.ReceiptAdd.CustomerContactEmail,
                CustomerAddress: $scope.ReceiptAdd.CustomerAddress,
                CustomerBranch: $scope.ReceiptAdd.CustomerBranch,
                ReceiptDate: ToJsonDate2($scope.ReceiptAdd.ReceiptDate),
                SaleID: $scope.ReceiptAdd.SelectSale.UID,
                SaleName: $scope.ReceiptAdd.SelectSale.EmployeeName,
                ProjectName: $scope.ReceiptAdd.ProjectName,
                ReferenceNo: $scope.ReceiptAdd.ReferenceNo,
                TaxType: $scope.ReceiptAdd.TaxType,
                TaxTypeName: $scope.ReceiptAdd.TaxType == 'N' ? 'ราคาไม่รวมภาษี' : $scope.ReceiptAdd.TaxType == 'V' ? 'ราคารวมภาษี' : '',
                TotalAmount: ConvertToDecimal($scope.ReceiptAdd.TotalAmount),
                IsDiscountHeader: '0',
                IsDiscountHeaderType: $scope.ReceiptAdd.IsDiscountHeaderType,
                DiscountPercent: $scope.ReceiptAdd.DiscountPercent,
                DiscountAmount: $scope.ReceiptAdd.DiscountAmount,
                TotalAfterDiscountAmount: $scope.ReceiptAdd.TotalAfterDiscountAmount,
                IsTaxHeader: '0',
                IsTaxSummary: '0',
                ExemptAmount: $scope.ReceiptAdd.ExemptAmount,
                VatableAmount: $scope.ReceiptAdd.VatableAmount,
                VAT: $scope.ReceiptAdd.VAT,
                TotalBeforeVatAmount: $scope.ReceiptAdd.TotalBeforeVatAmount,
                IsWithholdingTax: '0',
                WithholdingKey: undefined,
                WithholdingRate: undefined,
                WithholdingAmount: undefined,
                IsAdjustDiscount: $scope.ReceiptAdd.IsAdjustDiscount ? '1' : '0',
                AdjustDiscountType: $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined,
                AdjustDiscountTypeName: $scope.ReceiptAdd.IsAdjustDiscount ? adjusttype : undefined,
                AdjustDiscountAmount: $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : undefined,
                PaymentAmount: $scope.ReceiptAdd.IsWithholdingTax || $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.PaymentAmount : undefined,
                GrandAmount: $scope.ReceiptAdd.GrandAmount,
                Remark: $scope.ReceiptAdd.Remark,
                Signature: $scope.ReceiptAdd.Signature ? '1' : '0',
                Noted: $scope.ReceiptAdd.Noted,
                DocumentKey: $scope.ReceiptAdd.DocumentKey,
                DocumentNo: $scope.ReceiptAdd.DocumentNo,
                DocumentOwner: $scope.ReceiptAdd.DocumentOwner,
                BillingNoteCumulativeKey: $scope.ReceiptAdd.BillingNoteCumulativeKey,
                BillingNoteCumulativeNo: $scope.ReceiptAdd.BillingNoteCumulativeNo
            };
            data.ReceiptCumulativeDetail = [];
            _.each($scope.ReceiptAdd.ReceiptCumulativeDetail, function (item) {

                var tmpQT = {
                    Sequence: parseInt(item.Sequence),
                    DocumentKey: item.DocumentKey,
                    DocumentNo: item.DocumentNo,
                    DocumentType: item.DocumentType,
                    DocumentCategoryKey: item.DocumentCategoryKey,
                    DocumentDate: ToJsonDate2(item.DocumentDate),
                    DueDate: ToJsonDate2(item.DueDate),
                    Unit: item.Unit,
                    TaxType: item.TaxType,
                    TaxTypeName: item.TaxTypeName,
                    TotalAmount: ConvertToDecimal(item.TotalAmount),
                    IsDiscountHeader: item.IsDiscountHeader,
                    IsDiscountHeaderType: item.IsDiscountHeaderType,
                    DiscountPercent: item.DiscountPercent,
                    DiscountAmount: ConvertToDecimal(item.DiscountAmount),
                    TotalAfterDiscountAmount: ConvertToDecimal(item.TotalAfterDiscountAmount),
                    IsTaxHeader: item.IsTaxHeader,
                    IsTaxSummary: item.IsTaxSummary,
                    ExemptAmount: ConvertToDecimal(item.ExemptAmount),
                    VatableAmount: ConvertToDecimal(item.VatableAmount),
                    TotalBeforeVatAmount: ConvertToDecimal(item.TotalBeforeVatAmount),
                    IsWithholdingTax: item.IsWithholdingTax,
                    WithholdingKey: item.WithholdingKey,
                    WithholdingRate: ConvertToDecimal(item.WithholdingRate),
                    WithholdingAmount: ConvertToDecimal(item.WithholdingAmount),
                    GrandAmountAfterWithholding: ConvertToDecimal(item.GrandAmountAfterWithholding),
                    GrandAmount: ConvertToDecimal(item.GrandAmount)
                };


                data.ReceiptCumulativeDetail.push(tmpQT);
            });

            $scope.table.binding = 1;
            $http.post(baseURL + "DocumentSell/PostBusinessReceiptCumulative", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                var responseReceipt = response.data.responsedata;
                                if (responseReceipt != undefined) {

                                    if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'INV' && exit == 1)
                                        window.location.href = baseURL + "DocumentSell/Invoice";
                                    else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'CN' && exit == 1)
                                        window.location.href = baseURL + "DocumentSell/CreditNote";
                                    else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'DN' && exit == 1)
                                        window.location.href = baseURL + "DocumentSell/DebitNote";
                                    else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'BL' && exit == 1)
                                        window.location.href = baseURL + "DocumentSell/BillingNote";
                                    else if (QueryString('ref_key') != undefined && (QueryString('ref_info') == 'INV' || QueryString('ref_info') == 'CN' || QueryString('ref_info') == 'DN' || QueryString('ref_info') == 'BL'))
                                        window.location.href = baseURL + "DocumentSell/ReceiptCumulativeAdd?ReceiptKey=" + responseReceipt.ReceiptKey;
                                    else if (exit == 1)
                                        window.location.href = baseURL + "DocumentSell/Receipt";
                                    else {
                                        $scope.ReceiptAdd.ReceiptKey = responseReceipt.ReceiptKey;
                                        $scope.ReceiptAdd.ReceiptStatus = responseReceipt.ReceiptStatus;
                                        $scope.ReceiptAdd.ReceiptStatusName = responseReceipt.ReceiptStatusName;
                                        $scope.ReceiptAdd.CustomerKey = responseReceipt.CustomerKey;
                                        $scope.ReceiptAdd.DocumentKey = undefined;
                                        $scope.ReceiptAdd.DocumentNo = undefined;
                                        $scope.ReceiptAdd.DocumentOwner = undefined;

                                        $scope.table.binding = 0;
                                        showSuccessToast();

                                        if ($scope.SelectPayment == "1") {
                                            $scope.ReceiptAdd.ReceiptInfo = [];
                                            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt = [];
                                            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push({ ID: '0', Name: 'จำนวนเงิน', Rate: 0 });
                                            _.each($scope.Parameter.Param_Quotation.Param_WithholdingRate, function (item) { $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push(item); });


                                            $scope.ReceiptAdd.ReceiptInfo.DocumentKey = $scope.ReceiptAdd.ReceiptKey;
                                            $scope.ReceiptAdd.ReceiptInfo.DocumentNo = $scope.ReceiptAdd.ReceiptNo;
                                            $scope.ReceiptAdd.ReceiptInfo.DocumentType = headerdoc;
                                            $scope.ReceiptAdd.ReceiptInfo.CustomerName = $scope.ReceiptAdd.CustomerName;
                                            $scope.ReceiptAdd.ReceiptInfo.PaymentAmount = $scope.ReceiptAdd.GrandAmount;
                                            $scope.ReceiptAdd.ReceiptInfo.PaymentDate = GetDatetimeNow();
                                            $('#inputreceivedate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiptInfo.PaymentDate);
                                            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.PaymentAmount : $scope.ReceiptAdd.GrandAmount;
                                            $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax = false;

                                            $scope.ReceiptAdd.ReceiptInfo.IsDiscount = $scope.ReceiptAdd.IsAdjustDiscount;
                                            $scope.ReceiptAdd.ReceiptInfo.DiscountType = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined;
                                            $scope.ReceiptAdd.ReceiptInfo.DiscountTypeName = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountTypeName : undefined;
                                            $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : AFormatNumber(0, 2);
                                            $scope.ReceiptAdd.ReceiptInfo.PaymentType = "CA";
                                            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(0, 2);
                                            $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
                                            $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
                                            $scope.ReceiptAdd.ReceiptInfo.IsPayment = true;
                                            $scope.SelectPayment = undefined;
                                            $('#modalreceipt-receive').modal('show');
                                        }

                                        if ($scope.ReceiptAdd.ReceiptKey != undefined && $scope.Print == "1") {

                                            $scope.document.DocKey = $scope.ReceiptAdd.ReceiptKey;
                                            $scope.document.DocNo = $scope.ReceiptAdd.ReceiptNo;
                                            $scope.document.original = $scope.document.copy = true;
                                            $('#modal-content-report').modal('show');
                                        }
                                        $scope.Print = "0";
                                    }
                                }
                            }
                            else {
                                $scope.table.binding = 0;
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        $scope.table.binding = 0;
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickPaymentAdd = function () {
        $scope.SelectPayment = "1";
        $scope.OnClickSave('');
    };

    $scope.OnChangeIsAdjustReceive = function () {
        $scope.ReceiptAdd.ReceiptInfo.DiscountType = '1';
        $scope.ReceiptAdd.ReceiptInfo.DiscountAmount = AFormatNumber(0, 2);
    };

    $scope.OnChangeIsWithhollding = function () {
        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
        if (type.length > 0)
            $scope.ReceiptAdd.ReceiptInfo.SelectWithholding = type[0];
        $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    };

    $scope.OnChangeWithholldingReceipt = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID == '0') {
            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            $scope.OnChangeReceiveCalculate();
        }
        else {
            $scope.OnChangeReceiveCalculate();
        }
    };

    $scope.OnChangePaymentType = function () {
        if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR') {
            var type = $scope.Parameter.BizBank.filter(function (item) { return item.UID == $scope.Parameter.BizBank[0].UID; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer = type[0];
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
            $scope.ReceiptAdd.ReceiptInfo.ChequeDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiptInfo.ChequeNo = '';
        }
        else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC') {
            $scope.ReceiptAdd.ReceiptInfo.SelectBank = undefined;
        }
    };

    $scope.OnChangeReceiveCalculate = function () {
        try {
            var grandtotal = 0, netpayment = 0, discount = 0, withholding = 0, netgrand = 0, pricediscount = 0, pricevat, balance = 0;
            grandtotal = $scope.ReceiptAdd.ReceiptInfo.PaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.PaymentAmount); // Discount Percenter
            pricediscount = $scope.ReceiptAdd.TotalAfterDiscountAmount == undefined || $scope.ReceiptAdd.TotalAfterDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalAfterDiscountAmount); // Discount Percenter
            pricevat = $scope.ReceiptAdd.TotalBeforeVatAmount == undefined || $scope.ReceiptAdd.TotalBeforeVatAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalBeforeVatAmount); // Discount Percenter
            netpayment = $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount); // Discount Percenter
            discount = $scope.ReceiptAdd.ReceiptInfo.DiscountAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.DiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.DiscountAmount); // Discount Percenter
            withholding = $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount == undefined || $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiptInfo.WithholdingAmount); // Discount Percenter


            if ($scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID; });
                if (type.length > 0)
                    withholdingrate = type[0].Rate;

                if ($scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID == '0')
                    withholding = withholding;
                else {
                    if ($scope.ReceiptAdd.TaxType == 'N')
                        withholding = pricediscount * withholdingrate / 100;
                    else if ($scope.ReceiptAdd.TaxType == 'V')
                        withholding = pricevat * withholdingrate / 100;
                }

                grandtotal = grandtotal - withholding;
            }
            else
                grandtotal = grandtotal;

            grandtotal = grandtotal - discount;

            if (!$scope.ReceiptAdd.ReceiptInfo.IsPayment) {
                balance = netpayment - grandtotal;
            }
            else
                netpayment = grandtotal;

            if (balance < 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินขาด';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Lost';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
                balance = balance * (-1);
            }
            else if (balance > 0) {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiptInfo.IsText = 'เงินเกิน';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Over';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }
            else {
                $scope.ReceiptAdd.ReceiptInfo.IsCauseType = true;
                $scope.ReceiptAdd.ReceiptInfo.IsText = '';
                $scope.ReceiptAdd.ReceiptInfo.BalanceType = 'Equal';
                $scope.ReceiptAdd.ReceiptInfo.CauseType = undefined;
            }

            $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(withholding, 2);//
            $scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber(netpayment, 2);//
            $scope.ReceiptAdd.ReceiptInfo.Balance = AFormatNumber(balance, 2);//
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickReceiptPayment = function () {
        try {

            if (!$scope.ReceiptAdd.ReceiptInfo.IsCauseType && $scope.ReceiptAdd.ReceiptInfo.CauseType == undefined)
                showWariningToast("กรุณาระบุสาเหตุ เงินขาด/เงินเกิน");
            else {
                var adjusttype;
                if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '1')
                    adjusttype = 'ส่วนลดพิเศษ';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '2')
                    adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '3')
                    adjusttype = 'ค่าดำเนินการ';
                else if ($scope.ReceiptAdd.ReceiptInfo.DiscountType == '4')
                    adjusttype = 'ปัดเศษ';

                var paymenttype;
                if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CA')
                    paymenttype = 'เงินสด';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR')
                    paymenttype = 'โอนเงิน';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ')
                    paymenttype = 'เช็ค';
                else if ($scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC')
                    paymenttype = 'บัตรเครดิต';

                var cause;
                if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '1')
                    cause = 'ค่าธรรมเนียม';
                else if ($scope.ReceiptAdd.ReceiptInfo.CauseType == '2')
                    cause = 'เงินขาด/เงินเกิน';

                var data = {
                    DocumentKey: $scope.ReceiptAdd.ReceiptKey,
                    DocumentNo: $scope.ReceiptAdd.ReceiptNo,
                    DocumentType: headerdoc,
                    CustomerName: $scope.ReceiptAdd.ReceiptInfo.CustomerName,
                    PaymentAmount: $scope.ReceiptAdd.ReceiptInfo.PaymentAmount,
                    PaymentDate: ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.PaymentDate),
                    NetPaymentAmount: ConvertToDecimal($scope.ReceiptAdd.ReceiptInfo.NetPaymentAmount),
                    IsWithholdingTax: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.ReceiptAdd.ReceiptInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiptInfo.WithholdingAmount : undefined,
                    IsDiscount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? '1' : '0',
                    DiscountType: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountType : undefined,
                    DiscountTypeName: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? adjusttype : undefined,
                    DiscountAmount: $scope.ReceiptAdd.ReceiptInfo.IsDiscount ? $scope.ReceiptAdd.ReceiptInfo.DiscountAmount : undefined,
                    PaymentType: $scope.ReceiptAdd.ReceiptInfo.PaymentType,
                    PaymentTypeName: paymenttype,
                    BankAccountID: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.UID : undefined,
                    BankCode: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBank.ID : undefined,
                    BankName: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.BankName :
                        $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' || $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CC' ? $scope.ReceiptAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBank.Name : undefined,
                    BankAccountNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.AccountNo : undefined,
                    BankAccountType: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.ReceiptAdd.ReceiptInfo.SelectBankTransfer.DepositTypeName : undefined,
                    ChequeDate: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? ToJsonDate2($scope.ReceiptAdd.ReceiptInfo.ChequeDate) : undefined,
                    ChequeNo: $scope.ReceiptAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiptInfo.ChequeNo : undefined,
                    Comment: $scope.ReceiptAdd.ReceiptInfo.Comment,
                    Balance: $scope.ReceiptAdd.ReceiptInfo.Balance,
                    BalanceType: $scope.ReceiptAdd.ReceiptInfo.BalanceType,
                    CauseType: $scope.ReceiptAdd.ReceiptInfo.CauseType,
                    CauseTypeName: cause
                };

                $http.post(baseURL + "DocumentSell/PostBusinessReceiptPayment", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseReceipt = response.data.responsedata;
                                    if (responseReceipt != undefined) {
                                        window.location.href = baseURL + "DocumentSell/ReceiptCumulativeAdd?ReceiptKey=" + $scope.ReceiptAdd.ReceiptKey + "&succes_redic=1";
                                    }
                                }
                                else showErrorToast(response.data.errormessage);
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) { showErrorToast(err); }
    };

    /**************Select ContactBook **************/
    $scope.OnClickContactPopup = function () {
        $scope.SearchContact.InputFilter = [];
        $scope.contactcurrentPage = 0;
        $scope.contact = [];
        $scope.contact.binding = 0;
        $scope.document = [];
        $scope.contactretpage = [];
        $scope.contactrange();
        $('#modalcontact-list').modal('show');
    };

    $scope.OnClearContact = function () {
        $scope.ReceiptAdd.CustomerKey = $scope.ReceiptAdd.CustomerName = $scope.ReceiptAdd.CustomerTaxID =
            $scope.ReceiptAdd.CustomerContactName = $scope.ReceiptAdd.CustomerContactPhone = $scope.ReceiptAdd.CustomerContactEmail =
            $scope.ReceiptAdd.CustomerAddress = $scope.ReceiptAdd.CustomerBranch = $scope.ReceiptAdd.CreditDay = undefined;
    };

    $scope.OnClickContactSearch = function () {
        $scope.Parameter.Param_Contact = contactService.filterContact($scope.SearchContact.InputFilter, $scope.Parameter.Param_Contact, $scope.Parameter.Param_ContactMain);
        if ($scope.Parameter.Param_Contact == undefined)
            $scope.Parameter.Param_Contact = [];
    };

    $scope.OnClickContactSelection = function (contactkey) {
        try {
            var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: contactkey })[0];
            $('#modalcontact-list').modal('hide');
            $scope.contact.binding = 1;
            $scope.document = [];
            $scope.document.len = 0;
            $scope.document.total = '0.00';
            $scope.document.binding = 1;
            $scope.document.ContactKey = contactkey;
            $scope.document.ContactName = contact.BusinessName;
            $http.get(baseURL + "DocumentSell/GetBusinessInvoiceByContactReceiptCumative?contactkey=" + contactkey + "&Receiptkey=" + $scope.ReceiptAdd.ReceiptKey + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            $scope.Parameter.Param_Document = response.data.responsedata;

                            var grandtotal = 0;
                            _.each($scope.Parameter.Param_Document, function (item) {

                                var setture = _.where($scope.ReceiptAdd.ReceiptCumulativeDetail, { DocumentKey: item.InvoiceKey })[0];
                                if (setture)
                                    item.Mapping = true;
                                else
                                    item.Mapping = false;

                                if (item.CreditType == 'CD')
                                    item.DueDate = formatDate(item.DueDate);
                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                            });

                            $scope.documentcurrentPage = 0;
                            $scope.document.binding = 0;
                            $scope.documentretpage = [];
                            $scope.documentrange();

                        }
                        else if (response.data.responsecode == '400') {
                            $scope.document.binding = 0;
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            $scope.document.binding = 0;
            showErrorToast(err);
        }
    };

    $scope.contactcurrentPage = 0;

    $scope.contactLimitFirst = 0;
    $scope.contactLimitPage = 5;
    $scope.contactitemsPerPage = 10;

    $scope.contactpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Contact.length / $scope.contactitemsPerPage) - 1;
    };

    $scope.contactrange = function () {
        $scope.contactitemsCount = $scope.Parameter.Param_Contact.length;
        $scope.contactpageshow = $scope.contactpageCount() > $scope.contactLimitPage && $scope.contactpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.contactpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.contactpageshowdata = 1;
        $scope.contactretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.contactpcontactPage = function () {
        if ($scope.contactcurrentPage > 0) {
            $scope.contactcurrentPage--;
        }

        if ($scope.contactcurrentPage < $scope.contactLimitFirst && $scope.contactcurrentPage >= 1) {
            $scope.contactLimitFirst = $scope.contactLimitFirst - 5;
            $scope.contactLimitPage = $scope.contactLimitPage - 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }
    };

    $scope.contactpcontactPageDisabled = function () {
        return $scope.contactcurrentPage === 0 ? "disabled" : "";
    };

    $scope.contactnextPage = function () {
        if ($scope.contactcurrentPage < $scope.contactpageCount()) {
            $scope.contactcurrentPage++;
        }

        if ($scope.contactcurrentPage >= $scope.contactLimitPage && $scope.contactcurrentPage <= $scope.contactpageCount()) {
            $scope.contactLimitFirst = $scope.contactLimitFirst + 5;
            $scope.contactLimitPage = $scope.contactLimitPage + 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }

    };

    $scope.contactnextPageDisabled = function () {
        return $scope.contactcurrentPage === $scope.contactpageCount() ? "disabled" : "";
    };

    $scope.contactsetPage = function (n) {
        $scope.contactcurrentPage = n;
    };

    /**************Select Document **************/
    $scope.OnClickcheckedAll = function () {
        try {
            $scope.document.total = '0.00';
            $scope.document.len = '0';
            var i = 0;
            _.each($scope.Parameter.Param_Document, function (item) {
                item.Mapping = $scope.CheckingAll;

                if ($scope.CheckingAll)
                    $scope.OnChangeMapping(i);
                i++;
            });


        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickCloseDocument = function () {
        if ($scope.SelectInvoice == '1')
            window.location.href = baseURL + "DocumentSell/Receipt";
        else
            $('#modalinvoice-list').modal('hide');
    };

    $scope.OnClickDocumentPopup = function () {
        try {
            $('#modalinvoice-list').modal('show');
            $scope.document = [];
            $scope.document.len = 0;
            $scope.document.total = '0.00';
            $scope.document.binding = 1;
            $http.get(baseURL + "DocumentSell/GetBusinessInvoiceByContactReceiptCumative?contactkey=" + $scope.ReceiptAdd.CustomerKey + "&Receiptkey=" + $scope.ReceiptAdd.ReceiptKey + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            $scope.Parameter.Param_Document = response.data.responsedata;
                            var i = 0;
                            var grandtotal = 0;
                            _.each($scope.Parameter.Param_Document, function (item) {

                                var setture = _.where($scope.ReceiptAdd.ReceiptCumulativeDetail, { DocumentKey: item.InvoiceKey, DocumentType: 'INV' })[0];
                                if (setture)
                                    item.Mapping = true;
                                else
                                    item.Mapping = false;

                                if (item.CreditType == 'CD')
                                    item.DueDate = formatDate(item.DueDate);
                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);

                                if (item.Mapping)
                                    $scope.OnChangeMapping(i);
                                i++;
                            });

                            $scope.document.ContactName = $scope.ReceiptAdd.CustomerName;

                            $scope.documentcurrentPage = 0;
                            $scope.document.binding = 0;
                            $scope.documentretpage = [];
                            $scope.documentrange();

                        }
                        else if (response.data.responsecode == '400') {
                            $scope.document.binding = 0;
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            $scope.document.binding = 0;
            showErrorToast(err);
        }
    };

    $scope.OnClickDocumentSelection = function () {
        try {
            var docuno;
            var step = true;
            var values = [];

            _.each($scope.Parameter.Param_Document, function (invoicedoc) {
                if (invoicedoc.Mapping) {
                    var checkdup = _.where($scope.ReceiptAdd.ReceiptCumulativeDetail, { DocumentKey: invoicedoc.InvoiceKey })[0];
                    if (invoicedoc.ReceiptKey != undefined && checkdup == undefined) {
                        docuno = invoicedoc.InvoiceNo;
                        step = false;
                    }
                    else {
                        var dock = { documentkey: invoicedoc.InvoiceKey, documentype: 'INV' };
                        values.push(dock);
                    }
                }
            });

            if (!step)
                showWariningToast(docuno + " เคยถูกสร้างเป็นใบวางบิลรวมหรือใบเสร็จรวมแล้ว");
            else if (values.length > 0) {
                $scope.document.binding = 1;
                $http.post(baseURL + "DocumentSell/GetBusinessCumulative", values, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseDcoument = response.data.responsedata;
                                    if (responseDcoument != undefined) {

                                        var i = 1;
                                        $scope.ReceiptAdd.ReceiptCumulativeDetail = [];
                                        _.each(responseDcoument.Document_Invoice, function (item) {

                                            if (item.CreditType == 'CA')
                                                item.DueDate = GetDatetimeNow();
                                            else if (item.CreditType == 'CN')
                                                item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                            else if (item.CreditType == 'CD')
                                                item.DueDate = formatDate(item.DueDate);

                                            $scope.ReceiptAdd.ReceiptCumulativeDetail.push({
                                                Sequence: i,
                                                DocumentKey: item.InvoiceKey,
                                                DocumentNo: item.InvoiceNo,
                                                DocumentType: 'INV',
                                                DocumentCategoryKey: item.InvoiceKey,
                                                DocumentDate: formatDate(item.InvoiceDate),
                                                DueDate: item.DueDate,
                                                TaxType: item.TaxType,
                                                TaxTypeName: item.TaxTypeName,
                                                TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                IsDiscountHeader: item.IsDiscountHeader,
                                                IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                IsTaxHeader: item.IsTaxHeader,
                                                IsTaxSummary: item.IsTaxSummary,
                                                ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                VAT: AFormatNumber(item.VAT, 2),
                                                TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                IsWithholdingTax: item.IsWithholdingTax,
                                                WithholdingKey: item.WithholdingKey,
                                                WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                            });
                                            i++;
                                        });


                                        _.each(responseDcoument.Document_CreditNote, function (item) {

                                            if (item.CreditType == 'CA')
                                                item.DueDate = GetDatetimeNow();
                                            else if (item.CreditType == 'CN')
                                                item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                            else if (item.CreditType == 'CD')
                                                item.DueDate = formatDate(item.DueDate);

                                            $scope.ReceiptAdd.ReceiptCumulativeDetail.push({
                                                Sequence: i,
                                                DocumentKey: item.CreditNoteKey,
                                                DocumentNo: item.CreditNoteNo,
                                                DocumentType: 'CN',
                                                DocumentCategoryKey: item.InvoiceKey,
                                                DocumentDate: formatDate(item.CreditNoteDate),
                                                DueDate: item.DueDate,
                                                TaxType: item.TaxType,
                                                TaxTypeName: item.TaxTypeName,
                                                TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                IsDiscountHeader: item.IsDiscountHeader,
                                                IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                IsTaxHeader: item.IsTaxHeader,
                                                IsTaxSummary: item.IsTaxSummary,
                                                ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                VAT: AFormatNumber(item.VAT, 2),
                                                TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                IsWithholdingTax: item.IsWithholdingTax,
                                                WithholdingKey: item.WithholdingKey,
                                                WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                            });
                                            i++;
                                        });


                                        _.each(responseDcoument.Document_DebitNote, function (item) {

                                            if (item.CreditType == 'CA')
                                                item.DueDate = GetDatetimeNow();
                                            else if (item.CreditType == 'CN')
                                                item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                            else if (item.CreditType == 'CD')
                                                item.DueDate = formatDate(item.DueDate);


                                            $scope.ReceiptAdd.ReceiptCumulativeDetail.push({
                                                Sequence: i,
                                                DocumentKey: item.DebitNoteKey,
                                                DocumentNo: item.DebitNoteNo,
                                                DocumentType: 'DN',
                                                DocumentCategoryKey: item.InvoiceKey,
                                                DocumentDate: formatDate(item.DebitNoteDate),
                                                DueDate: item.DueDate,
                                                TaxType: item.TaxType,
                                                TaxTypeName: item.TaxTypeName,
                                                TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                IsDiscountHeader: item.IsDiscountHeader,
                                                IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                IsTaxHeader: item.IsTaxHeader,
                                                IsTaxSummary: item.IsTaxSummary,
                                                ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                VAT: AFormatNumber(item.VAT, 2),
                                                TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                IsWithholdingTax: item.IsWithholdingTax,
                                                WithholdingKey: item.WithholdingKey,
                                                WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                            });
                                            i++;
                                        });

                                        $scope.OnChangeDetailCalculate();

                                        if ($scope.SelectInvoice == '1') {

                                            var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: $scope.document.ContactKey })[0];
                                            if (contact != undefined) {
                                                $scope.OnClearContact();
                                                $scope.ReceiptAdd.CustomerKey = contact.ContactKey;
                                                $scope.ReceiptAdd.CustomerName = contact.BusinessName;
                                                $scope.ReceiptAdd.CustomerTaxID = contact.TaxID;
                                                $scope.ReceiptAdd.CustomerContactName = contact.ContactName;
                                                $scope.ReceiptAdd.CustomerContactPhone = contact.ContactMobile;
                                                $scope.ReceiptAdd.CustomerContactEmail = contact.ContactEmail;
                                                $scope.ReceiptAdd.CustomerAddress = contact.Address;
                                                $scope.ReceiptAdd.CustomerBranch = contact.BranchName;
                                            }

                                            $scope.SelectInvoice = undefined;
                                        }

                                        $scope.document.binding = 0;
                                        $('#modalinvoice-list').modal('hide');
                                    }
                                }
                                else {
                                    $scope.table.binding = 0;
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnChangeMapping = function (index) {
        var invoice = $scope.Parameter.Param_Document[index];

        if (invoice != undefined) {
            var len = $scope.document.len == undefined || $scope.document.len === '' ? 0 : detectFloat($scope.document.len); // ราคาจากหน้าจอ 
            var invoicetotal = $scope.document.total == undefined || $scope.document.total === '' ? 0 : detectFloat($scope.document.total); // ราคาจากหน้าจอ 
            var invoiceamount = invoice.GrandAmountText == undefined || invoice.GrandAmountText === '' ? 0 : detectFloat(invoice.GrandAmountText); // ราคาจากหน้าจอ 
            if (invoice.Mapping) {
                invoicetotal = invoicetotal + invoiceamount;
                $scope.document.total = AFormatNumber(invoicetotal, 2);
                len = len + 1;
            }
            else {
                invoicetotal = invoicetotal - invoiceamount;
                $scope.document.total = AFormatNumber(invoicetotal, 2);
                len = len - 1;
            }

            $scope.document.total = AFormatNumber(invoicetotal, 2);
            $scope.document.len = AFormatNumber(len, 0);
        }
    };

    $scope.documentcurrentPage = 0;

    $scope.documentLimitFirst = 0;
    $scope.documentLimitPage = 5;
    $scope.documentitemsPerPage = 10;

    $scope.documentpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Document.length / $scope.documentitemsPerPage) - 1;
    };

    $scope.documentrange = function () {
        $scope.documentitemsCount = $scope.Parameter.Param_Document.length;
        $scope.documentpageshow = $scope.documentpageCount() > $scope.documentLimitPage && $scope.documentpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.documentpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.documentpageshowdata = 1;
        $scope.documentretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.documentpdocumentPage = function () {
        if ($scope.documentcurrentPage > 0) {
            $scope.documentcurrentPage--;
        }

        if ($scope.documentcurrentPage < $scope.documentLimitFirst && $scope.documentcurrentPage >= 1) {
            $scope.documentLimitFirst = $scope.documentLimitFirst - 5;
            $scope.documentLimitPage = $scope.documentLimitPage - 5;
            for (var i = 1; i <= $scope.documentretpage.length; i++) {
                if (i >= $scope.documentLimitFirst && i <= $scope.documentLimitPage) {
                    $scope.documentretpage[i - 1].show = 1;
                }
                else
                    $scope.documentretpage[i - 1].show = 0;
            }
        }
    };

    $scope.documentpdocumentPageDisabled = function () {
        return $scope.documentcurrentPage === 0 ? "disabled" : "";
    };

    $scope.documentnextPage = function () {
        if ($scope.documentcurrentPage < $scope.documentpageCount()) {
            $scope.documentcurrentPage++;
        }

        if ($scope.documentcurrentPage >= $scope.documentLimitPage && $scope.documentcurrentPage <= $scope.documentpageCount()) {
            $scope.documentLimitFirst = $scope.documentLimitFirst + 5;
            $scope.documentLimitPage = $scope.documentLimitPage + 5;
            for (var i = 1; i <= $scope.documentretpage.length; i++) {
                if (i >= $scope.documentLimitFirst && i <= $scope.documentLimitPage) {
                    $scope.documentretpage[i - 1].show = 1;
                }
                else
                    $scope.documentretpage[i - 1].show = 0;
            }
        }

    };

    $scope.documentnextPageDisabled = function () {
        return $scope.documentcurrentPage === $scope.documentpageCount() ? "disabled" : "";
    };

    $scope.documentsetPage = function (n) {
        $scope.documentcurrentPage = n;
    };

    /****************** Document Attach ************************/
    $scope.OnLoadAttach = function () {
        try {
            $http.get(baseURL + "DocumentSell/GetDocumentAttach?documentkey=" + $scope.ReceiptAdd.ReceiptKey + "&documenttype=" + headerdoc + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.DocumentAttach = [];
                            $scope.DocumentAttach = response.data.responsedata;
                        }
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnUploadAttach = function (files) {
        try {
            if ($scope.ReceiptAdd.ReceiptKey == undefined || $scope.ReceiptAdd.ReceiptKey == "")
                throw "กรุณากดปุ่ม บันทึก ก่อนแนบไฟล์เอกสาร";
            else if ($scope.DocumentAttach.length > 3)
                throw "สามารถแนบไฟล์สูงสุดเพียง 3 ไฟล์ เท่านั้น";
            else if (files.length > 0) {
                var data = {
                    DocumentKey: $scope.ReceiptAdd.ReceiptKey,
                    DocumentNo: $scope.ReceiptAdd.ReceiptNo,
                    DocumentType: headerdoc,
                    AttachName: files[0].name,
                    AttachExtension: getFileExtension(files[0].name)
                };
                $http.post(baseURL + "DocumentSell/PostDocumentAttach", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    try {
                                        var attachfile = response.data.responsedata;
                                        var input = document.getElementById("document_attach");
                                        var files = input.files;
                                        var formData = new FormData();

                                        for (var i = 0; i != files.length; i++) {
                                            formData.append("files", files[i]);
                                        }

                                        $.ajax(
                                            {
                                                url: baseURL + "DocumentSell/UploadDocumentAttach?attachkey=" + attachfile.AttachKey,
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                type: "POST",
                                                success: function (data) {

                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();

                                                    $scope.OnLoadAttach();
                                                    showSuccessText('แนบไฟล์สำเร็จ');
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();
                                                    showErrorToast(textStatus);
                                                }
                                            }
                                        );
                                    }
                                    catch (ex) {
                                        var drEvent = $('.dropify').dropify();
                                        drEvent = drEvent.data('dropify');
                                        drEvent.resetPreview();
                                        drEvent.clearElement();
                                        drEvent.destroy();

                                        drEvent.init();
                                        showErrorToast(ex);
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            var drEvent = $('.dropify').dropify();
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.destroy();

            drEvent.init();
            showWariningToast(err);
        }
    };

    $scope.OnClickDeleteAttach = function (attachkey) {
        var data = {
            DocumentKey: $scope.ReceiptAdd.ReceiptKey,
            DocumentNo: $scope.ReceiptAdd.ReceiptNo,
            DocumentType: headerdoc,
            AttachKey: attachkey
        };
        $http.post(baseURL + "DocumentSell/DeleteDocumentAttach", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            try {
                                showDeleteSuccessToast();
                                $scope.OnLoadAttach();
                            }
                            catch (err) {
                                showErrorToast(err);
                            }
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickShowAttach = function (attachkey) {
        var attactfile = _.where($scope.DocumentAttach, { AttachKey: attachkey })[0];
        if (attactfile != undefined) {
            $scope.DocumentAttachView = [];
            $scope.DocumentAttachView = attactfile;

            if (attactfile.AttachExtension == 'pdf') {
                $scope.DocumentAttachView.AttachHeight = '850px';
                PDFObject.embed(baseURL + 'uploads/attach/' + attactfile.AttachPath, "#exampleattach");
            }
            else {
                $scope.DocumentAttachView.AttachHeight = 'auto';
                $scope.DocumentAttachView.AttachShow = baseURL + 'uploads/attach/' + attactfile.AttachPath;
            }
            $('#modal-attach').modal('show');
        }
    };
});


