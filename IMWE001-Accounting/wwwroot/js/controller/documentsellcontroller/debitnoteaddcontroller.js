﻿WEACCTAPP.controller('debitnoteaddcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, contactService, productService, $q, Upload) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'DN';

    $scope.DebitNoteAdd = [];
    $scope.DebitNoteAdd.InvoiceDetail = [];
    $scope.Parameter = [];
    $scope.UserProfile = [];
    $scope.Parameter.Param_Contact = [];
    $scope.Parameter.Param_Document = [];
    $scope.Parameter.Param_Product = [];
    $scope.Parameter.Param_Employee = [];
    $scope.Parameter.Param_Revenue = [];
    $scope.table = [];

    var gCustomer = paramService.getCustomer();
    var gProduct = paramService.getProduct();
    var gEmployee = paramService.getEmployee();
    var gParamQuotation = paramService.getParameterQuotation();
    var gProfile = paramService.getProfile();
    var gRemark = paramService.getDocumentRemark(headerdoc);

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        $scope.table.binding = 1;
        $scope.CheckingAll = false;
        var prefix = { asofdate: GetDatetimeNow(), documenttype: headerdoc };

        var qq = $q.all([paramService.getDocumentNo(prefix), gCustomer, gProduct, gEmployee, gParamQuotation, gProfile, gRemark]).then(function (data) {
            try {
                /****** GetDocument NO ******/
                if (data[0] != undefined && data[0] != "") {
                    if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.DebitNoteAdd.DebitNoteNo = data[0].data.responsedata; }
                    else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                }

                /****** Get Customer ******/
                if (data[1] != undefined && data[1] != "") {
                    if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                        $scope.SearchContact = [];
                        $scope.Parameter.Param_Contact = [];
                        $scope.Parameter.Param_ContactMain = [];
                        $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                    }
                    else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                }

                /****** Get Product ******/
                if (data[2] != undefined && data[2] != "") {
                    if (data[2].data.responsecode == '200' && data[2].data.responsedata != undefined && data[2].data.responsedata != "") {
                        $scope.SearchProduct = [];
                        $scope.Parameter.Param_Product = [];
                        $scope.Parameter.Param_ProductMain = [];

                        $scope.Parameter.Param_ProductMain = data[2].data.responsedata;

                        _.each($scope.Parameter.Param_ProductMain, function (item) {
                            item.SellPrice = AFormatNumber(item.SellPrice, 2);
                            item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                            item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.SellPriceText = item.SellTaxType != '2' ? AFormatNumber(item.SellPrice, 2) : AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                            item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                            item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                        });

                        $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;
                    }
                    else if (data[2].data.responsecode == '400') { showErrorToast(data[2].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[3] != undefined && data[3] != "") {
                    if (data[3].data.responsecode == '200' && data[3].data.responsedata != undefined && data[3].data.responsedata != "") {
                        $scope.Parameter.Param_Employee = [];
                        $scope.Parameter.Param_Employee = data[3].data.responsedata;
                    }
                    else if (data[3].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Parameter DebitNote ******/
                if (data[4] != undefined && data[4] != "") {
                    if (data[4].data.responsecode == '200' && data[4].data.responsedata != undefined && data[4].data.responsedata != "") {
                        $scope.Parameter.Param_Quotation = [];
                        $scope.Parameter.Param_Quotation = data[4].data.responsedata;

                        var substringMatcher = function (strs) {
                            return function findMatches(q, cb) {
                                if (q == '') {
                                    cb(strs);
                                }
                                else {
                                    var matches, substringRegex;

                                    // an array that will be populated with substring matches
                                    matches = [];

                                    // regex used to determine if a string contains the substring `q`
                                    var substrRegex = new RegExp(q, 'i');

                                    // iterate through the pool of strings and for any string that
                                    // contains the substring `q`, add it to the `matches` array
                                    for (var i = 0; i < strs.length; i++) {
                                        if (substrRegex.test(strs[i])) {
                                            matches.push(strs[i]);
                                        }
                                    }

                                    cb(matches);
                                }
                            };
                        };

                        var unittype = [];
                        _.each($scope.Parameter.Param_Quotation.Param_UnitType, function (item) { unittype.push(item.Name); });

                        $('#productunit .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0
                        }, {
                                name: 'unittype',
                                limit: 20,
                                source: substringMatcher(unittype)
                            });


                        var category = [];
                        _.each($scope.Parameter.Param_Quotation.Param_Category, function (item) { category.push(item.Name); });
                        $('#productcategory .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0,
                        }, {
                                name: 'category',
                                limit: 20,
                                source: substringMatcher(category)
                            });
                    }
                    else if (data[4].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[5] != undefined && data[5] != "") {
                    if (data[5].data.responsecode == '200' && data[5].data.responsedata != undefined && data[5].data.responsedata != "") {
                        $scope.UserProfile = [];
                        $scope.UserProfile = data[5].data.responsedata;
                    }
                    else if (data[5].data.responsecode == '400') { showErrorToast(data[5].data.errormessage); }
                }

                /****** Get Remark ******/
                if (data[6] != undefined && data[6] != "") {
                    if (data[6].data.responsecode == '200' && data[6].data.responsedata != undefined && data[6].data.responsedata != "") {
                        $scope.DebitNoteAdd.Remark = data[6].data.responsedata.DocumentText;
                    }
                    else if (data[6].data.responsecode == '400') { showErrorToast(data[6].data.errormessage); }
                }

                if (QueryString('DebitNoteKey') != undefined) {

                    $http.get(baseURL + "DocumentSell/GetBusinessDebitNotebykey?DebitNoteKey=" + QueryString('DebitNoteKey') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.DebitNoteAdd = [];
                                    $scope.DebitNoteAdd = response.data.responsedata;

                                    $scope.DebitNoteAdd.DebitNoteDate = formatDate($scope.DebitNoteAdd.DebitNoteDate);
                                    /*Credit */
                                    $scope.DebitNoteAdd.DueDate = $scope.DebitNoteAdd.DueDate != undefined && $scope.DebitNoteAdd.DueDate != "" ? formatDate($scope.DebitNoteAdd.DueDate) : "";

                                    $('#inputquotationdate-popup').datepicker('setDate', $scope.DebitNoteAdd.DebitNoteDate);
                                    $('#inputdatedue-popup').datepicker('setDate', $scope.DebitNoteAdd.DueDate);

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.DebitNoteAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.DebitNoteAdd.SelectSale = type[0];

                                    $scope.DebitNoteAdd.IsTaxHeader = $scope.DebitNoteAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                    $scope.DebitNoteAdd.IsDiscountHeader = $scope.DebitNoteAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                    $scope.DebitNoteAdd.IsTaxSummary = $scope.DebitNoteAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                    // หักภาษี ณ ที่จ่าย
                                    $scope.DebitNoteAdd.IsWithholdingTax = $scope.DebitNoteAdd.IsWithholdingTax == '1' ? true : false;
                                    if ($scope.DebitNoteAdd.IsWithholdingTax) {

                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.DebitNoteAdd.WithholdingKey; });
                                        if (type.length > 0)
                                            $scope.DebitNoteAdd.SelectWithholding = type[0];

                                        $scope.DebitNoteAdd.WithholdingAmount = AFormatNumber($scope.DebitNoteAdd.WithholdingAmount, 2);
                                        $scope.DebitNoteAdd.GrandAmountAfterWithholding = AFormatNumber($scope.DebitNoteAdd.GrandAmountAfterWithholding, 2);
                                    }
                                    else {
                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                        if (type.length > 0)
                                            $scope.DebitNoteAdd.SelectWithholding = type[0];

                                    }

                                    $scope.DebitNoteAdd.TotalAmount = AFormatNumber($scope.DebitNoteAdd.TotalAmount, 2);
                                    $scope.DebitNoteAdd.DiscountPercent = AFormatNumber($scope.DebitNoteAdd.DiscountPercent, 2);
                                    $scope.DebitNoteAdd.DiscountAmount = AFormatNumber($scope.DebitNoteAdd.DiscountAmount, 2);
                                    $scope.DebitNoteAdd.TotalAfterDiscountAmount = AFormatNumber($scope.DebitNoteAdd.TotalAfterDiscountAmount, 2);
                                    $scope.DebitNoteAdd.ExemptAmount = AFormatNumber($scope.DebitNoteAdd.ExemptAmount, 2);
                                    $scope.DebitNoteAdd.VatableAmount = AFormatNumber($scope.DebitNoteAdd.VatableAmount, 2);
                                    $scope.DebitNoteAdd.VAT = AFormatNumber($scope.DebitNoteAdd.VAT, 2);
                                    $scope.DebitNoteAdd.TotalBeforeVatAmount = AFormatNumber($scope.DebitNoteAdd.TotalBeforeVatAmount, 2);
                                    $scope.DebitNoteAdd.GrandAmount = AFormatNumber($scope.DebitNoteAdd.GrandAmount, 2);
                                    $scope.DebitNoteAdd.DebitPriceBefore = AFormatNumber($scope.DebitNoteAdd.DebitPriceBefore, 2);
                                    $scope.DebitNoteAdd.DebitPriceReal = AFormatNumber($scope.DebitNoteAdd.DebitPriceReal, 2);
                                    $scope.DebitNoteAdd.DebitPriceAfter = AFormatNumber($scope.DebitNoteAdd.DebitPriceAfter, 2);

                                    $scope.DebitNoteAdd.Signature == $scope.DebitNoteAdd.Signature == '1' ? true : false;

                                    _.each($scope.DebitNoteAdd.InvoiceDetail, function (item) {
                                        item.Mapping = false;
                                        item.Quantity = AFormatNumber(item.Quantity, 2);
                                        item.Price = AFormatNumber(item.Price, 2);
                                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.Discount = AFormatNumber(item.Discount, 2);
                                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                        item.VatRate = AFormatNumber(item.VatRate, 2);
                                        item.Vat = AFormatNumber(item.Vat, 2);
                                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                        item.Total = AFormatNumber(item.Total, 2);
                                    });

                                    _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {
                                        var invoice = _.where($scope.DebitNoteAdd.InvoiceDetail, { InvoiceDetailKey: item.InvoiceDetailKey })[0];
                                        if (invoice != undefined)
                                            invoice.Mapping = true;
                                        item.Quantity = AFormatNumber(item.Quantity, 2);
                                        item.Price = AFormatNumber(item.Price, 2);
                                        item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                        item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                        item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.Discount = AFormatNumber(item.Discount, 2);
                                        item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                        item.VatRate = AFormatNumber(item.VatRate, 2);
                                        item.Vat = AFormatNumber(item.Vat, 2);
                                        item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                        item.Total = AFormatNumber(item.Total, 2);
                                    });

                                    $scope.OnLoadAttach();

                                    $scope.OnClickClearDetail();

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else if (QueryString('ref_key') != undefined && QueryString('ref_info') != undefined) {
                    // New document With Invoice
                    $http.get(baseURL + "DocumentSell/GetBusinessDcoumentReference?DocumentKey=" + QueryString('ref_key') + "&DocumentType=" + QueryString('ref_info') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.DebitNoteAdd = [];
                                    $scope.DebitNoteAdd = response.data.responsedata;
                                    $scope.DebitNoteAdd.DebitNoteDetail = [];

                                    /** QuotationValues*/
                                    if (QueryString('ref_info') == 'INV') {
                                        $scope.DebitNoteAdd.DocumentKey = $scope.DebitNoteAdd.InvoiceKey;
                                        $scope.DebitNoteAdd.DocumentNo = $scope.DebitNoteAdd.InvoiceNo;
                                        $scope.DebitNoteAdd.ReferenceNo = $scope.DebitNoteAdd.InvoiceNo;

                                        $scope.DebitNoteAdd.DebitNoteStatus = "1";
                                        $scope.DebitNoteAdd.DebitNoteStatusName = "รอดำเนินการ";

                                        _.each($scope.DebitNoteAdd.InvoiceDetail, function (item) {
                                            item.Mapping = false;
                                            item.Quantity = AFormatNumber(item.Quantity, 2);
                                            item.Price = AFormatNumber(item.Price, 2);
                                            item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                            item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                            item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                            item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                            item.Discount = AFormatNumber(item.Discount, 2);
                                            item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                            item.VatRate = AFormatNumber(item.VatRate, 2);
                                            item.Vat = AFormatNumber(item.Vat, 2);
                                            item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                            item.Total = AFormatNumber(item.Total, 2);
                                        });

                                    }

                                    $scope.DebitNoteAdd.DocumentOwner = QueryString('ref_info');

                                    /**  clear values */
                                    $scope.DebitNoteAdd.DebitNoteNo = data[0].data.responsedata;
                                    $scope.DebitNoteAdd.DebitNoteKey = undefined;

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                    if (type.length > 0)
                                        $scope.DebitNoteAdd.SelectSale = type[0];


                                    $scope.DebitNoteAdd.DebitNoteDate = GetDatetimeNow();

                                    /*Credit */
                                    if ($scope.DebitNoteAdd.CreditType == 'CD')
                                        $scope.OnChangeCredit();

                                    $('#inputquotationdate-popup').datepicker('setDate', $scope.DebitNoteAdd.DebitNoteDate);
                                    $('#inputdatedue-popup').datepicker('setDate', $scope.DebitNoteAdd.DueDate);

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.DebitNoteAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.DebitNoteAdd.SelectSale = type[0];

                                    $scope.DebitNoteAdd.IsTaxHeader = $scope.DebitNoteAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                    $scope.DebitNoteAdd.IsDiscountHeader = $scope.DebitNoteAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                    $scope.DebitNoteAdd.IsTaxSummary = $scope.DebitNoteAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                    // หักภาษี ณ ที่จ่าย
                                    $scope.DebitNoteAdd.IsWithholdingTax = $scope.DebitNoteAdd.IsWithholdingTax == '1' ? true : false;
                                    if ($scope.DebitNoteAdd.IsWithholdingTax) {

                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.DebitNoteAdd.WithholdingKey; });
                                        if (type.length > 0)
                                            $scope.DebitNoteAdd.SelectWithholding = type[0];

                                        $scope.DebitNoteAdd.IsWithholdingTax.WithholdingAmount = AFormatNumber($scope.DebitNoteAdd.IsWithholdingTax.WithholdingAmount, 2);
                                        $scope.DebitNoteAdd.IsWithholdingTax.GrandAmountAfterWithholding = AFormatNumber($scope.DebitNoteAdd.IsWithholdingTax.GrandAmountAfterWithholding, 2);
                                    }
                                    else {
                                        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                        if (type.length > 0)
                                            $scope.DebitNoteAdd.SelectWithholding = type[0];

                                        $scope.DebitNoteAdd.WithholdingAmount = AFormatNumber(0, 2);//
                                    }

                                    $scope.DebitNoteAdd.TotalAmount = AFormatNumber($scope.DebitNoteAdd.TotalAmount, 2);
                                    $scope.DebitNoteAdd.DiscountPercent = AFormatNumber(0, 2);
                                    $scope.DebitNoteAdd.DiscountAmount = AFormatNumber(0, 2);
                                    $scope.DebitNoteAdd.TotalAfterDiscountAmount = AFormatNumber($scope.DebitNoteAdd.TotalAfterDiscountAmount, 2);
                                    $scope.DebitNoteAdd.ExemptAmount = AFormatNumber($scope.DebitNoteAdd.ExemptAmount, 2);
                                    $scope.DebitNoteAdd.VatableAmount = AFormatNumber($scope.DebitNoteAdd.VatableAmount, 2);
                                    $scope.DebitNoteAdd.VAT = AFormatNumber($scope.DebitNoteAdd.VAT, 2);
                                    $scope.DebitNoteAdd.TotalBeforeVatAmount = AFormatNumber($scope.DebitNoteAdd.TotalBeforeVatAmount, 2);
                                    $scope.DebitNoteAdd.GrandAmount = AFormatNumber($scope.DebitNoteAdd.GrandAmount, 2);
                                    $scope.DebitNoteAdd.DebitPriceBefore = AFormatNumber($scope.DebitNoteAdd.TotalAfterDiscountAmount, 2);
                                    $scope.DebitNoteAdd.DebitPriceReal = AFormatNumber(0, 2);
                                    $scope.DebitNoteAdd.DebitPriceAfter = AFormatNumber($scope.DebitNoteAdd.GrandAmount, 2);

                                    $scope.DebitNoteAdd.Signature = $scope.DebitNoteAdd.Signature == '1' ? true : false;

                                    $scope.DebitNoteAdd.DebitNoteDetail = [];

                                    $scope.OnClickClearDetail();
                                    $scope.FirstChooseProduct = '1';
                                    $scope.OnClickProductFromInvoice();
                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else {
                    /******* Initail ********/
                    $scope.DebitNoteAdd.DebitNoteDetail = [];
                    $('#modalinvoice-list').modal('show');

                    $scope.table.binding = 0;
                }
            }
            catch (err) {
                showErrorToast(err);
            }
        });
    };

    $scope.OnClickCancel = function () {
        if (QueryString('ref_info') == 'INV')
            window.location.href = baseURL + "DocumentSell/Invoice";
        else if (QueryString('ref_report') != undefined)
            window.location.href = baseURL + "Report/" + QueryString('ref_report');
        else
            window.location.href = baseURL + "DocumentSell/DebitNote";
    };

    $scope.OnDisabled = function () {
        if ($scope.DebitNoteAdd.DebitNoteStatus == undefined || $scope.DebitNoteAdd.DebitNoteStatus == '1')
            return false;
        else
            return true;
    };

    $scope.OnClickReportContent = function (type) {
        $scope.Print = "1";
        $scope.document = [];
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $scope.OnClickSave();
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'DN',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {

        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'DN',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    /******************Due Date *********** */
    $scope.OnChangeCreditType = function () {
        if ($scope.DebitNoteAdd.CreditType == 'CD') {
            $scope.OnChangeCredit();
        }
        if ($scope.DebitNoteAdd.CreditType == 'CA') { $scope.DebitNoteAdd.CreditDay = 0; }
    };
    $scope.OnChangeCredit = function () {

        if ($scope.DebitNoteAdd.CreditType == 'CD') {
            if ($scope.DebitNoteAdd.CreditDay == undefined || $scope.DebitNoteAdd.CreditDay == '') {
                $scope.DebitNoteAdd.DebitNoteDate = $scope.DebitNoteAdd.DueDate = GetDatetimeNow();
                $scope.DebitNoteAdd.CreditDay = 0;
            }
            else {
                $scope.DebitNoteAdd.DueDate = SetDatetimeDay($scope.DebitNoteAdd.DebitNoteDate, $scope.DebitNoteAdd.CreditDay);
            }
        }
    };
    $scope.OnChangeDateDue = function () {
        if ($scope.DebitNoteAdd.CreditType == 'CD') {
            if ($scope.DebitNoteAdd.DueDate == undefined || $scope.DebitNoteAdd.DueDate == '') {
                $scope.DebitNoteAdd.DebitNoteDate = GetDatetimeNow();
                $scope.DebitNoteAdd.CreditDay = 0;
            }
            else {
                var datedue = $scope.DebitNoteAdd.DueDate;
                var days = DatetimeLenof($scope.DebitNoteAdd.DebitNoteDate, $scope.DebitNoteAdd.DueDate);
                if (days < 0) {
                    $scope.DebitNoteAdd.DebitNoteDate = SetDatetimeDay($scope.DebitNoteAdd.DueDate, days);
                    $scope.DebitNoteAdd.DueDate = datedue;
                    $scope.DebitNoteAdd.CreditDay = (days * (-1));
                }
                else
                    $scope.DebitNoteAdd.CreditDay = days;
            }
        }
    };
    $scope.OnChangeDebitNoteDate = function () {
        if ($scope.DebitNoteAdd.CreditType == 'CD') {
            if ($scope.DebitNoteAdd.DebitNoteDate == undefined || $scope.DebitNoteAdd.DebitNoteDate == '') {
                $scope.DebitNoteAdd.DueDate = $scope.DebitNoteAdd.DebitNoteDate = GetDatetimeNow();
                $scope.DebitNoteAdd.CreditDay = 0;
            }
            else {
                var days = $scope.DebitNoteAdd.CreditDay == undefined || $scope.DebitNoteAdd.CreditDay == "" ? 0 : $scope.DebitNoteAdd.CreditDay;
                $scope.DebitNoteAdd.DueDate = SetDatetimeDay($scope.DebitNoteAdd.DebitNoteDate, $scope.DebitNoteAdd.CreditDay);
            }
        }
        var prefix = { asofdate: $scope.DebitNoteAdd.DebitNoteDate, documenttype: headerdoc };
        var qq = $q.all([paramService.getDocumentNo(prefix)]).then(function (data) {
            /****** GetDocument NO ******/
            if (data[0] != undefined && data[0] != "") {
                if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.DebitNoteAdd.DebitNoteNo = data[0].data.responsedata; }
                else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
            }
        });
    };

    /*********** Header Setting Discount , Tax**************/
    $scope.OnChangeIsTaxHeader = function () {
        if ($scope.DebitNoteAdd.IsTaxHeader) {
            $scope.DebitNoteAdd.IsDiscountHeader = true;
        }
        $scope.OnChangeDetailCalculate();
    };
    $scope.OnChangeIsDiscountHeader = function () {
        $scope.DebitNoteAdd.DiscountPercent = '0';
        $scope.DebitNoteAdd.DiscountAmount = '0';
        $scope.OnClickDiscountType($scope.DebitNoteAdd.IsDiscountHeaderType);
    };
    /******************* Detail And Calculate *************************/
    $scope.OnClickProductFromInvoice = function () {
        try {
            _.each($scope.DebitNoteAdd.InvoiceDetail, function (item) {
                item.Quantity = AFormatNumber(item.Quantity, 2);
                item.Price = AFormatNumber(item.Price, 2);
            });
            $('#modalinvoice-product').modal('show');
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickcheckedAll = function () {
        try {
            _.each($scope.DebitNoteAdd.InvoiceDetail, function (item) {
                item.Mapping = $scope.CheckingAll;
            });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickTableTR = function (index) {
        if ($scope.DebitNoteAdd.InvoiceDetail[index].Mapping)
            $scope.DebitNoteAdd.InvoiceDetail[index].Mapping = false;
        else
            $scope.DebitNoteAdd.InvoiceDetail[index].Mapping = true;
    };

    $scope.OnClickProductSelectInvoice = function () {
        try {

            _.each($scope.DebitNoteAdd.InvoiceDetail, function (item) {
                if (item.Mapping) {
                    var insert = _.where($scope.DebitNoteAdd.DebitNoteDetail, { InvoiceDetailKey: item.InvoiceDetailKey })[0];

                    if (insert == null) {
                        item.Sequence = i;
                        var values = angular.copy(item);
                        $scope.DebitNoteAdd.DebitNoteDetail.push(values);
                    }
                }
                else {
                    var del = _.where($scope.DebitNoteAdd.DebitNoteDetail, { InvoiceDetailKey: item.InvoiceDetailKey })[0];
                    if (del != null) {
                        var a = $scope.DebitNoteAdd.DebitNoteDetail.indexOf(del);
                        $scope.DebitNoteAdd.DebitNoteDetail.splice(a, 1);
                    }

                }
            });

            var i = 1;
            _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {
                item.Sequence = i;
                i++;
            });

            $scope.DebitNoteAdd.DebitNoteDetail = _.sortBy($scope.DebitNoteAdd.DebitNoteDetail, 'Sequence');
            $scope.OnChangeDetailCalculate();
            $scope.FirstChooseProduct = '0';
            $('#modalinvoice-product').modal('hide');
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickProductCancelInvoice = function () {
        if ($scope.FirstChooseProduct == '1')
            window.location.href = baseURL + "DocumentSell/Invoice";
        else if ($scope.FirstChooseProduct == '2')
            window.location.href = baseURL + "DocumentSell/DebitNote";
        else
            $('#modalinvoice-product').modal('hide');
    };

    $scope.OnClickProductDetailAdd = function () {
        try {
            if ($scope.ProductDetailAdd.Name == undefined || $scope.ProductDetailAdd.Name == "")
                throw "กรุณากรอกชื่อสินค้า";
            else if ($scope.ProductDetailAdd.Price == undefined || $scope.ProductDetailAdd.Price == "")
                throw "กรุณากรอกราคาต่อหน่วย";
            else if ($scope.ProductDetailAdd.Quantity == undefined || $scope.ProductDetailAdd.Quantity == "")
                throw "กรุณากรอกจำนวน";
            else {
                $scope.ProductDetailAdd.Sequence = $scope.DebitNoteAdd.DebitNoteDetail.length + 1;
                $scope.ProductDetailAdd.DiscountType = $scope.DebitNoteAdd.IsDiscountHeaderType;

                if ($scope.ProductDetailAdd.ProductKey == undefined) {
                    $scope.ProductDetailAdd.DiscountType = $scope.DebitNoteAdd.IsDiscountHeaderType;
                    $scope.ProductDetailAdd.DiscountPercent = '0.00';
                    $scope.ProductDetailAdd.Discount = '0.00';
                    $scope.ProductDetailAdd.VatType = '2';

                    //var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                    var sell = 0, selltax = 0, selllast = 0;
                    sell = ConvertToDecimal($scope.ProductDetailAdd.Price == undefined ? 0 : $scope.ProductDetailAdd.Price);
                    //selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                    //selllast = sell + selltax;

                    $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.PriceTax = AFormatNumber(0, 2);
                }

                if ($scope.DebitNoteAdd.TaxType == 'N')
                    $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
                else if ($scope.DebitNoteAdd.TaxType == 'V')
                    $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
                $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            }

            $scope.DebitNoteAdd.DebitNoteDetail.push($scope.ProductDetailAdd);
            $scope.OnClickClearDetail();
            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickProductDetailDelete = function (index) {
        try {

            var invoice = _.where($scope.DebitNoteAdd.InvoiceDetail, { InvoiceDetailKey: $scope.DebitNoteAdd.DebitNoteDetail[index].InvoiceDetailKey })[0];
            if (invoice != undefined) {
                invoice.Mapping = false;
            }

            $scope.DebitNoteAdd.DebitNoteDetail.splice(index, 1);

            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickClearDetail = function () {
        $scope.ProductDetailAdd = [];
        $scope.ProductDetailAdd.DiscountType = $scope.DebitNoteAdd.IsDiscountHeaderType;
        $scope.ProductDetailAdd.VatType = '2';
    };

    $scope.OnClickDiscountType = function (val) {
        $scope.DebitNoteAdd.IsDiscountHeaderType = val;
        _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {
            item.DiscountType = val;
            item.DiscountPercent = item.Discount = AFormatNumber(0, 2);
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountAmount = function () {
        $scope.DebitNoteAdd.DiscountPercent = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickNoTaxSummary = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnClickWithholdingTax = function () {
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeProductTaxType = function () {
        _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {
            if ($scope.DebitNoteAdd.TaxType == 'N')
                item.Price = item.PriceBeforeTax;
            else if ($scope.DebitNoteAdd.TaxType == 'V')
                item.Price = item.PriceAfterTax;
        });
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDiscountPercent = function () {
        if ($scope.DebitNoteAdd.DiscountPercent == undefined || $scope.DebitNoteAdd.DiscountPercent === '' || $scope.DebitNoteAdd.DiscountPercent == 0)
            $scope.DebitNoteAdd.DiscountAmount = AFormatNumber(0, 2);
        $scope.OnChangeDetailCalculate();
    };

    $scope.OnChangeDetailCalculate = function () {
        try {
            _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {
                var amt, qty, total, vatrate, vatamt, discountper, discountminus, discount, discountafter, vatafter = 0, totalamount, vatcal = 0;
                // Get Values
                amt = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                vat = item.VatType == '1' ? 0 : item.VatType == '2' ? 7 : 0; // dropdown vat หน้าจอ
                discountper = item.DiscountPercent == undefined || item.DiscountPercent === '' ? '0' : detectFloat(item.DiscountPercent); //ส่วนลด % จากหน้าจอ
                discount = item.Discount == undefined || item.Discount === '' ? '0' : detectFloat(item.Discount); //ส่วนลดจากหน้าจอ
                discountminus = vatafter = 0; // ราคาส่วนลด
                discountafter = 0; // หลังหักส่วนลด
                vatafter = 0;

                total = amt * qty;
                // Calculate
                if ($scope.DebitNoteAdd.IsDiscountHeader == true) { //เงื่อนไขหักส่วนลด
                    if (item.DiscountType == '%') {
                        discountminus = total * discountper / 100;
                        discountafter = detectFloat(AFormatNumber(total - discountminus, 2));
                    }
                    else if (item.DiscountType == '฿') {
                        discountminus = discount;
                        discountafter = total - discount;
                    }
                }
                else {
                    discountafter = total;
                    discountper = discount = 0;
                }

                if ($scope.DebitNoteAdd.IsTaxHeader == true) { //คำนวณหาภาษี ของแต่ละรายการ
                    if (item.VatType == '2') {
                        if ($scope.DebitNoteAdd.TaxType == 'N') {

                            vatcal = vat > 0 ? ((discountafter * (100 + vat) / 100)) : 0;
                            vatamt = detectFloat(AFormatNumber(vatcal, 2)) - discountafter;
                            vatafter = discountafter + vatamt;
                        }
                        else if ($scope.DebitNoteAdd.TaxType == 'V') {
                            vatcal = vat > 0 ? ((discountafter / (100 + vat) * 100)) : 0;
                            vatamt = discountafter - detectFloat(AFormatNumber(vatcal, 2));
                            vatafter = discountafter - vatamt;
                        }
                    }
                    else
                        vatamt = 0;
                }
                else {
                    item.VatType = '2';
                    vatamt = 0;
                }

                totalamount = discountafter;

                // Set Value
                item.Discount = AFormatNumber(discountminus, 2);
                item.DiscountAfter = AFormatNumber(discountafter, 2);
                item.VatRate = AFormatNumber(vat, 2);
                item.Vat = AFormatNumber(vatamt, 2);
                item.VatAfter = AFormatNumber(vatafter, 2);
                item.Total = AFormatNumber(totalamount, 2);
            });

            /******* หาค่ารวม Summary ***********/
            var price = 0, priceedis = 0, pricevat = 0, qty = 0, totalprice = 0, total = 0, discounthper = 0, discounthamt = 0, priceafterdiscount = 0, discounth = 0, grandtotal = 0, discountperitem = 0
                , exemptamt = 0, vatableamt = 0, vat = 0, withholdingrate = 0, withholdingprice = 0, withholdingafter = 0, vatafter = 0, creditprice = 0, creditafter = 0, creditbefore = 0;


            discounthper = $scope.DebitNoteAdd.DiscountPercent == undefined || $scope.DebitNoteAdd.DiscountPercent === '' ? 0 : detectFloat($scope.DebitNoteAdd.DiscountPercent); // Discount Percenter
            discounthamt = $scope.DebitNoteAdd.DiscountAmount == undefined || $scope.DebitNoteAdd.DiscountAmount === '' ? 0 : $scope.DebitNoteAdd.IsTaxHeader == false ? detectFloat($scope.DebitNoteAdd.DiscountAmount) : 0; // Discount Amount

            if ($scope.DebitNoteAdd.DebitNoteDetail != undefined && $scope.DebitNoteAdd.DebitNoteDetail.length > 0) {
                _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {

                    price = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price); // ราคาจากหน้าจอ
                    priceedis = item.Discount == undefined || item.Discount === '' ? 0 : detectFloat(item.Discount); // ส่วนลดคำนวณ
                    qty = item.Quantity == undefined || item.Quantity === '' ? 0 : detectFloat(item.Quantity); // จำนวน จากหน้าจอ
                    pricevat = item.Vat == undefined || item.Vat === '' ? 0 : detectFloat(item.Vat); // จำนวน จากหน้าจอ

                    totalprice = totalprice + price * qty;//รวมเป็นเงิน
                    discounth = discounth + priceedis; //ส่วนลดรวม
                    discountperitem = discountperitem + (price * qty - priceedis);
                    priceqty = price * qty;

                    if ($scope.DebitNoteAdd.IsTaxHeader) { // หักภาษี แบบทีละรายการ
                        if (item.VatType == '1' || item.VatType == '3') //0% ยกเว้น
                            exemptamt = exemptamt + priceqty - priceedis;
                        else if (item.VatType == '2') // 7%
                            vatableamt = (vatableamt + priceqty - priceedis) - ($scope.DebitNoteAdd.TaxType == 'V' ? pricevat : 0); //ถ้าเลือกรวมภาษีแล้ว ให้หัก ภาษีออก

                        vat = vat + pricevat;
                    }
                });

                /**** หักแบบ Summary ***/
                if ($scope.DebitNoteAdd.IsDiscountHeader == false) {
                    if (discounthper > 0)
                        discounthamt = totalprice * discounthper / 100;
                    discounth = discounthamt;
                }

                priceafterdiscount = totalprice - discounth; //รวมเป็นเงิน - หักส่วนลด = ราคาหลังหักส่วนลด

                /*** หักภาษีแบบ Summary **/
                if (!$scope.DebitNoteAdd.IsTaxHeader && $scope.DebitNoteAdd.IsTaxSummary) {
                    if ($scope.DebitNoteAdd.TaxType == 'N') {
                        vatcal = (priceafterdiscount * (100 + 7) / 100);
                        vatamt = vatcal - priceafterdiscount;
                        vatafter = priceafterdiscount + vatamt;
                    }
                    else if ($scope.DebitNoteAdd.TaxType == 'V') {
                        vatcal = (priceafterdiscount / (100 + 7) * 100);
                        vatamt = priceafterdiscount - vatcal;
                        vatafter = priceafterdiscount - vatamt;
                    }
                    vat = vatamt;
                }

                grandtotal = totalprice - discounth + ($scope.DebitNoteAdd.TaxType == 'N' ? vat : 0); //จำนวนเงินรวมทั้งสิ้น

                if ($scope.DebitNoteAdd.IsWithholdingTax && !$scope.DebitNoteAdd.IsTaxHeader) {
                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.DebitNoteAdd.SelectWithholding.ID; });
                    if (type.length > 0)
                        withholdingrate = type[0].Rate;

                    if ($scope.DebitNoteAdd.TaxType == 'N')
                        withholdingprice = priceafterdiscount * withholdingrate / 100;
                    else if ($scope.DebitNoteAdd.TaxType == 'V')
                        withholdingprice = vatafter * withholdingrate / 100;

                    withholdingafter = $scope.DebitNoteAdd.IsTaxSummary ? (grandtotal - withholdingprice) : (priceafterdiscount - withholdingprice);
                }

                creditbefore = $scope.DebitNoteAdd.DebitPriceBefore == undefined || $scope.DebitNoteAdd.DebitPriceBefore === '' ? 0 : detectFloat($scope.DebitNoteAdd.DebitPriceBefore);
                creditprice = creditbefore + discountperitem;
                creditbefore = creditprice- creditbefore ;

                $scope.DebitNoteAdd.TotalAmount = AFormatNumber(totalprice, 2);
                $scope.DebitNoteAdd.DiscountAmount = AFormatNumber(discounth, 2);
                $scope.DebitNoteAdd.TotalAfterDiscountAmount = AFormatNumber(priceafterdiscount, 2);
                $scope.DebitNoteAdd.ExemptAmount = AFormatNumber(exemptamt, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.DebitNoteAdd.VatableAmount = AFormatNumber(vatableamt, 2);//มูลค่าที่คำนวณภาษี
                $scope.DebitNoteAdd.VAT = AFormatNumber(vat, 2);//ภาษีมูลค่าเพิ่ม
                $scope.DebitNoteAdd.TotalBeforeVatAmount = AFormatNumber(vatafter, 2);//จำนวนเงินรวมภาษี
                $scope.DebitNoteAdd.GrandAmount = AFormatNumber(grandtotal, 2);//
                $scope.DebitNoteAdd.WithholdingAmount = AFormatNumber(withholdingprice, 2);//
                $scope.DebitNoteAdd.GrandAmountAfterWithholding = AFormatNumber(withholdingafter, 2);//
                $scope.DebitNoteAdd.DebitPriceReal = AFormatNumber(creditprice, 2);
                $scope.DebitNoteAdd.DebitPriceAfter = AFormatNumber(creditbefore, 2);
            }
            else {
                $scope.DebitNoteAdd.TotalAmount = AFormatNumber(0, 2);
                $scope.DebitNoteAdd.DiscountPercent = AFormatNumber(0, 2);
                $scope.DebitNoteAdd.DiscountAmount = AFormatNumber(0, 2);
                $scope.DebitNoteAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                $scope.DebitNoteAdd.ExemptAmount = AFormatNumber(0, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.DebitNoteAdd.VatableAmount = AFormatNumber(0, 2);//มูลค่าที่คำนวณภาษี
                $scope.DebitNoteAdd.VAT = AFormatNumber(0, 2);//ภาษีมูลค่าเพิ่ม
                $scope.DebitNoteAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);//จำนวนเงินรวมภาษี
                $scope.DebitNoteAdd.GrandAmount = AFormatNumber(0, 2);//
                $scope.DebitNoteAdd.WithholdingAmount = AFormatNumber(0, 2);//
                $scope.DebitNoteAdd.GrandAmountAfterWithholding = AFormatNumber(0, 2);//
                $scope.DebitNoteAdd.DebitPriceReal = AFormatNumber(0, 2);
                $scope.DebitNoteAdd.DebitPriceAfter = AFormatNumber(0, 2);
            }


        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickSave = function (exit) {
        try {
            var creditreal = 0;
            creditreal = $scope.DebitNoteAdd.CreditPriceReal == undefined || $scope.DebitNoteAdd.CreditPriceReal === '' ? 0 : detectFloat($scope.DebitNoteAdd.CreditPriceReal);

            if ($scope.DebitNoteAdd.CauseType == undefined)
                throw "กรุณาระบุเหตุผลในการออกเอกสาร";
            if (($scope.DebitNoteAdd.CauseType == '3' || $scope.DebitNoteAdd.CauseType == '6') && ($scope.DebitNoteAdd.CauseTypeOther == undefined || $scope.DebitNoteAdd.CauseTypeOther == ''))
                throw "กรุณาระบุสาเหตุเพิ่มเติม";
            else if (creditreal < 0)
                throw "ยอดมูลค่าที่ถูกต้องไม่สามารถติดลบได้ กรุณาตรวจสอบยอดมูลค่าที่ถูกต้องอีกครั้ง";
            else {
                var check = '';
                _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {
                    var invoice = _.where($scope.DebitNoteAdd.InvoiceDetail, { InvoiceDetailKey: item.InvoiceDetailKey })[0];
                    if (invoice != undefined) {
                        var before = item.Price == undefined || item.Price === '' ? 0 : detectFloat(item.Price);
                        var after = detectFloat(invoice.Price);
                        if (before > after)
                            check = 'ราคารวมแต่ละรายการต้องมีมูลค่าน้อยกว่าหรือเท่ากับราคารวมแต่ละรายการในเอกสารอ้างอิง <br> -' + item.Name;
                    }
                });

                if (check != '')
                    throw check;

                var credittype;
                if ($scope.DebitNoteAdd.CreditType == 'CD')
                    credittype = 'เครดิต(วัน)';
                else if ($scope.DebitNoteAdd.CreditType == 'CA')
                    credittype = 'เงินสด';
                else if ($scope.DebitNoteAdd.CreditType == 'CN')
                    credittype = 'เครดิต (ไม่แสดงวันที่)';

                var causetype;
                if ($scope.DebitNoteAdd.CauseType == '1')
                    causetype = 'มีการเพิ่มราคาค่าสินค้า (สินค้าเกินกว่าจำนวนที่ตกลงกัน)';
                else if ($scope.DebitNoteAdd.CauseType == '2')
                    causetype = 'คำนวณราคาสินค้า ผิดพลาดต่ำกว่าที่เป็นจริง';
                else if ($scope.DebitNoteAdd.CauseType == '3')
                    causetype = 'เหตุอื่น สำหรับการขายสินค้า (ระบุสาเหตุ)';
                else if ($scope.DebitNoteAdd.CauseType == '4')
                    causetype = 'การเพิ่มราคาค่าบริการ (บริการเกินกว่าข้อกำหนดที่ตกลงกัน)';
                else if ($scope.DebitNoteAdd.CauseType == '5')
                    causetype = 'คำนวณราคาค่าบริการ ผิดพลาดต่ำกว่าที่เป็นจริง';
                else if ($scope.DebitNoteAdd.CauseType == '6')
                    causetype = 'เหตุอื่น สำหรับการขายบริการ (ระบุสาเหตุ)';

                var data = {
                    DebitNoteKey: $scope.DebitNoteAdd.DebitNoteKey,
                    DebitNoteNo: $scope.DebitNoteAdd.DebitNoteNo,
                    DebitNoteStatus: $scope.DebitNoteAdd.DebitNoteStatus,
                    DebitNoteStatusName: $scope.DebitNoteAdd.DebitNoteStatusName,
                    InvoiceKey: $scope.DebitNoteAdd.InvoiceKey,
                    InvoiceNo: $scope.DebitNoteAdd.InvoiceNo,
                    CustomerKey: $scope.DebitNoteAdd.CustomerKey,
                    CustomerName: $scope.DebitNoteAdd.CustomerName,
                    CustomerTaxID: $scope.DebitNoteAdd.CustomerTaxID,
                    CustomerContactName: $scope.DebitNoteAdd.CustomerContactName,
                    CustomerContactPhone: $scope.DebitNoteAdd.CustomerContactPhone,
                    CustomerContactEmail: $scope.DebitNoteAdd.CustomerContactEmail,
                    CustomerAddress: $scope.DebitNoteAdd.CustomerAddress,
                    CustomerBranch: $scope.DebitNoteAdd.CustomerBranch,
                    DebitNoteDate: ToJsonDate2($scope.DebitNoteAdd.DebitNoteDate),
                    CreditType: $scope.DebitNoteAdd.CreditType,
                    CreditTypeName: credittype,
                    CreditDay: $scope.DebitNoteAdd.CreditDay,
                    DueDate: $scope.DebitNoteAdd.CreditType == 'CD' ? ToJsonDate2($scope.DebitNoteAdd.DueDate) : null,
                    SaleID: $scope.DebitNoteAdd.SelectSale.UID,
                    SaleName: $scope.DebitNoteAdd.SelectSale.EmployeeName,
                    ProjectName: $scope.DebitNoteAdd.ProjectName,
                    ReferenceNo: $scope.DebitNoteAdd.ReferenceNo,
                    CauseType: $scope.DebitNoteAdd.CauseType,
                    CauseTypeName: causetype,
                    CauseTypeOther: $scope.DebitNoteAdd.CauseTypeOther,
                    TaxType: $scope.DebitNoteAdd.TaxType,
                    TaxTypeName: $scope.DebitNoteAdd.TaxType == 'N' ? 'ราคาไม่รวมภาษี' : $scope.DebitNoteAdd.TaxType == 'V' ? 'ราคารวมภาษี' : '',
                    TotalAmount: ConvertToDecimal($scope.DebitNoteAdd.TotalAmount),
                    IsDiscountHeader: $scope.DebitNoteAdd.IsDiscountHeader ? '1' : '0',
                    IsDiscountHeaderType: $scope.DebitNoteAdd.IsDiscountHeaderType,
                    DiscountPercent: $scope.DebitNoteAdd.DiscountPercent,
                    DiscountAmount: $scope.DebitNoteAdd.DiscountAmount,
                    TotalAfterDiscountAmount: $scope.DebitNoteAdd.TotalAfterDiscountAmount,
                    IsTaxHeader: $scope.DebitNoteAdd.IsTaxHeader ? '1' : '0',
                    IsTaxSummary: $scope.DebitNoteAdd.IsTaxSummary ? '1' : '0',
                    ExemptAmount: $scope.DebitNoteAdd.ExemptAmount,
                    VatableAmount: $scope.DebitNoteAdd.VatableAmount,
                    VAT: $scope.DebitNoteAdd.VAT,
                    TotalBeforeVatAmount: $scope.DebitNoteAdd.TotalBeforeVatAmount,
                    IsWithholdingTax: $scope.DebitNoteAdd.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.DebitNoteAdd.IsWithholdingTax ? $scope.DebitNoteAdd.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.DebitNoteAdd.IsWithholdingTax ? $scope.DebitNoteAdd.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.DebitNoteAdd.IsWithholdingTax ? $scope.DebitNoteAdd.WithholdingAmount : undefined,
                    GrandAmountAfterWithholding: $scope.DebitNoteAdd.IsWithholdingTax ? $scope.DebitNoteAdd.GrandAmountAfterWithholding : undefined,
                    GrandAmount: $scope.DebitNoteAdd.GrandAmount,
                    DebitPriceBefore: $scope.DebitNoteAdd.DebitPriceBefore,
                    DebitPriceReal: $scope.DebitNoteAdd.DebitPriceReal,
                    DebitPriceAfter: $scope.DebitNoteAdd.DebitPriceAfter,
                    Remark: $scope.DebitNoteAdd.Remark,
                    Signature: $scope.DebitNoteAdd.Signature ? '1' : '0',
                    Noted: $scope.DebitNoteAdd.Noted,
                    DocumentKey: $scope.DebitNoteAdd.DocumentKey,
                    DocumentNo: $scope.DebitNoteAdd.DocumentNo,
                    DocumentOwner: $scope.DebitNoteAdd.DocumentOwner
                };

                data.DebitNoteDetail = [];
                _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {

                    var tmpQT = {
                        InvoiceDetailKey: item.InvoiceDetailKey,
                        Discount: ConvertToDecimal(item.Discount),
                        DiscountAfter: ConvertToDecimal(item.DiscountAfter),
                        DiscountPercent: ConvertToDecimal(item.DiscountPercent),
                        DiscountType: item.DiscountType,
                        Name: item.Name,
                        Unit: item.Unit,
                        Description: item.Description,
                        Price: ConvertToDecimal(item.Price),
                        PriceAfterTax: ConvertToDecimal(item.PriceAfterTax),
                        PriceBeforeTax: ConvertToDecimal(item.PriceBeforeTax),
                        PriceTax: ConvertToDecimal(item.PriceTax),
                        ProductKey: parseInt(item.ProductKey),
                        Quantity: ConvertToDecimal(item.Quantity),
                        Sequence: parseInt(item.Sequence),
                        Total: ConvertToDecimal(item.Total),
                        Vat: ConvertToDecimal(item.Vat),
                        VatAfter: ConvertToDecimal(item.VatAfter),
                        VatRate: ConvertToDecimal(item.VatRate),
                        VatType: item.VatType
                    };


                    data.DebitNoteDetail.push(tmpQT);
                });

                $scope.table.binding = 1;
                $http.post(baseURL + "DocumentSell/PostBusinessDebitNote", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseDebitNote = response.data.responsedata;
                                    if (responseDebitNote != undefined) {

                                        if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'INV' && exit == 1)
                                            window.location.href = baseURL + "DocumentSell/Invoice";
                                        else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'INV')
                                            window.location.href = baseURL + "DocumentSell/DebitNoteAdd?DebitNoteKey=" + responseDebitNote.DebitNoteKey;
                                        else if (exit == 1)
                                            window.location.href = baseURL + "DocumentSell/DebitNote";
                                        else {
                                            $scope.DebitNoteAdd.DebitNoteKey = responseDebitNote.DebitNoteKey;
                                            $scope.DebitNoteAdd.DebitNoteStatus = responseDebitNote.DebitNoteStatus;
                                            $scope.DebitNoteAdd.DebitNoteStatusName = responseDebitNote.DebitNoteStatusName;
                                            $scope.DebitNoteAdd.CustomerKey = responseDebitNote.CustomerKey;
                                            $scope.DebitNoteAdd.DocumentKey = undefined;
                                            $scope.DebitNoteAdd.DocumentNo = undefined;
                                            $scope.DebitNoteAdd.DocumentOwner = undefined;

                                            _.each($scope.DebitNoteAdd.DebitNoteDetail, function (item) {
                                                var detail = _.where(responseDebitNote.DebitNoteDetail, { Sequence: item.Sequence })[0];
                                                if (detail != null) {
                                                    item.ProductKey = detail.ProductKey;
                                                    item.DebitNoteKey = detail.DebitNoteKey;
                                                    item.DebitNoteDetailKey = detail.DebitNoteDetailKey;
                                                }
                                            });

                                            $scope.table.binding = 0;
                                            showSuccessToast();

                                            if ($scope.DebitNoteAdd.DebitNoteKey != undefined && $scope.Print == "1") {

                                                $scope.document.DocKey = $scope.DebitNoteAdd.DebitNoteKey;
                                                $scope.document.DocNo = $scope.DebitNoteAdd.DebitNoteNo;
                                                $scope.document.original = $scope.document.copy = true;
                                                $('#modal-content-report').modal('show');
                                            }
                                            $scope.Print = "0";
                                        }
                                    }
                                }
                                else {
                                    $scope.table.binding = 0;
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    /**************Select ContactBook **************/
    $scope.OnClickContactPopup = function () {
        $scope.SearchContact.InputFilter = [];
        $scope.contactcurrentPage = 0;
        $scope.contact = [];
        $scope.contact.binding = 0;
        $scope.document = [];
        $scope.contactretpage = [];
        $scope.contactrange();
        $('#modalcontact-list').modal('show');
    };

    $scope.OnClickContactSearch = function () {
        $scope.Parameter.Param_Contact = contactService.filterContact($scope.SearchContact.InputFilter, $scope.Parameter.Param_Contact, $scope.Parameter.Param_ContactMain);
        if ($scope.Parameter.Param_Contact == undefined)
            $scope.Parameter.Param_Contact = [];
    };

    $scope.OnClickContactSelection = function (contactkey) {
        try {
            var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: contactkey })[0];
            $scope.document.binding = 1;
            $scope.contact.binding = 1;
            $http.get(baseURL + "DocumentSell/GetBusinessInvoiceByContactNoDebit?contactkey=" + contactkey + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            $scope.Parameter.Param_Document = response.data.responsedata;

                            var grandtotal = 0;
                            _.each($scope.Parameter.Param_Document, function (item) {
                                item.InvoiceDate = formatDate(item.InvoiceDate);
                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                            });

                            $('#modalcontact-list').modal('hide');

                            $scope.document.ContactName = contact.BusinessName;

                            $scope.documentcurrentPage = 0;
                            $scope.document.binding = 0;
                            $scope.documentretpage = [];
                            $scope.documentrange();

                        }
                        else if (response.data.responsecode == '400') {
                            $scope.document.binding = 0;
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            $scope.document.binding = 0;
            showErrorToast(err);
        }
    };

    $scope.OnClickContactAdd = function () {
        $scope.BusinessContactAdd = [];
        $scope.BusinessContactAdd.BusinessType = 'C';
        $scope.BusinessContactAdd.ContactType1 = true;
        $scope.BusinessContactAdd.ContactType2 = false;
        $scope.BusinessContactAdd.BranchType = "H";
        $scope.BusinessContactAdd.AccountType = 'S';

        var type = $scope.Parameter.Param_Quotation.Param_Bank.filter(function (item) { return item.ID == undefined; });
        if (type.length > 0)
            $scope.BusinessContactAdd.SelectBank = type[0];

        $('#modalcontact-add').modal('show');
    };

    $scope.OnClickContactSave = function () {
        try {
            if ($scope.BusinessContactAdd.BusinessName == undefined || $scope.BusinessContactAdd.BusinessName == "")
                throw "กรุณาระบุชื่อธุรกิจ";
            else if ($scope.BusinessContactAdd.ContactType1 == false && $scope.BusinessContactAdd.ContactType2 == false)
                throw "กรุณาระบุประเภท";
            else if ($scope.BusinessContactAdd.ContactEmail != undefined && $scope.BusinessContactAdd.ContactEmail != "" && !ValidateEmail($scope.BusinessContactAdd.ContactEmail))
                throw "กรุณาแก้ไขอีเมล เนื่องจากรูปแบบอีเมลไม่ถูกต้อง";
            else {
                var qq = $q.all([contactService.postNewContact($scope.BusinessContactAdd)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var contact = data[0].data.responsedata;
                                if (contact != undefined) {
                                    $scope.OnClearContact();
                                    $scope.DebitNoteAdd.CustomerKey = contact.ContactKey;
                                    $scope.DebitNoteAdd.CustomerName = contact.BusinessName;
                                    $scope.DebitNoteAdd.CustomerTaxID = contact.TaxID;
                                    $scope.DebitNoteAdd.CustomerContactName = contact.ContactName;
                                    $scope.DebitNoteAdd.CustomerContactPhone = contact.ContactMobile;
                                    $scope.DebitNoteAdd.CustomerContactEmail = contact.ContactEmail;
                                    $scope.DebitNoteAdd.CustomerAddress = contact.Address;
                                    $scope.DebitNoteAdd.CustomerBranch = contact.BranchName;
                                    $scope.DebitNoteAdd.CreditDay = contact.CreditDate;
                                    $scope.OnChangeCredit();
                                }
                                $('#modalcontact-add').modal('hide');
                                showSuccessToast();

                                var q1 = $q.all([paramService.getCustomer()]).then(function (data) {
                                    if (data[0] != undefined && data[0] != "") {
                                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                            $scope.SearchContact = [];
                                            $scope.Parameter.Param_Contact = [];
                                            $scope.Parameter.Param_ContactMain = [];
                                            $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[0].data.responsedata;
                                        }
                                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                    }
                                });

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(response.data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.contactcurrentPage = 0;

    $scope.contactLimitFirst = 0;
    $scope.contactLimitPage = 5;
    $scope.contactitemsPerPage = 10;

    $scope.contactpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Contact.length / $scope.contactitemsPerPage) - 1;
    };

    $scope.contactrange = function () {
        $scope.contactitemsCount = $scope.Parameter.Param_Contact.length;
        $scope.contactpageshow = $scope.contactpageCount() > $scope.contactLimitPage && $scope.contactpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.contactpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.contactpageshowdata = 1;
        $scope.contactretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.contactpcontactPage = function () {
        if ($scope.contactcurrentPage > 0) {
            $scope.contactcurrentPage--;
        }

        if ($scope.contactcurrentPage < $scope.contactLimitFirst && $scope.contactcurrentPage >= 1) {
            $scope.contactLimitFirst = $scope.contactLimitFirst - 5;
            $scope.contactLimitPage = $scope.contactLimitPage - 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }
    };

    $scope.contactpcontactPageDisabled = function () {
        return $scope.contactcurrentPage === 0 ? "disabled" : "";
    };

    $scope.contactnextPage = function () {
        if ($scope.contactcurrentPage < $scope.contactpageCount()) {
            $scope.contactcurrentPage++;
        }

        if ($scope.contactcurrentPage >= $scope.contactLimitPage && $scope.contactcurrentPage <= $scope.contactpageCount()) {
            $scope.contactLimitFirst = $scope.contactLimitFirst + 5;
            $scope.contactLimitPage = $scope.contactLimitPage + 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }

    };

    $scope.contactnextPageDisabled = function () {
        return $scope.contactcurrentPage === $scope.contactpageCount() ? "disabled" : "";
    };

    $scope.contactsetPage = function (n) {
        $scope.contactcurrentPage = n;
    };

    /**************Select Document **************/
    $scope.OnClickCloseDocument = function () {
        window.location.href = baseURL + "DocumentSell/DebitNote";
    };

    $scope.OnClickDocumentSelection = function (invoicekey) {
        try {
            $http.get(baseURL + "DocumentSell/GetBusinessDcoumentReference?DocumentKey=" + invoicekey + "&DocumentType=INV&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            var creditno = $scope.DebitNoteAdd.DebitNoteNo;
                            $scope.DebitNoteAdd = [];
                            $scope.DebitNoteAdd = response.data.responsedata;
                            $scope.DebitNoteAdd.DebitNoteDetail = [];

                            if ($scope.DebitNoteAdd.DebitNoteKey != undefined) {
                                showWariningToast($scope.DebitNoteAdd.InvoiceNo + " เคยถูกสร้างเป็นใบเพิ่มหนี้แล้ว");
                            }
                            else {
                                $scope.DebitNoteAdd.DocumentKey = $scope.DebitNoteAdd.InvoiceKey;
                                $scope.DebitNoteAdd.DocumentNo = $scope.DebitNoteAdd.InvoiceNo;
                                $scope.DebitNoteAdd.ReferenceNo = $scope.DebitNoteAdd.InvoiceNo;

                                _.each($scope.DebitNoteAdd.InvoiceDetail, function (item) {
                                    item.Mapping = false;
                                    item.Quantity = AFormatNumber(item.Quantity, 2);
                                    item.Price = AFormatNumber(item.Price, 2);
                                    item.PriceTax = AFormatNumber(item.PriceTax, 2);
                                    item.PriceAfterTax = AFormatNumber(item.PriceAfterTax, 2);
                                    item.PriceBeforeTax = AFormatNumber(item.PriceBeforeTax, 2);
                                    item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                    item.Discount = AFormatNumber(item.Discount, 2);
                                    item.DiscountAfter = AFormatNumber(item.DiscountAfter, 2);
                                    item.VatRate = AFormatNumber(item.VatRate, 2);
                                    item.Vat = AFormatNumber(item.Vat, 2);
                                    item.VatAfter = AFormatNumber(item.VatAfter, 2);
                                    item.Total = AFormatNumber(item.Total, 2);
                                });

                                $scope.DebitNoteAdd.DocumentOwner = 'INV';

                                /**  clear values */
                                $scope.DebitNoteAdd.DebitNoteNo = creditno;
                                $scope.DebitNoteAdd.DebitNoteKey = undefined;

                                var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                if (type.length > 0)
                                    $scope.DebitNoteAdd.SelectSale = type[0];


                                $scope.DebitNoteAdd.DebitNoteDate = GetDatetimeNow();

                                /*Credit */
                                if ($scope.DebitNoteAdd.CreditType == 'CD')
                                    $scope.OnChangeCredit();

                                $('#inputquotationdate-popup').datepicker('setDate', $scope.DebitNoteAdd.DebitNoteDate);
                                $('#inputdatedue-popup').datepicker('setDate', $scope.DebitNoteAdd.DueDate);

                                var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.DebitNoteAdd.SaleID; });
                                if (type.length > 0)
                                    $scope.DebitNoteAdd.SelectSale = type[0];

                                $scope.DebitNoteAdd.IsTaxHeader = $scope.DebitNoteAdd.IsTaxHeader == '1' ? true : false; // ภาษีรายการ
                                $scope.DebitNoteAdd.IsDiscountHeader = $scope.DebitNoteAdd.IsDiscountHeader == '1' ? true : false; // ส่วนลดรายการ

                                $scope.DebitNoteAdd.IsTaxSummary = $scope.DebitNoteAdd.IsTaxSummary == '1' ? true : false; // หักภาษี 7 % รวม

                                // หักภาษี ณ ที่จ่าย
                                $scope.DebitNoteAdd.IsWithholdingTax = $scope.DebitNoteAdd.IsWithholdingTax == '1' ? true : false;
                                if ($scope.DebitNoteAdd.IsWithholdingTax) {

                                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == $scope.DebitNoteAdd.WithholdingKey; });
                                    if (type.length > 0)
                                        $scope.DebitNoteAdd.SelectWithholding = type[0];

                                    $scope.DebitNoteAdd.IsWithholdingTax.WithholdingAmount = AFormatNumber($scope.DebitNoteAdd.IsWithholdingTax.WithholdingAmount, 2);
                                    $scope.DebitNoteAdd.IsWithholdingTax.GrandAmountAfterWithholding = AFormatNumber($scope.DebitNoteAdd.IsWithholdingTax.GrandAmountAfterWithholding, 2);
                                }
                                else {
                                    var type = $scope.Parameter.Param_Quotation.Param_WithholdingRate.filter(function (item) { return item.ID == '1'; });
                                    if (type.length > 0)
                                        $scope.DebitNoteAdd.SelectWithholding = type[0];

                                    $scope.DebitNoteAdd.WithholdingAmount = AFormatNumber(0, 2);//
                                }

                                $scope.DebitNoteAdd.TotalAmount = AFormatNumber($scope.DebitNoteAdd.TotalAmount, 2);
                                $scope.DebitNoteAdd.DiscountPercent = AFormatNumber(0, 2);
                                $scope.DebitNoteAdd.DiscountAmount = AFormatNumber(0, 2);
                                $scope.DebitNoteAdd.TotalAfterDiscountAmount = AFormatNumber($scope.DebitNoteAdd.TotalAfterDiscountAmount, 2);
                                $scope.DebitNoteAdd.ExemptAmount = AFormatNumber($scope.DebitNoteAdd.ExemptAmount, 2);
                                $scope.DebitNoteAdd.VatableAmount = AFormatNumber($scope.DebitNoteAdd.VatableAmount, 2);
                                $scope.DebitNoteAdd.VAT = AFormatNumber($scope.DebitNoteAdd.VAT, 2);
                                $scope.DebitNoteAdd.TotalBeforeVatAmount = AFormatNumber($scope.DebitNoteAdd.TotalBeforeVatAmount, 2);
                                $scope.DebitNoteAdd.GrandAmount = AFormatNumber($scope.DebitNoteAdd.GrandAmount, 2);
                                $scope.DebitNoteAdd.DebitPriceBefore = AFormatNumber($scope.DebitNoteAdd.TotalAfterDiscountAmount, 2);
                                $scope.DebitNoteAdd.DebitPriceReal = AFormatNumber(0, 2);
                                $scope.DebitNoteAdd.DebitPriceAfter = AFormatNumber($scope.DebitNoteAdd.GrandAmount, 2);

                                $scope.DebitNoteAdd.Signature == $scope.DebitNoteAdd.Signature == '1' ? true : false;

                                $scope.DebitNoteAdd.DebitNoteDetail = [];

                                $('#modalinvoice-list').modal('hide');

                                $scope.OnClickClearDetail();
                                $scope.FirstChooseProduct = '2';
                                $scope.OnClickProductFromInvoice();
                                $scope.table.binding = 0;
                            }
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.documentcurrentPage = 0;

    $scope.documentLimitFirst = 0;
    $scope.documentLimitPage = 5;
    $scope.documentitemsPerPage = 10;

    $scope.documentpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Document.length / $scope.documentitemsPerPage) - 1;
    };

    $scope.documentrange = function () {
        $scope.documentitemsCount = $scope.Parameter.Param_Document.length;
        $scope.documentpageshow = $scope.documentpageCount() > $scope.documentLimitPage && $scope.documentpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.documentpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.documentpageshowdata = 1;
        $scope.documentretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.documentpdocumentPage = function () {
        if ($scope.documentcurrentPage > 0) {
            $scope.documentcurrentPage--;
        }

        if ($scope.documentcurrentPage < $scope.documentLimitFirst && $scope.documentcurrentPage >= 1) {
            $scope.documentLimitFirst = $scope.documentLimitFirst - 5;
            $scope.documentLimitPage = $scope.documentLimitPage - 5;
            for (var i = 1; i <= $scope.documentretpage.length; i++) {
                if (i >= $scope.documentLimitFirst && i <= $scope.documentLimitPage) {
                    $scope.documentretpage[i - 1].show = 1;
                }
                else
                    $scope.documentretpage[i - 1].show = 0;
            }
        }
    };

    $scope.documentpdocumentPageDisabled = function () {
        return $scope.documentcurrentPage === 0 ? "disabled" : "";
    };

    $scope.documentnextPage = function () {
        if ($scope.documentcurrentPage < $scope.documentpageCount()) {
            $scope.documentcurrentPage++;
        }

        if ($scope.documentcurrentPage >= $scope.documentLimitPage && $scope.documentcurrentPage <= $scope.documentpageCount()) {
            $scope.documentLimitFirst = $scope.documentLimitFirst + 5;
            $scope.documentLimitPage = $scope.documentLimitPage + 5;
            for (var i = 1; i <= $scope.documentretpage.length; i++) {
                if (i >= $scope.documentLimitFirst && i <= $scope.documentLimitPage) {
                    $scope.documentretpage[i - 1].show = 1;
                }
                else
                    $scope.documentretpage[i - 1].show = 0;
            }
        }

    };

    $scope.documentnextPageDisabled = function () {
        return $scope.documentcurrentPage === $scope.documentpageCount() ? "disabled" : "";
    };

    $scope.documentsetPage = function (n) {
        $scope.documentcurrentPage = n;
    };
    /**************Select Product **************/

    $scope.OnClickProductPopup = function () {
        $scope.SearchProduct.InputFilter = [];
        $scope.productcurrentPage = 0;
        $scope.product = [];
        $scope.product.binding = 0;
        $scope.productretpage = [];
        $scope.productrange();
        $('#modalproduct-list').modal('show');
    };

    $scope.OnClickProductSearch = function () {
        $scope.Parameter.Param_Product = productService.filterProduct($scope.SearchProduct.InputFilter, $scope.Parameter.Param_Product, $scope.Parameter.Param_ProductMain);
        if ($scope.Parameter.Param_Product == undefined)
            $scope.Parameter.Param_Product = [];
    };

    $scope.OnClickProductSelection = function (productkey) {
        var product = _.where($scope.Parameter.Param_Product, { ProductKey: productkey })[0];
        if (product != undefined) {
            $scope.OnClickClearDetail();
            $scope.ProductDetailAdd.ProductKey = product.ProductKey;
            $scope.ProductDetailAdd.Name = product.ProductName;
            $scope.ProductDetailAdd.Description = product.ProductDescription;
            $scope.ProductDetailAdd.Quantity = '1.00';
            $scope.ProductDetailAdd.Unit = product.UnitTypeName;
            $scope.ProductDetailAdd.DiscountType = $scope.DebitNoteAdd.IsDiscountHeaderType;
            $scope.ProductDetailAdd.DiscountPercent = '0.00';
            $scope.ProductDetailAdd.Discount = '0.00';
            $scope.ProductDetailAdd.VatType = '2';

            if (product.SellTaxType == '1' || product.SellTaxType == '3' || product.SellTaxType == '4') {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var sell = 0, selltax = 0, selllast = 0;
                sell = ConvertToDecimal(product.SellPrice == undefined ? 0 : product.SellPrice);
                selltax = (sell * (parseInt(100) + parseInt(vat))) / 100 - sell;
                selllast = sell + selltax;

                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(sell, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(selllast, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(selltax, 2);

                if (product.SellTaxType == '3') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '1';
                }
                else if (product.SellTaxType == '4') {
                    $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(sell, 2);
                    $scope.ProductDetailAdd.VatType = '3';
                }
            }
            else if (product.SellTaxType == '2') {

                $scope.ProductDetailAdd.PriceBeforeTax = AFormatNumber(product.SellAfterTaxPrice, 2);
                $scope.ProductDetailAdd.PriceAfterTax = AFormatNumber(product.SellPrice, 2);
                $scope.ProductDetailAdd.PriceTax = AFormatNumber(product.SellTaxPrice, 2);
            }

            if ($scope.DebitNoteAdd.TaxType == 'N')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceBeforeTax, 2);
            else if ($scope.DebitNoteAdd.TaxType == 'V')
                $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.PriceAfterTax, 2);

            $scope.ProductDetailAdd.Price = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $scope.ProductDetailAdd.Total = AFormatNumber($scope.ProductDetailAdd.Price, 2);
            $('#modalproduct-list').modal('hide');
        }
    };

    $scope.OnClickProductAdd = function () {
        $scope.BusinessProductAdd = [];
        $scope.BusinessProductAdd.ProductType = 'S';
        $scope.BusinessProductAdd.SellTaxType = '1';
        $scope.BusinessProductAdd.BuyTaxType = '1';
        initdropify('');

        $('#modalproduct-add').modal('show');
    };

    function initdropify(path) {
        $("#product_thumnail").addClass('dropify');
        var publicpath_identity_picture = path;

        var drEvent = $('.dropify').dropify();
        drEvent.on('dropify.afterClear', function (event, element) {
            $scope.DeleteImages = 1;
        });
        drEvent = drEvent.data('dropify');
        drEvent.resetPreview();
        drEvent.clearElement();
        drEvent.settings.defaultFile = publicpath_identity_picture;
        drEvent.destroy();

        drEvent.init();

        $('.dropify#identity_picture').dropify({
            defaultFile: publicpath_identity_picture
        });

        $('.dropify').dropify();
    }

    $scope.OnClickProductSave = function () {
        try {
            if ($scope.BusinessProductAdd.ProductName == undefined || $scope.BusinessProductAdd.ProductName == "")
                throw "กรุณาระบุชื่อสินค้า/บริการ";
            else {
                var vat = _.where($scope.Parameter.Param_Quotation.Biz_Param, { ParameterField: 'ValueAddedTax' })[0].ParameterValue;
                var unit = document.getElementById('input_unittype').value;
                var category = document.getElementById('input_categorytype').value;


                var qq = $q.all([productService.postNewProduct($scope.BusinessProductAdd, vat, unit, category)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var product = data[0].data.responsedata;

                                if (product != undefined) {
                                    //$scope.ProductDetailAdd.ProductKey = product.ProductKey;
                                    //$scope.ProductDetailAdd.Name = product.ProductName;
                                    //$scope.ProductDetailAdd.Description = product.ProductDescription;
                                    //$scope.ProductDetailAdd.Quantity = '1.00';
                                    //$scope.ProductDetailAdd.Unit = product.UnitTypeName;
                                    //$scope.ProductDetailAdd.Price = AFormatNumber($scope.DebitNoteAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);
                                    //$scope.ProductDetailAdd.Total = AFormatNumber($scope.DebitNoteAdd.TaxType == 'N' ? product.SellPrice : product.SellAfterTaxPrice, 2);

                                    if ($scope.SelectedFiles != undefined && $scope.Upload == 1) {
                                        try {

                                            var input = document.getElementById("product_thumnail");
                                            var files = input.files;
                                            var formData = new FormData();

                                            for (var i = 0; i != files.length; i++) {
                                                formData.append("files", files[i]);
                                            }

                                            $.ajax(
                                                {
                                                    url: baseURL + "Product/UploadPictureProducts?productkey=" + product.ProductKey,
                                                    data: formData,
                                                    processData: false,
                                                    contentType: false,
                                                    type: "POST",
                                                    success: function (data) {
                                                        $('#modalproduct-add').modal('hide');
                                                        showSuccessToast();
                                                    },
                                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                        showErrorToast(textStatus);
                                                    }
                                                }
                                            );
                                        }
                                        catch (ex) {
                                            showErrorToast(ex);
                                        }
                                    }
                                    else {
                                        $('#modalproduct-add').modal('hide');
                                        showSuccessToast();
                                    }


                                    var q1 = $q.all([paramService.getProduct()]).then(function (data) {
                                        if (data[0] != undefined && data[0] != "") {
                                            if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                                $scope.SearchProduct = [];
                                                $scope.Parameter.Param_Product = [];
                                                $scope.Parameter.Param_ProductMain = [];

                                                $scope.Parameter.Param_ProductMain = data[0].data.responsedata;

                                                _.each($scope.Parameter.Param_ProductMain, function (item) {
                                                    item.SellPrice = AFormatNumber(item.SellPrice, 2);
                                                    item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                                                    item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                                                    item.SellPriceText = item.SellTaxType != '2' ? AFormatNumber(item.SellPrice, 2) : AFormatNumber(item.SellAfterTaxPrice, 2);
                                                    item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                                                    item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                                                    item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                                                });

                                                $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;

                                                $scope.OnClickProductSelection(product.ProductKey);
                                            }
                                            else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                        }
                                    });
                                }

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(data[0].data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.productcurrentPage = 0;

    $scope.productLimitFirst = 0;
    $scope.productLimitPage = 5;
    $scope.productitemsPerPage = 10;

    $scope.productpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.DebitNoteAdd.InvoiceDetail.length / $scope.productitemsPerPage) - 1;
    };

    $scope.productrange = function () {
        $scope.productitemsCount = $scope.DebitNoteAdd.InvoiceDetail.length;
        $scope.productpageshow = $scope.productpageCount() > $scope.productLimitPage && $scope.productpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.productpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.productpageshowdata = 1;
        $scope.productretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.productpproductPage = function () {
        if ($scope.productcurrentPage > 0) {
            $scope.productcurrentPage--;
        }

        if ($scope.productcurrentPage < $scope.productLimitFirst && $scope.productcurrentPage >= 1) {
            $scope.productLimitFirst = $scope.productLimitFirst - 5;
            $scope.productLimitPage = $scope.productLimitPage - 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }
    };

    $scope.productpproductPageDisabled = function () {
        return $scope.productcurrentPage === 0 ? "disabled" : "";
    };

    $scope.productnextPage = function () {
        if ($scope.productcurrentPage < $scope.productpageCount()) {
            $scope.productcurrentPage++;
        }

        if ($scope.productcurrentPage >= $scope.productLimitPage && $scope.productcurrentPage <= $scope.productpageCount()) {
            $scope.productLimitFirst = $scope.productLimitFirst + 5;
            $scope.productLimitPage = $scope.productLimitPage + 5;
            for (var i = 1; i <= $scope.productretpage.length; i++) {
                if (i >= $scope.productLimitFirst && i <= $scope.productLimitPage) {
                    $scope.productretpage[i - 1].show = 1;
                }
                else
                    $scope.productretpage[i - 1].show = 0;
            }
        }

    };

    $scope.productnextPageDisabled = function () {
        return $scope.productcurrentPage === $scope.productpageCount() ? "disabled" : "";
    };

    $scope.productsetPage = function (n) {
        $scope.productcurrentPage = n;
    };

    $scope.UploadFiles = function (files) {
        $scope.Upload = 1;
        $scope.SelectedFiles = files;
    };

    /****************** Document Attach ************************/
    $scope.OnLoadAttach = function () {
        try {
            $http.get(baseURL + "DocumentSell/GetDocumentAttach?documentkey=" + $scope.DebitNoteAdd.DebitNoteKey + "&documenttype=" + headerdoc + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.DocumentAttach = [];
                            $scope.DocumentAttach = response.data.responsedata;
                        }
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnUploadAttach = function (files) {
        try {
            if ($scope.DebitNoteAdd.DebitNoteKey == undefined || $scope.DebitNoteAdd.DebitNoteKey == "")
                throw "กรุณากดปุ่ม บันทึก ก่อนแนบไฟล์เอกสาร";
            else if ($scope.DocumentAttach.length > 3)
                throw "สามารถแนบไฟล์สูงสุดเพียง 3 ไฟล์ เท่านั้น";
            else if (files.length > 0) {
                var data = {
                    DocumentKey: $scope.DebitNoteAdd.DebitNoteKey,
                    DocumentNo: $scope.DebitNoteAdd.DebitNoteNo,
                    DocumentType: headerdoc,
                    AttachName: files[0].name,
                    AttachExtension: getFileExtension(files[0].name)
                };
                $http.post(baseURL + "DocumentSell/PostDocumentAttach", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    try {
                                        var attachfile = response.data.responsedata;
                                        var input = document.getElementById("document_attach");
                                        var files = input.files;
                                        var formData = new FormData();

                                        for (var i = 0; i != files.length; i++) {
                                            formData.append("files", files[i]);
                                        }

                                        $.ajax(
                                            {
                                                url: baseURL + "DocumentSell/UploadDocumentAttach?attachkey=" + attachfile.AttachKey,
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                type: "POST",
                                                success: function (data) {

                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();

                                                    $scope.OnLoadAttach();
                                                    showSuccessText('แนบไฟล์สำเร็จ');
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();
                                                    showErrorToast(textStatus);
                                                }
                                            }
                                        );
                                    }
                                    catch (ex) {
                                        var drEvent = $('.dropify').dropify();
                                        drEvent = drEvent.data('dropify');
                                        drEvent.resetPreview();
                                        drEvent.clearElement();
                                        drEvent.destroy();

                                        drEvent.init();
                                        showErrorToast(ex);
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            var drEvent = $('.dropify').dropify();
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.destroy();

            drEvent.init();
            showWariningToast(err);
        }
    };

    $scope.OnClickDeleteAttach = function (attachkey) {
        var data = {
            DocumentKey: $scope.DebitNoteAdd.DebitNoteKey,
            DocumentNo: $scope.DebitNoteAdd.DebitNoteNo,
            DocumentType: headerdoc,
            AttachKey: attachkey
        };
        $http.post(baseURL + "DocumentSell/DeleteDocumentAttach", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            try {
                                showDeleteSuccessToast();
                                $scope.OnLoadAttach();
                            }
                            catch (err) {
                                showErrorToast(err);
                            }
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickShowAttach = function (attachkey) {
        var attactfile = _.where($scope.DocumentAttach, { AttachKey: attachkey })[0];
        if (attactfile != undefined) {
            $scope.DocumentAttachView = [];
            $scope.DocumentAttachView = attactfile;

            if (attactfile.AttachExtension == 'pdf') {
                $scope.DocumentAttachView.AttachHeight = '850px';
                PDFObject.embed(baseURL + 'uploads/attach/' + attactfile.AttachPath, "#exampleattach");
            }
            else {
                $scope.DocumentAttachView.AttachHeight = 'auto';
                $scope.DocumentAttachView.AttachShow = baseURL + 'uploads/attach/' + attactfile.AttachPath;
            }
            $('#modal-attach').modal('show');
        }
    };
});


