﻿WEACCTAPP.controller('billingcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, $q) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.BusinessBilling = [];
    $scope.BusinessBillingMain = [];
    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];
    $scope.BusinessProfile = [];
    $scope.BusinessEmployee = [];
    $scope.EmailAdd = [];
    var gEmp = paramService.getProfile();
    var gProfile = paramService.getBusinessProfile();

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
            $scope.Search.InputToDate = GetDatetimeNowLastMonth();

            var qq = $q.all([gProfile, gEmp]).then(function (data) {
                try {
                    /****** GetDocument NO ******/
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BusinessProfile = data[0].data.responsedata; }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Customer ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.BusinessEmployee = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });

            $http.get(baseURL + "DocumentSell/GetBusinessBilling?key=" + makeid())
                .then(function (response) {

                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.BusinessBillingMain = response.data.responsedata;

                            var grandtotal = 0;
                            _.each($scope.BusinessBillingMain, function (item) {
                                item.BillingDate = formatDate(item.BillingDate);
                                if (item.CreditType == 'CD')
                                    item.DueDate = formatDate(item.DueDate);
                                if (item.BillingStatus != '3')
                                    grandtotal = grandtotal + item.GrandAmount;
                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                            });
                            $scope.BillingSummary = AFormatNumber(grandtotal, 2);
                            $scope.BusinessBilling = $scope.BusinessBillingMain;
                            
                            $scope.Search.BillingType = 'ALL';

                            $scope.retpage = [];
                            $scope.range();
                            $scope.table.binding = 0;
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickSelectType = function (val) {

        if (val == 'ALL')
            $scope.BusinessBilling = $scope.BusinessBillingMain;
        else if (val == 'BL') // ใบวางบิล
            $scope.BusinessBilling = _.where($scope.BusinessBillingMain, { BillingType: 'BL' });
        else if (val == 'CBL') //ใบวางบิลรวม
            $scope.BusinessBilling = _.where($scope.BusinessBillingMain, { BillingType: 'CBL' });

        $scope.Search.BillingType = val;
    };

    $scope.OnClickClear = function () {
        $scope.Search = [];
        $scope.Search.SelectStatus = '0';
        $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
        $scope.Search.InputToDate = GetDatetimeNowLastMonth();
        $scope.BusinessBilling = $scope.BusinessBillingMain;
    };

    $scope.OnClickSearch = function () {
        $scope.BusinessBilling = $scope.BusinessBillingMain;
        $scope.table.binding = 1;

        $scope.BusinessBilling = _.filter($scope.BusinessBillingMain, function (item) {

            var c1 = true, c2 = true, c3 = true, c4 = true, c5 = true;

            if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.CustomerName != undefined && (item.CustomerName).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.CustomerName == undefined || (item.CustomerName).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.BillingNo != undefined && (item.BillingNo).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.BillingNo == undefined || (item.BillingNo).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProjectName != undefined && (item.ProjectName).indexOf($scope.Search.InputFilter) > -1)
                    c3 = true;
                else if (item.ProjectName == undefined || (item.ProjectName).indexOf($scope.Search.InputFilter) < 0)
                    c3 = false;


            }

            if ($scope.Search.SelectStatus == '0') {
                c4 = true;
            }
            else if (item.BillingStatus == $scope.Search.SelectStatus) {
                c4 = true;
            }
            else
                c4 = false;

            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != "" && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != "") {

                var fromarry = $scope.Search.InputFromDate.split('-');

                var quotationdatearry = item.BillingDate.split('-');

                var from = new Date(fromarry[2], fromarry[1] - 1, fromarry[0]);

                var quotationdate = new Date(quotationdatearry[2], quotationdatearry[1] - 1, quotationdatearry[0]);

                var toarry = $scope.Search.InputToDate.split('-');

                var to = new Date(toarry[2], toarry[1] - 1, toarry[0]);
                //alert(quotationdate); alert(from); alert(from);
                if (quotationdate >= from && quotationdate <= to)
                    c5 = true;
                else
                    c5 = false;
            }


            if ((c1 || c2 || c3) && c4 && c5) {
                return item;
            }
        });
        $scope.table.binding = 0;
    };

    $scope.OnClickAdd = function () {
        window.location.href = baseURL + "DocumentSell/BillingNoteAdd";
    };

    $scope.OnClickCumulativeAdd = function () {
        window.location.href = baseURL + "DocumentSell/BillingNoteCumulativeAdd";
    };

    $scope.OnClickUpdate = function (key, type) {
        if (type == 'BL')
            window.location.href = baseURL + "DocumentSell/BillingNoteAdd?BillingKey=" + key;
        else if (type == 'CBL')
            window.location.href = baseURL + "DocumentSell/BillingNoteCumulativeAdd?BillingKey=" + key;
    };

    $scope.OnClickDuplicate = function (key) {
        window.location.href = baseURL + "DocumentSell/BillingNoteAdd?BillingKey=" + key + "&dup_ref=1";

    };

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.BusinessBilling, { BillingKey: val })[0];
        if (filter != undefined) {
            $scope.BillingAdd = [];
            $scope.BillingAdd = angular.copy(filter);
            if ($scope.BillingAdd.BillingStatus == '1')
                $('#modalbilling-del').modal('show');
            else
                $('#modalbilling-alert').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            BillingKey: $scope.BillingAdd.BillingKey
        };

        $http.post(baseURL + "DocumentSell/DeleteBusinessBilling", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalbilling-del').modal('hide');
                            $scope.init();
                            $scope.BillingAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickNextWorkflow = function (billingkey, tostatus) {
        try {
            var data = {
                BillingKey: billingkey,
                BillingStatus: tostatus
            };
            $http.post(baseURL + "DocumentSell/PostBusinessBillingNextStep", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.init();
                                showSuccessWorkToast();
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickDocumentReference = function (billingkey, doctype) {
        if (doctype == 'INV')
            window.location.href = baseURL + "DocumentSell/InvoiceAdd?ref_key=" + billingkey + "&ref_info=BL";
        else if (doctype == 'CRE')
            window.location.href = baseURL + "DocumentSell/ReceiptCumulativeAdd?ref_key=" + billingkey + "&ref_info=BL";
    }

    $scope.OnClickReportContent = function (dockey,docno,type) {
        $scope.document = [];
        $scope.document.DocKey = dockey;
        $scope.document.DocNo = docno;
        $scope.document.original = $scope.document.copy = true;
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $('#modal-content-report').modal('show');
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload(); 
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        $scope.document.DocNo = $scope.document.DocNo;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'BL',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'BL',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Download',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickSendMail = function (dockey, docno) {
        var quo = _.where($scope.BusinessBilling, { BillingKey: dockey })[0];
        if (quo != undefined) {
            $scope.EmailAdd = [];
            $scope.EmailAdd.DocKey = dockey;
            $scope.EmailAdd.DocNo = docno;
            $scope.EmailAdd.MailToMe = $scope.EmailAdd.Content1 = $scope.EmailAdd.Content2 = true;
            $scope.EmailAdd.MailFrom = $scope.BusinessEmployee.Email;
            $scope.EmailAdd.MailTo = quo.CustomerContactEmail;
            $scope.EmailAdd.Subject = 'เอกสาร ' + docno;
            $scope.EmailAdd.Message = 'เรียน ' + quo.CustomerName + '\n\n' + $scope.BusinessProfile.BusinessName + ' เลขที่ ' + docno + ' มาให้ในอีเมลนี้\n\nกรุณาคลิกที่ไฟล์แนบ เพื่อดาวน์โหลดเอกสารของคุณ\n\nด้วยความเคารพ\n' + $scope.BusinessEmployee.EmployeeName + '\n' + $scope.BusinessProfile.BusinessName;
            $('#modalemail-send').modal('show');
        }

    };

    $scope.OnClickConfirmEmail = function () {
        $('#modalemail-send').modal('hide');
        var data = {
            MailFrom: $scope.EmailAdd.MailFrom,
            MailTo: $scope.EmailAdd.MailTo,
            MailCC: $scope.EmailAdd.MailCC,
            MailIsMe: $scope.EmailAdd.MailToMe ? "1" : "0",
            Subject: $scope.EmailAdd.Subject,
            Message: $scope.EmailAdd.Message,
            DocumentKey: $scope.EmailAdd.DocKey,
            DocumentNo: $scope.EmailAdd.DocNo,
            DocumentType: 'BL',
            Content: $scope.EmailAdd.Content1 == true && $scope.EmailAdd.Content2 == true ? "NORMAL" : $scope.EmailAdd.Content1 == true ? "ORIGINAL" : $scope.EmailAdd.Content2 == true ? "COPY" : "ORIGINAL",
        };

        $http.post(baseURL + "DocumentSell/PostEmailToSending", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {

                            showSuccessText(response.data.responsedata);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };
    /****************************/

    $scope.itemsPerPage = 20;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'BillingKey';
    $scope.reverseSort = true;


    $scope.pageCount = function () {
        return Math.ceil($scope.BusinessBilling.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessBilling.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

});


