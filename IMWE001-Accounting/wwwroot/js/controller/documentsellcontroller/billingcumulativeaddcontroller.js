﻿WEACCTAPP.controller('billingcumulativeaddcontroller', function ($scope, $http, $timeout, GlobalVar, paramService, contactService, productService, $q, Upload) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'BL';

    $scope.DocumentAttach = [];
    $scope.BillingAdd = [];
    $scope.BillingAdd.InvoiceDetail = [];
    $scope.Parameter = [];
    $scope.UserProfile = [];
    $scope.Parameter.Param_Contact = [];
    $scope.Parameter.Param_Document = [];
    $scope.Parameter.Param_Product = [];
    $scope.Parameter.Param_Employee = [];
    $scope.Parameter.Param_Revenue = [];
    $scope.table = [];

    var gCustomer = paramService.getCustomer();
    var gProduct = paramService.getProduct();
    var gEmployee = paramService.getEmployee();
    var gParamQuotation = paramService.getParameterQuotation();
    var gProfile = paramService.getProfile();
    var gRemark = paramService.getDocumentRemark(headerdoc);

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        $scope.table.binding = 1;
        $scope.CheckingAll = false;
        var prefix = { asofdate: GetDatetimeNow(), documenttype: headerdoc };

        var qq = $q.all([paramService.getDocumentNo(prefix), gCustomer, gProduct, gEmployee, gParamQuotation, gProfile, gRemark]).then(function (data) {
            try {
                /****** GetDocument NO ******/
                if (data[0] != undefined && data[0] != "") {
                    if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BillingAdd.BillingNo = data[0].data.responsedata; }
                    else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                }

                /****** Get Customer ******/
                if (data[1] != undefined && data[1] != "") {
                    if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                        $scope.SearchContact = [];
                        $scope.Parameter.Param_Contact = [];
                        $scope.Parameter.Param_ContactMain = [];
                        $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                    }
                    else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                }

                /****** Get Product ******/
                if (data[2] != undefined && data[2] != "") {
                    if (data[2].data.responsecode == '200' && data[2].data.responsedata != undefined && data[2].data.responsedata != "") {
                        $scope.SearchProduct = [];
                        $scope.Parameter.Param_Product = [];
                        $scope.Parameter.Param_ProductMain = [];

                        $scope.Parameter.Param_ProductMain = data[2].data.responsedata;

                        _.each($scope.Parameter.Param_ProductMain, function (item) {
                            item.SellPrice = AFormatNumber(item.SellPrice, 2);
                            item.SellTaxPrice = AFormatNumber(item.SellTaxPrice, 2);
                            item.SellAfterTaxPrice = AFormatNumber(item.SellAfterTaxPrice, 2);
                            item.BuyPrice = AFormatNumber(item.BuyPrice, 2);
                            item.BuyTaxPrice = AFormatNumber(item.BuyTaxPrice, 2);
                            item.BuyAfterTaxPrice = AFormatNumber(item.BuyAfterTaxPrice, 2);
                        });

                        $scope.Parameter.Param_Product = $scope.Parameter.Param_ProductMain;
                    }
                    else if (data[2].data.responsecode == '400') { showErrorToast(data[2].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[3] != undefined && data[3] != "") {
                    if (data[3].data.responsecode == '200' && data[3].data.responsedata != undefined && data[3].data.responsedata != "") {
                        $scope.Parameter.Param_Employee = [];
                        $scope.Parameter.Param_Employee = data[3].data.responsedata;
                    }
                    else if (data[3].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Parameter Billing ******/
                if (data[4] != undefined && data[4] != "") {
                    if (data[4].data.responsecode == '200' && data[4].data.responsedata != undefined && data[4].data.responsedata != "") {
                        $scope.Parameter.Param_Quotation = [];
                        $scope.Parameter.Param_Quotation = data[4].data.responsedata;

                        var substringMatcher = function (strs) {
                            return function findMatches(q, cb) {
                                if (q == '') {
                                    cb(strs);
                                }
                                else {
                                    var matches, substringRegex;

                                    // an array that will be populated with substring matches
                                    matches = [];

                                    // regex used to determine if a string contains the substring `q`
                                    var substrRegex = new RegExp(q, 'i');

                                    // iterate through the pool of strings and for any string that
                                    // contains the substring `q`, add it to the `matches` array
                                    for (var i = 0; i < strs.length; i++) {
                                        if (substrRegex.test(strs[i])) {
                                            matches.push(strs[i]);
                                        }
                                    }

                                    cb(matches);
                                }
                            };
                        };

                        var unittype = [];
                        _.each($scope.Parameter.Param_Quotation.Param_UnitType, function (item) { unittype.push(item.Name); });

                        $('#productunit .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0
                        }, {
                                name: 'unittype',
                                limit: 20,
                                source: substringMatcher(unittype)
                            });


                        var category = [];
                        _.each($scope.Parameter.Param_Quotation.Param_Category, function (item) { category.push(item.Name); });
                        $('#productcategory .typeahead').typeahead({
                            hint: false,
                            highlight: true,
                            minLength: 0,
                        }, {
                                name: 'category',
                                limit: 20,
                                source: substringMatcher(category)
                            });
                    }
                    else if (data[4].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                }

                /****** Get Employee ******/
                if (data[5] != undefined && data[5] != "") {
                    if (data[5].data.responsecode == '200' && data[5].data.responsedata != undefined && data[5].data.responsedata != "") {
                        $scope.UserProfile = [];
                        $scope.UserProfile = data[5].data.responsedata;
                    }
                    else if (data[5].data.responsecode == '400') { showErrorToast(data[5].data.errormessage); }
                }

                /****** Get Remark ******/
                if (data[6] != undefined && data[6] != "") {
                    if (data[6].data.responsecode == '200' && data[6].data.responsedata != undefined && data[6].data.responsedata != "") {
                        $scope.BillingAdd.Remark = data[6].data.responsedata.DocumentText;
                    }
                    else if (data[6].data.responsecode == '400') { showErrorToast(data[6].data.errormessage); }
                }

                if (QueryString('BillingKey') != undefined) {

                    $http.get(baseURL + "DocumentSell/GetBusinessBillingkey?BillingKey=" + QueryString('BillingKey') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.BillingAdd = [];
                                    $scope.BillingAdd = response.data.responsedata;

                                    $scope.BillingAdd.BillingDate = formatDate($scope.BillingAdd.BillingDate);
                                    /*Credit */
                                    $scope.BillingAdd.DueDate = $scope.BillingAdd.DueDate != undefined && $scope.BillingAdd.DueDate != "" ? formatDate($scope.BillingAdd.DueDate) : "";

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.BillingAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.BillingAdd.SelectSale = type[0];

                                    $scope.BillingAdd.TotalQTY = $scope.BillingAdd.BillingCumulativeDetail.length;
                                    $scope.BillingAdd.TotalAmount = $scope.BillingAdd.TaxType == 'Y' ? AFormatNumber($scope.BillingAdd.TotalBeforeVatAmount, 2) : AFormatNumber($scope.BillingAdd.TotalAmount, 2);
                                    $scope.BillingAdd.TotalQTY = $scope.BillingAdd.BillingCumulativeDetail.length;
                                    $scope.BillingAdd.DiscountPercent = AFormatNumber($scope.BillingAdd.DiscountPercent, 2);
                                    $scope.BillingAdd.DiscountAmount = AFormatNumber($scope.BillingAdd.DiscountAmount, 2);
                                    $scope.BillingAdd.TotalAfterDiscountAmount = AFormatNumber($scope.BillingAdd.TotalAfterDiscountAmount, 2);
                                    $scope.BillingAdd.ExemptAmount = AFormatNumber($scope.BillingAdd.ExemptAmount, 2);
                                    $scope.BillingAdd.VatableAmount = AFormatNumber($scope.BillingAdd.VatableAmount, 2);
                                    $scope.BillingAdd.VAT = AFormatNumber($scope.BillingAdd.VAT, 2);
                                    $scope.BillingAdd.TotalBeforeVatAmount = AFormatNumber($scope.BillingAdd.TotalBeforeVatAmount, 2);
                                    $scope.BillingAdd.GrandAmount = AFormatNumber($scope.BillingAdd.GrandAmount, 2);

                                    $scope.BillingAdd.Signature = $scope.BillingAdd.Signature == '1' ? true : false;

                                    _.each($scope.BillingAdd.BillingCumulativeDetail, function (item) {
                                        item.DocumentDate = formatDate(item.DocumentDate);
                                        item.DueDate = formatDate(item.DueDate);
                                        item.TotalAmount = AFormatNumber(item.TotalAmount, 2);
                                        item.DiscountPercent = AFormatNumber(item.DiscountPercent, 2);
                                        item.DiscountAmount = AFormatNumber(item.DiscountAmount, 2);
                                        item.TotalAfterDiscountAmount = AFormatNumber(item.TotalAfterDiscountAmount, 2);
                                        item.ExemptAmount = AFormatNumber(item.ExemptAmount, 2);
                                        item.VatableAmount = AFormatNumber(item.VatableAmount, 2);
                                        item.VAT = AFormatNumber(item.VAT, 2);
                                        item.TotalBeforeVatAmount = AFormatNumber(item.TotalBeforeVatAmount, 2);
                                        item.WithholdingRate = AFormatNumber(item.WithholdingRate, 2);
                                        item.WithholdingAmount = AFormatNumber(item.WithholdingAmount, 2);
                                        item.GrandAmountAfterWithholding = AFormatNumber(item.GrandAmountAfterWithholding, 2);
                                        item.GrandAmount = AFormatNumber(item.GrandAmount, 2);
                                    });

                                    $scope.OnLoadAttach();

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else if (QueryString('ref_key') != undefined && QueryString('ref_info') != undefined) {
                    // New document With Invoice
                    $http.get(baseURL + "DocumentSell/GetBusinessDcoumentReference?DocumentKey=" + QueryString('ref_key') + "&DocumentType=" + QueryString('ref_info') + "&key=" + makeid())
                        .then(function (response) {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {

                                    $scope.BillingAdd = [];
                                    $scope.BillingAdd = response.data.responsedata;
                                    $scope.BillingAdd.BillingCumulativeDetail = [];

                                    /** Credit Note Values*/
                                    if (QueryString('ref_info') == 'CN') {
                                        $scope.BillingAdd.DocumentKey = $scope.BillingAdd.CreditNoteKey;
                                        $scope.BillingAdd.DocumentNo = $scope.BillingAdd.CreditNoteNo;
                                    }
                                    else if (QueryString('ref_info') == 'INV') {
                                        $scope.BillingAdd.DocumentKey = $scope.BillingAdd.InvoiceKey;
                                        $scope.BillingAdd.DocumentNo = $scope.BillingAdd.InvoiceNo;
                                    }
                                    else if (QueryString('ref_info') == 'DN') {
                                        $scope.BillingAdd.DocumentKey = $scope.BillingAdd.DebitNoteKey;
                                        $scope.BillingAdd.DocumentNo = $scope.BillingAdd.DebitNoteNo;
                                    }
                                    $scope.BillingAdd.BillingStatus = "1";
                                    $scope.BillingAdd.BillingStatusName = "รอวางบิล";


                                    var dock = { documentkey: QueryString('ref_key'), documentype: QueryString('ref_info') };
                                    var values = [];
                                    values.push(dock);

                                    $http.post(baseURL + "DocumentSell/GetBusinessCumulative", values, config).then(
                                        function (response) {
                                            try {
                                                if (response != undefined && response != "") {
                                                    if (response.data.responsecode == 200) {
                                                        var responseDcoument = response.data.responsedata;
                                                        if (responseDcoument != undefined) {

                                                            var i = 1;
                                                            $scope.BillingAdd.BillingCumulativeDetail = [];
                                                            _.each(responseDcoument.Document_Invoice, function (item) {
                                                                
                                                                if (item.CreditType == 'CA')
                                                                    item.DueDate = GetDatetimeNow();
                                                                else if (item.CreditType == 'CN')
                                                                    item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                                                else if (item.CreditType == 'CD')
                                                                    item.DueDate = formatDate(item.DueDate);
                                                                
                                                                $scope.BillingAdd.BillingCumulativeDetail.push({
                                                                    Sequence: i,
                                                                    DocumentKey: item.InvoiceKey,
                                                                    DocumentNo: item.InvoiceNo,
                                                                    DocumentType: 'INV',
                                                                    DocumentCategoryKey: item.InvoiceKey,
                                                                    DocumentDate: formatDate(item.InvoiceDate),
                                                                    DueDate: item.DueDate,
                                                                    TaxType: item.TaxType,
                                                                    TaxTypeName: item.TaxTypeName,
                                                                    TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                                    IsDiscountHeader: item.IsDiscountHeader,
                                                                    IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                                    DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                                    DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                                    TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                                    IsTaxHeader: item.IsTaxHeader,
                                                                    IsTaxSummary: item.IsTaxSummary,
                                                                    ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                                    VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                                    VAT: AFormatNumber(item.VAT, 2),
                                                                    TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                                    IsWithholdingTax: item.IsWithholdingTax,
                                                                    WithholdingKey: item.WithholdingKey,
                                                                    WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                                    WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                                    GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                                    GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                                                });
                                                                i++;
                                                            });

                                                            _.each(responseDcoument.Document_CreditNote, function (item) {

                                                                if (item.CreditType == 'CA')
                                                                    item.DueDate = GetDatetimeNow();
                                                                else if (item.CreditType == 'CN')
                                                                    item.DueDate = SetDatetimeDay(formatDate(item.CreditNoteDate), item.CreditDay);
                                                                else if (item.CreditType == 'CD')
                                                                    item.DueDate = formatDate(item.DueDate);

                                                                $scope.BillingAdd.BillingCumulativeDetail.push({
                                                                    Sequence: i,
                                                                    DocumentKey: item.CreditNoteKey,
                                                                    DocumentNo: item.CreditNoteNo,
                                                                    DocumentType: 'CN',
                                                                    DocumentCategoryKey: item.InvoiceKey,
                                                                    DocumentDate: formatDate(item.CreditNoteDate),
                                                                    DueDate: item.DueDate,
                                                                    TaxType: item.TaxType,
                                                                    TaxTypeName: item.TaxTypeName,
                                                                    TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                                    IsDiscountHeader: item.IsDiscountHeader,
                                                                    IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                                    DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                                    DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                                    TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                                    IsTaxHeader: item.IsTaxHeader,
                                                                    IsTaxSummary: item.IsTaxSummary,
                                                                    ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                                    VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                                    VAT: AFormatNumber(item.VAT, 2),
                                                                    TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                                    IsWithholdingTax: item.IsWithholdingTax,
                                                                    WithholdingKey: item.WithholdingKey,
                                                                    WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                                    WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                                    GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                                    GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                                                });
                                                                i++;
                                                            });

                                                            _.each(responseDcoument.Document_DebitNote, function (item) {

                                                                if (item.CreditType == 'CA')
                                                                    item.DueDate = GetDatetimeNow();
                                                                else if (item.CreditType == 'CN')
                                                                    item.DueDate = SetDatetimeDay(formatDate(item.DebitNoteDate), item.CreditDay);
                                                                else if (item.CreditType == 'CD')
                                                                    item.DueDate = formatDate(item.DueDate);

                                                                $scope.BillingAdd.BillingCumulativeDetail.push({
                                                                    Sequence: i,
                                                                    DocumentKey: item.DebitNoteKey,
                                                                    DocumentNo: item.DebitNoteNo,
                                                                    DocumentType: 'DN',
                                                                    DocumentCategoryKey: item.InvoiceKey,
                                                                    DocumentDate: formatDate(item.DebitNoteDate),
                                                                    DueDate: item.DueDate,
                                                                    TaxType: item.TaxType,
                                                                    TaxTypeName: item.TaxTypeName,
                                                                    TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                                    IsDiscountHeader: item.IsDiscountHeader,
                                                                    IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                                    DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                                    DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                                    TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                                    IsTaxHeader: item.IsTaxHeader,
                                                                    IsTaxSummary: item.IsTaxSummary,
                                                                    ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                                    VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                                    VAT: AFormatNumber(item.VAT, 2),
                                                                    TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                                    IsWithholdingTax: item.IsWithholdingTax,
                                                                    WithholdingKey: item.WithholdingKey,
                                                                    WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                                    WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                                    GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                                    GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                                                });
                                                                i++;
                                                            });
                                                            $scope.OnChangeDetailCalculate();
                                                            $scope.table.binding = 0;
                                                        }
                                                    }
                                                    else {
                                                        $scope.table.binding = 0;
                                                        showErrorToast(response.data.errormessage);
                                                    }
                                                }
                                            }
                                            catch (err) {
                                                $scope.table.binding = 0;
                                                showErrorToast(err);
                                            }
                                        });


                                    $scope.BillingAdd.DocumentOwner = QueryString('ref_info');

                                    /**  clear values */
                                    $scope.BillingAdd.BillingNo = data[0].data.responsedata;
                                    $scope.BillingAdd.BillingKey = undefined;
                                    $scope.BillingAdd.ReferenceNo = '';
                                    $scope.BillingAdd.ProjectName = '';

                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                                    if (type.length > 0)
                                        $scope.BillingAdd.SelectSale = type[0];


                                    $scope.BillingAdd.BillingDate = GetDatetimeNow();
                                    $scope.BillingAdd.CreditType = 'CD';
                                    $scope.BillingAdd.CreditDay = 0;
                                    /*Credit */
                                    if ($scope.BillingAdd.CreditType == 'CD')
                                        $scope.OnChangeCredit();


                                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.BillingAdd.SaleID; });
                                    if (type.length > 0)
                                        $scope.BillingAdd.SelectSale = type[0];

                                    //$scope.BillingAdd.IsTaxHeader = false; // ภาษีรายการ
                                    //$scope.BillingAdd.IsDiscountHeader = false; // ส่วนลดรายการ
                                    //$scope.BillingAdd.IsTaxSummary = false; // หักภาษี 7 % รวม
                                    //$scope.BillingAdd.IsWithholdingTax = false;// หักภาษี ณ ที่จ่าย

                                    $scope.BillingAdd.TotalAmount = AFormatNumber(0, 2);
                                    $scope.BillingAdd.DiscountPercent = AFormatNumber(0, 2);
                                    $scope.BillingAdd.DiscountAmount = AFormatNumber(0, 2);
                                    $scope.BillingAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                                    $scope.BillingAdd.ExemptAmount = AFormatNumber(0, 2);
                                    $scope.BillingAdd.VatableAmount = AFormatNumber(0, 2);
                                    $scope.BillingAdd.VAT = AFormatNumber(0, 2);
                                    $scope.BillingAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);
                                    $scope.BillingAdd.GrandAmount = AFormatNumber(0, 2);

                                    $scope.BillingAdd.Signature = $scope.BillingAdd.Signature == '1' ? true : false;

                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                else {
                    /******* Initail ********/
                    $scope.SelectInvoice = '1';
                    $scope.BillingAdd.BillingCumulativeDetail = [];
                    $scope.BillingAdd.DueDate = $scope.BillingAdd.BillingDate = GetDatetimeNow();
                    $scope.BillingAdd.CreditType = 'CD';
                    $scope.BillingAdd.TaxType = 'N';
                    $scope.BillingAdd.IsTaxSummary = true; // หักภาษี 7 % รวม
                    $scope.BillingAdd.IsDiscountHeaderType = '%'; // ส่วนลดแยกรายการแบบ Percent
                    $scope.OnChangeCreditType();

                    var type = $scope.Parameter.Param_Employee.filter(function (item) { return item.UID == $scope.UserProfile.UID; });
                    if (type.length > 0)
                        $scope.BillingAdd.SelectSale = type[0];

                    $scope.document = [];
                    $scope.document.len = 0;
                    $scope.document.total = '0.00';

                    $('#modalinvoice-list').modal('show');

                    $scope.table.binding = 0;
                }
            }
            catch (err) {
                showErrorToast(err);
            }
        });
    };

    $scope.OnClickCancel = function () {
        if (QueryString('ref_info') == 'INV')
            window.location.href = baseURL + "DocumentSell/Invoice";
        else if (QueryString('ref_info') == 'CN')
            window.location.href = baseURL + "DocumentSell/CreditNote";
        else if (QueryString('ref_info') == 'DN')
            window.location.href = baseURL + "DocumentSell/DebitNote";
        else if (QueryString('ref_report') != undefined)
            window.location.href = baseURL + "Report/" + QueryString('ref_report');
        else
            window.location.href = baseURL + "DocumentSell/BillingNote";
    };

    $scope.OnDisabled = function () {
        if ($scope.BillingAdd.BillingStatus == undefined || $scope.BillingAdd.BillingStatus == '1')
            return false;
        else
            return true;
    };

    $scope.OnClickReportContent = function (type) {
        $scope.Print = "1";
        $scope.document = [];
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $scope.OnClickSave();
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'BL',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                                $scope.Print = "0";
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {

        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'BL',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    /******************Due Date *********** */
    $scope.OnChangeCreditType = function () {
        if ($scope.BillingAdd.CreditType == 'CD') {
            $scope.OnChangeCredit();
        }
        if ($scope.BillingAdd.CreditType == 'CA') { $scope.BillingAdd.CreditDay = 0; }
    };
    $scope.OnChangeCredit = function () {

        if ($scope.BillingAdd.CreditType == 'CD') {
            if ($scope.BillingAdd.CreditDay == undefined || $scope.BillingAdd.CreditDay == '') {
                $scope.BillingAdd.BillingDate = $scope.BillingAdd.DueDate = GetDatetimeNow();
                $scope.BillingAdd.CreditDay = 0;
            }
            else {
                $scope.BillingAdd.DueDate = SetDatetimeDay($scope.BillingAdd.BillingDate, $scope.BillingAdd.CreditDay);
            }
        }
    };
    $scope.OnChangeDateDue = function () {
        if ($scope.BillingAdd.CreditType == 'CD') {
            if ($scope.BillingAdd.DueDate == undefined || $scope.BillingAdd.DueDate == '') {
                $scope.BillingAdd.BillingDate = GetDatetimeNow();
                $scope.BillingAdd.CreditDay = 0;
            }
            else {
                var datedue = $scope.BillingAdd.DueDate;
                var days = DatetimeLenof($scope.BillingAdd.BillingDate, $scope.BillingAdd.DueDate);
                if (days < 0) {
                    $scope.BillingAdd.BillingDate = SetDatetimeDay($scope.BillingAdd.DueDate, days);
                    $scope.BillingAdd.DueDate = datedue;
                    $scope.BillingAdd.CreditDay = (days * (-1));
                }
                else
                    $scope.BillingAdd.CreditDay = days;
            }
        }
    };
    $scope.OnChangeBillingDate = function () {
        if ($scope.BillingAdd.CreditType == 'CD') {
            if ($scope.BillingAdd.BillingDate == undefined || $scope.BillingAdd.BillingDate == '') {
                $scope.BillingAdd.DueDate = $scope.BillingAdd.BillingDate = GetDatetimeNow();
                $scope.BillingAdd.CreditDay = 0;
            }
            else {
                var days = $scope.BillingAdd.CreditDay == undefined || $scope.BillingAdd.CreditDay == "" ? 0 : $scope.BillingAdd.CreditDay;
                $scope.BillingAdd.DueDate = SetDatetimeDay($scope.BillingAdd.BillingDate, $scope.BillingAdd.CreditDay);
            }
        }
        var prefix = { asofdate: $scope.BillingAdd.BillingDate, documenttype: headerdoc };
        var qq = $q.all([paramService.getDocumentNo(prefix)]).then(function (data) {
            /****** GetDocument NO ******/
            if (data[0] != undefined && data[0] != "") {
                if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BillingAdd.BillingNo = data[0].data.responsedata; }
                else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
            }
        });
    };

    /*********** Header Setting Discount , Tax**************/
    $scope.OnChangeIsTaxHeader = function () {
        if ($scope.BillingAdd.IsTaxHeader) {
            $scope.BillingAdd.IsDiscountHeader = true;
        }
        $scope.OnChangeDetailCalculate();
    };
    $scope.OnChangeIsDiscountHeader = function () {
        $scope.BillingAdd.DiscountPercent = '0';
        $scope.BillingAdd.DiscountAmount = '0';
        $scope.OnClickDiscountType($scope.BillingAdd.IsDiscountHeaderType);
    };
    /******************* Detail And Calculate *************************/
    $scope.OnClickProductFromInvoice = function () {
        try {
            _.each($scope.BillingAdd.InvoiceDetail, function (item) {
                item.Quantity = AFormatNumber(item.Quantity, 2);
                item.Price = AFormatNumber(item.Price, 2);
            });
            $('#modalinvoice-product').modal('show');
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickcheckedAll = function () {
        try {
            _.each($scope.BillingAdd.InvoiceDetail, function (item) {
                item.Mapping = $scope.CheckingAll;
            });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickTableTR = function (index) {
        if ($scope.BillingAdd.InvoiceDetail[index].Mapping)
            $scope.BillingAdd.InvoiceDetail[index].Mapping = false;
        else
            $scope.BillingAdd.InvoiceDetail[index].Mapping = true;
    };

    $scope.OnClickProductSelectInvoice = function () {
        try {

            _.each($scope.BillingAdd.InvoiceDetail, function (item) {
                if (item.Mapping) {
                    var insert = _.where($scope.BillingAdd.BillingCumulativeDetail, { InvoiceDetailKey: item.InvoiceDetailKey })[0];

                    if (insert == null) {
                        item.Sequence = i;
                        var values = angular.copy(item);
                        $scope.BillingAdd.BillingCumulativeDetail.push(values);
                    }
                }
                else {
                    var del = _.where($scope.BillingAdd.BillingCumulativeDetail, { InvoiceDetailKey: item.InvoiceDetailKey })[0];
                    if (del != null) {
                        var a = $scope.BillingAdd.BillingCumulativeDetail.indexOf(del);
                        $scope.BillingAdd.BillingCumulativeDetail.splice(a, 1);
                    }

                }
            });

            var i = 1;
            _.each($scope.BillingAdd.BillingCumulativeDetail, function (item) {
                item.Sequence = i;
                i++;
            });

            $scope.BillingAdd.BillingCumulativeDetail = _.sortBy($scope.BillingAdd.BillingCumulativeDetail, 'Sequence');
            $scope.OnChangeDetailCalculate();
            $scope.FirstChooseProduct = '0';
            $('#modalinvoice-product').modal('hide');
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickProductCancelInvoice = function () {
        if ($scope.FirstChooseProduct == '2')
            window.location.href = baseURL + "DocumentSell/Billing";
        else
            $('#modalinvoice-product').modal('hide');
    };

    $scope.OnClickDocumentDetailDelete = function (categorykey) {
        try {
            var invoice = _.where($scope.BillingAdd.BillingCumulativeDetail, { DocumentCategoryKey: categorykey });
            _.each(invoice, function (del) {
                var a = $scope.BillingAdd.BillingCumulativeDetail.indexOf(del);
                $scope.BillingAdd.BillingCumulativeDetail.splice(a, 1);
            });
            $scope.OnChangeDetailCalculate();
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnChangeDetailCalculate = function () {
        try {
            /******* หาค่ารวม Summary ***********/

            var sumtotalafterdiscount = 0, sumvat = 0, sumexemptamount = 0, sumvatableamount = 0, sumgrandamount = 0, sumtotalbeforevatamount = 0;

            if ($scope.BillingAdd.BillingCumulativeDetail != undefined && $scope.BillingAdd.BillingCumulativeDetail.length > 0) {
                _.each($scope.BillingAdd.BillingCumulativeDetail, function (item) {

                    totalafterdiscount = item.TotalAfterDiscountAmount == undefined || item.TotalAfterDiscountAmount === '' ? 0 : detectFloat(item.TotalAfterDiscountAmount); // ราคาจากหน้าจอ
                    vat = item.VAT == undefined || item.VAT === '' ? 0 : detectFloat(item.VAT); // ส่วนลดคำนวณ
                    exemptamount = item.ExemptAmount == undefined || item.ExemptAmount === '' ? 0 : detectFloat(item.ExemptAmount); // จำนวน จากหน้าจอ
                    vatableamount = item.VatableAmount == undefined || item.VatableAmount === '' ? 0 : detectFloat(item.VatableAmount); // จำนวน จากหน้าจอ
                    grandamount = item.GrandAmount == undefined || item.GrandAmount === '' ? 0 : detectFloat(item.GrandAmount); // จำนวน จากหน้าจอ
                    totalbeforevatamount = item.TotalBeforeVatAmount == undefined || item.TotalBeforeVatAmount === '' ? 0 : detectFloat(item.TotalBeforeVatAmount); // จำนวน จากหน้าจอ

                    if (item.DocumentType == 'CN') {
                        totalafterdiscount = totalafterdiscount * (-1);
                        vat = vat * (-1);
                        exemptamount = exemptamount * (-1);
                        vatableamount = vatableamount * (-1);
                        grandamount = grandamount * (-1);
                    }

                    // กร๊ไม่มี Vat 7% ของรวม


                    if (item.IsTaxHeader == '1') { // หักภาษี แบบทีละรายการ
                        sumexemptamount = sumexemptamount + exemptamount; // ยกเว้น
                        sumvatableamount = sumvatableamount + vatableamount; //คำนวณภาษี
                        sumvat = sumvat + vat;
                    }
                    else {
                        if (item.IsTaxHeader == '0' && item.IsTaxSummary == '0')
                            sumexemptamount = sumexemptamount + totalafterdiscount; // ยกเว้น
                        else
                            sumvatableamount = sumvatableamount + totalafterdiscount;
                        sumvat = sumvat + vat;
                    }

                    sumtotalbeforevatamount = sumtotalbeforevatamount + totalbeforevatamount;
                    sumtotalafterdiscount = sumtotalafterdiscount + totalafterdiscount;
                    sumgrandamount = sumgrandamount + grandamount;
                });

                $scope.BillingAdd.TotalQTY = $scope.BillingAdd.BillingCumulativeDetail.length;
                $scope.BillingAdd.TotalAmount = AFormatNumber(sumtotalafterdiscount, 2);
                $scope.BillingAdd.DiscountAmount = AFormatNumber(0, 2);
                $scope.BillingAdd.TotalAfterDiscountAmount = AFormatNumber(sumtotalafterdiscount, 2);
                $scope.BillingAdd.ExemptAmount = AFormatNumber(sumexemptamount, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.BillingAdd.VatableAmount = AFormatNumber(sumvatableamount, 2);//มูลค่าที่คำนวณภาษี
                $scope.BillingAdd.VAT = AFormatNumber(sumvat, 2);//ภาษีมูลค่าเพิ่ม
                $scope.BillingAdd.TotalBeforeVatAmount = AFormatNumber(totalbeforevatamount, 2);//จำนวนเงินรวมภาษี
                $scope.BillingAdd.GrandAmount = AFormatNumber(sumgrandamount, 2);//
                $scope.BillingAdd.WithholdingAmount = AFormatNumber(0, 2);//
                $scope.BillingAdd.GrandAmountAfterWithholding = AFormatNumber(0, 2);//
            }
            else {
                $scope.BillingAdd.TotalQTY = 0;
                $scope.BillingAdd.TotalAmount = AFormatNumber(0, 2);
                $scope.BillingAdd.DiscountPercent = AFormatNumber(0, 2);
                $scope.BillingAdd.DiscountAmount = AFormatNumber(0, 2);
                $scope.BillingAdd.TotalAfterDiscountAmount = AFormatNumber(0, 2);
                $scope.BillingAdd.ExemptAmount = AFormatNumber(0, 2);//มูลค่าที่ไม่มี/ยกเว้นภาษี
                $scope.BillingAdd.VatableAmount = AFormatNumber(0, 2);//มูลค่าที่คำนวณภาษี
                $scope.BillingAdd.VAT = AFormatNumber(0, 2);//ภาษีมูลค่าเพิ่ม
                $scope.BillingAdd.TotalBeforeVatAmount = AFormatNumber(0, 2);//จำนวนเงินรวมภาษี
                $scope.BillingAdd.GrandAmount = AFormatNumber(0, 2);//
                $scope.BillingAdd.WithholdingAmount = AFormatNumber(0, 2);//
                $scope.BillingAdd.GrandAmountAfterWithholding = AFormatNumber(0, 2);//
            }


        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickSave = function (exit) {
        try {
            var credittype;
            if ($scope.BillingAdd.CreditType == 'CD')
                credittype = 'เครดิต(วัน)';
            else if ($scope.BillingAdd.CreditType == 'CA')
                credittype = 'เงินสด';
            else if ($scope.BillingAdd.CreditType == 'CN')
                credittype = 'เครดิต (ไม่แสดงวันที่)';

            var data = {
                BillingKey: $scope.BillingAdd.BillingKey,
                BillingNo: $scope.BillingAdd.BillingNo,
                BillingStatus: $scope.BillingAdd.BillingStatus,
                BillingStatusName: $scope.BillingAdd.BillingStatusName,
                BillingType: 'CBL',
                BillingTypeName: 'ใบวางบิลรวม',
                CustomerKey: $scope.BillingAdd.CustomerKey,
                CustomerName: $scope.BillingAdd.CustomerName,
                CustomerTaxID: $scope.BillingAdd.CustomerTaxID,
                CustomerContactName: $scope.BillingAdd.CustomerContactName,
                CustomerContactPhone: $scope.BillingAdd.CustomerContactPhone,
                CustomerContactEmail: $scope.BillingAdd.CustomerContactEmail,
                CustomerAddress: $scope.BillingAdd.CustomerAddress,
                CustomerBranch: $scope.BillingAdd.CustomerBranch,
                BillingDate: ToJsonDate2($scope.BillingAdd.BillingDate),
                CreditType: $scope.BillingAdd.CreditType,
                CreditTypeName: credittype,
                CreditDay: $scope.BillingAdd.CreditDay,
                DueDate: $scope.BillingAdd.CreditType == 'CD' ? ToJsonDate2($scope.BillingAdd.DueDate) : null,
                SaleID: $scope.BillingAdd.SelectSale.UID,
                SaleName: $scope.BillingAdd.SelectSale.EmployeeName,
                ProjectName: $scope.BillingAdd.ProjectName,
                ReferenceNo: $scope.BillingAdd.ReferenceNo,
                TaxType: $scope.BillingAdd.TaxType,
                TaxTypeName: $scope.BillingAdd.TaxType == 'N' ? 'ราคาไม่รวมภาษี' : $scope.BillingAdd.TaxType == 'V' ? 'ราคารวมภาษี' : '',
                TotalAmount: ConvertToDecimal($scope.BillingAdd.TotalAmount),
                IsDiscountHeader: '0',
                IsDiscountHeaderType: $scope.BillingAdd.IsDiscountHeaderType,
                DiscountPercent: $scope.BillingAdd.DiscountPercent,
                DiscountAmount: $scope.BillingAdd.DiscountAmount,
                TotalAfterDiscountAmount: $scope.BillingAdd.TotalAfterDiscountAmount,
                IsTaxHeader: '0',
                IsTaxSummary: '0',
                ExemptAmount: $scope.BillingAdd.ExemptAmount,
                VatableAmount: $scope.BillingAdd.VatableAmount,
                VAT: $scope.BillingAdd.VAT,
                TotalBeforeVatAmount: $scope.BillingAdd.TotalBeforeVatAmount,
                IsWithholdingTax: '0',
                WithholdingKey: undefined,
                WithholdingRate: undefined,
                WithholdingAmount: undefined,
                GrandAmountAfterWithholding: $scope.BillingAdd.IsWithholdingTax ? $scope.BillingAdd.GrandAmountAfterWithholding : undefined,
                GrandAmount: $scope.BillingAdd.GrandAmount,
                Remark: $scope.BillingAdd.Remark,
                Signature: $scope.BillingAdd.Signature ? '1' : '0',
                Noted: $scope.BillingAdd.Noted,
                DocumentKey: $scope.BillingAdd.DocumentKey,
                DocumentNo: $scope.BillingAdd.DocumentNo,
                DocumentOwner: $scope.BillingAdd.DocumentOwner
            };
            data.BillingCumulativeDetail = [];
            _.each($scope.BillingAdd.BillingCumulativeDetail, function (item) {

                var tmpQT = {
                    Sequence: parseInt(item.Sequence),
                    DocumentKey: item.DocumentKey,
                    DocumentNo: item.DocumentNo,
                    DocumentType: item.DocumentType,
                    DocumentCategoryKey: item.DocumentCategoryKey,
                    DocumentDate: ToJsonDate2(item.DocumentDate),
                    DueDate: ToJsonDate2(item.DueDate),
                    Unit: item.Unit,
                    TaxType: item.TaxType,
                    TaxTypeName: item.TaxTypeName,
                    TotalAmount: ConvertToDecimal(item.TotalAmount),
                    IsDiscountHeader: item.IsDiscountHeader,
                    IsDiscountHeaderType: item.IsDiscountHeaderType,
                    DiscountPercent: item.DiscountPercent,
                    DiscountAmount: ConvertToDecimal(item.DiscountAmount),
                    TotalAfterDiscountAmount: ConvertToDecimal(item.TotalAfterDiscountAmount),
                    IsTaxHeader: item.IsTaxHeader,
                    IsTaxSummary: item.IsTaxSummary,
                    ExemptAmount: ConvertToDecimal(item.ExemptAmount),
                    VatableAmount: ConvertToDecimal(item.VatableAmount),
                    VAT: ConvertToDecimal(item.VAT),
                    TotalBeforeVatAmount: ConvertToDecimal(item.TotalBeforeVatAmount),
                    IsWithholdingTax: item.IsWithholdingTax,
                    WithholdingKey: item.WithholdingKey,
                    WithholdingRate: ConvertToDecimal(item.WithholdingRate),
                    WithholdingAmount: ConvertToDecimal(item.WithholdingAmount),
                    GrandAmountAfterWithholding: ConvertToDecimal(item.GrandAmountAfterWithholding),
                    GrandAmount: ConvertToDecimal(item.GrandAmount)
                };


                data.BillingCumulativeDetail.push(tmpQT);
            });

            $scope.table.binding = 1;
            $http.post(baseURL + "DocumentSell/PostBusinessBillingCumulative", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                var responseBilling = response.data.responsedata;
                                if (responseBilling != undefined) {

                                    if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'INV' && exit == 1)
                                        window.location.href = baseURL + "DocumentSell/Invoice";
                                    else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'CN' && exit == 1)
                                        window.location.href = baseURL + "DocumentSell/CreditNote";
                                    else if (QueryString('ref_key') != undefined && QueryString('ref_info') == 'DN' && exit == 1)
                                        window.location.href = baseURL + "DocumentSell/DebitNote";
                                    else if (QueryString('ref_key') != undefined && (QueryString('ref_info') == 'INV' || QueryString('ref_info') == 'CN' || QueryString('ref_info') == 'DN'))
                                        window.location.href = baseURL + "DocumentSell/BillingNoteCumulativeAdd?BillingKey=" + responseBilling.BillingKey;
                                    else if (exit == 1)
                                        window.location.href = baseURL + "DocumentSell/BillingNote";
                                    else {
                                        $scope.BillingAdd.BillingKey = responseBilling.BillingKey;
                                        $scope.BillingAdd.BillingStatus = responseBilling.BillingStatus;
                                        $scope.BillingAdd.BillingStatusName = responseBilling.BillingStatusName;
                                        $scope.BillingAdd.CustomerKey = responseBilling.CustomerKey;
                                        $scope.BillingAdd.DocumentKey = undefined;
                                        $scope.BillingAdd.DocumentNo = undefined;
                                        $scope.BillingAdd.DocumentOwner = undefined;

                                        $scope.table.binding = 0;
                                        showSuccessToast();

                                        if ($scope.BillingAdd.BillingKey != undefined && $scope.Print == "1") {
                                            $scope.document.DocKey = $scope.BillingAdd.BillingKey;
                                            $scope.document.DocNo = $scope.BillingAdd.BillingNo;
                                            $scope.document.original = $scope.document.copy = true;
                                            $('#modal-content-report').modal('show');
                                        }
                                        $scope.Print = "0";
                                    }
                                }
                            }
                            else {
                                $scope.table.binding = 0;
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        $scope.table.binding = 0;
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    /**************Select ContactBook **************/
    $scope.OnClickContactPopup = function () {
        $scope.SearchContact.InputFilter = [];
        $scope.contactcurrentPage = 0;
        $scope.contact = [];
        $scope.contact.binding = 0;
        $scope.document = [];
        $scope.contactretpage = [];
        $scope.contactrange();
        $('#modalcontact-list').modal('show');
    };

    $scope.OnClearContact = function () {
        $scope.BillingAdd.CustomerKey = $scope.BillingAdd.CustomerName = $scope.BillingAdd.CustomerTaxID =
            $scope.BillingAdd.CustomerContactName = $scope.BillingAdd.CustomerContactPhone = $scope.BillingAdd.CustomerContactEmail =
            $scope.BillingAdd.CustomerAddress = $scope.BillingAdd.CustomerBranch = $scope.BillingAdd.CreditDay = undefined;
    };

    $scope.OnClickContactSearch = function () {
        $scope.Parameter.Param_Contact = contactService.filterContact($scope.SearchContact.InputFilter, $scope.Parameter.Param_Contact, $scope.Parameter.Param_ContactMain);
        if ($scope.Parameter.Param_Contact == undefined)
            $scope.Parameter.Param_Contact = [];
    };

    $scope.OnClickContactSelection = function (contactkey) {
        try {
            var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: contactkey })[0];
            $('#modalcontact-list').modal('hide');
            $scope.contact.binding = 1;
            $scope.document = [];
            $scope.document.binding = 1;
            $scope.document.len = 0;
            $scope.document.total = '0.00';
            $scope.document.ContactKey = contactkey;
            $scope.document.ContactName = contact.BusinessName;
            $http.get(baseURL + "DocumentSell/GetBusinessInvoiceByContactBillingCumative?contactkey=" + contactkey + "&billingkey=" + $scope.BillingAdd.BillingKey + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            $scope.Parameter.Param_Document = response.data.responsedata;

                            var grandtotal = 0;
                            _.each($scope.Parameter.Param_Document, function (item) {

                                var setture = _.where($scope.BillingAdd.BillingCumulativeDetail, { DocumentKey: item.InvoiceKey })[0];
                                if (setture)
                                    item.Mapping = true;
                                else
                                    item.Mapping = false;

                                if (item.CreditType == 'CD')
                                    item.DueDate = formatDate(item.DueDate);
                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                            });

                            $scope.documentcurrentPage = 0;
                            $scope.document.binding = 0;
                            $scope.documentretpage = [];
                            $scope.documentrange();

                        }
                        else if (response.data.responsecode == '400') {
                            $scope.document.binding = 0;
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            $scope.document.binding = 0;
            showErrorToast(err);
        }
    };

    $scope.OnClickContactAdd = function () {
        $scope.BusinessContactAdd = [];
        $scope.BusinessContactAdd.BusinessType = 'C';
        $scope.BusinessContactAdd.ContactType1 = true;
        $scope.BusinessContactAdd.ContactType2 = false;
        $scope.BusinessContactAdd.BranchType = "H";
        $scope.BusinessContactAdd.AccountType = 'S';

        var type = $scope.Parameter.Param_Quotation.Param_Bank.filter(function (item) { return item.ID == undefined; });
        if (type.length > 0)
            $scope.BusinessContactAdd.SelectBank = type[0];

        $('#modalcontact-add').modal('show');
    };

    $scope.OnClickContactSave = function () {
        try {
            if ($scope.BusinessContactAdd.BusinessName == undefined || $scope.BusinessContactAdd.BusinessName == "")
                throw "กรุณาระบุชื่อธุรกิจ";
            else if ($scope.BusinessContactAdd.ContactType1 == false && $scope.BusinessContactAdd.ContactType2 == false)
                throw "กรุณาระบุประเภท";
            else if ($scope.BusinessContactAdd.ContactEmail != undefined && $scope.BusinessContactAdd.ContactEmail != "" && !ValidateEmail($scope.BusinessContactAdd.ContactEmail))
                throw "กรุณาแก้ไขอีเมล เนื่องจากรูปแบบอีเมลไม่ถูกต้อง";
            else {
                var qq = $q.all([contactService.postNewContact($scope.BusinessContactAdd)]).then(function (data) {
                    try {
                        if (data[0] != undefined && data[0] != "") {
                            if (data[0].data.responsecode == 200) {

                                var contact = data[0].data.responsedata;
                                if (contact != undefined) {
                                    $scope.OnClearContact();
                                    $scope.BillingAdd.CustomerKey = contact.ContactKey;
                                    $scope.BillingAdd.CustomerName = contact.BusinessName;
                                    $scope.BillingAdd.CustomerTaxID = contact.TaxID;
                                    $scope.BillingAdd.CustomerContactName = contact.ContactName;
                                    $scope.BillingAdd.CustomerContactPhone = contact.ContactMobile;
                                    $scope.BillingAdd.CustomerContactEmail = contact.ContactEmail;
                                    $scope.BillingAdd.CustomerAddress = contact.Address;
                                    $scope.BillingAdd.CustomerBranch = contact.BranchName;
                                    $scope.BillingAdd.CreditDay = contact.CreditDate;
                                    $scope.OnChangeCredit();
                                }
                                $('#modalcontact-add').modal('hide');
                                showSuccessToast();

                                var q1 = $q.all([paramService.getCustomer()]).then(function (data) {
                                    if (data[0] != undefined && data[0] != "") {
                                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                                            $scope.SearchContact = [];
                                            $scope.Parameter.Param_Contact = [];
                                            $scope.Parameter.Param_ContactMain = [];
                                            $scope.Parameter.Param_ContactMain = $scope.Parameter.Param_Contact = data[0].data.responsedata;
                                        }
                                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                                    }
                                });

                            }
                            else {
                                showErrorToast(data[0].data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(response.data.errormessage);
                    }
                });
            }
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.contactcurrentPage = 0;

    $scope.contactLimitFirst = 0;
    $scope.contactLimitPage = 5;
    $scope.contactitemsPerPage = 10;

    $scope.contactpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Contact.length / $scope.contactitemsPerPage) - 1;
    };

    $scope.contactrange = function () {
        $scope.contactitemsCount = $scope.Parameter.Param_Contact.length;
        $scope.contactpageshow = $scope.contactpageCount() > $scope.contactLimitPage && $scope.contactpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.contactpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.contactpageshowdata = 1;
        $scope.contactretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.contactpcontactPage = function () {
        if ($scope.contactcurrentPage > 0) {
            $scope.contactcurrentPage--;
        }

        if ($scope.contactcurrentPage < $scope.contactLimitFirst && $scope.contactcurrentPage >= 1) {
            $scope.contactLimitFirst = $scope.contactLimitFirst - 5;
            $scope.contactLimitPage = $scope.contactLimitPage - 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }
    };

    $scope.contactpcontactPageDisabled = function () {
        return $scope.contactcurrentPage === 0 ? "disabled" : "";
    };

    $scope.contactnextPage = function () {
        if ($scope.contactcurrentPage < $scope.contactpageCount()) {
            $scope.contactcurrentPage++;
        }

        if ($scope.contactcurrentPage >= $scope.contactLimitPage && $scope.contactcurrentPage <= $scope.contactpageCount()) {
            $scope.contactLimitFirst = $scope.contactLimitFirst + 5;
            $scope.contactLimitPage = $scope.contactLimitPage + 5;
            for (var i = 1; i <= $scope.contactretpage.length; i++) {
                if (i >= $scope.contactLimitFirst && i <= $scope.contactLimitPage) {
                    $scope.contactretpage[i - 1].show = 1;
                }
                else
                    $scope.contactretpage[i - 1].show = 0;
            }
        }

    };

    $scope.contactnextPageDisabled = function () {
        return $scope.contactcurrentPage === $scope.contactpageCount() ? "disabled" : "";
    };

    $scope.contactsetPage = function (n) {
        $scope.contactcurrentPage = n;
    };

    /**************Select Document **************/
    $scope.OnClickcheckedAll = function () {
        try {
            $scope.document.total = '0.00';
            $scope.document.len = '0';
            var i = 0;
            _.each($scope.Parameter.Param_Document, function (item) {
                item.Mapping = $scope.CheckingAll;

                if ($scope.CheckingAll)
                $scope.OnChangeMapping(i);
                i++;
            });

            
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickCloseDocument = function () {
        if ($scope.SelectInvoice == '1')
            window.location.href = baseURL + "DocumentSell/BillingNote";
        else
            $('#modalinvoice-list').modal('hide');
    };

    $scope.OnClickDocumentPopup = function () {
        try {
            $('#modalinvoice-list').modal('show');
            $scope.document = [];
            $scope.document.len = 0;
            $scope.document.total = '0.00';
            $scope.document.binding = 1;
            $http.get(baseURL + "DocumentSell/GetBusinessInvoiceByContactBillingCumative?contactkey=" + $scope.BillingAdd.CustomerKey + "&billingkey=" + $scope.BillingAdd.BillingKey + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {

                            $scope.Parameter.Param_Document = response.data.responsedata;
                            var i = 0;
                            var grandtotal = 0;
                            _.each($scope.Parameter.Param_Document, function (item) {

                                var setture = _.where($scope.BillingAdd.BillingCumulativeDetail, { DocumentKey: item.InvoiceKey, DocumentType: 'INV' })[0];
                                if (setture)
                                    item.Mapping = true;
                                else
                                    item.Mapping = false;

                                if (item.CreditType == 'CD')
                                    item.DueDate = formatDate(item.DueDate);
                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);

                                if (item.Mapping)
                                $scope.OnChangeMapping(i);
                                i++;
                            });

                            $scope.document.ContactName = $scope.BillingAdd.CustomerName;

                            $scope.documentcurrentPage = 0;
                            $scope.document.binding = 0;
                            $scope.documentretpage = [];
                            $scope.documentrange();

                        }
                        else if (response.data.responsecode == '400') {
                            $scope.document.binding = 0;
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            $scope.document.binding = 0;
            showErrorToast(err);
        }
    };

    $scope.OnClickDocumentSelection = function () {
        try {
            var docuno;
            var step = true;
            var values = [];

            _.each($scope.Parameter.Param_Document, function (invoicedoc) {
                if (invoicedoc.Mapping) {
                    var checkdup = _.where($scope.BillingAdd.BillingCumulativeDetail, { DocumentKey: invoicedoc.InvoiceKey })[0];
                    if (invoicedoc.BillingKey != undefined && checkdup == undefined) {
                        docuno = invoicedoc.InvoiceNo;
                        step = false;
                    }
                    else {
                        var dock = { documentkey: invoicedoc.InvoiceKey, documentype: 'INV' };
                        values.push(dock);
                    }
                }
            });

            if (!step)
                showWariningToast(docuno + " เคยถูกสร้างเป็นใบวางบิลรวมหรือใบเสร็จรวมแล้ว");
            else if (values.length > 0) {
                $scope.document.binding = 1;
                $http.post(baseURL + "DocumentSell/GetBusinessCumulative", values, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    var responseDcoument = response.data.responsedata;
                                    if (responseDcoument != undefined) {

                                        var i = 1;
                                        $scope.BillingAdd.BillingCumulativeDetail = [];
                                        _.each(responseDcoument.Document_Invoice, function (item) {

                                            if (item.CreditType == 'CA')
                                                item.DueDate = GetDatetimeNow();
                                            else if (item.CreditType == 'CN')
                                                item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                            else if (item.CreditType == 'CD')
                                                item.DueDate = formatDate(item.DueDate);

                                            $scope.BillingAdd.BillingCumulativeDetail.push({
                                                Sequence: i,
                                                DocumentKey: item.InvoiceKey,
                                                DocumentNo: item.InvoiceNo,
                                                DocumentType: 'INV',
                                                DocumentCategoryKey: item.InvoiceKey,
                                                DocumentDate: formatDate(item.InvoiceDate),
                                                DueDate: item.DueDate,
                                                TaxType: item.TaxType,
                                                TaxTypeName: item.TaxTypeName,
                                                TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                IsDiscountHeader: item.IsDiscountHeader,
                                                IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                IsTaxHeader: item.IsTaxHeader,
                                                IsTaxSummary: item.IsTaxSummary,
                                                ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                VAT: AFormatNumber(item.VAT, 2),
                                                TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                IsWithholdingTax: item.IsWithholdingTax,
                                                WithholdingKey: item.WithholdingKey,
                                                WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                            });
                                            i++;
                                        });

                                        _.each(responseDcoument.Document_CreditNote, function (item) {

                                            if (item.CreditType == 'CA')
                                                item.DueDate = GetDatetimeNow();
                                            else if (item.CreditType == 'CN')
                                                item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                            else if (item.CreditType == 'CD')
                                                item.DueDate = formatDate(item.DueDate);


                                            $scope.BillingAdd.BillingCumulativeDetail.push({
                                                Sequence: i,
                                                DocumentKey: item.CreditNoteKey,
                                                DocumentNo: item.CreditNoteNo,
                                                DocumentType: 'CN',
                                                DocumentCategoryKey: item.InvoiceKey,
                                                DocumentDate: formatDate(item.CreditNoteDate),
                                                DueDate: item.DueDate,
                                                TaxType: item.TaxType,
                                                TaxTypeName: item.TaxTypeName,
                                                TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                IsDiscountHeader: item.IsDiscountHeader,
                                                IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                IsTaxHeader: item.IsTaxHeader,
                                                IsTaxSummary: item.IsTaxSummary,
                                                ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                VAT: AFormatNumber(item.VAT, 2),
                                                TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                IsWithholdingTax: item.IsWithholdingTax,
                                                WithholdingKey: item.WithholdingKey,
                                                WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                            });
                                            i++;
                                        });

                                        _.each(responseDcoument.Document_DebitNote, function (item) {

                                            if (item.CreditType == 'CA')
                                                item.DueDate = GetDatetimeNow();
                                            else if (item.CreditType == 'CN')
                                                item.DueDate = SetDatetimeDay(formatDate(item.InvoiceDate), item.CreditDay);
                                            else if (item.CreditType == 'CD')
                                                item.DueDate = formatDate(item.DueDate);


                                            $scope.BillingAdd.BillingCumulativeDetail.push({
                                                Sequence: i,
                                                DocumentKey: item.DebitNoteKey,
                                                DocumentNo: item.DebitNoteNo,
                                                DocumentType: 'DN',
                                                DocumentCategoryKey: item.InvoiceKey,
                                                DocumentDate: formatDate(item.DebitNoteDate),
                                                DueDate:item.DueDate,
                                                TaxType: item.TaxType,
                                                TaxTypeName: item.TaxTypeName,
                                                TotalAmount: AFormatNumber(item.TotalAmount, 2),
                                                IsDiscountHeader: item.IsDiscountHeader,
                                                IsDiscountHeaderType: item.IsDiscountHeaderType,
                                                DiscountPercent: AFormatNumber(item.DiscountPercent, 2),
                                                DiscountAmount: AFormatNumber(item.DiscountAmount, 2),
                                                TotalAfterDiscountAmount: item.TaxType == 'V' ? AFormatNumber(item.TotalBeforeVatAmount, 2) : AFormatNumber(item.TotalAfterDiscountAmount, 2),
                                                IsTaxHeader: item.IsTaxHeader,
                                                IsTaxSummary: item.IsTaxSummary,
                                                ExemptAmount: AFormatNumber(item.ExemptAmount, 2),
                                                VatableAmount: AFormatNumber(item.VatableAmount, 2),
                                                VAT: AFormatNumber(item.VAT, 2),
                                                TotalBeforeVatAmount: AFormatNumber(item.TotalBeforeVatAmount, 2),
                                                IsWithholdingTax: item.IsWithholdingTax,
                                                WithholdingKey: item.WithholdingKey,
                                                WithholdingRate: AFormatNumber(item.WithholdingRate, 2),
                                                WithholdingAmount: AFormatNumber(item.WithholdingAmount, 2),
                                                GrandAmountAfterWithholding: AFormatNumber(item.GrandAmountAfterWithholding, 2),
                                                GrandAmount: AFormatNumber(item.GrandAmount, 2)
                                            });
                                            i++;
                                        });
                                        $scope.OnChangeDetailCalculate();

                                        if ($scope.SelectInvoice == '1') {

                                            var contact = _.where($scope.Parameter.Param_Contact, { ContactKey: $scope.document.ContactKey })[0];
                                            if (contact != undefined) {
                                                $scope.OnClearContact();
                                                $scope.BillingAdd.CustomerKey = contact.ContactKey;
                                                $scope.BillingAdd.CustomerName = contact.BusinessName;
                                                $scope.BillingAdd.CustomerTaxID = contact.TaxID;
                                                $scope.BillingAdd.CustomerContactName = contact.ContactName;
                                                $scope.BillingAdd.CustomerContactPhone = contact.ContactMobile;
                                                $scope.BillingAdd.CustomerContactEmail = contact.ContactEmail;
                                                $scope.BillingAdd.CustomerAddress = contact.Address;
                                                $scope.BillingAdd.CustomerBranch = contact.BranchName;
                                                $scope.BillingAdd.CreditDay = contact.CreditDate;
                                                $scope.OnChangeCredit();
                                            }

                                            $scope.SelectInvoice = undefined;
                                        }

                                        $scope.document.binding = 0;
                                        $('#modalinvoice-list').modal('hide');
                                    }
                                }
                                else {
                                    $scope.table.binding = 0;
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnChangeMapping = function (index) {
        var invoice = $scope.Parameter.Param_Document[index];

        if (invoice != undefined) {
            var len = $scope.document.len == undefined || $scope.document.len === '' ? 0 : detectFloat($scope.document.len); // ราคาจากหน้าจอ 
            var invoicetotal = $scope.document.total == undefined || $scope.document.total === '' ? 0 : detectFloat($scope.document.total); // ราคาจากหน้าจอ 
            var invoiceamount = invoice.GrandAmountText == undefined || invoice.GrandAmountText === '' ? 0 : detectFloat(invoice.GrandAmountText); // ราคาจากหน้าจอ 
            if (invoice.Mapping) {
                invoicetotal = invoicetotal + invoiceamount;
                $scope.document.total = AFormatNumber(invoicetotal, 2);
                len = len + 1;
            }
            else {
                invoicetotal = invoicetotal - invoiceamount;
                $scope.document.total = AFormatNumber(invoicetotal, 2);
                len = len - 1;
            }

            $scope.document.total = AFormatNumber(invoicetotal, 2);
            $scope.document.len = AFormatNumber(len, 0);
        }
    };

    $scope.documentcurrentPage = 0;

    $scope.documentLimitFirst = 0;
    $scope.documentLimitPage = 5;
    $scope.documentitemsPerPage = 10;

    $scope.documentpageCount = function () {
        //alert($scope.Device.length)
        return Math.ceil($scope.Parameter.Param_Document.length / $scope.documentitemsPerPage) - 1;
    };

    $scope.documentrange = function () {
        $scope.documentitemsCount = $scope.Parameter.Param_Document.length;
        $scope.documentpageshow = $scope.documentpageCount() > $scope.documentLimitPage && $scope.documentpageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.documentpageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.documentpageshowdata = 1;
        $scope.documentretpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.documentpdocumentPage = function () {
        if ($scope.documentcurrentPage > 0) {
            $scope.documentcurrentPage--;
        }

        if ($scope.documentcurrentPage < $scope.documentLimitFirst && $scope.documentcurrentPage >= 1) {
            $scope.documentLimitFirst = $scope.documentLimitFirst - 5;
            $scope.documentLimitPage = $scope.documentLimitPage - 5;
            for (var i = 1; i <= $scope.documentretpage.length; i++) {
                if (i >= $scope.documentLimitFirst && i <= $scope.documentLimitPage) {
                    $scope.documentretpage[i - 1].show = 1;
                }
                else
                    $scope.documentretpage[i - 1].show = 0;
            }
        }
    };

    $scope.documentpdocumentPageDisabled = function () {
        return $scope.documentcurrentPage === 0 ? "disabled" : "";
    };

    $scope.documentnextPage = function () {
        if ($scope.documentcurrentPage < $scope.documentpageCount()) {
            $scope.documentcurrentPage++;
        }

        if ($scope.documentcurrentPage >= $scope.documentLimitPage && $scope.documentcurrentPage <= $scope.documentpageCount()) {
            $scope.documentLimitFirst = $scope.documentLimitFirst + 5;
            $scope.documentLimitPage = $scope.documentLimitPage + 5;
            for (var i = 1; i <= $scope.documentretpage.length; i++) {
                if (i >= $scope.documentLimitFirst && i <= $scope.documentLimitPage) {
                    $scope.documentretpage[i - 1].show = 1;
                }
                else
                    $scope.documentretpage[i - 1].show = 0;
            }
        }

    };

    $scope.documentnextPageDisabled = function () {
        return $scope.documentcurrentPage === $scope.documentpageCount() ? "disabled" : "";
    };

    $scope.documentsetPage = function (n) {
        $scope.documentcurrentPage = n;
    };

    /****************** Document Attach ************************/
    $scope.OnLoadAttach = function () {
        try {
            $http.get(baseURL + "DocumentSell/GetDocumentAttach?documentkey=" + $scope.BillingAdd.BillingKey + "&documenttype=" + headerdoc + "&key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.DocumentAttach = [];
                            $scope.DocumentAttach = response.data.responsedata;
                        }
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnUploadAttach = function (files) {
        try {
            if ($scope.BillingAdd.BillingKey == undefined || $scope.BillingAdd.BillingKey == "")
                throw "กรุณากดปุ่ม บันทึก ก่อนแนบไฟล์เอกสาร";
            else if ($scope.DocumentAttach.length > 3)
                throw "สามารถแนบไฟล์สูงสุดเพียง 3 ไฟล์ เท่านั้น";
            else if (files.length > 0) {
                var data = {
                    DocumentKey: $scope.BillingAdd.BillingKey,
                    DocumentNo: $scope.BillingAdd.BillingNo,
                    DocumentType: headerdoc,
                    AttachName: files[0].name,
                    AttachExtension: getFileExtension(files[0].name)
                };
                $http.post(baseURL + "DocumentSell/PostDocumentAttach", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    try {
                                        var attachfile = response.data.responsedata;
                                        var input = document.getElementById("document_attach");
                                        var files = input.files;
                                        var formData = new FormData();

                                        for (var i = 0; i != files.length; i++) {
                                            formData.append("files", files[i]);
                                        }

                                        $.ajax(
                                            {
                                                url: baseURL + "DocumentSell/UploadDocumentAttach?attachkey=" + attachfile.AttachKey,
                                                data: formData,
                                                processData: false,
                                                contentType: false,
                                                type: "POST",
                                                success: function (data) {

                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();

                                                    $scope.OnLoadAttach();
                                                    showSuccessText('แนบไฟล์สำเร็จ');
                                                },
                                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                    var drEvent = $('.dropify').dropify();
                                                    drEvent = drEvent.data('dropify');
                                                    drEvent.resetPreview();
                                                    drEvent.clearElement();
                                                    drEvent.destroy();

                                                    drEvent.init();
                                                    showErrorToast(textStatus);
                                                }
                                            }
                                        );
                                    }
                                    catch (ex) {
                                        var drEvent = $('.dropify').dropify();
                                        drEvent = drEvent.data('dropify');
                                        drEvent.resetPreview();
                                        drEvent.clearElement();
                                        drEvent.destroy();

                                        drEvent.init();
                                        showErrorToast(ex);
                                    }
                                }
                                else {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                        }
                        catch (err) {
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) {
            var drEvent = $('.dropify').dropify();
            drEvent = drEvent.data('dropify');
            drEvent.resetPreview();
            drEvent.clearElement();
            drEvent.destroy();

            drEvent.init();
            showWariningToast(err);
        }
    };

    $scope.OnClickDeleteAttach = function (attachkey) {
        var data = {
            DocumentKey: $scope.BillingAdd.BillingKey,
            DocumentNo: $scope.BillingAdd.BillingNo,
            DocumentType: headerdoc,
            AttachKey: attachkey
        };
        $http.post(baseURL + "DocumentSell/DeleteDocumentAttach", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            try {
                                showDeleteSuccessToast();
                                $scope.OnLoadAttach();
                            }
                            catch (err) {
                                showErrorToast(err);
                            }
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickShowAttach = function (attachkey) {
        var attactfile = _.where($scope.DocumentAttach, { AttachKey: attachkey })[0];
        if (attactfile != undefined) {
            $scope.DocumentAttachView = [];
            $scope.DocumentAttachView = attactfile;

            if (attactfile.AttachExtension == 'pdf') {
                $scope.DocumentAttachView.AttachHeight = '850px';
                PDFObject.embed(baseURL + 'uploads/attach/' + attactfile.AttachPath, "#exampleattach");
            }
            else {
                $scope.DocumentAttachView.AttachHeight = 'auto';
                $scope.DocumentAttachView.AttachShow = baseURL + 'uploads/attach/' + attactfile.AttachPath;
            }
            $('#modal-attach').modal('show');
        }
    };
});


