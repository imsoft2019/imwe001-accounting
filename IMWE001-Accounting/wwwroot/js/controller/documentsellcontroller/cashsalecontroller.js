﻿WEACCTAPP.controller('cashsalecontroller', function ($scope, $http, $timeout, GlobalVar, paramService, $q) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    const headerdoc = 'CA';
    $scope.BusinessCashSale = [];
    $scope.BusinessCashSaleMain = [];
    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];
    $scope.Parameter.BizBank = [];
    $scope.BusinessProfile = [];
    $scope.BusinessEmployee = [];
    $scope.EmailAdd = [];
    var gEmp = paramService.getProfile();
    var gProfile = paramService.getBusinessProfile();

    var gParamQuotation = paramService.getParameterQuotation();
    var gBizBank = paramService.getBankBusiness();

    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
            $scope.Search.InputToDate = GetDatetimeNowLastMonth();

            var qq1 = $q.all([gProfile, gEmp]).then(function (data) {
                try {
                    /****** GetDocument NO ******/
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BusinessProfile = data[0].data.responsedata; }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Customer ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.BusinessEmployee = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });

            var qq = $q.all([gParamQuotation, gBizBank]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                            $scope.Parameter.Param_Quotation = [];
                            $scope.Parameter.Param_Quotation = data[0].data.responsedata;
                        }
                        else if (data[0].data.responsecode == '000') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Business Bank ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.Parameter.BizBank = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                    }


                    $http.get(baseURL + "DocumentSell/GetBusinessCashSale?key=" + makeid())
                        .then(function (response) {

                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {
                                    $scope.BusinessCashSaleMain = response.data.responsedata;

                                    var grandtotal = 0;
                                    _.each($scope.BusinessCashSaleMain, function (item) {
                                        item.CashSaleDate = formatDate(item.CashSaleDate);
                                        if (item.CashSaleStatus != '2')
                                            grandtotal = grandtotal + item.GrandAmount;
                                        item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                                        item.TotalAfterDiscountAmount = AFormatNumber(item.TotalAfterDiscountAmount, 2);
                                        item.TotalBeforeVatAmount = AFormatNumber(item.TotalBeforeVatAmount, 2);

                                        if (item.ReceiptInfo != undefined) {
                                            item.ReceiptInfo.PaymentDate = formatDate(item.ReceiptInfo.PaymentDate);
                                            item.ReceiptInfo.PaymentAmount = AFormatNumber(item.ReceiptInfo.PaymentAmount, 2);
                                            item.ReceiptInfo.NetPaymentAmount = AFormatNumber(item.ReceiptInfo.NetPaymentAmount, 2);
                                            item.ReceiptInfo.WithholdingAmount = item.ReceiptInfo.IsWithholdingTax == '1' ? AFormatNumber(item.ReceiptInfo.WithholdingAmount, 2) : '0.00';
                                            item.ReceiptInfo.DiscountAmount = item.ReceiptInfo.IsDiscount == '1' ? AFormatNumber(item.ReceiptInfo.DiscountAmount, 2) : '0.00';
                                            item.ReceiptInfo.ChequeDate = item.ReceiptInfo.ChequeDate != undefined ? formatDate(item.ReceiptInfo.ChequeDate) : undefined;
                                            item.ReceiptInfo.Balance = AFormatNumber(item.ReceiptInfo.Balance, 2);

                                            var htmltag;
                                            htmltag = "<div class='title' style='width:220px;' >รายละเอียดการชำระ </div> <hr class='mb-2'> ";
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ยอดที่เรียกเก็บ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.PaymentAmount + " บาท</div></div>";
                                            if (item.ReceiptInfo.IsDiscount == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.DiscountTypeName + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.DiscountAmount + " บาท</div></div> ";
                                            if (item.ReceiptInfo.IsWithholdingTax == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ภาษีหัก ณ ที่จ่าย </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.WithholdingAmount + " บาท</div> </div>";
                                            if (item.ReceiptInfo.BalanceType == 'Lost')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.CauseTypeName.replace('/เงินเกิน', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.Balance + " บาท</div> </div>";
                                            if (item.ReceiptInfo.BalanceType == 'Over')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiptInfo.CauseTypeName.replace('เงินขาด/', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.Balance + " บาท</div> </div>";
                                            htmltag = htmltag + " <hr class='mb-2'><div class='row'> <div class='col-lg-6'> ยอดรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.NetPaymentAmount + " บาท</div> </div>";
                                            htmltag = htmltag + " <div class='row'> <div class='col-lg-6'> วิธีการรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiptInfo.PaymentTypeName + "</div> </div>";
                                            item.ReceiptInfo.TagHtml = htmltag;
                                        }
                                    });
                                    $scope.CashSaleSummary = AFormatNumber(grandtotal, 2);
                                    $scope.BusinessCashSale = $scope.BusinessCashSaleMain;

                                    $scope.retpage = [];
                                    $scope.range();
                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnClickSearch = function () {
        $scope.BusinessCashSale = $scope.BusinessCashSaleMain;
        $scope.table.binding = 1;

        $scope.BusinessCashSale = _.filter($scope.BusinessCashSaleMain, function (item) {

            var c1 = true, c2 = true, c3 = true, c4 = true, c5 = true;

            if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.CustomerName != undefined && (item.CustomerName).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.CustomerName == undefined || (item.CustomerName).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.CashSaleNo != undefined && (item.CashSaleNo).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.CashSaleNo == undefined || (item.CashSaleNo).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProjectName != undefined && (item.ProjectName).indexOf($scope.Search.InputFilter) > -1)
                    c3 = true;
                else if (item.ProjectName == undefined || (item.ProjectName).indexOf($scope.Search.InputFilter) < 0)
                    c3 = false;


            }

            if ($scope.Search.SelectStatus == '0') {
                c4 = true;
            }
            else if (item.CashSaleStatus == $scope.Search.SelectStatus) {
                c4 = true;
            }
            else
                c4 = false;

            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != "" && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != "") {

                var fromarry = $scope.Search.InputFromDate.split('-');

                var quotationdatearry = item.CashSaleDate.split('-');

                var from = new Date(fromarry[2], fromarry[1] - 1, fromarry[0]);

                var quotationdate = new Date(quotationdatearry[2], quotationdatearry[1] - 1, quotationdatearry[0]);

                var toarry = $scope.Search.InputToDate.split('-');

                var to = new Date(toarry[2], toarry[1] - 1, toarry[0]);
                //alert(quotationdate); alert(from); alert(from);
                if (quotationdate >= from && quotationdate <= to)
                    c5 = true;
                else
                    c5 = false;
            }


            if ((c1 || c2 || c3) && c4 && c5) {
                return item;
            }
        });
        $scope.table.binding = 0;
    };

    $scope.OnClickAdd = function () {
        window.location.href = baseURL + "DocumentSell/CashSaleAdd";
    };

    $scope.OnClickClear = function () {
        $scope.Search = [];
        $scope.Search.SelectStatus = '0';
        $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
        $scope.Search.InputToDate = GetDatetimeNowLastMonth();
        $scope.BusinessCashSale = $scope.BusinessCashSaleMain;
    };

    $scope.OnClickDuplicate = function (key) {
        window.location.href = baseURL + "DocumentSell/CashSaleAdd?CashSaleKey=" + key + "&dup_ref=1";
    };

    $scope.OnClickUpdate = function (key) {
        window.location.href = baseURL + "DocumentSell/CashSaleAdd?CashSaleKey=" + key;
    };

    $scope.OnclickDelete = function (val) {
        //var filter = _.where($scope.BusinessCashSale, { CashSaleKey: val })[0];
        //if (filter != undefined) {
        //    $scope.CashSaleAdd = [];
        //    $scope.CashSaleAdd = angular.copy(filter);
        //    if ($scope.CashSaleAdd.CashSaleStatus == '1')
        //        $('#modalcashsale-del').modal('show');
        //    else
        $('#modalcashsale-alert').modal('show');
        //}
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            CashSaleKey: $scope.CashSaleAdd.CashSaleKey
        };

        $http.post(baseURL + "DocumentSell/DeleteBusinessCashSale", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalcashsale-del').modal('hide');
                            $scope.init();
                            $scope.CashSaleAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickNextWorkflow = function (CashSalekey, tostatus) {
        try {
            var data = {
                CashSaleKey: CashSalekey,
                CashSaleStatus: tostatus
            };
            $http.post(baseURL + "DocumentSell/PostBusinessCashSaleNextStep", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.init();
                                showSuccessWorkToast();
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickPaymentAdd = function (val) {
        var filter = _.where($scope.BusinessCashSale, { CashSaleKey: val })[0];
        if (filter != undefined) {
            $scope.CashSaleAdd = [];
            $scope.CashSaleAdd = angular.copy(filter);
            $scope.CashSaleAdd.IsTaxHeader = $scope.CashSaleAdd.IsTaxHeader =='1' ? true : false;

            $scope.CashSaleAdd.ReceiptInfo = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push({ ID: '0', Name: 'จำนวนเงิน', Rate: 0 });
            _.each($scope.Parameter.Param_Quotation.Param_WithholdingRate, function (item) { $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push(item); });


            $scope.CashSaleAdd.ReceiptInfo.DocumentKey = $scope.CashSaleAdd.ReceiptKey;
            $scope.CashSaleAdd.ReceiptInfo.DocumentNo = $scope.CashSaleAdd.ReceiptNo;
            $scope.CashSaleAdd.ReceiptInfo.DocumentType = headerdoc;
            $scope.CashSaleAdd.ReceiptInfo.CustomerName = $scope.CashSaleAdd.CustomerName;
            $scope.CashSaleAdd.ReceiptInfo.PaymentAmount = AFormatNumber($scope.CashSaleAdd.GrandAmount, 2);
            $scope.CashSaleAdd.ReceiptInfo.PaymentDate = GetDatetimeNow();
            $scope.CashSaleAdd.IsWithholdingTax = $scope.CashSaleAdd.IsWithholdingTax == '1' ? true : false;

            $scope.CashSaleAdd.ReceiptInfo.IsWithholdingTax = $scope.CashSaleAdd.IsWithholdingTax;
            if ($scope.CashSaleAdd.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.CashSaleAdd.WithholdingKey; });
                if (type.length > 0)
                    $scope.CashSaleAdd.ReceiptInfo.SelectWithholding = type[0];

                $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount = $scope.CashSaleAdd.IsWithholdingTax ? AFormatNumber($scope.CashSaleAdd.WithholdingAmount, 2) : AFormatNumber(0, 2);
            }

            if ($scope.CashSaleAdd.IsTaxHeader == '1') {
                $scope.CashSaleAdd.ReceiptInfo.IsWithholdingTax = false;
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
                if (type.length > 0)
                    $scope.CashSaleAdd.ReceiptInfo.SelectWithholding = type[0];

                $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            }

            $scope.CashSaleAdd.IsAdjustDiscount = $scope.CashSaleAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.CashSaleAdd.ReceiptInfo.IsDiscount = $scope.CashSaleAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.CashSaleAdd.ReceiptInfo.DiscountType = $scope.CashSaleAdd.IsAdjustDiscount ? $scope.CashSaleAdd.AdjustDiscountType : undefined;
            $scope.CashSaleAdd.ReceiptInfo.DiscountTypeName = $scope.CashSaleAdd.IsAdjustDiscount ? $scope.CashSaleAdd.AdjustDiscountTypeName : undefined;
            $scope.CashSaleAdd.ReceiptInfo.DiscountAmount = $scope.CashSaleAdd.IsAdjustDiscount ? $scope.CashSaleAdd.AdjustDiscountAmount : AFormatNumber(0, 2);

            $scope.CashSaleAdd.ReceiptInfo.NetPaymentAmount = $scope.CashSaleAdd.IsAdjustDiscount || $scope.CashSaleAdd.IsWithholdingTax ? AFormatNumber($scope.CashSaleAdd.PaymentAmount, 2) : AFormatNumber($scope.CashSaleAdd.GrandAmount, 2);

            $scope.CashSaleAdd.ReceiptInfo.PaymentType = "CA";
            $scope.CashSaleAdd.ReceiptInfo.Balance = AFormatNumber(0, 2);
            $scope.CashSaleAdd.ReceiptInfo.CauseType = undefined;
            $scope.CashSaleAdd.ReceiptInfo.IsCauseType = true;
            $scope.CashSaleAdd.ReceiptInfo.IsPayment = true;

            $('#modalreceipt-receive').modal('show');
        }
    };


    $scope.OnChangeIsAdjustReceive = function () {
        $scope.CashSaleAdd.ReceiptInfo.DiscountType = '1';
        $scope.CashSaleAdd.ReceiptInfo.DiscountAmount = AFormatNumber(0, 2);
    };

    $scope.OnChangeIsWithhollding = function () {
        if ($scope.CashSaleAdd.IsTaxHeader == '1') {
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
            if (type.length > 0)
                $scope.CashSaleAdd.ReceiptInfo.SelectWithholding = type[0];
        }
        else {
            var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '1'; });
            if (type.length > 0)
                $scope.CashSaleAdd.ReceiptInfo.SelectWithholding = type[0];
        }
        $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    };

    $scope.OnChangeWithholldingReceipt = function () {
        if ($scope.CashSaleAdd.ReceiptInfo.SelectWithholding.ID == '0') {
            $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(0, 2);
            $scope.OnChangeReceiveCalculate();
        }
        else {
            $scope.OnChangeReceiveCalculate();
        }
    };

    $scope.OnChangePaymentType = function () {
        if ($scope.CashSaleAdd.ReceiptInfo.PaymentType == 'TR') {
            var type = $scope.Parameter.BizBank.filter(function (item) { return item.UID == $scope.Parameter.BizBank[0].UID; });
            if (type.length > 0)
                $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer = type[0];
        }
        else if ($scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CQ') {
            $scope.CashSaleAdd.ReceiptInfo.SelectBank = undefined;
            $scope.CashSaleAdd.ReceiptInfo.ChequeDate = GetDatetimeNow();
            $scope.CashSaleAdd.ReceiptInfo.ChequeNo = '';
        }
        else if ($scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CC') {
            $scope.CashSaleAdd.ReceiptInfo.SelectBank = undefined;
        }
    };

    $scope.OnChangeReceiveCalculate = function () {
        try {
            var grandtotal = 0, netpayment = 0, discount = 0, withholding = 0, netgrand = 0, pricediscount = 0, pricevat, balance = 0;
            grandtotal = $scope.CashSaleAdd.ReceiptInfo.PaymentAmount == undefined || $scope.CashSaleAdd.ReceiptInfo.PaymentAmount === '' ? 0 : detectFloat($scope.CashSaleAdd.ReceiptInfo.PaymentAmount); // Discount Percenter
            pricediscount = $scope.CashSaleAdd.TotalAfterDiscountAmount == undefined || $scope.CashSaleAdd.TotalAfterDiscountAmount === '' ? 0 : detectFloat($scope.CashSaleAdd.TotalAfterDiscountAmount); // Discount Percenter
            pricevat = $scope.CashSaleAdd.TotalBeforeVatAmount == undefined || $scope.CashSaleAdd.TotalBeforeVatAmount === '' ? 0 : detectFloat($scope.CashSaleAdd.TotalBeforeVatAmount); // Discount Percenter
            netpayment = $scope.CashSaleAdd.ReceiptInfo.NetPaymentAmount == undefined || $scope.CashSaleAdd.ReceiptInfo.NetPaymentAmount === '' ? 0 : detectFloat($scope.CashSaleAdd.ReceiptInfo.NetPaymentAmount); // Discount Percenter
            discount = $scope.CashSaleAdd.ReceiptInfo.DiscountAmount == undefined || $scope.CashSaleAdd.ReceiptInfo.DiscountAmount === '' ? 0 : detectFloat($scope.CashSaleAdd.ReceiptInfo.DiscountAmount); // Discount Percenter
            withholding = $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount == undefined || $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount === '' ? 0 : detectFloat($scope.CashSaleAdd.ReceiptInfo.WithholdingAmount); // Discount Percenter


            if ($scope.CashSaleAdd.ReceiptInfo.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.CashSaleAdd.ReceiptInfo.SelectWithholding.ID; });
                if (type.length > 0)
                    withholdingrate = type[0].Rate;

                if ($scope.CashSaleAdd.ReceiptInfo.SelectWithholding.ID == '0')
                    withholding = withholding;
                else {
                    if ($scope.CashSaleAdd.TaxType == 'N')
                        withholding = pricediscount * withholdingrate / 100;
                    else if ($scope.CashSaleAdd.TaxType == 'V')
                        withholding = pricevat * withholdingrate / 100;
                }

                grandtotal = grandtotal - withholding;
            }
            else
                grandtotal = grandtotal;

            grandtotal = grandtotal - discount;

            if (!$scope.CashSaleAdd.ReceiptInfo.IsPayment) {
                balance = netpayment - grandtotal;
            }
            else
                netpayment = grandtotal;

            if (balance < 0) {
                $scope.CashSaleAdd.ReceiptInfo.IsCauseType = false;
                $scope.CashSaleAdd.ReceiptInfo.IsText = 'เงินขาด';
                $scope.CashSaleAdd.ReceiptInfo.BalanceType = 'Lost';
                $scope.CashSaleAdd.ReceiptInfo.CauseType = undefined;
                balance = balance * (-1);
            }
            else if (balance > 0) {
                $scope.CashSaleAdd.ReceiptInfo.IsCauseType = false;
                $scope.CashSaleAdd.ReceiptInfo.IsText = 'เงินเกิน';
                $scope.CashSaleAdd.ReceiptInfo.BalanceType = 'Over';
                $scope.CashSaleAdd.ReceiptInfo.CauseType = undefined;
            }
            else {
                $scope.CashSaleAdd.ReceiptInfo.IsCauseType = true;
                $scope.CashSaleAdd.ReceiptInfo.IsText = '';
                $scope.CashSaleAdd.ReceiptInfo.BalanceType = 'Equal';
                $scope.CashSaleAdd.ReceiptInfo.CauseType = undefined;
            }

            $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount = AFormatNumber(withholding, 2);//
            $scope.CashSaleAdd.ReceiptInfo.NetPaymentAmount = AFormatNumber(netpayment, 2);//
            $scope.CashSaleAdd.ReceiptInfo.Balance = AFormatNumber(balance, 2);//
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickIsPayment = function () {
        $scope.CashSaleAdd.ReceiptInfo.IsPayment = false;
        var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });
        if (type.length > 0)
            $scope.CashSaleAdd.ReceiptInfo.SelectWithholding = type[0];
    };

    $scope.OnClickReceiptPayment = function () {
        try {
            if (!$scope.CashSaleAdd.ReceiptInfo.IsCauseType && $scope.CashSaleAdd.ReceiptInfo.CauseType == undefined)
                showWariningToast("กรุณาระบุสาเหตุ เงินขาด/เงินเกิน");
            else {
                var adjusttype;
                if ($scope.CashSaleAdd.ReceiptInfo.DiscountType == '1')
                    adjusttype = 'ส่วนลดพิเศษ';
                else if ($scope.CashSaleAdd.ReceiptInfo.DiscountType == '2')
                    adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
                else if ($scope.CashSaleAdd.ReceiptInfo.DiscountType == '3')
                    adjusttype = 'ค่าดำเนินการ';
                else if ($scope.CashSaleAdd.ReceiptInfo.DiscountType == '4')
                    adjusttype = 'ปัดเศษ';

                var paymenttype;
                if ($scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CA')
                    paymenttype = 'เงินสด';
                else if ($scope.CashSaleAdd.ReceiptInfo.PaymentType == 'TR')
                    paymenttype = 'โอนเงิน';
                else if ($scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CQ')
                    paymenttype = 'เช็ค';
                else if ($scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CC')
                    paymenttype = 'บัตรเครดิต';

                var cause;
                if ($scope.CashSaleAdd.ReceiptInfo.CauseType == '1')
                    cause = 'ค่าธรรมเนียม';
                else if ($scope.CashSaleAdd.ReceiptInfo.CauseType == '2')
                    cause = 'เงินขาด/เงินเกิน';

                var data = {
                    DocumentKey: $scope.CashSaleAdd.CashSaleKey,
                    DocumentNo: $scope.CashSaleAdd.CashSaleNo,
                    DocumentType: headerdoc,
                    CustomerName: $scope.CashSaleAdd.ReceiptInfo.CustomerName,
                    PaymentAmount: $scope.CashSaleAdd.ReceiptInfo.PaymentAmount,
                    PaymentDate: ToJsonDate2($scope.CashSaleAdd.ReceiptInfo.PaymentDate),
                    NetPaymentAmount: ConvertToDecimal($scope.CashSaleAdd.ReceiptInfo.NetPaymentAmount),
                    IsWithholdingTax: $scope.CashSaleAdd.ReceiptInfo.IsWithholdingTax ? '1' : '0',
                    WithholdingKey: $scope.CashSaleAdd.ReceiptInfo.IsWithholdingTax ? $scope.CashSaleAdd.ReceiptInfo.SelectWithholding.ID : undefined,
                    WithholdingRate: $scope.CashSaleAdd.ReceiptInfo.IsWithholdingTax ? $scope.CashSaleAdd.ReceiptInfo.SelectWithholding.Rate : undefined,
                    WithholdingAmount: $scope.CashSaleAdd.ReceiptInfo.IsWithholdingTax ? $scope.CashSaleAdd.ReceiptInfo.WithholdingAmount : undefined,
                    IsDiscount: $scope.CashSaleAdd.ReceiptInfo.IsDiscount ? '1' : '0',
                    DiscountType: $scope.CashSaleAdd.ReceiptInfo.IsDiscount ? $scope.CashSaleAdd.ReceiptInfo.DiscountType : undefined,
                    DiscountTypeName: $scope.CashSaleAdd.ReceiptInfo.IsDiscount ? adjusttype : undefined,
                    DiscountAmount: $scope.CashSaleAdd.ReceiptInfo.IsDiscount ? $scope.CashSaleAdd.ReceiptInfo.DiscountAmount : undefined,
                    PaymentType: $scope.CashSaleAdd.ReceiptInfo.PaymentType,
                    PaymentTypeName: paymenttype,
                    BankAccountID: $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer.UID : undefined,
                    BankCode: $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.CashSaleAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.CashSaleAdd.ReceiptInfo.SelectBank.ID : undefined,
                    BankName: $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer.BankName :
                        $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CQ' || $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CC' ? $scope.CashSaleAdd.ReceiptInfo.SelectBank == undefined ? undefined : $scope.CashSaleAdd.ReceiptInfo.SelectBank.Name : undefined,
                    BankAccountNo: $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer.AccountNo : undefined,
                    BankAccountType: $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'TR' ? $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer == undefined ? undefined : $scope.CashSaleAdd.ReceiptInfo.SelectBankTransfer.DepositTypeName : undefined,
                    ChequeDate: $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CQ' ? ToJsonDate2($scope.CashSaleAdd.ReceiptInfo.ChequeDate) : undefined,
                    ChequeNo: $scope.CashSaleAdd.ReceiptInfo.PaymentType == 'CQ' ? $scope.CashSaleAdd.ReceiptInfo.ChequeNo : undefined,
                    Comment: $scope.CashSaleAdd.ReceiptInfo.Comment,
                    Balance: $scope.CashSaleAdd.ReceiptInfo.Balance,
                    BalanceType: $scope.CashSaleAdd.ReceiptInfo.BalanceType,
                    CauseType: $scope.CashSaleAdd.ReceiptInfo.CauseType,
                    CauseTypeName: cause
                };

                $http.post(baseURL + "DocumentSell/PostBusinessCashSaleReceiptPayment", data, config).then(
                    function (response) {
                        try {
                            if (response != undefined && response != "") {
                                if (response.data.responsecode == 200) {
                                    showSuccessToast();
                                    $('#modalreceipt-receive').modal('hide');
                                    $scope.init();
                                    $scope.CashSaleAdd = [];
                                }
                                else showErrorToast(response.data.errormessage);
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                        catch (err) {
                            $scope.table.binding = 0;
                            showErrorToast(err);
                        }
                    });
            }
        }
        catch (err) { showErrorToast(err); }
    };

    $scope.OnClickReportContent = function (dockey, docno, type) {
        $scope.document = [];
        $scope.document.DocKey = dockey;
        $scope.document.DocNo = docno;
        $scope.document.original = $scope.document.copy = true;
        $scope.document.type = type;
        $scope.document.typedesc = type == 'Report' ? 'พิมพ์' : 'ดาวน์โหลด';
        $('#modal-content-report').modal('show');
    };

    $scope.OnClickReportType = function () {
        if ($scope.document.type == "Report")
            $scope.OnClickReport();
        else if ($scope.document.type == "Download")
            $scope.OnClickDownload();
    };

    $scope.OnClickReport = function () {
        $('#modal-content-report').modal('hide');
        $('#modal-report').modal('show');
        $scope.document.bindding = 1;
        $scope.document.DocNo = $scope.document.DocNo;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'CA',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Report',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function () {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: $scope.document.DocKey,
                DocumentType: 'CA',
                DocumentNo: $scope.document.DocNo,
                TypeAction: 'Download',
                Content: $scope.document.original == true && $scope.document.copy == true ? "NORMAL" : $scope.document.original == true ? "ORIGINAL" : $scope.document.copy == true ? "COPY" : "ORIGINAL",
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $('#modal-content-report').modal('hide');
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickSendMail = function (dockey, docno) {
        var quo = _.where($scope.BusinessCashSale, { CashSaleKey: dockey })[0];
        if (quo != undefined) {
            $scope.EmailAdd = [];
            $scope.EmailAdd.DocKey = dockey;
            $scope.EmailAdd.DocNo = docno;
            $scope.EmailAdd.MailToMe = $scope.EmailAdd.Content1 = $scope.EmailAdd.Content2 = true;
            $scope.EmailAdd.MailFrom = $scope.BusinessEmployee.Email;
            $scope.EmailAdd.MailTo = quo.CustomerContactEmail;
            $scope.EmailAdd.Subject = 'เอกสาร ' + docno;
            $scope.EmailAdd.Message = 'เรียน ' + quo.CustomerName + '\n\n' + $scope.BusinessProfile.BusinessName + ' เลขที่ ' + docno + ' มาให้ในอีเมลนี้\n\nกรุณาคลิกที่ไฟล์แนบ เพื่อดาวน์โหลดเอกสารของคุณ\n\nด้วยความเคารพ\n' + $scope.BusinessEmployee.EmployeeName + '\n' + $scope.BusinessProfile.BusinessName;
            $('#modalemail-send').modal('show');
        }

    };

    $scope.OnClickConfirmEmail = function () {
        $('#modalemail-send').modal('hide');
        var data = {
            MailFrom: $scope.EmailAdd.MailFrom,
            MailTo: $scope.EmailAdd.MailTo,
            MailCC: $scope.EmailAdd.MailCC,
            MailIsMe: $scope.EmailAdd.MailToMe ? "1" : "0",
            Subject: $scope.EmailAdd.Subject,
            Message: $scope.EmailAdd.Message,
            DocumentKey: $scope.EmailAdd.DocKey,
            DocumentNo: $scope.EmailAdd.DocNo,
            DocumentType: 'CA',
            Content: $scope.EmailAdd.Content1 == true && $scope.EmailAdd.Content2 == true ? "NORMAL" : $scope.EmailAdd.Content1 == true ? "ORIGINAL" : $scope.EmailAdd.Content2 == true ? "COPY" : "ORIGINAL",
        };

        $http.post(baseURL + "DocumentSell/PostEmailToSending", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {

                            showSuccessText(response.data.responsedata);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };
    /****************************/

    $scope.itemsPerPage = 20;

    $scope.currentPage = 0;

    $scope.LimitFirst = 0;
    $scope.LimitPage = 5;

    $scope.orderByField = 'CashSaleKey';
    $scope.reverseSort = true;



    $scope.pageCount = function () {
        return Math.ceil($scope.BusinessCashSale.length / $scope.itemsPerPage) - 1;
    };

    $scope.range = function () {
        $scope.itemsCount = $scope.BusinessCashSale.length;
        $scope.pageshow = $scope.pageCount() > $scope.LimitPage && $scope.pageCount() > 0 ? 1 : 0;

        var rangeSize = 5;
        var ret = [];
        var start = 1;

        for (var i = 0; i <= $scope.pageCount(); i++) {
            ret.push({ code: i, name: i + 1, show: i <= 4 ? 1 : 0 });
        }
        $scope.pageshowdata = 1;
        $scope.retpage = ret;
    };

    $scope.showallpages = function () {
        if ($scope.pageshowdata == 1) {
            _.each($scope.retpage, function (e) {
                e.show = 1;
            });
            $scope.pageshowdata = 0;
        }
        else {
            _.each($scope.retpage, function (e) {
                if (e.code >= $scope.LimitFirst && e.code <= $scope.LimitPage)
                    e.show = 1;
                else
                    e.show = 0;
            });
            $scope.pageshowdata = 1;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }

        if ($scope.currentPage < $scope.LimitFirst && $scope.currentPage >= 1) {
            $scope.LimitFirst = $scope.LimitFirst - 5;
            $scope.LimitPage = $scope.LimitPage - 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }

        if ($scope.currentPage >= $scope.LimitPage && $scope.currentPage <= $scope.pageCount()) {
            $scope.LimitFirst = $scope.LimitFirst + 5;
            $scope.LimitPage = $scope.LimitPage + 5;
            for (var i = 1; i <= $scope.retpage.length; i++) {
                if (i >= $scope.LimitFirst && i <= $scope.LimitPage) {
                    $scope.retpage[i - 1].show = 1;
                }
                else
                    $scope.retpage[i - 1].show = 0;
            }
        }

    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
});


