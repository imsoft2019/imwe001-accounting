﻿WEACCTAPP.controller('activitylogcontroller', function ($scope, $http, $timeout, GlobalVar) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.activitylog = [];
    $scope.init = function () {
        try {
            $http.get(baseURL + "Home/GetLogActivity?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                            $scope.activitylog = [];
                            $scope.activitylog = response.data.responsedata;
                            _.each($scope.activitylog, function (log) {
                                log.ActivityDate = formatDate3(log.ActivityDate);
                            });
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };
});


