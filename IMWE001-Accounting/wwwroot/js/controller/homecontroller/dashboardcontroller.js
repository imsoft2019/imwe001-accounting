﻿WEACCTAPP.controller('dashboardcontroller', function ($scope, $http, $timeout, GlobalVar, $q, paramService) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

    $scope.Parameter = [];
    $scope.table = [];
    $scope.receive = [];
    $scope.pay = [];
    $scope.donutreceive = [];
    $scope.donutpay = [];
    $scope.barreceive = [];
    $scope.barpay = [];
    $scope.areaall = [];
    $scope.activitylog = [];

    var gCustomer = paramService.getCustomer();

    $scope.init = function () {
        try {

            var qq = $q.all([gCustomer]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                            $scope.SearchContact = [];
                            $scope.Parameter.Param_Contact = [];

                            $scope.Parameter.Param_Contact = data[0].data.responsedata;

                            google.charts.load('current', {
                                'packages': ['corechart']
                            });
                            google.charts.setOnLoadCallback(drawArea);

                            google.charts.load("current", {
                                packages: ["corechart"]
                            });
                            google.charts.setOnLoadCallback(drawChart);

                            google.charts.load("current", {
                                packages: ["corechart"]
                            });
                            google.charts.setOnLoadCallback(drawChart1);

                            google.charts.load('current', {
                                'packages': ['bar']
                            });
                            google.charts.setOnLoadCallback(drawStuff);

                            google.charts.load('current', {
                                'packages': ['bar']
                            });
                            google.charts.setOnLoadCallback(drawStuff1);
                        }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                }
                catch (err) {
                    showErrorToast(err);
                }
            });


            /*** ยอดค้างรับ **/
            $scope.receive.InputFromDate = GetDatetimeNow();
            $('#imputstartdate-receive-popup').datepicker('setDate', $scope.receive.InputFromDate);
            $scope.OnFilterReceive();

            $scope.pay.InputFromDate = GetDatetimeNow();
            $('#imputstartdate-pay-popup').datepicker('setDate', $scope.pay.InputFromDate);
            $scope.OnFilterPay();

            $http.get(baseURL + "Home/GetLogActivity?key=" + makeid())
                .then(function (response) {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200' && response.data.responsedata != undefined && response.data.responsedata != "") {
                            $scope.activitylog = [];
                            $scope.activitylog = response.data.responsedata;
                            _.each($scope.activitylog, function (log) {
                                log.ActivityDate = formatDate3(log.ActivityDate);
                            });
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

    $scope.OnFilterReceive = function () {
        $scope.receive.binding = 1;
        var data = {
            typedate: '5',
            startdate: ToJsonDate2($scope.receive.InputFromDate)
        };

        $http.post(baseURL + "Report/PostReportReceivable", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.receive.list = [];
                            $scope.receive.list = _.sortBy(response.data.responsedata, 'DueDate');

                            var total = 0;

                            _.each($scope.receive.list, function (inv) {
                                inv.DocumentDate = formatDate(inv.DocumentDate);
                                inv.DueDate = formatDate(inv.DueDate);

                                if (inv.DocumentStatus != '3') {
                                    if (inv.DocumentType == 'CN') {
                                        total = total - inv.GrandAmount;
                                    }
                                    else {
                                        total = total + inv.GrandAmount;
                                    }
                                }
                                inv.GrandAmountText = AFormatNumber(inv.GrandAmount, 2);
                            });
                            $scope.receive.Total = AFormatNumber(total, 2);

                            $scope.receive.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });

    };

    $scope.OnFilterPay = function () {
        $scope.pay.binding = 1;
        var data = {
            typedate: '5',
            startdate: ToJsonDate2($scope.pay.InputFromDate)
        };

        $http.post(baseURL + "Report/PostReportPayable", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.pay.list = [];
                            $scope.pay.list = _.sortBy(response.data.responsedata, 'DueDate');

                            var total = 0;

                            _.each($scope.pay.list, function (inv) {
                                inv.DocumentDate = formatDate(inv.DocumentDate);
                                inv.DueDate = formatDate(inv.DueDate);

                                if (inv.DocumentStatus != '3') {
                                    if (inv.DocumentType == 'CN') {
                                        total = total - inv.GrandAmount;
                                    }
                                    else {
                                        total = total + inv.GrandAmount;
                                    }
                                }
                                inv.GrandAmountText = AFormatNumber(inv.GrandAmount, 2);
                            });
                            $scope.pay.Total = AFormatNumber(total, 2);

                            $scope.pay.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });

    };

    function drawChart() {
        $scope.donutreceive.binding = 1;
        var data = {
            typedate: '5',
            startdate: ToJsonDate2($scope.receive.InputFromDate)
        };

        $http.post(baseURL + "Report/PostReportReceivable", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.donutreceive.list = [];
                            $scope.donutreceive.list = response.data.responsedata;

                            $scope.Parameter.Param_ContactFilter = [];
                            var total = 0;
                            var other = 0;
                            var nameother = 0;
                            var dataArray = [['Year', 'Reading']];

                            var i = 1;
                            $scope.donutreceive.customer = [];
                            _.each($scope.Parameter.Param_Contact, function (cus) {

                                if (cus.CustomerKey != '0') {
                                    var cusfilter = _.where($scope.donutreceive.list, { CustomerKey: cus.ContactKey });
                                    if (cusfilter != undefined && cusfilter.length > 0) {

                                        var customer = { ContactKey: cus.ContactKey, BusinessName: cus.BusinessName, Total: AFormatNumber(0, 2) };
                                        var amount = 0;
                                        _.each(cusfilter, function (inv) {

                                            if (inv.DocumentStatus != '3') {

                                                if (inv.DocumentType == 'CN') {
                                                    total = total - inv.GrandAmount;
                                                    amount = amount - inv.GrandAmount;
                                                }
                                                else {
                                                    total = total + inv.GrandAmount;
                                                    amount = amount + inv.GrandAmount;
                                                }
                                            }
                                            inv.GrandAmountText = AFormatNumber(inv.GrandAmount, 2);
                                        });
                                        $scope.donutreceive.customer.push({ BusinessName: customer.BusinessName, Total: amount });

                                    }
                                }
                            });

                            $scope.donutreceive.customer = _.sortBy($scope.donutreceive.customer, 'Total').reverse();
                            for (i = 0; i <= 4; i++) {
                                if ($scope.donutreceive.customer.length >= i + 1)
                                dataArray.push([$scope.donutreceive.customer[i].BusinessName, $scope.donutreceive.customer[i].Total]);
                            }

                            var data = google.visualization.arrayToDataTable(dataArray);
                            var options = {
                                fontName: 'Aksorn',
                                title: 'รายได้รวม ' + AFormatNumber(total, 2),
                                pieHole: 0.4,
                                colors: ['#76C1FA', '#63CF72', '#F36368', '#FABA66', '#fdd0c2'],
                                chartArea: {
                                    width: 500,
                                    height: 300
                                }
                            };

                            var Donutchart = new google.visualization.PieChart(document.getElementById('Donut-chart'));
                            Donutchart.draw(data, options);
                            $scope.donutreceive.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });


    }

    function drawChart1() {
        $scope.donutpay.binding = 1;
        var data = {
            typedate: '5',
            startdate: ToJsonDate2($scope.receive.InputFromDate)
        };

        $http.post(baseURL + "Report/PostReportPayable", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.donutpay.list = [];
                            $scope.donutpay.list = response.data.responsedata;

                            $scope.Parameter.Param_ContactFilter = [];
                            var total = 0;
                            var other = 0;
                            var nameother = 0;
                            var dataArray = [['Year', 'Reading']];

                            var i = 1;
                            $scope.donutpay.customer = [];
                            _.each($scope.Parameter.Param_Contact, function (cus) {

                                if (cus.CustomerKey != '0') {
                                    var cusfilter = _.where($scope.donutpay.list, { CustomerKey: cus.ContactKey });
                                    if (cusfilter != undefined && cusfilter.length > 0) {

                                        var customer = { ContactKey: cus.ContactKey, BusinessName: cus.BusinessName, Total: AFormatNumber(0, 2) };
                                        var amount = 0;
                                        _.each(cusfilter, function (inv) {

                                            if (inv.DocumentStatus != '3') {

                                                if (inv.DocumentType == 'CN') {
                                                    total = total - inv.GrandAmount;
                                                    amount = amount - inv.GrandAmount;
                                                }
                                                else {
                                                    total = total + inv.GrandAmount;
                                                    amount = amount + inv.GrandAmount;
                                                }
                                            }
                                            inv.GrandAmountText = AFormatNumber(inv.GrandAmount, 2);
                                        });

                                        $scope.donutpay.customer.push({ BusinessName: customer.BusinessName, Total: amount });

                                    }
                                }
                            });

                            $scope.donutpay.customer = _.sortBy($scope.donutpay.customer, 'Total').reverse();
                            for (i = 0; i <= 4; i++) {
                                if ($scope.donutpay.customer.length >= i + 1)
                                    dataArray.push([$scope.donutpay.customer[i].BusinessName, $scope.donutpay.customer[i].Total]);
                            }

                            var data = google.visualization.arrayToDataTable(dataArray);
                            var options = {
                                fontName: 'Aksorn',
                                title: 'รายจ่ายรวม ' + AFormatNumber(total, 2),
                                pieHole: 0.4,
                                colors: ['#76C1FA', '#63CF72', '#F36368', '#FABA66', '#fdd0c2'],
                                chartArea: {
                                    width: 500,
                                    height: 300
                                }
                            };

                            var Donutchart = new google.visualization.PieChart(document.getElementById('Donut-chart1'));
                            Donutchart.draw(data, options);
                            $scope.donutpay.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });


    }

    function drawStuff() {

        $scope.barreceive.binding = 1;
        var data = {
            typedate: '4',
            startdate: ToJsonDate2('01-01-' + getYearTh1(GetDatetimeNow()))
        };
        $http.post(baseURL + "Report/PostReportReceipt", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.barreceive.list = [];
                            $scope.barreceive.list = response.data.responsedata;

                            var amount = 0;
                            var vat = 0;
                            $scope.barreceive.list = _.sortBy($scope.barreceive.list, 'DocumentDate');

                            $scope.barreceive.monthyear = [];
                            for (m = 1; m <= 12; m++) {
                                var my = getMonthYearTh('01/' + m + '/' + getYearTh1(GetDatetimeNow()));
                                $scope.barreceive.monthyear.push({ MonthText: my, Total: 0 });
                            }

                            _.each($scope.barreceive.monthyear, function (mt) {
                                var total = 0;
                                _.each($scope.barreceive.list, function (doc) {
                                    if (getMonthYearTh(doc.DocumentDate) == mt.MonthText) {
                                        if (doc.DocumentStatus != '3') {

                                            if (doc.DocumentType == 'CN') {
                                                amount = amount - doc.Total;
                                                total = total - doc.Total;
                                            }
                                            else {
                                                amount = amount + doc.Total;
                                                total = total + doc.Total;
                                            }
                                        }
                                    }
                                });
                                mt.Total = total;
                            });

                            var dataArray = [['การเก็บเงิน : ' + AFormatNumber(amount, 2) + ' บาท', 'ยอดรวมสุทธิ']];

                            for (i = 0; i <= $scope.barreceive.monthyear.length - 1; i++)
                                dataArray.push([$scope.barreceive.monthyear[i].MonthText, $scope.barreceive.monthyear[i].Total]);

                            var data = new google.visualization.arrayToDataTable(dataArray);

                            var options = {
                                fontName: 'Aksorn',
                                colors: ['#3ca240'],
                                hAxis: {
                                    legend: {
                                        position: 'none'
                                    },

                                    chartArea: {
                                        width: 201,
                                        height: 200
                                    },
                                    hAxis: {
                                        ticks: [-1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1]
                                    },
                                    bar: {
                                        gap: 0
                                    },
                                    histogram: {
                                        bucketSize: 0.02,
                                        maxNumBuckets: 200,
                                        minValue: -1,
                                        maxValue: 1
                                    },
                                    titleTextStyle: {
                                        fontName: 'Aksorn',
                                    }
                                }
                            };

                            var chart = new google.charts.Bar(document.getElementById('Bar-chart'));
                            chart.draw(data, options);


                            $scope.barreceive.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });
    }

    function drawStuff1() {
        $scope.barpay.binding = 1;
        var data = {
            typedate: '4',
            startdate: ToJsonDate2('01-01-' + getYearTh1(GetDatetimeNow()))
        };
        $http.post(baseURL + "Report/PostReportExpense", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.barpay.list = [];
                            $scope.barpay.list = response.data.responsedata;

                            var amount = 0;
                            var vat = 0;
                            $scope.barpay.list = _.sortBy($scope.barpay.list, 'DocumentDate');

                            $scope.barpay.monthyear = [];
                            for (m = 1; m <= 12; m++) {
                                var my = getMonthYearTh('01/' + m + '/' + getYearTh1(GetDatetimeNow()));
                                $scope.barpay.monthyear.push({ MonthText: my, Total: 0 });
                            }

                            _.each($scope.barpay.monthyear, function (mt) {
                                var total = 0;
                                _.each($scope.barpay.list, function (doc) {
                                    if (getMonthYearTh(doc.DocumentDate) == mt.MonthText) {
                                        if (doc.DocumentStatus != '3') {

                                            if (doc.DocumentType == 'CN') {
                                                amount = amount - doc.Total;
                                                total = total - doc.Total;
                                            }
                                            else {
                                                amount = amount + doc.Total;
                                                total = total + doc.Total;
                                            }
                                        }
                                    }
                                });
                                mt.Total = total;
                            });

                            var dataArray = [['ชำระเงินแล้ว : ' + AFormatNumber(amount, 2) + ' บาท', 'ยอดรวมสุทธิ']];

                            for (i = 0; i <= $scope.barpay.monthyear.length - 1; i++)
                                dataArray.push([$scope.barpay.monthyear[i].MonthText, $scope.barpay.monthyear[i].Total]);

                            var data = new google.visualization.arrayToDataTable(dataArray);

                            var options = {
                                fontName: 'Aksorn',
                                colors: ['#f2525d'],

                                hAxis: {
                                    legend: {
                                        position: 'none'
                                    },
                                    is3D: true,
                                    chartArea: {
                                        width: 201,
                                        height: 200
                                    },
                                    hAxis: {
                                        ticks: [-1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1]
                                    },
                                    bar: {
                                        gap: 0
                                    },
                                    histogram: {
                                        bucketSize: 0.02,
                                        maxNumBuckets: 200,
                                        minValue: -1,
                                        maxValue: 1
                                    },
                                    titleTextStyle: {
                                        fontName: 'Aksorn',
                                    }
                                }
                            };

                            var chart = new google.charts.Bar(document.getElementById('Bar-chart1'));
                            chart.draw(data, options);


                            $scope.barpay.binding = 0;
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });
    }

    function drawArea() {

        $scope.areaall.binding = 1;
        var data = {
            typedate: '5',
            startdate: ToJsonDate2($scope.receive.InputFromDate)
        };
        $http.post(baseURL + "Report/PostReportReceivable", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            $scope.areaall.listreceiver = [];
                            $scope.areaall.listreceiver = response.data.responsedata;
                            var totalreceiver = 0;
                            var totalpay = 0;

                            $scope.areaall.monthyear = [];
                            for (m = 1; m <= 12; m++) {
                                var my = getMonthYearTh('01/' + m + '/' + getYearTh1(GetDatetimeNow()));
                                $scope.areaall.monthyear.push({ MonthText: my, TotalReceiver: 0 , TotalPay : 0 });
                            }

                            _.each($scope.areaall.monthyear, function (mt) {
                                var total = 0;
                                _.each($scope.areaall.listreceiver, function (doc) {
                                    if (getMonthYearTh(doc.DocumentDate) == mt.MonthText) {
                                        if (doc.DocumentStatus != '3') {

                                            if (doc.DocumentType == 'CN') {
                                                totalreceiver = totalreceiver - doc.GrandAmount;
                                                total = total - doc.GrandAmount;
                                            }
                                            else {
                                                totalreceiver = totalreceiver + doc.GrandAmount;
                                                total = total + doc.GrandAmount;
                                            }
                                        }
                                    }
                                });
                                mt.TotalReceiver = total;
                            });

                            $http.post(baseURL + "Report/PostReportPayable", data, config).then(
                                function (response) {
                                    try {
                                        if (response != undefined && response != "") {
                                            if (response.data.responsecode == 200) {
                                                $scope.areaall.listpay = [];
                                                $scope.areaall.listpay = _.sortBy(response.data.responsedata, 'DueDate');
                                                
                                                _.each($scope.areaall.monthyear, function (mt) {
                                                    var total = 0;
                                                    _.each($scope.areaall.listpay, function (doc) {
                                                        if (getMonthYearTh(doc.DocumentDate) == mt.MonthText) {
                                                            if (doc.DocumentStatus != '3') {

                                                                if (doc.DocumentType == 'CN') {
                                                                    totalpay = totalpay - doc.GrandAmount;
                                                                    total = total - doc.GrandAmount;
                                                                }
                                                                else {
                                                                    totalpay = totalpay + doc.GrandAmount;
                                                                    total = total + doc.GrandAmount;
                                                                }
                                                            }
                                                        }
                                                    });
                                                    mt.TotalPay = total;
                                                });

                                                var dataArray = [['', 'รายได้', 'ค่าใช้จ่าย']];

                                                for (i = 0; i <= $scope.areaall.monthyear.length - 1; i++)
                                                    dataArray.push([$scope.areaall.monthyear[i].MonthText, $scope.areaall.monthyear[i].TotalReceiver,$scope.areaall.monthyear[i].TotalPay]);

                                                var data = google.visualization.arrayToDataTable(dataArray);

                                                var options = {
                                                    fontName: 'Aksorn',
                                                    title: 'รายได้รวม ' + AFormatNumber(totalreceiver, 2) + ' ค่าใช้จ่าย ' + AFormatNumber(totalpay, 2),
                                                    hAxis: {
                                                        title: '',
                                                        titleTextStyle: {
                                                            color: '#333'
                                                        }
                                                    },
                                                    colors: ['#76C1FA', '#63CF72', '#F36368', '#FABA66'],
                                                    chartArea: {
                                                        width: 1050,
                                                        height: 300
                                                    },
                                                    vAxis: {
                                                        minValue: 0
                                                    }
                                                };

                                                var AreaChart = new google.visualization.AreaChart(document.getElementById('area-chart'));
                                                AreaChart.draw(data, options);

                                                $scope.areaall.binding = 0;
                                            }
                                            else
                                                showErrorToast(response.data.errormessage);
                                        }
                                        else {
                                            $scope.table.binding = 0;
                                            showErrorToast(response.data.errormessage);
                                        }
                                    }
                                    catch (err) {
                                        $scope.table.binding = 0;
                                        showErrorToast(err);
                                    }
                                });
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        $scope.table.binding = 0;
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    $scope.table.binding = 0;
                    showErrorToast(err);
                }
            });

       
    }
});


