﻿WEACCTAPP.controller('receiveinventorycontroller', function ($scope, $http, $timeout, GlobalVar, paramService, $q) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;
    const headerdoc = 'RI';

    $scope.PurchaseOrder = [];
    $scope.PurchaseOrderMain = [];

    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];

    $scope.BusinessProfile = [];
    $scope.BusinessEmployee = [];
    $scope.EmailAdd = [];

    var gEmp = paramService.getProfile();
    var gProfile = paramService.getBusinessProfile();

    var gParamQuotation = paramService.getParameterQuotation();
    var gBizBank = paramService.getBankBusiness();


    var DocumentAddURL = "DocumentBuy/ReceivingInventoryAdd";


    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $scope.init = function () {
        try
        {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
            $scope.Search.InputToDate = GetDatetimeNowLastMonth();

            var qq1 = $q.all([gProfile, gEmp]).then(function (data) {
                try {
                    /****** GetDocument NO ******/
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BusinessProfile = data[0].data.responsedata; }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Customer ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.BusinessEmployee = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });

            var qq = $q.all([gParamQuotation, gBizBank]).then(function (data) {
                try {
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") {
                            $scope.Parameter.Param_Quotation = [];
                            $scope.Parameter.Param_Quotation = data[0].data.responsedata;
                        }
                        else if (data[0].data.responsecode == '000') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Business Bank ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.Parameter.BizBank = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[4].data.errormessage); }
                    }

                    $http.get(baseURL + "DocumentBuy/GetBusinessRI?key=" + makeid())
                        .then(function (response) {

                            if (response != undefined && response != "") {
                                if (response.data.responsecode == '200') {
                                    $scope.PurchaseOrderMain = response.data.responsedata;

                                    var grandtotal = 0;
                                    _.each($scope.PurchaseOrderMain, function (item) {
                                        item.PurchaseOrderDate = formatDate(item.PurchaseOrderDate);
                                        if (item.CreditType == 'CD')
                                            item.DueDate = formatDate(item.DueDate);
                                        if (item.PurchaseOrderStatus != '3')
                                            grandtotal = grandtotal + item.GrandAmount;
                                        item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);

                                        if (item.ReceiveInfo != undefined) {
                                            item.ReceiveInfo.PaymentDate = formatDate(item.ReceiveInfo.PaymentDate);
                                            item.ReceiveInfo.PaymentAmount = AFormatNumber(item.ReceiveInfo.PaymentAmount, 2);
                                            item.ReceiveInfo.NetPaymentAmount = AFormatNumber(item.ReceiveInfo.NetPaymentAmount, 2);
                                            item.ReceiveInfo.WithholdingAmount = item.ReceiveInfo.IsWithholdingTax == '1' ? AFormatNumber(item.ReceiveInfo.WithholdingAmount, 2) : '0.00';
                                            item.ReceiveInfo.DiscountAmount = item.ReceiveInfo.IsDiscount == '1' ? AFormatNumber(item.ReceiveInfo.DiscountAmount, 2) : '0.00';
                                            item.ReceiveInfo.ChequeDate = item.ReceiveInfo.ChequeDate != undefined ? formatDate(item.ReceiveInfo.ChequeDate) : undefined;
                                            item.ReceiveInfo.Balance = AFormatNumber(item.ReceiveInfo.Balance, 2);

                                            var htmltag;
                                            htmltag = "<div class='title' style='width:220px;' >รายละเอียดการชำระ </div> <hr class='mb-2'> ";
                                            htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ยอดที่เรียกเก็บ </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.PaymentAmount + " บาท</div></div>";
                                            if (item.ReceiveInfo.IsDiscount == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiveInfo.DiscountTypeName + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.DiscountAmount + " บาท</div></div> ";
                                            if (item.ReceiveInfo.IsWithholdingTax == '1')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> ภาษีหัก ณ ที่จ่าย </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.WithholdingAmount + " บาท</div> </div>";
                                            if (item.ReceiveInfo.BalanceType == 'Lost')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiveInfo.CauseTypeName.replace('/เงินเกิน', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.Balance + " บาท</div> </div>";
                                            if (item.ReceiveInfo.BalanceType == 'Over')
                                                htmltag = htmltag + "<div class='row'><div class='col-lg-6'> " + item.ReceiveInfo.CauseTypeName.replace('เงินขาด/', '') + " </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.Balance + " บาท</div> </div>";
                                            htmltag = htmltag + " <hr class='mb-2'><div class='row'> <div class='col-lg-6'> ยอดรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.NetPaymentAmount + " บาท</div> </div>";
                                            htmltag = htmltag + " <div class='row'> <div class='col-lg-6'> วิธีการรับชำระ </div>  <div class='col-lg-6 text-right'> " + item.ReceiveInfo.PaymentTypeName + "</div> </div>";
                                            item.ReceiveInfo.TagHtml = htmltag;
                                        }
                                    });
                                    $scope.BillingSummary = AFormatNumber(grandtotal, 2);
                                    $scope.PurchaseOrder = $scope.PurchaseOrderMain;


                                    $scope.Search.BillingType = 'ALL';
                                    $scope.table.binding = 0;
                                }
                                else if (response.data.responsecode == '400') {
                                    showErrorToast(response.data.errormessage);
                                }
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        });
                }
                catch (err) { showErrorToast(err); }
            });
        }
        catch (err) {
            showErrorToast(err);
        }
    };


    $scope.OnClickAdd = function () {
        window.location.href = baseURL + DocumentAddURL;
    };

    $scope.OnClickUpdate = function (key) {
        window.location.href = baseURL + DocumentAddURL + "?key=" + key;
    };

    $scope.OnClickNextWorkflow = function (quotationkey, tostatus) {
        try {
            var data = {
                PurchaseOrderKey: quotationkey,
                PurchaseOrderStatus: tostatus
            };
            $http.post(baseURL + "DocumentBuy/UpdateStatusRI", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.init();
                                showSuccessWorkToast();
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickDuplicate = function (key) {
        window.location.href = baseURL + DocumentAddURL + "?key=" + key + "&dup_ref=1";

    };

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.PurchaseOrder, { PurchaseOrderKey: val })[0];
        if (filter != undefined) {
            $scope.PurchaseOrderAdd = [];
            $scope.PurchaseOrderAdd = angular.copy(filter);
            if ($scope.PurchaseOrderAdd.PurchaseOrderStatus == '1')
                $('#modalPO-del').modal('show');
            else
                $('#modalPO-alert').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            PurchaseOrderKey: $scope.PurchaseOrderAdd.PurchaseOrderKey
        };

        $http.post(baseURL + "DocumentBuy/DeleteRI", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalPO-del').modal('hide');
                            $scope.init();
                            $scope.PurchaseOrderAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickReport = function (dockey, docno) {
        $('#modal-report').modal('show');
        $scope.document = [];
        $scope.document.bindding = 1;
        $scope.document.DocNo = docno;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: 'RI',
                DocumentNo: docno,
                TypeAction: 'Report',
                Content: 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function (dockey, docno) {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: 'RI',
                DocumentNo: docno,
                TypeAction: 'Download',
                Content: 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };


    $scope.OnClickSendMail = function (dockey, docno) {
        var quo = _.where($scope.PurchaseOrder, { PurchaseOrderKey: dockey })[0];
        if (quo != undefined) {
            $scope.EmailAdd = [];
            $scope.EmailAdd.DocKey = dockey;
            $scope.EmailAdd.DocNo = docno;
            $scope.EmailAdd.MailToMe = true;
            $scope.EmailAdd.MailFrom = $scope.BusinessEmployee.Email;
            $scope.EmailAdd.MailTo = quo.CustomerContactEmail;
            $scope.EmailAdd.Subject = 'เอกสาร ' + docno;
            $scope.EmailAdd.Message = 'เรียน ' + quo.CustomerName + '\n\n' + $scope.BusinessProfile.BusinessName + ' เลขที่ ' + docno + ' มาให้ในอีเมลนี้\n\nกรุณาคลิกที่ไฟล์แนบ เพื่อดาวน์โหลดเอกสารของคุณ\n\nด้วยความเคารพ\n' + $scope.BusinessEmployee.EmployeeName + '\n' + $scope.BusinessProfile.BusinessName;
            $('#modalemail-send').modal('show');
        }

    };

    $scope.OnClickConfirmEmail = function () {
        $('#modalemail-send').modal('hide');
        var data = {
            MailFrom: $scope.EmailAdd.MailFrom,
            MailTo: $scope.EmailAdd.MailTo,
            MailCC: $scope.EmailAdd.MailCC,
            MailIsMe: $scope.EmailAdd.MailToMe ? "1" : "0",
            Subject: $scope.EmailAdd.Subject,
            Message: $scope.EmailAdd.Message,
            DocumentKey: $scope.EmailAdd.DocKey,
            DocumentNo: $scope.EmailAdd.DocNo,
            DocumentType: 'RI',
            Content: 'ORIGINAL'
        };

        $http.post(baseURL + "DocumentSell/PostEmailToSending", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {

                            showSuccessText(response.data.responsedata);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    // #region Payment Popup
    $scope.OnClickPaymentAdd = function (val) {

        var filter = _.where($scope.PurchaseOrder, { PurchaseOrderKey: val })[0];
        if (filter != undefined) {
            $scope.ReceiptAdd = [];
            $scope.ReceiptAdd = angular.copy(filter);

            $scope.ReceiptAdd.ReceiveInfo = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt = [];
            $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push({ ID: '0', Name: 'จำนวนเงิน', Rate: 0 });
            _.each($scope.Parameter.Param_Quotation.Param_WithholdingRate, function (item) { $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.push(item); });

            $scope.ReceiptAdd.ReceiveInfo.DocumentKey = $scope.ReceiptAdd.PurchaseOrderKey;
            $scope.ReceiptAdd.ReceiveInfo.DocumentNo = $scope.ReceiptAdd.PurchaseOrderNo;
            $scope.ReceiptAdd.ReceiveInfo.DocumentType = headerdoc;
            $scope.ReceiptAdd.ReceiveInfo.CustomerName = $scope.ReceiptAdd.CustomerName;
            $scope.ReceiptAdd.ReceiveInfo.PaymentAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            $scope.ReceiptAdd.ReceiveInfo.PaymentDate = GetDatetimeNow();
            $('#inputreceivedate-popup').datepicker('setDate', $scope.ReceiptAdd.ReceiveInfo.PaymentDate);
            $scope.ReceiptAdd.ReceiveInfo.NetPaymentAmount = AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
           //$scope.ReceiptAdd.ReceiveInfo.NetPaymentAmount = $scope.ReceiptAdd.IsAdjustDiscount || $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.PaymentAmount, 2) : AFormatNumber($scope.ReceiptAdd.GrandAmount, 2);
            $scope.ReceiptAdd.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax == '1' ? true : false;
            $scope.ReceiptAdd.ReceiveInfo.IsWithholdingTax = $scope.ReceiptAdd.IsWithholdingTax;
           // $scope.ReceiptAdd.ReceiveInfo.SelectWithholding = '0';
            if ($scope.ReceiptAdd.IsWithholdingTax) {
                
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.WithholdingKey; });
                if (type.length > 0) {
                    $scope.ReceiptAdd.ReceiveInfo.SelectWithholding = type[0];
                }

                $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount = $scope.ReceiptAdd.IsWithholdingTax ? AFormatNumber($scope.ReceiptAdd.WithholdingAmount, 2) : AFormatNumber(0, 2);
            }
            $scope.ReceiptAdd.IsAdjustDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiveInfo.IsDiscount = $scope.ReceiptAdd.IsAdjustDiscount == '1' ? true : false;
            $scope.ReceiptAdd.ReceiveInfo.DiscountType = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountType : undefined;
            $scope.ReceiptAdd.ReceiveInfo.DiscountTypeName = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountTypeName : undefined;
            $scope.ReceiptAdd.ReceiveInfo.DiscountAmount = $scope.ReceiptAdd.IsAdjustDiscount ? $scope.ReceiptAdd.AdjustDiscountAmount : AFormatNumber(0, 2);
            $scope.ReceiptAdd.ReceiveInfo.PaymentType = "CA";
            $scope.ReceiptAdd.ReceiveInfo.Balance = AFormatNumber(0, 2);
            $scope.ReceiptAdd.ReceiveInfo.CauseType = undefined;
            $scope.ReceiptAdd.ReceiveInfo.IsCauseType = true;
            $scope.ReceiptAdd.ReceiveInfo.IsPayment = true;

            $('#modalreceipt-receive').modal('show');
        }
    };
    
    $scope.OnChangeIsAdjustReceive = function () {
        $scope.ReceiptAdd.ReceiveInfo.DiscountType = "1";
        $scope.ReceiptAdd.ReceiveInfo.DiscountAmount = AFormatNumber(0, 2);
    };

    $scope.OnChangeFee = function ()
    {
        $scope.ReceiptAdd.ReceiveInfo.Fee = AFormatNumber($scope.ReceiptAdd.ReceiveInfo.Fee, 2);
    }

    $scope.OnChangeIsWithhollding = function () {
       
        var type = [];
        if ($scope.ReceiptAdd.ReceiveInfo.IsPayment)
            type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '1'; });
        else
            type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == '0'; });

        if (type.length > 0)
            $scope.ReceiptAdd.ReceiveInfo.SelectWithholding = type[0].ID;
        $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    };

    $scope.OnChangeWithholldingReceipt = function () {
        if ($scope.ReceiptAdd.ReceiveInfo.SelectWithholding == '0') {
            $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount = AFormatNumber(0, 2);
            $scope.OnChangeReceiveCalculate();
        }
        else {
            $scope.OnChangeReceiveCalculate();
        }
    };

    $scope.OnClickChangePaymentAmount = function ()
    {
        $scope.ReceiptAdd.ReceiveInfo.IsPayment = false
        $scope.ReceiptAdd.ReceiveInfo.SelectWithholding = '0';
        $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount = AFormatNumber(0, 2);
        $scope.OnChangeReceiveCalculate();
    }

    $scope.OnChangePaymentType = function () {
        if ($scope.ReceiptAdd.ReceiveInfo.PaymentType == 'TR') {
            var type = $scope.Parameter.BizBank.filter(function (item) { return item.UID == $scope.Parameter.BizBank[0].UID; });
            if (type.length > 0)
                $scope.ReceiptAdd.ReceiveInfo.SelectBankTransfer = type[0];
        }
        else if ($scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CQ') {
            $scope.ReceiptAdd.ReceiveInfo.SelectBank = undefined;
            $scope.ReceiptAdd.ReceiveInfo.ChequeDate = GetDatetimeNow();
            $scope.ReceiptAdd.ReceiveInfo.ChequeNo = '';
        }
        else if ($scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CC') {
            $scope.ReceiptAdd.ReceiveInfo.SelectBank = undefined;
        }
    };

    $scope.OnChangeReceiveCalculate = function () {
        try {
            var grandtotal = 0, netpayment = 0, discount = 0, withholding = 0, netgrand = 0, pricediscount = 0, pricevat, balance = 0;
            grandtotal = $scope.ReceiptAdd.ReceiveInfo.PaymentAmount == undefined || $scope.ReceiptAdd.ReceiveInfo.PaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiveInfo.PaymentAmount); // Discount Percenter
            pricediscount = $scope.ReceiptAdd.TotalAfterDiscountAmount == undefined || $scope.ReceiptAdd.TotalAfterDiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalAfterDiscountAmount); // Discount Percenter
            pricevat = $scope.ReceiptAdd.TotalBeforeVatAmount == undefined || $scope.ReceiptAdd.TotalBeforeVatAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.TotalBeforeVatAmount); // Discount Percenter
            netpayment = $scope.ReceiptAdd.ReceiveInfo.NetPaymentAmount == undefined || $scope.ReceiptAdd.ReceiveInfo.NetPaymentAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiveInfo.NetPaymentAmount); // Discount Percenter
            discount = $scope.ReceiptAdd.ReceiveInfo.DiscountAmount == undefined || $scope.ReceiptAdd.ReceiveInfo.DiscountAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiveInfo.DiscountAmount); // Discount Percenter
            withholding = $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount == undefined || $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount === '' ? 0 : detectFloat($scope.ReceiptAdd.ReceiveInfo.WithholdingAmount); // Discount Percenter


            if ($scope.ReceiptAdd.ReceiveInfo.IsWithholdingTax) {
                var type = $scope.Parameter.Param_Quotation.Param_WithholdingRateReceipt.filter(function (item) { return item.ID == $scope.ReceiptAdd.ReceiveInfo.SelectWithholding; });
                if (type.length > 0)
                    withholdingrate = type[0].Rate;

                if ($scope.ReceiptAdd.ReceiveInfo.SelectWithholding == '0')
                    withholding = withholding;
                else {
                    if ($scope.ReceiptAdd.TaxType == 'N')
                        withholding = pricediscount * withholdingrate / 100;
                    else if ($scope.ReceiptAdd.TaxType == 'V')
                        withholding = pricevat * withholdingrate / 100;
                }

                grandtotal = grandtotal - withholding;
            }
            else
                grandtotal = grandtotal;

            grandtotal = grandtotal - discount;

            if (!$scope.ReceiptAdd.ReceiveInfo.IsPayment) {
                balance = netpayment - grandtotal;
            }
            else
                netpayment = grandtotal;

            if (balance < 0) {
                $scope.ReceiptAdd.ReceiveInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiveInfo.IsText = 'เงินขาด';
                $scope.ReceiptAdd.ReceiveInfo.BalanceType = 'Lost';
                $scope.ReceiptAdd.ReceiveInfo.CauseType = undefined;
                balance = balance * (-1);
            }
            else if (balance > 0) {
                $scope.ReceiptAdd.ReceiveInfo.IsCauseType = false;
                $scope.ReceiptAdd.ReceiveInfo.IsText = 'เงินเกิน';
                $scope.ReceiptAdd.ReceiveInfo.BalanceType = 'Over';
                $scope.ReceiptAdd.ReceiveInfo.CauseType = undefined;
            }
            else {
                $scope.ReceiptAdd.ReceiveInfo.IsCauseType = true;
                $scope.ReceiptAdd.ReceiveInfo.IsText = '';
                $scope.ReceiptAdd.ReceiveInfo.BalanceType = 'Equal';
                $scope.ReceiptAdd.ReceiveInfo.CauseType = undefined;
            }

            $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount = AFormatNumber(withholding, 2);//
            $scope.ReceiptAdd.ReceiveInfo.NetPaymentAmount = AFormatNumber(netpayment, 2);//
            $scope.ReceiptAdd.ReceiveInfo.Balance = AFormatNumber(balance, 2);//
        }
        catch (err) { showErrorToast(err); }
    };
    
    $scope.OnClickReceiptPayment = function () {
        try {

            var adjusttype;
            if ($scope.ReceiptAdd.ReceiveInfo.DiscountType == '1')
                adjusttype = 'ส่วนลดพิเศษ';
            else if ($scope.ReceiptAdd.ReceiveInfo.DiscountType == '2')
                adjusttype = 'ค่านายหน้า/ส่วนแบ่งการขาย';
            else if ($scope.ReceiptAdd.ReceiveInfo.DiscountType == '3')
                adjusttype = 'ค่าดำเนินการ';
            else if ($scope.ReceiptAdd.ReceiveInfo.DiscountType == '4')
                adjusttype = 'ปัดเศษ';

            var paymenttype;
            if ($scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CA')
                paymenttype = 'เงินสด';
            else if ($scope.ReceiptAdd.ReceiveInfo.PaymentType == 'TR')
                paymenttype = 'โอนเงิน';
            else if ($scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CQ')
                paymenttype = 'เช็ค';
            else if ($scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CC')
                paymenttype = 'บัตรเครดิต';

            var cause;
            if ($scope.ReceiptAdd.ReceiveInfo.CauseType == '1')
                cause = 'ค่าธรรมเนียม';
            else if ($scope.ReceiptAdd.ReceiveInfo.CauseType == '2')
                cause = 'เงินขาด/เงินเกิน';

            var data = {
                DocumentKey: $scope.ReceiptAdd.PurchaseOrderKey,
                DocumentNo: $scope.ReceiptAdd.PurchaseOrderNo,
                DocumentType: headerdoc,
                CustomerName: $scope.ReceiptAdd.ReceiveInfo.CustomerName,
                PaymentAmount: $scope.ReceiptAdd.ReceiveInfo.PaymentAmount,
                PaymentDate: ToJsonDate2($scope.ReceiptAdd.ReceiveInfo.PaymentDate),
                NetPaymentAmount: ConvertToDecimal($scope.ReceiptAdd.ReceiveInfo.NetPaymentAmount),
                IsWithholdingTax: $scope.ReceiptAdd.ReceiveInfo.IsWithholdingTax ? '1' : '0',
                WithholdingKey: $scope.ReceiptAdd.ReceiveInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiveInfo.SelectWithholding.ID : undefined,
                WithholdingRate: $scope.ReceiptAdd.ReceiveInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiveInfo.SelectWithholding.Rate : undefined,
                WithholdingAmount: $scope.ReceiptAdd.ReceiveInfo.IsWithholdingTax ? $scope.ReceiptAdd.ReceiveInfo.WithholdingAmount : undefined,
                IsDiscount: $scope.ReceiptAdd.ReceiveInfo.IsDiscount ? '1' : '0',
                DiscountType: $scope.ReceiptAdd.ReceiveInfo.IsDiscount ? $scope.ReceiptAdd.ReceiveInfo.DiscountType : undefined,
                DiscountTypeName: $scope.ReceiptAdd.ReceiveInfo.IsDiscount ? adjusttype : undefined,
                DiscountAmount: $scope.ReceiptAdd.ReceiveInfo.IsDiscount ? $scope.ReceiptAdd.ReceiveInfo.DiscountAmount : undefined,
                PaymentType: $scope.ReceiptAdd.ReceiveInfo.PaymentType,
                PaymentTypeName: paymenttype,
                BankAccountID: $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiveInfo.SelectBankTransfer.UID : undefined,
                BankCode: $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiveInfo.SelectBank.ID : undefined,
                BankName: $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiveInfo.SelectBankTransfer.BankName : $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CQ' || $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CC' ? $scope.ReceiptAdd.ReceiveInfo.SelectBank.Name : undefined,
                BankAccountNo: $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiveInfo.SelectBankTransfer.AccountNo : undefined,
                BankAccountType: $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'TR' ? $scope.ReceiptAdd.ReceiveInfo.SelectBankTransfer.DepositTypeName : undefined,
                ChequeDate: $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CQ' ? ToJsonDate2($scope.ReceiptAdd.ReceiveInfo.ChequeDate) : undefined,
                ChequeNo: $scope.ReceiptAdd.ReceiveInfo.PaymentType == 'CQ' ? $scope.ReceiptAdd.ReceiveInfo.ChequeNo : undefined,
                Comment: $scope.ReceiptAdd.ReceiveInfo.Comment,
                Balance: $scope.ReceiptAdd.ReceiveInfo.Balance,
                BalanceType: $scope.ReceiptAdd.ReceiveInfo.BalanceType,
                CauseType: $scope.ReceiptAdd.ReceiveInfo.CauseType,
                CauseTypeName: cause,
                PaymentFee : $scope.ReceiptAdd.ReceiveInfo.Fee
            };

            $http.post(baseURL + "DocumentBuy/PostBusinessReceiptPayment", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessToast();
                                $('#modalreceipt-receive').modal('hide');
                                $scope.init();
                                $scope.ReceiptAdd = [];
                            }
                            else showErrorToast(response.data.errormessage);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    catch (err) {
                        $scope.table.binding = 0;
                        showErrorToast(err);
                    }
                });

        }
        catch (err) { showErrorToast(err); }
    };
    // #endregion

    $scope.OnClickClear = function () {
        $scope.Search = [];
        $scope.Search.SelectStatus = '0';
        $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
        $scope.Search.InputToDate = GetDatetimeNowLastMonth();
        $scope.PurchaseOrder = $scope.PurchaseOrderMain;
    };

    $scope.OnClickSearch = function () {
        $scope.PurchaseOrder = $scope.PurchaseOrderMain;
        $scope.table.binding = 1;

        $scope.PurchaseOrder = _.filter($scope.PurchaseOrderMain, function (item) {

            var c1 = true, c2 = true, c3 = true, c4 = true, c5 = true;

            if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.CustomerName != undefined && (item.CustomerName).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.CustomerName == undefined || (item.CustomerName).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.PurchaseOrderNo != undefined && (item.PurchaseOrderNo).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.PurchaseOrderNo == undefined || (item.PurchaseOrderNo).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProjectName != undefined && (item.ProjectName).indexOf($scope.Search.InputFilter) > -1)
                    c3 = true;
                else if (item.ProjectName == undefined || (item.ProjectName).indexOf($scope.Search.InputFilter) < 0)
                    c3 = false;


            }

            if ($scope.Search.SelectStatus == '0') {
                c4 = true;
            }
            else if (item.PurchaseOrderStatus == $scope.Search.SelectStatus) {
                c4 = true;
            }
            else
                c4 = false;

            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != "" && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != "") {

                var fromarry = $scope.Search.InputFromDate.split('-');

                var quotationdatearry = item.PurchaseOrderDate.split('-');

                var from = new Date(fromarry[2], fromarry[1] - 1, fromarry[0]);

                var quotationdate = new Date(quotationdatearry[2], quotationdatearry[1] - 1, quotationdatearry[0]);

                var toarry = $scope.Search.InputToDate.split('-');

                var to = new Date(toarry[2], toarry[1] - 1, toarry[0]);
                //alert(quotationdate); alert(from); alert(from);
                if (quotationdate >= from && quotationdate <= to)
                    c5 = true;
                else
                    c5 = false;
            }


            if ((c1 || c2 || c3) && c4 && c5) {
                return item;
            }
        });
        $scope.table.binding = 0;
    };

    $scope.OnClickSelectType = function (type) {
        if (type == 'ALL')
            $scope.init();
    }
});