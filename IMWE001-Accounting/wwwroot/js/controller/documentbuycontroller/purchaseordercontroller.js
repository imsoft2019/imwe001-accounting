﻿WEACCTAPP.controller('purchaseordercontroller', function ($scope, $http, $timeout, GlobalVar, paramService, $q) {
    var config = GlobalVar.HeaderConfig;
    var baseURL = $("base")[0].href;

   // $scope.BusinessInvoice = [];
    //$scope.BusinessInvoiceMain = [];

    $scope.PurchaseOrder = [];
    $scope.PurchaseOrderMain = [];

    $scope.Parameter = [];
    $scope.table = [];
    $scope.Search = [];

    $scope.BusinessProfile = [];
    $scope.BusinessEmployee = [];
    $scope.EmailAdd = [];

    var gEmp = paramService.getProfile();
    var gProfile = paramService.getBusinessProfile();


    var DocumentAddURL = "DocumentBuy/PurchaseOrderAdd";


    function QueryString(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }


    $scope.OnClickDocumentReference = function (key, doctype,no) {
        if (doctype == 'RI')
            window.location.href = baseURL + "DocumentBuy/ReceivingInventoryAdd?ref_key=" + key + "&ref_info=PO&ref_no=" + no;
        else if (doctype == 'PV')
            window.location.href = baseURL + "DocumentExpenses/ExpensesAdd?ref_key=" + key + "&ref_info=PO&ref_no=" + no;
        
    };


    $scope.init = function () {
        try {
            $scope.table.binding = 1;
            $scope.Search = [];
            $scope.Search.SelectStatus = '0';
            $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
            $scope.Search.InputToDate = GetDatetimeNowLastMonth();

            var qq = $q.all([gProfile, gEmp]).then(function (data) {
                try {
                    /****** GetDocument NO ******/
                    if (data[0] != undefined && data[0] != "") {
                        if (data[0].data.responsecode == '200' && data[0].data.responsedata != undefined && data[0].data.responsedata != "") { $scope.BusinessProfile = data[0].data.responsedata; }
                        else if (data[0].data.responsecode == '400') { showErrorToast(data[0].data.errormessage); }
                    }

                    /****** Get Customer ******/
                    if (data[1] != undefined && data[1] != "") {
                        if (data[1].data.responsecode == '200' && data[1].data.responsedata != undefined && data[1].data.responsedata != "") {
                            $scope.BusinessEmployee = $scope.Parameter.Param_Contact = data[1].data.responsedata;
                        }
                        else if (data[1].data.responsecode == '400') { showErrorToast(data[1].data.errormessage); }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });

            $http.get(baseURL + "DocumentBuy/GetBusinessPO?key=" + makeid())
                .then(function (response) {

                    if (response != undefined && response != "") {
                        if (response.data.responsecode == '200') {
                            $scope.PurchaseOrderMain = response.data.responsedata;

                            var grandtotal = 0;
                            _.each($scope.PurchaseOrderMain, function (item) {
                                item.PurchaseOrderDate = formatDate(item.PurchaseOrderDate);
                                if (item.CreditType == 'CD')
                                    item.DueDate = formatDate(item.DueDate);
                                if (item.PurchaseOrderStatus != '3')
                                    grandtotal = grandtotal + item.GrandAmount;
                                item.GrandAmountText = AFormatNumber(item.GrandAmount, 2);
                            });
                            $scope.BillingSummary = AFormatNumber(grandtotal, 2);
                            $scope.PurchaseOrder = $scope.PurchaseOrderMain;


                            $scope.Search.BillingType = 'ALL';
                            $scope.table.binding = 0;
                        }
                        else if (response.data.responsecode == '400') {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                });
        }
        catch (err) {
            showErrorToast(err);
        }
    };

   
    $scope.OnClickAdd = function () {
        window.location.href = baseURL + DocumentAddURL;
    };

    $scope.OnClickUpdate = function (key) {
        window.location.href = baseURL + DocumentAddURL + "?key=" + key;
    };

    $scope.OnClickNextWorkflow = function (quotationkey, tostatus) {
        try {
            var data = {
                PurchaseOrderKey: quotationkey,
               PurchaseOrderStatus: tostatus
            };
            $http.post(baseURL + "DocumentBuy/UpdateStatusPO", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                $scope.init();
                                showSuccessWorkToast();
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }
    };

    $scope.OnClickDuplicate = function (key) {
        window.location.href = baseURL + DocumentAddURL + "?key=" + key + "&dup_ref=1";

    };

    $scope.OnclickDelete = function (val) {
        var filter = _.where($scope.PurchaseOrder, {PurchaseOrderKey: val })[0];
        if (filter != undefined) {
            $scope.PurchaseOrderAdd = [];
            $scope.PurchaseOrderAdd = angular.copy(filter);
            if ($scope.PurchaseOrderAdd.PurchaseOrderStatus == '1')
                $('#modalPO-del').modal('show');
            else
                $('#modalPO-alert').modal('show');
        }
    };

    $scope.OnClickConfirmDelete = function () {

        var data = {
            PurchaseOrderKey: $scope.PurchaseOrderAdd.PurchaseOrderKey
        };

        $http.post(baseURL + "DocumentBuy/DeletePO", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {
                            showDeleteSuccessToast();
                            $('#modalPO-del').modal('hide');
                            $scope.init();
                            $scope.QuotationAdd = [];
                        }
                        else
                            showErrorToast(response.data.errormessage);
                    }
                    else {
                        showErrorToast(response.data.errormessage);
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };

    $scope.OnClickReport = function (dockey, docno) {
        $('#modal-report').modal('show');
        $scope.document = [];
        $scope.document.bindding = 1;
        $scope.document.DocNo = docno;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: 'PO',
                DocumentNo: docno,
                TypeAction: 'Report',
                Content : 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {

                                PDFObject.embed(baseURL + 'uploads/report/' + response.data.responsedata, "#example1");
                                $scope.document.bindding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.document.bindding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickDownload = function (dockey, docno) {
        $scope.table.binding = 1;
        try {
            var data = {
                DocumentKey: dockey,
                DocumentType: 'PO',
                DocumentNo: docno,
                TypeAction: 'Download',
                Content: 'ORIGINAL'
            };
            $http.post(baseURL + "DocumentSell/GetReportDocument", data, config).then(
                function (response) {
                    try {
                        if (response != undefined && response != "") {
                            if (response.data.responsecode == 200) {
                                showSuccessText('ดาวน์โหลดสำเร็จ');
                                window.location = baseURL + "DocumentSell/DownloadFromPath?file=" + response.data.responsedata;
                                $scope.table.binding = 0;
                            }
                            else {
                                showErrorToast(response.data.errormessage);
                                $scope.table.binding = 0;
                            }
                        }
                    }
                    catch (err) {
                        showErrorToast(err);
                    }
                });
        }
        catch (err) {
            showWariningToast(err);
        }

    };

    $scope.OnClickClear = function () {
        $scope.Search = [];
        $scope.Search.SelectStatus = '0';
        $scope.Search.InputFromDate = GetDatetimeNowFirstMonth();
        $scope.Search.InputToDate = GetDatetimeNowLastMonth();
        $scope.PurchaseOrder = $scope.PurchaseOrderMain;
    };

    $scope.OnClickSearch = function () {
        $scope.PurchaseOrder = $scope.PurchaseOrderMain;
        $scope.table.binding = 1;

        $scope.PurchaseOrder = _.filter($scope.PurchaseOrderMain, function (item) {

            var c1 = true, c2 = true, c3 = true, c4 = true, c5 = true;

            if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "") {

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.CustomerName != undefined && (item.CustomerName).indexOf($scope.Search.InputFilter) > -1)
                    c1 = true;
                else if (item.CustomerName == undefined || (item.CustomerName).indexOf($scope.Search.InputFilter) < 0)
                    c1 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.PurchaseOrderNo != undefined && (item.PurchaseOrderNo).indexOf($scope.Search.InputFilter) > -1)
                    c2 = true;
                else if (item.PurchaseOrderNo == undefined || (item.PurchaseOrderNo).indexOf($scope.Search.InputFilter) < 0)
                    c2 = false;

                if ($scope.Search.InputFilter != undefined && $scope.Search.InputFilter != "" && item.ProjectName != undefined && (item.ProjectName).indexOf($scope.Search.InputFilter) > -1)
                    c3 = true;
                else if (item.ProjectName == undefined || (item.ProjectName).indexOf($scope.Search.InputFilter) < 0)
                    c3 = false;


            }

            if ($scope.Search.SelectStatus == '0') {
                c4 = true;
            }
            else if (item.PurchaseOrderStatus == $scope.Search.SelectStatus) {
                c4 = true;
            }
            else
                c4 = false;

            if ($scope.Search.InputFromDate != undefined && $scope.Search.InputFromDate != "" && $scope.Search.InputToDate != undefined && $scope.Search.InputToDate != "") {

                var fromarry = $scope.Search.InputFromDate.split('-');

                var quotationdatearry = item.PurchaseOrderDate.split('-');

                var from = new Date(fromarry[2], fromarry[1] - 1, fromarry[0]);

                var quotationdate = new Date(quotationdatearry[2], quotationdatearry[1] - 1, quotationdatearry[0]);

                var toarry = $scope.Search.InputToDate.split('-');

                var to = new Date(toarry[2], toarry[1] - 1, toarry[0]);
                //alert(quotationdate); alert(from); alert(from);
                if (quotationdate >= from && quotationdate <= to)
                    c5 = true;
                else
                    c5 = false;
            }


            if ((c1 || c2 || c3) && c4 && c5) {
                return item;
            }
        });
        $scope.table.binding = 0;
    };

    $scope.OnClickSelectType = function (type)
    {
        if (type == 'ALL')
            $scope.init();
    }

    $scope.OnClickSendMail = function (dockey, docno) {
        var quo = _.where($scope.PurchaseOrder, { PurchaseOrderKey: dockey })[0];
        if (quo != undefined) {
            $scope.EmailAdd = [];
            $scope.EmailAdd.DocKey = dockey;
            $scope.EmailAdd.DocNo = docno;
            $scope.EmailAdd.MailToMe = true;
            $scope.EmailAdd.MailFrom = $scope.BusinessEmployee.Email;
            $scope.EmailAdd.MailTo = quo.CustomerContactEmail;
            $scope.EmailAdd.Subject = 'เอกสาร ' + docno;
            $scope.EmailAdd.Message = 'เรียน ' + quo.CustomerName + '\n\n' + $scope.BusinessProfile.BusinessName + ' เลขที่ ' + docno + ' มาให้ในอีเมลนี้\n\nกรุณาคลิกที่ไฟล์แนบ เพื่อดาวน์โหลดเอกสารของคุณ\n\nด้วยความเคารพ\n' + $scope.BusinessEmployee.EmployeeName + '\n' + $scope.BusinessProfile.BusinessName;
            $('#modalemail-send').modal('show');
        }

    };

    $scope.OnClickConfirmEmail = function () {
        $('#modalemail-send').modal('hide');
        var data = {
            MailFrom: $scope.EmailAdd.MailFrom,
            MailTo: $scope.EmailAdd.MailTo,
            MailCC: $scope.EmailAdd.MailCC,
            MailIsMe: $scope.EmailAdd.MailToMe ? "1" : "0",
            Subject: $scope.EmailAdd.Subject,
            Message: $scope.EmailAdd.Message,
            DocumentKey: $scope.EmailAdd.DocKey,
            DocumentNo: $scope.EmailAdd.DocNo,
            DocumentType: 'PO',
            Content: 'ORIGINAL'
        };

        $http.post(baseURL + "DocumentSell/PostEmailToSending", data, config).then(
            function (response) {
                try {
                    if (response != undefined && response != "") {
                        if (response.data.responsecode == 200) {

                            showSuccessText(response.data.responsedata);
                        }
                        else {
                            showErrorToast(response.data.errormessage);
                        }
                    }
                }
                catch (err) {
                    showErrorToast(err);
                }
            });
    };
});