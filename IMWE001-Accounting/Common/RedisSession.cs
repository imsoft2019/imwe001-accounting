﻿using CSRedis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_Accounting.Common
{
    /*
        test1 tt = new test1() { ID = "1",Name= "bell" };
        var asdf = new RedisSession<test1>();
        asdf["test001"] = tt;
        var ff = asdf["test001"];
     */

    public class RedisSession<T> where T : class
    {
        string HostName = "localhost";
        int Port = 6379;
        int ExpireSecond = 3600;

        public RedisSession()
        {
            var _config = ApiConfigureServices.ConfigureAppSetting();
            HostName = _config["Redis:HostName"];
            Port = Convert.ToInt32(_config["Redis:Port"]);
            ExpireSecond = Convert.ToInt32(_config["Redis:Expired"]);
        }

        public T this[string Key]
        {
            get
            {
                return GetValue(Key);
            }
            set
            {
                SetValue(Key, value);
            }
        }

        public void ClearAll()
        {
            using (var redis = new RedisClient(HostName, Port))
                redis.FlushAll();
        }

        private T GetValue(string Key)
        {
            T result = null;

            using (var redis = new RedisClient(HostName, Port))
            {
               //var a = r.Echo("Hello");
                var containKey = redis.Keys(Key);

                if (containKey.Length > 0)
                {
                    var tmp = redis.Get(Key);

                    if (!string.IsNullOrEmpty(tmp))
                    {
                        try
                        {
                            result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(tmp);
                        }
                        catch { result = null; }
                    }
                }
            }
                       
            return result;
        }
        private void SetValue(string Key, T Value)
        {
            string JsonValue = string.Empty;

            JsonValue = Newtonsoft.Json.JsonConvert.SerializeObject(Value);

            using (var redis = new RedisClient(HostName, Port))
            {
                if (string.IsNullOrEmpty(JsonValue))
                {
                    var tmpkey = redis.Keys(Key);
                    if (tmpkey != null && tmpkey.Length > 0)
                        redis.Del(tmpkey);
                }
                else
                {
                    redis.Set(Key, JsonValue);
                    redis.Expire(Key, ExpireSecond);
                }
            }
        }
    }
}
