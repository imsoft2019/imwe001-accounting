﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMWE001_Accounting.Models
{
    public class LoginAuthority
    {
        public Guid BusinessID { get; set; }
        public Guid UID { get; set; }
        public string EmployeeName { get; set; }
        public string Position { get; set; }

        public static LoginAuthority GetProfileUser()
        {
            LoginAuthority login = new LoginAuthority()
            {
                BusinessID = new Guid("d976337a-ae44-11ea-b90b-42010a940007"),
                EmployeeName = "วีแอคเค้า ทดสอบ-01",
                Position = "เจ้าของกิจการ",
            };

            return login;
        }
    }
}
