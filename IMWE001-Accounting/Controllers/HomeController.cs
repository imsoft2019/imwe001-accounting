﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using IMWE001_AccountingModels.User;
using Newtonsoft.Json;
using System.Text;

using Microsoft.AspNetCore.Http;
using IMWE001_AccountingModels.EventLog;
//using Microsoft.AspNetCore.Mvc;
//using Session_State.Models;

namespace IMWE001_Accounting.Controllers
{

    public class HomeController : BaseController
    {
        public HomeController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }

        public IActionResult DashBoard()
        {
            GetViewBag();
            return View();
        }


        public IActionResult Login()
        {
            LoginViewModel defaults = new LoginViewModel();
            defaults.UserName = "admin@imsoft.com";
            defaults.PWD = "1234";
            return View(defaults);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login(LoginViewModel model)
        {
            // PostApi("")
            try
            {
                if (string.IsNullOrEmpty(model.UserName) || string.IsNullOrEmpty(model.PWD))
                    throw new Exception("กรุณากรอกข้อมูลอีเมลและรหัสผ่านให้ครบถ้วน");

                LoginViewModel input = new LoginViewModel()
                {
                    UserName = model.UserName,
                    PWD = MD5Hash(model.PWD)
                };



                var jsonObject = JsonConvert.SerializeObject(input);
                var user = PostApi<UserProfile>("user/Login", jsonObject);

                if (user.IsDelete == 1)
                {
                    var tmp = JsonConvert.SerializeObject(user);
                    HttpContext.Session.SetString("UserLogin", tmp);


                    //string url = "DashBoard";

                    //return Redirect(url);
                    if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Owner) || Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Account))
                        return RedirectToAction("DashBoard");
                    else if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Manager) || Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Employee))
                        return RedirectToAction("Quotation", "DocumentSell");
                    else
                        return RedirectToAction("Login");
                }
                else
                {
                    ViewBag.ErrorMsg = "ไม่พบผู้ใช้งาน";
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMsg = ex.Message;
                return View();
            }
        }

        [HttpGet]
        public IActionResult GetLogActivity(string key)
        {
            try
            {
                List<Log_Activity> responseval = new List<Log_Activity>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Log_Activity>>(string.Format("business/getactivity/{0}", user.BusinessID));

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
