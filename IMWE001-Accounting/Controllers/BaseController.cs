﻿using IMWE001_Accounting.Common;
using IMWE001_Accounting.Models;
using IMWE001_AccountingModels.Parameter;
using IMWE001_AccountingModels.User;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace IMWE001_Accounting.Controllers
{
    public class BaseController : Controller
    {

        protected IConfiguration _config;
        protected IHostingEnvironment _hostingEnvironment;
        public string siteenvironment = string.Empty;  

        public BaseController(IHostingEnvironment hostingEnvironment)
        {
            _config = ApiConfigureServices.ConfigureAppSetting();
            _hostingEnvironment = hostingEnvironment;
            siteenvironment =  _config["Environment"];
        }

        public UserProfile GetProfileUser()
        {
            //LoginAuthority login = new LoginAuthority()
            //{
            //    BusinessID = new Guid("d976337a-ae44-11ea-b90b-42010a940007"),
            //    EmployeeName = "วีแอคเค้า ทดสอบ-01",
            //    Position = "เจ้าของกิจการ",
            //    UID = new Guid("907b85b9-8f91-4d1f-a5a1-e742a69007a7")
            //};
            var userString = HttpContext.Session.GetString("UserLogin");
            UserProfile result = JsonConvert.DeserializeObject<UserProfile>(userString);
            return result;
        }

        public void GetViewBag()
        {
            UserProfile user = GetProfileUser();
            if (user != null)
            {
                ViewBag.EmployeeName = user.EmployeeName;
                ViewBag.PermissionName = user.PositionName;
                ViewBag.PositionCode = user.Position;
            }
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
            if (((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor).ActionName != "Login")
            {
                if (HttpContext.Session.GetString("UserLogin") == null)
                    context.Result = new RedirectToActionResult("Login", "Home", null);
                else
                    GetViewBag();
            }
        }

        private HttpClient InitHttpClient()
        {
            // LoginAuthority user = GetProfileUser();

            string pathlink = _config["Micro-API_WeAccount:Path"];
            string tokenid = _config["Weacct-Apim-Subscription-Key:TokenID"];

            var result = new HttpClient { BaseAddress = new Uri(pathlink) };

            if (pathlink.Substring(0, 5).ToUpper() == "HTTPS")
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }
            result.DefaultRequestHeaders.Add("Weacct-Apim-Subscription-Key", tokenid);

            return result;
        }

        private HttpClient InitHttpClientSite2()
        {
            // LoginAuthority user = GetProfileUser();

            string pathlink = _config["Report-API_WeAccount:Path"];

            var result = new HttpClient { BaseAddress = new Uri(pathlink) };

            if (pathlink.Substring(0, 5).ToUpper() == "HTTPS")
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }

            return result;
        }

        public T GetApi<T>(string url)
        {
            T result;

            using (HttpClient client = InitHttpClient())
            {
                HttpResponseMessage responseMsg = client.GetAsync(url).Result;

                if (responseMsg.IsSuccessStatusCode)
                {
                    result = responseMsg.Content.ReadAsAsync<T>().Result;
                }
                else
                {
                    var err = responseMsg.Content.ReadAsAsync<ResponseError>().Result;
                    throw new Exception(err.ResponseMessage);
                }

            }

            return result;
        }

        public object GetApi(string url)
        {
            object result;

            using (HttpClient client = InitHttpClient())
            {
                HttpResponseMessage responseMsg = client.GetAsync(url).Result;

                if (responseMsg.IsSuccessStatusCode)
                {
                    result = responseMsg.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    var err = responseMsg.Content.ReadAsAsync<ResponseError>().Result;
                    throw new Exception(err.ResponseMessage);
                }

            }

            return result;
        }

        public T GetApiSite2<T>(string url)
        {
            T result;

            using (HttpClient client = InitHttpClientSite2())
            {
                HttpResponseMessage responseMsg = client.GetAsync(url).Result;

                if (responseMsg.IsSuccessStatusCode)
                {
                    result = responseMsg.Content.ReadAsAsync<T>().Result;
                }
                else
                {
                    var err = responseMsg.Content.ReadAsAsync<ResponseError>().Result;
                    throw new Exception(err.ResponseMessage);
                }

            }

            return result;
        }

        public object PostApi(string URL, string jsoninput)
        {
            using (HttpClient client = InitHttpClient())
            {
                //var jsonObject = JsonConvert.SerializeObject(input);
                var content = new StringContent(jsoninput.ToString(), Encoding.UTF8, "application/json");

                HttpResponseMessage responseMsg = client.PostAsync(URL, content).Result;
                string msg = responseMsg.Content.ReadAsStringAsync().Result;

                if (responseMsg.IsSuccessStatusCode)
                    return WriteJson(new
                    {
                        responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                        responsedata = "บันทึกข้อมูลเรียบร้อย"
                    });
                else
                {
                    var obj = JsonConvert.DeserializeObject<ResponseError>(msg);
                    // ResponseError errormessage = JsonConvert.DeserializeObject<ResponseError>(obj.ToString());
                    throw new Exception(obj.ResponseMessage);
                }
            }
        }

        public object PostApiReturnKey(string URL, string jsoninput)
        {
            using (HttpClient client = InitHttpClient())
            {
                //var jsonObject = JsonConvert.SerializeObject(input);
                var content = new StringContent(jsoninput.ToString(), Encoding.UTF8, "application/json");

                HttpResponseMessage responseMsg = client.PostAsync(URL, content).Result;
                string msg = responseMsg.Content.ReadAsStringAsync().Result;

                if (responseMsg.IsSuccessStatusCode)
                    return WriteJson(new
                    {
                        responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                        responsedata = msg
                    });
                else
                {
                    var obj = JsonConvert.DeserializeObject<ResponseError>(msg);
                    // ResponseError errormessage = JsonConvert.DeserializeObject<ResponseError>(obj.ToString());
                    throw new Exception(obj.ResponseMessage);
                }
            }
        }

        public T PostApi<T>(string URL, string jsoninput)
        {
            using (HttpClient client = InitHttpClient())
            {
                //var jsonObject = JsonConvert.SerializeObject(input);
                var content = new StringContent(jsoninput.ToString(), Encoding.UTF8, "application/json");

                HttpResponseMessage responseMsg = client.PostAsync(URL, content).Result;
                string msg = responseMsg.Content.ReadAsStringAsync().Result;

                if (responseMsg.IsSuccessStatusCode)
                    return JsonConvert.DeserializeObject<T>(msg);
                else
                {
                    var obj = JsonConvert.DeserializeObject<ResponseError>(msg);
                    // ResponseError errormessage = JsonConvert.DeserializeObject<ResponseError>(obj.ToString());
                    throw new Exception(obj.ResponseMessage);
                }
            }
        }

        public T PostApiSite2<T>(string URL, string jsoninput)
        {
            using (HttpClient client = InitHttpClientSite2())
            {
                //var jsonObject = JsonConvert.SerializeObject(input);
                var content = new StringContent(jsoninput.ToString(), Encoding.UTF8, "application/json");

                HttpResponseMessage responseMsg = client.PostAsync(URL, content).Result;
                string msg = responseMsg.Content.ReadAsStringAsync().Result;

                if (responseMsg.IsSuccessStatusCode)
                    return JsonConvert.DeserializeObject<T>(msg);
                else
                {
                    var obj = JsonConvert.DeserializeObject<ResponseError>(msg);
                    // ResponseError errormessage = JsonConvert.DeserializeObject<ResponseError>(obj.ToString());
                    throw new Exception(obj.ResponseMessage);
                }
            }
        }

        [NonAction]
        public virtual JsonResult WriteJson(object data)
        {
            try
            {

                JsonSerializerSettings sett = new JsonSerializerSettings();
                sett.Converters.Add(new UtcDateTimeConverter());
                return new JsonResult(data, sett);
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        protected string MD5Hash(string input)
        {
            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.ASCII.GetBytes(input));

                // byte[] result = md5.Hash;

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    //change it into 2 hexadecimal digits  
                    //for each byte  
                    strBuilder.Append(result[i].ToString("x2"));
                }

                return strBuilder.ToString();

            }
        }

        #region Parameter

        public T GetParam<T>(string url = @"parameter/get/" , string sessionName = "") where T : class
        {
            T result = null; ;

            try
            {
                string paramurl = url;
                string Name = sessionName;

                if (sessionName == "")
                    Name = MappignParam()[typeof(T)];

                if (url == @"parameter/get/")
                    paramurl = url + Name;

                //result = null;
                

                if (siteenvironment == "Develop")
                    result = GetApi<T>(paramurl);
                else
                {
                    var _config = ApiConfigureServices.ConfigureAppSetting();
                    var OnRedis = (_config["Redis:Enable"] == "Yes");

                    var redisParam = new RedisSession<T>();
                    if (OnRedis)
                    {
                        var tmp = redisParam[Name];

                        if (tmp == null)
                        {
                            result = GetApi<T>(paramurl);

                            if (OnRedis)
                                redisParam[Name] = result;
                        }
                        else
                            result = tmp;
                    }
                }
            }
            catch(Exception ex)
            {
                result = null;
            }

            return result;
        }

        private Dictionary<Type, string> MappignParam()
        {
            var result = new Dictionary<Type, string>();

            result.Add(typeof(Param_Position), "position");
            result.Add(typeof(Param_Bank), "bank");

            return result;
        }

        public class ResponseMessage
        {
            public string responsecode { get; set; }
            public string reponsedata { get; set; }
        }

        #endregion
    }



}
