﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using IMWE001_Accounting.Common;
using IMWE001_AccountingModels.Parameter;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.User;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace IMWE001_Accounting.Controllers
{
    [Produces("application/json")]
    public class ProductController : BaseController
    {
        public ProductController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }

        public IActionResult Products()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Products
        [HttpGet]
        public IActionResult GetBusinessProducts(string key)
        {
            try
            {
                List<Product> responseval = new List<Product>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Product>>(string.Format("products/getproduct/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessProducts([FromBody]Product input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApiReturnKey(string.Format("products/postproduct/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessProductsReply([FromBody]Product input)
        {
            try
            {
                Product responseval = new Product();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Product>(string.Format("products/postproductreply/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult UploadPictureProducts(List<IFormFile> files, int productkey)
        {
            try
            {
                long size = files.Sum(f => f.Length);

                // full path to file in temp location

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Products"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Empty;

                foreach (var formFile in files)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    fullpath = Path.Combine(fullpath, filename);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(fullpath, FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }

                UserProfile user = GetProfileUser();

                Product input = new Product();
                input.BusinessID = user.BusinessID.Value;
                input.ProductKey = productkey;
                input.PicturePath = fullpath;
                input.ThumbnailPath = filename;

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("products/postproductupload/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessProducts([FromBody]Product input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("products/deleteproduct/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }


        public IActionResult GetParameterProducts(string key)
        {
            try
            {
               // RParameter responseval = new RParameter();
                UserProfile user = GetProfileUser();
                // responseval = GetApi<RParameter>(string.Format("parameter/get/product/{0}", user.BusinessID));

                var responseval = GetParam<RParameter>(string.Format("parameter/get/product/{0}", user.BusinessID), "param:product");
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        private HttpClient GetHttpClient(string baseAddress)
        {
            return new HttpClient { BaseAddress = new Uri(baseAddress) };
        }
    }
}
