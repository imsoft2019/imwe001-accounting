﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using IMWE001_Accounting.Common;
using IMWE001_AccountingModels.Parameter;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.User;
using Microsoft.AspNetCore.Http;
using System.IO;
using IMWE001_Accounting.Common.RDService;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Cors;

namespace IMWE001_Accounting.Controllers
{
    [Produces("application/json")]
    public class DocumentSellController : BaseController
    {
        public DocumentSellController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }

        #region Pages
        public IActionResult Quotation()
        {
            return View();
        }

        public IActionResult QuotationAdd()
        {
            return View();
        }

        public IActionResult BillingNote()
        {
            return View();
        }

        public IActionResult BillingNoteAdd()
        {
            return View();
        }

        public IActionResult BillingNoteCumulativeAdd()
        {
            return View();
        }

        public IActionResult Invoice()
        {
            return View();
        }

        public IActionResult InvoiceAdd()
        {
            return View();
        }

        public IActionResult Receipt()
        {
            return View();
        }

        public IActionResult ReceiptAdd()
        {
            return View();
        }

        public IActionResult ReceiptCumulativeAdd()
        {
            return View();
        }

        public IActionResult CashSale()
        {
            return View();
        }

        public IActionResult CashSaleAdd()
        {
            return View();
        }

        public IActionResult CreditNote()
        {
            return View();
        }

        public IActionResult CreditNoteAdd()
        {
            return View();
        }

        public IActionResult DebitNote()
        {
            return View();
        }

        public IActionResult DebitNoteAdd()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #endregion 

        #region Reference 
        public IActionResult GetBusinessDcoumentReference(string key, string DocumentType, int DocumentKey)
        {
            try
            {
                UserProfile user = GetProfileUser();
                if (DocumentType == "QT")
                    return GetBusinessQuotationbykey(DocumentKey, key);
                else if (DocumentType == "BL")
                    return GetBusinessBillingkey(DocumentKey, key);
                else if (DocumentType == "INV")
                    return GetBusinessInvoicebykey(DocumentKey, key);
                else if (DocumentType == "CN")
                    return GetBusinessCreditNotebykey(DocumentKey, key);
                else if (DocumentType == "DN")
                    return GetBusinessDebitNotebykey(DocumentKey, key);
                else
                    throw new Exception("กรุณาส่งเลขอ้างอิงเอกสาร");

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostEmailToSending([FromBody]PostEmail input)
        {
            try
            {
                RParameter responseval = new RParameter();
                UserProfile user = GetProfileUser();
                fn_Email fnmail = new fn_Email();


                GetReport fnreport = new GetReport()
                {
                    DocumentKey = input.DocumentKey,
                    DocumentNo = input.DocumentNo,
                    Content = input.Content,
                    DocumentType = input.DocumentType,
                    TypeAction = "Download"
                };

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = input.DocumentNo + ".pdf";
                fullpath = Path.Combine(fullpath, filename);

                GetReportDocument(fnreport);

                fnmail.smtp = _config["Weacct-Email:SmtpHost"];
                fnmail.port = Convert.ToInt32(_config["Weacct-Email:Port"]);
                fnmail.mailinfo = _config["Weacct-Email:UserName"];
                fnmail.mailname = user.EmployeeName;
                fnmail.password = _config["Weacct-Email:Password"];
                fnmail.mailfrom = input.MailFrom;
                fnmail.mailto = input.MailTo + (input.MailIsMe == "1" ? "," + input.MailFrom : "");
                fnmail.subject = input.Subject;
                fnmail.message = input.Message;
                fnmail.path = fullpath;

                fnmail.SendMail();

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = "ส่งอีเมลสำเร็จ"
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult GetReportDocument([FromBody]GetReport input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                UserProfile user = GetProfileUser();
                ResponseMessage responseval = new ResponseMessage();
                if (input.DocumentType == "QT")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetQuotation/{0}/{1}?content={2}", user.BusinessID.ToString(), input.DocumentKey.ToString(), "ORIGINAL"));
                else if (input.DocumentType == "BL" || input.DocumentType == "CBL")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetBilling/{0}/{1}/{2}", user.BusinessID.ToString(), input.DocumentKey.ToString(), input.Content));
                else if (input.DocumentType == "INV")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetInvoice/{0}/{1}/{2}", user.BusinessID.ToString(), input.DocumentKey.ToString(), input.Content));
                else if (input.DocumentType == "RE" || input.DocumentType == "CRE")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetReceipt/{0}/{1}/{2}", user.BusinessID.ToString(), input.DocumentKey.ToString(), input.Content));
                else if (input.DocumentType == "CN")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetCreditNote/{0}/{1}/{2}", user.BusinessID.ToString(), input.DocumentKey.ToString(), input.Content));
                else if (input.DocumentType == "DN")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetDeditNote/{0}/{1}/{2}", user.BusinessID.ToString(), input.DocumentKey.ToString(), input.Content));
                else if (input.DocumentType == "RI")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetRI/{0}/{1}", user.BusinessID.ToString(), input.DocumentKey.ToString()));
                else if (input.DocumentType == "PO")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetPurchaseOrder/{0}/{1}", user.BusinessID.ToString(), input.DocumentKey.ToString()));
                else if (input.DocumentType == "EX")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetExpense/{0}/{1}", user.BusinessID.ToString(), input.DocumentKey.ToString()));
                else if (input.DocumentType == "CA")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetCashSale/{0}/{1}/{2}", user.BusinessID.ToString(), input.DocumentKey.ToString(), input.Content));
                else if (input.DocumentType == "WT")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetWithholdingTax/{0}/{1}", user.BusinessID.ToString(), input.DocumentKey.ToString()));


                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = input.DocumentNo + ".pdf";
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsepath = pathlink
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult GetReportSimplifiedDocument([FromBody]GetReport input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                UserProfile user = GetProfileUser();
                ResponseMessage responseval = new ResponseMessage();
                if (input.DocumentType == "CA")
                    responseval = GetApiSite2<ResponseMessage>(string.Format("GetCashSaleMini/{0}/{1}", user.BusinessID.ToString(), input.DocumentKey.ToString()));

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = input.DocumentNo + ".pdf";
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsepath = pathlink
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public ActionResult DownloadFromPath(string file)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string pathuploads = _config["UploadFilesPath:Report"];
            string fullpath = Path.Combine(webRootPath, pathuploads);
            fullpath = Path.Combine(fullpath, file);

            var stream = new FileStream(fullpath, FileMode.Open);
            return File(stream, "application/pdf", file);
        }

        [HttpGet]
        public IActionResult GetDocumentAttach(int documentkey, string documenttype , string key)
        {
            try
            {
                List<Document_Attach> responseval = new List<Document_Attach>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_Attach>>(string.Format("documentsell/getdocumentattach/{0}/{1}/{2}", user.BusinessID , documentkey, documenttype ));

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostDocumentAttach([FromBody]Document_Attach input)
        {
            try
            {
                Document_Attach responseval = new Document_Attach();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate =DateTime.Now;
                input.CreateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_Attach>(string.Format("documentsell/postdocumentattach/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteDocumentAttach([FromBody]Document_Attach input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deletedocumentattach/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult UploadDocumentAttach(int attachkey , List<IFormFile> files)
        {
            try
            {
                long size = files.Sum(f => f.Length);

                // full path to file in temp location

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Attach"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Empty;

                foreach (var formFile in files)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    fullpath = Path.Combine(fullpath, filename);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(fullpath, FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }

                UserProfile user = GetProfileUser();
                Document_Attach attach = new Document_Attach();
                attach.BusinessID = user.BusinessID.Value;
                attach.AttachKey = attachkey;
                attach.AttachPath = filename;

                var jsonObject = JsonConvert.SerializeObject(attach);
                return PostApi(string.Format("documentsell/postdocumentattach/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        #endregion

        #region Quotation
        [HttpGet]
        public IActionResult GetBusinessQuotation(string key)
        {
            try
            {
                List<Document_Quotation> responseval = new List<Document_Quotation>();
                UserProfile user = GetProfileUser();

                string sid = string.Empty;
                if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Employee))
                    sid = user.UID.ToString();

                responseval = GetApi<List<Document_Quotation>>(string.Format("documentsell/getquotation/{0}?sid={1}", user.BusinessID, sid));

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessQuotationbykey(int QuotationKey, string key)
        {
            try
            {
                Document_Quotation responseval = new Document_Quotation();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_Quotation>(string.Format("documentsell/getquotationbykey/{0}/{1}", user.BusinessID, QuotationKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessQuotation([FromBody]Document_Quotation input)
        {
            try
            {
                Document_Quotation responseval = new Document_Quotation();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_Quotation>(string.Format("documentsell/postquotation/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessQuotationNextStep([FromBody]Document_Quotation input)
        {
            try
            {
                Document_Quotation responseval = new Document_Quotation();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/postquotationnextflow/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessQuotation([FromBody]Document_Quotation input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deletequotation/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetParameterQuotation(string key)
        {
            try
            {
                // RParameter responseval = new RParameter();
                UserProfile user = GetProfileUser();
                // responseval = GetApi<RParameter>(string.Format("parameter/get/quotation/{0}", user.BusinessID));

                var responseval = GetParam<RParameter>(string.Format("parameter/get/quotation/{0}", user.BusinessID), "param:quotation");


                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        #endregion

        #region Billing
        [HttpGet]
        public IActionResult GetBusinessBilling(string key)
        {
            try
            {
                List<Document_Billing> responseval = new List<Document_Billing>();
                UserProfile user = GetProfileUser();

                string sid = string.Empty;
                if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Employee))
                    sid = user.UID.ToString();

                responseval = GetApi<List<Document_Billing>>(string.Format("documentsell/getbilling/{0}?sid={1}", user.BusinessID, sid));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessBillingkey(int BillingKey, string key)
        {
            try
            {
                Document_Billing responseval = new Document_Billing();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_Billing>(string.Format("documentsell/getbillingbykey/{0}/{1}", user.BusinessID, BillingKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessBilling([FromBody]Document_Billing input)
        {
            try
            {
                Document_Billing responseval = new Document_Billing();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_Billing>(string.Format("documentsell/postbilling/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessBillingCumulative([FromBody]Document_Billing input)
        {
            try
            {
                Document_Billing responseval = new Document_Billing();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_Billing>(string.Format("documentsell/postbillingcumulative/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessBillingNextStep([FromBody]Document_Billing input)
        {
            try
            {
                Document_Billing responseval = new Document_Billing();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/postbillingnextflow/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessBilling([FromBody]Document_Billing input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deletebilling/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region Invoice
        [HttpGet]
        public IActionResult GetBusinessInvoice(string key)
        {
            try
            {
                List<Document_Invoice> responseval = new List<Document_Invoice>();
                UserProfile user = GetProfileUser();

                string sid = string.Empty;
                if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Employee))
                    sid = user.UID.ToString();

                responseval = GetApi<List<Document_Invoice>>(string.Format("documentsell/getinvoice/{0}?sid={1}", user.BusinessID, sid));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessInvoiceByContactNoCredit(string key, int contactkey)
        {
            try
            {
                List<Document_Invoice> responseval = new List<Document_Invoice>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_Invoice>>(string.Format("documentsell/getinvoice/{0}", user.BusinessID));
                if (responseval != null && responseval.Count > 0)
                    responseval = responseval.Where(a => a.CustomerKey == contactkey && !a.CreditNoteKey.HasValue && (a.InvoiceStatus == "1" || a.InvoiceStatus == "2")).ToList();

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessInvoiceByContactNoDebit(string key, int contactkey)
        {
            try
            {
                List<Document_Invoice> responseval = new List<Document_Invoice>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_Invoice>>(string.Format("documentsell/getinvoice/{0}", user.BusinessID));
                if (responseval != null && responseval.Count > 0)
                    responseval = responseval.Where(a => a.CustomerKey == contactkey && !a.DebitNoteKey.HasValue && (a.InvoiceStatus == "1" || a.InvoiceStatus == "2")).ToList();

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessInvoiceByContactBillingCumative(int contactkey, int? billingkey, string key)
        {
            try
            {
                List<Document_Invoice> responseval = new List<Document_Invoice>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_Invoice>>(string.Format("documentsell/getinvoice/{0}", user.BusinessID));
                if (responseval != null && responseval.Count > 0)
                    responseval = responseval.Where(a => (a.CustomerKey == contactkey && (a.InvoiceStatus == "1" || a.InvoiceStatus == "2")) && (!a.BillingNoteKey.HasValue || a.BillingNoteKey.Value == billingkey)).ToList();

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessInvoiceByContactReceiptCumative(int contactkey, int? receiptkey, string key)
        {
            try
            {
                List<Document_Invoice> responseval = new List<Document_Invoice>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_Invoice>>(string.Format("documentsell/getinvoice/{0}", user.BusinessID));
                if (responseval != null && responseval.Count > 0)
                    responseval = responseval.Where(a => (a.CustomerKey == contactkey && (a.InvoiceStatus == "1" || a.InvoiceStatus == "2" || a.InvoiceStatus == "99")) && !a.BillingNoteKey.HasValue && (!a.ReceiptKey.HasValue || a.ReceiptKey.Value == receiptkey)).ToList();

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessInvoicebykey(int InvoiceKey, string key)
        {
            try
            {
                Document_Invoice responseval = new Document_Invoice();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_Invoice>(string.Format("documentsell/getinvoicebykey/{0}/{1}", user.BusinessID, InvoiceKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessInvoice([FromBody]Document_Invoice input)
        {
            try
            {
                Document_Invoice responseval = new Document_Invoice();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_Invoice>(string.Format("documentsell/postinvoice/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessInvoiceNextStep([FromBody]Document_Invoice input)
        {
            try
            {
                Document_Invoice responseval = new Document_Invoice();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/postinvoicenextflow/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessInvoice([FromBody]Document_Invoice input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deleteinvoice/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region Receipt
        [HttpGet]
        public IActionResult GetBusinessReceipt(string key)
        {
            try
            {
                List<Document_Receipt> responseval = new List<Document_Receipt>();
                UserProfile user = GetProfileUser();

                string sid = string.Empty;
                if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Employee))
                    sid = user.UID.ToString();

                responseval = GetApi<List<Document_Receipt>>(string.Format("documentsell/getreceipt/{0}?sid={1}", user.BusinessID, sid));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessReceiptbykey(int ReceiptKey, string key)
        {
            try
            {
                Document_Receipt responseval = new Document_Receipt();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_Receipt>(string.Format("documentsell/getreceiptbykey/{0}/{1}", user.BusinessID, ReceiptKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessReceipt([FromBody]Document_Receipt input)
        {
            try
            {
                Document_Receipt responseval = new Document_Receipt();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_Receipt>(string.Format("documentsell/postreceipt/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessReceiptCumulative([FromBody]Document_Receipt input)
        {
            try
            {
                Document_Receipt responseval = new Document_Receipt();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_Receipt>(string.Format("documentsell/postreceiptcumulative/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessReceiptNextStep([FromBody]Document_Receipt input)
        {
            try
            {
                Document_Receipt responseval = new Document_Receipt();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/postreceiptnextflow/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessReceipt([FromBody]Document_Receipt input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deletereceipt/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region Receive
        [HttpPost]
        public IActionResult PostBusinessReceiptPayment([FromBody]Document_ReceiveInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentsell/postreceiptpayment/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessCashSaleReceiptPayment([FromBody]Document_ReceiveInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentsell/postcashsalepayment/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region CreditNote
        [HttpGet]
        public IActionResult GetBusinessCreditNote(string key)
        {
            try
            {
                List<Document_CreditNote> responseval = new List<Document_CreditNote>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_CreditNote>>(string.Format("documentsell/getcreditnote/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessCreditNotebykey(int CreditNoteKey, string key)
        {
            try
            {
                Document_CreditNote responseval = new Document_CreditNote();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_CreditNote>(string.Format("documentsell/getCreditnotebykey/{0}/{1}", user.BusinessID, CreditNoteKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessCreditNote([FromBody]Document_CreditNote input)
        {
            try
            {
                Document_CreditNote responseval = new Document_CreditNote();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_CreditNote>(string.Format("documentsell/postcreditnote/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessCreditNoteNextStep([FromBody]Document_CreditNote input)
        {
            try
            {
                Document_CreditNote responseval = new Document_CreditNote();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/postcreditnotenextflow/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessCreditNote([FromBody]Document_CreditNote input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deletecreditnote/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region DebitNote
        [HttpGet]
        public IActionResult GetBusinessDebitNote(string key)
        {
            try
            {
                List<Document_DebitNote> responseval = new List<Document_DebitNote>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_DebitNote>>(string.Format("documentsell/getdebitnote/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessDebitNotebykey(int DebitNoteKey, string key)
        {
            try
            {
                Document_DebitNote responseval = new Document_DebitNote();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_DebitNote>(string.Format("documentsell/getdebitnotebykey/{0}/{1}", user.BusinessID, DebitNoteKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessDebitNote([FromBody]Document_DebitNote input)
        {
            try
            {
                Document_DebitNote responseval = new Document_DebitNote();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_DebitNote>(string.Format("documentsell/postdebitnote/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessDebitNoteNextStep([FromBody]Document_DebitNote input)
        {
            try
            {
                Document_DebitNote responseval = new Document_DebitNote();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/postdebitnotenextflow/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessDebitNote([FromBody]Document_DebitNote input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deletedebitnote/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region Cumulative
        [HttpPost]
        public IActionResult GetBusinessCumulative([FromBody]List<GetDocumentCumulative> input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                DocumentCumulative responseval = new DocumentCumulative();
                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<DocumentCumulative>(string.Format("documentsell/getdocumentcumulative/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region CashSale
        [HttpGet]
        public IActionResult GetBusinessCashSale(string key)
        {
            try
            {
                List<Document_CashSale> responseval = new List<Document_CashSale>();
                UserProfile user = GetProfileUser();

                string sid = string.Empty;
                if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Employee))
                    sid = user.UID.ToString();

                responseval = GetApi<List<Document_CashSale>>(string.Format("documentsell/getcashsale/{0}?sid={1}", user.BusinessID, sid));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessCashSalebykey(int CashSaleKey, string key)
        {
            try
            {
                Document_CashSale responseval = new Document_CashSale();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_CashSale>(string.Format("documentsell/getcashsalebykey/{0}/{1}", user.BusinessID, CashSaleKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessCashSale([FromBody]Document_CashSale input)
        {
            try
            {
                Document_CashSale responseval = new Document_CashSale();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_CashSale>(string.Format("documentsell/postcashsale/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessCashSaleNextStep([FromBody]Document_CashSale input)
        {
            try
            {
                Document_CashSale responseval = new Document_CashSale();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/postcashsalenextflow/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteBusinessCashSale([FromBody]Document_CashSale input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentsell/deletecashsale/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion
        private HttpClient GetHttpClient(string baseAddress)
        {
            return new HttpClient { BaseAddress = new Uri(baseAddress) };
        }
    }
}
