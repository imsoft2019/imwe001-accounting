﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using Microsoft.AspNetCore.Hosting;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.User;
using Newtonsoft.Json;
using IMWE001_AccountingModels.DocumentSell;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace IMWE001_Accounting.Controllers
{
    public class DocumentBuyController :  BaseController
    {
        public DocumentBuyController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }

        #region Common
        public IActionResult PurchaseOrder()
        {
            return View();
        }

        public IActionResult PurchaseOrderAdd()
        {
            return View();
        }

        public IActionResult ReceivingInventory()
        {
            return View();
        }

        public IActionResult ReceivingInventoryAdd()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #endregion



        #region PO (ใบสั่งซื้อ)
        [HttpPost]
        public IActionResult PostPurchaseOrder([FromBody]Document_PurchaseOrder input)
        {
            try
            {
                Document_PurchaseOrder responseval = new Document_PurchaseOrder();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval =  PostApi<Document_PurchaseOrder>(string.Format("documentbuy/postpo/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int) Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message
    });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessPO(string key)
        {
            try
            {
                List<Document_PurchaseOrder> responseval = new List<Document_PurchaseOrder>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Document_PurchaseOrder>>(string.Format("documentbuy/getpo/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessPOkey(int POKey, string key)
        {
            try
            {
                Document_PurchaseOrder responseval = new Document_PurchaseOrder();
                UserProfile user = GetProfileUser();
                responseval = GetApi<Document_PurchaseOrder>(string.Format("documentbuy/getpobykey/{0}/{1}", user.BusinessID, POKey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        public IActionResult UpdateStatusPO([FromBody]Document_PurchaseOrder input)
        {
            try
            {
                Document_PurchaseOrder responseval = new Document_PurchaseOrder();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

               // input.PurchaseOrderKey

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentbuy/poststatuspo/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeletePO([FromBody]Document_PurchaseOrder input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("documentbuy/postdeletepo/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion


        #region RI (ใบรับสินค้า)

        [HttpPost]
        public IActionResult PostReceiveInventory([FromBody]Document_Receive input)
        {
            try
            {
                var tmp = input.toReceiveInventory();

                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(tmp);
                var tmpResult = PostApi<Document_ReceiveInventory>(string.Format("documentbuy/postri/{0}", user.BusinessID), jsonObject);

                Document_Receive result = new Document_Receive();
                result.SetME(tmpResult);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = result
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(),
                    errormessage = ex.Message
                });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessRI(string key)
        {
            try
            {
                List<Document_Receive> responseval = new List<Document_Receive>();
                UserProfile user = GetProfileUser();
                var tmp = GetApi<List<Document_ReceiveInventory>>(string.Format("documentbuy/getri/{0}", user.BusinessID));
                if (tmp != null)
                {
                    foreach (var item in tmp)
                    {
                        var re = new Document_Receive(); re.SetME(item);
                        responseval.Add(re);
                    }
                }
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessRIkey(int POKey, string key)
        {
            try
            {
                Document_Receive responseval = new Document_Receive();
                UserProfile user = GetProfileUser();
                var tmp = GetApi<Document_ReceiveInventory>(string.Format("documentbuy/getribykey/{0}/{1}", user.BusinessID, POKey));
                responseval.SetME(tmp);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        public IActionResult UpdateStatusRI([FromBody]Document_Receive input)
        {
            try
            {
                var tmp = input.toReceiveInventory();

                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                // input.PurchaseOrderKey

                var jsonObject = JsonConvert.SerializeObject(tmp);
                return PostApi(string.Format("documentbuy/poststatusri/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteRI([FromBody]Document_Receive input)
        {
            try
            {
                var tmp = input.toReceiveInventory();
                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(tmp);
                return PostApi(string.Format("documentbuy/postdeleteri/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessReceiptPayment([FromBody]Document_ReceiveInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();
                input.DocumentType = "RI";
                

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentbuy/postreceiptpayment/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReceiptTax([FromBody]Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate  = DateTime.Now;
                input.CreateBy = user.UID.ToString();
                input.DocumentType = "RI";


                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentbuy/postreceipttax/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostdeleteReceiptTax([FromBody]Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = DateTime.Now;
                input.CreateBy = user.UID.ToString();
                input.DocumentType = "RI";


                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentbuy/postdeletereceipttax/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult UploadTax(List<IFormFile> files)
        {
            try
            {
                long size = files.Sum(f => f.Length);

                // full path to file in temp location

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:tax"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Empty;

                foreach (var formFile in files)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(Path.Combine(fullpath, filename), FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(), fullname = fullpath, filename = filename });


            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        #endregion
    }

    public class Document_Receive : Document_PurchaseOrder
    {
        public Document_ReceiveInfo ReceiveInfo { get; set; }
        public Document_ReceiveTaxInvoiceInfo ReceiveTaxInfo { get; set; }

        public void SetME(Document_ReceiveInventory input)
        {
            BusinessID = input.BusinessID;
            CreateBy = input.CreateBy;
            CreateDate = input.CreateDate;
            CreditDay = input.CreditDay;
            CreditType = input.CreditType;
            CreditTypeName = input.CreditTypeName;
            CustomerAddress = input.CustomerAddress;
            CustomerBranch = input.CustomerBranch;
            CustomerContactEmail = input.CustomerContactEmail;
            CustomerContactName = input.CustomerContactName;
            CustomerContactPhone = input.CustomerContactPhone;
            CustomerKey = input.CustomerKey;
            CustomerName = input.CustomerName;
            CustomerTaxID = input.CustomerTaxID;
            DiscountAmount = input.DiscountAmount;
            DiscountPercent = input.DiscountPercent;
            DocumentKey = input.DocumentKey;
            DocumentNo = input.DocumentNo;
            DocumentOwner = input.DocumentOwner;
            DocumentRefNo = input.DocumentRefNo;
            DueDate = input.DueDate;
            ExemptAmount = input.ExemptAmount;
            GrandAmount = input.GrandAmount;
            GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
            isdelete = input.isdelete;
            IsDiscountHeader = input.IsDiscountHeader;
            IsDiscountHeaderType = input.IsDiscountHeaderType;
            IsTaxHeader = input.IsTaxHeader;
            IsTaxSummary = input.IsWithholdingTax;
            IsWithholdingTax = input.IsWithholdingTax;
            Noted = input.Noted;
            ProjectName = input.ProjectName;
            PurchaseOrderDate = input.ReceiveInventoryDate;
            PurchaseOrderKey = input.ReceiveInventoryKey;
            PurchaseOrderNo = input.ReceiveInventoryNo;
            PurchaseOrderStatus = input.ReceiveInventoryStatus;
            PurchaseOrderStatusName = input.ReceiveInventoryStatusName;
            ReferenceNo = input.ReferenceNo;
            Remark = input.Remark;
            SaleID = input.SaleID;
            SaleName = input.SaleName;
            Signature = input.Signature;
            TaxType = input.TaxType;
            TaxTypeName = input.TaxTypeName;
            TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
            TotalAmount = input.TotalAmount;
            TotalBeforeVatAmount = input.TotalBeforeVatAmount;
            UpdateBy = input.UpdateBy;
            UpdateDate = input.UpdateDate;
            VAT = input.VAT;
            VatableAmount = input.VatableAmount;
            WithholdingAmount = input.WithholdingAmount;
            WithholdingKey = input.WithholdingKey;
            WithholdingRate = input.WithholdingRate;
            ReceiveInfo = input.ReceiveInfo;
            ReceiveTaxInfo = input.ReceiveTaxInfo;
            ReferenceNo = input.ReferenceNo;

            if (input.ReceiveInventoryDetails != null)
            {
                PurchaseOrderDetails = new List<Document_PurchaseOrderDetail>();

                foreach (var item in input.ReceiveInventoryDetails)
                {
                    var tmp = new Document_PurchaseOrderDetail()
                    {
                        BusinessID = item.BusinessID,
                        Description = item.Description,
                        Discount = item.Discount,
                        DiscountAfter = item.DiscountAfter,
                        DiscountPercent = item.DiscountPercent,
                        DiscountType = item.DiscountType,
                        Name = item.Name,
                        Price = item.Price,
                        PriceAfterTax = item.PriceAfterTax,
                        PriceBeforeTax = item.PriceBeforeTax,
                        PriceTax = item.PriceTax,
                        ProductKey = item.ProductKey,
                        Quantity = item.Quantity,
                        PurchaseOrderDetailKey = item.ReceiveInventoryDetailKey,
                        PurchaseOrderKey = item.ReceiveInventoryKey,
                        Sequence = item.Sequence,
                        Total = item.Total,
                        Unit = item.Unit,
                        Vat = item.Vat,
                        VatAfter = item.VatAfter,
                        VatRate = item.VatRate,
                        VatType = item.VatType
                    };
                    PurchaseOrderDetails.Add(tmp);
                }
            }
        }

        public Document_ReceiveInventory toReceiveInventory()
        {
            Document_ReceiveInventory result = new Document_ReceiveInventory()
            {
                BusinessID = this.BusinessID,
                CreateBy = this.CreateBy,
                CreateDate = this.CreateDate,
                CreditDay = this.CreditDay,
                CreditType = this.CreditType,
                CreditTypeName = this.CreditTypeName,
                CustomerAddress = this.CustomerAddress,
                CustomerBranch = this.CustomerBranch,
                CustomerContactEmail = this.CustomerContactEmail,
                CustomerContactName = this.CustomerContactName,
                CustomerContactPhone = this.CustomerContactPhone,
                CustomerKey = this.CustomerKey,
                CustomerName = this.CustomerName,
                CustomerTaxID = this.CustomerTaxID,
                DiscountAmount = this.DiscountAmount,
                DiscountPercent = this.DiscountPercent,
                DocumentKey = this.DocumentKey,
                DocumentNo = this.DocumentNo,
                DocumentOwner = this.DocumentOwner,
                DocumentRefNo = this.DocumentRefNo,
                DueDate = this.DueDate,
                ExemptAmount = this.ExemptAmount,
                GrandAmount = this.GrandAmount,
                GrandAmountAfterWithholding = this.GrandAmountAfterWithholding,
                isdelete = this.isdelete,
                IsDiscountHeader = this.IsDiscountHeader,
                IsDiscountHeaderType = this.IsDiscountHeaderType,
                IsTaxHeader = this.IsTaxHeader,
                IsTaxSummary = this.IsWithholdingTax,
                IsWithholdingTax = this.IsWithholdingTax,
                Noted = this.Noted,
                ProjectName = this.ProjectName,
                ReceiveInventoryDate = this.PurchaseOrderDate,
                ReceiveInventoryKey = this.PurchaseOrderKey,
                ReceiveInventoryNo = this.PurchaseOrderNo,
                ReceiveInventoryStatus = this.PurchaseOrderStatus,
                ReceiveInventoryStatusName = this.PurchaseOrderStatusName,
                ReferenceNo = this.ReferenceNo,
                Remark = this.Remark,
                SaleID = this.SaleID,
                SaleName = this.SaleName,
                Signature = this.Signature,
                TaxType = this.TaxType,
                TaxTypeName = this.TaxTypeName,
                TotalAfterDiscountAmount = this.TotalAfterDiscountAmount,
                TotalAmount = this.TotalAmount,
                TotalBeforeVatAmount = this.TotalBeforeVatAmount,
                UpdateBy = this.UpdateBy,
                UpdateDate = this.UpdateDate,
                VAT = this.VAT,
                VatableAmount = this.VatableAmount,
                WithholdingAmount = this.WithholdingAmount,
                WithholdingKey = this.WithholdingKey,
                WithholdingRate = this.WithholdingRate,
            };

            if (this.PurchaseOrderDetails != null && this.PurchaseOrderDetails.Count > 0)
            {
                result.ReceiveInventoryDetails = new List<Document_ReceiveInventoryDetail>();

                foreach (var item in this.PurchaseOrderDetails)
                {
                    var tmp = new Document_ReceiveInventoryDetail()
                    {
                        BusinessID = item.BusinessID,
                        Description = item.Description,
                        Discount = item.Discount,
                        DiscountAfter = item.DiscountAfter,
                        DiscountPercent = item.DiscountPercent,
                        DiscountType = item.DiscountType,
                        Name = item.Name,
                        Price = item.Price,
                        PriceAfterTax = item.PriceAfterTax,
                        PriceBeforeTax = item.PriceBeforeTax,
                        PriceTax = item.PriceTax,
                        ProductKey = item.ProductKey,
                        Quantity = item.Quantity,
                        ReceiveInventoryDetailKey = item.PurchaseOrderDetailKey,
                        ReceiveInventoryKey = item.PurchaseOrderKey,
                        Sequence = item.Sequence,
                        Total = item.Total,
                        Unit = item.Unit,
                        Vat = item.Vat,
                        VatAfter = item.VatAfter,
                        VatRate = item.VatRate,
                        VatType = item.VatType
                    };
                    result.ReceiveInventoryDetails.Add(tmp);
                }
            }

            return result;
        }
    }
}
