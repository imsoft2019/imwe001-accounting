﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using IMWE001_Accounting.Common;
using IMWE001_AccountingModels.Parameter;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.User;
using Microsoft.AspNetCore.Http;
using System.IO;
using IMWE001_Accounting.Common.RDService;

namespace IMWE001_Accounting.Controllers
{
    [Produces("application/json")]
    public class ContactBookController : BaseController
    {
        public ContactBookController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }

        public IActionResult Contacts()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Contact Book
        [HttpGet]
        public IActionResult GetBusinessContact(string key)
        {
            try
            {
                List<Contact> responseval = new List<Contact>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Contact>>(string.Format("contactbook/getcontact/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessContactCustom(string key)
        {
            try
            {
                List<Contact> responseval = new List<Contact>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Contact>>(string.Format("contactbook/getcontact/custom/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessContactSupplier(string key)
        {
            try
            {
                List<Contact> responseval = new List<Contact>();
                UserProfile user = GetProfileUser();
                responseval = GetApi<List<Contact>>(string.Format("contactbook/getcontact/supplier/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult GetRevenurevalue([FromBody]RequestRevenure request)
        {
            try
            {
                List<RDVatResult> responseval = new List<RDVatResult>();
                UserProfile user = GetProfileUser();

                var isNumeric = decimal.TryParse(request.NID, out decimal n);
                if (request.NID.Length == 13 && isNumeric)
                {
                    responseval = RDVatResult.Get(request.NID, "");
                }
                else
                    responseval = RDVatResult.Get("", request.Name);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessContact([FromBody]Contact input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApiReturnKey(string.Format("contactbook/postcontact/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessContactReply([FromBody]Contact input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                Contact responseval = new Contact();
                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Contact>(string.Format("contactbook/postcontactreply/{0}", user.BusinessID), jsonObject);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        

        [HttpPost]
        public IActionResult DeleteBusinessContact([FromBody]Contact input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("contactbook/deletecontact/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        #endregion

        private HttpClient GetHttpClient(string baseAddress)
        {
            return new HttpClient { BaseAddress = new Uri(baseAddress) };
        }
    }
}
