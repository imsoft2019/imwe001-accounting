﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using Microsoft.AspNetCore.Hosting;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.User;
using Newtonsoft.Json;
using IMWE001_AccountingModels.DocumentSell;
using Microsoft.AspNetCore.Http;
using System.IO;
using IMWE001_AccountingModels.DocumentExpenses;
using IMWE001_AccountingModels.Parameter;

namespace IMWE001_Accounting.Controllers
{
    public class DocumentExpensesController : BaseController
    {
        #region Common
        public DocumentExpensesController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }
        public IActionResult ExpensesList()
        {
            return View();
        }

        public IActionResult ExpensesAdd()
        {
            return View();
        }

        public IActionResult WithholdingTax()
        {
            return View();
        }

        public IActionResult WithholdingTaxAdd()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #endregion

        #region DocumentExpense

        [HttpGet]
        public IActionResult GetExpenseCatagory()
        {
            List<Param_ExpenseCatagory> responseval = new List<Param_ExpenseCatagory>();
            responseval.Add(new Param_ExpenseCatagory() { ID = "1", Name = "การตลาดและโฆษณา" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "2", Name = "ส่งเสริมการขาย" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "3", Name = "รับรองเลี้ยงลูกค้า" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "4", Name = "ค่าเดินทางและที่พัก" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "5", Name = "ค่าน้ำมัน/แก๊ส/รถยนต์" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "6", Name = "ค่าขนส่งสินค้า/ลอจิสติกส์" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "7", Name = "สินค้า/วัตถุดิบ/แพคเกจจิ้ง" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "8", Name = "ออฟฟิศ" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "9", Name = "ค่าจ้าง/บริการ" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "10", Name = "เงินเดือน/สวัสดิการ" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "11", Name = "ค่าเช่า" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "12", Name = "เงินมัดจำ/ประกัน" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "13", Name = "เบี้ยประกัน" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "14", Name = "ค่าธรรมเนียม" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "14", Name = "ภาษีและทะเบียน" });
            responseval.Add(new Param_ExpenseCatagory() { ID = "14", Name = "เบ็ดเตล็ด" });



            return WriteJson(new
            {
                responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                responsedata = responseval
            });
        }

        [HttpPost]
        public IActionResult PostExpense([FromBody]Document_ReceiveEX input)
        {
            try
            {
                var tmp = input.toExpenses();

                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(tmp);
                var tmpResult = PostApi<Document_Expenses>(string.Format("documentexpenses/postex/{0}", user.BusinessID), jsonObject);

                var result = new Document_ReceiveEX();
                result.SetME(tmpResult);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = result
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(),
                    errormessage = ex.Message
                });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessEX(string key)
        {
            try
            {
                List<Document_ReceiveEX> responseval = new List<Document_ReceiveEX>();
                UserProfile user = GetProfileUser();
                string sid = string.Empty;
                if (Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Employee) || Convert.ToInt32(user.Position) == ((int)Common.Abstract.Permision.Manager))
                    sid = user.UID.ToString();

                var tmp = GetApi<List<Document_Expenses>>(string.Format("documentexpenses/getex/{0}?sid={1}", user.BusinessID, sid));
                if (tmp != null)
                {
                    foreach (var item in tmp)
                    {
                        var re = new Document_ReceiveEX(); re.SetME(item);
                        responseval.Add(re);
                    }
                }
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessEXkey(int POKey, string key)
        {
            try
            {
                Document_ReceiveEX responseval = new Document_ReceiveEX();
                UserProfile user = GetProfileUser();
                var tmp = GetApi<Document_Expenses>(string.Format("documentexpenses/getexbykey/{0}/{1}", user.BusinessID, POKey));
                responseval.SetME(tmp);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        public IActionResult UpdateStatusEX([FromBody]Document_ReceiveEX input)
        {
            try
            {
                var tmp = input.toExpenses();

                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                // input.PurchaseOrderKey

                var jsonObject = JsonConvert.SerializeObject(tmp);
                return PostApi(string.Format("documentexpenses/poststatusex/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteEX([FromBody]Document_ReceiveEX input)
        {
            try
            {
                var tmp = input.toExpenses();
                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(tmp);
                return PostApi(string.Format("documentexpenses/postdeleteex/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessReceiptPayment([FromBody]Document_ReceiveInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();
                input.DocumentType = "PV";


                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentexpenses/postreceiptpayment/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReceiptTax([FromBody]Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = DateTime.Now;
                input.CreateBy = user.UID.ToString();
                input.DocumentType = "EX";


                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentexpenses/postreceipttax/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostdeleteReceiptTax([FromBody]Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                Document_ReceiveInfo responseval = new Document_ReceiveInfo();
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = DateTime.Now;
                input.CreateBy = user.UID.ToString();
                input.DocumentType = "EX";


                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<Document_ReceiveInfo>(string.Format("documentexpenses/postdeletereceipttax/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult UploadTax(List<IFormFile> files)
        {
            try
            {
                long size = files.Sum(f => f.Length);

                // full path to file in temp location

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:tax"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Empty;

                foreach (var formFile in files)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(Path.Combine(fullpath, filename), FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(), fullname = fullpath, filename = filename });


            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }



        #endregion


        #region WT (หักภาษี ณ.ที่จ่าย)

        [HttpPost]
        public IActionResult PostWithholdingTax([FromBody]Document_ReceiveWT input)
        {
            try
            {
                var tmp = input.toWithholdingTax();

                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                Validate(tmp);

                var jsonObject = JsonConvert.SerializeObject(tmp);
                var tmpResult = PostApi<Document_WithholdingTax>(string.Format("documentexpenses/postwt/{0}", user.BusinessID), jsonObject);

                Document_ReceiveWT result = new Document_ReceiveWT();
                result.SetME(tmpResult);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = result
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(),
                    errormessage = ex.Message
                });
            }
        }

        private void Validate(Document_WithholdingTax doc)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (string.IsNullOrEmpty(doc.WithholdingTaxForm))
                sb.AppendLine("- กรุณาเลือกประเภทแบบฟอร์ม");

            List<string> tmp = new List<string>();
            foreach (var item in doc.WithholdingTaxDetails)
            {
                if (!tmp.Contains(item.WithholdingTaxCatagory))
                    tmp.Add(item.WithholdingTaxCatagory);
                else
                {
                    sb.Append("- ประเภทหักภาษี ณ.ที่จ่ายไม่สามารถซ้ำกันได้"); break;
                }
            }

            var msg = sb.ToString();

            if (msg != "")
                throw new Exception(msg);

        }

        [HttpGet]
        public IActionResult GetBusinessWT(string key)
        {
            try
            {
                var responseval = new List<Document_ReceiveWT>();
                UserProfile user = GetProfileUser();
                var tmp = GetApi<List<Document_WithholdingTax>>(string.Format("documentexpenses/getwt/{0}", user.BusinessID));
                if (tmp != null)
                {
                    foreach (var item in tmp)
                    {
                        var re = new Document_ReceiveWT(); re.SetME(item);
                        responseval.Add(re);
                    }
                }
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetBusinessWTkey(int POKey, string key)
        {
            try
            {
                var responseval = new Document_ReceiveWT();
                UserProfile user = GetProfileUser();
                var tmp = GetApi<Document_WithholdingTax>(string.Format("documentexpenses/getwtbykey/{0}/{1}", user.BusinessID, POKey));
                responseval.SetME(tmp);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        public IActionResult UpdateStatusWT([FromBody]Document_ReceiveWT input)
        {
            try
            {
                var tmp = input.toWithholdingTax();

                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                // input.PurchaseOrderKey

                var jsonObject = JsonConvert.SerializeObject(tmp);
                return PostApi(string.Format("documentexpenses/poststatuswt/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult DeleteWT([FromBody]Document_ReceiveWT input)
        {
            try
            {
                var tmp = input.toWithholdingTax();
                UserProfile user = GetProfileUser();
                tmp.BusinessID = user.BusinessID.Value;
                tmp.UpdateDate = DateTime.Now;
                tmp.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(tmp);
                return PostApi(string.Format("documentexpenses/postdeletewt/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }


        #endregion
    }


    public class Document_ReceiveEX : Document_PurchaseOrder
    {
        public Document_ReceiveInfo ReceiveInfo { get; set; }
        public Document_ReceiveTaxInvoiceInfo ReceiveTaxInfo { get; set; }

        public void SetME(Document_Expenses input)
        {
            BusinessID = input.BusinessID;
            CreateBy = input.CreateBy;
            CreateDate = input.CreateDate;
            CreditDay = input.CreditDay;
            CreditType = input.CreditType;
            CreditTypeName = input.CreditTypeName;
            CustomerAddress = input.CustomerAddress;
            CustomerBranch = input.CustomerBranch;
            CustomerContactEmail = input.CustomerContactEmail;
            CustomerContactName = input.CustomerContactName;
            CustomerContactPhone = input.CustomerContactPhone;
            CustomerKey = input.CustomerKey;
            CustomerName = input.CustomerName;
            CustomerTaxID = input.CustomerTaxID;
            DiscountAmount = input.DiscountAmount;
            DiscountPercent = input.DiscountPercent;
            DocumentKey = input.DocumentKey;
            DocumentNo = input.DocumentNo;
            DocumentOwner = input.DocumentOwner;
            DocumentRefNo = input.DocumentRefNo;
            DueDate = input.DueDate;
            ExemptAmount = input.ExemptAmount;
            GrandAmount = input.GrandAmount;
            GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
            isdelete = input.isdelete;
            IsDiscountHeader = input.IsDiscountHeader;
            IsDiscountHeaderType = input.IsDiscountHeaderType;
            IsTaxHeader = input.IsTaxHeader;
            IsTaxSummary = input.IsWithholdingTax;
            IsWithholdingTax = input.IsWithholdingTax;
            Noted = input.Noted;
            ProjectName = input.ProjectName;
            PurchaseOrderDate = input.ExpensesDate;
            PurchaseOrderKey = input.ExpensesKey;
            PurchaseOrderNo = input.ExpensesNo;
            PurchaseOrderStatus = input.ExpensesStatus;
            PurchaseOrderStatusName = input.ExpensesStatusName;
            ReferenceNo = input.ReferenceNo;
            Remark = input.Remark;
            SaleID = input.SaleID;
            SaleName = input.SaleName;
            Signature = input.Signature;
            TaxType = input.TaxType;
            TaxTypeName = input.TaxTypeName;
            TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
            TotalAmount = input.TotalAmount;
            TotalBeforeVatAmount = input.TotalBeforeVatAmount;
            UpdateBy = input.UpdateBy;
            UpdateDate = input.UpdateDate;
            VAT = input.VAT;
            VatableAmount = input.VatableAmount;
            WithholdingAmount = input.WithholdingAmount;
            WithholdingKey = input.WithholdingKey;
            WithholdingRate = input.WithholdingRate;
            ReceiveInfo = input.ReceiveInfo;
            ReceiveTaxInfo = input.ReceiveTaxInfo;

            if (input.ExpensesDetails != null)
            {
                PurchaseOrderDetails = new List<Document_PurchaseOrderDetail>();

                foreach (var item in input.ExpensesDetails)
                {
                    var tmp = new Document_PurchaseOrderDetail()
                    {
                        BusinessID = item.BusinessID,
                        Description = item.Description,
                        Discount = item.Discount,
                        DiscountAfter = item.DiscountAfter,
                        DiscountPercent = item.DiscountPercent,
                        DiscountType = item.DiscountType,
                        Name = item.Name,
                        Price = item.Price,
                        PriceAfterTax = item.PriceAfterTax,
                        PriceBeforeTax = item.PriceBeforeTax,
                        PriceTax = item.PriceTax,
                        ProductKey = item.ProductKey,
                        Quantity = item.Quantity,
                        PurchaseOrderDetailKey = item.ExpensesDetailKey,
                        PurchaseOrderKey = item.ExpensesKey,
                        Sequence = item.Sequence,
                        Total = item.Total,
                        Unit = item.Unit,
                        Vat = item.Vat,
                        VatAfter = item.VatAfter,
                        VatRate = item.VatRate,
                        VatType = item.VatType,
                        CatagoryID = item.CatagoryID,
                        CatagoryName = item.CatagoryName
                    };
                    PurchaseOrderDetails.Add(tmp);
                }
            }
        }

        public Document_Expenses toExpenses()
        {
            var result = new Document_Expenses()
            {
                BusinessID = this.BusinessID,
                CreateBy = this.CreateBy,
                CreateDate = this.CreateDate,
                CreditDay = this.CreditDay,
                CreditType = this.CreditType,
                CreditTypeName = this.CreditTypeName,
                CustomerAddress = this.CustomerAddress,
                CustomerBranch = this.CustomerBranch,
                CustomerContactEmail = this.CustomerContactEmail,
                CustomerContactName = this.CustomerContactName,
                CustomerContactPhone = this.CustomerContactPhone,
                CustomerKey = this.CustomerKey,
                CustomerName = this.CustomerName,
                CustomerTaxID = this.CustomerTaxID,
                DiscountAmount = this.DiscountAmount,
                DiscountPercent = this.DiscountPercent,
                DocumentKey = this.DocumentKey,
                DocumentNo = this.DocumentNo,
                DocumentOwner = this.DocumentOwner,
                DocumentRefNo = this.DocumentRefNo,
                DueDate = this.DueDate,
                ExemptAmount = this.ExemptAmount,
                GrandAmount = this.GrandAmount,
                GrandAmountAfterWithholding = this.GrandAmountAfterWithholding,
                isdelete = this.isdelete,
                IsDiscountHeader = this.IsDiscountHeader,
                IsDiscountHeaderType = this.IsDiscountHeaderType,
                IsTaxHeader = this.IsTaxHeader,
                IsTaxSummary = this.IsWithholdingTax,
                IsWithholdingTax = this.IsWithholdingTax,
                Noted = this.Noted,
                ProjectName = this.ProjectName,
                ExpensesDate = this.PurchaseOrderDate,
                ExpensesKey = this.PurchaseOrderKey,
                ExpensesNo = this.PurchaseOrderNo,
                ExpensesStatus = this.PurchaseOrderStatus,
                ExpensesStatusName = this.PurchaseOrderStatusName,
                ReferenceNo = this.ReferenceNo,
                Remark = this.Remark,
                SaleID = this.SaleID,
                SaleName = this.SaleName,
                Signature = this.Signature,
                TaxType = this.TaxType,
                TaxTypeName = this.TaxTypeName,
                TotalAfterDiscountAmount = this.TotalAfterDiscountAmount,
                TotalAmount = this.TotalAmount,
                TotalBeforeVatAmount = this.TotalBeforeVatAmount,
                UpdateBy = this.UpdateBy,
                UpdateDate = this.UpdateDate,
                VAT = this.VAT,
                VatableAmount = this.VatableAmount,
                WithholdingAmount = this.WithholdingAmount,
                WithholdingKey = this.WithholdingKey,
                WithholdingRate = this.WithholdingRate
            };

            if (this.PurchaseOrderDetails != null && this.PurchaseOrderDetails.Count > 0)
            {
                result.ExpensesDetails = new List<Document_ExpensesDetail>();

                foreach (var item in this.PurchaseOrderDetails)
                {
                    var tmp = new Document_ExpensesDetail()
                    {
                        BusinessID = item.BusinessID,
                        Description = item.Description,
                        Discount = item.Discount,
                        DiscountAfter = item.DiscountAfter,
                        DiscountPercent = item.DiscountPercent,
                        DiscountType = item.DiscountType,
                        Name = item.Name,
                        Price = item.Price,
                        PriceAfterTax = item.PriceAfterTax,
                        PriceBeforeTax = item.PriceBeforeTax,
                        PriceTax = item.PriceTax,
                        ProductKey = item.ProductKey,
                        Quantity = item.Quantity,
                        ExpensesDetailKey = item.PurchaseOrderDetailKey,
                        ExpensesKey = item.PurchaseOrderKey,
                        Sequence = item.Sequence,
                        Total = item.Total,
                        Unit = item.Unit,
                        Vat = item.Vat,
                        VatAfter = item.VatAfter,
                        VatRate = item.VatRate,
                        VatType = item.VatType,
                        CatagoryID = item.CatagoryID,
                        CatagoryName = item.CatagoryName
                    };
                    result.ExpensesDetails.Add(tmp);
                }
            }

            return result;
        }
    }


    public class Document_ReceiveWT : Document_PurchaseOrder
    {

        public string WithholdingTaxForm { get; set; }
        public string WithholdingTaxFormName { get; set; }
        public string PayerMethod { get; set; }
        public string PayerDesc { get; set; }
        public string SSONumber { get; set; }
        public decimal SSOPrice { get; set; }
        public decimal SSOFundPrice { get; set; }

        public string TaxTypeDesc { get; set; }


        public string WithholdingTaxFormDesc
        {
            get
            {
                string result = "";
                switch (this.WithholdingTaxForm)
                {
                    case "1":
                        result = "ภงด 3";
                        break;
                    case "2":
                        result = "ภงด 53";
                        break;
                    case "3":
                        result = "ภงด 1ก";
                        break;
                    case "4":
                        result = "ภงด 1ก(พิเศษ)";
                        break;
                    case "5":
                        result = "ภงด 2";
                        break;
                    case "6":
                        result = "ภงด 2ก";
                        break;
                    case "7":
                        result = "ภงด 3ก";
                        break;
                }
                return result;
            }
        }

        public void SetME(Document_WithholdingTax input)
        {
            BusinessID = input.BusinessID;
            CreateBy = input.CreateBy;
            CreateDate = input.CreateDate;
            CreditDay = input.CreditDay;
            CreditType = input.CreditType;
            CreditTypeName = input.CreditTypeName;
            CustomerAddress = input.CustomerAddress;
            CustomerBranch = input.CustomerBranch;
            CustomerContactEmail = input.CustomerContactEmail;
            CustomerContactName = input.CustomerContactName;
            CustomerContactPhone = input.CustomerContactPhone;
            CustomerKey = input.CustomerKey;
            CustomerName = input.CustomerName;
            CustomerTaxID = input.CustomerTaxID;
            DiscountAmount = input.DiscountAmount;
            DiscountPercent = input.DiscountPercent;
            DocumentKey = input.DocumentKey;
            DocumentNo = input.DocumentNo;
            DocumentOwner = input.DocumentOwner;
            DocumentRefNo = input.DocumentRefNo;
            DueDate = input.DueDate;
            ExemptAmount = input.ExemptAmount;
            GrandAmount = input.GrandAmount;
            GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
            isdelete = input.isdelete;
            IsDiscountHeader = input.IsDiscountHeader;
            IsDiscountHeaderType = input.IsDiscountHeaderType;
            IsTaxHeader = input.IsTaxHeader;
            IsTaxSummary = input.IsWithholdingTax;
            IsWithholdingTax = input.IsWithholdingTax;
            Noted = input.Noted;
            ProjectName = input.ProjectName;
            PurchaseOrderDate = input.WithholdingTaxDate;
            PurchaseOrderKey = input.WithholdingTaxKey;
            PurchaseOrderNo = input.WithholdingTaxNo;
            PurchaseOrderStatus = input.WithholdingTaxStatus;
            PurchaseOrderStatusName = input.WithholdingTaxStatusName;
            ReferenceNo = input.ReferenceNo;
            Remark = input.Remark;
            SaleID = input.SaleID;
            SaleName = input.SaleName;
            Signature = input.Signature;
            TaxType = input.TaxType;
            TaxTypeName = input.TaxTypeName;
            TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
            TotalAmount = input.TotalAmount;
            TotalBeforeVatAmount = input.TotalBeforeVatAmount;
            UpdateBy = input.UpdateBy;
            UpdateDate = input.UpdateDate;
            VAT = input.VAT;
            VatableAmount = input.VatableAmount;
            WithholdingAmount = input.WithholdingAmount;
            WithholdingKey = input.WithholdingKey;
            WithholdingRate = input.WithholdingRate;

            WithholdingTaxForm = input.WithholdingTaxForm;
            WithholdingTaxFormName = input.WithholdingTaxFormName;
            PayerMethod = input.PayerMethod;
            PayerDesc = input.PayerDesc;
            SSONumber = input.SSONumber;
            SSOPrice = input.SSOPrice;
            SSOFundPrice = input.SSOFundPrice;

            if (input.WithholdingTaxDetails != null)
            {
                PurchaseOrderDetails = new List<Document_PurchaseOrderDetail>();
                List<string> tmpTaxType = new List<string>();
                foreach (var item in input.WithholdingTaxDetails)
                {
                    var tmp = new Document_PurchaseOrderDetail()
                    {
                        BusinessID = item.BusinessID,
                        Description = item.Description,
                        // DiscountType = item.DiscountType,
                        Name = item.Name,
                        Price = item.Price,
                        PriceAfterTax = item.PriceAfterTax,
                        PriceBeforeTax = item.PriceBeforeTax,
                        PriceTax = item.PriceTax,
                        WithholdingTaxCatagory = item.WithholdingTaxCatagory,
                        WithholdingTaxCatagoryName = item.WithholdingTaxCatagoryName,
                        PurchaseOrderDetailKey = item.WithholdingTaxDetailKey,
                        PurchaseOrderKey = item.WithholdingTaxKey,
                        Sequence = item.Sequence,
                        Total = item.Total,
                        Vat = item.Vat,
                        VatAfter = item.VatAfter,
                        VatRate = item.VatRate,
                        VatType = item.VatType,
                        TaxType = item.TaxType,
                        TaxPercent = item.TaxPercent,
                        TaxPercentType = item.TaxPercentType
                    };
                    PurchaseOrderDetails.Add(tmp);

                    if (item.TaxType == "0")
                        tmpTaxType.Add(item.PriceTax.Value.ToString("n2"));
                    else
                        tmpTaxType.Add(item.TaxType + @"%");
                }

                if (tmpTaxType.Count == 0)
                    TaxType = "-";
                else
                {
                    foreach (var item in tmpTaxType)
                        TaxType += "," + item;

                    TaxType = TaxType.Substring(1, TaxType.Length - 1);
                }

                //if(TaxTypeDesc)
            }
        }

        public Document_WithholdingTax toWithholdingTax()
        {
            var result = new Document_WithholdingTax()
            {
                BusinessID = this.BusinessID,
                CreateBy = this.CreateBy,
                CreateDate = this.CreateDate,
                CreditDay = this.CreditDay,
                CreditType = this.CreditType,
                CreditTypeName = this.CreditTypeName,
                CustomerAddress = this.CustomerAddress,
                CustomerBranch = this.CustomerBranch,
                CustomerContactEmail = this.CustomerContactEmail,
                CustomerContactName = this.CustomerContactName,
                CustomerContactPhone = this.CustomerContactPhone,
                CustomerKey = this.CustomerKey,
                CustomerName = this.CustomerName,
                CustomerTaxID = this.CustomerTaxID,
                DiscountAmount = this.DiscountAmount,
                DiscountPercent = this.DiscountPercent,
                DocumentKey = this.DocumentKey,
                DocumentNo = this.DocumentNo,
                DocumentOwner = this.DocumentOwner,
                DocumentRefNo = this.DocumentRefNo,
                DueDate = this.DueDate,
                ExemptAmount = this.ExemptAmount,
                GrandAmount = this.GrandAmount,
                GrandAmountAfterWithholding = this.GrandAmountAfterWithholding,
                isdelete = this.isdelete,
                IsDiscountHeader = this.IsDiscountHeader,
                IsDiscountHeaderType = this.IsDiscountHeaderType,
                IsTaxHeader = this.IsTaxHeader,
                IsTaxSummary = this.IsWithholdingTax,
                IsWithholdingTax = this.IsWithholdingTax,
                Noted = this.Noted,
                ProjectName = this.ProjectName,
                WithholdingTaxDate = this.PurchaseOrderDate,
                WithholdingTaxKey = this.PurchaseOrderKey,
                WithholdingTaxNo = this.PurchaseOrderNo,
                WithholdingTaxStatus = this.PurchaseOrderStatus,
                WithholdingTaxStatusName = this.PurchaseOrderStatusName,
                ReferenceNo = this.ReferenceNo,
                Remark = this.Remark,
                SaleID = this.SaleID,
                SaleName = this.SaleName,
                Signature = this.Signature,
                TaxType = this.TaxType,
                TaxTypeName = this.TaxTypeName,
                TotalAfterDiscountAmount = this.TotalAfterDiscountAmount,
                TotalAmount = this.TotalAmount,
                TotalBeforeVatAmount = this.TotalBeforeVatAmount,
                UpdateBy = this.UpdateBy,
                UpdateDate = this.UpdateDate,
                VAT = this.VAT,
                VatableAmount = this.VatableAmount,
                WithholdingAmount = this.WithholdingAmount,
                WithholdingKey = this.WithholdingKey,
                WithholdingRate = this.WithholdingRate,
                SSOFundPrice = this.SSOFundPrice,
                PayerDesc = this.PayerDesc,
                PayerMethod = this.PayerMethod,
                SSONumber = this.SSONumber,
                SSOPrice = this.SSOPrice,
                WithholdingTaxForm = this.WithholdingTaxForm,
                WithholdingTaxFormName = this.WithholdingTaxFormName
            };

            if (this.PurchaseOrderDetails != null && this.PurchaseOrderDetails.Count > 0)
            {
                result.WithholdingTaxDetails = new List<Document_WithholdingTaxDetail>();

                foreach (var item in this.PurchaseOrderDetails)
                {
                    var tmp = new Document_WithholdingTaxDetail()
                    {
                        BusinessID = item.BusinessID,
                        Description = item.Description,
                        //DiscountType = item.DiscountType,
                        Name = item.Name,
                        Price = item.Price,
                        PriceAfterTax = item.PriceAfterTax,
                        PriceBeforeTax = item.PriceBeforeTax,
                        PriceTax = item.PriceTax,
                        Sequence = item.Sequence,
                        Total = item.Total,
                        Vat = item.Vat,
                        VatAfter = item.VatAfter,
                        VatRate = item.VatRate,
                        VatType = item.VatType,
                        WithholdingTaxCatagory = item.WithholdingTaxCatagory,
                        WithholdingTaxCatagoryName = item.WithholdingTaxCatagoryName,
                        TaxPercent = item.TaxPercent,
                        TaxType = item.TaxType,
                        TaxPercentType = item.TaxPercentType
                    };
                    result.WithholdingTaxDetails.Add(tmp);
                }
            }

            return result;
        }
    }


}
