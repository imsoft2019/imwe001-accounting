﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using IMWE001_Accounting.Common;
using IMWE001_AccountingModels.Business;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Text;
using IMWE001_AccountingModels.Parameter;
using Microsoft.AspNetCore.Hosting;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingModels.User;
using System.Security.Cryptography;
using IMWE001_AccountingModels.Request;

namespace IMWE001_Accounting.Controllers
{
    [Produces("application/json")]
    public class SettingController : BaseController
    {

        public SettingController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }

        public IActionResult CompanyProfile()
        {
            return View();
        }

        public IActionResult UserProfile()
        {
            return View();
        }

        public IActionResult DocumentRuning()
        {
            return View();
        }


        public IActionResult DocumentRemark()
        {
            return View();
        }

        public IActionResult BankAccount()
        {
            return View();
        }
        public IActionResult ManageUser()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Business Profile 
        [HttpGet]
        public IActionResult GetBusinessProfile(string key)
        {
            try
            {
                UserProfile user = GetProfileUser();

                var responseval = GetApi<BusinessProfile>(string.Format("business/getprofile/{0}", user.BusinessID));

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessProfile([FromBody]BusinessProfile input)
        {
            try
            {
                UserProfile user = GetProfileUser();

                input.UpdateDate = DateTime.Now;
                input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("business/postprofile/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }


        [HttpPost]
        public IActionResult UploadCompanyProfile(List<IFormFile> files,List<IFormFile> rabbers)
        {
            try
            {
                long size = files.Sum(f => f.Length);

                // full path to file in temp location

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:CompanyProfile"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string fullpathrabber = Path.Combine(webRootPath, pathuploads);
                string filenamerabber = string.Empty;   
                string filename = string.Empty;

                foreach (var formFile in files)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    fullpath = Path.Combine(fullpath, filename);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(fullpath, FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }

                foreach (var formFile in rabbers)
                {
                    filenamerabber = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    fullpathrabber = Path.Combine(fullpathrabber, filenamerabber);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(fullpathrabber, FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }

                UserProfile user = GetProfileUser();

                BusinessProfile input = new BusinessProfile();
                input.UID = user.BusinessID;
                input.LogoPath = !string.IsNullOrEmpty(filename) ? fullpath : string.Empty;
                input.RabberStampLogo = !string.IsNullOrEmpty(filenamerabber) ?  filenamerabber : string.Empty;
                input.ThumnailLogo = !string.IsNullOrEmpty(filename) ? filename : string.Empty;

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("business/postprofileupload/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult UploadCompanyProfileRabber(List<IFormFile> files)
        {
            try
            {
                long size = files.Sum(f => f.Length);

                // full path to file in temp location

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:CompanyProfile"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Empty;

                foreach (var formFile in files)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    fullpath = Path.Combine(fullpath, filename);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(fullpath, FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }

                UserProfile user = GetProfileUser();

                BusinessProfile input = new BusinessProfile();
                input.UID = user.BusinessID;
                input.RabberStampLogo = filename;

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("business/postprofileupload/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        #endregion

        #region Account Bank
        [HttpGet]
        public IActionResult GetBusinessBankAccount(string key)
        {
            try
            {
                UserProfile user = GetProfileUser();
                List<BusinessBankAccount> responseval = GetApi<List<BusinessBankAccount>>(string.Format("business/getbank/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult DeleteBusinessBankAccount(string uid, string key)
        {
            try
            {
                UserProfile user = GetProfileUser();
                var responseval = GetApi(string.Format("business/deletebank/{0}?uid={1}", user.BusinessID, uid));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = "ลบข้อมูลเรียบร้อย"
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostBusinessBankAccount([FromBody]BusinessBankAccount input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("business/postbank/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        public IActionResult GetParameterBank(string key)
        {
            try
            {
                UserProfile user = GetProfileUser();
                //  List<Param_Bank> responseval = GetApi<List<Param_Bank>>("parameter/get/bank");

                var responseval = GetParam<List<Param_Bank>>("parameter/get/bank", "param:bank");

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        #region Document Remark
        [HttpGet]
        public IActionResult GetDocumentRemark(string key)
        {
            try
            {

                UserProfile user = GetProfileUser();
                List<DocumentRemark> responseval = GetApi<List<DocumentRemark>>(string.Format("business/getdocumentremark/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetDocumentRemarkByType(string key, string DocumentType)
        {
            try
            {

                UserProfile user = GetProfileUser();
                DocumentRemark responseval = GetApi<DocumentRemark>(string.Format("business/getdocumentremarkbytype/{0}?documenttype={1}", user.BusinessID, DocumentType));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostDocumentRemark([FromBody]DocumentRemark input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID.Value;
                input.CreateDate = input.UpdateDate = DateTime.Now;
                input.CreateBy = input.UpdateBy = user.UID.ToString();

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("business/postdocumentremark/{0}", user.BusinessID), jsonObject) as JsonResult;
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult DeleteDocumentRemark(string dockey, string key)
        {
            try
            {
                UserProfile user = GetProfileUser();
                var responseval = GetApi(string.Format("business/deletedocumentremark/{0}?dockey={1}", user.BusinessID, dockey));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = "ลบข้อมูลเรียบร้อย"
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        #endregion

        #region Document Running
        [HttpGet]
        public IActionResult GetDocumentRuning(string key)
        {
            try
            {
                UserProfile user = GetProfileUser();
                DocumentRuning responseval = GetApi<DocumentRuning>(string.Format("business/getdocumentparameter/{0}", user.BusinessID));
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostDocumentParameter([FromBody]DocumentRuning input)
        {
            try
            {
                UserProfile user = GetProfileUser();

                foreach (BusinessParameter param in input.BizParam)
                {
                    param.BusinessID = user.BusinessID;
                    param.UpdateDate = DateTime.Now;
                    param.UpdateBy = user.UID.ToString();
                }

                foreach (DocumentPrefix prefix in input.BizRun)
                {
                    prefix.BusinessID = user.BusinessID;
                    prefix.CreateDate = prefix.UpdateDate = DateTime.Now;
                    prefix.CreateBy = prefix.UpdateBy = user.UID.ToString();
                }

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi(string.Format("business/postdocumentparameter/{0}", user.BusinessID), jsonObject) as JsonResult;

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostGetDocumentNo([FromBody]GetDocumentNo input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;

                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApiReturnKey(string.Format("business/postgetdocumentno/{0}", user.BusinessID), jsonObject) as JsonResult;

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        #endregion

        #region User

        [HttpGet]
        public IActionResult GetParameterPosition()
        {
            try
            {
                //List<Param_Position> responseval = null;
                //if (siteenvironment == "Develop")
                //    responseval = GetApi<List<Param_Position>>("parameter/get/position");
                //else
                var responseval = this.GetParam<List<Param_Position>>("parameter/get/position","param:position");

                //responseval.Insert(0, new Param_Position() { ID = "", Name = "กรุณาเลือก" });

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });

            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostUserProfile([FromBody]UserProfile input)
        {
            try
            {
                UserProfile user = GetProfileUser();
                input.BusinessID = user.BusinessID;
                input.CreateBy = user.UID.ToString();
                input.UpdateBy = user.UID.ToString();

                if (input.UID == new Guid())
                {
                    input.Password = input.newPwd;
                    input.Pwd = MD5Hash(input.newPwd);
                }
                else
                {
                    bool isChange = true;
                    if (!string.IsNullOrEmpty(input.oldPwd))
                    {
                        isChange = (input.Pwd == MD5Hash(input.oldPwd));
                        if (!isChange)
                            return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = "รหัสผ่านเดิมไม่ถูกต้อง" });
                    }

                    if (!string.IsNullOrEmpty(input.newPwd) && isChange)
                    {
                        input.Password = input.newPwd;
                        input.Pwd = MD5Hash(input.newPwd);
                    }
                }


                var jsonObject = JsonConvert.SerializeObject(input);
                return PostApi("user", jsonObject) as JsonResult;


            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult UploadUserProfile(List<IFormFile> files)
        {
            try
            {
                long size = files.Sum(f => f.Length);

                // full path to file in temp location

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:UserProfile"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Empty;

                foreach (var formFile in files)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    if (formFile.Length > 0)
                    {
                        using (var stream = new FileStream(Path.Combine(fullpath, filename), FileMode.Create))
                        {
                            formFile.CopyTo(stream);
                        }
                    }
                }
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(), fullname = fullpath, filename = filename });


            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetUserProfiles()
        {
            try
            {
                UserProfile user = GetProfileUser();

                var responseval = GetApi<List<UserProfile>>(string.Format("user/profiles?businessid={0}", user.BusinessID));

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }
        #endregion

        public IActionResult GetUserProfile()
        {
            try
            {
                UserProfile user = GetProfileUser();

                var responseval = GetApi<UserProfile>(string.Format("user/profile?uid={0}&email=", user.UID));

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        public IActionResult GetIProfile()
        {
            try
            {
                UserProfile user = GetProfileUser();

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = user
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        private HttpClient GetHttpClient(string baseAddress)
        {
            return new HttpClient { BaseAddress = new Uri(baseAddress) };
        }

    }
}


public class ApiHelper
{ }