﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMWE001_Accounting.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using IMWE001_Accounting.Common;
using IMWE001_AccountingModels.Parameter;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.User;
using Microsoft.AspNetCore.Http;
using System.IO;
using IMWE001_Accounting.Common.RDService;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingModels.Business;

namespace IMWE001_Accounting.Controllers
{
    [Produces("application/json")]
    public class ReportController : BaseController
    {
        public ReportController(IHostingEnvironment hostingEnvironment) : base(hostingEnvironment) { }
        public IActionResult Reportsales()
        {
            return View();
        }

        public IActionResult Reportsalebycustomer()
        {
            return View();
        }

        public IActionResult Reportbillingnote()
        {
            return View();
        }

        public IActionResult Reportreceivables()
        {
            return View();
        }

        public IActionResult Reportpurchaseorder()
        {
            return View();
        }


        public IActionResult Reportreceivinginventory()
        {
            return View();
        }

        public IActionResult Reportexpense()
        {
            return View();
        }
        public IActionResult Reportsalestax()
        {
            return View();
        }

        public IActionResult Reportpurchasevat()
        {
            return View();
        }

        public IActionResult Reportwithholdingtax()
        {
            return View();
        }

        public IActionResult Reportaccountreceivable()
        {
            return View();
        }

        public IActionResult Reportaccountpayable()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private GetReportWithDate GetTypeDatefilter(GetReportWithDate input)
        {
            DateTime DateNow = DateTime.Now.Date;
            if (input.typedate == "1") // เดือนปัจจุบัน
            {
                DateTime DateBefore = new DateTime(DateNow.Year, DateNow.Month, 1);
                DateTime DateAfter = new DateTime(DateNow.Year, DateNow.Month, 1);
                DateAfter = DateAfter.AddMonths(1);
                DateAfter = DateAfter.AddDays(-1);
                input.startdate = DateBefore;
                input.enddate = DateAfter;
            }
            else if (input.typedate == "2") // เดือนก่อน
            {
                DateTime DateBefore = new DateTime(input.startdate.Year, input.startdate.Month, 1);
                DateTime DateAfter = new DateTime(DateBefore.Year, DateBefore.Month, 1);
                DateAfter = DateAfter.AddMonths(1);
                DateAfter = DateAfter.AddDays(-1);
                input.startdate = DateBefore;
                input.enddate = DateAfter;
            }
            else if (input.typedate == "3") // ปีปัจจุบัน
            {
                DateTime DateBefore = new DateTime(DateNow.Year, 1, 1);
                DateTime DateAfter = new DateTime(DateNow.Year, 12, 31);
                input.startdate = DateBefore;
                input.enddate = DateAfter;
            }
            else if (input.typedate == "4") // ปีก่อน
            {
                DateTime DateBefore = new DateTime(input.startdate.Year, 1, 1);
                DateTime DateAfter = new DateTime(input.startdate.Year, 12, 31);
                input.startdate = DateBefore;
                input.enddate = DateAfter;
            }   

            return input;
        }

        [HttpGet]
        public ActionResult DownloadFromPath(string file)
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string pathuploads = _config["UploadFilesPath:Report"];
            string fullpath = Path.Combine(webRootPath, pathuploads);
            fullpath = Path.Combine(fullpath, file);

            var stream = new FileStream(fullpath, FileMode.Open);
            return File(stream, "application/vnd.ms-excel", file);
        }

        [HttpPost]
        public IActionResult PostReportBilling([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportbilling/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportBilling([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportbilling/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("BillingNoteReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportSalesSummary([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportsalessummary/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportSalesSummary([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportsales/{0}", user.BusinessID.ToString()),jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("SalesReport_{0:ddMMyyyy}.xlsx",DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportReceipt([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportreceipt/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportReceipt([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportreceipt/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("CashCollectionReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportSalesCustomer([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportsalesbycustomer/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportSalesCustomer([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportsalesbycustomer/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("SalesByCustomerReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportPurchase([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportpurchase/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportPurchase([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportpurchase/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("PurchaseOrderReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportReceivingInventory([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportreceivinginventory/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportReceivingInventory([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportreceivinginventory/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("ReceivingInventoryReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportExpense([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportexpense/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportExpense([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportexpense/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("ExpenseReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportSalesTax([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                BusinessProfile business = new BusinessProfile();
                business = GetApi<BusinessProfile>(string.Format("business/getprofile/{0}", user.BusinessID));

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportsalestax/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input,
                    responsebusiness = business
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportSalesTax([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportsalestax/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("SalesTaxReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportPurchaseVat([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                BusinessProfile business = new BusinessProfile();
                business = GetApi<BusinessProfile>(string.Format("business/getprofile/{0}", user.BusinessID));

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportpurchasevat/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input,
                    responsebusiness = business
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportPurchaseVat([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportpurchasetax/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("PurchaseTaxReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportWithHoldingTax([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportwithholdingtax/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportWithHoldingTax([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportwithholdingtax/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("WithholdingTaxReport_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportReceivable([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportreceivable/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportReceivable([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportaccountreceivable/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("AccountReceivables_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostReportPayable([FromBody]GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> responseval = new List<DocumentReport>();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApi<List<DocumentReport>>(string.Format("report/reportpayable/{0}", user.BusinessID), jsonObject);
                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = responseval,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

        [HttpPost]
        public IActionResult PostXlsxReportPayable([FromBody]GetReportWithDate input)
        {
            try
            {
                string pathlink = _config["Report-API_WeAccount:PathUpload"];
                ResponseMessage responseval = new ResponseMessage();
                UserProfile user = GetProfileUser();
                input.businessid = user.BusinessID.Value;
                GetTypeDatefilter(input);

                var jsonObject = JsonConvert.SerializeObject(input);
                responseval = PostApiSite2<ResponseMessage>(string.Format("postreportaccountpayable/{0}", user.BusinessID.ToString()), jsonObject);

                string webRootPath = _hostingEnvironment.WebRootPath;
                string pathuploads = _config["UploadFilesPath:Report"];
                string fullpath = Path.Combine(webRootPath, pathuploads);
                string filename = string.Format("AccountPayables_{0:ddMMyyyy}.xlsx", DateTime.Now);
                fullpath = Path.Combine(fullpath, filename);

                byte[] sPDFDecoded = Convert.FromBase64String(responseval.reponsedata);
                System.IO.File.WriteAllBytes(fullpath, sPDFDecoded);

                return WriteJson(new
                {
                    responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString(),
                    responsedata = filename,
                    responsecondition = input
                });
            }
            catch (Exception ex)
            {
                return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
            }
        }

    }
}
