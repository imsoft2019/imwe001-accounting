﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingModels.DocumentBuy
{
    public class Document_ReceiveTaxInvoiceInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? SID { get; set; }
        public Guid BusinessID { get; set; }
        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string SupplierName { get; set; }

        public string SupplierTaxID { get; set; }

        public string SupplierBranch { get; set; }

        public string DocumentPath { get; set; }

        public string ThumbnailDocument { get; set; }

        public int isdelete { get; set; }

        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        
    }
}
