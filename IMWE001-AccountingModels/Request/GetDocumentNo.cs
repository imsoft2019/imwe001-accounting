﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Request
{
    public class GetDocumentNo
    {
        public Guid businessid { get; set; }
        public string asofdate { get; set; }
        public string documenttype { get; set; }
    }
}
