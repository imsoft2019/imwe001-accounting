﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Request
{
    public class GetReport
    {
        public Guid businessid { get; set; }
        public int DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        public string Content { get; set; }
        public string TypeAction { get; set; }
    }
}
