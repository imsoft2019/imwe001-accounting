﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Request
{
    public class GenerateDocumentRef
    {
        public Guid BusinessID { get; set; }
        public int? DocumentKey { get; set; }
        public int? ToDocumentKey { get; set; }
        public string ToDocumentNo { get; set; }
        public string remarkishas { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        
    }
}
