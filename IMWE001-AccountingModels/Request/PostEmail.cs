﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Request
{
    public class PostEmail
    {
        public string MailFrom { get; set; }
        public string MailName { get; set; }
        public string MailTo { get; set; }
        public string MailCC { get; set; }
        public string MailIsMe { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public int DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        public string Content { get; set; }
    }
}
