﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Request
{
    public class GetDocumentCumulative
    {
        public int documentkey { get; set; }
        public string documentype { get; set; }
    }
}
