﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Request
{
    public class GetReportWithDate
    {
        public Guid businessid { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public string typedate { get; set; }
    }
}
