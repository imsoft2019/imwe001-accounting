﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.User
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string PWD { get; set; }
    }
}
