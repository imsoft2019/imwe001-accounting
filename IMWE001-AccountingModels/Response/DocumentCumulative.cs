﻿using IMWE001_AccountingModels.DocumentSell;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Response
{
    public class DocumentCumulative
    {
        public List<Document_Invoice> Document_Invoice { get; set; }
        public List<Document_CreditNote> Document_CreditNote { get; set; }
        public List<Document_DebitNote> Document_DebitNote { get; set; }
    }
}
