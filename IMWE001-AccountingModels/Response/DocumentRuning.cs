﻿using IMWE001_AccountingModels.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Response
{
    public class DocumentRuning
    {
        public List<BusinessParameter> BizParam { get; set; }
        public List<DocumentPrefix> BizRun { get; set; }
    }
}
