﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingModels.ContactBook
{
    public class Contact
    {
        public Guid BusinessID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? ContactKey { get; set; }
        public string BusinessType { get; set; }
        public string BusinessTypeName { get; set; }
        public string ContactType { get; set; }
        public string ContactTypeName { get; set; }
        public string ContactCode { get; set; }
        public string BusinessName { get; set; }
        public string TaxID { get; set; }
        public string BranchType { get; set; }
        public string BranchTypeName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string ShippingAddress { get; set; }
        public string OfficeNumber { get; set; }
        public string FaxNumber { get; set; }
        public string WebSite { get; set; }
        public int? CreditDate { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactMobile { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string AccountType { get; set; }
        public string AccountTypeName { get; set; }
        public string AccountNo { get; set; }
        public string AccountBranch { get; set; }
        public string Note { get; set; }
        public string RevenueFlag { get; set; }
        public int? isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
