﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingModels.Log
{
    public class Log_Workflow
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? SID { get; set; }
        public Guid BusinessID { get; set; }
        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public int? ToDocumentKey { get; set; }
        public string ToDocumentNo { get; set; }
        public string CurrentStatus { get; set; }
        public string CurrentStatusName { get; set; }
        public string ToStatus { get; set; }
       public string ToStatusName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Remark { get; set; }
    }
}
