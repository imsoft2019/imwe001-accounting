﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingModels.EventLog
{
    public class Log_Activity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? SID { get; set; }
        public Guid BusinessID { get; set; }
        public DateTime? ActivityDate { get; set; }
        public string ActivityHeader { get; set; }
        public string ActivityTodo { get; set; }
        public string ActivityDesc { get; set; }
        public string ActivityByName { get; set; }
        public DateTime? CreateOn { get; set; }
        public string CreateBy { get; set; }
    }
}
