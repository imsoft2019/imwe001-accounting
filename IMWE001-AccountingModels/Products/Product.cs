﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingModels.Products
{
    public class Product
    {
        public Guid BusinessID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? ProductKey { get; set; }
        public string ProductType { get; set; }
        public string ProductTypeName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string UnitTypeName { get; set; }
        public string CategoryName { get; set; }
        public decimal? SellPrice { get; set; }
        public decimal? SellTaxPrice { get; set; }
        public decimal? SellAfterTaxPrice { get; set; }
        public string SellTaxType { get; set; }
        public string SellTaxTypeName { get; set; }
        public decimal? BuyPrice { get; set; }
        public decimal? BuyTaxPrice { get; set; }
        public decimal? BuyAfterTaxPrice { get; set; }
        public string BuyTaxType { get; set; }
        public string BuyTaxTypeName { get; set; }
        public string BuyDescription { get; set; }
        public string ProductBarCode { get; set; }
        public string PicturePath { get; set; }
        public string ThumbnailPath { get; set; }
        public int? isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
