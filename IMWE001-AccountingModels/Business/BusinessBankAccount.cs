﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Business
{
    public class BusinessBankAccount
    {
        public Guid BusinessID { get; set; }
        public Guid? UID { get; set; }
        public string BankCode { get; set; }
        public string AccountNo { get; set; }
        public string BranchName { get; set; }
        public string AccountName { get; set; }
        public string DepositType { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string BankName { get; set; }
        public string DepositTypeName { get; set; }
        public int isdelete { get; set; }
    }
}
