﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Business
{
   public  class DocumentPrefix
    {
       public  Guid? BusinessID { get; set; }
        public string DocumentType { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int? NextNumber { get; set; }
        public int? Len { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
