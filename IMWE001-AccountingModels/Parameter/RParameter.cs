﻿using IMWE001_AccountingModels.Business;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Parameter
{
    public class RParameter
    {
        public List<Param_Bank> Param_Bank { get; set; }
        public List<Param_UnitType> Param_UnitType { get; set; }
        public List<Param_Category> Param_Category { get; set; }
        public List<BusinessParameter> Biz_Param { get; set; }

        public List<Param_WithholdingRate> Param_WithholdingRate { get; set; }

    }
}
