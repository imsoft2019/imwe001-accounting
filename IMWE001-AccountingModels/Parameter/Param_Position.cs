﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Parameter
{
    public class Param_Position
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
