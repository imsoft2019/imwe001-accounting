﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingModels.Parameter
{
    public class Param_WithholdingRate
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public decimal? Rate { get; set; }
        public int isactive { get; set; }
    }
}
