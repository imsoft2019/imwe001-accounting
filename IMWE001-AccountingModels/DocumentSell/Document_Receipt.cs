﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace IMWE001_AccountingModels.DocumentSell
{
    public class Document_Receipt
    {
        public Guid BusinessID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? ReceiptKey { get; set; }
        public string ReceiptNo { get; set; }
        public int? BillingNoteCumulativeKey { get; set; }
        public string BillingNoteCumulativeNo { get; set; }
        public string ReceiptStatus { get; set; }
        public string ReceiptStatusName { get; set; }
        public string ReceiptType { get; set; }
        public string ReceiptTypeName { get; set; }
        public int? CustomerKey { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTaxID { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerContactEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerBranch { get; set; }
        public DateTime? ReceiptDate { get; set; }
        public Guid? SaleID { get; set; }
        public string SaleName { get; set; }
        public string ProjectName { get; set; }
        public string ReferenceNo { get; set; }
        public string TaxType { get; set; }
        public string TaxTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string IsDiscountHeader { get; set; }
        public string IsDiscountHeaderType { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? TotalAfterDiscountAmount { get; set; }
        public string IsTaxHeader { get; set; }
        public string IsTaxSummary { get; set; }
        public decimal? ExemptAmount { get; set; }
        public decimal? VatableAmount { get; set; }
        public decimal? VAT { get; set; }
        public decimal? TotalBeforeVatAmount { get; set; }
        public string IsWithholdingTax { get; set; }
        public string WithholdingKey { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? WithholdingAmount { get; set; }
        public string IsAdjustDiscount { get; set; }
        public string AdjustDiscountType { get; set; }
        public string AdjustDiscountTypeName { get; set; }
        public decimal? AdjustDiscountAmount { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? GrandAmount { get; set; }
        public string Remark { get; set; }
        public string Signature { get; set; }
        public string Noted { get; set; }
        public int isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public List<Document_ReceiptDetail> ReceiptDetail { get; set; }

        public List<Document_ReceiptCumulativeDetail> ReceiptCumulativeDetail { get; set; }

        [NotMapped]
        public Document_ReceiveInfo ReceiptInfo { get; set; }

        [NotMapped]
        public int? DocumentKey { get; set; }
        [NotMapped]
        public string DocumentNo { get; set; }
        [NotMapped]
        public string DocumentOwner { get; set; }
        [NotMapped]
        public string DocumentRefNo { get; set; }
    }
}
