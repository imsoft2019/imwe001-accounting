﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMWE001_AccountingModels.DocumentSell
{
    public class Document_ReceiptCumulativeDetail
    {
        public Guid BusinessID { get; set; }
        public int? ReceiptKey { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? ReceiptDetailKey { get; set; }
        public int? Sequence { get; set; }
        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        public int? DocumentCategoryKey { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string   TaxType { get; set; }
        public string   TaxTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string   IsDiscountHeader { get; set; }
        public string   IsDiscountHeaderType { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? TotalAfterDiscountAmount { get; set; }
        public string   IsTaxHeader { get; set; }
        public string   IsTaxSummary { get; set; }
        public decimal? ExemptAmount { get; set; }
        public decimal? VatableAmount { get; set; }
        public decimal? VAT { get; set; }
        public decimal? TotalBeforeVatAmount { get; set; }
        public string   IsWithholdingTax { get; set; }
        public string     WithholdingKey { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? WithholdingAmount { get; set; }
        public decimal? GrandAmountAfterWithholding { get; set; }
        public decimal? GrandAmount { get; set; }
    }
}
