﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace IMWE001_AccountingModels.DocumentSell
{
    public class Document_DebitNote
    {
        public Guid BusinessID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? DebitNoteKey { get; set; }
        public string DebitNoteNo { get; set; }
        public int? InvoiceKey { get; set; }
        public string InvoiceNo { get; set; }
        public int? BillingNoteKey { get; set; }
        public string BillingNoteNo { get; set; }
        public string DebitNoteStatus { get; set; }
        public string DebitNoteStatusName { get; set; }
        public int? ReceiptKey { get; set; }
        public string ReceiptNo { get; set; }
        public int? CustomerKey { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTaxID { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerContactEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerBranch { get; set; }
        public DateTime? DebitNoteDate { get; set; }
        public string CreditType { get; set; }
        public string CreditTypeName { get; set; }
        public string CauseTypeOther { get; set; }
        public int? CreditDay { get; set; }
        public DateTime? DueDate { get; set; }
        public Guid? SaleID { get; set; }
        public string SaleName { get; set; }
        public string ProjectName { get; set; }
        public string ReferenceNo { get; set; }
        public string CauseType { get; set; }
        public string CauseTypeName { get; set; }
        public string TaxType { get; set; }
        public string TaxTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string IsDiscountHeader { get; set; }
        public string IsDiscountHeaderType { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? TotalAfterDiscountAmount { get; set; }
        public string IsTaxHeader { get; set; }
        public string IsTaxSummary { get; set; }
        public decimal? ExemptAmount { get; set; }
        public decimal? VatableAmount { get; set; }
        public decimal? VAT { get; set; }
        public decimal? TotalBeforeVatAmount { get; set; }
        public string IsWithholdingTax { get; set; }
        public string WithholdingKey { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? WithholdingAmount { get; set; }
        public decimal? GrandAmountAfterWithholding { get; set; }
        public decimal? DebitPriceBefore { get; set; }
        public decimal? DebitPriceReal { get; set; }
        public decimal? DebitPriceAfter { get; set; }
        public decimal? GrandAmount { get; set; }
        public string Remark { get; set; }
        public string Signature { get; set; }
        public string Noted { get; set; }
        public int isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public List<Document_DebitNoteDetail> DebitNoteDetail { get; set; }

        [NotMapped]
        public List<Document_InvoiceDetail> InvoiceDetail { get; set; }
        [NotMapped]
        public int? DocumentKey { get; set; }
        [NotMapped]
        public string DocumentNo { get; set; }
        [NotMapped]
        public string DocumentOwner { get; set; }
        [NotMapped]
        public string DocumentRefNo { get; set; }
    }
}
