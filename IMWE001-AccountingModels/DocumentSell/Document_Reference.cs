﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingModels.DocumentSell
{
    public class Document_Reference
    {
        public Guid BusinessID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? SID { get; set; }
        public string DocumentOwner { get; set; }
        public string DocumentGen { get; set; }
        public string QuotationNo { get; set; }
        public string BillingNo { get; set; }
        public string InvoiceNo { get; set; }
        public string ReceiptNo { get; set; }
        public string CashSaleNo { get; set; }
        public string CreditNoteNo { get; set; }
        public string DebitNoteNo { get; set; }
        public string PurchaseNo { get; set; }
        public string ReceivingInventoryNo { get; set; }
        public string ExpenseNo { get; set; }
        public string WithholdingTaxNo { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
