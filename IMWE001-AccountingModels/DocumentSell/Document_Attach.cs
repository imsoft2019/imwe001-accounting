﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace IMWE001_AccountingModels.DocumentSell
{
    public class Document_Attach
    {
        public Guid BusinessID { get; set; }
        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? AttachKey { get; set; }
        public string AttachName { get; set; }
        public string AttachPath { get; set; }
        public string AttachExtension { get; set; }
        public int isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
