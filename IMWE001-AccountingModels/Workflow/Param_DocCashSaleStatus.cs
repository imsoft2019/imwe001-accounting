﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingModels.Workflow
{
    public class Param_DocCashSaleStatus
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
