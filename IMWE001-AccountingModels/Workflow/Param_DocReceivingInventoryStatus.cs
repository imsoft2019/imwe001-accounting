﻿using System;

namespace IMWE001_AccountingModels.Workflow
{
    public class Param_DocReceivingInventoryStatus
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
