﻿namespace IMWE001_AccountingModels.Workflow
{
    public class Param_DocExpenseStatus
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}