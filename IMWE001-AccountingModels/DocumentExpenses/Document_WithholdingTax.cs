﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMWE001_AccountingModels.DocumentExpenses
{
   public class Document_WithholdingTax
    {

        public Guid BusinessID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? WithholdingTaxKey { get; set; }
        public string WithholdingTaxNo { get; set; }
        public string WithholdingTaxStatus { get; set; }
        public string WithholdingTaxStatusName { get; set; }
        public int? CustomerKey { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTaxID { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerContactEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerBranch { get; set; }
        public DateTime? WithholdingTaxDate { get; set; }
        public string CreditType { get; set; }
        public string CreditTypeName { get; set; }
        public int? CreditDay { get; set; }
        public DateTime? DueDate { get; set; }
        public Guid? SaleID { get; set; }
        public string SaleName { get; set; }
        public string ProjectName { get; set; }
        public string ReferenceNo { get; set; }
        public string TaxType { get; set; }
        public string TaxTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string IsDiscountHeader { get; set; }
        public string IsDiscountHeaderType { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? TotalAfterDiscountAmount { get; set; }
        public string IsTaxHeader { get; set; }
        public string IsTaxSummary { get; set; }
        public decimal? ExemptAmount { get; set; }
        public decimal? VatableAmount { get; set; }
        public decimal? VAT { get; set; }
        public decimal? TotalBeforeVatAmount { get; set; }
        public string IsWithholdingTax { get; set; }
        public string WithholdingKey { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? WithholdingAmount { get; set; }
        public decimal? GrandAmountAfterWithholding { get; set; }
        public decimal? GrandAmount { get; set; }
        public string Remark { get; set; }
        public string Signature { get; set; }
        public string Noted { get; set; }
        public int isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public string WithholdingTaxForm { get; set; }
        public string WithholdingTaxFormName { get; set; }
        public string PayerMethod { get; set; }
        public string PayerDesc { get; set; }
        public string SSONumber { get; set; }
        public decimal SSOPrice { get; set; }
        public decimal SSOFundPrice { get; set; }

        public List<Document_WithholdingTaxDetail> WithholdingTaxDetails { get; set; }

        [NotMapped]
        public int? DocumentKey { get; set; }
        [NotMapped]
        public string DocumentNo { get; set; }
        [NotMapped]
        public string DocumentOwner { get; set; }
        [NotMapped]
        public string DocumentRefNo { get; set; }
    }
}
