﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMWE001_AccountingModels.DocumentExpenses
{
    public class Document_WithholdingTaxDetail
    {
        public Guid BusinessID { get; set; }

        public int? WithholdingTaxKey { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WithholdingTaxDetailKey { get; set; }

        public int? Sequence { get; set; }
        public string WithholdingTaxCatagory { get; set; }
        public string WithholdingTaxCatagoryName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? TaxPercent { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceTax { get; set; }
        public decimal? PriceAfterTax { get; set; }
        public decimal? PriceBeforeTax { get; set; }
        // public string DiscountType { get; set; }
        public string VatType { get; set; }
        public decimal? VatRate { get; set; }
        public decimal? Vat { get; set; }
        public decimal? VatAfter { get; set; }
        public decimal? Total { get; set; }
        public string TaxType { get; set; }
        public string TaxPercentType { get; set; }
    }

}
