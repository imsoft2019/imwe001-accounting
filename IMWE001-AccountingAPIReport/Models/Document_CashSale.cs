﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class Document_CashSale
    {
        public string Page { get; set; }
        public string AmountTH { get; set; }
        public string ChkColDisCountDetail { get; set; }
        public string ChkColVatDetail { get; set; }
        public string Content { get; set; }
        public string CompanyName { get; set; }
        public string BranchCompanyName { get; set; }
        public byte[] ImageUserProfile { get; set; }
        public byte[] ImageRabberBusinessProfile { get; set; }
        public byte[] ImageBusinessProfile { get; set; }
        public Guid BusinessID { get; set; }
        public int? CashSaleKey { get; set; }
        public string CashSaleNo { get; set; }
        public string CashSaleStatus { get; set; }
        public string CashSaleStatusName { get; set; }
        public int? CustomerKey { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTaxID { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerContactEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerBranch { get; set; }
        public DateTime? CashSaleDate { get; set; }
        public Guid? SaleID { get; set; }
        public string SaleName { get; set; }
        public string ProjectName { get; set; }
        public string ReferenceNo { get; set; }
        public string TaxType { get; set; }
        public string TaxTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string IsDiscountHeader { get; set; }
        public string IsDiscountHeaderType { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? TotalAfterDiscountAmount { get; set; }
        public string IsTaxHeader { get; set; }
        public string IsTaxSummary { get; set; }
        public decimal? ExemptAmount { get; set; }
        public decimal? VatableAmount { get; set; }
        public decimal? VAT { get; set; }
        public decimal? TotalBeforeVatAmount { get; set; }
        public string IsWithholdingTax { get; set; }
        public string WithholdingKey { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? WithholdingAmount { get; set; }
        public string IsAdjustDiscount { get; set; }
        public string AdjustDiscountType { get; set; }
        public string AdjustDiscountTypeName { get; set; }
        public decimal? AdjustDiscountAmount { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? GrandAmount { get; set; }
        public string Remark { get; set; }
        public string Signature { get; set; }
        public string Noted { get; set; }
        public int isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string ChkCash { get; set; }
        public string ChkCheque { get; set; }
        public string ChkTranfer { get; set; }
        public string ChkCreditCard { get; set; }
        public decimal? WithholdingAmount2 { get; set; }
        public string Bank { get; set; }
        public string No { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? NetPayAmount { get; set; }
        public List<Document_CashSaleDetail> CashSaleDetail { get; set; }

        public Document_ReceiveInfo ReceiptInfo { get; set; }

        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentOwner { get; set; }
        public string DocumentRefNo { get; set; }
        public List<Document_CashSale> GetDatas()
        {
            return new List<Document_CashSale>();
        }
    }
}
