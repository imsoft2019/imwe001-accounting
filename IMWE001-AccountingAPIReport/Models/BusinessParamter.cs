﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class BusinessParameter
    {
        public Guid? BusinessID { get; set; }
        public string ParameterField { get; set; }
        public string ParameterValue { get; set; }
        public string ParameterDescription { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}