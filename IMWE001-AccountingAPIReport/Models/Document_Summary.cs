﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class Document_Summary
    {
        public int Seq { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public List<Document_Summary> GetDatas()
        {
            return new List<Document_Summary>();
        }
    }
}