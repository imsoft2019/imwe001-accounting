﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class Document_Billing
    {
        public string Page { get; set; }
        public string AmountTH { get; set; }
        public string ChkColDisCountDetail { get; set; }
        public string ChkColVatDetail { get; set; }
        public string CompanyName { get; set; }
        public string DocumentNameHeader { get; set; }
        public string BranchCompanyName { get; set; }
        public byte[] ImageRabberBusinessProfile { get; set; }
        public byte[] ImageUserProfile { get; set; }
        public byte[] ImageBusinessProfile { get; set; }
        public string Content { get; set; }
        public Guid BusinessID { get; set; }
        public int? BillingKey { get; set; }
        public string BillingNo { get; set; }
        public string BillingStatus { get; set; }
        public string BillingStatusName { get; set; }
        public string BillingType { get; set; }
        public string BillingTypeName { get; set; }
        public int? CustomerKey { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTaxID { get; set; }
        public string CustomerContactName { get; set; }
        public string CustomerContactPhone { get; set; }
        public string CustomerContactEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerBranch { get; set; }
        public DateTime? BillingDate { get; set; }
        public string CreditType { get; set; }
        public string CreditTypeName { get; set; }
        public int? CreditDay { get; set; }
        public DateTime? DueDate { get; set; }
        public Guid? SaleID { get; set; }
        public string SaleName { get; set; }
        public string ProjectName { get; set; }
        public string ReferenceNo { get; set; }
        public string TaxType { get; set; }
        public string TaxTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string IsDiscountHeader { get; set; }
        public string IsDiscountHeaderType { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? TotalAfterDiscountAmount { get; set; }
        public string IsTaxHeader { get; set; }
        public string IsTaxSummary { get; set; }
        public decimal? ExemptAmount { get; set; }
        public decimal? VatableAmount { get; set; }
        public decimal? VAT { get; set; }
        public decimal? TotalBeforeVatAmount { get; set; }
        public string IsWithholdingTax { get; set; }
        public string WithholdingKey { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? WithholdingAmount { get; set; }
        public decimal? GrandAmountAfterWithholding { get; set; }
        public decimal? GrandAmount { get; set; }
        public string Remark { get; set; }
        public string Signature { get; set; }
        public string Noted { get; set; }
        public int isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public List<Document_BillingDetail> BillingDetail { get; set; }

        public List<Document_BillingCumulativeDetail> BillingCumulativeDetail { get; set; }

        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentOwner { get; set; }
        public string DocumentRefNo { get; set; }

        public List<Document_Billing> GetDatas()
        {
            return new List<Document_Billing>();
        }
    }
}