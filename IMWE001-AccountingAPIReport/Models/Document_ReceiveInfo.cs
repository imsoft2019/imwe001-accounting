﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class Document_ReceiveInfo
    {
        public Guid BusinessID { get; set; }
       
        public int? SID { get; set; }
        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        public string CustomerName { get; set; }
        public decimal? PaymentAmount { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? NetPaymentAmount { get; set; }
        public string IsWithholdingTax { get; set; }
        public string WithholdingKey { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? WithholdingAmount { get; set; }
        public string IsDiscount { get; set; }
        public string DiscountType { get; set; }
        public string DiscountTypeName { get; set; }
        public decimal? DiscountAmount { get; set; }
        public string PaymentType { get; set; }
        public string PaymentTypeName { get; set; }
        public Guid? BankAccountID { get; set; }
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string BankAccountNo { get; set; }
        public string BankAccountType { get; set; }
        public decimal? PaymentFee { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string ChequeNo { get; set; }
        public string Comment { get; set; }
        public string BalanceType { get; set; }
        public decimal? Balance { get; set; }
        public string CauseType { get; set; }
        public string CauseTypeName { get; set; }
        public string CustomerBankCode { get; set; }
        public string CustomerBankName { get; set; }
        public string CustomerAccountNo { get; set; }
        public int isdelete { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}