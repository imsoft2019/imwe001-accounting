﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IMWE001_AccountingAPIReport.Models
{
    public class UserProfile
    {
        public Guid UID { get; set; }

        public Guid? BusinessID { get; set; }

        public string Email { get; set; }


        public string Pwd { get; set; }
        public string Password { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Position { get; set; }

        public string Telephone { get; set; }

        public string ThumbnailSignature { get; set; }


        public string SignaturePath { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreateDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime UpdateDate { get; set; }

        public string UpdateBy { get; set; }

        public int IsDelete { get; set; }

        public string EmployeeName
        {
            get { return Name + " " + Surname; }
        }

        public string newPwd { get; set; }

        public string oldPwd { get; set; }

        public string PositionName { get; set; }
    }
}
