﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class DocumentReport
    {
        public int? DocumentKey { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentType { get; set; }
        public DateTime? DocumentDate { get; set; }
        public string CreditType { get; set; }
        public int? CreditDay { get; set; }
        public DateTime? DueDate { get; set; }
        public int? CustomerKey { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string ProjectName { get; set; }
        public string TaxID { get; set; }
        public string BranchName { get; set; }
        public string CategoryName { get; set; }
        public string ReferenceNo { get; set; }
        public string SaleName { get; set; }
        public string WithHoldingTaxFormName { get; set; }
        public string WithHoldingRateDesc { get; set; }
        public decimal? Total { get; set; }
        public decimal? VAT { get; set; }
        public decimal? GrandAmount { get; set; }
        public string DocumentStatus { get; set; }
        public string DocumentStatusName { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentType { get; set; }
        public string PaymentDescription { get; set; }
        public decimal? PaymentAmount { get; set; }
        public string PaymentDiscountType { get; set; }
        public decimal? PaymentDiscount { get; set; }
        public string TaxRate { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? PaymentBalance { get; set; }
        public string CauseType { get; set; }
        public string Remark { get; set; }
        public string Noted { get; set; }
        public string DocumentRefNo { get; set; }

        public string BillingNoteNo { get; set; }
        public string ReceiptNo { get; set; }

        public Document_ReceiveInfo ReceiptInfo { get; set; }
    }
}