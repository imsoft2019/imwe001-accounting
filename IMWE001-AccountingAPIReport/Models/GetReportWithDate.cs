﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class GetReportWithDate
    {
        public Guid businessid { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public string typedate { get; set; }
    }
}