﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport
{
   
    public class QuotationData
    {
        public string Date { get; set; }
        public string No { get; set; }
        public string SaleName { get; set; }
        public string ProjectName { get; set; }

        public string TaxNo { get; set; }
        public string Address { get; set; }
        public string CustomerName { get; set; }
        public string Vat7 { get; set; }
        public string Vat3 { get; set; }
        public string Total1 { get; set; }
        public string Total2 { get; set; }
        public string Total3 { get; set; }
        public string Total2TH { get; set; }
        public QuotationData()
        {
          
        }

        public List<QuotationData> GetDatas()
        {
            return new List<QuotationData>();
        }
    }
}