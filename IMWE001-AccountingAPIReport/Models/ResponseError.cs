﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class ResponseError
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}