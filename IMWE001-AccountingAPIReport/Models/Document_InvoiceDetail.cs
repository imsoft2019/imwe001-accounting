﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Models
{
    public class Document_InvoiceDetail
    {
        public Guid BusinessID { get; set; }
        public int? InvoiceKey { get; set; }
        public int? InvoiceDetailKey { get; set; }
        public int? Sequence { get; set; }
        public int? ProductKey { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public string Unit { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceTax { get; set; }
        public decimal? PriceAfterTax { get; set; }
        public decimal? PriceBeforeTax { get; set; }
        public string DiscountType { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? Discount { get; set; }
        public decimal? DiscountAfter { get; set; }
        public string VatType { get; set; }
        public decimal? VatRate { get; set; }
        public decimal? Vat { get; set; }
        public decimal? VatAfter { get; set; }
        public decimal? Total { get; set; }

        public List<Document_InvoiceDetail> GetDatas()
        {
            return new List<Document_InvoiceDetail>();
        }
    }
}