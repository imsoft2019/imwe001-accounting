﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport
{
   
    public class QuotationDetailData
    {
        public string No { get; set; }
        public string Detail { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }

        public string Total { get; set; }
        public QuotationDetailData()
        {
          
        }

        public List<QuotationDetailData> GetDatas()
        {
            return new List<QuotationDetailData>();
        }
    }
}