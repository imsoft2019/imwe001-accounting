﻿using System.Web;
using System.Web.Mvc;

namespace IMWE001_AccountingAPIReport
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
