﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Controllers
{
    public class ExcelController : ApiController
    {
        string[] monthsShort = {"none", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค." };
        string[] monthsFull = { "none","มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
        private Contact GetContact(string businessid, string contactKey)

        {
            ApiUtilities api = new ApiUtilities();
            List<Contact> responseval = api.GetApi<List<Contact>>(string.Format("contactbook/getcontact/{0}", businessid));
            Contact c = responseval.Where(x => x.ContactKey.Value == int.Parse(contactKey)).FirstOrDefault();
            return c;
        }
        private BusinessProfile GetBusinessProfile(string businessid)
        {
            ApiUtilities api = new ApiUtilities();
            BusinessProfile responseval = api.GetApi<BusinessProfile>(string.Format("business/getprofile/{0}", businessid));
            return responseval;
        }
        private UserProfile GetUserProfile(string saleid)
        {
            ApiUtilities api = new ApiUtilities();
            UserProfile responseval = api.GetApi<UserProfile>(string.Format("user/profile/?uid={0}", saleid));
            return responseval;
        }
        private byte[] GetFileImageBusinessProfile(string fileImage)
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["UrlWebMain"] + "/uploads/company/" + fileImage;
            return GetImage(url);
        }
        private byte[] GetFileImageUserProfile(string fileImage)
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["UrlWebMain"] + "/uploads/user/" + fileImage;
            return GetImage(url);
        }
        private byte[] GetImage(string urlImage)
        {
            byte[] datas = null;
            using (WebClient client = new WebClient())
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                    delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                System.Security.Cryptography.X509Certificates.X509Chain chain,
                                System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true; // **** Always accept
                    };
                try
                {
                    Stream stream = client.OpenRead(urlImage);
                    if (stream != null)
                    {
                        datas = client.DownloadData(urlImage);
                    }
                }
                catch (Exception ex)
                {

                }


            }
            return datas;
        }

        [HttpPost]
        [Route("postreportsales/{businessid}")]
        public ResponseMessage postreportsales(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportsalessummary/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptSales rpt = new Excel.RptSales();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("SalesReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "SalesReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportbilling/{businessid}")]
        public ResponseMessage postreportbilling(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportbilling/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptBilling rpt = new Excel.RptBilling();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("BillingNoteReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "BillingNoteReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportreceipt/{businessid}")]
        public ResponseMessage postreportreceipt(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportreceipt/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptReceipt rpt = new Excel.RptReceipt();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("CashCollectionReport_.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "CashCollectionReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportsalesbycustomer/{businessid}")]
        public ResponseMessage postreportsalesbycustomer(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportsalesbycustomer/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptSalesByCustomer rpt = new Excel.RptSalesByCustomer();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("SalesByCustomerReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "SalesByCustomerReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportpurchase/{businessid}")]
        public ResponseMessage postreportpurchase(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportpurchase/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptPurchaseOrder rpt = new Excel.RptPurchaseOrder();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("PurchaseOrderReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "PurchaseOrderReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportreceivinginventory/{businessid}")]
        public ResponseMessage postreportreceivinginventory(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportreceivinginventory/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptReceivingInventory rpt = new Excel.RptReceivingInventory();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("ReceivingInventoryReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "ReceivingInventoryReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportexpense/{businessid}")]
        public ResponseMessage postreportexpense(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportexpense/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptExpense rpt = new Excel.RptExpense();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("ExpenseReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "ExpenseReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportsalestax/{businessid}")]
        public ResponseMessage postreportsalestax(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportsalestax/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptSalesTax rpt = new Excel.RptSalesTax();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodMonth =  monthsFull[Convert.ToInt32(start[1])];
                rpt.PeriodYear = start[2];
                string filename = string.Format("SalesTaxReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "SalesTaxReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportpurchasetax/{businessid}")]
        public ResponseMessage postreportpurchasetax(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportpurchasevat/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptPruchaseTax rpt = new Excel.RptPruchaseTax();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodMonth = monthsFull[Convert.ToInt32(start[1])];
                rpt.PeriodYear = start[2];
                string filename = string.Format("PurchaseTaxReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "PurchaseTaxReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportwithholdingtax/{businessid}")]
        public ResponseMessage postreportwithholdingtax(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportwithholdingtax/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');
                string[] end = input.enddate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptWithholddingtax rpt = new Excel.RptWithholddingtax();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0} - {1}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2], end[0] + " " + monthsShort[Convert.ToInt32(end[1])] + " " + end[2]);
                string filename = string.Format("WithholdingTaxReport.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "WithholdingTaxReport");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportaccountreceivable/{businessid}")]
        public ResponseMessage postreportaccountreceivable(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportreceivable/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptAccountReceivables rpt = new Excel.RptAccountReceivables();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2]) ;
                string filename = string.Format("AccountReceivables.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "AccountReceivables");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpPost]
        [Route("postreportaccountpayable/{businessid}")]
        public ResponseMessage postreportaccountpayable(string businessid, [FromBody]GetReportWithDate input)
        {
            ResponseMessage result = new ResponseMessage();
            try
            {
                List<DocumentReport> datas = new List<DocumentReport>();
                ApiUtilities api = new ApiUtilities();

                var jsonObject = JsonConvert.SerializeObject(input);
                List<DocumentReport> responseval = api.PostApi<List<DocumentReport>>(string.Format("excel/reportpayable/{0}", businessid), jsonObject);
                BusinessProfile bu = GetBusinessProfile(businessid);

                string[] start = input.startdate.ToString("dd/MM/yyyy").Substring(0, 10).Split('/');

                Excel.RptAccountPayables rpt = new Excel.RptAccountPayables();
                rpt.business = bu;
                rpt.list = responseval;
                rpt.PeriodDate = string.Format("{0}", start[0] + " " + monthsShort[Convert.ToInt32(start[1])] + " " + start[2]);
                string filename = string.Format("AccountPayables.xlsx", DateTime.Now);
                string Path = System.Web.Hosting.HostingEnvironment.MapPath(string.Format(@"~\\Temp\\" + filename));
                rpt.Writes(Path, "AccountPayables");
                string filebase = ConvertFileToBase64(Path);
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }


        protected string ConvertFileToBase64(string urlPath)
        {
            byte[] imageBytes = System.IO.File.ReadAllBytes(urlPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }

}
