﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Controllers
{
    public class ReportController : ApiController
    {

        private Contact GetContact(string businessid, string contactKey)

        {
            ApiUtilities api = new ApiUtilities();
            List<Contact> responseval = api.GetApi<List<Contact>>(string.Format("contactbook/getcontact/{0}", businessid));
            Contact c = responseval.Where(x => x.ContactKey.Value == int.Parse(contactKey)).FirstOrDefault();
            return c;
        }
        private BusinessProfile GetBusinessProfile(string businessid)
        {
            ApiUtilities api = new ApiUtilities();
            BusinessProfile responseval = api.GetApi<BusinessProfile>(string.Format("business/getprofile/{0}", businessid));
            return responseval;
        }
        private BusinessParameter GetBusinessParameter(string businessid,string parafield)
        {
            BusinessParameter val = new BusinessParameter();
            ApiUtilities api = new ApiUtilities();
            List<BusinessParameter> responseval = api.GetApi< List<BusinessParameter>>(string.Format("business/getdocumentparameterheader/{0}", businessid));
            if (responseval != null && responseval.Count > 0)
                val = responseval.Where(a => a.ParameterField == parafield).FirstOrDefault();
            return val;
        }
        private UserProfile GetUserProfile(string saleid)
        {
            ApiUtilities api = new ApiUtilities();
            UserProfile responseval = api.GetApi<UserProfile>(string.Format("user/profile/?uid={0}", saleid));
            return responseval;
        }
        private byte[] GetFileImageBusinessProfile(string fileImage)
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["UrlWebMain"] + "/uploads/company/" + fileImage;
            return GetImage(url);
        }
        private byte[] GetFileImageUserProfile(string fileImage)
        {
            string url = System.Configuration.ConfigurationManager.AppSettings["UrlWebMain"] + "/uploads/user/" + fileImage;
            return GetImage(url);
        }
        private byte[] GetImage(string urlImage)
        {
            byte[] datas = null;
            using (WebClient client = new WebClient())
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                    delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                                System.Security.Cryptography.X509Certificates.X509Chain chain,
                                System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    {
                        return true; // **** Always accept
                    };
                try
                {
                    Stream stream = client.OpenRead(urlImage);
                    if (stream != null)
                    {
                        datas = client.DownloadData(urlImage);
                    }
                }
                catch (Exception ex)
                {

                }


            }
            return datas;
        }
        [HttpGet]
        [Route("GetQuotation/{businessid}/{quotationkey}")]
        public ResponseMessage GetQuotation(string businessid, string quotationkey, string content)
        {
            string dataset1 = "Quotation_DS";
            string dataset2 = "QuotationDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "Quotation";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_Quotation> datas = new List<Document_Quotation>();
                List<Document_QuotationDetail> datasdetail = new List<Document_QuotationDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_Quotation responseval = api.GetApi<Document_Quotation>(string.Format("documentsell/getquotationbykey/{0}/{1}", businessid, quotationkey));
                BusinessProfile bu = GetBusinessProfile(businessid);

                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;

                string[] header = GetBusinessParameter(businessid, "DocumentQuotation").ParameterDescription.Split('|');
                responseval.DocumentNameHeader = header[0];
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;

                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";

                #region
                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                {
                    reportrdlc = "Quotation";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });

                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                {
                    reportrdlc = "Quotation";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "Quotation2";
                    responseval.ChkColVatDetail = "show";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักสว่นลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                {
                    reportrdlc = "Quotation3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "Quotation3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                {
                    reportrdlc = "Quotation3.rdlc";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                }

                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,

                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });
                }



                #endregion
                datassummary = datassummary.OrderBy(x => x.Seq).ToList();

                if (content.ToUpper() == "ORIGINAL" || content.ToUpper() == "NORMAL")
                {
                    datas.Add(responseval);
                    datas[datas.Count - 1].Content = "ต้นฉบับ";
                    datas[datas.Count - 1].Page = "1";
                    for (int i = 0; i < responseval.QuotationDetail.Count; i++)
                    {
                        datasdetail.Add(responseval.QuotationDetail[i]);
                    }
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                if (content.ToUpper() == "COPY" || content.ToUpper() == "NORMAL")
                {
                    if (datas.Count == 0)
                    {
                        datas.Add(responseval);
                        datas[datas.Count - 1].Page = "1";
                        for (int i = 0; i < responseval.QuotationDetail.Count; i++)
                        {
                            datasdetail.Add(responseval.QuotationDetail[i]);
                        }
                    }
                    else
                    {
                        datas[datas.Count - 1].Page = "2";
                    }
                    datas[datas.Count - 1].Content = "สำเนา";
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }
                string newfilename = "All" + Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                filename = filename.Replace(Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename)), newfilename);
                pdfmerg.DestinationFile = System.Web.Hosting.HostingEnvironment.MapPath(filename);
                pdfmerg.Execute();


                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }
        [HttpGet]
        [Route("GetBilling/{businessid}/{billingkey}/{content}")]
        public ResponseMessage GetBilling(string businessid, string billingkey, string content)
        {
            string dataset1 = "Billing_DS";
            string dataset2 = "BillingDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "Billing";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_Billing> datas = new List<Document_Billing>();
                List<Document_BillingDetail> datasdetail = new List<Document_BillingDetail>();
                List<Document_BillingCumulativeDetail> datasCumutivedetail = new List<Document_BillingCumulativeDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();



                Document_Billing responseval = new Document_Billing();
                responseval = api.GetApi<Document_Billing>(string.Format("documentsell/getbillingbykey/{0}/{1}", businessid, billingkey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                string[] header = GetBusinessParameter(businessid, "DocumentBilling").ParameterDescription.Split('|');
                responseval.DocumentNameHeader = header[0];
                responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";


                if (responseval.BillingType.ToUpper() == "CBL")
                {
                    dataset2 = "BillingAccruDetail_DS";
                    reportrdlc = "BillingAll";
                    #region
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "จำนวนรวม",
                        Value = string.Format("{0} รายการ", responseval.BillingCumulativeDetail.Count),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });


                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });


                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ส่วนลดพิเศษ",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),

                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    #endregion
                }
                else
                {
                    #region
                    if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                    {
                        reportrdlc = "Billing";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "hide";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });

                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                    {
                        reportrdlc = "Billing";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "hide";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                    {
                        reportrdlc = "Billing2";
                        responseval.ChkColVatDetail = "show";
                        responseval.ChkColDisCountDetail = "show";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = "ส่วนลดรวม",
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักสว่นลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                            Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "มูลค่าที่คำนวณภาษี",
                            Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 7,
                            Name = "จำนวนรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                    {
                        reportrdlc = "Billing3";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "show";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = "ส่วนลดรวม",
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                    {
                        reportrdlc = "Billing3";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "show";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = "ส่วนลดรวม",
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                    {
                        reportrdlc = "Billing3";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "show";
                    }

                    if (responseval.IsWithholdingTax == "1")
                    {
                        string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                        string withrate = string.Empty;
                        if (Convert.ToInt32(rate[1]) > 0)
                            withrate = responseval.WithholdingRate.Value.ToString("n2");
                        else
                            withrate = responseval.WithholdingRate.Value.ToString("n0");

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 8,

                            Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                            Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                        });
                    }

                    #endregion
                }
                datassummary = datassummary.OrderBy(x => x.Seq).ToList();

                if (content.ToUpper() == "ORIGINAL" || content.ToUpper() == "NORMAL")
                {
                    datas.Add(responseval);
                    datas[datas.Count - 1].Content = "ต้นฉบับ";
                    datas[datas.Count - 1].Page = "1";
                    if (responseval.BillingType.ToUpper() == "CBL")
                    {
                        for (int i = 0; i < responseval.BillingCumulativeDetail.Count; i++)
                        {
                            datasCumutivedetail.Add(responseval.BillingCumulativeDetail[i]);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < responseval.BillingDetail.Count; i++)
                        {
                            datasdetail.Add(responseval.BillingDetail[i]);
                        }
                    }

                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = responseval.BillingType.ToUpper() == "CBL" ? new ReportDataSource(dataset2, datasCumutivedetail) : new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                if (content.ToUpper() == "COPY" || content.ToUpper() == "NORMAL")
                {
                    if (datas.Count == 0)
                    {
                        datas.Add(responseval);
                        datas[datas.Count - 1].Page = "1";
                        if (responseval.BillingType.ToUpper() == "CBL")
                        {
                            for (int i = 0; i < responseval.BillingCumulativeDetail.Count; i++)
                            {
                                datasCumutivedetail.Add(responseval.BillingCumulativeDetail[i]);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < responseval.BillingDetail.Count; i++)
                            {
                                datasdetail.Add(responseval.BillingDetail[i]);
                            }
                        }
                    }
                    else
                    {
                        datas[datas.Count - 1].Page = "2";
                    }
                    datas[datas.Count - 1].Content = "สำเนา";

                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = responseval.BillingType.ToUpper() == "CBL" ? new ReportDataSource(dataset2, datasCumutivedetail) : new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }
                string newfilename = "All" + Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                filename = filename.Replace(Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename)), newfilename);
                pdfmerg.DestinationFile = System.Web.Hosting.HostingEnvironment.MapPath(filename);
                pdfmerg.Execute();

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetInvoice/{businessid}/{invoicekey}/{content}")]
        public ResponseMessage GetInvoice(string businessid, string invoicekey, string content)
        {
            string dataset1 = "Invoice_DS";
            string dataset2 = "InvoiceDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "Invoice";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_Invoice> datas = new List<Document_Invoice>();
                List<Document_InvoiceDetail> datasdetail = new List<Document_InvoiceDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_Invoice responseval = api.GetApi<Document_Invoice>(string.Format("documentsell/getinvoicebykey/{0}/{1}", businessid, invoicekey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                string[] header = GetBusinessParameter(businessid, "DocumentDelivery").ParameterDescription.Split('|');
                responseval.DocumentNameHeader = header[0];
                responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";


                #region
                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                {
                    reportrdlc = "Invoice";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });

                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                {
                    reportrdlc = "Invoice";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "Invoice2";
                    responseval.ChkColVatDetail = "show";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักสว่นลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                {
                    reportrdlc = "Invoice3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "Invoice3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                {
                    reportrdlc = "Invoice3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                }

                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,

                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });
                }



                #endregion
                datassummary = datassummary.OrderBy(x => x.Seq).ToList();

                if (content.ToUpper() == "ORIGINAL" || content.ToUpper() == "NORMAL")
                {
                    datas.Add(responseval);
                    datas[datas.Count - 1].Content = "ต้นฉบับ";
                    datas[datas.Count - 1].Page = "1";


                    for (int i = 0; i < responseval.InvoiceDetail.Count; i++)
                    {
                        datasdetail.Add(responseval.InvoiceDetail[i]);
                    }

                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                if (content.ToUpper() == "COPY" || content.ToUpper() == "NORMAL")
                {
                    if (datas.Count == 0)
                    {
                        datas.Add(responseval);
                        datas[datas.Count - 1].Page = "1";
                        for (int i = 0; i < responseval.InvoiceDetail.Count; i++)
                        {
                            datasdetail.Add(responseval.InvoiceDetail[i]);
                        }
                    }
                    else
                    {
                        datas[datas.Count - 1].Page = "2";
                    }
                    datas[datas.Count - 1].Content = "สำเนา";

                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }
                string newfilename = "All" + Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                filename = filename.Replace(Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename)), newfilename);
                pdfmerg.DestinationFile = System.Web.Hosting.HostingEnvironment.MapPath(filename);
                pdfmerg.Execute();

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetReceipt/{businessid}/{receiptKey}/{content}")]
        public ResponseMessage GetReceipt(string businessid, string receiptKey, string content)
        {
            string dataset1 = "Receipt_DS";
            string dataset2 = "ReceiptDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "Receipt";

            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_Receipt> datas = new List<Document_Receipt>();
                List<Document_ReceiptCumulativeDetail> datascumutivedetail = new List<Document_ReceiptCumulativeDetail>();
                List<Document_ReceiptDetail> datasdetail = new List<Document_ReceiptDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_Receipt responseval = new Document_Receipt();
                responseval = api.GetApi<Document_Receipt>(string.Format("documentsell/getreceiptbykey/{0}/{1}", businessid, receiptKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }
                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";
                responseval.ChkCash = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "CA" ? "1" : "0";
                responseval.ChkCheque = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "CQ" ? "1" : "0";
                responseval.ChkTranfer = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "TR" ? "1" : "0";
                responseval.ChkCreditCard = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "CC" ? "1" : "0";
                if (responseval.ReceiptInfo != null)
                {
                    responseval.PaymentDate = responseval.ReceiptInfo.PaymentDate;
                    responseval.NetPayAmount = responseval.ReceiptInfo.NetPaymentAmount;
                    responseval.Bank = responseval.ReceiptInfo.BankName;
                    if (responseval.ChkCheque == "1")
                        responseval.No = responseval.ReceiptInfo.ChequeNo;
                    else
                        responseval.No = responseval.ReceiptInfo.BankAccountNo;
                }

                if (responseval.ReceiptType == "CRE")
                {
                    reportrdlc = "ReceiptAll";
                    dataset2 = "ReceiptAccruDetail_DS";
                    #region
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมรายการ",
                        Value = string.Format("{0} รายการ", responseval.ReceiptCumulativeDetail.Count),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });


                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    if (responseval.IsAdjustDiscount == "1")
                    {
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = responseval.AdjustDiscountTypeName == null ? "ส่วนลดพิเศษ" : responseval.AdjustDiscountTypeName,
                            Value = responseval.AdjustDiscountAmount == null ? "0.00 บาท" : string.Format("{0} บาท", responseval.AdjustDiscountAmount.Value.ToString("n2")),
                        });

                    }

                   

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    #endregion
                }
                else
                {
                    #region
                    if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                    {
                        reportrdlc = "Receipt";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "hide";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });

                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                    {
                        reportrdlc = "Receipt";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "hide";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                    {
                        reportrdlc = "Receipt2";
                        responseval.ChkColVatDetail = "show";
                        responseval.ChkColDisCountDetail = "show";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = "ส่วนลดรวม",
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักสว่นลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                            Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "มูลค่าที่คำนวณภาษี",
                            Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 7,
                            Name = "จำนวนรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                    {
                        reportrdlc = "Receipt3";

                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "show";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = "ส่วนลดรวม",
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                    {
                        reportrdlc = "Receipt3";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "show";
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 1,
                            Name = "รวมเป็นเงิน",
                            Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 2,
                            Name = "ส่วนลดรวม",
                            Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 3,
                            Name = "ราคาหลังหักส่วนลด",
                            Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 4,
                            Name = "จำนวนเงินรวมทั้งสิ้น",
                            Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 5,
                            Name = "ภาษีมูลค่าเพิ่ม 7%",
                            Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                        });
                        datassummary.Add(new Document_Summary
                        {
                            Seq = 6,
                            Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                            Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                        });
                    }
                    else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                    {
                        reportrdlc = "Receipt3";
                        responseval.ChkColVatDetail = "hide";
                        responseval.ChkColDisCountDetail = "show";
                    }

                    if (responseval.IsWithholdingTax == "1")
                    {
                        string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                        string withrate = string.Empty;
                        if (Convert.ToInt32(rate[1]) > 0)
                            withrate = responseval.WithholdingRate.Value.ToString("n2");
                        else
                            withrate = responseval.WithholdingRate.Value.ToString("n0");

                        datassummary.Add(new Document_Summary
                        {
                            Seq = 8,

                            Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                            Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                        });
                    }
                    #endregion
                }
                datassummary = datassummary.OrderBy(x => x.Seq).ToList();


                if (content.ToUpper() == "ORIGINAL" || content.ToUpper() == "NORMAL")
                {
                    datas.Add(responseval);
                    datas[datas.Count - 1].Content = "ต้นฉบับ";
                    datas[datas.Count - 1].Page = "1";
                    if (responseval.ReceiptType == "CRE")
                    {
                        for (int i = 0; i < responseval.ReceiptCumulativeDetail.Count; i++)
                        {
                            datascumutivedetail.Add(responseval.ReceiptCumulativeDetail[i]);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < responseval.ReceiptDetail.Count; i++)
                        {
                            datasdetail.Add(responseval.ReceiptDetail[i]);
                        }
                    }
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = responseval.ReceiptType == "CRE" ? new ReportDataSource(dataset2, datascumutivedetail) : new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                if (content.ToUpper() == "COPY" || content.ToUpper() == "NORMAL")
                {
                    if (datas.Count == 0)
                    {
                        datas.Add(responseval);
                        datas[datas.Count - 1].Page = "1";
                        if (responseval.ReceiptType == "CRE")
                        {
                            for (int i = 0; i < responseval.ReceiptCumulativeDetail.Count; i++)
                            {
                                datascumutivedetail.Add(responseval.ReceiptCumulativeDetail[i]);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < responseval.ReceiptDetail.Count; i++)
                            {
                                datasdetail.Add(responseval.ReceiptDetail[i]);
                            }
                        }
                    }
                    else
                    {
                        datas[datas.Count - 1].Page = "2";
                    }
                    datas[datas.Count - 1].Content = "สำเนา";
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = responseval.ReceiptType == "CRE" ? new ReportDataSource(dataset2, datascumutivedetail) : new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }
                string newfilename = "All" + Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                filename = filename.Replace(Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename)), newfilename);
                pdfmerg.DestinationFile = System.Web.Hosting.HostingEnvironment.MapPath(filename);
                pdfmerg.Execute();

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetCreditNote/{businessid}/{creditNoteKey}/{content}")]

        public ResponseMessage GetCreditNote(string businessid, string creditNoteKey, string content)
        {
            string dataset1 = "CreditNote_DS";
            string dataset2 = "CreditNoteDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "CreditNote";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_CreditNote> datas = new List<Document_CreditNote>();
                List<Document_CreditNoteDetail> datasdetail = new List<Document_CreditNoteDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_CreditNote responseval = api.GetApi<Document_CreditNote>(string.Format("documentsell/getCreditnotebykey/{0}/{1}", businessid, creditNoteKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                responseval.Remark = "สาเหตุ:" + responseval.CauseTypeName +(responseval.CauseTypeOther==null?"":(":"+responseval.CauseTypeOther))+ System.Environment.NewLine + responseval.Remark;
                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";

                #region
                datassummary.Add(new Document_Summary
                {
                    Seq = 1,
                    Name = "มูลค่าตามเอกสารเดิม",
                    Value = string.Format("{0} บาท", responseval.CreditPriceBefore.Value.ToString("n2")),
                });
                datassummary.Add(new Document_Summary
                {
                    Seq = 2,
                    Name = "มูลค่าที่ถูกต้อง",
                    Value = string.Format("{0} บาท", responseval.CreditPriceReal.Value.ToString("n2")),
                });
                datassummary.Add(new Document_Summary
                {
                    Seq = 3,
                    Name = "ผลต่าง",
                    Value = string.Format("{0} บาท", responseval.CreditPriceAfter.Value.ToString("n2")),
                });

                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                {
                    reportrdlc = "CreditNote";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });

                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                {
                    reportrdlc = "CreditNote";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "CreditNote2";
                    responseval.ChkColVatDetail = "show";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักสว่นลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 10,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                {
                    reportrdlc = "CreditNote3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "CreditNote3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                {
                    reportrdlc = "CreditNote3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                }

                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 10,

                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });
                    //datassummary.Add(new Document_Summary
                    //{
                    //    Seq = 10,
                    //    Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", responseval.WithholdingRate.Value.ToString("n0")),
                    //    Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    //});
                }
                #endregion

                datassummary = datassummary.OrderBy(x => x.Seq).ToList();


                if (content.ToUpper() == "ORIGINAL" || content.ToUpper() == "NORMAL")
                {
                    datas.Add(responseval);
                    datas[datas.Count - 1].Content = "ต้นฉบับ(เอกสารออกเป็นชุด)";
                    datas[datas.Count - 1].Page = "1";
                    for (int i = 0; i < responseval.CreditNoteDetail.Count; i++)
                    {
                        datasdetail.Add(responseval.CreditNoteDetail[i]);
                    }
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                if (content.ToUpper() == "COPY" || content.ToUpper() == "NORMAL")
                {
                    if (datas.Count == 0)
                    {
                        datas.Add(responseval);
                        datas[datas.Count - 1].Page = "1";
                        for (int i = 0; i < responseval.CreditNoteDetail.Count; i++)
                        {
                            datasdetail.Add(responseval.CreditNoteDetail[i]);
                        }
                    }
                    else
                    {
                        datas[datas.Count - 1].Page = "2";
                    }
                    datas[datas.Count - 1].Content = "สำเนา(เอกสารออกเป็นชุด)";
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }
                string newfilename = "All" + Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                filename = filename.Replace(Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename)), newfilename);
                pdfmerg.DestinationFile = System.Web.Hosting.HostingEnvironment.MapPath(filename);
                pdfmerg.Execute();

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetDeditNote/{businessid}/{debitNoteKey}/{content}")]

        public ResponseMessage GetDebitNote(string businessid, string debitNoteKey, string content)
        {
            string dataset1 = "DebitNote_DS";
            string dataset2 = "DebitNoteDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "DebitNote";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_DebitNote> datas = new List<Document_DebitNote>();
                List<Document_DebitNoteDetail> datasdetail = new List<Document_DebitNoteDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_DebitNote responseval = api.GetApi<Document_DebitNote>(string.Format("documentsell/getdebitnotebykey/{0}/{1}", businessid, debitNoteKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }


                responseval.Remark = "สาเหตุ:" + responseval.CauseTypeName + (responseval.CauseTypeOther == null ? "" : (":" + responseval.CauseTypeOther)) + System.Environment.NewLine + responseval.Remark;

                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";

                #region
                datassummary.Add(new Document_Summary
                {
                    Seq = 1,
                    Name = "มูลค่าตามเอกสารเดิม",
                    Value = string.Format("{0} บาท", responseval.DebitPriceBefore.Value.ToString("n2")),
                });
                datassummary.Add(new Document_Summary
                {
                    Seq = 2,
                    Name = "มูลค่าที่ถูกต้อง",
                    Value = string.Format("{0} บาท", responseval.DebitPriceReal.Value.ToString("n2")),
                });
                datassummary.Add(new Document_Summary
                {
                    Seq = 3,
                    Name = "ผลต่าง",
                    Value = string.Format("{0} บาท", responseval.DebitPriceAfter.Value.ToString("n2")),
                });

                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                {
                    reportrdlc = "DebitNote";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });

                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                {
                    reportrdlc = "DebitNote";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "DebitNote2";
                    responseval.ChkColVatDetail = "show";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักสว่นลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 10,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                {
                    reportrdlc = "DebitNote3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "DebitNote3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                {
                    reportrdlc = "DebitNote3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                }

                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 10,
                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });
                    //datassummary.Add(new Document_Summary
                    //{
                    //    Seq = 10,
                    //    Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", responseval.WithholdingRate.Value.ToString("n0")),
                    //    Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    //});
                }
                #endregion

                if (content.ToUpper() == "ORIGINAL" || content.ToUpper() == "NORMAL")
                {
                    datas.Add(responseval);
                    datas[datas.Count - 1].Content = "ต้นฉบับ(เอกสารออกเป็นชุด)";
                    datas[datas.Count - 1].Page = "1";
                    for (int i = 0; i < responseval.DebitNoteDetail.Count; i++)
                    {
                        datasdetail.Add(responseval.DebitNoteDetail[i]);
                    }
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                if (content.ToUpper() == "COPY" || content.ToUpper() == "NORMAL")
                {
                    if (datas.Count == 0)
                    {
                        datas.Add(responseval);
                        datas[datas.Count - 1].Page = "1";
                        for (int i = 0; i < responseval.DebitNoteDetail.Count; i++)
                        {
                            datasdetail.Add(responseval.DebitNoteDetail[i]);
                        }
                    }
                    else
                    {
                        datas[datas.Count - 1].Page = "2";
                    }
                    datas[datas.Count - 1].Content = "สำเนา(เอกสารออกเป็นชุด)";
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                string newfilename = "All" + Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                filename = filename.Replace(Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename)), newfilename);
                pdfmerg.DestinationFile = System.Web.Hosting.HostingEnvironment.MapPath(filename);
                pdfmerg.Execute();

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetPurchaseOrder/{businessid}/{poKey}")]
        public ResponseMessage GetPurchaseOrder(string businessid, string poKey)
        {
            string dataset1 = "PurchaseOrder_DS";
            string dataset2 = "PurchaseOrderDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "PurchaseOrder";

            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_PurchaseOrder> datas = new List<Document_PurchaseOrder>();
                List<Document_PurchaseOrderDetail> datasdetail = new List<Document_PurchaseOrderDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_PurchaseOrder responseval = new Document_PurchaseOrder();
                responseval = api.GetApi<Document_PurchaseOrder>(string.Format("documentbuy/getpobykey/{0}/{1}", businessid, poKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";

                #region
                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                {
                    reportrdlc = "PurchaseOrder";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });

                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                {
                    reportrdlc = "PurchaseOrder";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "PurchaseOrder2";
                    responseval.ChkColVatDetail = "show";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักสว่นลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                {
                    reportrdlc = "PurchaseOrder3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "PurchaseOrder3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                {
                    reportrdlc = "PurchaseOrder3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                }

                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });
                }
                #endregion

                datas.Add(responseval);
                datas[datas.Count - 1].Page = "1";
                for (int i = 0; i < responseval.PurchaseOrderDetails.Count; i++)
                {
                    datasdetail.Add(responseval.PurchaseOrderDetails[i]);
                }
                ReportDataSource[] datasource = new ReportDataSource[3];
                datasource[0] = new ReportDataSource(dataset1, datas);
                datasource[1] = new ReportDataSource(dataset2, datasdetail);
                datasource[2] = new ReportDataSource(dataset3, datassummary);
                GenerateReport report = new GenerateReport(reportrdlc);
                filename = report.PDF(datasource);

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetRI/{businessid}/{riKey}")]
        public ResponseMessage GetRI(string businessid, string riKey)
        {
            string dataset1 = "RI_DS";
            string dataset2 = "RIDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "RI";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_ReceiveInventory> datas = new List<Document_ReceiveInventory>();
                List<Document_ReceiveInventoryDetail> datasdetail = new List<Document_ReceiveInventoryDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_ReceiveInventory responseval = new Document_ReceiveInventory();
                responseval = api.GetApi<Document_ReceiveInventory>(string.Format("documentbuy/getribykey/{0}/{1}", businessid, riKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";

                #region
                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                {
                    reportrdlc = "RI";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });

                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                {
                    reportrdlc = "RI";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "RI2";
                    responseval.ChkColVatDetail = "show";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักสว่นลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                {
                    reportrdlc = "RI3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "RI3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                {
                    reportrdlc = "RI3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                }

                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });
                }
                #endregion

                datas.Add(responseval);
                datas[datas.Count - 1].Page = "1";
                for (int i = 0; i < responseval.ReceiveInventoryDetails.Count; i++)
                {
                    datasdetail.Add(responseval.ReceiveInventoryDetails[i]);
                }
                ReportDataSource[] datasource = new ReportDataSource[3];
                datasource[0] = new ReportDataSource(dataset1, datas);
                datasource[1] = new ReportDataSource(dataset2, datasdetail);
                datasource[2] = new ReportDataSource(dataset3, datassummary);
                GenerateReport report = new GenerateReport(reportrdlc);
                filename = report.PDF(datasource);


                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetCashSale/{businessid}/{cashSaleKey}/{content}")]

        public ResponseMessage GetCashSale(string businessid, string cashSaleKey, string content)
        {
            string dataset1 = "CashSale_DS";
            string dataset2 = "CashSaleDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "CashSale";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_CashSale> datas = new List<Document_CashSale>();
                List<Document_CashSaleDetail> datasdetail = new List<Document_CashSaleDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_CashSale responseval = new Document_CashSale();
                responseval = api.GetApi<Document_CashSale>(string.Format("documentsell/getcashsalebykey/{0}/{1}", businessid, cashSaleKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.CompanyName = bu.BusinessName;
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";
                responseval.ChkCash = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "CA" ? "1" : "0";
                responseval.ChkCheque = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "CQ" ? "1" : "0";
                responseval.ChkTranfer = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "TR" ? "1" : "0";
                responseval.ChkCreditCard = responseval.ReceiptInfo != null && responseval.ReceiptInfo.PaymentType == "CC" ? "1" : "0";
                if (responseval.ReceiptInfo != null)
                {
                    responseval.PaymentDate = responseval.ReceiptInfo.PaymentDate;
                    responseval.NetPayAmount = responseval.ReceiptInfo.NetPaymentAmount;
                    responseval.Bank = responseval.ReceiptInfo.BankName;
                    responseval.WithholdingAmount2 = responseval.ReceiptInfo.WithholdingAmount;
                    if (responseval.ChkCheque == "1")
                        responseval.No = responseval.ReceiptInfo.ChequeNo;
                    else
                    {
                        responseval.No = responseval.ReceiptInfo.BankAccountNo;
                    }
                }
                #region
                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "N")
                {
                    reportrdlc = "CashSale";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });

                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0" && responseval.TaxType == "V")
                {
                    reportrdlc = "CashSale";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "hide";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "CashSale2";
                    responseval.ChkColVatDetail = "show";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักสว่นลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0} บาท", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "จำนวนรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "N")
                {
                    reportrdlc = "CashSale3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && (responseval.TaxType == "V" || responseval.TaxType == "N"))
                {
                    reportrdlc = "CashSale3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0} บาท", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "ราคาหลังหักส่วนลด",
                        Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "จำนวนเงินรวมทั้งสิ้น",
                        Value = string.Format("{0} บาท", responseval.GrandAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ราคาไม่รวมภาษีมูลค่าเพิ่ม",
                        Value = string.Format("{0} บาท", responseval.ExemptAmount.Value.ToString("n2")),
                    });
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1" && responseval.TaxType == "V")
                {
                    reportrdlc = "CashSale3";
                    responseval.ChkColVatDetail = "hide";
                    responseval.ChkColDisCountDetail = "show";
                }

                if (responseval.IsAdjustDiscount == "1")
                {
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 8,
                        Name = responseval.AdjustDiscountTypeName == null ? "ส่วนลดพิเศษ" : responseval.AdjustDiscountTypeName,
                        Value = responseval.AdjustDiscountAmount == null ? "0.00 บาท" : string.Format("{0} บาท", responseval.AdjustDiscountAmount.Value.ToString("n2")),
                    });

                }

               
                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 9,
                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });

                }

                if (responseval.IsWithholdingTax == "1" || responseval.IsAdjustDiscount == "1" || responseval.ReceiptInfo != null)
                {
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 10,
                        Name = "ยอดชำระ",
                        Value = string.Format("{0} บาท", responseval.ReceiptInfo != null ? responseval.NetPayAmount.Value.ToString("n2") : responseval.PaymentAmount.HasValue ? responseval.PaymentAmount.Value.ToString("n2") : "0.00"),
                    });
                }
                #endregion
                datassummary = datassummary.OrderBy(x => x.Seq).ToList();


                if (content.ToUpper() == "ORIGINAL" || content.ToUpper() == "NORMAL")
                {
                    datas.Add(responseval);
                    datas[datas.Count - 1].Content = "ต้นฉบับ";
                    datas[datas.Count - 1].Page = "1";
                    for (int i = 0; i < responseval.CashSaleDetail.Count; i++)
                    {
                        datasdetail.Add(responseval.CashSaleDetail[i]);
                    }
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }

                if (content.ToUpper() == "COPY" || content.ToUpper() == "NORMAL")
                {
                    if (datas.Count == 0)
                    {
                        datas.Add(responseval);
                        datas[datas.Count - 1].Page = "1";
                        for (int i = 0; i < responseval.CashSaleDetail.Count; i++)
                        {
                            datasdetail.Add(responseval.CashSaleDetail[i]);
                        }
                    }
                    else
                    {
                        datas[datas.Count - 1].Page = "2";
                    }
                    datas[datas.Count - 1].Content = "สำเนา";
                    ReportDataSource[] datasource = new ReportDataSource[3];
                    datasource[0] = new ReportDataSource(dataset1, datas);
                    datasource[1] = new ReportDataSource(dataset2, datasdetail);
                    datasource[2] = new ReportDataSource(dataset3, datassummary);
                    GenerateReport report = new GenerateReport(reportrdlc);
                    filename = report.PDF(datasource);
                    pdfmerg.AddFile(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                }
                string newfilename = "All" + Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                filename = filename.Replace(Path.GetFileNameWithoutExtension(System.Web.Hosting.HostingEnvironment.MapPath(filename)), newfilename);
                pdfmerg.DestinationFile = System.Web.Hosting.HostingEnvironment.MapPath(filename);
                pdfmerg.Execute();

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetCashSaleMini/{businessid}/{cashSaleKey}")]

        public ResponseMessage GetCashSaleMini(string businessid, string cashSaleKey)
        {
            string dataset1 = "CashSaleMini_DS";
            string dataset2 = "CashSaleMiniDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "CashSaleMini";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_CashSale> datas = new List<Document_CashSale>();
                List<Document_CashSaleDetail> datasdetail = new List<Document_CashSaleDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_CashSale responseval = new Document_CashSale();
                responseval = api.GetApi<Document_CashSale>(string.Format("documentsell/getcashsalebykey/{0}/{1}", businessid, cashSaleKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                responseval.CompanyName = bu.BusinessName;
                responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;

                responseval.AmountTH = "จำนวนรวม " + responseval.CashSaleDetail.Sum(x => x.Quantity).Value.ToString("n0");
               
                #region
                if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "0")
                {
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0}", responseval.TotalAmount.Value.ToString("n2")),
                    });


                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0}", responseval.VAT.Value.ToString("n2")),
                    });

                    //datassummary.Add(new Document_Summary
                    //{
                    //    Seq = 5,
                    //    Name = "รวมทั้งสิ้น",
                    //    Value = string.Format("{0}", responseval.GrandAmount.Value.ToString("n2")),
                    //});
                    responseval.CreateBy = "รวมทั้งสิ้น";
                    responseval.UpdateBy = string.Format("{0}", responseval.GrandAmount.Value.ToString("n2"));

                }
                else if (responseval.IsTaxHeader == "1" && responseval.IsDiscountHeader == "1")
                {
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0}", responseval.TotalAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลดรวม",
                        Value = string.Format("{0}", responseval.DiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "จำนวนเงินหลังหักสว่นลด",
                        Value = string.Format("{0}", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "มูลค่าที่ไม่มี / ยกเว้นภาษี",
                        Value = string.Format("{0}", responseval.ExemptAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 5,
                        Name = "มูลค่าที่คำนวณภาษี",
                        Value = string.Format("{0}", responseval.VatableAmount.Value.ToString("n2")),
                    });

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0}", responseval.VAT.Value.ToString("n2")),
                    });

                    responseval.CreateBy = "รวมทั้งสิ้น";
                    responseval.UpdateBy = string.Format("{0}", responseval.GrandAmount.Value.ToString("n2"));
                    //datassummary.Add(new Document_Summary
                    //{
                    //    Seq = 7,
                    //    Name = "รวมทั้งสิ้น",
                    //    Value = string.Format("{0}", responseval.GrandAmount.Value.ToString("n2")),
                    //});
                }
                else if (responseval.IsTaxHeader == "0" && responseval.IsDiscountHeader == "1")
                {
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 1,
                        Name = "รวมเป็นเงิน",
                        Value = string.Format("{0}", responseval.TotalBeforeVatAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = "ส่วนลด",
                        Value = string.Format("{0}", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 3,
                        Name = "จำนวนเงินหลังหักส่วนลด",
                        Value = string.Format("{0}", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                    });
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 4,
                        Name = "ภาษีมูลค่าเพิ่ม 7%",
                        Value = string.Format("{0}", responseval.VAT.Value.ToString("n2")),
                    });
                    //datassummary.Add(new Document_Summary
                    //{
                    //    Seq = 5,
                    //    Name = "รวมทั้งสิ้น",
                    //    Value = string.Format("{0}", responseval.GrandAmount.Value.ToString("n2")),
                    //});
                    responseval.CreateBy = "รวมทั้งสิ้น";
                    responseval.UpdateBy = string.Format("{0}", responseval.GrandAmount.Value.ToString("n2"));


                }
                #endregion
                datassummary = datassummary.OrderBy(x => x.Seq).ToList();

              

                datas.Add(responseval);
             
                for (int i = 0; i < responseval.CashSaleDetail.Count; i++)
                {
                    datasdetail.Add(responseval.CashSaleDetail[i]);
                }
                ReportDataSource[] datasource = new ReportDataSource[3];
                datasource[0] = new ReportDataSource(dataset1, datas);
                datasource[1] = new ReportDataSource(dataset2, datasdetail);
                datasource[2] = new ReportDataSource(dataset3, datassummary);
                GenerateReport report = new GenerateReport(reportrdlc);
                filename = report.PDF(datasource);
                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetExpense/{businessid}/{expenseKey}")]
        public ResponseMessage GetExpense(string businessid, string expenseKey)
        {
            string dataset1 = "Expense_DS";
            string dataset2 = "ExpenseDetail_DS";
            string dataset3 = "Summary_DS";
            string reportrdlc = "Expense";
            ResponseMessage result = new ResponseMessage();
            try
            {
                string filename = "";
                PDFMerger pdfmerg = new PDFMerger();
                List<Document_Expenses> datas = new List<Document_Expenses>();
                List<Document_ExpensesDetail> datasdetail = new List<Document_ExpensesDetail>();
                List<Document_Summary> datassummary = new List<Document_Summary>();
                ApiUtilities api = new ApiUtilities();
                Document_Expenses responseval = api.GetApi<Document_Expenses>(string.Format("documentexpenses/getexbykey/{0}/{1}", businessid, expenseKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                responseval.AmountTH = "(" + ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.TotalAmount.Value), "บาท", "สตางค์", "ถ้วน") + ")";
                responseval.CompanyName = bu.BusinessName;
                 responseval.BranchCompanyName = bu.BranchType == "H" ? bu.BranchTypeName : bu.BranchName;
                responseval.ImageBusinessProfile = imgbusiness;
                if (responseval.Signature == "1")
                {
                    responseval.ImageUserProfile = imguser;
                    responseval.ImageRabberBusinessProfile = imgrabberbusiness;
                }

                if (responseval.ReceiveInfo != null)
                {
                    responseval.ChkCash = responseval.ReceiveInfo != null && responseval.ReceiveInfo.PaymentType == "CA" ? "1" : "0";
                    responseval.ChkCheque = responseval.ReceiveInfo != null && responseval.ReceiveInfo.PaymentType == "CQ" ? "1" : "0";
                    responseval.ChkTranfer = responseval.ReceiveInfo != null && responseval.ReceiveInfo.PaymentType == "TR" ? "1" : "0";
                    responseval.ChkCreditCard = responseval.ReceiveInfo != null && responseval.ReceiveInfo.PaymentType == "CC" ? "1" : "0";
                    responseval.PaymentDate = responseval.ReceiveInfo.PaymentDate;
                    responseval.NetPayAmount = responseval.ReceiveInfo.NetPaymentAmount;
                    responseval.WithholdingAmount2 = responseval.ReceiveInfo.WithholdingAmount;
               
                    responseval.Bank = responseval.ReceiveInfo.BankName;
                    responseval.CustomerBank= responseval.ReceiveInfo.CustomerBankName;
                    responseval.CustomerAccount = responseval.ReceiveInfo.CustomerAccountNo;

                    if (responseval.ChkCheque == "1")
                        responseval.No = responseval.ReceiveInfo.ChequeNo;
                    else
                    {
                        responseval.No = responseval.ReceiveInfo.BankAccountNo;
                    }
                }
                #region

                datassummary.Add(new Document_Summary
                {
                    Seq = 1,
                    Name = "รวมเป็นเงิน",
                    Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                });

                if (responseval.IsDiscountHeader == "1")
                {
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 2,
                        Name = string.Format("ส่วนลด {0}%", responseval.DiscountPercent),
                        Value = string.Format("{0} บาท", responseval.DiscountAmount.Value.ToString("n2")),
                    });
                }

                datassummary.Add(new Document_Summary
                {
                    Seq = 3,
                    Name = "ราคาหลังหักส่วนลด",
                    Value = string.Format("{0} บาท", responseval.TotalAfterDiscountAmount.Value.ToString("n2")),
                });

                datassummary.Add(new Document_Summary
                {
                    Seq = 4,
                    Name = "ภาษีมูลค่าเพิ่ม 7%",
                    Value = string.Format("{0} บาท", responseval.VAT.Value.ToString("n2")),
                });

                datassummary.Add(new Document_Summary
                {
                    Seq = 5,
                    Name = "จำนวนรวมทั้งสิ้น",
                    Value = string.Format("{0} บาท", responseval.TotalAmount.Value.ToString("n2")),
                });

                if (responseval.IsWithholdingTax == "1")
                {
                    string[] rate = responseval.WithholdingRate.Value.ToString("n2").Split('.');
                    string withrate = string.Empty;
                    if (Convert.ToInt32(rate[1]) > 0)
                        withrate = responseval.WithholdingRate.Value.ToString("n2");
                    else
                        withrate = responseval.WithholdingRate.Value.ToString("n0");

                    datassummary.Add(new Document_Summary
                    {
                        Seq = 6,
                        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", withrate),
                        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                    });

                }

                //if (responseval.WithholdingAmount.HasValue)
                //{
                //    datassummary.Add(new Document_Summary
                //    {
                //        Seq = 6,
                //        Name = string.Format("หักภาษี ณ ที่จ่าย {0}%", responseval.WithholdingRate.Value.ToString("n0")),
                //        Value = string.Format("{0} บาท", responseval.WithholdingAmount.Value.ToString("n2")),
                //    });
                //}

                if (responseval.IsWithholdingTax == "1" ||  responseval.ReceiveInfo != null)
                {
                    datassummary.Add(new Document_Summary
                    {
                        Seq = 7,
                        Name = "ยอดชำระ",
                        Value = string.Format("{0} บาท", responseval.ReceiveInfo != null ? responseval.NetPayAmount.Value.ToString("n2") : responseval.GrandAmountAfterWithholding.HasValue ? responseval.GrandAmountAfterWithholding.Value.ToString("n2") : "0.00"),
                    });
                }

                //datassummary.Add(new Document_Summary
                //{
                //    Seq = 7,
                //    Name = "ยอดชำระ",
                //    Value = string.Format("{0} บาท", responseval.NetPayAmount.Value.ToString("n2")),
                //});
                datassummary = datassummary.OrderBy(x => x.Seq).ToList();
                #endregion


                datas.Add(responseval);
                datas[datas.Count - 1].Page = "1";
                for (int i = 0; i < responseval.ExpensesDetails.Count; i++)
                {
                    datasdetail.Add(responseval.ExpensesDetails[i]);
                }
                ReportDataSource[] datasource = new ReportDataSource[3];
                datasource[0] = new ReportDataSource(dataset1, datas);
                datasource[1] = new ReportDataSource(dataset2, datasdetail);
                datasource[2] = new ReportDataSource(dataset3, datassummary);
                GenerateReport report = new GenerateReport(reportrdlc);
                filename = report.PDF(datasource);

                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;

            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        [HttpGet]
        [Route("GetWithholdingTax/{businessid}/{withholdingKey}")]
        public ResponseMessage GetWithholdingTax(string businessid, string withholdingKey)
        {
            ResponseMessage result = new ResponseMessage();

            try
            {
                PDFMerger pdfmerg = new PDFMerger();
                ApiUtilities api = new ApiUtilities();
                Document_WithholdingTax responseval = api.GetApi<Document_WithholdingTax>(string.Format("documentexpenses/getwtbykey/{0}/{1}", businessid, withholdingKey));
                BusinessProfile bu = GetBusinessProfile(businessid);
                UserProfile user = GetUserProfile(responseval.SaleID.Value.ToString());
                byte[] imgbusiness = GetFileImageBusinessProfile(bu.ThumnailLogo);
                byte[] imgrabberbusiness = GetFileImageBusinessProfile(bu.RabberStampLogo);
                byte[] imguser = GetFileImageUserProfile(user.ThumbnailSignature);
                string sourcefilename = string.Format("~\\Template\\{0}.pdf", "Tawi50");
                string filename = string.Format("~\\Temp\\{0}.pdf", DateTime.Now.ToString("ddMMyyyyHHmmssfff"));

                iTextSharpBinding itxt = new iTextSharpBinding(System.Web.Hosting.HostingEnvironment.MapPath(sourcefilename), System.Web.Hosting.HostingEnvironment.MapPath(filename));
                itxt.SetField("txtBuName", bu.BusinessName);

                if (imgrabberbusiness != null)
                    itxt.SetImage("txtLogo", imgrabberbusiness,51);

                if (imguser != null)
                    itxt.SetImage("txtSignature", imguser,null);

                itxt.SetField("txtBuAddr", bu.Address);

                if (!string.IsNullOrEmpty(bu.TaxID) && bu.TaxID.Length >= 13)
                {
                    string id1 = string.Format("{0} {1}{2}{3}{4} {5}{6}{7}{8}{9} {10}{11} {12}", bu.TaxID.Substring(0, 1)
                    , bu.TaxID.Substring(1, 1)
                    , bu.TaxID.Substring(2, 1)
                    , bu.TaxID.Substring(3, 1)
                    , bu.TaxID.Substring(4, 1)
                    , bu.TaxID.Substring(5, 1)
                    , bu.TaxID.Substring(6, 1)
                    , bu.TaxID.Substring(7, 1)
                    , bu.TaxID.Substring(8, 1)
                    , bu.TaxID.Substring(9, 1)
                    , bu.TaxID.Substring(10, 1)
                    , bu.TaxID.Substring(11, 1)
                    , bu.TaxID.Substring(12, 1)
                    );
                    itxt.SetField("txtid1", id1);
                }

                if (!string.IsNullOrEmpty(responseval.CustomerTaxID) && responseval.CustomerTaxID.Length >= 13)
                {
                    string id2 = string.Format("{0} {1}{2}{3}{4} {5}{6}{7}{8}{9} {10}{11} {12}", responseval.CustomerTaxID.Substring(0, 1)
                      , responseval.CustomerTaxID.Substring(1, 1)
                      , responseval.CustomerTaxID.Substring(2, 1)
                      , responseval.CustomerTaxID.Substring(3, 1)
                      , responseval.CustomerTaxID.Substring(4, 1)
                      , responseval.CustomerTaxID.Substring(5, 1)
                      , responseval.CustomerTaxID.Substring(6, 1)
                      , responseval.CustomerTaxID.Substring(7, 1)
                      , responseval.CustomerTaxID.Substring(8, 1)
                      , responseval.CustomerTaxID.Substring(9, 1)
                      , responseval.CustomerTaxID.Substring(10, 1)
                      , responseval.CustomerTaxID.Substring(11, 1)
                      , responseval.CustomerTaxID.Substring(12, 1)
                      );
                    itxt.SetField("txtid2", id2);

                }

                itxt.SetField("run_no", responseval.WithholdingTaxNo);
                itxt.SetField("txtCustomerName", responseval.CustomerContactName);
                itxt.SetField("txtCustomerAddr", responseval.CustomerAddress);


                switch (responseval.WithholdingTaxForm)
                {
                    case "1": itxt.SetField("chk4", "Yes",true); break;
                    case "2": itxt.SetField("chk7", "Yes",true); break;
                    case "3": itxt.SetField("chk1", "Yes",true); break;
                    case "4": itxt.SetField("chk2", "Yes", true); break;
                    case "5": itxt.SetField("chk3", "Yes", true); break;
                    case "6": itxt.SetField("chk5", "Yes", true); break;
                    case "7": itxt.SetField("chk6", "Yes", true); break;
                }


                for (int i = 0; i < responseval.WithholdingTaxDetails.Count; i++)
                {
                    switch (responseval.WithholdingTaxDetails[i].WithholdingTaxCatagory)
                    {
                        case "100":
                            itxt.SetField("pay1.0", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.0", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date1", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");
                            break;
                        case "200":
                            itxt.SetField("pay1.1", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.1", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date2", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");
                            break;
                        case "300":
                            itxt.SetField("pay1.2", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.2", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date3", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");


                            break;
                        case "400":
                            itxt.SetField("pay1.3", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.3", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date4", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "411":
                            itxt.SetField("pay1.5", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.5", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date6", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "412":
                            itxt.SetField("pay1.6", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.6", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date7", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");
                            break;
                        case "413":

                            itxt.SetField("txtDescription414", responseval.WithholdingTaxDetails[i].Description==null ?  "":responseval.WithholdingTaxDetails[i].Description);
                            itxt.SetField("pay1.7", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.7", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date8", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "421":
                            itxt.SetField("pay1.8", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.8", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date9", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "422":
                            itxt.SetField("pay1.9", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.9", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date10", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "423":
                            itxt.SetField("pay1.10", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.10", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date11", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "424":
                            itxt.SetField("pay1.11", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.11", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date12", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "425":
                            itxt.SetField("txtDescription1.12", responseval.WithholdingTaxDetails[i].Description);
                            itxt.SetField("pay1.12", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.12", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date13", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "500":
                            itxt.SetField("pay1.13.0", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.13.0", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date14.0", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;
                        case "600":
                            itxt.SetField("txtDescription1.13.1", responseval.WithholdingTaxDetails[i].Description);
                            itxt.SetField("pay1.13.1", responseval.WithholdingTaxDetails[i].PriceAfterTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceAfterTax.Value.ToString("n2") : "");
                            itxt.SetField("tax1.13.1", responseval.WithholdingTaxDetails[i].PriceTax.HasValue ? responseval.WithholdingTaxDetails[i].PriceTax.Value.ToString("n2") : "");
                            itxt.SetField("date14.1", responseval.WithholdingTaxDate.HasValue ? responseval.WithholdingTaxDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) : "");

                            break;

                    }
                }

                itxt.SetField("pay1.14", responseval.ExemptAmount.HasValue ? responseval.ExemptAmount.Value.ToString("n2") : "");
                itxt.SetField("tax1.14", responseval.VatableAmount.HasValue ? responseval.WithholdingAmount.Value.ToString("n2") : "");
                itxt.SetField("txtTotal", responseval.TotalAmount.HasValue ? ThaiBathText.NumberToTextThai(Convert.ToDouble(responseval.WithholdingAmount.Value), "บาท", "สตางค์", "ถ้วน") : "");

                switch (responseval.PayerMethod)
                {

                    case "1": itxt.SetField("chkPayer1", "Yes",true); break;
                    case "2": itxt.SetField("chkPayer1", "Yes",true); break;
                    case "3": itxt.SetField("chkPayer1", "Yes",true); break;
                    case "4":
                        itxt.SetField("chkPayer4", "Yes",true);
                        itxt.SetField("txtPayerDesc", responseval.PayerDesc);
                        break;
                }
                itxt.SetField("txtSSOFund", responseval.SSOFundPrice.ToString("n2"));
                itxt.SetField("txtSSO", responseval.SSOPrice.ToString("n2"));
                itxt.Dispose();
                string filebase = ConvertFileToBase64(System.Web.Hosting.HostingEnvironment.MapPath(filename));
                result.responsecode = ((int)Common.Abstract.ResponseCode.SuccessTransaction).ToString();
                result.reponsedata = filebase;
            }
            catch (Exception ex)
            {
                result.responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString();
                result.reponsedata = ex.Message;
            }
            return result;
        }

        protected string ConvertFileToBase64(string urlPath)
        {
            byte[] imageBytes = System.IO.File.ReadAllBytes(urlPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }

}
