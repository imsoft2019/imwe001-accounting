﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Excel
{
    public class RptReceipt : IMauchly.Excel.ClsCreateExcel
    {
        public BusinessProfile business { get; set; }
        public List<DocumentReport> list { get; set; }
        public string PeriodDate { get; set; }
        public override void Writes(string PathFile, string NameSheet)
        {
            base.Writes(PathFile, NameSheet);

            int i = 1;

            XlsMergeCell("A1", "T1");
            XlsMergeCell("B2", "D2");
            XlsMergeCell("B3", "D3");
            XlsMergeCell("B4", "D4");
            XlsStyleFontBold("A1", "A4");
            XlsGenFromCustom("A1", "รายงานการเก็บเงิน");
            XlsStyleAligntCenter("A1", "A1");
            XlsGenFromCustom("A2", "ชื่อบริษัท");
            XlsGenFromCustom("A3", "เลขผู้เสียภาษี");
            XlsGenFromCustom("A4", "ช่วงเวลา");
            XlsGenFromCustom("B2", business.BusinessName);
            XlsGenFromCustom("B3", business.TaxID);
            XlsGenFromCustom("B4", PeriodDate);

            XlsStyleAligntCenter("A5", "T5");
            XlsStyleBackground("A5", "P5", System.Drawing.Color.Yellow);
            XlsGenFromCustom("A5", "เลขที่เอกสาร");
            XlsStyleBorderAll("A5", "A5", System.Drawing.Color.Black);
            XlsGenFromCustom("B5", "วัน/เดือน/ปี");
            XlsStyleBorderAll("B5", "B5", System.Drawing.Color.Black);
            XlsGenFromCustom("C5", "ชื่อลูกค้า");
            XlsStyleBorderAll("C5", "C5", System.Drawing.Color.Black);
            XlsGenFromCustom("D5", "ชื่อโปรเจ็ค");
            XlsStyleBorderAll("D5", "D5", System.Drawing.Color.Black);
            XlsGenFromCustom("E5", "วันที่รับเงิน");
            XlsStyleBorderAll("E5", "E5", System.Drawing.Color.Black);
            XlsGenFromCustom("F5", "ช่องทาง");
            XlsStyleBorderAll("F5", "F5", System.Drawing.Color.Black);
            XlsGenFromCustom("G5", "รายละเอียดการรับชำระ");
            XlsStyleBorderAll("G5", "G5", System.Drawing.Color.Black);
            XlsGenFromCustom("H5", "มูลค่า");
            XlsStyleBorderAll("H5", "H5", System.Drawing.Color.Black);
            XlsGenFromCustom("I5", "ภาษีมูลค่าเพิ่ม");
            XlsStyleBorderAll("I5", "I5", System.Drawing.Color.Black);
            XlsGenFromCustom("J5", "ยอดรวมสุทธิ");
            XlsStyleBorderAll("J5", "J5", System.Drawing.Color.Black);
            XlsGenFromCustom("K5", "รายการปรับลด");
            XlsStyleBorderAll("K5", "K5", System.Drawing.Color.Black);
            XlsGenFromCustom("l5", "จำนวนปรับลด");
            XlsStyleBorderAll("L5", "L5", System.Drawing.Color.Black);
            XlsGenFromCustom("M5", "อัตราภาษี");
            XlsStyleBorderAll("M5", "M5", System.Drawing.Color.Black);
            XlsGenFromCustom("N5", "หัก ณ ที่จ่าย");
            XlsStyleBorderAll("N5", "N5", System.Drawing.Color.Black);
            XlsGenFromCustom("O5", "ผลต่าง");
            XlsStyleBorderAll("O5", "O5", System.Drawing.Color.Black);
            XlsGenFromCustom("P5", "เหตุผล");
            XlsStyleBorderAll("P5", "P5", System.Drawing.Color.Black);
            XlsGenFromCustom("Q5", "ยอดชำระ");
            XlsStyleBorderAll("Q5", "Q5", System.Drawing.Color.Black);
            XlsGenFromCustom("R5", "สถานะ");
            XlsStyleBorderAll("R5", "R5", System.Drawing.Color.Black);
            XlsGenFromCustom("S5", "พนักงานขาย");
            XlsStyleBorderAll("S5", "S5", System.Drawing.Color.Black);
            XlsGenFromCustom("T5", "หมายเหตุ");
            XlsStyleBorderAll("T5", "T5", System.Drawing.Color.Black);

            int row = 6;
            decimal total = 0, vat = 0, grandtotal = 0,discount = 0,tax =0 , balance =0 , payment = 0;

            foreach (DocumentReport val in list)
            {

                if (val.DocumentType == "CN")
                {
                    val.Total = val.Total * (-1);
                    val.VAT = val.VAT * (-1);
                    val.GrandAmount = val.GrandAmount * (-1);
                }

                XlsGenFromCustom("A" + row, val.DocumentNo);
                XlsStyleBorderAll("A" + row, "A" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("B" + row, val.DocumentDate);
                XlsStyleBorderAll("B" + row, "B" + row, System.Drawing.Color.Black);
                XlsStyleFormat("B" + row, "B" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                XlsGenFromCustom("C" + row, val.CustomerName);
                XlsStyleBorderAll("C" + row, "C" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("D" + row, val.ProjectName);
                XlsStyleBorderAll("D" + row, "D" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("E" + row, val.PaymentDate);
                XlsStyleBorderAll("E" + row, "E" + row, System.Drawing.Color.Black);
                XlsStyleFormat("E" + row, "E" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                XlsGenFromCustom("F" + row, val.PaymentType);
                XlsStyleBorderAll("F" + row, "F" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("G" + row, val.PaymentDescription);
                XlsStyleBorderAll("G" + row, "G" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("H" + row, val.Total);
                XlsStyleBorderAll("H" + row, "H" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("I" + row, val.VAT);
                XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("J" + row, val.GrandAmount);
                XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("K" + row, val.PaymentDiscountType);
                XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("l" + row, val.PaymentDiscount);
                XlsStyleBorderAll("L" + row, "L" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("M" + row, val.TaxRate);
                XlsStyleBorderAll("M" + row, "M" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("N" + row, val.TaxAmount);
                XlsStyleBorderAll("N" + row, "N" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("O" + row, val.PaymentBalance);
                XlsStyleBorderAll("O" + row, "O" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("P" + row, val.CauseType);
                XlsStyleBorderAll("P" + row, "P" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("Q" + row, val.PaymentAmount);
                XlsStyleBorderAll("Q" + row, "Q" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("R" + row, val.DocumentStatusName);
                XlsStyleBorderAll("R" + row, "R" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("S" + row, val.SaleName);
                XlsStyleBorderAll("S" + row, "S" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("T" + row, val.Remark);
                XlsStyleBorderAll("T" + row, "T" + row, System.Drawing.Color.Black);


                if (val.DocumentStatus != "3")
                {
                    total += val.Total.Value;
                    vat += val.VAT.Value;
                    grandtotal += val.GrandAmount.Value;
                    discount += val.PaymentDiscount.HasValue ? val.PaymentDiscount.Value : 0;
                    tax += val.TaxAmount.HasValue ? val.TaxAmount.Value : 0;
                    balance += val.PaymentBalance.HasValue ? val.PaymentBalance.Value : 0;
                    payment += val.PaymentAmount.HasValue ? val.PaymentAmount.Value : 0;

                }
                else
                    ObjWorkSheet.Cells[row, 1, row, 20].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                i++;
                row++;
            }

            XlsStyleFontBold("A5", "T5");
            XlsGenFromCustom("G" + row, "ยอดรวมทั้งหมด");
            XlsGenFromCustom("H" + row, total);
            XlsGenFromCustom("I" + row, vat);
            XlsGenFromCustom("J" + row, grandtotal);
            XlsGenFromCustom("J" + row, discount);
            XlsGenFromCustom("J" + row, tax);
            XlsGenFromCustom("J" + row, balance);
            XlsGenFromCustom("J" + row, payment);
            XlsStyleBorderBottom("A" + row, "T" + row);
            XlsStyleFormat("H6", "J" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");
            XlsStyleFormat("L6", "Q" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");
            XlsStyleAutoFitColumns();

            base.SaveExcelFileNotMsg();
        }
    }
}