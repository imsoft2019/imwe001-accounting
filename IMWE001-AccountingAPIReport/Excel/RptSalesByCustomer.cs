﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Excel
{
    public class RptSalesByCustomer : IMauchly.Excel.ClsCreateExcel
    {
        public BusinessProfile business { get; set; }
        public List<DocumentReport> list { get; set; }
        public string PeriodDate { get; set; }
        public override void Writes(string PathFile, string NameSheet)
        {
            base.Writes(PathFile, NameSheet);

            int i = 1;

            XlsMergeCell("A1", "H1");
            XlsMergeCell("B2", "D2");
            XlsMergeCell("B3", "D3");
            XlsMergeCell("B4", "D4");
            XlsStyleFontBold("A1", "A4");
            XlsGenFromCustom("A1", "รายงานยอดขายตามลูกค้า");
            XlsStyleAligntCenter("A1", "A1");
            XlsGenFromCustom("A2", "ชื่อบริษัท");
            XlsGenFromCustom("A3", "เลขผู้เสียภาษี");
            XlsGenFromCustom("A4", "ช่วงเวลา");
            XlsGenFromCustom("B2", business.BusinessName);
            XlsGenFromCustom("B3", business.TaxID);
            XlsGenFromCustom("B4", PeriodDate);

            XlsStyleAligntCenter("A5", "H5");
            XlsStyleBackground("A5", "H5", System.Drawing.Color.Yellow);
            XlsGenFromCustom("A5", "รหัสลูกค้า");
            XlsStyleBorderAll("A5", "A5", System.Drawing.Color.Black);
            XlsGenFromCustom("B5", "เลขที่เอกสาร");
            XlsStyleBorderAll("B5", "B5", System.Drawing.Color.Black);
            XlsGenFromCustom("C5", "วัน/เดือน/ปี");
            XlsStyleBorderAll("C5", "C5", System.Drawing.Color.Black);
            XlsGenFromCustom("D5", "ครบกำหนด");
            XlsStyleBorderAll("D5", "D5", System.Drawing.Color.Black);
            XlsGenFromCustom("E5", "มูลค่า");
            XlsStyleBorderAll("E5", "E5", System.Drawing.Color.Black);
            XlsGenFromCustom("F5", "ภาษีมูลค่าเพิ่ม");
            XlsStyleBorderAll("F5", "F5", System.Drawing.Color.Black);
            XlsGenFromCustom("G5", "ยอดรวมสุทธิ");
            XlsStyleBorderAll("G5", "G5", System.Drawing.Color.Black);
            XlsGenFromCustom("H5", "สถานะ");
            XlsStyleBorderAll("H5", "H5", System.Drawing.Color.Black);

            var customer = list.GroupBy(a => new { a.CustomerKey}).Select(g => new { g.Key.CustomerKey});

            int row = 6;

            foreach (var cus in customer.OrderBy(c => c.CustomerKey))
            {
                decimal total = 0, vat = 0, grandtotal = 0;
                List<DocumentReport> filter = list.Where(a => a.CustomerKey == cus.CustomerKey).ToList();
                int fixcus = row;
                XlsStyleBorderAll("A" + row, "H" + row, System.Drawing.Color.Black);
                
                row++;

                foreach (DocumentReport val in filter)
                {
                    if (!string.IsNullOrEmpty(val.CustomerCode))
                    XlsGenFromCustom("A" + fixcus, val.CustomerCode);
                    if (!string.IsNullOrEmpty(val.CustomerName))
                        XlsGenFromCustom("B" + fixcus, val.CustomerName);

                    XlsGenFromCustom("B" + row, val.DocumentNo);
                    XlsGenFromCustom("C" + row, val.DocumentDate);
                    XlsGenFromCustom("D" + row, val.DueDate);
                    XlsStyleFormat("C" + row, "D" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                    XlsGenFromCustom("E" + row, val.Total);
                    XlsGenFromCustom("F" + row, val.VAT);
                    XlsGenFromCustom("G" + row, val.GrandAmount);
                    XlsGenFromCustom("H" + row, val.DocumentStatusName);
                    XlsStyleBorderRight("H" + row , "H" + row);

                    if (val.DocumentStatus != "3")
                    {
                        total += val.Total.Value;
                        vat += val.VAT.Value;
                        grandtotal += val.GrandAmount.Value;
                    }
                    else
                        ObjWorkSheet.Cells[row, 1, row, 8].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                    row++;
                }

                XlsStyleBorderBottom("D" + (row - 1), "H" + (row - 1));
                XlsStyleBorderBottom("D" + (row), "H" + (row));
                XlsStyleFontBold("C" + (row), "H" + (row));
                XlsGenFromCustom("C" + row, "ยอดรวม");
                XlsGenFromCustom("E" + row, total);
                XlsGenFromCustom("F" + row, vat);
                XlsGenFromCustom("G" + row, grandtotal);
                XlsStyleBorderRight("H" + row, "H" + row);
                row++; row++;
            }

            XlsStyleFormat("E6", "G" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");


            XlsStyleAutoFitColumns();

            base.SaveExcelFileNotMsg();
        }
    }
}