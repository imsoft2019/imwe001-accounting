﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Excel
{
    public class RptAccountPayables : IMauchly.Excel.ClsCreateExcel
    {
        public BusinessProfile business { get; set; }
        public List<DocumentReport> list { get; set; }
        public string PeriodDate { get; set; }
        public override void Writes(string PathFile, string NameSheet)
        {
            base.Writes(PathFile, NameSheet);

            int i = 1;

            XlsMergeCell("A1", "J1");
            XlsMergeCell("B2", "D2");
            XlsMergeCell("B3", "D3");
            XlsMergeCell("B4", "D4");
            XlsStyleFontBold("A1", "A4");
            XlsGenFromCustom("A1", "รายงานเจ้าหนี้ตามเอกสาร");
            XlsStyleAligntCenter("A1", "A1");
            XlsGenFromCustom("A2", "ชื่อบริษัท");
            XlsGenFromCustom("A3", "เลขผู้เสียภาษี");
            XlsGenFromCustom("A4", "ช่วงเวลา");
            XlsGenFromCustom("B2", business.BusinessName);
            XlsGenFromCustom("B3", business.TaxID);
            XlsGenFromCustom("B4", PeriodDate);

            XlsStyleAligntCenter("A5", "J5");
            XlsStyleBackground("A5", "J5", System.Drawing.Color.Yellow);
            XlsGenFromCustom("A5", "เลขที่เอกสาร");
            XlsStyleBorderAll("A5", "A5", System.Drawing.Color.Black);
            XlsGenFromCustom("B5", "วัน/เดือน/ปี");
            XlsStyleBorderAll("B5", "B5", System.Drawing.Color.Black);
            XlsGenFromCustom("C5", "ครบกำหนด");
            XlsStyleBorderAll("C5", "C5", System.Drawing.Color.Black);
            XlsGenFromCustom("D5", "ชื่อผู้จำหน่าย");
            XlsStyleBorderAll("D5", "D5", System.Drawing.Color.Black);
            XlsGenFromCustom("E5", "ชื่อโปรเจ็ค");
            XlsStyleBorderAll("E5", "E5", System.Drawing.Color.Black);
            XlsGenFromCustom("F5", "ยอดรวมตามเอกสาร");
            XlsStyleBorderAll("F5", "F5", System.Drawing.Color.Black);
            XlsGenFromCustom("G5", "ยอดค้างชำระ");
            XlsStyleBorderAll("G5", "G5", System.Drawing.Color.Black);
            XlsGenFromCustom("H5", "สถานะ");
            XlsStyleBorderAll("H5", "H5", System.Drawing.Color.Black);
            XlsGenFromCustom("I5", "พนักงานขาย");
            XlsStyleBorderAll("I5", "I5", System.Drawing.Color.Black);
            XlsGenFromCustom("J5", "หมายเหตุ");
            XlsStyleBorderAll("J5", "J5", System.Drawing.Color.Black);

            var customer = list.GroupBy(a => new { a.CustomerKey}).Select(g => new { g.Key.CustomerKey});

            int row = 6;

            foreach (var cus in customer.OrderBy(c => c.CustomerKey))
            {
                decimal total = 0, vat = 0, grandtotal = 0;
                List<DocumentReport> filter = list.Where(a => a.CustomerKey == cus.CustomerKey).ToList();
                XlsStyleBorderBottom("A" + (row-1), "J" + (row-1));

                foreach (DocumentReport val in filter)
                {
                    XlsGenFromCustom("A" + row, val.DocumentNo);
                    XlsGenFromCustom("B" + row, val.DocumentDate);
                    XlsGenFromCustom("C" + row, val.DueDate);
                    XlsStyleFormat("B" + row, "C" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                    XlsGenFromCustom("D" + row, val.CustomerName);
                    XlsGenFromCustom("E" + row, val.ProjectName);
                    XlsGenFromCustom("F" + row, val.GrandAmount);
                    XlsGenFromCustom("G" + row, val.GrandAmount);
                    XlsGenFromCustom("H" + row, val.DocumentStatusName);
                    XlsGenFromCustom("I" + row, val.SaleName);
                    XlsGenFromCustom("J" + row, val.Remark);
                    XlsStyleBorderRight("A" + row , "J" + row);

                    if (val.DocumentStatus != "3")
                    {
                        total += val.GrandAmount.Value;
                    }
                    else
                        ObjWorkSheet.Cells[row, 1, row, 10].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                    row++;
                }

                XlsStyleBorderBottom("A" + (row-1), "J" + (row-1));
                XlsStyleBorderBottom("A" + (row), "J" + (row));
                XlsStyleFontBold("E" + (row), "G" + (row));
                XlsGenFromCustom("E" + row, "ยอดรวม");
                XlsGenFromCustom("F" + row, total);
                XlsGenFromCustom("G" + row, total);
                XlsStyleBorderRight("J" + row, "J" + row);
                row++; row++;
            }

            XlsStyleFormat("E6", "G" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");


            XlsStyleAutoFitColumns();

            base.SaveExcelFileNotMsg();
        }
    }
}