﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Excel
{
    public class RptExpense : IMauchly.Excel.ClsCreateExcel
    {
        public BusinessProfile business { get; set; }
        public List<DocumentReport> list { get; set; }
        public string PeriodDate { get; set; }
        public override void Writes(string PathFile, string NameSheet)
        {
            base.Writes(PathFile, NameSheet);

            int i = 1;

            XlsMergeCell("A1", "O1");
            XlsMergeCell("B2", "D2");
            XlsMergeCell("B3", "D3");
            XlsMergeCell("B4", "D4");
            XlsStyleFontBold("A1", "A4");
            XlsGenFromCustom("A1", "รายงานค่าใช้จ่าย");
            XlsStyleAligntCenter("A1", "A1");
            XlsGenFromCustom("A2", "ชื่อบริษัท");
            XlsGenFromCustom("A3", "เลขผู้เสียภาษี");
            XlsGenFromCustom("A4", "ช่วงเวลา");
            XlsGenFromCustom("B2", business.BusinessName);
            XlsGenFromCustom("B3", business.TaxID);
            XlsGenFromCustom("B4", PeriodDate);

            XlsStyleAligntCenter("A5", "O5");
            XlsStyleBackground("A5", "O5", System.Drawing.Color.Yellow);
            XlsGenFromCustom("A5", "วัน/เดือน/ปี");
            XlsStyleBorderAll("A5", "A5", System.Drawing.Color.Black);
            XlsGenFromCustom("B5", "เลขที่เอกสาร");
            XlsStyleBorderAll("B5", "B5", System.Drawing.Color.Black);
            XlsGenFromCustom("C5", "เลขผู้เสียภาษี");
            XlsStyleBorderAll("C5", "C5", System.Drawing.Color.Black);
            XlsGenFromCustom("D5", "ชื่อผู้จำหน่าย");
            XlsStyleBorderAll("D5", "D5", System.Drawing.Color.Black);
            XlsGenFromCustom("E5", "ชื่อโปรเจ็ค");
            XlsStyleBorderAll("E5", "E5", System.Drawing.Color.Black);
            XlsGenFromCustom("F5", "สำนักงานใหญ่/สาขา");
            XlsStyleBorderAll("F5", "F5", System.Drawing.Color.Black);
            XlsGenFromCustom("G5", "มูลค่า");
            XlsStyleBorderAll("G5", "G5", System.Drawing.Color.Black);
            XlsGenFromCustom("H5", "ภาษีมูลค่าเพิ่ม");
            XlsStyleBorderAll("H5", "H5", System.Drawing.Color.Black);
            XlsGenFromCustom("I5", "ยอดรวมสุทธิ");
            XlsStyleBorderAll("I5", "I5", System.Drawing.Color.Black);
            XlsGenFromCustom("J5", "ประเภทค่าใช้จ่าย");
            XlsStyleBorderAll("J5", "J5", System.Drawing.Color.Black);
            XlsGenFromCustom("K5", "เอกสารอ้างอิงในระบบ");
            XlsStyleBorderAll("K5", "K5", System.Drawing.Color.Black);
            XlsGenFromCustom("l5", "เลขที่อ้างอิง");
            XlsStyleBorderAll("L5", "L5", System.Drawing.Color.Black);
            XlsGenFromCustom("M5", "สถานะ");
            XlsStyleBorderAll("M5", "M5", System.Drawing.Color.Black);
            XlsGenFromCustom("N5", "หมายเหตุ");
            XlsStyleBorderAll("N5", "N5", System.Drawing.Color.Black);
            XlsGenFromCustom("O5", "โน้ต (สำหรับใช้ภายในบริษัท)");
            XlsStyleBorderAll("O5", "O5", System.Drawing.Color.Black);

            int row = 6;
            decimal total = 0, vat = 0, grandtotal = 0;

            foreach (DocumentReport val in list)
            {

                XlsGenFromCustom("A" + row, val.DocumentDate);
                XlsStyleBorderAll("A" + row, "A" + row, System.Drawing.Color.Black);
                XlsStyleFormat("A" + row, "A" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                XlsGenFromCustom("B" + row, val.DocumentNo);
                XlsStyleBorderAll("B" + row, "B" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("C" + row, val.TaxID);
                XlsStyleBorderAll("C" + row, "C" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("D" + row, val.CustomerName);
                XlsStyleBorderAll("D" + row, "D" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("E" + row, val.ProjectName);
                XlsStyleBorderAll("E" + row, "E" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("F" + row, val.BranchName);
                XlsStyleBorderAll("F" + row, "F" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("G" + row, val.Total);
                XlsStyleBorderAll("G" + row, "G" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("H" + row, val.VAT);
                XlsStyleBorderAll("H" + row, "H" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("I" + row, val.GrandAmount);
                XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("J" + row, val.CategoryName);
                XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("K" + row, val.DocumentRefNo);
                XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("l" + row, val.ReferenceNo);
                XlsStyleBorderAll("L" + row, "L" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("M" + row, val.DocumentStatusName);
                XlsStyleBorderAll("M" + row, "M" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("N" + row, val.Remark);
                XlsStyleBorderAll("N" + row, "N" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("O" + row, val.Noted);
                XlsStyleBorderAll("O" + row, "O" + row, System.Drawing.Color.Black);


                if (val.DocumentStatus != "3")
                {
                    total += val.Total.Value;
                    vat += val.VAT.Value;
                    grandtotal += val.GrandAmount.Value;
                }
                else
                    ObjWorkSheet.Cells[row, 1, row, 15].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                i++;
                row++;
            }

            XlsStyleFontBold("A5", "O5");
            XlsGenFromCustom("F" + row, "ยอดรวมทั้งหมด");
            XlsGenFromCustom("G" + row, total);
            XlsGenFromCustom("H" + row, vat);
            XlsGenFromCustom("I" + row, grandtotal);
            XlsStyleBorderBottom("A" + row, "O" + row);
            XlsStyleFormat("G6", "I" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");
            XlsStyleAutoFitColumns();

            base.SaveExcelFileNotMsg();
        }
    }
}