﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Excel
{
    public class RptWithholddingtax : IMauchly.Excel.ClsCreateExcel
    {
        public class Summary
        {
            public string Form { get; set; }
            public decimal vat { get; set; }
        }
        public BusinessProfile business { get; set; }
        public List<DocumentReport> list { get; set; }
        public string PeriodDate { get; set; }
        public override void Writes(string PathFile, string NameSheet)
        {
            base.Writes(PathFile, NameSheet);

            XlsMergeCell("A1", "N1");
            XlsMergeCell("B2", "D2");
            XlsMergeCell("B3", "D3");
            XlsMergeCell("B4", "D4");
            XlsStyleFontBold("A1", "A4");
            XlsGenFromCustom("A1", "รายงานภาษีหัก ณ ที่จ่าย");
            XlsStyleAligntCenter("A1", "A1");
            XlsGenFromCustom("A2", "ชื่อบริษัท");
            XlsGenFromCustom("A3", "เลขผู้เสียภาษี");
            XlsGenFromCustom("A4", "ช่วงเวลา");
            XlsGenFromCustom("B2", business.BusinessName);
            XlsGenFromCustom("B3", business.TaxID);
            XlsGenFromCustom("B4", PeriodDate);

            var form = list.GroupBy(a => new { a.WithHoldingTaxFormName }).Select(g => new { g.Key.WithHoldingTaxFormName });

            int i = 1;
            int row = 5;

            List<Summary> sumwith = new List<Summary>();

            foreach (var fm in form.OrderBy(c => c.WithHoldingTaxFormName))
            {
                decimal total = 0, vat = 0;
                List<DocumentReport> filter = list.Where(a => a.WithHoldingTaxFormName == fm.WithHoldingTaxFormName).ToList();

                XlsStyleAligntCenter("A" + row, "N" + row);
                XlsStyleBackground("A" + row, "N" + row, System.Drawing.Color.Yellow);
                XlsGenFromCustom("A" + row, "ลำดับที่");
                XlsStyleBorderAll("A" + row, "A" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("B" + row, "เลขผู้เสียภาษี");
                XlsStyleBorderAll("B" + row, "B" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("C" + row, "เลขที่เอกสาร");
                XlsStyleBorderAll("C" + row, "C" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("D" + row, "ผู้ถูกหัก ณ ที่จ่าย");
                XlsStyleBorderAll("D" + row, "D" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("E" + row, "ที่อยู่");
                XlsStyleBorderAll("E" + row, "E" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("F" + row, "สำนักงาน/สาขาเลขที่");
                XlsStyleBorderAll("F" + row, "F" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("G" + row, "วัน/เดือน/ปี");
                XlsStyleBorderAll("G" + row, "G" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("H" + row, "ประเภทเงินได้ที่จ่าย");
                XlsStyleBorderAll("H" + row, "H" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("I" + row, "อัตราภาษี");
                XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("J" + row, "มูลค่า");
                XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("K" + row, "ภาษีที่หัก");
                XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("L" + row, "ใบแนบ");
                XlsStyleBorderAll("L" + row, "L" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("M" + row, "อ้างอิง");
                XlsStyleBorderAll("M" + row, "M" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("N" + row, "สถานะ");
                XlsStyleBorderAll("N" + row, "N" + row, System.Drawing.Color.Black);

                row++;

                foreach (DocumentReport val in filter)
                {

                    XlsGenFromCustom("A" + row, i);
                    XlsStyleBorderAll("A" + row, "A" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("B" + row, val.TaxID);
                    XlsStyleBorderAll("B" + row, "B" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("C" + row, val.DocumentNo);
                    XlsStyleBorderAll("C" + row, "C" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("D" + row, val.CustomerName);
                    XlsStyleBorderAll("D" + row, "D" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("E" + row, val.SaleName);
                    XlsStyleBorderAll("E" + row, "E" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("F" + row, val.BranchName);
                    XlsStyleBorderAll("F" + row, "F" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("G" + row, val.DocumentDate);
                    XlsStyleFormat("G" + row, "G" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                    XlsStyleBorderAll("G" + row, "G" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("H" + row, val.CategoryName);
                    XlsStyleBorderAll("H" + row, "H" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("I" + row, val.WithHoldingRateDesc);
                    XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("J" + row, val.Total);
                    XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("K" + row, val.VAT);
                    XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("L" + row, val.WithHoldingTaxFormName);
                    XlsStyleBorderAll("L" + row, "L" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("M" + row, val.ReferenceNo);
                    XlsStyleBorderAll("M" + row, "M" + row, System.Drawing.Color.Black);
                    XlsGenFromCustom("N" + row, val.DocumentStatusName);
                    XlsStyleBorderAll("N" + row, "N" + row, System.Drawing.Color.Black);
                    XlsStyleBorderRight("N" + row, "N" + row);

                    if (val.DocumentStatus != "3")
                    {
                        total += val.Total.Value;
                        vat += val.VAT.Value;
                    }
                    else
                        ObjWorkSheet.Cells[row, 1, row, 14].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                    i++;
                    row++;
                }

                XlsStyleBorderBottom("A" + (row - 1), "N" + (row - 1));
                XlsStyleBorderBottom("A" + (row), "N" + (row));
                XlsStyleFontBold("H" + (row), "N" + (row));
                XlsGenFromCustom("H" + row, "ยอดรวม");
                XlsGenFromCustom("J" + row, total);
                XlsGenFromCustom("K" + row, vat);
                XlsStyleBorderRight("N" + row, "N" + row);
                row++; row++;

                sumwith.Add(new Summary()
                {
                    Form = fm.WithHoldingTaxFormName,
                    vat = vat
                }
                );
            }

            decimal sumval = 0;
            foreach (Summary sumnet in sumwith)
            {
                sumval = sumval + sumnet.vat;
                XlsGenFromCustom("I" + row, sumnet.Form);
                XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("J" + row, "");
                XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("K" + row, sumnet.vat);
                XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);

                row++;
            }

            XlsGenFromCustom("I" + row, "ยอดรวมทั้งหมด");
            XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
            XlsGenFromCustom("J" + row, "");
            XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
            XlsGenFromCustom("K" + row, sumval);
            XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);

            XlsStyleFormat("J6", "K" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");


            XlsStyleAutoFitColumns();

            base.SaveExcelFileNotMsg();
        }
    }
}