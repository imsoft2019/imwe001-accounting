﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Excel
{
    public class RptPurchaseOrder : IMauchly.Excel.ClsCreateExcel
    {
        public BusinessProfile business { get; set; }
        public List<DocumentReport> list { get; set; }
        public string PeriodDate { get; set; }
        public override void Writes(string PathFile, string NameSheet)
        {
            base.Writes(PathFile, NameSheet);

            int i = 1;

            XlsMergeCell("A1", "L1");
            XlsMergeCell("B2", "D2");
            XlsMergeCell("B3", "D3");
            XlsMergeCell("B4", "D4");
            XlsStyleFontBold("A1", "A4");
            XlsGenFromCustom("A1", "รายงานใบสั่งซื้อ");
            XlsStyleAligntCenter("A1", "A1");
            XlsGenFromCustom("A2", "ชื่อบริษัท");
            XlsGenFromCustom("A3", "เลขผู้เสียภาษี");
            XlsGenFromCustom("A4", "ช่วงเวลา");
            XlsGenFromCustom("B2", business.BusinessName);
            XlsGenFromCustom("B3", business.TaxID);
            XlsGenFromCustom("B4", PeriodDate);

            XlsStyleAligntCenter("A5", "L5");
            XlsStyleBackground("A5", "L5", System.Drawing.Color.Yellow);
            XlsGenFromCustom("A5", "ลำดับที่");
            XlsStyleBorderAll("A5", "A5", System.Drawing.Color.Black);
            XlsGenFromCustom("B5", "เลขที่เอกสาร");
            XlsStyleBorderAll("B5", "B5", System.Drawing.Color.Black);
            XlsGenFromCustom("C5", "วัน/เดือน/ปี");
            XlsStyleBorderAll("C5", "C5", System.Drawing.Color.Black);
            XlsGenFromCustom("D5", "ชื่อผู้จำหน่าย");
            XlsStyleBorderAll("D5", "D5", System.Drawing.Color.Black);
            XlsGenFromCustom("E5", "ชื่อโปรเจ็ค");
            XlsStyleBorderAll("E5", "E5", System.Drawing.Color.Black);
            XlsGenFromCustom("F5", "เลขผู้เสียภาษี");
            XlsStyleBorderAll("F5", "F5", System.Drawing.Color.Black);
            XlsGenFromCustom("G5", "สำนักงานใหญ่/สาขา");
            XlsStyleBorderAll("G5", "G5", System.Drawing.Color.Black);
            XlsGenFromCustom("H5", "มูลค่า");
            XlsStyleBorderAll("H5", "H5", System.Drawing.Color.Black);
            XlsGenFromCustom("I5", "ภาษีมูลค่าเพิ่ม");
            XlsStyleBorderAll("I5", "I5", System.Drawing.Color.Black);
            XlsGenFromCustom("J5", "เอกสารอ้างอิงในระบบ");
            XlsStyleBorderAll("J5", "J5", System.Drawing.Color.Black);
            XlsGenFromCustom("K5", "เลขที่อ้างอิง");
            XlsStyleBorderAll("K5", "K5", System.Drawing.Color.Black);
            XlsGenFromCustom("L5", "สถานะ");
            XlsStyleBorderAll("L5", "L5", System.Drawing.Color.Black);

            int row = 6;
            decimal total = 0, vat = 0, grandtotal = 0;

            foreach (DocumentReport val in list)
            {

                XlsGenFromCustom("A" + row, i);
                XlsStyleBorderAll("A" + row, "A" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("B" + row, val.DocumentNo);
                XlsStyleBorderAll("B" + row, "B" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("C" + row, val.DocumentDate);
                XlsStyleBorderAll("C" + row, "C" + row, System.Drawing.Color.Black);
                XlsStyleFormat("C" + row, "C" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                XlsGenFromCustom("D" + row, val.CustomerName);
                XlsStyleBorderAll("D" + row, "D" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("E" + row, val.ProjectName);
                XlsStyleBorderAll("E" + row, "E" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("F" + row, val.TaxID);
                XlsStyleBorderAll("F" + row, "F" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("G" + row, val.BranchName);
                XlsStyleBorderAll("G" + row, "G" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("H" + row, val.Total);
                XlsStyleBorderAll("H" + row, "H" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("I" + row, val.VAT);
                XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("J" + row, val.DocumentRefNo);
                XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("K" + row, val.ReferenceNo);
                XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("l" + row, val.DocumentStatusName);
                XlsStyleBorderAll("L" + row, "L" + row, System.Drawing.Color.Black);

                if (val.DocumentStatus != "3")
                {
                    total += val.Total.Value;
                    vat += val.VAT.Value;
                    grandtotal += val.GrandAmount.Value;
                }
                else
                    ObjWorkSheet.Cells[row, 1, row, 16].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                i++;
                row++;
            }

            XlsStyleFontBold("A5", "L5");
            XlsGenFromCustom("G" + row, "ยอดรวมทั้งหมด");
            XlsGenFromCustom("H" + row, total);
            XlsGenFromCustom("I" + row, vat);
            XlsStyleBorderBottom("A" + row, "L" + row);
            XlsStyleFormat("H6", "I" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");
            XlsStyleAutoFitColumns();

            base.SaveExcelFileNotMsg();
        }
    }
}