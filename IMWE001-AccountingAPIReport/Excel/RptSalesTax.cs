﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;


namespace IMWE001_AccountingAPIReport.Excel
{
    public class RptSalesTax : IMauchly.Excel.ClsCreateExcel
    {
        public BusinessProfile business { get; set; }
        public List<DocumentReport> list { get; set; }
        public string PeriodMonth { get; set; }
        public string PeriodYear { get; set; }
        public override void Writes(string PathFile, string NameSheet)
        {
            base.Writes(PathFile, NameSheet);

            int i = 1;

            XlsMergeCell("A1", "L1");
            XlsMergeCell("A2", "L2");
            XlsMergeCell("A3", "L3");
            XlsMergeCell("D4", "F4");
            XlsMergeCell("G4", "I4");
            XlsGenFromCustom("A1", "รายงานภาษีขายตามเอกสาร");
            XlsStyleAligntCenter("A1", "L3");
            XlsGenFromCustom("A2", string.Format("สำหรับงวดภาษี เดือน {0} ปี {1}", PeriodMonth, PeriodYear));
            XlsGenFromCustom("A3", string.Format("ชื่อผู้ประกอบการ {0} เลขประจำผู้เสียภาษีอากร {1}",business.BusinessName,business.TaxID));
            XlsGenFromCustom("C4", "ชื่อสถานที่ประกอบการ ");
            XlsGenFromCustom("D4", business.Address);
            XlsGenFromCustom("G4", string.Format("สำนักงาน/สาขาเลขที่ {0}",business.BranchName));

            XlsStyleAligntCenter("A6", "L6");
            XlsStyleBackground("A6", "L6", System.Drawing.Color.Yellow);
            XlsGenFromCustom("A6", "ลำดับที่");
            XlsStyleBorderAll("A6", "A6", System.Drawing.Color.Black);
            XlsGenFromCustom("B6", "วัน/เดือน/ปี");
            XlsStyleBorderAll("B6", "B6", System.Drawing.Color.Black);
            XlsGenFromCustom("C6", "เลขที่เอกสาร");
            XlsStyleBorderAll("C6", "C6", System.Drawing.Color.Black);
            XlsGenFromCustom("D6", "เลขที่อ้างอิง");
            XlsStyleBorderAll("D6", "D6", System.Drawing.Color.Black);
            XlsGenFromCustom("E6", "ชื่อลูกค้า");
            XlsStyleBorderAll("E6", "E6", System.Drawing.Color.Black);
            XlsGenFromCustom("F6", "ชื่อโปรเจ็ค");
            XlsStyleBorderAll("F6", "F6", System.Drawing.Color.Black);
            XlsGenFromCustom("G6", "เลขผู้เสียภาษี");
            XlsStyleBorderAll("G6", "G6", System.Drawing.Color.Black);
            XlsGenFromCustom("H6", "สำนักงานใหญ่/สาขา");
            XlsStyleBorderAll("H6", "H6", System.Drawing.Color.Black);
            XlsGenFromCustom("I6", "มูลค่า");
            XlsStyleBorderAll("I6", "I6", System.Drawing.Color.Black);
            XlsGenFromCustom("J6", "ภาษีมูลค่าเพิ่ม");
            XlsStyleBorderAll("J6", "J6", System.Drawing.Color.Black);
            XlsGenFromCustom("K6", "เอกสารอ้างอิงในระบบ");
            XlsStyleBorderAll("K6", "K6", System.Drawing.Color.Black);
            XlsGenFromCustom("l6", "สถานะ");
            XlsStyleBorderAll("L6", "L6", System.Drawing.Color.Black);

            int row = 7;
            decimal total = 0, vat = 0, grandtotal = 0;

            foreach (DocumentReport val in list)
            {

                XlsGenFromCustom("A" + row, i);
                XlsStyleBorderAll("A" + row, "A" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("B" + row, val.DocumentDate);
                XlsStyleBorderAll("B" + row, "B" + row, System.Drawing.Color.Black);
                XlsStyleFormat("B" + row, "B" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.วันเดือนปี102, "");
                XlsGenFromCustom("C" + row, val.DocumentNo);
                XlsStyleBorderAll("C" + row, "C" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("D" + row, val.ReferenceNo);
                XlsStyleBorderAll("D" + row, "D" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("E" + row, val.CustomerName);
                XlsStyleBorderAll("E" + row, "E" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("F" + row, val.ProjectName);
                XlsStyleBorderAll("F" + row, "F" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("G" + row, val.TaxID);
                XlsStyleBorderAll("G" + row, "G" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("H" + row, val.BranchName);
                XlsStyleBorderAll("H" + row, "H" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("I" + row, val.Total);
                XlsStyleBorderAll("I" + row, "I" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("J" + row, val.VAT);
                XlsStyleBorderAll("J" + row, "J" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("K" + row, val.DocumentRefNo);
                XlsStyleBorderAll("K" + row, "K" + row, System.Drawing.Color.Black);
                XlsGenFromCustom("l" + row, val.DocumentStatusName);
                XlsStyleBorderAll("L" + row, "L" + row, System.Drawing.Color.Black);

                if (val.DocumentStatus != "3")
                {
                    total += val.Total.Value;
                    vat += val.VAT.Value;
                    grandtotal += val.GrandAmount.Value;
                }
                else
                    ObjWorkSheet.Cells[row, 1, row, 16].Style.Font.Color.SetColor(System.Drawing.Color.Red);

                i++;
                row++;
            }

            XlsStyleFontBold("A6", "L6");
            XlsGenFromCustom("H" + row, "ยอดรวมทั้งหมด");
            XlsGenFromCustom("I" + row, total);
            XlsGenFromCustom("J" + row, vat);
            XlsStyleBorderBottom("A" + row, "L" + row);
            XlsStyleFormat("I6", "J" + row, IMauchly.Excel.ClassEnumulation.EnumFormatExcel.ตัวเลขมีทศนิยม2ตำแหน่ง, "");
            XlsStyleAutoFitColumns();

            base.SaveExcelFileNotMsg();
        }
    }
}