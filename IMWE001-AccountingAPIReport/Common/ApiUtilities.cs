﻿using IMWE001_AccountingAPIReport.Common;
using IMWE001_AccountingAPIReport.Models;
//using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace IMWE001_AccountingAPIReport.Controllers
{
   
    public class ApiUtilities 
    {
        private HttpClient InitHttpClient()
        {
            string pathlink = System.Configuration.ConfigurationManager.AppSettings["WebApi"];
            string tokenid = System.Configuration.ConfigurationManager.AppSettings["Tokenid"];

            var result = new HttpClient { BaseAddress = new Uri(pathlink) };

            if (pathlink.Substring(0, 5).ToUpper() == "HTTPS")
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            }
            result.DefaultRequestHeaders.Add("Weacct-Apim-Subscription-Key", tokenid);

            //HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri(pathlink);
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return result;


        }
        public T GetApi<T>(string url)
        {
            T result;
            using (HttpClient client = InitHttpClient())
            {
                HttpResponseMessage responseMsg = client.GetAsync(url).Result;
                if (responseMsg.IsSuccessStatusCode)
                {
                    result = responseMsg.Content.ReadAsAsync<T>().Result;
                }
                else
                {
                    var err = responseMsg.Content.ReadAsAsync<ResponseError>().Result;
                    throw new Exception(err.ResponseMessage);
                }
            }
            return result;
        }

        public T PostApi<T>(string URL, string jsoninput)
        {
            T result;
            using (HttpClient client = InitHttpClient())
            {
                //var jsonObject = JsonConvert.SerializeObject(input);
                var content = new StringContent(jsoninput.ToString(), Encoding.UTF8, "application/json");

                HttpResponseMessage responseMsg = client.PostAsync(URL, content).Result;
                string msg = responseMsg.Content.ReadAsStringAsync().Result;

                if (responseMsg.IsSuccessStatusCode)
                    result = responseMsg.Content.ReadAsAsync<T>().Result;
                else
                {
                    var err = responseMsg.Content.ReadAsAsync<ResponseError>().Result;
                    throw new Exception(err.ResponseMessage);
                }
            }
            return result;
        }


        //[NonAction]
        //public virtual JsonResult WriteJson(object data)
        //{
        //    try
        //    {

        //        JsonSerializerSettings sett = new JsonSerializerSettings();
        //        sett.Converters.Add(new UtcDateTimeConverter());
        //        return new JsonResult(data, sett);

        //    catch (Exception ex)
        //    {
        //        return WriteJson(new { responsecode = ((int)Common.Abstract.ResponseCode.ErrorTransaction).ToString(), errormessage = ex.Message });
        //    }
        //}
    }
}