﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Common
{
    public class iTextSharpBinding : IDisposable
    {
        PdfReader oReader;
        PdfStamper oStamper;
        byte[] s;
        public iTextSharpBinding(string SourceFile, string DestinationFile)
        {
            oReader = new PdfReader(SourceFile);
            oStamper = new PdfStamper(oReader, new System.IO.FileStream(DestinationFile, System.IO.FileMode.Create));
        }
        public iTextSharpBinding(byte[] SourceFile, string DestinationFile)
        {
            s = SourceFile;
            oReader = new PdfReader(SourceFile);
            oStamper = new PdfStamper(oReader, new System.IO.FileStream(DestinationFile, System.IO.FileMode.Create));
        }

        public void SetField(string FieldName, string Value)
        {
            AcroFields oField = oStamper.AcroFields;
            oField.SetField(FieldName, Value);
            
        }
        public void SetField(string FieldName, string Value,bool Appearance)
        {
            AcroFields oField = oStamper.AcroFields;
            oField.SetField(FieldName, Value,Appearance);

        }
        public void RemoveField(string FieldName)
        {
            AcroFields oField = oStamper.AcroFields;
            oField.RemoveField(FieldName);

        }
       
        private iTextSharp.text.Image SetRectangleImage(string FieldName, string FileImage, int PositionX, int PositionY)
        {
            AcroFields oField = oStamper.AcroFields;
            iTextSharp.text.Image image = null;
            image = iTextSharp.text.Image.GetInstance(FileImage);
            var rec = oField.GetFieldPositions(FieldName)[0].position;
            image.ScaleAbsolute(rec.Width, rec.Height);
            image.SetAbsolutePosition(PositionX, PositionY);
            return image;
        }
        public void SetImage(string FieldName, byte[] bimg, float? Rotation)
        {
            using (Stream fs = new MemoryStream(bimg))
            {
                if (fs != null)
                {
                    AcroFields oField = oStamper.AcroFields;
                    PdfContentByte cb = oStamper.GetOverContent(1);
                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(fs);
                    var rec = oField.GetFieldPositions(FieldName)[0].position;
                    if (FieldName == "txtLogo")
                    {
                        img.ScaleAbsolute(170, 30);
                        img.SetAbsolutePosition(440, rec.Bottom);
                    }
                    else
                    {
                        img.ScaleAbsolute(rec.Width, rec.Height);
                        img.SetAbsolutePosition(rec.Left, rec.Bottom);
                    }
                    if (Rotation.HasValue)
                    {
                        img.InitialRotation = Rotation.Value;
                    }
                    cb.AddImage(img);
                }

                fs.Dispose();
            }
        }
      
        public void Dispose()
        {
            if (oStamper != null)
                oStamper.Close();
            if (oReader != null)
                oReader.Close();
        }


    }
}