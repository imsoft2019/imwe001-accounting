﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Common
{
    public class ThaiBathText
    {
        //MainUtil="บาท",SubUnit="สตางค์",WordOfEnd="ถ้วน"
        public static string NumberToTextThai(double Number, string MainUnit, string SubUnit, string WordOfEnd)
        {
            string strNum;
            string strAfterPoint = "";
            if (Number > 0)
            {
                strNum = Number.ToString("#.#0");
                if (strNum.Substring(0, 1) == ".")
                    strNum = "0" + strNum;
            }
            else
                strNum = "0.00";
            string tmpNum = strNum;
            if (strNum.Length > 3)
                strNum = strNum.Substring(0, strNum.Length - 3);
            int[] intNum = new int[strNum.Length];

            for (int i = 0; i < strNum.Length; i++)
            {
                intNum[i] = Convert.ToInt16(tmpNum.Substring(0, 1));
                tmpNum = tmpNum.Remove(0, 1);

            }

            string strResult = "";
            for (int i = 0; i < intNum.Length; i++)
            {
                strResult += GetTextNumberThai(intNum[i], intNum.Length - i, strNum);
            }
            strNum = Number.ToString("#.#0");
            strNum = strNum.Substring(strNum.Length - 2, 2);
            tmpNum = strNum;
            intNum = new int[strNum.Length];
            for (int i = 0; i < strNum.Length; i++)
            {
                intNum[i] = Convert.ToInt16(tmpNum.Substring(0, 1));
                tmpNum = tmpNum.Remove(0, 1);
            }
            for (int i = 0; i < intNum.Length; i++)
            {
                strAfterPoint += GetTextNumberThai(intNum[i], intNum.Length - i, strNum);
            }

            strResult += MainUnit;
            if (strAfterPoint.Length > 0)
            {
                strAfterPoint += SubUnit;
                strResult += strAfterPoint;
            }

            double tmp = (Number * 100) % 100;

            if (tmp > 0)
                return strResult;
            else
                return strResult + WordOfEnd;
        }

        private static string GetTextNumberThai(int Number, int Position, string AllNumber)
        {
            string TextNum = "";
            switch (Number)
            {
                case 0:
                    if (AllNumber.Length == 1)
                        TextNum = "ศูนย์";
                    break;
                case 1:
                    if (Position == 1 && AllNumber.Length != 1)
                    {
                        if (AllNumber.Substring(AllNumber.Length - 2, 1) != "0")
                            TextNum = "เอ็ด";
                        else
                            TextNum = "หนึ่ง";
                    }
                    else if (Position == 7 && AllNumber.Length >= 8)
                    {
                        if (AllNumber.Substring(AllNumber.Length - 8, 1) != "0")
                            TextNum = "เอ็ด";
                        else
                            TextNum = "หนึ่ง";
                    }
                    else
                        TextNum = "หนึ่ง";
                    break;
                case 2:
                    if (Position == 2 || Position == 8)
                        TextNum = "ยี่";
                    else
                        TextNum = "สอง";
                    break;
                case 3:
                    TextNum = "สาม";
                    break;
                case 4:
                    TextNum = "สี่";
                    break;
                case 5:
                    TextNum = "ห้า";
                    break;
                case 6:
                    TextNum = "หก";
                    break;
                case 7:
                    TextNum = "เจ็ด";
                    break;
                case 8:
                    TextNum = "แปด";
                    break;
                case 9:
                    TextNum = "เก้า";
                    break;

            }
            switch (Position)
            {
                case 1: //หน่วย
                    //					if (Number==0)
                    //						TextNum="ศูนย์";
                    break;
                case 2: //สิบ
                    if (Number == 1)
                        TextNum = "สิบ";
                    else if (Number == 0)
                        TextNum += "";
                    else
                        TextNum += "สิบ";
                    break;
                case 3: //ร้อย
                    if (Number != 0)
                        TextNum += "ร้อย";
                    else
                        TextNum = "";
                    break;
                case 4: //พัน
                    if (Number != 0)
                        TextNum += "พัน";
                    else
                        TextNum = "";
                    break;
                case 5: //หมื่น
                    if (Number != 0)
                        TextNum += "หมื่น";
                    else
                        TextNum = "";
                    break;
                case 6: //แสน
                    if (Number != 0)
                        TextNum += "แสน";
                    else
                        TextNum = "";
                    break;
                case 7: //ล้าน
                    if (Number == 0 && AllNumber.Length > 7)
                        TextNum += "ล้าน";
                    else if (AllNumber.Length == 7)
                        TextNum += "ล้าน";
                    else if (AllNumber.Length >= 8)
                        TextNum += "ล้าน";
                    else if (Number == 0)
                        TextNum = "";
                    break;
                case 8: //สิบล้าน
                    if (Number == 1)
                        TextNum = "สิบ";
                    else if (Number == 2)
                        TextNum = "ยี่สิบ";
                    else if (Number >= 3)
                        TextNum += "สิบ";
                    else if (Number == 0)
                        TextNum = "";
                    break;
                case 9: //ร้อยล้าน
                    if (Number > 0)
                        TextNum += "ร้อย";
                    else
                        TextNum = "";
                    break;
                case 10: //พันล้าน
                    if (Number > 0)
                        TextNum += "พัน";
                    else
                        TextNum = "";
                    break;
                case 11: //หมื่นล้าน
                    if (Number > 0)
                        TextNum += "หมื่น";
                    else
                        TextNum = "";
                    break;
                case 12: // แสนล้าน
                    if (Number > 0)
                        TextNum += "แสน";
                    else
                        TextNum = "";
                    break;
            }
            return TextNum;
        }
    }

}