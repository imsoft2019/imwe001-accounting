﻿
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace IMWE001_AccountingAPIReport.Common
{
    public class GenerateReport
    {
        string FileRDLC;
        string NewFileName;
        public GenerateReport(string FileRDLC)
        {
            this.FileRDLC = FileRDLC;
        }
        public void OverideFileName(string NewFileName)
        {
            this.NewFileName = NewFileName;
        }
        public string PDF(ReportDataSource[] DataSource)
        {
            string reportpdf = "";
           ReportViewer viewer = new ReportViewer();
            viewer.LocalReport.ReportPath = System.Web.Hosting.HostingEnvironment.MapPath("~\\Report\\"+this.FileRDLC+".rdlc");
            for(int i=0; i<DataSource.Length;i++)
            {
                viewer.LocalReport.DataSources.Add(DataSource[i]);
            }
            //viewer.LocalReport.DataSources.Add(new ReportDataSource(this.DataSetName, Datas));
            if (string.IsNullOrEmpty(this.NewFileName))
            {
              reportpdf = "~\\Temp\\" + this.FileRDLC + "_" + DateTime.Now.ToString("ddMMyyyyHHmmssfff") + ".pdf";
            }
            else
            {
                reportpdf = "~\\Temp\\" +this.NewFileName+ ".pdf";
            }
            ExportPDF(System.Web.Hosting.HostingEnvironment.MapPath(reportpdf), viewer);
            return reportpdf;
        }
      
        private void ExportPDF(string PathFile, ReportViewer viewer)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            viewer.Font.Name = "Aksorn Regular";
            byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            // byte[] bytes = viewer.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.          
            // System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);


            using (FileStream fs = new FileStream(PathFile, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        }
    }
}