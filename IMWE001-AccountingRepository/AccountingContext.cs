﻿using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.DocumentExpenses;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.Parameter;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.Workflow;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace IMWE001_AccountingRepository
{
    public class AccountingContext : DbContext
    {
        //public readonly string _connectionString = string.Empty;

        //public string ConnectionString
        //{
        //    get => _connectionString;
        //}

        public static string ConnectionString { get; set; }

        public AccountingContext()
        {
            Database.SetCommandTimeout(1500000);
        }

        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(_connectionString);
            
          //  string ConnectionString = ConfigurationExtensions.GetConnectionString(config, "PostAccountDatabase");

            optionsBuilder.UseNpgsql(AccountingContext.ConnectionString);

            //optionsBuilder.UseNpgsql("User ID =risk;Password=p@ssw0rd;Server=35.240.164.180;Port=5432;Database=postaccount_uat;Integrated Security=true;Pooling=true;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            #region Business
            modelBuilder.Entity<BusinessProfile>(entity =>
            {
                entity.HasKey(e => e.UID);
            });

            modelBuilder.Entity<BusinessBankAccount>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.UID, e.BankCode, e.AccountNo });
            });

            modelBuilder.Entity<DocumentRemark>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.DocumentType, e.DocumentKey });
            });

            modelBuilder.Entity<BusinessParameter>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.ParameterField });
            });

            modelBuilder.Entity<DocumentPrefix>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.Prefix, e.DocumentType, e.Suffix });
            });
            #endregion

            #region Products
            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.ProductKey });
            });
            #endregion

            #region User

            modelBuilder.Entity<UserProfile>(entity =>
            {
                entity.HasKey(e => new { e.UID, e.Email });
            });

            #endregion

            #region Contact

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.ContactKey });
            });

            #endregion

            #region Parameter
            modelBuilder.Entity<Param_Bank>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_Category>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_UnitType>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });

            modelBuilder.Entity<Param_Position>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_WithholdingRate>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            #endregion

            #region Workflow
            modelBuilder.Entity<Param_DocQuotationStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });

            modelBuilder.Entity<Param_DocBillingStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_DocInvoiceStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_DocReceiptStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_DocPurchaseStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_DocReceivingInventoryStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_DocCreditNoteStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
            modelBuilder.Entity<Param_DocDebitNoteStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });

            modelBuilder.Entity<Param_DocCashSaleStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });

            modelBuilder.Entity<Param_DocExpenseStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });
               modelBuilder.Entity<Param_DocWithholdingTaxStatus>(entity =>
            {
                entity.HasKey(e => new { e.ID });
            });

            #endregion

            #region Document Sell
            modelBuilder.Entity<Document_Attach>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.AttachKey });
            });

            modelBuilder.Entity<Document_Reference>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.SID });
            });

            modelBuilder.Entity<Document_ReceiveInfo>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.SID, e.DocumentKey, e.DocumentType });
            });

            modelBuilder.Entity<Document_Quotation>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.QuotationKey });
            });

            modelBuilder.Entity<Document_QuotationDetail>(entity =>
            {
                entity.HasKey(e => new { e.QuotationDetailKey });
            });

            modelBuilder.Entity<Document_Billing>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.BillingKey });
            });

            modelBuilder.Entity<Document_BillingDetail>(entity =>
            {
                entity.HasKey(e => new { e.BillingDetailKey });
            });

            modelBuilder.Entity<Document_BillingCumulativeDetail>(entity =>
            {
                entity.HasKey(e => new { e.BillingDetailKey });
            });

            modelBuilder.Entity<Document_Invoice>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.InvoiceKey });
            });

            modelBuilder.Entity<Document_InvoiceDetail>(entity =>
            {
                entity.HasKey(e => new { e.InvoiceDetailKey });
            });

            modelBuilder.Entity<Document_CreditNote>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.CreditNoteKey });
            });

            modelBuilder.Entity<Document_CreditNoteDetail>(entity =>
            {
                entity.HasKey(e => new { e.CreditNoteDetailKey });
            });

            modelBuilder.Entity<Document_DebitNote>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.DebitNoteKey });
            });

            modelBuilder.Entity<Document_DebitNoteDetail>(entity =>
            {
                entity.HasKey(e => new { e.DebitNoteDetailKey });
            });

            modelBuilder.Entity<Document_Receipt>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.ReceiptKey });
            });

            modelBuilder.Entity<Document_ReceiptDetail>(entity =>
            {
                entity.HasKey(e => new { e.ReceiptDetailKey });
            });

            modelBuilder.Entity<Document_ReceiptCumulativeDetail>(entity =>
            {
                entity.HasKey(e => new { e.ReceiptDetailKey });
            });

            modelBuilder.Entity<Document_CashSale>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.CashSaleKey });
            });

            modelBuilder.Entity<Document_CashSaleDetail>(entity =>
            {
                entity.HasKey(e => new { e.CashSaleDetailKey });
            });

            #endregion

            #region Document Buy

            modelBuilder.Entity<Document_PurchaseOrder>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.PurchaseOrderKey });
            });

            modelBuilder.Entity<Document_PurchaseOrderDetail>(entity =>
            {
                entity.HasKey(e => new { e.PurchaseOrderDetailKey });
            });

            modelBuilder.Entity<Document_ReceiveInventory>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.ReceiveInventoryKey });
            });

            modelBuilder.Entity<Document_ReceiveInventoryDetail>(entity =>
            {
                entity.HasKey(e => new { e.ReceiveInventoryDetailKey });
            });

            modelBuilder.Entity<Document_ReceiveTaxInvoiceInfo>(entity =>
            {
                entity.HasKey(e => new { e.SID });
            });


            #endregion

            #region Document Expense

            modelBuilder.Entity<Document_Expenses>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.ExpensesKey });
            });

            modelBuilder.Entity<Document_ExpensesDetail>(entity =>
            {
                entity.HasKey(e => new { e.ExpensesDetailKey });
            });


            modelBuilder.Entity<Document_WithholdingTax>(entity =>
            {
                entity.HasKey(e => new { e.BusinessID, e.WithholdingTaxKey });
            });

            modelBuilder.Entity<Document_WithholdingTaxDetail>(entity =>
            {
                entity.HasKey(e => new { e.WithholdingTaxDetailKey });
            });


            #endregion

            #region Log

            modelBuilder.Entity<Log_Workflow>(entity =>
            {
                entity.HasKey(e => new { e.SID });
            });

            modelBuilder.Entity<Log_Activity>(entity =>
            {
                entity.HasKey(e => new { e.SID });
            });
            #endregion
        }
    }

}
