﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.Products
{
    public class ProductRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Product, AccountingContext>
    {
        public Product Save(Product input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Product>();
                        if (!input.ProductKey.HasValue)
                        {
                            input.isdelete = 1;
                            otable.Add(input);
                            context.SaveChanges();
                        }
                        else
                        {
                            Product val = context.Set<Product>().Where(a => a.BusinessID == input.BusinessID && a.ProductKey == input.ProductKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลสินค้า/บริการ");

                            val.ProductCode = input.ProductCode;
                            val.ProductName = input.ProductName;
                            val.ProductDescription = input.ProductDescription;
                            val.UnitTypeName = input.UnitTypeName;
                            val.CategoryName = input.CategoryName;
                            val.SellPrice = input.SellPrice;
                            val.SellTaxPrice = input.SellTaxPrice;
                            val.SellAfterTaxPrice = input.SellAfterTaxPrice;
                            val.SellTaxType = input.SellTaxType;
                            val.SellTaxTypeName = input.SellTaxTypeName;
                            val.BuyPrice = input.BuyPrice;
                            val.BuyTaxPrice = input.BuyTaxPrice;
                            val.BuyAfterTaxPrice = input.BuyAfterTaxPrice;
                            val.BuyTaxType = input.BuyTaxType;
                            val.BuyTaxTypeName = input.BuyTaxTypeName;
                            val.BuyDescription = input.BuyDescription;
                            val.ProductBarCode = input.ProductBarCode;
                            val.PicturePath = input.PicturePath;
                            val.ThumbnailPath = input.ThumbnailPath;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;
                            context.SaveChanges();
                        }

                        var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                        Log_Activity request = new Log_Activity();
                        request.BusinessID = input.BusinessID;
                        request.ActivityDate = DateTime.Now;
                        request.ActivityHeader = "ข้อมูลสินค้า / บริการ";
                        request.ActivityTodo = "เพิ่ม/แก้ไขข้อมูลสินค้า / บริการ";
                        request.ActivityDesc = "ทำการเพิ่ม/แก้ไขข้อมูลสินค้า / บริการ ชื่อสนค้า" + input.ProductName;
                        request.ActivityByName = user;
                        request.CreateOn = DateTime.Now;
                        request.CreateBy = input.UpdateBy;
                        Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                        Log_ActivityRepository.Save(request, context);
                        context.SaveChanges();

                        if (!string.IsNullOrEmpty(input.UnitTypeName) && !string.IsNullOrEmpty(input.UnitTypeName.Trim()))
                        {
                            Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository();
                            Param_UnitTypeRepository.Add(input.UnitTypeName, context);
                        }

                        if (!string.IsNullOrEmpty(input.CategoryName) && !string.IsNullOrEmpty(input.CategoryName.Trim()))
                        {
                            Param_CategoryRepository Param_CategoryRepository = new Param_CategoryRepository();
                            Param_CategoryRepository.Add(input.CategoryName, context);
                        }

                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Delete(Product input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Product>().Where(a => a.ProductKey == input.ProductKey).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();

                        var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                        Log_Activity request = new Log_Activity();
                        request.BusinessID = input.BusinessID;
                        request.ActivityDate = DateTime.Now;
                        request.ActivityHeader = "ข้อมูลสินค้า / บริการ";
                        request.ActivityTodo = "ลบอมูลสินค้า / บริการ";
                        request.ActivityDesc = "ทำการลบข้อมูลสินค้า / บริการ ชื่อสนค้า" + input.ProductName;
                        request.ActivityByName = user;
                        request.CreateOn = DateTime.Now;
                        request.CreateBy = input.UpdateBy;
                        Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                        Log_ActivityRepository.Save(request, context);
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
