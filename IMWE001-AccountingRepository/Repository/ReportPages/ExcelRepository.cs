﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.DocumentExpenses;

namespace IMWE001_AccountingRepository.Repository.ReportPages
{
    public class ExcelRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_Invoice, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        public List<DocumentReport> GetReportSalesSummary(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var cashtable = context.Set<Document_CashSale>();
                    var cashdata = from bil in cashtable
                                   where bil.BusinessID == input.businessid && bil.CashSaleDate >= input.startdate && bil.CashSaleDate <= input.enddate && bil.isdelete == isactive
                                   select new
                                   {
                                       bil.BusinessID,
                                       bil.CashSaleKey,
                                       bil.CashSaleNo,
                                       bil.CashSaleStatus,
                                       bil.CashSaleStatusName,
                                       bil.TotalAfterDiscountAmount,
                                       bil.VAT,
                                       bil.GrandAmount,
                                       bil.CashSaleDate,
                                       bil.ProjectName,
                                       bil.CustomerName,
                                       bil.CustomerTaxID,
                                       bil.CustomerBranch,
                                       bil.Remark,
                                       bil.Noted,
                                       bil.SaleName,
                                       bil.ReferenceNo
                                   };

                    var info = context.Set<Document_ReceiveInfo>();

                    foreach (var af in cashdata.ToList().OrderByDescending(a => a.CashSaleKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CashSaleKey,
                            DocumentNo = af.CashSaleNo,
                            DocumentType = "CA",
                            DocumentDate = af.CashSaleDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.CashSaleStatus,
                            DocumentStatusName = af.CashSaleStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            ReferenceNo = af.ReferenceNo,
                            Remark = af.Remark,
                            Noted = af.Noted,
                            SaleName = af.SaleName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch
                        });

                    }


                    var invtable = context.Set<Document_Invoice>();
                    var invdata = from bil in invtable
                                  where bil.BusinessID == input.businessid && bil.InvoiceDate >= input.startdate && bil.InvoiceDate <= input.enddate && bil.isdelete == isactive
                                  select new
                                  {
                                      bil.BusinessID,
                                      bil.InvoiceKey,
                                      bil.InvoiceNo,
                                      bil.InvoiceStatus,
                                      bil.InvoiceStatusName,
                                      bil.TotalAfterDiscountAmount,
                                      bil.VAT,
                                      bil.GrandAmount,
                                      bil.InvoiceDate,
                                      bil.ProjectName,
                                      bil.CustomerName,
                                      bil.CustomerTaxID,
                                      bil.CustomerBranch,
                                      bil.Remark,
                                      bil.Noted,
                                      bil.SaleName,
                                      bil.ReferenceNo,
                                      bil.BillingNoteNo,
                                      bil.ReceiptNo
                                  };

                    foreach (var af in invdata.ToList().OrderByDescending(a => a.InvoiceKey))
                        response.Add(new DocumentReport()

                        {
                            DocumentKey = af.InvoiceKey,
                            DocumentNo = af.InvoiceNo,
                            DocumentType = "INV",
                            DocumentDate = af.InvoiceDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.InvoiceStatus,
                            DocumentStatusName = af.InvoiceStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReferenceNo = af.ReferenceNo,
                            Remark = af.Remark,
                            Noted = af.Noted,
                            SaleName = af.SaleName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch
                        });

                    var debittable = context.Set<Document_DebitNote>();
                    var debitdata = from bil in debittable
                                    where bil.BusinessID == input.businessid && bil.DebitNoteDate >= input.startdate && bil.DebitNoteDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.DebitNoteKey,
                                        bil.DebitNoteNo,
                                        bil.DebitNoteStatus,
                                        bil.DebitNoteStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.DebitNoteDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.CustomerTaxID,
                                        bil.CustomerBranch,
                                        bil.Remark,
                                        bil.Noted,
                                        bil.SaleName,
                                        bil.ReferenceNo,
                                        bil.BillingNoteNo,
                                        bil.ReceiptNo
                                    };

                    foreach (var af in debitdata.ToList().OrderByDescending(a => a.DebitNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.DebitNoteKey,
                            DocumentNo = af.DebitNoteNo,
                            DocumentType = "DN",
                            DocumentDate = af.DebitNoteDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.DebitNoteStatus,
                            DocumentStatusName = af.DebitNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReceiptNo = af.ReceiptNo,
                            ReferenceNo = af.ReferenceNo,
                            Remark = af.Remark,
                            Noted = af.Noted,
                            SaleName = af.SaleName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch
                        });


                    var credittable = context.Set<Document_CreditNote>();
                    var creditdata = from bil in credittable
                                     where bil.BusinessID == input.businessid && bil.CreditNoteDate >= input.startdate && bil.CreditNoteDate <= input.enddate && bil.isdelete == isactive
                                     select new
                                     {
                                         bil.BusinessID,
                                         bil.CreditNoteKey,
                                         bil.CreditNoteNo,
                                         bil.CreditNoteStatus,
                                         bil.CreditNoteStatusName,
                                         bil.TotalAfterDiscountAmount,
                                         bil.VAT,
                                         bil.GrandAmount,
                                         bil.CreditNoteDate,
                                         bil.ProjectName,
                                         bil.CustomerName,
                                         bil.CustomerTaxID,
                                         bil.CustomerBranch,
                                         bil.Remark,
                                         bil.Noted,
                                         bil.SaleName,
                                         bil.ReferenceNo,
                                         bil.BillingNoteNo,
                                         bil.ReceiptNo
                                     };

                    foreach (var af in creditdata.ToList().OrderByDescending(a => a.CreditNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CreditNoteKey,
                            DocumentNo = af.CreditNoteNo,
                            DocumentType = "CN",
                            DocumentDate = af.CreditNoteDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.CreditNoteStatus,
                            DocumentStatusName = af.CreditNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReceiptNo = af.ReceiptNo,
                            ReferenceNo = af.ReferenceNo,
                            Remark = af.Remark,
                            Noted = af.Noted,
                            SaleName = af.SaleName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch
                        });
                    // GetDocument = context.Set<Document_Billing>().Where(a => a.BusinessID == input.businessid && a.BillingDate >= input.startdate && a.BillingDate <= input.enddate && a.isdelete == isactive).OrderByDescending(a => a.BillingKey).ToList();

                    if (response != null && response.Count > 0)
                    {
                        var rtable = context.Set<Document_Reference>();
                        foreach (DocumentReport doc in response)
                        {
                            doc.DocumentRefNo = string.Empty;
                            List<Document_Reference> dref = new List<Document_Reference>();

                            if (doc.DocumentType == "CA")
                                dref = rtable.Where(a => a.CashSaleNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "INV")
                                dref = rtable.Where(a => a.InvoiceNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "CN")
                                dref = rtable.Where(a => a.CreditNoteNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "DN")
                                dref = rtable.Where(a => a.DebitNoteNo == doc.DocumentNo).ToList();

                            if (dref != null && dref.Count > 0)
                            {
                                doc.DocumentRefNo = ",";
                                Document_Reference bref = dref.FirstOrDefault();
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo) && doc.DocumentType != "INV")
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo) && doc.DocumentType != "CA")
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo) && doc.DocumentType != "CN")
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo) && doc.DocumentType != "DN")
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                doc.DocumentRefNo = doc.DocumentRefNo + docno;
                                doc.DocumentRefNo = doc.DocumentRefNo.Replace(",,", "");

                                if (doc.DocumentType == "INV" || doc.DocumentType == "CN" || doc.DocumentType == "DN")
                                {
                                    if (!string.IsNullOrEmpty(doc.BillingNoteNo))
                                    {
                                        doc.DocumentRefNo = !string.IsNullOrEmpty(doc.DocumentRefNo) ? doc.DocumentRefNo : ",";
                                        doc.DocumentRefNo = doc.DocumentRefNo + "," + doc.BillingNoteNo;
                                    }
                                    if (!string.IsNullOrEmpty(doc.ReceiptNo))
                                    {
                                        doc.DocumentRefNo = !string.IsNullOrEmpty(doc.DocumentRefNo) ? doc.DocumentRefNo : ",";
                                        doc.DocumentRefNo = doc.DocumentRefNo + "," + doc.ReceiptNo;
                                    }

                                    doc.DocumentRefNo = doc.DocumentRefNo.Replace(",,", "");
                                }
                            }
                        }
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportBilling(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    List<Document_Billing> GetDocument = new List<Document_Billing>();
                    var biltable = context.Set<Document_Billing>();
                    var bildata = from bil in biltable
                                  where bil.BusinessID == input.businessid && bil.BillingDate >= input.startdate && bil.BillingDate <= input.enddate && bil.isdelete == isactive
                                  select new
                                  {
                                      bil.BusinessID,
                                      bil.BillingKey,
                                      bil.BillingNo,
                                      bil.BillingStatus,
                                      bil.BillingStatusName,
                                      bil.TotalAfterDiscountAmount,
                                      bil.CreditType,
                                      bil.CreditDay,
                                      bil.CustomerTaxID,
                                      bil.DueDate,
                                      bil.CustomerBranch,
                                      bil.SaleName,
                                      bil.VAT,
                                      bil.GrandAmount,
                                      bil.BillingDate,
                                      bil.BillingType,
                                      bil.ProjectName,
                                      bil.CustomerName
                                  };

                    foreach (var af in bildata.ToList().OrderByDescending(a => a.BillingKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.BillingKey,
                            DocumentNo = af.BillingNo,
                            DocumentType = af.BillingType,
                            DocumentDate = af.BillingDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DueDate = af.CreditType == "CA" ? af.BillingDate : af.CreditType == "CN" ? af.BillingDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            SaleName = af.SaleName,
                            DocumentStatus = af.BillingStatus,
                            DocumentStatusName = af.BillingStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount
                        });
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportReceipt(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var cashtable = context.Set<Document_CashSale>();
                    var cashdata = from bil in cashtable
                                   where bil.BusinessID == input.businessid && bil.CashSaleDate >= input.startdate && bil.CashSaleDate <= input.enddate && bil.isdelete == isactive
                                   select new { bil.BusinessID, bil.CashSaleKey, bil.CashSaleNo, bil.CashSaleStatus, bil.CashSaleStatusName, bil.TotalAfterDiscountAmount, bil.VAT, bil.GrandAmount, bil.CashSaleDate, bil.ProjectName, bil.CustomerName, bil.SaleName, bil.Remark };

                    var info = context.Set<Document_ReceiveInfo>();

                    foreach (var af in cashdata.ToList().OrderByDescending(a => a.CashSaleKey))
                    {
                        Document_ReceiveInfo val = new Document_ReceiveInfo();
                        val = info.Where(a => a.DocumentKey == af.CashSaleKey && a.DocumentType == "CA" && a.isdelete == isactive).FirstOrDefault();

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CashSaleKey,
                            DocumentNo = af.CashSaleNo,
                            DocumentType = "CA",
                            DocumentDate = af.CashSaleDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            SaleName = af.SaleName,
                            Remark = af.Remark,
                            DocumentStatus = af.CashSaleStatus,
                            DocumentStatusName = af.CashSaleStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            PaymentDate = val != null ? val.PaymentDate : default(DateTime?),
                            PaymentType = val != null ? val.PaymentTypeName : string.Empty,
                            PaymentDescription = val != null ? val.PaymentType == "TR" ? val.BankName : val.PaymentType == "CQ" ? "เลขที่ " + val.ChequeNo : string.Empty : string.Empty,
                            PaymentAmount = val != null ? val.NetPaymentAmount : default(decimal?),
                            PaymentDiscountType = val != null && val.IsDiscount == "1" ? val.DiscountTypeName : string.Empty,
                            PaymentDiscount = val != null && val.IsDiscount == "1" ? val.DiscountAmount.Value : default(decimal?),
                            TaxRate = val != null && val.IsWithholdingTax == "1" ? val.WithholdingKey == "0" ? "จำนวนเงิน" : val.WithholdingRate.Value.ToString() + "%" : string.Empty,
                            TaxAmount = val != null && val.IsWithholdingTax == "1" ? val.WithholdingAmount.Value : default(decimal?),
                            PaymentBalance = val != null ? val.Balance.Value : default(decimal?),
                            CauseType = val != null ? val.CauseTypeName : string.Empty
                        });

                    }


                    var invtable = context.Set<Document_Invoice>();
                    var invdata = from bil in invtable
                                  where bil.BusinessID == input.businessid && bil.InvoiceDate >= input.startdate && bil.InvoiceDate <= input.enddate && bil.isdelete == isactive && bil.InvoiceStatus != "99"
                                  select new { bil.BusinessID, bil.InvoiceKey, bil.InvoiceNo, bil.InvoiceStatus, bil.InvoiceStatusName, bil.TotalAfterDiscountAmount, bil.VAT, bil.GrandAmount, bil.InvoiceDate, bil.ProjectName, bil.CustomerName, bil.SaleName, bil.Remark };

                    foreach (var af in invdata.ToList().OrderByDescending(a => a.InvoiceKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.InvoiceKey,
                            DocumentNo = af.InvoiceNo,
                            DocumentType = "INV",
                            DocumentDate = af.InvoiceDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            SaleName = af.SaleName,
                            Remark = af.Remark,
                            DocumentStatus = af.InvoiceStatus,
                            DocumentStatusName = af.InvoiceStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });

                    var retable = context.Set<Document_Receipt>();
                    var redata = from bil in retable
                                 where bil.BusinessID == input.businessid && bil.ReceiptDate >= input.startdate && bil.ReceiptDate <= input.enddate && bil.isdelete == isactive
                                 select new { bil.BusinessID, bil.ReceiptKey, bil.ReceiptNo, bil.ReceiptStatus, bil.ReceiptStatusName, bil.TotalAfterDiscountAmount, bil.VAT, bil.GrandAmount, bil.ReceiptDate, bil.ProjectName, bil.CustomerName, bil.SaleName, bil.Remark, bil.ReceiptType };

                    foreach (var af in redata.ToList().OrderByDescending(a => a.ReceiptKey))
                    {
                        Document_ReceiveInfo val = new Document_ReceiveInfo();
                        val = info.Where(a => a.DocumentKey == af.ReceiptKey && a.DocumentType == "RE" && a.isdelete == isactive).FirstOrDefault();

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ReceiptKey,
                            DocumentNo = af.ReceiptNo,
                            DocumentDate = af.ReceiptDate,
                            DocumentStatus = af.ReceiptStatus,
                            DocumentStatusName = af.ReceiptStatusName,
                            DocumentType = af.ReceiptType,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            SaleName = af.SaleName,
                            Remark = af.Remark,
                            PaymentDate = val != null ? val.PaymentDate : default(DateTime?),
                            PaymentType = val != null ? val.PaymentTypeName : string.Empty,
                            PaymentDescription = val != null ? val.PaymentType == "TR" ? val.BankName : val.PaymentType == "CQ" ? "เลขที่ " + val.ChequeNo : string.Empty : string.Empty,
                            PaymentAmount = val != null ? val.NetPaymentAmount : default(decimal?),
                            PaymentDiscountType = val != null && val.IsDiscount == "1" ? val.DiscountTypeName : string.Empty,
                            PaymentDiscount = val != null && val.IsDiscount == "1" ? val.DiscountAmount.Value : default(decimal?),
                            TaxRate = val != null && val.IsWithholdingTax == "1" ? val.WithholdingKey == "0" ? "จำนวนเงิน" : val.WithholdingRate.Value.ToString() + "%" : string.Empty,
                            TaxAmount = val != null && val.IsWithholdingTax == "1" ? val.WithholdingAmount.Value : default(decimal?),
                            PaymentBalance = val != null ? val.Balance.Value : default(decimal?),
                            CauseType = val != null ? val.CauseTypeName : string.Empty
                        });
                    }

                    var debittable = context.Set<Document_DebitNote>();
                    var debitdata = from bil in debittable
                                    where bil.BusinessID == input.businessid && bil.DebitNoteDate >= input.startdate && bil.DebitNoteDate <= input.enddate && bil.isdelete == isactive && bil.DebitNoteStatus != "99"
                                    select new { bil.BusinessID, bil.DebitNoteKey, bil.DebitNoteNo, bil.DebitNoteStatus, bil.DebitNoteStatusName, bil.TotalAfterDiscountAmount, bil.VAT, bil.GrandAmount, bil.DebitNoteDate, bil.ProjectName, bil.CustomerName, bil.SaleName, bil.Remark };

                    foreach (var af in debitdata.ToList().OrderByDescending(a => a.DebitNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.DebitNoteKey,
                            DocumentNo = af.DebitNoteNo,
                            DocumentType = "DN",
                            DocumentDate = af.DebitNoteDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            SaleName = af.SaleName,
                            Remark = af.Remark,
                            DocumentStatus = af.DebitNoteStatus,
                            DocumentStatusName = af.DebitNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });


                    var credittable = context.Set<Document_CreditNote>();
                    var creditdata = from bil in credittable
                                     where bil.BusinessID == input.businessid && bil.CreditNoteDate >= input.startdate && bil.CreditNoteDate <= input.enddate && bil.isdelete == isactive && bil.CreditNoteStatus != "99"
                                     select new { bil.BusinessID, bil.CreditNoteKey, bil.CreditNoteNo, bil.CreditNoteStatus, bil.CreditNoteStatusName, bil.TotalAfterDiscountAmount, bil.VAT, bil.GrandAmount, bil.CreditNoteDate, bil.ProjectName, bil.CustomerName, bil.SaleName, bil.Remark };

                    foreach (var af in creditdata.ToList().OrderByDescending(a => a.CreditNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CreditNoteKey,
                            DocumentNo = af.CreditNoteNo,
                            DocumentType = "CN",
                            DocumentDate = af.CreditNoteDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            SaleName = af.SaleName,
                            Remark = af.Remark,
                            DocumentStatus = af.CreditNoteStatus,
                            DocumentStatusName = af.CreditNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });
                }


                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportSalesSummaryByCustomer(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var contact = context.Set<Contact>();
                    var cashtable = context.Set<Document_CashSale>();
                    var cashdata = from bil in cashtable
                                   where bil.BusinessID == input.businessid && bil.CashSaleDate >= input.startdate && bil.CashSaleDate <= input.enddate && bil.isdelete == isactive
                                   select new { bil.BusinessID, bil.CashSaleKey, bil.CashSaleNo, bil.CashSaleStatus, bil.CashSaleStatusName, bil.TotalAfterDiscountAmount, bil.VAT, bil.GrandAmount, bil.CashSaleDate, bil.ProjectName, bil.CustomerName, bil.CustomerKey };

                    var info = context.Set<Document_ReceiveInfo>();

                    foreach (var af in cashdata.ToList().OrderByDescending(a => a.CashSaleKey))
                    {
                        Contact cont = contact.Where(a => a.ContactKey == af.CustomerKey).FirstOrDefault();

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CashSaleKey,
                            DocumentNo = af.CashSaleNo,
                            DocumentType = "CA",
                            DocumentDate = af.CashSaleDate,
                            DueDate = af.CashSaleDate.Value.AddDays(cont.CreditDate.HasValue ? cont.CreditDate.Value : 0),
                            CustomerCode = cont.ContactCode,
                            CustomerKey = af.CustomerKey,
                            DocumentStatus = af.CashSaleStatus,
                            DocumentStatusName = af.CashSaleStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount
                        });

                    }


                    var invtable = context.Set<Document_Invoice>();
                    var invdata = from bil in invtable
                                  where bil.BusinessID == input.businessid && bil.InvoiceDate >= input.startdate && bil.InvoiceDate <= input.enddate && bil.isdelete == isactive
                                  select new
                                  {
                                      bil.BusinessID,
                                      bil.InvoiceKey,
                                      bil.InvoiceNo,
                                      bil.InvoiceStatus,
                                      bil.InvoiceStatusName,
                                      bil.TotalAfterDiscountAmount,
                                      bil.VAT,
                                      bil.GrandAmount,
                                      bil.InvoiceDate,
                                      bil.CustomerName,
                                      bil.CustomerKey,
                                      bil.DueDate,
                                      bil.CreditType,
                                      bil.CreditDay
                                  };

                    foreach (var af in invdata.ToList().OrderByDescending(a => a.InvoiceKey))
                    {
                        Contact cont = contact.Where(a => a.ContactKey == af.CustomerKey).FirstOrDefault();

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.InvoiceKey,
                            DocumentNo = af.InvoiceNo,
                            DocumentType = "INV",
                            DocumentDate = af.InvoiceDate,
                            CreditType = af.CreditType,
                            CreditDay = af.CreditDay,
                            DueDate = af.CreditType == "CA" ? af.InvoiceDate : af.CreditType == "CN" ? af.InvoiceDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerCode = cont.ContactCode,
                            CustomerName = af.CustomerName,
                            DocumentStatus = af.InvoiceStatus,
                            DocumentStatusName = af.InvoiceStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });
                    }

                    var debittable = context.Set<Document_DebitNote>();
                    var debitdata = from bil in debittable
                                    where bil.BusinessID == input.businessid && bil.DebitNoteDate >= input.startdate && bil.DebitNoteDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.DebitNoteKey,
                                        bil.DebitNoteNo,
                                        bil.DebitNoteStatus,
                                        bil.DebitNoteStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.DebitNoteDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.BillingNoteNo,
                                        bil.ReceiptNo,
                                        bil.CustomerKey,
                                        bil.DueDate,
                                        bil.CreditType,
                                        bil.CreditDay
                                    };

                    foreach (var af in debitdata.ToList().OrderByDescending(a => a.DebitNoteKey))
                    {
                        Contact cont = contact.Where(a => a.ContactKey == af.CustomerKey).FirstOrDefault();
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.DebitNoteKey,
                            DocumentNo = af.DebitNoteNo,
                            DocumentType = "DN",
                            DocumentDate = af.DebitNoteDate,
                            CreditType = af.CreditType,
                            CreditDay = af.CreditDay,
                            DueDate = af.CreditType == "CA" ? af.DebitNoteDate : af.CreditType == "CN" ? af.DebitNoteDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerCode = cont.ContactCode,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.DebitNoteStatus,
                            DocumentStatusName = af.DebitNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReceiptNo = af.ReceiptNo
                        });
                    }

                    var credittable = context.Set<Document_CreditNote>();
                    var creditdata = from bil in credittable
                                     where bil.BusinessID == input.businessid && bil.CreditNoteDate >= input.startdate && bil.CreditNoteDate <= input.enddate && bil.isdelete == isactive
                                     select new
                                     {
                                         bil.BusinessID,
                                         bil.CreditNoteKey,
                                         bil.CreditNoteNo,
                                         bil.CreditNoteStatus,
                                         bil.CreditNoteStatusName,
                                         bil.TotalAfterDiscountAmount,
                                         bil.VAT,
                                         bil.GrandAmount,
                                         bil.CreditNoteDate,
                                         bil.ProjectName,
                                         bil.CustomerName,
                                         bil.BillingNoteNo,
                                         bil.ReceiptNo,
                                         bil.CustomerKey,
                                         bil.DueDate,
                                         bil.CreditType,
                                         bil.CreditDay
                                     };

                    foreach (var af in creditdata.ToList().OrderByDescending(a => a.CreditNoteKey))
                    {
                        Contact cont = contact.Where(a => a.ContactKey == af.CustomerKey).FirstOrDefault();
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CreditNoteKey,
                            DocumentNo = af.CreditNoteNo,
                            DocumentType = "CN",
                            DocumentDate = af.CreditNoteDate,
                            CreditType = af.CreditType,
                            CreditDay = af.CreditDay,
                            DueDate = af.CreditType == "CA" ? af.CreditNoteDate : af.CreditType == "CN" ? af.CreditNoteDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerCode = cont.ContactCode,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.CreditNoteStatus,
                            DocumentStatusName = af.CreditNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReceiptNo = af.ReceiptNo
                        });
                    }

                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportPurchase(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    List<Document_PurchaseOrder> GetDocument = new List<Document_PurchaseOrder>();
                    var purtable = context.Set<Document_PurchaseOrder>();
                    var purdata = from bil in purtable
                                  where bil.BusinessID == input.businessid && bil.PurchaseOrderDate >= input.startdate && bil.PurchaseOrderDate <= input.enddate && bil.isdelete == isactive
                                  select new
                                  {
                                      bil.BusinessID,
                                      bil.PurchaseOrderKey,
                                      bil.PurchaseOrderNo,
                                      bil.PurchaseOrderStatus,
                                      bil.PurchaseOrderStatusName,
                                      bil.TotalAfterDiscountAmount,
                                      bil.VAT,
                                      bil.GrandAmount,
                                      bil.PurchaseOrderDate,
                                      bil.ProjectName,
                                      bil.CustomerName,
                                      bil.CustomerTaxID,
                                      bil.CustomerBranch,
                                      bil.ReferenceNo
                                  };

                    foreach (var af in purdata.ToList().OrderByDescending(a => a.PurchaseOrderKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.PurchaseOrderKey,
                            DocumentNo = af.PurchaseOrderNo,
                            DocumentDate = af.PurchaseOrderDate,
                            DocumentType = "PO",
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.PurchaseOrderStatus,
                            DocumentStatusName = af.PurchaseOrderStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount

                        });
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportReceivingInventory(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var receitable = context.Set<Document_ReceiveInventory>();
                    var receidata = from bil in receitable
                                    where bil.BusinessID == input.businessid && bil.ReceiveInventoryDate >= input.startdate && bil.ReceiveInventoryDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.ReceiveInventoryKey,
                                        bil.ReceiveInventoryNo,
                                        bil.ReceiveInventoryStatus,
                                        bil.ReceiveInventoryStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.ReceiveInventoryDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.CustomerTaxID,
                                        bil.CustomerBranch,
                                        bil.ReferenceNo
                                    };

                    var info = context.Set<Document_ReceiveInfo>();

                    foreach (var af in receidata.ToList().OrderByDescending(a => a.ReceiveInventoryKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ReceiveInventoryKey,
                            DocumentNo = af.ReceiveInventoryNo,
                            DocumentType = "RI",
                            DocumentDate = af.ReceiveInventoryDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            ReferenceNo = af.ReferenceNo,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.ReceiveInventoryStatus,
                            DocumentStatusName = af.ReceiveInventoryStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });

                    }


                    if (response != null && response.Count > 0)
                    {
                        var rtable = context.Set<Document_Reference>();
                        foreach (DocumentReport doc in response)
                        {
                            doc.DocumentRefNo = string.Empty;
                            List<Document_Reference> dref = new List<Document_Reference>();

                            if (doc.DocumentType == "RI")
                                dref = rtable.Where(a => a.ReceivingInventoryNo == doc.DocumentNo).ToList();

                            if (dref != null && dref.Count > 0)
                            {
                                doc.DocumentRefNo = ",";
                                Document_Reference bref = dref.FirstOrDefault();
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo) && doc.DocumentType != "INV")
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo) && doc.DocumentType != "CA")
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo) && doc.DocumentType != "CN")
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo) && doc.DocumentType != "DN")
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo) && doc.DocumentType != "RI")
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                doc.DocumentRefNo = doc.DocumentRefNo + docno;
                                doc.DocumentRefNo = doc.DocumentRefNo.Replace(",,", "");
                            }
                        }
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportExpense(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var receitable = context.Set<Document_ReceiveInventory>();
                    var receidata = from bil in receitable
                                    where bil.BusinessID == input.businessid && bil.ReceiveInventoryDate >= input.startdate && bil.ReceiveInventoryDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.ReceiveInventoryKey,
                                        bil.ReceiveInventoryNo,
                                        bil.ReceiveInventoryStatus,
                                        bil.ReceiveInventoryStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.ReceiveInventoryDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.CustomerTaxID,
                                        bil.CustomerBranch,
                                        bil.ReferenceNo,
                                        bil.Remark,
                                        bil.Noted
                                    };

                    foreach (var af in receidata.ToList().OrderByDescending(a => a.ReceiveInventoryKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ReceiveInventoryKey,
                            DocumentNo = af.ReceiveInventoryNo,
                            DocumentType = "RI",
                            DocumentDate = af.ReceiveInventoryDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            Remark = af.Remark,
                            Noted = af.Noted,
                            DocumentStatus = af.ReceiveInventoryStatus,
                            DocumentStatusName = af.ReceiveInventoryStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });

                    }

                    var expentable = context.Set<Document_Expenses>();
                    var expendata = from bil in expentable
                                    where bil.BusinessID == input.businessid && bil.ExpensesDate >= input.startdate && bil.ExpensesDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.ExpensesKey,
                                        bil.ExpensesNo,
                                        bil.ExpensesStatus,
                                        bil.ExpensesStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.ExpensesDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.CustomerTaxID,
                                        bil.CustomerBranch,
                                        bil.ReferenceNo,
                                        bil.Remark,
                                        bil.Noted
                                    };

                    var detail = context.Set<Document_ExpensesDetail>();

                    foreach (var af in expendata.ToList().OrderByDescending(a => a.ExpensesKey))
                    {
                        Document_ExpensesDetail val = detail.Where(a => a.ExpensesKey == af.ExpensesKey).FirstOrDefault();

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ExpensesKey,
                            DocumentNo = af.ExpensesNo,
                            DocumentType = "EX",
                            DocumentDate = af.ExpensesDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            Remark = af.Remark,
                            Noted = af.Noted,
                            CategoryName = val != null ? val.CatagoryName : string.Empty,
                            DocumentStatus = af.ExpensesStatus,
                            DocumentStatusName = af.ExpensesStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });

                    }


                    if (response != null && response.Count > 0)
                    {
                        var rtable = context.Set<Document_Reference>();
                        foreach (DocumentReport doc in response)
                        {
                            doc.DocumentRefNo = string.Empty;
                            List<Document_Reference> dref = new List<Document_Reference>();

                            if (doc.DocumentType == "RI")
                                dref = rtable.Where(a => a.ReceivingInventoryNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "EX")
                                dref = rtable.Where(a => a.ExpenseNo == doc.DocumentNo).ToList();

                            if (dref != null && dref.Count > 0)
                            {
                                doc.DocumentRefNo = ",";
                                Document_Reference bref = dref.FirstOrDefault();
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo) && doc.DocumentType != "INV")
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo) && doc.DocumentType != "CA")
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo) && doc.DocumentType != "CN")
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo) && doc.DocumentType != "DN")
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo) && doc.DocumentType != "RI")
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo) && doc.DocumentType != "EX")
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                doc.DocumentRefNo = doc.DocumentRefNo + docno;
                                doc.DocumentRefNo = doc.DocumentRefNo.Replace(",,", "");
                            }
                        }
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportSalesTax(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var cashtable = context.Set<Document_CashSale>();
                    var cashdata = from bil in cashtable
                                   where bil.BusinessID == input.businessid && bil.CashSaleDate >= input.startdate && bil.CashSaleDate <= input.enddate && bil.isdelete == isactive
                                   select new
                                   {
                                       bil.BusinessID,
                                       bil.CashSaleKey,
                                       bil.CashSaleNo,
                                       bil.CashSaleStatus,
                                       bil.CashSaleStatusName,
                                       bil.TotalAfterDiscountAmount,
                                       bil.VAT,
                                       bil.GrandAmount,
                                       bil.CashSaleDate,
                                       bil.ProjectName,
                                       bil.CustomerName,
                                       bil.CustomerBranch,
                                       bil.CustomerTaxID,
                                       bil.ReferenceNo
                                   };

                    var info = context.Set<Document_ReceiveInfo>();

                    foreach (var af in cashdata.ToList().OrderBy(a => a.CashSaleKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CashSaleKey,
                            DocumentNo = af.CashSaleNo,
                            DocumentType = "CA",
                            DocumentDate = af.CashSaleDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.CashSaleStatus,
                            DocumentStatusName = af.CashSaleStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            ReferenceNo = af.ReferenceNo
                        });

                    }


                    var invtable = context.Set<Document_Invoice>();
                    var invdata = from bil in invtable
                                  where bil.BusinessID == input.businessid && bil.InvoiceDate >= input.startdate && bil.InvoiceDate <= input.enddate && bil.isdelete == isactive
                                  select new
                                  {
                                      bil.BusinessID,
                                      bil.InvoiceKey,
                                      bil.InvoiceNo,
                                      bil.InvoiceStatus,
                                      bil.InvoiceStatusName,
                                      bil.TotalAfterDiscountAmount,
                                      bil.VAT,
                                      bil.GrandAmount,
                                      bil.InvoiceDate,
                                      bil.ProjectName,
                                      bil.CustomerName,
                                      bil.CustomerBranch,
                                      bil.CustomerTaxID,
                                      bil.ReferenceNo
                                  };

                    foreach (var af in invdata.ToList().OrderBy(a => a.InvoiceKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.InvoiceKey,
                            DocumentNo = af.InvoiceNo,
                            DocumentType = "INV",
                            DocumentDate = af.InvoiceDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.InvoiceStatus,
                            DocumentStatusName = af.InvoiceStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            ReferenceNo = af.ReferenceNo
                        });

                    var debittable = context.Set<Document_DebitNote>();
                    var debitdata = from bil in debittable
                                    where bil.BusinessID == input.businessid && bil.DebitNoteDate >= input.startdate && bil.DebitNoteDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.DebitNoteKey,
                                        bil.DebitNoteNo,
                                        bil.DebitNoteStatus,
                                        bil.DebitNoteStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.DebitNoteDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.CustomerBranch,
                                        bil.CustomerTaxID,
                                        bil.ReferenceNo
                                    };

                    foreach (var af in debitdata.ToList().OrderBy(a => a.DebitNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.DebitNoteKey,
                            DocumentNo = af.DebitNoteNo,
                            DocumentType = "DN",
                            DocumentDate = af.DebitNoteDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.DebitNoteStatus,
                            DocumentStatusName = af.DebitNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            ReferenceNo = af.ReferenceNo
                        });


                    var credittable = context.Set<Document_CreditNote>();
                    var creditdata = from bil in credittable
                                     where bil.BusinessID == input.businessid && bil.CreditNoteDate >= input.startdate && bil.CreditNoteDate <= input.enddate && bil.isdelete == isactive
                                     select new
                                     {
                                         bil.BusinessID,
                                         bil.CreditNoteKey,
                                         bil.CreditNoteNo,
                                         bil.CreditNoteStatus,
                                         bil.CreditNoteStatusName,
                                         bil.TotalAfterDiscountAmount,
                                         bil.VAT,
                                         bil.GrandAmount,
                                         bil.CreditNoteDate,
                                         bil.ProjectName,
                                         bil.CustomerName,
                                         bil.CustomerBranch,
                                         bil.CustomerTaxID,
                                         bil.ReferenceNo
                                     };

                    foreach (var af in creditdata.ToList().OrderBy(a => a.CreditNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CreditNoteKey,
                            DocumentNo = af.CreditNoteNo,
                            DocumentType = "CN",
                            DocumentDate = af.CreditNoteDate,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.CreditNoteStatus,
                            DocumentStatusName = af.CreditNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            ReferenceNo = af.ReferenceNo
                        });

                    if (response != null && response.Count > 0)
                    {
                        var rtable = context.Set<Document_Reference>();
                        foreach (DocumentReport doc in response)
                        {
                            doc.DocumentRefNo = string.Empty;
                            List<Document_Reference> dref = new List<Document_Reference>();

                            if (doc.DocumentType == "CA")
                                dref = rtable.Where(a => a.CashSaleNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "INV")
                                dref = rtable.Where(a => a.InvoiceNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "CN")
                                dref = rtable.Where(a => a.CreditNoteNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "DN")
                                dref = rtable.Where(a => a.DebitNoteNo == doc.DocumentNo).ToList();

                            if (dref != null && dref.Count > 0)
                            {
                                doc.DocumentRefNo = ",";
                                Document_Reference bref = dref.FirstOrDefault();
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo) && doc.DocumentType != "INV")
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo) && doc.DocumentType != "CA")
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo) && doc.DocumentType != "CN")
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo) && doc.DocumentType != "DN")
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                doc.DocumentRefNo = doc.DocumentRefNo + docno;
                                doc.DocumentRefNo = doc.DocumentRefNo.Replace(",,", "");
                            }
                        }
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportPurchaseVat(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var receitable = context.Set<Document_ReceiveInventory>();
                    var receidata = from bil in receitable
                                    where bil.BusinessID == input.businessid && bil.ReceiveInventoryDate >= input.startdate && bil.ReceiveInventoryDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.ReceiveInventoryKey,
                                        bil.ReceiveInventoryNo,
                                        bil.ReceiveInventoryStatus,
                                        bil.ReceiveInventoryStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.ReceiveInventoryDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.CustomerBranch,
                                        bil.CustomerTaxID,
                                        bil.ReferenceNo
                                    };

                    foreach (var af in receidata.ToList().OrderBy(a => a.ReceiveInventoryKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ReceiveInventoryKey,
                            DocumentNo = af.ReceiveInventoryNo,
                            DocumentType = "RI",
                            DocumentDate = af.ReceiveInventoryDate,
                            ReferenceNo = af.ReferenceNo,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.ReceiveInventoryStatus,
                            DocumentStatusName = af.ReceiveInventoryStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });

                    }

                    var expentable = context.Set<Document_Expenses>();
                    var expendata = from bil in expentable
                                    where bil.BusinessID == input.businessid && bil.ExpensesDate >= input.startdate && bil.ExpensesDate <= input.enddate && bil.isdelete == isactive
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.ExpensesKey,
                                        bil.ExpensesNo,
                                        bil.ExpensesStatus,
                                        bil.ExpensesStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.ExpensesDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.CustomerBranch,
                                        bil.CustomerTaxID,
                                        bil.ReferenceNo
                                    };

                    var detail = context.Set<Document_ExpensesDetail>();

                    foreach (var af in expendata.ToList().OrderBy(a => a.ExpensesKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ExpensesKey,
                            DocumentNo = af.ExpensesNo,
                            DocumentType = "EX",
                            DocumentDate = af.ExpensesDate,
                            ReferenceNo = af.ReferenceNo,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.ExpensesStatus,
                            DocumentStatusName = af.ExpensesStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                        });

                    }


                    if (response != null && response.Count > 0)
                    {
                        var rtable = context.Set<Document_Reference>();
                        foreach (DocumentReport doc in response)
                        {
                            doc.DocumentRefNo = string.Empty;
                            List<Document_Reference> dref = new List<Document_Reference>();

                            if (doc.DocumentType == "RI")
                                dref = rtable.Where(a => a.ReceivingInventoryNo == doc.DocumentNo).ToList();
                            else if (doc.DocumentType == "EX")
                                dref = rtable.Where(a => a.ExpenseNo == doc.DocumentNo).ToList();

                            if (dref != null && dref.Count > 0)
                            {
                                doc.DocumentRefNo = ",";
                                Document_Reference bref = dref.FirstOrDefault();
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo) && doc.DocumentType != "INV")
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo) && doc.DocumentType != "CA")
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo) && doc.DocumentType != "CN")
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo) && doc.DocumentType != "DN")
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo) && doc.DocumentType != "RI")
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo) && doc.DocumentType != "EX")
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                doc.DocumentRefNo = doc.DocumentRefNo + docno;
                            }
                            doc.DocumentRefNo = doc.DocumentRefNo.Replace(",,", "");
                        }
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportWithholdingTax(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var withtable = context.Set<Document_WithholdingTax>();
                    var withdata = from bil in withtable
                                   where bil.BusinessID == input.businessid && bil.WithholdingTaxDate >= input.startdate && bil.WithholdingTaxDate <= input.enddate && bil.isdelete == isactive
                                   select new
                                   {
                                       bil.BusinessID,
                                       bil.WithholdingTaxKey,
                                       bil.WithholdingTaxNo,
                                       bil.WithholdingTaxStatus,
                                       bil.WithholdingTaxStatusName,
                                       bil.ExemptAmount,
                                       bil.VatableAmount,
                                       bil.WithholdingTaxDate,
                                       bil.WithholdingTaxFormName,
                                       bil.ProjectName,
                                       bil.CustomerName,
                                       bil.ReferenceNo,
                                       bil.CustomerTaxID,
                                       bil.CustomerAddress,
                                       bil.CustomerBranch
                                   };

                    foreach (var af in withdata.ToList().OrderBy(a => a.WithholdingTaxKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.WithholdingTaxKey,
                            DocumentNo = af.WithholdingTaxNo,
                            DocumentType = "WT",
                            DocumentDate = af.WithholdingTaxDate,
                            WithHoldingTaxFormName = af.WithholdingTaxFormName,
                            ReferenceNo = af.ReferenceNo,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            TaxID = af.CustomerTaxID,
                            SaleName = af.CustomerAddress,
                            BranchName = af.CustomerBranch,
                            DocumentStatus = af.WithholdingTaxStatus,
                            DocumentStatusName = af.WithholdingTaxStatusName,
                            Total = af.ExemptAmount,
                            VAT = af.VatableAmount,
                        });

                    }

                    if (response != null && response.Count > 0)
                    {
                        var rtable = context.Set<Document_WithholdingTaxDetail>();
                        foreach (DocumentReport doc in response)
                        {
                            doc.WithHoldingRateDesc = string.Empty;
                            List<Document_WithholdingTaxDetail> dref = new List<Document_WithholdingTaxDetail>();
                            dref = rtable.Where(a => a.WithholdingTaxKey == doc.DocumentKey).OrderBy(a => a.Sequence).ToList();

                            if (dref != null && dref.Count > 0)
                            {
                                doc.WithHoldingRateDesc = ",";
                                doc.CategoryName = ",";
                                foreach (Document_WithholdingTaxDetail bref in dref)
                                {
                                    string docno = string.Empty;
                                    string categoryname = string.Empty;
                                    docno = docno + ", " + (bref.TaxPercentType == "0" ? bref.PriceTax.Value.ToString("n2") : bref.TaxPercentType + "%");
                                    categoryname = categoryname + ", " + bref.WithholdingTaxCatagoryName;

                                    doc.WithHoldingRateDesc = doc.WithHoldingRateDesc + docno;
                                    doc.CategoryName = doc.CategoryName + categoryname;
                                }
                                doc.WithHoldingRateDesc = doc.WithHoldingRateDesc.Replace(",,", "");
                                doc.CategoryName = doc.CategoryName.Replace(",,", "");
                                doc.CategoryName = doc.CategoryName.Replace(",", "," + System.Environment.NewLine);
                            }
                        }
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportAccountreceivable(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    List<Document_Billing> GetDocument = new List<Document_Billing>();
                    var biltable = context.Set<Document_Billing>();
                    var bildata = from bil in biltable
                                  where bil.BusinessID == input.businessid && bil.BillingDate <= input.startdate && bil.isdelete == isactive && (bil.BillingStatus == "1" || bil.BillingStatus == "2") && bil.BillingType == "BL"
                                  select new { bil.BusinessID, bil.BillingKey, bil.BillingNo, bil.BillingStatus, bil.BillingStatusName, bil.VAT, bil.GrandAmount, bil.BillingDate, bil.ProjectName, bil.CustomerName, bil.CustomerKey, bil.DueDate, bil.CreditType, bil.CreditDay ,bil.SaleName , bil.Remark };

                    foreach (var af in bildata.ToList().OrderBy(a => a.BillingKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.BillingKey,
                            DocumentNo = af.BillingNo,
                            DocumentType = "BL",
                            DocumentDate = af.BillingDate,
                            CustomerName = af.CustomerName,
                            DueDate = af.CreditType == "CA" ? af.BillingDate : af.CreditType == "CN" ? af.BillingDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.BillingStatus,
                            DocumentStatusName = af.BillingStatusName,
                            GrandAmount = af.GrandAmount,
                            SaleName = af.SaleName,
                             Remark = af.Remark

                        });
                    }

                    var contact = context.Set<Contact>();
                    var cashtable = context.Set<Document_CashSale>();
                    var cashdata = from bil in cashtable
                                   where bil.BusinessID == input.businessid && bil.CashSaleDate <= input.startdate && bil.isdelete == isactive && bil.CashSaleStatus == "1"
                                   select new { bil.BusinessID, bil.CashSaleKey, bil.CashSaleNo, bil.CashSaleStatus, bil.CashSaleStatusName, bil.TotalAfterDiscountAmount, bil.VAT, bil.GrandAmount, bil.CashSaleDate, bil.ProjectName, bil.CustomerName, bil.CustomerKey, bil.SaleName, bil.Remark };

                    foreach (var af in cashdata.ToList().OrderBy(a => a.CashSaleKey))
                    {
                        Contact cont = contact.Where(a => a.ContactKey == af.CustomerKey).FirstOrDefault();

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CashSaleKey,
                            DocumentNo = af.CashSaleNo,
                            DocumentType = "CA",
                            DocumentDate = af.CashSaleDate,
                            DueDate = af.CashSaleDate.Value.AddDays(cont.CreditDate.HasValue ? cont.CreditDate.Value : 0),
                            CustomerKey = af.CustomerKey,
                            CustomerName = af.CustomerName,
                            DocumentStatus = af.CashSaleStatus,
                            DocumentStatusName = af.CashSaleStatusName,
                            GrandAmount = af.GrandAmount,
                            SaleName = af.SaleName,
                            Remark = af.Remark
                        });

                    }


                    var invtable = context.Set<Document_Invoice>();
                    var invdata = from bil in invtable
                                  where bil.BusinessID == input.businessid && bil.InvoiceDate <= input.startdate && bil.isdelete == isactive && (bil.InvoiceStatus == "1" || bil.InvoiceStatus == "2")
                                  select new
                                  {
                                      bil.BusinessID,
                                      bil.InvoiceKey,
                                      bil.InvoiceNo,
                                      bil.InvoiceStatus,
                                      bil.InvoiceStatusName,
                                      bil.TotalAfterDiscountAmount,
                                      bil.VAT,
                                      bil.GrandAmount,
                                      bil.InvoiceDate,
                                      bil.ProjectName,
                                      bil.CustomerName,
                                      bil.BillingNoteNo,
                                      bil.ReceiptNo,
                                      bil.CustomerKey,
                                      bil.DueDate,
                                      bil.CreditType,
                                      bil.CreditDay,
                                      bil.SaleName,
                                      bil.Remark
                                  };

                    foreach (var af in invdata.ToList().OrderBy(a => a.InvoiceKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.InvoiceKey,
                            DocumentNo = af.InvoiceNo,
                            DocumentType = "INV",
                            DocumentDate = af.InvoiceDate,
                            CreditType = af.CreditType,
                            CreditDay = af.CreditDay,
                            DueDate = af.CreditType == "CA" ? af.InvoiceDate : af.CreditType == "CN" ? af.InvoiceDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.InvoiceStatus,
                            DocumentStatusName = af.InvoiceStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReceiptNo = af.ReceiptNo,
                            SaleName = af.SaleName,
                            Remark = af.Remark
                        });

                    var retable = context.Set<Document_Receipt>();
                    var redata = from bil in retable
                                 where bil.BusinessID == input.businessid && bil.ReceiptDate <= input.startdate && bil.isdelete == isactive && (bil.ReceiptStatus == "1")
                                 select new
                                 {
                                     bil.BusinessID,
                                     bil.ReceiptKey,
                                     bil.ReceiptNo,
                                     bil.ReceiptStatus,
                                     bil.ReceiptStatusName,
                                     bil.TotalAfterDiscountAmount,
                                     bil.VAT,
                                     bil.GrandAmount,
                                     bil.ReceiptDate,
                                     bil.ProjectName,
                                     bil.CustomerName,
                                     bil.BillingNoteCumulativeNo,
                                     bil.ReceiptType,
                                     bil.CustomerKey,
                                     bil.SaleName,
                                     bil.Remark
                                 };

                    foreach (var af in redata.ToList().OrderByDescending(a => a.ReceiptKey))
                    {

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ReceiptKey,
                            DocumentNo = af.ReceiptNo,
                            DocumentType = af.ReceiptType,
                            DocumentDate = af.ReceiptDate,
                            DueDate = af.ReceiptDate,
                            CustomerKey = af.CustomerKey,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.ReceiptStatus,
                            DocumentStatusName = af.ReceiptStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteCumulativeNo,
                            SaleName = af.SaleName,
                            Remark = af.Remark
                        });
                    }

                    var debittable = context.Set<Document_DebitNote>();
                    var debitdata = from bil in debittable
                                    where bil.BusinessID == input.businessid && bil.DebitNoteDate <= input.startdate && bil.isdelete == isactive && (bil.DebitNoteStatus == "1" || bil.DebitNoteStatus == "2")
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.DebitNoteKey,
                                        bil.DebitNoteNo,
                                        bil.DebitNoteStatus,
                                        bil.DebitNoteStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.DebitNoteDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.BillingNoteNo,
                                        bil.ReceiptNo,
                                        bil.CustomerKey,
                                        bil.DueDate,
                                        bil.CreditType,
                                        bil.CreditDay,
                                        bil.SaleName,
                                        bil.Remark
                                    };

                    foreach (var af in debitdata.ToList().OrderBy(a => a.DebitNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.DebitNoteKey,
                            DocumentNo = af.DebitNoteNo,
                            DocumentType = "DN",
                            DocumentDate = af.DebitNoteDate,
                            CreditType = af.CreditType,
                            CreditDay = af.CreditDay,
                            DueDate = af.CreditType == "CA" ? af.DebitNoteDate : af.CreditType == "CN" ? af.DebitNoteDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.DebitNoteStatus,
                            DocumentStatusName = af.DebitNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReceiptNo = af.ReceiptNo,
                            SaleName = af.SaleName,
                            Remark = af.Remark
                        });


                    var credittable = context.Set<Document_CreditNote>();
                    var creditdata = from bil in credittable
                                     where bil.BusinessID == input.businessid && bil.CreditNoteDate <= input.startdate && bil.isdelete == isactive && (bil.CreditNoteStatus == "1" || bil.CreditNoteStatus == "2")
                                     select new
                                     {
                                         bil.BusinessID,
                                         bil.CreditNoteKey,
                                         bil.CreditNoteNo,
                                         bil.CreditNoteStatus,
                                         bil.CreditNoteStatusName,
                                         bil.TotalAfterDiscountAmount,
                                         bil.VAT,
                                         bil.GrandAmount,
                                         bil.CreditNoteDate,
                                         bil.ProjectName,
                                         bil.CustomerName,
                                         bil.BillingNoteNo,
                                         bil.ReceiptNo,
                                         bil.CustomerKey,
                                         bil.DueDate,
                                         bil.CreditType,
                                         bil.CreditDay,
                                         bil.SaleName,
                                         bil.Remark
                                     };

                    foreach (var af in creditdata.ToList().OrderBy(a => a.CreditNoteKey))
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.CreditNoteKey,
                            DocumentNo = af.CreditNoteNo,
                            DocumentType = "CN",
                            DocumentDate = af.CreditNoteDate,
                            CreditType = af.CreditType,
                            CreditDay = af.CreditDay,
                            DueDate = af.CreditType == "CA" ? af.CreditNoteDate : af.CreditType == "CN" ? af.CreditNoteDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.CreditNoteStatus,
                            DocumentStatusName = af.CreditNoteStatusName,
                            Total = af.TotalAfterDiscountAmount,
                            VAT = af.VAT,
                            GrandAmount = af.GrandAmount,
                            BillingNoteNo = af.BillingNoteNo,
                            ReceiptNo = af.ReceiptNo,
                            SaleName = af.SaleName,
                            Remark = af.Remark
                        });
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DocumentReport> GetReportAccountpayable(GetReportWithDate input)
        {
            try
            {
                List<DocumentReport> response = new List<DocumentReport>();
                using (AccountingContext context = new AccountingContext())
                {
                    var receitable = context.Set<Document_ReceiveInventory>();
                    var receidata = from bil in receitable
                                    where bil.BusinessID == input.businessid && bil.ReceiveInventoryDate <= input.startdate && bil.isdelete == isactive && (bil.ReceiveInventoryStatus == "1" || bil.ReceiveInventoryStatus == "2")
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.ReceiveInventoryKey,
                                        bil.ReceiveInventoryNo,
                                        bil.ReceiveInventoryStatus,
                                        bil.CustomerKey,
                                        bil.ReceiveInventoryStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.ReceiveInventoryDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.DueDate,
                                        bil.CreditType,
                                        bil.CreditDay,
                                        bil.SaleName,
                                        bil.Remark
                                    };

                    foreach (var af in receidata.ToList().OrderByDescending(a => a.ReceiveInventoryKey))
                    {
                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ReceiveInventoryKey,
                            DocumentNo = af.ReceiveInventoryNo,
                            DocumentType = "RI",
                            DocumentDate = af.ReceiveInventoryDate,
                            DueDate = af.CreditType == "CA" ? af.ReceiveInventoryDate : af.CreditType == "CN" ? af.ReceiveInventoryDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.ReceiveInventoryStatus,
                            DocumentStatusName = af.ReceiveInventoryStatusName,
                            GrandAmount = af.GrandAmount,
                            SaleName = af.SaleName,
                            Remark = af.Remark
                        });

                    }

                    var expentable = context.Set<Document_Expenses>();
                    var expendata = from bil in expentable
                                    where bil.BusinessID == input.businessid && bil.ExpensesDate <= input.startdate && bil.isdelete == isactive && (bil.ExpensesStatus == "1" || bil.ExpensesStatus == "2")
                                    select new
                                    {
                                        bil.BusinessID,
                                        bil.ExpensesKey,
                                        bil.ExpensesNo,
                                        bil.ExpensesStatus,
                                        bil.ExpensesStatusName,
                                        bil.TotalAfterDiscountAmount,
                                        bil.VAT,
                                        bil.GrandAmount,
                                        bil.CustomerKey,
                                        bil.ExpensesDate,
                                        bil.ProjectName,
                                        bil.CustomerName,
                                        bil.DueDate,
                                        bil.CreditType,
                                        bil.CreditDay,
                                        bil.SaleName,
                                        bil.Remark
                                    };

                    var detail = context.Set<Document_ExpensesDetail>();

                    foreach (var af in expendata.ToList().OrderByDescending(a => a.ExpensesKey))
                    {
                        Document_ExpensesDetail val = detail.Where(a => a.ExpensesKey == af.ExpensesKey).FirstOrDefault();

                        response.Add(new DocumentReport()
                        {
                            DocumentKey = af.ExpensesKey,
                            DocumentNo = af.ExpensesNo,
                            DocumentType = "EX",
                            DocumentDate = af.ExpensesDate,
                            DueDate = af.CreditType == "CA" ? af.ExpensesDate : af.CreditType == "CN" ? af.ExpensesDate.Value.AddDays(af.CreditDay.HasValue ? af.CreditDay.Value : 0) : af.DueDate,
                            CustomerKey = af.CustomerKey,
                            CustomerName = af.CustomerName,
                            ProjectName = af.ProjectName,
                            DocumentStatus = af.ExpensesStatus,
                            DocumentStatusName = af.ExpensesStatusName,
                            GrandAmount = af.GrandAmount,
                            SaleName = af.SaleName,
                            Remark = af.Remark
                        });

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
