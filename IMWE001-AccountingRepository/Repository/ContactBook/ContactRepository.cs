﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.ContactBook
{
    public class ContactRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Contact, AccountingContext>
    {
        public Contact Save(Contact input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Contact>();
                        if (!input.ContactKey.HasValue)
                        {
                            input.isdelete = 1;
                            otable.Add(input);
                            context.SaveChanges();
                        }
                        else
                        {
                            Contact val = context.Set<Contact>().Where(a => a.BusinessID == input.BusinessID && a.ContactKey == input.ContactKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลผู้ติดต่อ");

                            val.BusinessType = input.BusinessType;
                            val.BusinessTypeName = input.BusinessTypeName;
                            val.ContactType = input.ContactType;
                            val.ContactTypeName = input.ContactTypeName;
                            val.ContactCode = input.ContactCode;
                            val.BusinessName = input.BusinessName;
                            val.TaxID = input.TaxID;
                            val.BranchType = input.BranchType;
                            val.BranchTypeName = input.BranchTypeName;
                            val.BranchCode = input.BranchCode;
                            val.BranchName = input.BranchName;
                            val.Address = input.Address;
                            val.ZipCode = input.ZipCode;
                            val.ShippingAddress = input.ShippingAddress;
                            val.OfficeNumber = input.OfficeNumber;
                            val.FaxNumber = input.FaxNumber;
                            val.WebSite = input.WebSite;
                            val.CreditDate = input.CreditDate;
                            val.ContactName = input.ContactName;
                            val.ContactEmail = input.ContactEmail;
                            val.ContactMobile = input.ContactMobile;
                            val.BankCode = input.BankCode;
                            val.BankName = input.BankName;
                            val.AccountType = input.AccountType;
                            val.AccountTypeName = input.AccountTypeName;
                            val.AccountNo = input.AccountNo;
                            val.AccountBranch = input.AccountBranch;
                            val.Note = input.Note;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;
                            context.SaveChanges();
                        }

                        var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                        Log_Activity request = new Log_Activity();
                        request.BusinessID = input.BusinessID;
                        request.ActivityDate = DateTime.Now;
                        request.ActivityHeader = "ข้อมูลสมุดรายชื่อ";
                        request.ActivityTodo = "เพิ่ม/แก้ไขข้อมูลสมุดรายชื่อ";
                        request.ActivityDesc = "ทำการเพิ่ม/แก้ไขข้อมูลสมุดรายชื่อ ชื่อธุรกิจ " + input.BusinessName;
                        request.ActivityByName = user;
                        request.CreateOn = DateTime.Now;
                        request.CreateBy = input.UpdateBy;
                        Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                        Log_ActivityRepository.Save(request, context);
                        context.SaveChanges();
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Delete(Contact input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Contact>().Where(a => a.ContactKey == input.ContactKey).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();

                        var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                        Log_Activity request = new Log_Activity();
                        request.BusinessID = input.BusinessID;
                        request.ActivityDate = DateTime.Now;
                        request.ActivityHeader = "ข้อมูลสมุดรายชื่อ";
                        request.ActivityTodo = "ลบข้อมูลสมุดรายชื่อ";
                        request.ActivityDesc = "ทำการลบข้อมูลสมุดรายชื่อ ชื่อธุรกิจ " + input.BusinessName;
                        request.ActivityByName = user;
                        request.CreateOn = DateTime.Now;
                        request.CreateBy = input.UpdateBy;
                        Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                        Log_ActivityRepository.Save(request, context);
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}
