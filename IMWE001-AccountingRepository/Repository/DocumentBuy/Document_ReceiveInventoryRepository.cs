﻿using System;
using System.Collections.Generic;
using System.Transactions;
using System.Linq;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingRepository.Repository.DocumentSell;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentBuy
{
    public class Document_ReceiveInventoryRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_ReceiveInventory, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "RI";
        public Document_ReceiveInventory Save(Document_ReceiveInventory input)
        {
            try
            {
                // bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var PO_Table = context.Set<Document_ReceiveInventory>();

                        if (!input.ReceiveInventoryKey.HasValue)
                        {
                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.ReceiveInventoryNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.ReceiveInventoryDate.Value.Day.ToString().PadLeft(2, '0'), input.ReceiveInventoryDate.Value.Month.ToString().PadLeft(2, '0'), input.ReceiveInventoryDate.Value.Year)
                            });

                            input.ReceiveInventoryStatus = "1";
                            input.ReceiveInventoryStatusName = "รออนุมัติ";
                            input.CreateBy = input.UpdateBy;
                            input.CreateDate = input.UpdateDate;
                            input.isdelete = 1;

                            PO_Table.Add(input);

                            context.SaveChanges();

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบรับสินค้า";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบรับสินค้า";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบรับสินค้า เลขที่เอกสาร" + input.ReceiveInventoryNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();


                            if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                            {
                                input.DocumentOwner = "PO";
                                //เก็บข้อมูลลง Reference
                                Document_Reference doc_ref = new Document_Reference()
                                {
                                    BusinessID = input.BusinessID,
                                    DocumentOwner = input.DocumentOwner,
                                    DocumentGen = "RI",
                                    PurchaseNo = input.DocumentOwner == "PO" ? input.DocumentNo : string.Empty,
                                    ReceivingInventoryNo = input.ReceiveInventoryNo,
                                    CreateDate = input.CreateDate,
                                    CreateBy = input.CreateBy
                                };
                                Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                                Document_ReferenceRepository.Add(doc_ref, context);

                                GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                                {
                                    BusinessID = input.BusinessID,
                                    DocumentKey = input.DocumentKey,
                                    remarkishas = string.Format("เปลี่ยนแปลงประเภทเอกสาร{0}เป็นใบรับสินค้า", input.DocumentOwner == "PO" ? "ใบสั่งซื้อ" : string.Empty),
                                    ToDocumentKey = input.ReceiveInventoryKey,
                                    ToDocumentNo = input.ReceiveInventoryNo,
                                    UpdateBy = input.UpdateBy,
                                    UpdateDate = input.UpdateDate.Value
                                };

                                if (input.DocumentOwner == "PO")
                                {
                                    // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                    Document_PurchaseOrderRepository porep = new Document_PurchaseOrderRepository();
                                    porep.UpdateStatus(new Document_PurchaseOrder() { BusinessID = input.BusinessID, PurchaseOrderKey = input.DocumentKey, PurchaseOrderStatus = "99", UpdateBy = input.UpdateBy, UpdateDate = input.UpdateDate }, "", context);
                                }
                                Document_ReferenceRepository.Dispose();
                                context.SaveChanges();
                            }

                        }
                        else
                        {
                            var val = PO_Table.Where(S => S.BusinessID == input.BusinessID && S.ReceiveInventoryKey == input.ReceiveInventoryKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบสั่งซื้อ");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.ReceiveInventoryStatus = input.ReceiveInventoryStatus;
                            val.ReceiveInventoryStatusName = input.ReceiveInventoryStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.ReceiveInventoryDate = input.ReceiveInventoryDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;
                            context.SaveChanges();

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบรับสินค้า";
                                request.ActivityTodo = "แก้ไขใบรับสินค้า";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบรับสินค้า เลขที่เอกสาร" + input.ReceiveInventoryNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }

                            var tbPODetail = context.Set<Document_ReceiveInventoryDetail>();
                            var tmp = tbPODetail.Where(S => S.ReceiveInventoryKey == input.ReceiveInventoryKey);

                            if (tmp != null && tmp.Count() > 0)
                            {
                                tbPODetail.RemoveRange(tmp); context.SaveChanges();
                            }
                            if (input.ReceiveInventoryDetails != null && input.ReceiveInventoryDetails.Count > 0)
                            {
                                foreach (var item in input.ReceiveInventoryDetails)
                                {
                                    item.BusinessID = input.BusinessID;
                                    item.ReceiveInventoryKey = input.ReceiveInventoryKey;
                                }
                                tbPODetail.AddRange(input.ReceiveInventoryDetails); context.SaveChanges();
                            }
                        }
                    }
                    scope.Complete();
                }

                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(Document_ReceiveInventory input, string remarkishas)
        {
            try
            {
                var data = this.Get(input.BusinessID.ToString(), input.ReceiveInventoryKey.Value);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var val = context.Set<Document_ReceiveInventory>().Where(a => a.BusinessID == input.BusinessID && a.ReceiveInventoryKey == input.ReceiveInventoryKey).FirstOrDefault();

                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบสั่งซื้อ");

                        if (val.ReceiveInventoryStatus == "2" && data.ReceiveTaxInfo != null)
                        {
                            if (input.ReceiveInventoryStatus == "3")
                                throw new Exception(@"ไม่สามารถไม่อนุมัติเอกสารได้ <br/> เอกสารนี้ได้ถูกเพิ่มข้อมูลใบกำกับภาษีแล้ว <br/> หากต้องการไม่อนุมัติกรุณาลบข้อมูลใบกำกับภาษีที่ได้รับ");
                            else if (input.ReceiveInventoryStatus == "1")
                                throw new Exception(@"ไม่สามารถรีเซ็ตเอกสารได้ <br/> เอกสารนี้ได้ถูกเพิ่มข้อมูลใบกำกับภาษีแล้ว <br/> หากต้องการไม่อนุมัติกรุณาลบข้อมูลใบกำกับภาษีที่ได้รับ");
                        }

                        if (val.ReceiveInventoryStatus == "99" && input.ReceiveInventoryStatus == "1")
                        {
                            if (data.ReceiveTaxInfo != null)
                                input.ReceiveInventoryStatus = "2";
                        }

                        var stable = context.Set<Param_DocReceivingInventoryStatus>().Where(a => a.ID == input.ReceiveInventoryStatus).FirstOrDefault();

                        string curstatus = val.ReceiveInventoryStatus;
                        string curstatusdes = val.ReceiveInventoryStatusName;

                        val.ReceiveInventoryStatus = stable.ID;
                        val.ReceiveInventoryStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();


                        if (stable.ID == "1")
                        {
                            var paymenttable = context.Set<Document_ReceiveInfo>().Where(S => S.BusinessID == input.BusinessID && S.DocumentKey == input.ReceiveInventoryKey && S.isdelete == isactive);

                            if (paymenttable.Count() > 0)
                            {
                                foreach (var paymentitem in paymenttable)
                                    paymentitem.isdelete = isflagdelete;
                            }
                            context.SaveChanges();
                        }

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.ReceiveInventoryKey,
                            DocumentNo = val.ReceiveInventoryNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_ReceiveInventory Get(string businessid, int key)
        {
            Document_ReceiveInventory response = new Document_ReceiveInventory();
            using (Document_ReceiveInventoryRepository repo = new Document_ReceiveInventoryRepository())
                response = repo.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.ReceiveInventoryKey.Value == key);
            if (response != null)
            {
                response.ReceiveInventoryDetails = new List<Document_ReceiveInventoryDetail>();
                using (Document_ReceiveInventoryDetailRepository repodetail = new Document_ReceiveInventoryDetailRepository())
                    response.ReceiveInventoryDetails = repodetail.SelectDataWithCondition(a => a.ReceiveInventoryKey == response.ReceiveInventoryKey).OrderBy(a => a.Sequence).ToList();

                using (Document_ReceiveInfoRepository riRep = new Document_ReceiveInfoRepository())
                    response.ReceiveInfo = riRep.SelectFirstDataWithCondition(a => a.DocumentNo == response.ReceiveInventoryNo && a.BusinessID == response.BusinessID && a.isdelete == 1);

                using (Document_ReceiveTaxInvoiceInfoRepository ritRep = new Document_ReceiveTaxInvoiceInfoRepository())
                    response.ReceiveTaxInfo = ritRep.SelectFirstDataWithCondition(r => r.DocumentNo == response.ReceiveInventoryNo && r.BusinessID == response.BusinessID && r.isdelete == 1);
            }

            return response;
        }

        public void Delete(Document_ReceiveInventory input)
        {
            try
            {
                using (AccountingContext context = new AccountingContext())
                {
                    var otable = context.Set<Document_ReceiveInventory>().Where(a => a.ReceiveInventoryKey == input.ReceiveInventoryKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                    if (otable != null)
                    {
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_ReceiveInventory> GetWithRef(string businessid)
        {
            List<Document_ReceiveInventory> response = new List<Document_ReceiveInventory>();
            using (AccountingContext context = new AccountingContext())
            {
                var otable = context.Set<Document_ReceiveInventory>();
                response = otable.Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.ReceiveInventoryKey).ToList();
                if (response != null && response.Count > 0)
                {
                    string[] documentconvert = response.ConvertAll(x => x.ReceiveInventoryNo).ToArray();
                    List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.ReceivingInventoryNo)).ToList();
                    int[] documentkeyconvert = response.ConvertAll(x => x.ReceiveInventoryKey.Value).ToArray();
                    List<Document_ReceiveInfo> infotable = context.Set<Document_ReceiveInfo>().Where(a => documentkeyconvert.Contains(a.DocumentKey.Value) && a.DocumentType == Prefix && a.isdelete == isactive).ToList();

                    foreach (var bil in response)
                    {
                        bil.ReceiveInfo = new Document_ReceiveInfo();
                        bil.ReceiveInfo = infotable.Where(a => a.DocumentKey == bil.ReceiveInventoryKey).FirstOrDefault();
                        //using (Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository())
                        //    bil.ReceiveInfo = Document_ReceiveInfoRepository.SelectFirstDataWithCondition(a => a.DocumentKey == bil.ReceiveInventoryKey && a.DocumentType == Prefix && a.isdelete == isactive);


                        bil.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.ReceivingInventoryNo == bil.ReceiveInventoryNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            bil.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                bil.DocumentRefNo = bil.DocumentRefNo + docno;
                            }
                            bil.DocumentRefNo = bil.DocumentRefNo.Replace(",,", "");
                            bil.DocumentRefNo = bil.DocumentRefNo.Replace(",", "<br>");
                        }
                    }
                }

            }

            return response;
        }

        public Document_ReceiveInfo SaveReceivePayment(Document_ReceiveInfo input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_ReceiveInventory val = context.Set<Document_ReceiveInventory>().Where(a => a.BusinessID == input.BusinessID && a.ReceiveInventoryNo == input.DocumentNo).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบรับสินค้า");

                        var stable = context.Set<Param_DocReceivingInventoryStatus>().Where(a => a.ID == "99").FirstOrDefault();

                        string curstatus = val.ReceiveInventoryStatus;
                        string curstatusdes = val.ReceiveInventoryStatusName;

                        val.ReceiveInventoryStatus = stable.ID;
                        val.ReceiveInventoryStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;
                        context.SaveChanges();

                        Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository();
                        Document_ReceiveInfoRepository.Save(input, context);

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.ReceiveInventoryKey,
                            DocumentNo = val.ReceiveInventoryNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Document_ReceiveTaxInvoiceInfo SaveReceiveTax(Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var rtiInfoTable = context.Set<Document_ReceiveTaxInvoiceInfo>();

                        Document_ReceiveInventory val = context.Set<Document_ReceiveInventory>().Where(a => a.BusinessID == input.BusinessID && a.ReceiveInventoryKey == input.DocumentKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบรับสินค้า");

                        string status = "99";
                        if (val.ReceiveInventoryStatus != "99")
                            status = "2";

                        var stable = context.Set<Param_DocReceivingInventoryStatus>().Where(a => a.ID == status).FirstOrDefault();

                        string curstatus = val.ReceiveInventoryStatus;
                        string curstatusdes = val.ReceiveInventoryStatusName;

                        val.ReceiveInventoryStatus = stable.ID;
                        val.ReceiveInventoryStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;
                        context.SaveChanges();


                        var tmp = rtiInfoTable.Where(S => S.DocumentType == input.DocumentType && S.BusinessID == input.BusinessID && S.DocumentNo == input.DocumentNo && S.isdelete == isactive).FirstOrDefault();

                        if (tmp == null)
                        {
                            input.CreateDate = DateTime.Now;

                            rtiInfoTable.Add(input);
                        }
                        else
                        {
                            tmp.DocumentPath = input.DocumentPath;
                            tmp.InvoiceDate = input.InvoiceDate;
                            tmp.InvoiceNo = input.InvoiceNo;
                            tmp.SupplierBranch = input.SupplierBranch;
                            tmp.SupplierName = input.SupplierName;
                            tmp.SupplierTaxID = input.SupplierTaxID;
                            tmp.UpdateBy = tmp.CreateBy;
                            tmp.UpdateDate = DateTime.Now;
                            tmp.DocumentPath = input.DocumentPath;
                            tmp.ThumbnailDocument = input.ThumbnailDocument;
                        }
                        context.SaveChanges();

                        input = rtiInfoTable.Where(S => S.DocumentType == input.DocumentType && S.BusinessID == input.BusinessID && S.DocumentNo == input.DocumentNo && input.isdelete == isactive).FirstOrDefault();
                    }
                    scope.Complete();
                }
                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Document_ReceiveTaxInvoiceInfo DeleteReceiveTax(Document_ReceiveTaxInvoiceInfo input)
        {
            try
            {
                using (AccountingContext context = new AccountingContext())
                {
                    var rtiInfoTable = context.Set<Document_ReceiveTaxInvoiceInfo>();
                    var tmp = rtiInfoTable.Where(S => S.DocumentType == input.DocumentType && S.BusinessID == input.BusinessID && S.DocumentNo == input.DocumentNo && S.isdelete == isactive).FirstOrDefault();

                    tmp.isdelete = isflagdelete;
                    context.SaveChanges();

                    input = tmp;
                }

                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}