﻿using System;
using System.Collections.Generic; 
using System.Transactions; 
using System.Linq;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingRepository.Repository.Parameter;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business; 
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingRepository.Repository.DocumentSell;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentBuy
{
    public class Document_PurchaseOrderRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_PurchaseOrder, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "PO";
        public Document_PurchaseOrder Save(Document_PurchaseOrder input)
        {
            try
            {
               // bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var PO_Table = context.Set<Document_PurchaseOrder>();

                        if (!input.PurchaseOrderKey.HasValue)
                        {
                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.PurchaseOrderNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.PurchaseOrderDate.Value.Day.ToString().PadLeft(2, '0'), input.PurchaseOrderDate.Value.Month.ToString().PadLeft(2, '0'), input.PurchaseOrderDate.Value.Year)
                            });

                            input.PurchaseOrderStatus = "1";
                            input.PurchaseOrderStatusName = "รออนุมัติ";
                            input.CreateBy = input.UpdateBy;
                            input.CreateDate = input.UpdateDate;
                            input.isdelete = 1;

                            PO_Table.Add(input);

                            context.SaveChanges();

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบสั่งซื้อ";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบสั่งซื้อ";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบสั่งซื้อ เลขที่เอกสาร" + input.PurchaseOrderNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();

                            if (!input.CustomerKey.HasValue)
                            {
                                Contact contact = new Contact()
                                {
                                    BusinessID = input.BusinessID,
                                    BusinessType = "C",
                                    BusinessTypeName = "นิติบุคคล",
                                    ContactType = "S",
                                    ContactTypeName = "ผู้จำหน่าย",
                                    BusinessName = input.CustomerName,
                                    TaxID = input.CustomerTaxID,
                                    BranchType = "H",
                                    BranchTypeName = "สำนักงานใหญ่",
                                    Address = input.CustomerAddress,
                                    CreditDate = input.CreditDay,
                                    ContactName = input.CustomerContactName,
                                    ContactEmail = input.CustomerContactEmail,
                                    ContactMobile = input.CustomerContactPhone,
                                    isdelete = 1,
                                    CreateDate = input.UpdateDate,
                                    CreateBy = input.UpdateBy,
                                    UpdateDate = input.UpdateDate,
                                    UpdateBy = input.UpdateBy,
                                    BranchName = input.CustomerBranch
                                };
                                var ctable = context.Set<Contact>();
                                ctable.Add(contact);
                                context.SaveChanges();

                                Document_PurchaseOrder val = context.Set<Document_PurchaseOrder>().Where(a => a.BusinessID == input.BusinessID && a.PurchaseOrderKey == input.PurchaseOrderKey).FirstOrDefault();
                                val.CustomerKey = contact.ContactKey;
                                context.SaveChanges();
                            }

                            if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                            {
                                //เก็บข้อมูลลง Reference
                                Document_Reference doc_ref = new Document_Reference()
                                {
                                    BusinessID = input.BusinessID,
                                    DocumentOwner = input.DocumentOwner,
                                    DocumentGen = "PO",
                                    QuotationNo = input.DocumentOwner == "QT" ? input.DocumentNo : string.Empty,
                                    PurchaseNo = input.PurchaseOrderNo,
                                    CreateDate = input.CreateDate,
                                    CreateBy = input.CreateBy
                                };
                                Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                                Document_ReferenceRepository.Add(doc_ref, context);

                                GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                                {
                                    BusinessID = input.BusinessID,
                                    DocumentKey = input.DocumentKey,
                                    remarkishas = string.Format("เปลี่ยนแปลงประเภทเอกสาร{0}เป็นใบสั่งสินค้า", input.DocumentOwner == "QT" ? "ใบเสนอราคา" : string.Empty),
                                    ToDocumentKey = input.PurchaseOrderKey,
                                    ToDocumentNo = input.PurchaseOrderNo,
                                    UpdateBy = input.UpdateBy,
                                    UpdateDate = input.UpdateDate.Value
                                };

                                if (input.DocumentOwner == "QT")
                                {
                                    // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                    Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository();
                                    Document_QuotationRepository.SaveWorkflow(gen_ref, context);
                                    Document_QuotationRepository.Dispose();
                                }
                                Document_ReferenceRepository.Dispose();
                                context.SaveChanges();
                            }
                        }
                        else
                        {
                            var val = PO_Table.Where(S => S.BusinessID == input.BusinessID && S.PurchaseOrderKey == input.PurchaseOrderKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบสั่งซื้อ");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.PurchaseOrderStatus = input.PurchaseOrderStatus;
                            val.PurchaseOrderStatusName = input.PurchaseOrderStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.PurchaseOrderDate = input.PurchaseOrderDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;
                            context.SaveChanges();

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบสั่งซื้อ";
                                request.ActivityTodo = "แก้ไขใบสั่งซื้อ";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบสั่งซื้อ เลขที่เอกสาร" + input.PurchaseOrderNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }

                            var tbPODetail = context.Set<Document_PurchaseOrderDetail>();
                            var tmp = tbPODetail.Where(S => S.PurchaseOrderKey == input.PurchaseOrderKey);

                            if (tmp != null && tmp.Count() > 0)
                            {
                                tbPODetail.RemoveRange(tmp); context.SaveChanges();
                            }
                            if (input.PurchaseOrderDetails != null && input.PurchaseOrderDetails.Count > 0)
                            {
                                foreach (var item in input.PurchaseOrderDetails)
                                {
                                    item.BusinessID = input.BusinessID;
                                    item.PurchaseOrderKey = input.PurchaseOrderKey;
                                }
                                tbPODetail.AddRange(input.PurchaseOrderDetails); context.SaveChanges();
                            }

                            if (input.PurchaseOrderDetails != null && input.PurchaseOrderDetails.Count > 0)
                            {
                                foreach (Document_PurchaseOrderDetail object1 in input.PurchaseOrderDetails)
                                {
                                    // เพิ่ม Product
                                    if (!object1.ProductKey.HasValue)
                                    {
                                        var ptable = context.Set<Product>();
                                        Product product = new Product()
                                        {
                                            BusinessID = input.BusinessID,
                                            ProductType = "S",
                                            ProductTypeName = "บริการ",
                                            ProductName = object1.Name,
                                            ProductDescription = object1.Description,
                                            UnitTypeName = object1.Unit,
                                            SellPrice = object1.PriceBeforeTax,
                                            SellTaxPrice = object1.PriceTax,
                                            SellAfterTaxPrice = object1.PriceAfterTax,
                                            SellTaxType = "2",
                                            SellTaxTypeName = "ราคาไม่รวม VAT",
                                            BuyTaxType = "2",
                                            BuyTaxTypeName = "ราคาไม่รวม VAT",
                                            BuyPrice = object1.PriceBeforeTax,
                                            BuyAfterTaxPrice = object1.PriceTax,
                                            BuyTaxPrice = object1.PriceAfterTax,
                                            CreateBy = input.UpdateBy,
                                            CreateDate = input.UpdateDate,
                                            UpdateDate = input.UpdateDate,
                                            UpdateBy = input.UpdateBy,
                                            isdelete = 1
                                        };
                                        ptable.Add(product);
                                        context.SaveChanges();

                                        Document_PurchaseOrderDetail valdetail = context.Set<Document_PurchaseOrderDetail>().Where(a => a.PurchaseOrderDetailKey == object1.PurchaseOrderDetailKey && a.PurchaseOrderKey == input.PurchaseOrderKey).FirstOrDefault();
                                        valdetail.ProductKey = product.ProductKey;
                                        context.SaveChanges();
                                    }

                                    // เพิ่ม Unit
                                    if (!string.IsNullOrEmpty(object1.Unit) && !string.IsNullOrEmpty(object1.Unit.Trim()))
                                    {
                                        Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository();
                                        Param_UnitTypeRepository.Add(object1.Unit, context);
                                    }
                                }
                            }
                        }
                    }
                    scope.Complete();
                }

                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(Document_PurchaseOrder input, string remarkishas, AccountingContext context)
        {
            var val = context.Set<Document_PurchaseOrder>().Where(a => a.BusinessID == input.BusinessID && a.PurchaseOrderKey == input.PurchaseOrderKey).FirstOrDefault();

            if (val == null)
                throw new Exception("ไม่พบข้อมูลใบสั่งซื้อ");

            var stable = context.Set<Param_DocPurchaseStatus>().Where(a => a.ID == input.PurchaseOrderStatus).FirstOrDefault();

            string curstatus = val.PurchaseOrderStatus;
            string curstatusdes = val.PurchaseOrderStatusName;

            val.PurchaseOrderStatus = stable.ID;
            val.PurchaseOrderStatusName = stable.Name;
            val.UpdateDate = input.UpdateDate;
            val.UpdateBy = input.UpdateBy;

            context.SaveChanges();

            Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
            Log_Workflow obj = new Log_Workflow()
            {
                BusinessID = val.BusinessID,
                DocumentKey = val.PurchaseOrderKey,
                DocumentNo = val.PurchaseOrderNo,
                CurrentStatus = curstatus,
                CurrentStatusName = curstatusdes,
                ToStatus = stable.ID,
                ToStatusName = stable.Name,
                CreateDate = input.UpdateDate,
                CreateBy = input.UpdateBy
            };
            Log_WorkflowRepository.Save(obj, context);

        }

        public void UpdateStatus(Document_PurchaseOrder input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var val = context.Set<Document_PurchaseOrder>().Where(a => a.BusinessID == input.BusinessID && a.PurchaseOrderKey == input.PurchaseOrderKey).FirstOrDefault();

                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบสั่งซื้อ");

                        var stable = context.Set<Param_DocPurchaseStatus>().Where(a => a.ID == input.PurchaseOrderStatus).FirstOrDefault();

                        string curstatus = val.PurchaseOrderStatus;
                        string curstatusdes = val.PurchaseOrderStatusName;

                        val.PurchaseOrderStatus = stable.ID;
                        val.PurchaseOrderStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.PurchaseOrderKey,
                            DocumentNo = val.PurchaseOrderNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_PurchaseOrder Get(string businessid, int key)
        {
            Document_PurchaseOrder response = new Document_PurchaseOrder();
            using (Document_PurchaseOrderRepository repo = new Document_PurchaseOrderRepository())
                response = repo.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.PurchaseOrderKey.Value == key);
            if (response != null)
            {
                response.PurchaseOrderDetails = new List<Document_PurchaseOrderDetail>();
                using (Document_PurchaseOrderDetailRepository repodetail = new Document_PurchaseOrderDetailRepository())
                    response.PurchaseOrderDetails = repodetail.SelectDataWithCondition(a => a.PurchaseOrderKey == response.PurchaseOrderKey).OrderBy(a => a.Sequence).ToList();
            }

            return response;
        }

        public void Delete(Document_PurchaseOrder input)
        {
            try
            {
                using (AccountingContext context = new AccountingContext())
                {
                    var otable = context.Set<Document_PurchaseOrder>().Where(a => a.PurchaseOrderKey == input.PurchaseOrderKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                    if (otable != null)
                    {
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_PurchaseOrder> GetWithRef(string businessid)
        {
            List<Document_PurchaseOrder> response = new List<Document_PurchaseOrder>();
            using (AccountingContext context = new AccountingContext())
            {
                response = context.Set<Document_PurchaseOrder>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.PurchaseOrderKey).ToList();
                if (response != null && response.Count > 0)
                {
                    string[] documentconvert = response.ConvertAll(x => x.PurchaseOrderNo).ToArray();
                    List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.PurchaseNo)).ToList();
                    foreach (Document_PurchaseOrder bil in response)
                    {
                        bil.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.PurchaseNo == bil.PurchaseOrderNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            bil.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                bil.DocumentRefNo = bil.DocumentRefNo + docno;
                            }
                            bil.DocumentRefNo = bil.DocumentRefNo.Replace(",,", "");
                            bil.DocumentRefNo = bil.DocumentRefNo.Replace(",", "<br>");
                        }
                    }
                }
            }
            return response;
        }
    }
}
