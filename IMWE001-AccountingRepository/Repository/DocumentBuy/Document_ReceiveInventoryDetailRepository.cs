﻿using System;
using System.Collections.Generic;
using System.Transactions;
using System.Linq;
using IMWE001_AccountingModels.DocumentBuy;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;

namespace IMWE001_AccountingRepository.Repository.DocumentBuy
{
    public class Document_ReceiveInventoryDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_ReceiveInventoryDetail, AccountingContext>
    {
    }
}
