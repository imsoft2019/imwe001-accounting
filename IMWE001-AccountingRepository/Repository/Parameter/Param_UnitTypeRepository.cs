﻿using IMWE001_AccountingModels.Parameter;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace IMWE001_AccountingRepository.Repository.Parameter
{

    public class Param_UnitTypeRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Param_UnitType, AccountingContext>
    {
        public void Add(string unit, AccountingContext context)
        {
            var otable = context.Set<Param_UnitType>();
            Param_UnitType stable = otable.Where(a => a.Name.Trim() == unit.Trim()).FirstOrDefault();
            if (stable == null)
            {
                Param_UnitType ntable = new Param_UnitType() { Name = unit };
                otable.Add(ntable);
                context.SaveChanges();
            }
        }
    }
}
