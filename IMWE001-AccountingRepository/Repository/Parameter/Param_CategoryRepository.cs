﻿using IMWE001_AccountingModels.Parameter;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace IMWE001_AccountingRepository.Repository.Parameter
{

    public class Param_CategoryRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Param_Category, AccountingContext>
    {
        public void Add(string category,AccountingContext context)
        {
            var otable = context.Set<Param_Category>();
            Param_Category stable = otable.Where(a => a.Name == category).FirstOrDefault();
            if (stable == null)
            {
                Param_Category ntable = new Param_Category() {Name = category  };
                otable.Add(ntable);
                context.SaveChanges();
            }
        }
    }
}
