﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_ReceiptDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_ReceiptDetail, AccountingContext>
    {
        public void Add(int ReceiptKey, Guid BusinessID, List<Document_ReceiptDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_ReceiptDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_ReceiptDetail obj in request)
                {
                    obj.ReceiptKey = ReceiptKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int ReceiptKey, AccountingContext context)
        {
            var otable = context.Set<Document_ReceiptDetail>();
            List<Document_ReceiptDetail> val = new List<Document_ReceiptDetail>();
            val = otable.Where(a => a.ReceiptKey.Value == ReceiptKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_ReceiptDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
