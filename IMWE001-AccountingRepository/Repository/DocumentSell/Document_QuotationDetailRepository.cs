﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_QuotationDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_QuotationDetail, AccountingContext>
    {
        public void Add(int QuotationKey, Guid BusinessID , List<Document_QuotationDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_QuotationDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_QuotationDetail obj in request)
                {
                    obj.QuotationKey = QuotationKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int QuotationKey, AccountingContext context)
        {
            var otable = context.Set<Document_QuotationDetail>();
            List<Document_QuotationDetail> val = new List<Document_QuotationDetail>();
            val = otable.Where(a => a.QuotationKey.Value == QuotationKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_QuotationDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
