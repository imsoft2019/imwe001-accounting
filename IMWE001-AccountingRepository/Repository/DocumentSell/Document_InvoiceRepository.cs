﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_InvoiceRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_Invoice, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "INV";
        public Document_Invoice Save(Document_Invoice input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Invoice>();
                        if (!input.InvoiceKey.HasValue)
                        {
                            string flagstate = "1";
                            if (input.DocumentOwner == "BL")
                                flagstate = "2";

                            Param_DocInvoiceStatus stable = context.Set<Param_DocInvoiceStatus>().Where(a => a.ID == flagstate).FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.InvoiceNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.InvoiceDate.Value.Day.ToString().PadLeft(2, '0'), input.InvoiceDate.Value.Month.ToString().PadLeft(2, '0'), input.InvoiceDate.Value.Year)
                            });
                            input.InvoiceStatus = stable.ID;
                            input.InvoiceStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบแจ้งหนี้";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบแจ้งหนี้";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบแจ้งหนี้ เลขที่เอกสาร" + input.InvoiceNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            Document_Invoice val = context.Set<Document_Invoice>().Where(a => a.BusinessID == input.BusinessID && a.InvoiceKey == input.InvoiceKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.InvoiceStatus = input.InvoiceStatus;
                            val.InvoiceStatusName = input.InvoiceStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.InvoiceDate = input.InvoiceDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบแจ้งหนี้";
                                request.ActivityTodo = "แก้ไขใบแจ้งหนี้";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบแจ้งหนี้ เลขที่เอกสาร" + input.InvoiceNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();

                        Document_InvoiceDetailRepository Document_InvoiceDetailRepository = new Document_InvoiceDetailRepository();
                        if (!isnew)
                        {
                            Document_InvoiceDetailRepository.Delete(input.InvoiceKey.Value, context);
                            context.SaveChanges();

                            if (input.InvoiceDetail != null && input.InvoiceDetail.Count > 0)
                            {
                                Document_InvoiceDetailRepository.Add(input.InvoiceKey.Value, input.BusinessID, input.InvoiceDetail, context);
                            }

                            context.SaveChanges();

                            Document_InvoiceDetailRepository.Dispose();
                        }

                        // เพิ่ม Customer
                        if (!input.CustomerKey.HasValue)
                        {
                            Contact contact = new Contact()
                            {
                                BusinessID = input.BusinessID,
                                BusinessType = "C",
                                BusinessTypeName = "นิติบุคคล",
                                ContactType = "C",
                                ContactTypeName = "ลูกค้า",
                                BusinessName = input.CustomerName,
                                TaxID = input.CustomerTaxID,
                                BranchType = "H",
                                BranchTypeName = "สำนักงานใหญ่",
                                Address = input.CustomerAddress,
                                CreditDate = input.CreditDay,
                                ContactName = input.CustomerContactName,
                                ContactEmail = input.CustomerContactEmail,
                                ContactMobile = input.CustomerContactPhone,
                                isdelete = 1,
                                CreateDate = input.UpdateDate,
                                CreateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate,
                                UpdateBy = input.UpdateBy,
                                BranchName = input.CustomerBranch
                            };
                            var ctable = context.Set<Contact>();
                            ctable.Add(contact);
                            context.SaveChanges();

                            Document_Invoice val = context.Set<Document_Invoice>().Where(a => a.BusinessID == input.BusinessID && a.InvoiceKey == input.InvoiceKey).FirstOrDefault();
                            val.CustomerKey = contact.ContactKey;
                            context.SaveChanges();
                        }

                        if (input.InvoiceDetail != null && input.InvoiceDetail.Count > 0)
                        {
                            foreach (Document_InvoiceDetail object1 in input.InvoiceDetail)
                            {
                                // เพิ่ม Product
                                if (!object1.ProductKey.HasValue)
                                {
                                    var ptable = context.Set<Product>();
                                    Product product = new Product()
                                    {
                                        BusinessID = input.BusinessID,
                                        ProductType = "S",
                                        ProductTypeName = "บริการ",
                                        ProductName = object1.Name,
                                        ProductDescription = object1.Description,
                                        UnitTypeName = object1.Unit,
                                        SellPrice = object1.PriceBeforeTax,
                                        SellTaxPrice = object1.PriceTax,
                                        SellAfterTaxPrice = object1.PriceAfterTax,
                                        SellTaxType = "2",
                                        SellTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyTaxType = "2",
                                        BuyTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyPrice = object1.PriceBeforeTax,
                                        BuyAfterTaxPrice = object1.PriceTax,
                                        BuyTaxPrice = object1.PriceAfterTax,
                                        CreateBy = input.UpdateBy,
                                        CreateDate = input.UpdateDate,
                                        UpdateDate = input.UpdateDate,
                                        UpdateBy = input.UpdateBy,
                                        isdelete = 1
                                    };
                                    ptable.Add(product);
                                    context.SaveChanges();

                                    Document_InvoiceDetail valdetail = context.Set<Document_InvoiceDetail>().Where(a => a.InvoiceDetailKey == object1.InvoiceDetailKey && a.InvoiceKey == input.InvoiceKey).FirstOrDefault();
                                    valdetail.ProductKey = product.ProductKey;
                                    context.SaveChanges();
                                }

                                // เพิ่ม Unit
                                if (!string.IsNullOrEmpty(object1.Unit) && !string.IsNullOrEmpty(object1.Unit.Trim()))
                                {
                                    Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository();
                                    Param_UnitTypeRepository.Add(object1.Unit, context);
                                }
                            }
                        }

                        if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        {
                            //เก็บข้อมูลลง Reference
                            Document_Reference doc_ref = new Document_Reference()
                            {
                                BusinessID = input.BusinessID,
                                DocumentOwner = input.DocumentOwner,
                                DocumentGen = "INV",
                                QuotationNo = input.DocumentOwner == "QT" ? input.DocumentNo : string.Empty,
                                BillingNo = input.DocumentOwner == "BL" ? input.DocumentNo : string.Empty,
                                InvoiceNo = input.InvoiceNo,
                                CreateDate = input.CreateDate,
                                CreateBy = input.CreateBy
                            };
                            Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                            Document_ReferenceRepository.Add(doc_ref, context);

                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = input.DocumentKey,
                                remarkishas = string.Format("เปลี่ยนแปลงประเภทเอกสาร{0}เป็นใบแจ้งหนี้/ใบกำกับภาษี", input.DocumentOwner == "QT" ? "ใบเสนอราคา" : input.DocumentOwner == "BL" ? "ใบแจ้งหนี้" : string.Empty),
                                ToDocumentKey = input.InvoiceKey,
                                ToDocumentNo = input.InvoiceNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            if (input.DocumentOwner == "QT")
                            {
                                // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository();
                                Document_QuotationRepository.SaveWorkflow(gen_ref, context);
                                Document_QuotationRepository.Dispose();
                            }
                            else if (input.DocumentOwner == "BL")
                            {
                                // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                Document_BillingRepository Document_BillingRepository = new Document_BillingRepository();
                                Document_BillingRepository.SaveWorkflow(gen_ref, context);
                                Document_BillingRepository.Dispose();
                            }
                            Document_ReferenceRepository.Dispose();
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(Document_Invoice input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_Invoice val = context.Set<Document_Invoice>().Where(a => a.BusinessID == input.BusinessID && a.InvoiceKey == input.InvoiceKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                        Param_DocInvoiceStatus stable = context.Set<Param_DocInvoiceStatus>().Where(a => a.ID == input.InvoiceStatus).FirstOrDefault();

                        string curstatus = val.InvoiceStatus;
                        string curstatusdes = val.InvoiceStatusName;

                        val.InvoiceStatus = stable.ID;
                        val.InvoiceStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.InvoiceKey,
                            DocumentNo = val.InvoiceNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(GenerateDocumentRef input, AccountingContext context)
        {
            try
            {
                Document_Invoice val = context.Set<Document_Invoice>().Where(a => a.BusinessID == input.BusinessID && a.InvoiceKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                if (val.InvoiceStatus == "99")
                    throw new Exception("สถานะเอกสารไม่สามารถเปลี่ยนแปลงได้ เนื่องจากถูกเปลี่ยนแปลงเรียบร้อยแล้ว");

                Param_DocInvoiceStatus stable = context.Set<Param_DocInvoiceStatus>().Where(a => a.ID == "99").FirstOrDefault();

                string curstatus = val.InvoiceStatus;
                string curstatusdes = val.InvoiceStatusName;

                val.InvoiceStatus = stable.ID;
                val.InvoiceStatusName = stable.Name;
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                Log_Workflow obj = new Log_Workflow()
                {
                    BusinessID = val.BusinessID,
                    DocumentKey = val.InvoiceKey,
                    DocumentNo = val.InvoiceNo,
                    CurrentStatus = curstatus,
                    CurrentStatusName = curstatusdes,
                    ToStatus = stable.ID,
                    ToStatusName = stable.Name,
                    ToDocumentKey = input.ToDocumentKey,
                    ToDocumentNo = input.ToDocumentNo,
                    Remark = input.remarkishas,
                    CreateDate = input.UpdateDate,
                    CreateBy = input.UpdateBy
                };
                Log_WorkflowRepository.Save(obj, context);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflowNote(GenerateDocumentRef input, AccountingContext context, string DocumentType)
        {
            try
            {
                Document_Invoice val = context.Set<Document_Invoice>().Where(a => a.BusinessID == input.BusinessID && a.InvoiceKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                if (val.InvoiceStatus == "99")
                    throw new Exception("สถานะเอกสารไม่สามารถเปลี่ยนแปลงได้ เนื่องจากถูกเปลี่ยนแปลงเรียบร้อยแล้ว");

                Param_DocInvoiceStatus stable = context.Set<Param_DocInvoiceStatus>().Where(a => a.ID == "2").FirstOrDefault();

                string curstatus = val.InvoiceStatus;
                string curstatusdes = val.InvoiceStatusName;

                val.InvoiceStatus = stable.ID;
                val.InvoiceStatusName = stable.Name;
                if (DocumentType == "CN")
                {
                    val.CreditNoteKey = input.ToDocumentKey;
                    val.CreditNoteNo = input.ToDocumentNo;
                }
                else if (DocumentType == "DN")
                {
                    val.DebitNoteKey = input.ToDocumentKey;
                    val.DebitNoteNo = input.ToDocumentNo;
                }
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                Log_Workflow obj = new Log_Workflow()
                {
                    BusinessID = val.BusinessID,
                    DocumentKey = val.InvoiceKey,
                    DocumentNo = val.InvoiceNo,
                    CurrentStatus = curstatus,
                    CurrentStatusName = curstatusdes,
                    ToStatus = stable.ID,
                    ToStatusName = stable.Name,
                    ToDocumentKey = input.ToDocumentKey,
                    ToDocumentNo = input.ToDocumentNo,
                    Remark = input.remarkishas,
                    CreateDate = input.UpdateDate,
                    CreateBy = input.UpdateBy
                };
                Log_WorkflowRepository.Save(obj, context);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflowCumulative(GenerateDocumentRef input, AccountingContext context, string DocumentType)
        {
            try
            {
                Document_Invoice val = context.Set<Document_Invoice>().Where(a => a.BusinessID == input.BusinessID && a.InvoiceKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                Param_DocInvoiceStatus stable = context.Set<Param_DocInvoiceStatus>().Where(a => a.ID == (DocumentType == "BL" ? "2" : DocumentType == "RE" ? "99" : "1")).FirstOrDefault();

                string curstatus = val.InvoiceStatus;
                string curstatusdes = val.InvoiceStatusName;

                val.InvoiceStatus = stable.ID;
                val.InvoiceStatusName = stable.Name;
                if (DocumentType == "BL")
                {
                    val.BillingNoteKey = input.ToDocumentKey;
                    val.BillingNoteNo = input.ToDocumentNo;
                }
                else if (DocumentType == "RE")
                {
                    val.ReceiptKey = input.ToDocumentKey;
                    val.ReceiptNo = input.ToDocumentNo;
                }
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                if (curstatus != "2" && curstatus != "99")
                {
                    Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                    Log_Workflow obj = new Log_Workflow()
                    {
                        BusinessID = val.BusinessID,
                        DocumentKey = val.InvoiceKey,
                        DocumentNo = val.InvoiceNo,
                        CurrentStatus = curstatus,
                        CurrentStatusName = curstatusdes,
                        ToStatus = stable.ID,
                        ToStatusName = stable.Name,
                        ToDocumentKey = input.ToDocumentKey,
                        ToDocumentNo = input.ToDocumentNo,
                        Remark = input.remarkishas,
                        CreateDate = input.UpdateDate,
                        CreateBy = input.UpdateBy
                    };
                    Log_WorkflowRepository.Save(obj, context);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Invoice Get(string businessid, int InvoiceKey)
        {
            Document_Invoice response = new Document_Invoice();
            using (Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository())
                response = Document_InvoiceRepository.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.InvoiceKey.Value == InvoiceKey);
            if (response != null)
            {
                response.InvoiceDetail = new List<Document_InvoiceDetail>();
                using (Document_InvoiceDetailRepository Document_InvoiceDetailRepository = new Document_InvoiceDetailRepository())
                    response.InvoiceDetail = Document_InvoiceDetailRepository.SelectDataWithCondition(a => a.InvoiceKey == response.InvoiceKey).OrderBy(a => a.Sequence).ToList();
            }

            return response;
        }

        public void Delete(Document_Invoice input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Invoice>().Where(a => a.InvoiceKey == input.InvoiceKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_Invoice> GetWithRef(string businessid,string sid)
        {
            List<Document_Invoice> response = new List<Document_Invoice>();
            using (AccountingContext context = new AccountingContext())
            {
                if (string.IsNullOrEmpty(sid))
                    response = context.Set<Document_Invoice>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.InvoiceKey).ToList();
                else
                    response = context.Set<Document_Invoice>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive
                     && a.SaleID.ToString().ToUpper() == sid.ToUpper()).OrderByDescending(a => a.InvoiceKey).ToList();

                if (response != null && response.Count > 0)
                {
                    string[] documentconvert = response.ConvertAll(x => x.InvoiceNo).ToArray();
                    List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.InvoiceNo)).ToList();
                    foreach (Document_Invoice inv in response)
                    {
                        inv.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.InvoiceNo == inv.InvoiceNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            inv.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                inv.DocumentRefNo = inv.DocumentRefNo + docno;
                            }

                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                        }

                        if (!string.IsNullOrEmpty(inv.BillingNoteNo))
                        {
                            inv.DocumentRefNo = !string.IsNullOrEmpty(inv.DocumentRefNo) ? inv.DocumentRefNo : ",";
                            inv.DocumentRefNo = inv.DocumentRefNo + "," + inv.BillingNoteNo;
                        }
                        if (!string.IsNullOrEmpty(inv.ReceiptNo))
                        {
                            inv.DocumentRefNo = !string.IsNullOrEmpty(inv.DocumentRefNo) ? inv.DocumentRefNo : ",";
                            inv.DocumentRefNo = inv.DocumentRefNo + "," + inv.ReceiptNo;
                        }

                        inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                        inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                    }
                }
            }
            return response;
        }
    }
}
