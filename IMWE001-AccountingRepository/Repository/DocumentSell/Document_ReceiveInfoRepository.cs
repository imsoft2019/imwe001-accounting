﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_ReceiveInfoRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_ReceiveInfo, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        public Document_ReceiveInfo Save(Document_ReceiveInfo input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_ReceiveInfo>();
                        if (!input.SID.HasValue)
                        {
                            input.isdelete = 1;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            context.Add(input);
                            isnew = true;
                        }
                        context.SaveChanges();
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Save(Document_ReceiveInfo input, AccountingContext context)
        {
            var otable = context.Set<Document_ReceiveInfo>();
            input.isdelete = 1;
            input.CreateDate = input.UpdateDate;
            input.CreateBy = input.UpdateBy;
            context.Add(input);
            context.SaveChanges();
        }

        public void Delete(Document_ReceiveInfo input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_ReceiveInfo>().Where(a => a.SID == input.SID && a.DocumentKey == input.DocumentKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Delete(Document_ReceiveInfo input, AccountingContext context)
        {
            var otable = context.Set<Document_ReceiveInfo>().Where(a => a.SID == input.SID && a.DocumentKey == input.DocumentKey && a.BusinessID == input.BusinessID).FirstOrDefault();
            otable.isdelete = 0;
            otable.UpdateBy = input.UpdateBy;
            otable.UpdateDate = input.UpdateDate;
            context.SaveChanges();
        }
    }
}
