﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_BillingRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_Billing, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "BL";
        public Document_Billing Save(Document_Billing input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Billing>();
                        if (!input.BillingKey.HasValue)
                        {
                            Param_DocBillingStatus stable = context.Set<Param_DocBillingStatus>().Where(a => a.ID == "1").FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.BillingNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.BillingDate.Value.Day.ToString().PadLeft(2, '0'), input.BillingDate.Value.Month.ToString().PadLeft(2, '0'), input.BillingDate.Value.Year)
                            });
                            input.BillingStatus = stable.ID;
                            input.BillingStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบวางบิล";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบวางบิล";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบวางบิล เลขที่เอกสาร" + input.BillingNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            Document_Billing val = context.Set<Document_Billing>().Where(a => a.BusinessID == input.BusinessID && a.BillingKey == input.BillingKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบวางบิล");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.BillingStatus = input.BillingStatus;
                            val.BillingStatusName = input.BillingStatusName;
                            val.BillingType = input.BillingType;
                            val.BillingTypeName = input.BillingTypeName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.BillingDate = input.BillingDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบวางบิล";
                                request.ActivityTodo = "แก้ไขใบวางบิล";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบวางบิล เลขที่เอกสาร" + input.BillingNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();

                        Document_BillingDetailRepository Document_BillingDetailRepository = new Document_BillingDetailRepository();
                        if (!isnew)
                        {
                            Document_BillingDetailRepository.Delete(input.BillingKey.Value, context);
                            context.SaveChanges();

                            if (input.BillingDetail != null && input.BillingDetail.Count > 0)
                            {
                                Document_BillingDetailRepository.Add(input.BillingKey.Value, input.BusinessID, input.BillingDetail, context);
                            }

                            context.SaveChanges();

                            Document_BillingDetailRepository.Dispose();
                        }

                        // เพิ่ม Customer
                        if (!input.CustomerKey.HasValue)
                        {
                            Contact contact = new Contact()
                            {
                                BusinessID = input.BusinessID,
                                BusinessType = "C",
                                BusinessTypeName = "นิติบุคคล",
                                ContactType = "C",
                                ContactTypeName = "ลูกค้า",
                                BusinessName = input.CustomerName,
                                TaxID = input.CustomerTaxID,
                                BranchType = "H",
                                BranchTypeName = "สำนักงานใหญ่",
                                Address = input.CustomerAddress,
                                CreditDate = input.CreditDay,
                                ContactName = input.CustomerContactName,
                                ContactEmail = input.CustomerContactEmail,
                                ContactMobile = input.CustomerContactPhone,
                                isdelete = 1,
                                CreateDate = input.UpdateDate,
                                CreateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate,
                                UpdateBy = input.UpdateBy,
                                BranchName = input.CustomerBranch
                            };
                            var ctable = context.Set<Contact>();
                            ctable.Add(contact);
                            context.SaveChanges();

                            Document_Billing val = context.Set<Document_Billing>().Where(a => a.BusinessID == input.BusinessID && a.BillingKey == input.BillingKey).FirstOrDefault();
                            val.CustomerKey = contact.ContactKey;
                            context.SaveChanges();
                        }

                        if (input.BillingDetail != null && input.BillingDetail.Count > 0)
                        {
                            foreach (Document_BillingDetail object1 in input.BillingDetail)
                            {
                                // เพิ่ม Product
                                if (!object1.ProductKey.HasValue)
                                {
                                    var ptable = context.Set<Product>();
                                    Product product = new Product()
                                    {
                                        BusinessID = input.BusinessID,
                                        ProductType = "S",
                                        ProductTypeName = "บริการ",
                                        ProductName = object1.Name,
                                        ProductDescription = object1.Description,
                                        UnitTypeName = object1.Unit,
                                        SellPrice = object1.PriceBeforeTax,
                                        SellTaxPrice = object1.PriceTax,
                                        SellAfterTaxPrice = object1.PriceAfterTax,
                                        SellTaxType = "2",
                                        SellTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyTaxType = "2",
                                        BuyTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyPrice = object1.PriceBeforeTax,
                                        BuyAfterTaxPrice = object1.PriceTax,
                                        BuyTaxPrice = object1.PriceAfterTax,
                                        CreateBy = input.UpdateBy,
                                        CreateDate = input.UpdateDate,
                                        UpdateDate = input.UpdateDate,
                                        UpdateBy = input.UpdateBy,
                                        isdelete = 1
                                    };
                                    ptable.Add(product);
                                    context.SaveChanges();

                                    Document_BillingDetail valdetail = context.Set<Document_BillingDetail>().Where(a => a.BillingDetailKey == object1.BillingDetailKey && a.BillingKey == input.BillingKey).FirstOrDefault();
                                    valdetail.ProductKey = product.ProductKey;
                                    context.SaveChanges();
                                }

                                // เพิ่ม Unit
                                if (!string.IsNullOrEmpty(object1.Unit) && !string.IsNullOrEmpty(object1.Unit.Trim()))
                                {
                                    Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository();
                                    Param_UnitTypeRepository.Add(object1.Unit, context);
                                }
                            }
                        }

                        if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        {
                            //เก็บข้อมูลลง Reference
                            Document_Reference doc_ref = new Document_Reference()
                            {
                                BusinessID = input.BusinessID,
                                DocumentOwner = input.DocumentOwner,
                                DocumentGen = "BL",
                                QuotationNo = input.DocumentOwner == "QT" ? input.DocumentNo : string.Empty,
                                BillingNo = input.BillingNo,
                                CreateDate = input.CreateDate,
                                CreateBy = input.CreateBy
                            };
                            Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                            Document_ReferenceRepository.Add(doc_ref, context);

                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = input.DocumentKey,
                                remarkishas = "เปลี่ยนแปลงประเภทเอกสารใบเสนอราคาเป็นใบวางบิล",
                                ToDocumentKey = input.BillingKey,
                                ToDocumentNo = input.BillingNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                            Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository();
                            Document_QuotationRepository.SaveWorkflow(gen_ref, context);

                            Document_QuotationRepository.Dispose(); Document_ReferenceRepository.Dispose();
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Billing SaveCumulative(Document_Billing input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Billing>();
                        if (!input.BillingKey.HasValue)
                        {
                            Param_DocBillingStatus stable = context.Set<Param_DocBillingStatus>().Where(a => a.ID == "1").FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.BillingNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.BillingDate.Value.Day.ToString().PadLeft(2, '0'), input.BillingDate.Value.Month.ToString().PadLeft(2, '0'), input.BillingDate.Value.Year)
                            });
                            input.BillingStatus = stable.ID;
                            input.BillingStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;
                        }
                        else
                        {
                            Document_Billing val = context.Set<Document_Billing>().Where(a => a.BusinessID == input.BusinessID && a.BillingKey == input.BillingKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบวางบิลรวม");

                            val.BillingStatus = input.BillingStatus;
                            val.BillingStatusName = input.BillingStatusName;
                            val.BillingType = input.BillingType;
                            val.BillingTypeName = input.BillingTypeName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.BillingDate = input.BillingDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;
                        }
                        context.SaveChanges();

                        Document_BillingCumulativeDetailRepository Document_BillingCumulativeDetailRepository = new Document_BillingCumulativeDetailRepository();
                        if (!isnew)
                        {
                            /*Clear Values Before*/
                            List<Document_BillingCumulativeDetail> beforels = new List<Document_BillingCumulativeDetail>();
                            var stable = context.Set<Document_BillingCumulativeDetail>().Where(a => a.BillingKey == input.BillingKey);
                            foreach (Document_BillingCumulativeDetail ob1 in stable)
                            {
                                Document_Invoice invoice = new Document_Invoice();
                                invoice = context.Set<Document_Invoice>().Where(a => a.InvoiceKey == ob1.DocumentKey).FirstOrDefault();
                                if (invoice != null) { invoice.BillingNoteKey = default(int?); invoice.BillingNoteNo = string.Empty; }
                                Document_CreditNote creditnote = new Document_CreditNote();
                                creditnote = context.Set<Document_CreditNote>().Where(a => a.CreditNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (creditnote != null) { creditnote.BillingNoteKey = default(int?); creditnote.BillingNoteNo = string.Empty; }
                                Document_DebitNote debitnote = new Document_DebitNote();
                                debitnote = context.Set<Document_DebitNote>().Where(a => a.DebitNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (debitnote != null) { debitnote.BillingNoteKey = default(int?); debitnote.BillingNoteNo = string.Empty; }
                                context.SaveChanges();
                            }


                            Document_BillingCumulativeDetailRepository.Delete(input.BillingKey.Value, context);
                            context.SaveChanges();

                            if (input.BillingCumulativeDetail != null && input.BillingCumulativeDetail.Count > 0)
                            {
                                Document_BillingCumulativeDetailRepository.Add(input.BillingKey.Value, input.BusinessID, input.BillingCumulativeDetail, context);
                            }

                            context.SaveChanges();

                            Document_BillingCumulativeDetailRepository.Dispose();
                        }

                        /* Set Values & Workflow*/
                        foreach (Document_BillingCumulativeDetail detail in input.BillingCumulativeDetail)
                        {
                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = detail.DocumentKey,
                                remarkishas = string.Empty,
                                ToDocumentKey = input.BillingKey,
                                ToDocumentNo = input.BillingNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            if (detail.DocumentType == "INV")
                            {
                                Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository();
                                Document_InvoiceRepository.SaveWorkflowCumulative(gen_ref, context, Prefix);
                                Document_InvoiceRepository.Dispose();
                            }
                            else if (detail.DocumentType == "CN")
                            {
                                Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository();
                                Document_CreditNoteRepository.SaveWorkflowCumulative(gen_ref, context, Prefix);
                                Document_CreditNoteRepository.Dispose();
                            }
                            else if (detail.DocumentType == "DN")
                            {
                                Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository();
                                Document_DebitNoteRepository.SaveWorkflowCumulative(gen_ref, context, Prefix);
                                Document_DebitNoteRepository.Dispose();
                            }
                        }

                        //if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        //{
                        //    //เก็บข้อมูลลง Reference
                        //    Document_Reference doc_ref = new Document_Reference()
                        //    {
                        //        BusinessID = input.BusinessID,
                        //        DocumentOwner = input.DocumentOwner,
                        //        DocumentGen = "BL",
                        //        InvoiceNo = input.DocumentOwner == "INV" ? input.DocumentNo : string.Empty,
                        //        CreditNoteNo = input.DocumentOwner == "CN" ? input.DocumentNo : string.Empty,
                        //        DebitNoteNo = input.DocumentOwner == "DN" ? input.DocumentNo : string.Empty,
                        //        BillingNo = input.BillingNo,
                        //        CreateDate = input.CreateDate,
                        //        CreateBy = input.CreateBy
                        //    };
                        //    Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                        //    Document_ReferenceRepository.Add(doc_ref, context);

                        //    GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                        //    {
                        //        BusinessID = input.BusinessID,
                        //        DocumentKey = input.DocumentKey,
                        //        remarkishas = string.Format("เปลี่ยนแปลงประเภทเอกสาร{0}เป็นใบวางบิลรวม", input.DocumentOwner == "INV" ? "ใบแจ้งหนี้" : input.DocumentOwner == "CN" ? "ใบลดหนี้" : input.DocumentOwner == "DN" ? "ใบเพิ่มหนี้" : string.Empty),
                        //        ToDocumentKey = input.BillingKey,
                        //        ToDocumentNo = input.BillingNo,
                        //        UpdateBy = input.UpdateBy,
                        //        UpdateDate = input.UpdateDate.Value
                        //    };

                        //    if (input.DocumentOwner == "INV")
                        //    {
                        //        Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository();
                        //        Document_InvoiceRepository.SaveWorkflowNote(gen_ref, context, string.Empty);
                        //        Document_InvoiceRepository.Dispose();
                        //    }
                        //    else if (input.DocumentOwner == "CN")
                        //    {
                        //        Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository();
                        //        Document_CreditNoteRepository.SaveWorkflow(gen_ref, context);
                        //        Document_CreditNoteRepository.Dispose();
                        //    }
                        //    else if (input.DocumentOwner == "DN")
                        //    {
                        //        Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository();
                        //        Document_DebitNoteRepository.SaveWorkflow(gen_ref, context);
                        //        Document_DebitNoteRepository.Dispose();
                        //    }
                        //    // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว

                        //    context.SaveChanges();
                        //}
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(Document_Billing input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_Billing val = context.Set<Document_Billing>().Where(a => a.BusinessID == input.BusinessID && a.BillingKey == input.BillingKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบวางบิล");

                        Param_DocBillingStatus stable = context.Set<Param_DocBillingStatus>().Where(a => a.ID == input.BillingStatus).FirstOrDefault();

                        string curstatus = val.BillingStatus;
                        string curstatusdes = val.BillingStatusName;

                        val.BillingStatus = stable.ID;
                        val.BillingStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.BillingKey,
                            DocumentNo = val.BillingNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(GenerateDocumentRef input, AccountingContext context)
        {
            try
            {
                Document_Billing val = context.Set<Document_Billing>().Where(a => a.BusinessID == input.BusinessID && a.BillingKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบวางบิล");

                if (val.BillingStatus == "99")
                    throw new Exception("สถานะเอกสารไม่สามารถเปลี่ยนแปลงได้ เนื่องจากถูกเปลี่ยนแปลงเรียบร้อยแล้ว");

                Param_DocBillingStatus stable = context.Set<Param_DocBillingStatus>().Where(a => a.ID == "99").FirstOrDefault();

                string curstatus = val.BillingStatus;
                string curstatusdes = val.BillingStatusName;

                val.BillingStatus = stable.ID;
                val.BillingStatusName = stable.Name;
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                Log_Workflow obj = new Log_Workflow()
                {
                    BusinessID = val.BusinessID,
                    DocumentKey = val.BillingKey,
                    DocumentNo = val.BillingNo,
                    CurrentStatus = curstatus,
                    CurrentStatusName = curstatusdes,
                    ToStatus = stable.ID,
                    ToStatusName = stable.Name,
                    ToDocumentKey = input.ToDocumentKey,
                    ToDocumentNo = input.ToDocumentNo,
                    Remark = input.remarkishas,
                    CreateDate = input.UpdateDate,
                    CreateBy = input.UpdateBy
                };
                Log_WorkflowRepository.Save(obj, context);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflowCumulative(GenerateDocumentRef input, AccountingContext context)
        {
            try
            {
                Document_Billing val = context.Set<Document_Billing>().Where(a => a.BusinessID == input.BusinessID && a.BillingKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบวางบิลรวม");

                if (val.BillingStatus == "98")
                    throw new Exception("สถานะเอกสารไม่สามารถเปลี่ยนแปลงได้ เนื่องจากถูกเปลี่ยนแปลงเรียบร้อยแล้ว");

                Param_DocBillingStatus stable = context.Set<Param_DocBillingStatus>().Where(a => a.ID == "98").FirstOrDefault();

                string curstatus = val.BillingStatus;
                string curstatusdes = val.BillingStatusName;

                val.BillingStatus = stable.ID;
                val.BillingStatusName = stable.Name;
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                Log_Workflow obj = new Log_Workflow()
                {
                    BusinessID = val.BusinessID,
                    DocumentKey = val.BillingKey,
                    DocumentNo = val.BillingNo,
                    CurrentStatus = curstatus,
                    CurrentStatusName = curstatusdes,
                    ToStatus = stable.ID,
                    ToStatusName = stable.Name,
                    ToDocumentKey = input.ToDocumentKey,
                    ToDocumentNo = input.ToDocumentNo,
                    Remark = input.remarkishas,
                    CreateDate = input.UpdateDate,
                    CreateBy = input.UpdateBy
                };
                Log_WorkflowRepository.Save(obj, context);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Billing Get(string businessid, int BillingKey)
        {
            Document_Billing response = new Document_Billing();
            using (Document_BillingRepository Document_BillingRepository = new Document_BillingRepository())
                response = Document_BillingRepository.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.BillingKey.Value == BillingKey);
            if (response != null)
            {
                response.BillingDetail = new List<Document_BillingDetail>();
                using (Document_BillingDetailRepository Document_BillingDetailRepository = new Document_BillingDetailRepository())
                    response.BillingDetail = Document_BillingDetailRepository.SelectDataWithCondition(a => a.BillingKey == response.BillingKey).OrderBy(a => a.Sequence).ToList();

                response.BillingCumulativeDetail = new List<Document_BillingCumulativeDetail>();
                using (Document_BillingCumulativeDetailRepository Document_BillingCumulativeDetailRepository = new Document_BillingCumulativeDetailRepository())
                    response.BillingCumulativeDetail = Document_BillingCumulativeDetailRepository.SelectDataWithCondition(a => a.BillingKey == response.BillingKey).OrderBy(a => a.Sequence).ToList();
            }

            return response;
        }

        public void Delete(Document_Billing input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Billing>().Where(a => a.BillingKey == input.BillingKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();

                        if (otable.BillingType == "CBL")
                        {
                            List<Document_BillingCumulativeDetail> beforels = new List<Document_BillingCumulativeDetail>();
                            var stable = context.Set<Document_BillingCumulativeDetail>().Where(a => a.BillingKey == input.BillingKey);
                            foreach (Document_BillingCumulativeDetail ob1 in stable)
                            {
                                Document_Invoice invoice = new Document_Invoice();
                                invoice = context.Set<Document_Invoice>().Where(a => a.InvoiceKey == ob1.DocumentKey).FirstOrDefault();
                                if (invoice != null) { invoice.BillingNoteKey = default(int?); invoice.BillingNoteNo = string.Empty; }
                                Document_CreditNote creditnote = new Document_CreditNote();
                                creditnote = context.Set<Document_CreditNote>().Where(a => a.CreditNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (creditnote != null) { creditnote.BillingNoteKey = default(int?); creditnote.BillingNoteNo = string.Empty; }
                                Document_DebitNote debitnote = new Document_DebitNote();
                                debitnote = context.Set<Document_DebitNote>().Where(a => a.DebitNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (debitnote != null) { debitnote.BillingNoteKey = default(int?); debitnote.BillingNoteNo = string.Empty; }
                                context.SaveChanges();
                            }
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_Billing> GetWithRef(string businessid,string sid)
        {
            try
            {
                List<Document_Billing> response = new List<Document_Billing>();
                using (AccountingContext context = new AccountingContext())
                {
                    if (string.IsNullOrEmpty(sid))
                        response = context.Set<Document_Billing>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.BillingKey).ToList();
                    else
                        response = context.Set<Document_Billing>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive
                           && a.SaleID.ToString().ToUpper() == sid.ToUpper() ).OrderByDescending(a => a.BillingKey).ToList();
                    
                    if (response != null && response.Count > 0)
                    {
                        string[] documentconvert = response.ConvertAll(x => x.BillingNo).ToArray();
                        int[] documentkeyconvert = response.ConvertAll(x => x.BillingKey.Value).ToArray();
                        List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.BillingNo)).ToList();
                        List<Document_BillingCumulativeDetail> cumulativetable = context.Set<Document_BillingCumulativeDetail>().Where(a => documentkeyconvert.Contains(a.BillingKey.Value)).ToList();

                        foreach (Document_Billing bil in response)
                        {
                            bil.DocumentRefNo = string.Empty;
                            List<Document_Reference> dref = new List<Document_Reference>();
                            dref = rtable.Where(a => a.BillingNo == bil.BillingNo).ToList();
                            if (dref != null && dref.Count > 0)
                            {
                                bil.DocumentRefNo = ",";
                                foreach (Document_Reference bref in dref)
                                {
                                    string docno = string.Empty;
                                    if (!string.IsNullOrEmpty(bref.QuotationNo))
                                        docno = docno + "," + bref.QuotationNo;
                                    if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                        docno = docno + "," + bref.InvoiceNo;
                                    if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                        docno = docno + "," + bref.ReceiptNo;
                                    if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                        docno = docno + "," + bref.CashSaleNo;
                                    if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                        docno = docno + "," + bref.CreditNoteNo;
                                    if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                        docno = docno + "," + bref.DebitNoteNo;
                                    if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                        docno = docno + "," + bref.PurchaseNo;
                                    if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                        docno = docno + "," + bref.ReceivingInventoryNo;
                                    if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                        docno = docno + "," + bref.ExpenseNo;
                                    if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                        docno = docno + "," + bref.WithholdingTaxNo;

                                    bil.DocumentRefNo = bil.DocumentRefNo + docno;
                                }
                                bil.DocumentRefNo = bil.DocumentRefNo.Replace(",,", "");
                                bil.DocumentRefNo = bil.DocumentRefNo.Replace(",", "<br>");
                            }

                            if (bil.BillingType == "CBL")
                            {

                                List<Document_BillingCumulativeDetail> cumulative = cumulativetable.Where(a => a.BillingKey == bil.BillingKey).ToList();
                                if (cumulative != null && cumulative.Count > 0)
                                {
                                    bil.DocumentRefNo = !string.IsNullOrEmpty(bil.DocumentRefNo) ? bil.DocumentRefNo : ",";
                                    foreach (Document_BillingCumulativeDetail repeat in cumulative)
                                        bil.DocumentRefNo = bil.DocumentRefNo + "," + repeat.DocumentNo;

                                    bil.DocumentRefNo = bil.DocumentRefNo.Replace(",,", "");
                                    bil.DocumentRefNo = bil.DocumentRefNo.Replace(",", "<br>");
                                }
                            }
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DocumentCumulative GetDocumentCumulativeWithkey(string businessid, List<GetDocumentCumulative> input)
        {
            DocumentCumulative response = new DocumentCumulative();
            response.Document_Invoice = new List<Document_Invoice>();
            response.Document_DebitNote = new List<Document_DebitNote>();
            response.Document_CreditNote = new List<Document_CreditNote>();

            using (AccountingContext context = new AccountingContext())
            {
                var intable = context.Set<Document_Invoice>();
                var cretable = context.Set<Document_CreditNote>();
                var detable = context.Set<Document_DebitNote>();

                foreach (GetDocumentCumulative doc in input)
                {
                    int key = 0;
                    if (doc.documentype == "INV")
                        key = intable.Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.InvoiceKey.Value == doc.documentkey).FirstOrDefault().InvoiceKey.Value;
                    else if (doc.documentype == "CN")
                        key = cretable.Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.CreditNoteKey.Value == doc.documentkey).FirstOrDefault().InvoiceKey.Value;
                    else if (doc.documentype == "DN")
                        key = detable.Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.DebitNoteKey.Value == doc.documentkey).FirstOrDefault().InvoiceKey.Value;

                    Document_Invoice lsinvoice = new Document_Invoice();
                    lsinvoice = intable.Where(a => a.InvoiceKey.Value == key).FirstOrDefault();
                    if (lsinvoice != null)
                        response.Document_Invoice.Add(lsinvoice);

                    Document_CreditNote lscredit = new Document_CreditNote();
                    lscredit = cretable.Where(a => a.InvoiceKey.Value == key).FirstOrDefault();
                    if (lscredit != null)
                        response.Document_CreditNote.Add(lscredit);

                    Document_DebitNote lsdebit = new Document_DebitNote();
                    lsdebit = detable.Where(a => a.InvoiceKey.Value == key).FirstOrDefault();
                    if (lsdebit != null)
                        response.Document_DebitNote.Add(lsdebit);
                }
            }
            return response;
        }

    }
}
