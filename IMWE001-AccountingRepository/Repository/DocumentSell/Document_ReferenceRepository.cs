﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_ReferenceRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_Reference, AccountingContext>
    {
        public void Add(Document_Reference input , AccountingContext context)
        {
            var otable = context.Set<Document_Reference>();
            otable.Add(input);
            context.SaveChanges();
        }

    }
}
