﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.Response;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_AttachRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_Attach, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        public Document_Attach Save(Document_Attach input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Attach>();
                        if (!input.AttachKey.HasValue)
                        {
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.CreateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "แนบไฟล์เอกสาร";
                            request.ActivityTodo = "แนบไฟล์เอกสาร";
                            request.ActivityDesc = "ทำการแนบไฟล์เอกสาร เลขที่เอกสาร" + input.DocumentNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.CreateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            var dtable = context.Set<Document_Attach>().Where(a => a.BusinessID == input.BusinessID && a.AttachKey == input.AttachKey).FirstOrDefault();

                            if (dtable == null)
                                throw new Exception("ไม่พบข้อมูลไฟล์แนบ");

                            dtable.AttachPath = input.AttachPath;
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Document_Attach> Get(string businessid, int DocumentKey , string DocumentType )
        {
            List<Document_Attach> response = new List<Document_Attach>();
            using (Document_AttachRepository Document_AttachRepository = new Document_AttachRepository())
                response = Document_AttachRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.DocumentKey.Value == DocumentKey && a.DocumentType == DocumentType && a.isdelete == isactive);

            return response;
        }

        public void Delete(Document_Attach input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Attach>().Where(a => a.AttachKey == input.AttachKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}
