﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_CreditNoteDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_CreditNoteDetail, AccountingContext>
    {
        public void Add(int CreditNoteKey, Guid BusinessID, List<Document_CreditNoteDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_CreditNoteDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_CreditNoteDetail obj in request)
                {
                    obj.CreditNoteKey = CreditNoteKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int CreditNoteKey, AccountingContext context)
        {
            var otable = context.Set<Document_CreditNoteDetail>();
            List<Document_CreditNoteDetail> val = new List<Document_CreditNoteDetail>();
            val = otable.Where(a => a.CreditNoteKey.Value == CreditNoteKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_CreditNoteDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
