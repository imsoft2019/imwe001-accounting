﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_InvoiceDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_InvoiceDetail, AccountingContext>
    {
        public void Add(int InvoiceKey, Guid BusinessID, List<Document_InvoiceDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_InvoiceDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_InvoiceDetail obj in request)
                {
                    obj.InvoiceKey = InvoiceKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int InvoiceKey, AccountingContext context)
        {
            var otable = context.Set<Document_InvoiceDetail>();
            List<Document_InvoiceDetail> val = new List<Document_InvoiceDetail>();
            val = otable.Where(a => a.InvoiceKey.Value == InvoiceKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_InvoiceDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
