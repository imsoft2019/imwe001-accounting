﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_QuotationRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_Quotation, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "QT";
        public Document_Quotation Save(Document_Quotation input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Quotation>();
                        if (!input.QuotationKey.HasValue)
                        {
                            Param_DocQuotationStatus stable = context.Set<Param_DocQuotationStatus>().Where(a => a.ID == "1").FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.QuotationNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.QuotationDate.Value.Day.ToString().PadLeft(2, '0'), input.QuotationDate.Value.Month.ToString().PadLeft(2, '0'), input.QuotationDate.Value.Year)
                            });
                            input.QuotationStatus = stable.ID;
                            input.QuotationStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบเสนอราคา";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบเสนอราคา";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบเสนอราคา เลขที่เอกสาร" + input.QuotationNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {

                            Document_Quotation val = context.Set<Document_Quotation>().Where(a => a.BusinessID == input.BusinessID && a.QuotationKey == input.QuotationKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบเสนอราคา");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.QuotationStatus = input.QuotationStatus;
                            val.QuotationStatusName = input.QuotationStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.QuotationDate = input.QuotationDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบเสนอราคา";
                                request.ActivityTodo = "แก้ไขใบเสนอราคา";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบเสนอราคา เลขที่เอกสาร" + input.QuotationNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();

                        Document_QuotationDetailRepository Document_QuotationDetailRepository = new Document_QuotationDetailRepository();
                        if (!isnew)
                        {
                            Document_QuotationDetailRepository.Delete(input.QuotationKey.Value, context);
                            context.SaveChanges();

                            if (input.QuotationDetail != null && input.QuotationDetail.Count > 0)
                            {
                                Document_QuotationDetailRepository.Add(input.QuotationKey.Value, input.BusinessID, input.QuotationDetail, context);
                            }

                            context.SaveChanges();

                            Document_QuotationDetailRepository.Dispose();
                        }

                        // เพิ่ม Customer
                        if (!input.CustomerKey.HasValue)
                        {
                            Contact contact = new Contact()
                            {
                                BusinessID = input.BusinessID,
                                BusinessType = "C",
                                BusinessTypeName = "นิติบุคคล",
                                ContactType = "C",
                                ContactTypeName = "ลูกค้า",
                                BusinessName = input.CustomerName,
                                TaxID = input.CustomerTaxID,
                                BranchType = "H",
                                BranchTypeName = "สำนักงานใหญ่",
                                Address = input.CustomerAddress,
                                CreditDate = input.CreditDay,
                                ContactName = input.CustomerContactName,
                                ContactEmail = input.CustomerContactEmail,
                                ContactMobile = input.CustomerContactPhone,
                                isdelete = 1,
                                CreateDate = input.UpdateDate,
                                CreateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate,
                                UpdateBy = input.UpdateBy,
                                BranchName = input.CustomerBranch
                            };
                            var ctable = context.Set<Contact>();
                            ctable.Add(contact);
                            context.SaveChanges();

                            Document_Quotation val = context.Set<Document_Quotation>().Where(a => a.BusinessID == input.BusinessID && a.QuotationKey == input.QuotationKey).FirstOrDefault();
                            val.CustomerKey = contact.ContactKey;
                            context.SaveChanges();
                        }

                        if (input.QuotationDetail != null && input.QuotationDetail.Count > 0)
                        {
                            foreach (Document_QuotationDetail object1 in input.QuotationDetail)
                            {
                                // เพิ่ม Product
                                if (!object1.ProductKey.HasValue)
                                {
                                    var ptable = context.Set<Product>();
                                    Product product = new Product()
                                    {
                                        BusinessID = input.BusinessID,
                                        ProductType = "S",
                                        ProductTypeName = "บริการ",
                                        ProductName = object1.Name,
                                        ProductDescription = object1.Description,
                                        UnitTypeName = object1.Unit,
                                        SellPrice = object1.PriceBeforeTax,
                                        SellTaxPrice = object1.PriceTax,
                                        SellAfterTaxPrice = object1.PriceAfterTax,
                                        SellTaxType = "2",
                                        SellTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyTaxType = "2",
                                        BuyTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyPrice = object1.PriceBeforeTax,
                                        BuyAfterTaxPrice = object1.PriceTax,
                                        BuyTaxPrice = object1.PriceAfterTax,
                                        CreateBy = input.UpdateBy,
                                        CreateDate = input.UpdateDate,
                                        UpdateDate = input.UpdateDate,
                                        UpdateBy = input.UpdateBy,
                                        isdelete = 1
                                    };
                                    ptable.Add(product);
                                    context.SaveChanges();

                                    Document_QuotationDetail valdetail = context.Set<Document_QuotationDetail>().Where(a => a.QuotationDetailKey == object1.QuotationDetailKey && a.QuotationKey == input.QuotationKey).FirstOrDefault();
                                    valdetail.ProductKey = product.ProductKey;
                                    context.SaveChanges();
                                }

                                // เพิ่ม Unit
                                if (!string.IsNullOrEmpty(object1.Unit) && !string.IsNullOrEmpty(object1.Unit.Trim()))
                                {
                                    Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository();
                                    Param_UnitTypeRepository.Add(object1.Unit, context);
                                }
                            }
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(Document_Quotation input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_Quotation val = context.Set<Document_Quotation>().Where(a => a.BusinessID == input.BusinessID && a.QuotationKey == input.QuotationKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบเสนอราคา");

                        Param_DocQuotationStatus stable = context.Set<Param_DocQuotationStatus>().Where(a => a.ID == input.QuotationStatus).FirstOrDefault();

                        string curstatus = val.QuotationStatus;
                        string curstatusdes = val.QuotationStatusName;

                        val.QuotationStatus = stable.ID;
                        val.QuotationStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.QuotationKey,
                            DocumentNo = val.QuotationNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(GenerateDocumentRef input, AccountingContext context)
        {
            try
            {
                Document_Quotation val = context.Set<Document_Quotation>().Where(a => a.BusinessID == input.BusinessID && a.QuotationKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบเสนอราคา");

                if (val.QuotationStatus == "99")
                    throw new Exception("สถานะเอกสารไม่สามารถเปลี่ยนแปลงได้ เนื่องจากถูกเปลี่ยนแปลงเรียบร้อยแล้ว");

                Param_DocQuotationStatus stable = context.Set<Param_DocQuotationStatus>().Where(a => a.ID == "99").FirstOrDefault();

                string curstatus = val.QuotationStatus;
                string curstatusdes = val.QuotationStatusName;

                val.QuotationStatus = stable.ID;
                val.QuotationStatusName = stable.Name;
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                Log_Workflow obj = new Log_Workflow()
                {
                    BusinessID = val.BusinessID,
                    DocumentKey = val.QuotationKey,
                    DocumentNo = val.QuotationNo,
                    CurrentStatus = curstatus,
                    CurrentStatusName = curstatusdes,
                    ToStatus = stable.ID,
                    ToStatusName = stable.Name,
                    ToDocumentKey = input.ToDocumentKey,
                    ToDocumentNo = input.ToDocumentNo,
                    Remark = input.remarkishas,
                    CreateDate = input.UpdateDate,
                    CreateBy = input.UpdateBy
                };
                Log_WorkflowRepository.Save(obj, context);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Quotation Get(string businessid, int QuotationKey)
        {
            Document_Quotation response = new Document_Quotation();
            using (Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository())
                response = Document_QuotationRepository.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.QuotationKey.Value == QuotationKey);
            if (response != null)
            {
                response.QuotationDetail = new List<Document_QuotationDetail>();
                using (Document_QuotationDetailRepository Document_QuotationDetailRepository = new Document_QuotationDetailRepository())
                    response.QuotationDetail = Document_QuotationDetailRepository.SelectDataWithCondition(a => a.QuotationKey == response.QuotationKey).OrderBy(a => a.Sequence).ToList();
            }

            return response;
        }

        public void Delete(Document_Quotation input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Quotation>().Where(a => a.QuotationKey == input.QuotationKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_Quotation> GetWithRef(string businessid, string sid)
        {
            List<Document_Quotation> response = new List<Document_Quotation>();
            using (AccountingContext context = new AccountingContext())
            {
                if (string.IsNullOrEmpty(sid))
                    response = context.Set<Document_Quotation>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.QuotationKey).ToList();
                else
                    response = context.Set<Document_Quotation>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive 
                    && a.SaleID.ToString().ToUpper() == sid.ToUpper()).OrderByDescending(a => a.QuotationKey).ToList();

                if (response != null && response.Count > 0)
                {
                    string[] documentconvert = response.ConvertAll(x => x.QuotationNo).ToArray();
                    List< Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.QuotationNo)).ToList();
                    foreach (Document_Quotation quo in response)
                    {
                        quo.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.QuotationNo == quo.QuotationNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            quo.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                quo.DocumentRefNo = quo.DocumentRefNo + docno;
                            }
                            quo.DocumentRefNo = quo.DocumentRefNo.Replace(",,", "");
                            quo.DocumentRefNo = quo.DocumentRefNo.Replace(",", "<br>");
                        }
                    }
                }
            }
            return response;
        }
    }
}
