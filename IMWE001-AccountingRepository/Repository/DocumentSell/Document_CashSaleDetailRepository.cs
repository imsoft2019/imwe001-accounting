﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_CashSaleDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_CashSaleDetail, AccountingContext>
    {
        public void Add(int CashSaleKey, Guid BusinessID, List<Document_CashSaleDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_CashSaleDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_CashSaleDetail obj in request)
                {
                    obj.CashSaleKey = CashSaleKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int CashSaleKey, AccountingContext context)
        {
            var otable = context.Set<Document_CashSaleDetail>();
            List<Document_CashSaleDetail> val = new List<Document_CashSaleDetail>();
            val = otable.Where(a => a.CashSaleKey.Value == CashSaleKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_CashSaleDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
