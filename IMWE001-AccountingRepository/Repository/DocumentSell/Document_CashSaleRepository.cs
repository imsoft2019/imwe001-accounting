﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_CashSaleRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_CashSale, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "CA";
        public Document_CashSale Save(Document_CashSale input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_CashSale>();
                        if (!input.CashSaleKey.HasValue)
                        {
                            Param_DocCashSaleStatus stable = context.Set<Param_DocCashSaleStatus>().Where(a => a.ID == "1").FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.CashSaleNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.CashSaleDate.Value.Day.ToString().PadLeft(2, '0'), input.CashSaleDate.Value.Month.ToString().PadLeft(2, '0'), input.CashSaleDate.Value.Year)
                            });
                            input.CashSaleStatus = stable.ID;
                            input.CashSaleStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลเอกสารขายเงินสด";
                            request.ActivityTodo = "เพิ่ม/แก้ไขเอกสารขายเงินสด";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขเอกสารขายเงินสด เลขที่เอกสาร" + input.CashSaleNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            Document_CashSale val = context.Set<Document_CashSale>().Where(a => a.BusinessID == input.BusinessID && a.CashSaleKey == input.CashSaleKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.CashSaleStatus = input.CashSaleStatus;
                            val.CashSaleStatusName = input.CashSaleStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.CashSaleDate = input.CashSaleDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.IsAdjustDiscount = input.IsAdjustDiscount;
                            val.AdjustDiscountType = input.AdjustDiscountType;
                            val.AdjustDiscountTypeName = input.AdjustDiscountTypeName;
                            val.AdjustDiscountAmount = input.AdjustDiscountAmount;
                            val.PaymentAmount = input.PaymentAmount;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลเอกสารขายเงินสด";
                                request.ActivityTodo = "แก้ไขเอกสารขายเงินสด";
                                request.ActivityDesc = string.Format("ทำการแก้ไขเอกสารขายเงินสด เลขที่เอกสาร" + input.CashSaleNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();

                        Document_CashSaleDetailRepository Document_CashSaleDetailRepository = new Document_CashSaleDetailRepository();
                        if (!isnew)
                        {
                            Document_CashSaleDetailRepository.Delete(input.CashSaleKey.Value, context);
                            context.SaveChanges();

                            if (input.CashSaleDetail != null && input.CashSaleDetail.Count > 0)
                            {
                                Document_CashSaleDetailRepository.Add(input.CashSaleKey.Value, input.BusinessID, input.CashSaleDetail, context);
                            }

                            context.SaveChanges();

                            Document_CashSaleDetailRepository.Dispose();
                        }

                        if (!input.CustomerKey.HasValue)
                        {
                            Contact contact = new Contact()
                            {
                                BusinessID = input.BusinessID,
                                BusinessType = "C",
                                BusinessTypeName = "นิติบุคคล",
                                ContactType = "C",
                                ContactTypeName = "ลูกค้า",
                                BusinessName = input.CustomerName,
                                TaxID = input.CustomerTaxID,
                                BranchType = "H",
                                BranchTypeName = "สำนักงานใหญ่",
                                Address = input.CustomerAddress,
                                ContactName = input.CustomerContactName,
                                ContactEmail = input.CustomerContactEmail,
                                ContactMobile = input.CustomerContactPhone,
                                isdelete = 1,
                                CreateDate = input.UpdateDate,
                                CreateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate,
                                UpdateBy = input.UpdateBy,
                                BranchName = input.CustomerBranch
                            };
                            var ctable = context.Set<Contact>();
                            ctable.Add(contact);
                            context.SaveChanges();

                            Document_CashSale val = context.Set<Document_CashSale>().Where(a => a.BusinessID == input.BusinessID && a.CashSaleKey == input.CashSaleKey).FirstOrDefault();
                            val.CustomerKey = contact.ContactKey;
                            context.SaveChanges();
                        }

                        if (input.CashSaleDetail != null && input.CashSaleDetail.Count > 0)
                        {
                            foreach (Document_CashSaleDetail object1 in input.CashSaleDetail)
                            {
                                // เพิ่ม Product
                                if (!object1.ProductKey.HasValue)
                                {
                                    var ptable = context.Set<Product>();
                                    Product product = new Product()
                                    {
                                        BusinessID = input.BusinessID,
                                        ProductType = "S",
                                        ProductTypeName = "บริการ",
                                        ProductName = object1.Name,
                                        ProductDescription = object1.Description,
                                        UnitTypeName = object1.Unit,
                                        SellPrice = object1.PriceBeforeTax,
                                        SellTaxPrice = object1.PriceTax,
                                        SellAfterTaxPrice = object1.PriceAfterTax,
                                        SellTaxType = "2",
                                        SellTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyTaxType = "2",
                                        BuyTaxTypeName = "ราคาไม่รวม VAT",
                                        BuyPrice = object1.PriceBeforeTax,
                                        BuyAfterTaxPrice = object1.PriceTax,
                                        BuyTaxPrice = object1.PriceAfterTax,
                                        CreateBy = input.UpdateBy,
                                        CreateDate = input.UpdateDate,
                                        UpdateDate = input.UpdateDate,
                                        UpdateBy = input.UpdateBy,
                                        isdelete = 1
                                    };
                                    ptable.Add(product);
                                    context.SaveChanges();

                                    Document_CashSaleDetail valdetail = context.Set<Document_CashSaleDetail>().Where(a => a.CashSaleDetailKey == object1.CashSaleDetailKey && a.CashSaleKey == input.CashSaleKey).FirstOrDefault();
                                    valdetail.ProductKey = product.ProductKey;
                                    context.SaveChanges();
                                }

                                // เพิ่ม Unit
                                if (!string.IsNullOrEmpty(object1.Unit) && !string.IsNullOrEmpty(object1.Unit.Trim()))
                                {
                                    Param_UnitTypeRepository Param_UnitTypeRepository = new Param_UnitTypeRepository();
                                    Param_UnitTypeRepository.Add(object1.Unit, context);
                                }
                            }
                        }

                        if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        {
                            //เก็บข้อมูลลง Reference
                            Document_Reference doc_ref = new Document_Reference()
                            {
                                BusinessID = input.BusinessID,
                                DocumentOwner = input.DocumentOwner,
                                DocumentGen = "CA",
                                QuotationNo = input.DocumentOwner == "QT" ? input.DocumentNo : string.Empty,
                                CashSaleNo = input.CashSaleNo,
                                CreateDate = input.CreateDate,
                                CreateBy = input.CreateBy
                            };
                            Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                            Document_ReferenceRepository.Add(doc_ref, context);

                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = input.DocumentKey,
                                remarkishas = string.Format("เปลี่ยนแปลงประเภทเอกสาร{0}เป็นใบเสร็จรับเงิน (เงินสด)", input.DocumentOwner == "QT" ? "ใบเสนอราคา" : string.Empty),
                                ToDocumentKey = input.CashSaleKey,
                                ToDocumentNo = input.CashSaleNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            if (input.DocumentOwner == "QT")
                            {
                                // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                Document_QuotationRepository Document_QuotationRepository = new Document_QuotationRepository();
                                Document_QuotationRepository.SaveWorkflow(gen_ref, context);
                                Document_QuotationRepository.Dispose();
                            }
                            Document_ReferenceRepository.Dispose();
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(Document_CashSale input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_CashSale val = context.Set<Document_CashSale>().Where(a => a.BusinessID == input.BusinessID && a.CashSaleKey == input.CashSaleKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน (เงินสด)");

                        Param_DocCashSaleStatus stable = context.Set<Param_DocCashSaleStatus>().Where(a => a.ID == input.CashSaleStatus).FirstOrDefault();

                        string curstatus = val.CashSaleStatus;
                        string curstatusdes = val.CashSaleStatusName;

                        val.CashSaleStatus = stable.ID;
                        val.CashSaleStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        if (input.CashSaleStatus == "1") //Reset
                        {
                            var rtable = context.Set<Document_ReceiveInfo>();
                            List<Document_ReceiveInfo> ls = new List<Document_ReceiveInfo>();
                            ls = rtable.Where(a => a.DocumentKey == input.CashSaleKey && a.BusinessID == input.BusinessID && a.isdelete == isactive).ToList();
                            if (ls != null && ls.Count > 0)
                            {
                                foreach (Document_ReceiveInfo rev in ls)
                                {
                                    Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository();
                                    rev.UpdateBy = input.UpdateBy;
                                    rev.UpdateDate = input.UpdateDate;
                                    Document_ReceiveInfoRepository.Delete(rev, context);
                                }
                            }
                        }

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.CashSaleKey,
                            DocumentNo = val.CashSaleNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_CashSale Get(string businessid, int CashSaleKey)
        {
            Document_CashSale response = new Document_CashSale();
            using (Document_CashSaleRepository Document_CashSaleRepository = new Document_CashSaleRepository())
                response = Document_CashSaleRepository.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.CashSaleKey.Value == CashSaleKey);
            if (response != null)
            {
                response.CashSaleDetail = new List<Document_CashSaleDetail>();
                using (Document_CashSaleDetailRepository Document_CashSaleDetailRepository = new Document_CashSaleDetailRepository())
                    response.CashSaleDetail = Document_CashSaleDetailRepository.SelectDataWithCondition(a => a.CashSaleKey == response.CashSaleKey).OrderBy(a => a.Sequence).ToList();

                response.ReceiptInfo = new Document_ReceiveInfo();
                using (Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository())
                    response.ReceiptInfo = Document_ReceiveInfoRepository.SelectFirstDataWithCondition(a => a.DocumentKey == response.CashSaleKey && a.DocumentType == Prefix && a.isdelete == isactive);
            }

            return response;
        }

        public void Delete(Document_CashSale input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_CashSale>().Where(a => a.CashSaleKey == input.CashSaleKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();

                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_CashSale> GetWithRef(string businessid, string sid)
        {
            List<Document_CashSale> response = new List<Document_CashSale>();
            using (AccountingContext context = new AccountingContext())
            {
                if (string.IsNullOrEmpty(sid))
                    response = context.Set<Document_CashSale>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.CashSaleKey).ToList();
                else
                    response = context.Set<Document_CashSale>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive
                      && a.SaleID.ToString().ToUpper() == sid.ToUpper()).OrderByDescending(a => a.CashSaleKey).ToList();

                if (response != null && response.Count > 0)
                {

                    string[] documentconvert = response.ConvertAll(x => x.CashSaleNo).ToArray();
                    int[] documentkeyconvert = response.ConvertAll(x => x.CashSaleKey.Value).ToArray();
                    List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.CashSaleNo)).ToList();
                    List<Document_ReceiveInfo> infotable = context.Set<Document_ReceiveInfo>().Where(a => documentkeyconvert.Contains(a.DocumentKey.Value) && a.DocumentType == Prefix && a.isdelete == isactive).ToList();

                    foreach (Document_CashSale inv in response)
                    {
                        inv.ReceiptInfo = new Document_ReceiveInfo();
                        inv.ReceiptInfo = infotable.Where(a => a.DocumentKey == inv.CashSaleKey).FirstOrDefault();

                        //inv.ReceiptInfo = new Document_ReceiveInfo();
                        //using (Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository())
                        //    inv.ReceiptInfo = Document_ReceiveInfoRepository.SelectFirstDataWithCondition(a => a.DocumentKey == inv.CashSaleKey && a.DocumentType == Prefix && a.isdelete == isactive);

                        inv.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.CashSaleNo == inv.CashSaleNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            inv.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                inv.DocumentRefNo = inv.DocumentRefNo + docno;
                            }
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                        }
                    }
                }
            }
            return response;
        }

        public Document_ReceiveInfo SaveReceivePayment(Document_ReceiveInfo input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_CashSale val = context.Set<Document_CashSale>().Where(a => a.BusinessID == input.BusinessID && a.CashSaleKey == input.DocumentKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน");

                        Param_DocCashSaleStatus stable = context.Set<Param_DocCashSaleStatus>().Where(a => a.ID == "99").FirstOrDefault();

                        string curstatus = val.CashSaleStatus;
                        string curstatusdes = val.CashSaleStatusName;

                        val.CashSaleStatus = stable.ID;
                        val.CashSaleStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;
                        context.SaveChanges();

                        Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository();
                        Document_ReceiveInfoRepository.Save(input, context);

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.CashSaleKey,
                            DocumentNo = val.CashSaleNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
