﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_ReceiptCumulativeDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_ReceiptCumulativeDetail, AccountingContext>
    {
        public void Add(int ReceiptKey, Guid BusinessID, List<Document_ReceiptCumulativeDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_ReceiptCumulativeDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_ReceiptCumulativeDetail obj in request)
                {
                    obj.ReceiptKey = ReceiptKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int ReceiptKey, AccountingContext context)
        {
            var otable = context.Set<Document_ReceiptCumulativeDetail>();
            List<Document_ReceiptCumulativeDetail> val = new List<Document_ReceiptCumulativeDetail>();
            val = otable.Where(a => a.ReceiptKey.Value == ReceiptKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_ReceiptCumulativeDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
