﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_BillingCumulativeDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_BillingCumulativeDetail, AccountingContext>
    {
        public void Add(int BillingKey, Guid BusinessID, List<Document_BillingCumulativeDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_BillingCumulativeDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_BillingCumulativeDetail obj in request)
                {
                    obj.BillingKey = BillingKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int BillingKey, AccountingContext context)
        {
            var otable = context.Set<Document_BillingCumulativeDetail>();
            List<Document_BillingCumulativeDetail> val = new List<Document_BillingCumulativeDetail>();
            val = otable.Where(a => a.BillingKey.Value == BillingKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_BillingCumulativeDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
