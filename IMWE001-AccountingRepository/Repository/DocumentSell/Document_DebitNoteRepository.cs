﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_DebitNoteRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_DebitNote, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "DN";
        public Document_DebitNote Save(Document_DebitNote input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_DebitNote>();
                        if (!input.DebitNoteKey.HasValue)
                        {
                            string flagstate = "1";

                            Param_DocDebitNoteStatus stable = context.Set<Param_DocDebitNoteStatus>().Where(a => a.ID == flagstate).FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.DebitNoteNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.DebitNoteDate.Value.Day.ToString().PadLeft(2, '0'), input.DebitNoteDate.Value.Month.ToString().PadLeft(2, '0'), input.DebitNoteDate.Value.Year)
                            });
                            input.DebitNoteStatus = stable.ID;
                            input.DebitNoteStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบเพิ่มหนี้";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบเพิ่มหนี้";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบเพิ่มหนี้ เลขที่เอกสาร" + input.DebitNoteNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            Document_DebitNote val = context.Set<Document_DebitNote>().Where(a => a.BusinessID == input.BusinessID && a.DebitNoteKey == input.DebitNoteKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบเพิ่มหนี้");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.DebitNoteStatus = input.DebitNoteStatus;
                            val.DebitNoteStatusName = input.DebitNoteStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.DebitNoteDate = input.DebitNoteDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.CauseType = input.CauseType;
                            val.CauseTypeName = input.CauseTypeName;
                            val.CauseTypeOther = input.CauseTypeOther;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.DebitPriceBefore = input.DebitPriceBefore;
                            val.DebitPriceReal = input.DebitPriceReal;
                            val.DebitPriceAfter = input.DebitPriceAfter;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบเพิ่มหนี้";
                                request.ActivityTodo = "แก้ไขใบเพิ่มหนี้";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบเพิ่มหนี้ เลขที่เอกสาร" + input.DebitNoteNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();

                        Document_DebitNoteDetailRepository Document_DebitNoteDetailRepository = new Document_DebitNoteDetailRepository();
                        if (!isnew)
                        {
                            Document_DebitNoteDetailRepository.Delete(input.DebitNoteKey.Value, context);
                            context.SaveChanges();

                            if (input.DebitNoteDetail != null && input.DebitNoteDetail.Count > 0)
                            {
                                Document_DebitNoteDetailRepository.Add(input.DebitNoteKey.Value, input.BusinessID, input.DebitNoteDetail, context);
                            }

                            context.SaveChanges();

                            Document_DebitNoteDetailRepository.Dispose();
                        }

                        if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        {
                            //เก็บข้อมูลลง Reference
                            Document_Reference doc_ref = new Document_Reference()
                            {
                                BusinessID = input.BusinessID,
                                DocumentOwner = input.DocumentOwner,
                                DocumentGen = Prefix,
                                InvoiceNo = input.DocumentOwner == "INV" ? input.DocumentNo : string.Empty,
                                DebitNoteNo = input.DebitNoteNo,
                                CreateDate = input.CreateDate,
                                CreateBy = input.CreateBy
                            };
                            Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                            Document_ReferenceRepository.Add(doc_ref, context);

                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = input.DocumentKey,
                                remarkishas = string.Format("สร้างประเภทเอกสารใบแจ้งหนี้ราคาเป็น{0}/ใบเพิ่มหนี้", input.DocumentOwner == "INV" ? "ใบแจ้งหนี้" : string.Empty),
                                ToDocumentKey = input.DebitNoteKey,
                                ToDocumentNo = input.DebitNoteNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            if (input.DocumentOwner == "INV")
                            {
                                // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository();
                                Document_InvoiceRepository.SaveWorkflowNote(gen_ref, context,Prefix);
                                Document_InvoiceRepository.Dispose();
                            }
                            Document_ReferenceRepository.Dispose();
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(Document_DebitNote input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_DebitNote val = context.Set<Document_DebitNote>().Where(a => a.BusinessID == input.BusinessID && a.DebitNoteKey == input.DebitNoteKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                        Param_DocDebitNoteStatus stable = context.Set<Param_DocDebitNoteStatus>().Where(a => a.ID == input.DebitNoteStatus).FirstOrDefault();

                        string curstatus = val.DebitNoteStatus;
                        string curstatusdes = val.DebitNoteStatusName;

                        val.DebitNoteStatus = stable.ID;
                        val.DebitNoteStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.DebitNoteKey,
                            DocumentNo = val.DebitNoteNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(GenerateDocumentRef input, AccountingContext context)
        {
            try
            {
                Document_DebitNote val = context.Set<Document_DebitNote>().Where(a => a.BusinessID == input.BusinessID && a.DebitNoteKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบวางบิล");

                if (val.DebitNoteStatus == "99")
                    throw new Exception("สถานะเอกสารไม่สามารถเปลี่ยนแปลงได้ เนื่องจากถูกเปลี่ยนแปลงเรียบร้อยแล้ว");

                Param_DocDebitNoteStatus stable = context.Set<Param_DocDebitNoteStatus>().Where(a => a.ID == "2").FirstOrDefault();

                string curstatus = val.DebitNoteStatus;
                string curstatusdes = val.DebitNoteStatusName;

                val.DebitNoteStatus = stable.ID;
                val.DebitNoteStatusName = stable.Name;
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                Log_Workflow obj = new Log_Workflow()
                {
                    BusinessID = val.BusinessID,
                    DocumentKey = val.DebitNoteKey,
                    DocumentNo = val.DebitNoteNo,
                    CurrentStatus = curstatus,
                    CurrentStatusName = curstatusdes,
                    ToStatus = stable.ID,
                    ToStatusName = stable.Name,
                    CreateDate = input.UpdateDate,
                    CreateBy = input.UpdateBy
                };
                Log_WorkflowRepository.Save(obj, context);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflowCumulative(GenerateDocumentRef input, AccountingContext context, string DocumentType)
        {
            try
            {
                Document_DebitNote val = context.Set<Document_DebitNote>().Where(a => a.BusinessID == input.BusinessID && a.DebitNoteKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบลดหนี้");

                Param_DocDebitNoteStatus stable = context.Set<Param_DocDebitNoteStatus>().Where(a => a.ID == (DocumentType == "BL" ? "2" : DocumentType == "RE" ? "99" : "1")).FirstOrDefault();

                string curstatus = val.DebitNoteStatus;
                string curstatusdes = val.DebitNoteStatusName;

                val.DebitNoteStatus = stable.ID;
                val.DebitNoteStatusName = stable.Name;
                if (DocumentType == "BL")
                {
                    val.BillingNoteKey = input.ToDocumentKey;
                    val.BillingNoteNo = input.ToDocumentNo;
                }
                else if (DocumentType == "RE")
                {
                    val.ReceiptKey = input.ToDocumentKey;
                    val.ReceiptNo = input.ToDocumentNo;
                }
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                if (curstatus != "2" && curstatus != "99")
                {
                    Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                    Log_Workflow obj = new Log_Workflow()
                    {
                        BusinessID = val.BusinessID,
                        DocumentKey = val.DebitNoteKey,
                        DocumentNo = val.DebitNoteNo,
                        CurrentStatus = curstatus,
                        CurrentStatusName = curstatusdes,
                        ToStatus = stable.ID,
                        ToStatusName = stable.Name,
                        ToDocumentKey = input.ToDocumentKey,
                        ToDocumentNo = input.ToDocumentNo,
                        Remark = input.remarkishas,
                        CreateDate = input.UpdateDate,
                        CreateBy = input.UpdateBy
                    };
                    Log_WorkflowRepository.Save(obj, context);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_DebitNote Get(string businessid, int DebitNoteKey)
        {
            Document_DebitNote response = new Document_DebitNote();
            using (Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository())
                response = Document_DebitNoteRepository.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.DebitNoteKey.Value == DebitNoteKey);
            if (response != null)
            {
                response.DebitNoteDetail = new List<Document_DebitNoteDetail>();
                using (Document_DebitNoteDetailRepository Document_DebitNoteDetailRepository = new Document_DebitNoteDetailRepository())
                    response.DebitNoteDetail = Document_DebitNoteDetailRepository.SelectDataWithCondition(a => a.DebitNoteKey == response.DebitNoteKey).OrderBy(a => a.Sequence).ToList();

                response.InvoiceDetail = new List<Document_InvoiceDetail>();
                using (Document_InvoiceDetailRepository Document_InvoiceDetailRepository = new Document_InvoiceDetailRepository())
                    response.InvoiceDetail = Document_InvoiceDetailRepository.SelectDataWithCondition(a => a.InvoiceKey == response.InvoiceKey).OrderBy(a => a.Sequence).ToList();
            }

            return response;
        }

        public void Delete(Document_DebitNote input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_DebitNote>().Where(a => a.DebitNoteKey == input.DebitNoteKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_DebitNote> GetWithRef(string businessid)
        {
            List<Document_DebitNote> response = new List<Document_DebitNote>();
            using (AccountingContext context = new AccountingContext())
            {
                response = context.Set<Document_DebitNote>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.DebitNoteKey).ToList();
                if (response != null && response.Count > 0)
                {
                    string[] documentconvert = response.ConvertAll(x => x.DebitNoteNo).ToArray();
                    List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.DebitNoteNo)).ToList();
                    foreach (Document_DebitNote inv in response)
                    {
                        inv.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.DebitNoteNo == inv.DebitNoteNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            inv.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                inv.DocumentRefNo = inv.DocumentRefNo + docno;
                            }
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                        }

                        

                        if (!string.IsNullOrEmpty(inv.BillingNoteNo))
                        {
                            inv.DocumentRefNo = !string.IsNullOrEmpty(inv.DocumentRefNo) ? inv.DocumentRefNo : ",";
                            inv.DocumentRefNo = inv.DocumentRefNo + "," + inv.BillingNoteNo;
                        }
                        if (!string.IsNullOrEmpty(inv.ReceiptNo))
                        {
                            inv.DocumentRefNo = !string.IsNullOrEmpty(inv.DocumentRefNo) ? inv.DocumentRefNo : ",";
                            inv.DocumentRefNo = inv.DocumentRefNo + "," + inv.ReceiptNo;
                        }

                        inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                        inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                    }
                }
            }
            return response;
        }
    }
}
