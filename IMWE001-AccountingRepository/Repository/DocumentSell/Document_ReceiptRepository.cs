﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_ReceiptRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_Receipt, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "RE";
        public Document_Receipt Save(Document_Receipt input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Receipt>();
                        if (!input.ReceiptKey.HasValue)
                        {
                            Param_DocReceiptStatus stable = context.Set<Param_DocReceiptStatus>().Where(a => a.ID == "1").FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.ReceiptNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.ReceiptDate.Value.Day.ToString().PadLeft(2, '0'), input.ReceiptDate.Value.Month.ToString().PadLeft(2, '0'), input.ReceiptDate.Value.Year)
                            });
                            input.ReceiptStatus = stable.ID;
                            input.ReceiptStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบเสร็จรับเงิน";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบเสร็จรับเงิน";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบเสร็จรับเงิน เลขที่เอกสาร" + input.ReceiptNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            Document_Receipt val = context.Set<Document_Receipt>().Where(a => a.BusinessID == input.BusinessID && a.ReceiptKey == input.ReceiptKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.ReceiptStatus = input.ReceiptStatus;
                            val.ReceiptStatusName = input.ReceiptStatusName;
                            val.ReceiptType = input.ReceiptType;
                            val.ReceiptTypeName = input.ReceiptTypeName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.ReceiptDate = input.ReceiptDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.IsAdjustDiscount = input.IsAdjustDiscount;
                            val.AdjustDiscountType = input.AdjustDiscountType;
                            val.AdjustDiscountTypeName = input.AdjustDiscountTypeName;
                            val.AdjustDiscountAmount = input.AdjustDiscountAmount;
                            val.PaymentAmount = input.PaymentAmount;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบเสร็จรับเงิน";
                                request.ActivityTodo = "แก้ไขใบเสร็จรับเงิน";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบเสร็จรับเงิน เลขที่เอกสาร" + input.ReceiptNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();

                        Document_ReceiptDetailRepository Document_ReceiptDetailRepository = new Document_ReceiptDetailRepository();
                        if (!isnew)
                        {
                            Document_ReceiptDetailRepository.Delete(input.ReceiptKey.Value, context);
                            context.SaveChanges();

                            if (input.ReceiptDetail != null && input.ReceiptDetail.Count > 0)
                            {
                                Document_ReceiptDetailRepository.Add(input.ReceiptKey.Value, input.BusinessID, input.ReceiptDetail, context);
                            }

                            context.SaveChanges();

                            Document_ReceiptDetailRepository.Dispose();
                        }

                        if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        {
                            //เก็บข้อมูลลง Reference
                            Document_Reference doc_ref = new Document_Reference()
                            {
                                BusinessID = input.BusinessID,
                                DocumentOwner = input.DocumentOwner,
                                DocumentGen = "RE",
                                QuotationNo = input.DocumentOwner == "QT" ? input.DocumentNo : string.Empty,
                                InvoiceNo = input.DocumentOwner == "INV" ? input.DocumentNo : string.Empty,
                                ReceiptNo = input.ReceiptNo,
                                CreateDate = input.CreateDate,
                                CreateBy = input.CreateBy
                            };
                            Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                            Document_ReferenceRepository.Add(doc_ref, context);

                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = input.DocumentKey,
                                remarkishas = string.Format("เปลี่ยนแปลงประเภทเอกสาร{0}เป็นใบเสร็จรับเงิน", input.DocumentOwner == "INV" ? "ใบแจ้งหนี้" : string.Empty),
                                ToDocumentKey = input.ReceiptKey,
                                ToDocumentNo = input.ReceiptNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            if (input.DocumentOwner == "INV")
                            {
                                // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository();
                                Document_InvoiceRepository.SaveWorkflow(gen_ref, context);
                                Document_InvoiceRepository.Dispose();
                            }
                            Document_ReferenceRepository.Dispose();
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Receipt SaveCumulative(Document_Receipt input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Receipt>();
                        if (!input.ReceiptKey.HasValue)
                        {
                            Param_DocReceiptStatus stable = context.Set<Param_DocReceiptStatus>().Where(a => a.ID == "1").FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.ReceiptNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.ReceiptDate.Value.Day.ToString().PadLeft(2, '0'), input.ReceiptDate.Value.Month.ToString().PadLeft(2, '0'), input.ReceiptDate.Value.Year)
                            });
                            input.ReceiptStatus = stable.ID;
                            input.ReceiptStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;
                        }
                        else
                        {
                            Document_Receipt val = context.Set<Document_Receipt>().Where(a => a.BusinessID == input.BusinessID && a.ReceiptKey == input.ReceiptKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบเสร็จรวม");

                            val.ReceiptStatus = input.ReceiptStatus;
                            val.ReceiptStatusName = input.ReceiptStatusName;
                            val.ReceiptType = input.ReceiptType;
                            val.ReceiptTypeName = input.ReceiptTypeName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.ReceiptDate = input.ReceiptDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.IsAdjustDiscount = input.IsAdjustDiscount;
                            val.AdjustDiscountType = input.AdjustDiscountType;
                            val.AdjustDiscountTypeName = input.AdjustDiscountTypeName;
                            val.AdjustDiscountAmount = input.AdjustDiscountAmount;
                            val.PaymentAmount = input.PaymentAmount;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;
                        }
                        context.SaveChanges();

                        Document_ReceiptCumulativeDetailRepository Document_ReceiptCumulativeDetailRepository = new Document_ReceiptCumulativeDetailRepository();
                        if (!isnew)
                        {
                            /*Clear Values Before*/
                            List<Document_ReceiptCumulativeDetail> beforels = new List<Document_ReceiptCumulativeDetail>();
                            var stable = context.Set<Document_ReceiptCumulativeDetail>().Where(a => a.ReceiptKey == input.ReceiptKey);
                            foreach (Document_ReceiptCumulativeDetail ob1 in stable)
                            {
                                Document_Invoice invoice = new Document_Invoice();
                                invoice = context.Set<Document_Invoice>().Where(a => a.InvoiceKey == ob1.DocumentKey).FirstOrDefault();
                                if (invoice != null) { invoice.ReceiptKey = default(int?); invoice.ReceiptNo = string.Empty; }
                                Document_CreditNote creditnote = new Document_CreditNote();
                                creditnote = context.Set<Document_CreditNote>().Where(a => a.CreditNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (creditnote != null) { creditnote.ReceiptKey = default(int?); creditnote.ReceiptNo = string.Empty; }
                                Document_DebitNote debitnote = new Document_DebitNote();
                                debitnote = context.Set<Document_DebitNote>().Where(a => a.DebitNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (debitnote != null) { debitnote.ReceiptKey = default(int?); debitnote.ReceiptNo = string.Empty; }
                                context.SaveChanges();
                            }


                            Document_ReceiptCumulativeDetailRepository.Delete(input.ReceiptKey.Value, context);
                            context.SaveChanges();

                            if (input.ReceiptCumulativeDetail != null && input.ReceiptCumulativeDetail.Count > 0)
                            {
                                Document_ReceiptCumulativeDetailRepository.Add(input.ReceiptKey.Value, input.BusinessID, input.ReceiptCumulativeDetail, context);
                            }

                            context.SaveChanges();

                            Document_ReceiptCumulativeDetailRepository.Dispose();
                        }

                        if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        {
                            if (input.DocumentOwner == "BL")
                            {
                                //เก็บข้อมูลลง Reference
                                Document_Reference doc_ref = new Document_Reference()
                                {
                                    BusinessID = input.BusinessID,
                                    DocumentOwner = input.DocumentOwner,
                                    DocumentGen = "RE",
                                    BillingNo = input.DocumentNo,
                                    ReceiptNo = input.ReceiptNo,
                                    CreateDate = input.CreateDate,
                                    CreateBy = input.CreateBy
                                };
                                Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                                Document_ReferenceRepository.Add(doc_ref, context);

                                GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                                {
                                    BusinessID = input.BusinessID,
                                    DocumentKey = input.DocumentKey,
                                    remarkishas = string.Format("เปลี่ยนแปลงประเภทเอกสาร{0}เป็นใบเสร็จรวม", input.DocumentOwner == "BL" ? "ใบวางบิลรวม" : string.Empty),
                                    ToDocumentKey = input.ReceiptKey,
                                    ToDocumentNo = input.ReceiptNo,
                                    UpdateBy = input.UpdateBy,
                                    UpdateDate = input.UpdateDate.Value
                                };

                                if (input.DocumentOwner == "BL")
                                {
                                    // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                    Document_BillingRepository Document_BillingRepository = new Document_BillingRepository();
                                    Document_BillingRepository.SaveWorkflowCumulative(gen_ref, context);
                                    Document_BillingRepository.Dispose();
                                }
                                Document_ReferenceRepository.Dispose();
                                context.SaveChanges();
                            }
                        }

                        /* Set Values & Workflow*/
                        foreach (Document_ReceiptCumulativeDetail detail in input.ReceiptCumulativeDetail)
                        {
                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = detail.DocumentKey,
                                remarkishas = string.Empty,
                                ToDocumentKey = input.ReceiptKey,
                                ToDocumentNo = input.ReceiptNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            if (detail.DocumentType == "INV")
                            {
                                Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository();
                                Document_InvoiceRepository.SaveWorkflowCumulative(gen_ref, context, Prefix);
                                Document_InvoiceRepository.Dispose();
                            }
                            else if (detail.DocumentType == "CN")
                            {
                                Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository();
                                Document_CreditNoteRepository.SaveWorkflowCumulative(gen_ref, context, Prefix);
                                Document_CreditNoteRepository.Dispose();
                            }
                            else if (detail.DocumentType == "DN")
                            {
                                Document_DebitNoteRepository Document_DebitNoteRepository = new Document_DebitNoteRepository();
                                Document_DebitNoteRepository.SaveWorkflowCumulative(gen_ref, context, Prefix);
                                Document_DebitNoteRepository.Dispose();
                            }
                        }

                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void SaveWorkflow(Document_Receipt input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_Receipt val = context.Set<Document_Receipt>().Where(a => a.BusinessID == input.BusinessID && a.ReceiptKey == input.ReceiptKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน");

                        Param_DocReceiptStatus stable = context.Set<Param_DocReceiptStatus>().Where(a => a.ID == input.ReceiptStatus).FirstOrDefault();

                        string curstatus = val.ReceiptStatus;
                        string curstatusdes = val.ReceiptStatusName;

                        val.ReceiptStatus = stable.ID;
                        val.ReceiptStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        if (input.ReceiptStatus == "1") //Reset
                        {
                            var rtable = context.Set<Document_ReceiveInfo>();
                            List<Document_ReceiveInfo> ls = new List<Document_ReceiveInfo>();
                            ls = rtable.Where(a => a.DocumentKey == input.ReceiptKey && a.BusinessID == input.BusinessID && a.isdelete == isactive).ToList();
                            if (ls != null && ls.Count > 0)
                            {
                                foreach (Document_ReceiveInfo rev in ls)
                                {
                                    Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository();
                                    rev.UpdateBy = input.UpdateBy;
                                    rev.UpdateDate = input.UpdateDate;
                                    Document_ReceiveInfoRepository.Delete(rev, context);
                                }
                            }
                        }

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.ReceiptKey,
                            DocumentNo = val.ReceiptNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_Receipt Get(string businessid, int ReceiptKey)
        {
            Document_Receipt response = new Document_Receipt();
            using (Document_ReceiptRepository Document_ReceiptRepository = new Document_ReceiptRepository())
                response = Document_ReceiptRepository.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.ReceiptKey.Value == ReceiptKey);
            if (response != null)
            {
                response.ReceiptDetail = new List<Document_ReceiptDetail>();
                using (Document_ReceiptDetailRepository Document_ReceiptDetailRepository = new Document_ReceiptDetailRepository())
                    response.ReceiptDetail = Document_ReceiptDetailRepository.SelectDataWithCondition(a => a.ReceiptKey == response.ReceiptKey).OrderBy(a => a.Sequence).ToList();

                response.ReceiptCumulativeDetail = new List<Document_ReceiptCumulativeDetail>();
                using (Document_ReceiptCumulativeDetailRepository Document_ReceiptCumulativeDetailRepository = new Document_ReceiptCumulativeDetailRepository())
                    response.ReceiptCumulativeDetail = Document_ReceiptCumulativeDetailRepository.SelectDataWithCondition(a => a.ReceiptKey == response.ReceiptKey).OrderBy(a => a.Sequence).ToList();

                response.ReceiptInfo = new Document_ReceiveInfo();
                using (Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository())
                    response.ReceiptInfo = Document_ReceiveInfoRepository.SelectFirstDataWithCondition(a => a.DocumentKey == response.ReceiptKey && a.DocumentType == Prefix && a.isdelete == isactive);
            }

            return response;
        }

        public void Delete(Document_Receipt input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_Receipt>().Where(a => a.ReceiptKey == input.ReceiptKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();

                        if (otable.ReceiptType == "CRE")
                        {
                            List<Document_ReceiptCumulativeDetail> beforels = new List<Document_ReceiptCumulativeDetail>();
                            var stable = context.Set<Document_ReceiptCumulativeDetail>().Where(a => a.ReceiptKey == input.ReceiptKey);
                            foreach (Document_ReceiptCumulativeDetail ob1 in stable)
                            {
                                Document_Invoice invoice = new Document_Invoice();
                                invoice = context.Set<Document_Invoice>().Where(a => a.InvoiceKey == ob1.DocumentKey).FirstOrDefault();
                                if (invoice != null) { invoice.ReceiptKey = default(int?); invoice.ReceiptNo = string.Empty; }
                                Document_CreditNote creditnote = new Document_CreditNote();
                                creditnote = context.Set<Document_CreditNote>().Where(a => a.CreditNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (creditnote != null) { creditnote.ReceiptKey = default(int?); creditnote.ReceiptNo = string.Empty; }
                                Document_DebitNote debitnote = new Document_DebitNote();
                                debitnote = context.Set<Document_DebitNote>().Where(a => a.DebitNoteKey == ob1.DocumentKey).FirstOrDefault();
                                if (debitnote != null) { debitnote.ReceiptKey = default(int?); debitnote.ReceiptNo = string.Empty; }
                                context.SaveChanges();
                            }
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_Receipt> GetWithRef(string businessid, string sid)
        {
            List<Document_Receipt> response = new List<Document_Receipt>();
            using (AccountingContext context = new AccountingContext())
            {
                if (string.IsNullOrEmpty(sid))
                    response = context.Set<Document_Receipt>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.ReceiptKey).ToList();
                else
                    response = context.Set<Document_Receipt>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive
                     && a.SaleID.ToString().ToUpper() == sid.ToUpper()).OrderByDescending(a => a.ReceiptKey).ToList();


                if (response != null && response.Count > 0)
                {
                    string[] documentconvert = response.ConvertAll(x => x.ReceiptNo).ToArray();
                    int[] documentkeyconvert = response.ConvertAll(x => x.ReceiptKey.Value).ToArray();
                    List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.ReceiptNo)).ToList();
                    List<Document_ReceiveInfo> infotable = context.Set<Document_ReceiveInfo>().Where(a => documentkeyconvert.Contains(a.DocumentKey.Value) && a.DocumentType == Prefix && a.isdelete == isactive).ToList();
                    List < Document_ReceiptCumulativeDetail > cumulativetable = context.Set<Document_ReceiptCumulativeDetail>().Where(a => documentkeyconvert.Contains(a.ReceiptKey.Value)).ToList();

                    foreach (Document_Receipt inv in response)
                    {
                        inv.ReceiptInfo = new Document_ReceiveInfo();
                        inv.ReceiptInfo = infotable.Where(a => a.DocumentKey == inv.ReceiptKey).FirstOrDefault();
                            //using (Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository())
                            //inv.ReceiptInfo = Document_ReceiveInfoRepository.SelectFirstDataWithCondition(a => a.DocumentKey == inv.ReceiptKey );

                        inv.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.ReceiptNo == inv.ReceiptNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            inv.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.CreditNoteNo))
                                    docno = docno + "," + bref.CreditNoteNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                inv.DocumentRefNo = inv.DocumentRefNo + docno;
                            }
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                        }

                        if (inv.ReceiptType == "CRE")
                        {

                            List<Document_ReceiptCumulativeDetail> cumulative = cumulativetable.Where(a => a.ReceiptKey == inv.ReceiptKey).ToList();
                            if (cumulative != null && cumulative.Count > 0)
                            {
                                inv.DocumentRefNo = !string.IsNullOrEmpty(inv.DocumentRefNo) ? inv.DocumentRefNo : ",";
                                foreach (Document_ReceiptCumulativeDetail repeat in cumulative)
                                    inv.DocumentRefNo = inv.DocumentRefNo + "," + repeat.DocumentNo;

                                inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                                inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                            }
                        }
                    }
                }
            }
            return response;
        }

        public Document_ReceiveInfo SaveReceivePayment(Document_ReceiveInfo input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_Receipt val = context.Set<Document_Receipt>().Where(a => a.BusinessID == input.BusinessID && a.ReceiptKey == input.DocumentKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบเสร็จรับเงิน");

                        Param_DocReceiptStatus stable = context.Set<Param_DocReceiptStatus>().Where(a => a.ID == "99").FirstOrDefault();

                        string curstatus = val.ReceiptStatus;
                        string curstatusdes = val.ReceiptStatusName;

                        val.ReceiptStatus = stable.ID;
                        val.ReceiptStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;
                        context.SaveChanges();

                        Document_ReceiveInfoRepository Document_ReceiveInfoRepository = new Document_ReceiveInfoRepository();
                        Document_ReceiveInfoRepository.Save(input, context);

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.ReceiptKey,
                            DocumentNo = val.ReceiptNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
