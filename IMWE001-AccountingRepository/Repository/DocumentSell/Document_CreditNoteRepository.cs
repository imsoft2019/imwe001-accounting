﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;
using IMWE001_AccountingModels.Business;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.ContactBook;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_CreditNoteRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_CreditNote, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "CN";
        public Document_CreditNote Save(Document_CreditNote input)
        {
            try
            {
                bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_CreditNote>();
                        if (!input.CreditNoteKey.HasValue)
                        {
                            string flagstate = "1";

                            Param_DocCreditNoteStatus stable = context.Set<Param_DocCreditNoteStatus>().Where(a => a.ID == flagstate).FirstOrDefault();

                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.CreditNoteNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.CreditNoteDate.Value.Day.ToString().PadLeft(2, '0'), input.CreditNoteDate.Value.Month.ToString().PadLeft(2, '0'), input.CreditNoteDate.Value.Year)
                            });
                            input.CreditNoteStatus = stable.ID;
                            input.CreditNoteStatusName = stable.Name;
                            input.CreateDate = input.UpdateDate;
                            input.CreateBy = input.UpdateBy;
                            input.isdelete = 1;
                            context.Add(input);
                            isnew = true;

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลใบลดหนี้";
                            request.ActivityTodo = "เพิ่ม/แก้ไขใบลดหนี้";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขใบลดหนี้ เลขที่เอกสาร" + input.CreditNoteNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            Document_CreditNote val = context.Set<Document_CreditNote>().Where(a => a.BusinessID == input.BusinessID && a.CreditNoteKey == input.CreditNoteKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบลดหนี้");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.CreditNoteStatus = input.CreditNoteStatus;
                            val.CreditNoteStatusName = input.CreditNoteStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.CreditNoteDate = input.CreditNoteDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.CauseType = input.CauseType;
                            val.CauseTypeName = input.CauseTypeName;
                            val.CauseTypeOther = input.CauseTypeOther;
                            val.TaxType = input.TaxType;
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.CreditPriceBefore = input.CreditPriceBefore;
                            val.CreditPriceReal = input.CreditPriceReal;
                            val.CreditPriceAfter = input.CreditPriceAfter;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลใบลดหนี้";
                                request.ActivityTodo = "แก้ไขใบลดหนี้";
                                request.ActivityDesc = string.Format("ทำการแก้ไขใบลดหนี้ เลขที่เอกสาร" + input.CreditNoteNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();

                        Document_CreditNoteDetailRepository Document_CreditNoteDetailRepository = new Document_CreditNoteDetailRepository();
                        if (!isnew)
                        {
                            Document_CreditNoteDetailRepository.Delete(input.CreditNoteKey.Value, context);
                            context.SaveChanges();

                            if (input.CreditNoteDetail != null && input.CreditNoteDetail.Count > 0)
                            {
                                Document_CreditNoteDetailRepository.Add(input.CreditNoteKey.Value, input.BusinessID, input.CreditNoteDetail, context);
                            }

                            context.SaveChanges();

                            Document_CreditNoteDetailRepository.Dispose();
                        }

                        if (input.DocumentKey.HasValue && !string.IsNullOrEmpty(input.DocumentNo))
                        {
                            //เก็บข้อมูลลง Reference
                            Document_Reference doc_ref = new Document_Reference()
                            {
                                BusinessID = input.BusinessID,
                                DocumentOwner = input.DocumentOwner,
                                DocumentGen = Prefix,
                                InvoiceNo = input.DocumentOwner == "INV" ? input.DocumentNo : string.Empty,
                                CreditNoteNo = input.CreditNoteNo,
                                CreateDate = input.CreateDate,
                                CreateBy = input.CreateBy
                            };
                            Document_ReferenceRepository Document_ReferenceRepository = new Document_ReferenceRepository();
                            Document_ReferenceRepository.Add(doc_ref, context);

                            GenerateDocumentRef gen_ref = new GenerateDocumentRef()
                            {
                                BusinessID = input.BusinessID,
                                DocumentKey = input.DocumentKey,
                                remarkishas = string.Format("สร้างประเภทเอกสารใบแจ้งหนี้ราคาเป็น{0}/ใบลดหนี้", input.DocumentOwner == "INV" ? "ใบแจ้งหนี้" : string.Empty),
                                ToDocumentKey = input.CreditNoteKey,
                                ToDocumentNo = input.CreditNoteNo,
                                UpdateBy = input.UpdateBy,
                                UpdateDate = input.UpdateDate.Value
                            };

                            if (input.DocumentOwner == "INV")
                            {
                                // เปลี่ยนสถานะเอกสารใบเสนอราคาเป็น ดำเนินการแล้ว
                                Document_InvoiceRepository Document_InvoiceRepository = new Document_InvoiceRepository();
                                Document_InvoiceRepository.SaveWorkflowNote(gen_ref, context, Prefix);
                                Document_InvoiceRepository.Dispose();
                            }
                            Document_ReferenceRepository.Dispose();
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                    return input;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(Document_CreditNote input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        Document_CreditNote val = context.Set<Document_CreditNote>().Where(a => a.BusinessID == input.BusinessID && a.CreditNoteKey == input.CreditNoteKey).FirstOrDefault();
                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบแจ้งหนี้");

                        Param_DocCreditNoteStatus stable = context.Set<Param_DocCreditNoteStatus>().Where(a => a.ID == input.CreditNoteStatus).FirstOrDefault();

                        string curstatus = val.CreditNoteStatus;
                        string curstatusdes = val.CreditNoteStatusName;

                        val.CreditNoteStatus = stable.ID;
                        val.CreditNoteStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.CreditNoteKey,
                            DocumentNo = val.CreditNoteNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflow(GenerateDocumentRef input, AccountingContext context)
        {
            try
            {
                Document_CreditNote val = context.Set<Document_CreditNote>().Where(a => a.BusinessID == input.BusinessID && a.CreditNoteKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบวางบิล");

                if (val.CreditNoteStatus == "99")
                    throw new Exception("สถานะเอกสารไม่สามารถเปลี่ยนแปลงได้ เนื่องจากถูกเปลี่ยนแปลงเรียบร้อยแล้ว");

                Param_DocCreditNoteStatus stable = context.Set<Param_DocCreditNoteStatus>().Where(a => a.ID == "2").FirstOrDefault();

                string curstatus = val.CreditNoteStatus;
                string curstatusdes = val.CreditNoteStatusName;

                val.CreditNoteStatus = stable.ID;
                val.CreditNoteStatusName = stable.Name;
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                Log_Workflow obj = new Log_Workflow()
                {
                    BusinessID = val.BusinessID,
                    DocumentKey = val.CreditNoteKey,
                    DocumentNo = val.CreditNoteNo,
                    CurrentStatus = curstatus,
                    CurrentStatusName = curstatusdes,
                    ToStatus = stable.ID,
                    ToStatusName = stable.Name,
                    CreateDate = input.UpdateDate,
                    CreateBy = input.UpdateBy
                };
                Log_WorkflowRepository.Save(obj, context);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SaveWorkflowCumulative(GenerateDocumentRef input, AccountingContext context, string DocumentType)
        {
            try
            {
                Document_CreditNote val = context.Set<Document_CreditNote>().Where(a => a.BusinessID == input.BusinessID && a.CreditNoteKey == input.DocumentKey).FirstOrDefault();
                if (val == null)
                    throw new Exception("ไม่พบข้อมูลใบลดหนี้");

                Param_DocCreditNoteStatus stable = context.Set<Param_DocCreditNoteStatus>().Where(a => a.ID == (DocumentType == "BL" ? "2" : DocumentType == "RE" ? "99" : "1")).FirstOrDefault();

                string curstatus = val.CreditNoteStatus;
                string curstatusdes = val.CreditNoteStatusName;

                val.CreditNoteStatus = stable.ID;
                val.CreditNoteStatusName = stable.Name;
                if (DocumentType == "BL")
                {
                    val.BillingNoteKey = input.ToDocumentKey;
                    val.BillingNoteNo = input.ToDocumentNo;
                }
                else if (DocumentType == "RE")
                {
                    val.ReceiptKey = input.ToDocumentKey;
                    val.ReceiptNo = input.ToDocumentNo;
                }
                val.UpdateDate = input.UpdateDate;
                val.UpdateBy = input.UpdateBy;

                context.SaveChanges();

                if (curstatus != "2" && curstatus != "99")
                {
                    Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                    Log_Workflow obj = new Log_Workflow()
                    {
                        BusinessID = val.BusinessID,
                        DocumentKey = val.CreditNoteKey,
                        DocumentNo = val.CreditNoteNo,
                        CurrentStatus = curstatus,
                        CurrentStatusName = curstatusdes,
                        ToStatus = stable.ID,
                        ToStatusName = stable.Name,
                        ToDocumentKey = input.ToDocumentKey,
                        ToDocumentNo = input.ToDocumentNo,
                        Remark = input.remarkishas,
                        CreateDate = input.UpdateDate,
                        CreateBy = input.UpdateBy
                    };
                    Log_WorkflowRepository.Save(obj, context);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_CreditNote Get(string businessid, int CreditNoteKey)
        {
            Document_CreditNote response = new Document_CreditNote();
            using (Document_CreditNoteRepository Document_CreditNoteRepository = new Document_CreditNoteRepository())
                response = Document_CreditNoteRepository.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.CreditNoteKey.Value == CreditNoteKey);
            if (response != null)
            {
                response.CreditNoteDetail = new List<Document_CreditNoteDetail>();
                using (Document_CreditNoteDetailRepository Document_CreditNoteDetailRepository = new Document_CreditNoteDetailRepository())
                    response.CreditNoteDetail = Document_CreditNoteDetailRepository.SelectDataWithCondition(a => a.CreditNoteKey == response.CreditNoteKey).OrderBy(a => a.Sequence).ToList();

                response.InvoiceDetail = new List<Document_InvoiceDetail>();
                using (Document_InvoiceDetailRepository Document_InvoiceDetailRepository = new Document_InvoiceDetailRepository())
                    response.InvoiceDetail = Document_InvoiceDetailRepository.SelectDataWithCondition(a => a.InvoiceKey == response.InvoiceKey).OrderBy(a => a.Sequence).ToList();
            }

            return response;
        }

        public void Delete(Document_CreditNote input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<Document_CreditNote>().Where(a => a.CreditNoteKey == input.CreditNoteKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_CreditNote> GetWithRef(string businessid)
        {
            List<Document_CreditNote> response = new List<Document_CreditNote>();
            using (AccountingContext context = new AccountingContext())
            {
                response = context.Set<Document_CreditNote>().Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.CreditNoteKey).ToList();
                if (response != null && response.Count > 0)
                {
                    string[] documentconvert = response.ConvertAll(x => x.CreditNoteNo).ToArray();
                    List<Document_Reference> rtable = context.Set<Document_Reference>().Where(a => documentconvert.Contains(a.CreditNoteNo)).ToList();
                    foreach (Document_CreditNote inv in response)
                    {
                        inv.DocumentRefNo = string.Empty;
                        List<Document_Reference> dref = new List<Document_Reference>();
                        dref = rtable.Where(a => a.CreditNoteNo == inv.CreditNoteNo).ToList();
                        if (dref != null && dref.Count > 0)
                        {
                            inv.DocumentRefNo = ",";
                            foreach (Document_Reference bref in dref)
                            {
                                string docno = string.Empty;
                                if (!string.IsNullOrEmpty(bref.QuotationNo))
                                    docno = docno + "," + bref.QuotationNo;
                                if (!string.IsNullOrEmpty(bref.BillingNo))
                                    docno = docno + "," + bref.BillingNo;
                                if (!string.IsNullOrEmpty(bref.ReceiptNo))
                                    docno = docno + "," + bref.ReceiptNo;
                                if (!string.IsNullOrEmpty(bref.CashSaleNo))
                                    docno = docno + "," + bref.CashSaleNo;
                                if (!string.IsNullOrEmpty(bref.InvoiceNo))
                                    docno = docno + "," + bref.InvoiceNo;
                                if (!string.IsNullOrEmpty(bref.DebitNoteNo))
                                    docno = docno + "," + bref.DebitNoteNo;
                                if (!string.IsNullOrEmpty(bref.PurchaseNo))
                                    docno = docno + "," + bref.PurchaseNo;
                                if (!string.IsNullOrEmpty(bref.ReceivingInventoryNo))
                                    docno = docno + "," + bref.ReceivingInventoryNo;
                                if (!string.IsNullOrEmpty(bref.ExpenseNo))
                                    docno = docno + "," + bref.ExpenseNo;
                                if (!string.IsNullOrEmpty(bref.WithholdingTaxNo))
                                    docno = docno + "," + bref.WithholdingTaxNo;

                                inv.DocumentRefNo = inv.DocumentRefNo + docno;
                            }
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                            inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                        }

                        if (!string.IsNullOrEmpty(inv.BillingNoteNo))
                        {
                            inv.DocumentRefNo = !string.IsNullOrEmpty(inv.DocumentRefNo) ? inv.DocumentRefNo : ",";
                            inv.DocumentRefNo = inv.DocumentRefNo + "," + inv.BillingNoteNo;
                        }
                        if (!string.IsNullOrEmpty(inv.ReceiptNo))
                        {
                            inv.DocumentRefNo = !string.IsNullOrEmpty(inv.DocumentRefNo) ? inv.DocumentRefNo : ",";
                            inv.DocumentRefNo = inv.DocumentRefNo + "," + inv.ReceiptNo;
                        }

                        inv.DocumentRefNo = inv.DocumentRefNo.Replace(",,", "");
                        inv.DocumentRefNo = inv.DocumentRefNo.Replace(",", "<br>");
                    }
                }
            }
            return response;
        }
    }
}
