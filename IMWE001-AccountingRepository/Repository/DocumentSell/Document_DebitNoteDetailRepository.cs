﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using IMWE001_AccountingModels.Products;
using IMWE001_AccountingRepository.Repository.Parameter;
using System.Linq;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.DocumentSell
{
    public class Document_DebitNoteDetailRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_DebitNoteDetail, AccountingContext>
    {
        public void Add(int DebitNoteKey, Guid BusinessID, List<Document_DebitNoteDetail> request, AccountingContext context)
        {
            var otable = context.Set<Document_DebitNoteDetail>();
            if (request != null && request.Count > 0)
                foreach (Document_DebitNoteDetail obj in request)
                {
                    obj.DebitNoteKey = DebitNoteKey;
                    obj.BusinessID = BusinessID;
                    otable.Add(obj);
                }

            context.SaveChanges();
        }

        public void Delete(int DebitNoteKey, AccountingContext context)
        {
            var otable = context.Set<Document_DebitNoteDetail>();
            List<Document_DebitNoteDetail> val = new List<Document_DebitNoteDetail>();
            val = otable.Where(a => a.DebitNoteKey.Value == DebitNoteKey).ToList();
            if (val != null && val.Count > 0)
                foreach (Document_DebitNoteDetail del in val)
                    otable.Remove(del);

            context.SaveChanges();

        }
    }
}
