﻿using System;
using System.Collections.Generic;
using System.Transactions;
using System.Linq;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingRepository.Repository.Business;
using IMWE001_AccountingModels.Workflow;
using IMWE001_AccountingRepository.Repository.Log;
using IMWE001_AccountingModels.Log;
using IMWE001_AccountingModels.DocumentExpenses;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.DocumentExpenses
{
    public class Document_WithholdingTaxRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Document_WithholdingTax, AccountingContext>
    {
        int isactive = 1;
        int isflagdelete = 0;
        const string Prefix = "WT";


        public Document_WithholdingTax Save(Document_WithholdingTax input)
        {
            try
            {
                // bool isnew = false;

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var PO_Table = context.Set<Document_WithholdingTax>();

                        if (!input.WithholdingTaxKey.HasValue)
                        {
                            DocumentPrefixRepository DocumentPrefixRepository = new DocumentPrefixRepository();
                            input.WithholdingTaxNo = DocumentPrefixRepository.GetDocumentNo(context, true, new GetDocumentNo
                            {
                                businessid = input.BusinessID,
                                documenttype = Prefix,
                                asofdate = string.Format("{0}-{1}-{2}", input.WithholdingTaxDate.Value.Day.ToString().PadLeft(2, '0'), input.WithholdingTaxDate.Value.Month.ToString().PadLeft(2, '0'), input.WithholdingTaxDate.Value.Year)
                            });

                            input.TaxType = "N";
                            input.WithholdingTaxStatus = "2";
                            input.WithholdingTaxStatusName = "ดำเนินการแล้ว";
                            input.CreateBy = input.UpdateBy;
                            input.CreateDate = input.UpdateDate;
                            input.isdelete = 1;

                            PO_Table.Add(input);

                            context.SaveChanges();

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "ข้อมูลหัก ณ ที่จ่าย";
                            request.ActivityTodo = "เพิ่ม/แก้ไขหัก ณ ที่จ่าย";
                            request.ActivityDesc = "ทำการเพิ่ม/แก้ไขหัก ณ ที่จ่าย เลขที่เอกสาร" + input.WithholdingTaxNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                        else
                        {
                            var val = PO_Table.Where(S => S.BusinessID == input.BusinessID && S.WithholdingTaxKey == input.WithholdingTaxKey).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลใบสั่งซื้อ");

                            decimal totalbefore = 0;
                            totalbefore = val.GrandAmount.Value;

                            val.WithholdingTaxStatus = input.WithholdingTaxStatus;
                            val.WithholdingTaxStatusName = input.WithholdingTaxStatusName;
                            val.CustomerKey = input.CustomerKey;
                            val.CustomerName = input.CustomerName;
                            val.CustomerTaxID = input.CustomerTaxID;
                            val.CustomerContactName = input.CustomerContactName;
                            val.CustomerContactPhone = input.CustomerContactPhone;
                            val.CustomerContactEmail = input.CustomerContactEmail;
                            val.CustomerAddress = input.CustomerAddress;
                            val.CustomerBranch = input.CustomerBranch;
                            val.WithholdingTaxDate = input.WithholdingTaxDate;
                            val.CreditType = input.CreditType;
                            val.CreditTypeName = input.CreditTypeName;
                            val.CreditDay = input.CreditDay;
                            val.DueDate = input.DueDate;
                            val.SaleID = input.SaleID;
                            val.SaleName = input.SaleName;
                            val.ProjectName = input.ProjectName;
                            val.ReferenceNo = input.ReferenceNo;
                            val.TaxType =  "N";
                            val.TaxTypeName = input.TaxTypeName;
                            val.TotalAmount = input.TotalAmount;
                            val.IsDiscountHeader = input.IsDiscountHeader;
                            val.IsDiscountHeaderType = input.IsDiscountHeaderType;
                            val.DiscountPercent = input.DiscountPercent;
                            val.DiscountAmount = input.DiscountAmount;
                            val.TotalAfterDiscountAmount = input.TotalAfterDiscountAmount;
                            val.IsTaxHeader = input.IsTaxHeader;
                            val.IsTaxSummary = input.IsTaxSummary;
                            val.ExemptAmount = input.ExemptAmount;
                            val.VatableAmount = input.VatableAmount;
                            val.VAT = input.VAT;
                            val.TotalBeforeVatAmount = input.TotalBeforeVatAmount;
                            val.IsWithholdingTax = input.IsWithholdingTax;
                            val.WithholdingKey = input.WithholdingKey;
                            val.WithholdingRate = input.WithholdingRate;
                            val.WithholdingAmount = input.WithholdingAmount;
                            val.GrandAmountAfterWithholding = input.GrandAmountAfterWithholding;
                            val.GrandAmount = input.GrandAmount;
                            val.Remark = input.Remark;
                            val.Signature = input.Signature;
                            val.Noted = input.Noted;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;

                            val.SSOFundPrice = input.SSOFundPrice;
                            val.SSONumber = input.SSONumber;
                            val.SSOPrice = input.SSOPrice;
                            val.PayerMethod = input.PayerMethod;
                            val.PayerDesc = input.PayerDesc;
                            val.WithholdingTaxForm = input.WithholdingTaxForm;
                            val.WithholdingTaxFormName = input.WithholdingTaxFormName;

                            context.SaveChanges();

                            if (totalbefore != input.GrandAmount)
                            {
                                var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                                Log_Activity request = new Log_Activity();
                                request.BusinessID = input.BusinessID;
                                request.ActivityDate = DateTime.Now;
                                request.ActivityHeader = "ข้อมูลหัก ณ ที่จ่าย";
                                request.ActivityTodo = "แก้ไขหัก ณ ที่จ่าย";
                                request.ActivityDesc = string.Format("ทำการแก้ไขหัก ณ ที่จ่าย เลขที่เอกสาร" + input.WithholdingTaxNo + " จากราคา {0:n2} บาท เป็น {1:n2} บาท", totalbefore, input.GrandAmount);
                                request.ActivityByName = user;
                                request.CreateOn = DateTime.Now;
                                request.CreateBy = input.UpdateBy;
                                Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                                Log_ActivityRepository.Save(request, context);
                                context.SaveChanges();
                            }

                            var tbPODetail = context.Set<Document_WithholdingTaxDetail>();
                            var tmp = tbPODetail.Where(S => S.WithholdingTaxKey == input.WithholdingTaxKey);

                            if (tmp != null && tmp.Count() > 0)
                            {
                                tbPODetail.RemoveRange(tmp); context.SaveChanges();
                            }
                            if (input.WithholdingTaxDetails != null && input.WithholdingTaxDetails.Count > 0)
                            {
                                foreach (var item in input.WithholdingTaxDetails)
                                {
                                    item.BusinessID = input.BusinessID;
                                    item.WithholdingTaxKey = input.WithholdingTaxKey;
                                }
                                tbPODetail.AddRange(input.WithholdingTaxDetails); context.SaveChanges();
                            }
                        }
                    }
                    scope.Complete();
                }

                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(Document_WithholdingTax input, string remarkishas)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var val = context.Set<Document_WithholdingTax>().Where(a => a.BusinessID == input.BusinessID && a.WithholdingTaxKey == input.WithholdingTaxKey).FirstOrDefault();

                        if (val == null)
                            throw new Exception("ไม่พบข้อมูลใบสั่งซื้อ");

                        var stable = context.Set<Param_DocWithholdingTaxStatus>().Where(a => a.ID == input.WithholdingTaxStatus).FirstOrDefault();

                        string curstatus = val.WithholdingTaxStatus;
                        string curstatusdes = val.WithholdingTaxStatusName;

                        val.WithholdingTaxStatus = stable.ID;
                        val.WithholdingTaxStatusName = stable.Name;
                        val.UpdateDate = input.UpdateDate;
                        val.UpdateBy = input.UpdateBy;

                        context.SaveChanges();

                        Log_WorkflowRepository Log_WorkflowRepository = new Log_WorkflowRepository();
                        Log_Workflow obj = new Log_Workflow()
                        {
                            BusinessID = val.BusinessID,
                            DocumentKey = val.WithholdingTaxKey,
                            DocumentNo = val.WithholdingTaxNo,
                            CurrentStatus = curstatus,
                            CurrentStatusName = curstatusdes,
                            ToStatus = stable.ID,
                            ToStatusName = stable.Name,
                            CreateDate = input.UpdateDate,
                            CreateBy = input.UpdateBy
                        };
                        Log_WorkflowRepository.Save(obj, context);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Document_WithholdingTax Get(string businessid, int key)
        {
            var response = new Document_WithholdingTax();
            using (var repo = new Document_WithholdingTaxRepository())
                response = repo.SelectFirstDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper() && a.WithholdingTaxKey.Value == key);
            if (response != null)
            {
                response.WithholdingTaxDetails = new List<Document_WithholdingTaxDetail>();
                using (var repodetail = new Document_WithholdingTaxDetailRepository())
                    response.WithholdingTaxDetails = repodetail.SelectDataWithCondition(a => a.WithholdingTaxKey == response.WithholdingTaxKey).OrderBy(a => a.Sequence).ToList();
            }

            return response;
        }

        public void Delete(Document_WithholdingTax input)
        {
            try
            {
                using (AccountingContext context = new AccountingContext())
                {
                    var otable = context.Set<Document_WithholdingTax>().Where(a => a.WithholdingTaxKey == input.WithholdingTaxKey && a.BusinessID == input.BusinessID).FirstOrDefault();
                    if (otable != null)
                    {
                        otable.isdelete = 0;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public List<Document_WithholdingTax> GetWithRef(string businessid)
        {
            List<Document_WithholdingTax> response = new List<Document_WithholdingTax>();
            using (AccountingContext context = new AccountingContext())
            {
                var otable = context.Set<Document_WithholdingTax>();
                response = otable.Where(a => a.BusinessID.ToString().ToUpper() == businessid.ToString().ToUpper() && a.isdelete == isactive).OrderByDescending(a => a.WithholdingTaxKey).ToList();

                if (response != null && response.Count > 0)
                {
                    var rtable = context.Set<Document_WithholdingTaxDetail>();
                    foreach (Document_WithholdingTax doc in response)
                    {
                        doc.DocumentRefNo = string.Empty;
                        List<Document_WithholdingTaxDetail> dref = new List<Document_WithholdingTaxDetail>();
                        dref = rtable.Where(a => a.WithholdingTaxKey == doc.WithholdingTaxKey).OrderBy(a => a.Sequence).ToList();

                        if (dref != null && dref.Count > 0)
                        {
                            doc.DocumentRefNo = ",";
                            foreach (Document_WithholdingTaxDetail bref in dref)
                            {
                                string docno = string.Empty;
                                docno = docno + ", " + (bref.TaxPercentType == "0" ? bref.PriceTax.Value.ToString("n2") : bref.TaxPercentType + "%");

                                doc.DocumentRefNo = doc.DocumentRefNo + docno;
                            }
                            doc.DocumentRefNo = doc.DocumentRefNo.Replace(",,", "");
                        }
                    }
                }
            }

            return response;
        }
    }
}
