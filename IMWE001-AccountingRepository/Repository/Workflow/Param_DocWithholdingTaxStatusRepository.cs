﻿using IMWE001_AccountingModels.Workflow;

namespace IMWE001_AccountingRepository.Repository.Workflow
{
    public class Param_DocWithholdingTaxStatusRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Param_DocWithholdingTaxStatus, AccountingContext>
    {
    }
}
