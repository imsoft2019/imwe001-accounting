﻿using IMWE001_AccountingModels.Workflow;

namespace IMWE001_AccountingRepository.Repository.Workflow
{
    public class Param_DocExpenseStatusRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Param_DocExpenseStatus, AccountingContext>
    {
    }
}
