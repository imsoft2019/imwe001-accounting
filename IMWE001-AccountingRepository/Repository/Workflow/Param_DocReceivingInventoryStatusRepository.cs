﻿using IMWE001_AccountingModels.Workflow;

namespace IMWE001_AccountingRepository.Repository.Workflow
{
    public class Param_DocReceivingInventoryStatusRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Param_DocReceivingInventoryStatus, AccountingContext>
    {
    }
}
