﻿using IMWE001_AccountingModels.Workflow;

namespace IMWE001_AccountingRepository.Repository.Workflow
{
    public class Param_DocPurchaseStatusRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Param_DocPurchaseStatus, AccountingContext>
    {
    }
}
