﻿using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingModels.Log;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingRepository.Repository.EventLog
{
    public class Log_ActivityRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Log_Activity, AccountingContext>
    {
        public void Save(Log_Activity input, AccountingContext context)
        {
            var ltable = context.Set<Log_Activity>();
            ltable.Add(input);
            context.SaveChanges();
        }

        public List<Log_Activity> GetActivity(string businessid)
        {
            List<Log_Activity> response = new List<Log_Activity>();

            using (Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository())
                response = Log_ActivityRepository.SelectDataWithCondition(a => a.BusinessID.ToString().ToUpper() == businessid.ToUpper()).OrderByDescending(c => c.ActivityDate).Take(10).ToList();

            return response;
        }
    }
}
