﻿using IMWE001_AccountingModels.Log;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMWE001_AccountingRepository.Repository.Log
{
    public class Log_WorkflowRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<Log_Workflow, AccountingContext>
    {
        public void Save(Log_Workflow input , AccountingContext context)
        {
            var ltable = context.Set<Log_Workflow>();
            ltable.Add(input);   
            context.SaveChanges();
        }
    }
}
