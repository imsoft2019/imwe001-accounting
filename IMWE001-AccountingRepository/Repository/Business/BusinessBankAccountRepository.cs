﻿using IMWE001_AccountingModels.Business;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.DocumentSell;

namespace IMWE001_AccountingRepository.Repository.Business
{

    public class BusinessBankAccountRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<BusinessBankAccount, AccountingContext>
    {
        public void Save(BusinessBankAccount input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<BusinessBankAccount>();
                        if (!input.UID.HasValue)
                        {
                            input.UID = Guid.NewGuid();
                            input.isdelete = 1;
                            otable.Add(input);
                            context.SaveChanges();

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = input.BusinessID;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "บัญชีธนาคาร";
                            request.ActivityTodo = "เพิ่มบัญชีธนาคาร";
                            request.ActivityDesc = "เพิ่มบัญชีธนาคาร " + input.BankName + " เลขที่บัญชี " + input.AccountNo;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = input.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Delete(string uid)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<BusinessBankAccount>().Where(a => a.UID.ToString().ToUpper() == uid.ToUpper()).FirstOrDefault();

                        Document_ReceiveInfo info = new Document_ReceiveInfo();
                        info = context.Set<Document_ReceiveInfo>().Where(a => a.BankAccountID == otable.UID.Value).FirstOrDefault();

                        if (info != null)
                            throw new Exception("เนื่องจากบัญชีมีความเคลื่อนไหวแล้ว");

                        otable.isdelete = 0;
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
