﻿using IMWE001_AccountingModels.Business;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;

namespace IMWE001_AccountingRepository.Repository.Business
{

    public class BusinessParameterRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<BusinessParameter, AccountingContext>
    {
        public void Save(BusinessParameter input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<BusinessParameter>().Where(a => a.BusinessID == input.BusinessID && a.ParameterField == input.ParameterField).FirstOrDefault();
                        if (otable == null)
                            throw new Exception("ไม่พบข้อมูลพารามิเตอร์ธุรกิจ");

                        otable.ParameterValue = input.ParameterValue;
                        otable.ParameterDescription = input.ParameterDescription;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;

                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Save(List<BusinessParameter> input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        foreach (BusinessParameter param in input)
                        {
                            var otable = context.Set<BusinessParameter>().Where(a => a.BusinessID == param.BusinessID && a.ParameterField == param.ParameterField).FirstOrDefault();
                            if (otable == null)
                                throw new Exception("ไม่พบข้อมูลพารามิเตอร์ธุรกิจ");

                            otable.ParameterValue = param.ParameterValue;
                            otable.ParameterDescription = param.ParameterDescription;
                            otable.UpdateBy = param.UpdateBy;
                            otable.UpdateDate = param.UpdateDate;

                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
