﻿using IMWE001_AccountingModels.Business;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.Business
{

    public class BusinessProfileRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<BusinessProfile, AccountingContext>
    {
        public void Save(BusinessProfile input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<BusinessProfile>().Where(a => a.UID == input.UID).FirstOrDefault();
                        if (otable == null)
                            throw new Exception("ไม่พบข้อมูลธุรกิจ");

                        otable.UserType = input.UserType;
                        otable.UserTypeName = input.UserTypeName;
                        otable.BusinessName = input.BusinessName;
                        otable.Address = input.Address;
                        otable.TaxID = input.TaxID;
                        otable.BranchType = input.BranchType;
                        otable.BranchTypeName = input.BranchTypeName;
                        otable.OfficePhone = input.OfficePhone;
                        otable.MobilePhone = input.MobilePhone;
                        otable.Fax = input.Fax;
                        otable.WebsiteURL = input.WebsiteURL;
                        otable.LogoPath = input.LogoPath;
                        otable.ThumnailLogo = input.ThumnailLogo;
                        otable.RabberStampLogo = input.RabberStampLogo;
                        otable.BranchCode = input.BranchCode;
                        otable.BranchName = input.BranchName;
                        otable.IsRegisterTax = input.IsRegisterTax;
                        otable.IsRegisterTaxTypeName = input.IsRegisterTaxTypeName;
                        otable.UpdateBy = input.UpdateBy;
                        otable.UpdateDate = input.UpdateDate;

                        context.SaveChanges();

                        var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                        Log_Activity request = new Log_Activity();
                        request.BusinessID = input.UID.Value;
                        request.ActivityDate = DateTime.Now;
                        request.ActivityHeader = "ข้อมูลธุรกิจ";
                        request.ActivityTodo = "แก้ไขข้อมูลธุรกิจ";
                        request.ActivityDesc = "ทำการแก้ไขข้อมูลธุรกิจ";
                        request.ActivityByName = user;
                        request.CreateOn = DateTime.Now;
                        request.CreateBy = input.UpdateBy;
                        Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                        Log_ActivityRepository.Save(request, context);
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
