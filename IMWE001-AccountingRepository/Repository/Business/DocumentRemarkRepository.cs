﻿using IMWE001_AccountingModels.Business;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;
using IMWE001_AccountingModels.User;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;

namespace IMWE001_AccountingRepository.Repository.Business
{

    public class DocumentRemarkRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<DocumentRemark, AccountingContext>
    {
        public void Save(DocumentRemark input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<DocumentRemark>();
                        if (!input.DocumentKey.HasValue)
                        {
                            input.isdelete = 1;
                            otable.Add(input);
                            context.SaveChanges();
                        }
                        else
                        {
                            DocumentRemark val = context.Set<DocumentRemark>().Where(a => a.BusinessID == input.BusinessID && a.DocumentKey == input.DocumentKey && a.DocumentType == a.DocumentType).FirstOrDefault();
                            if (val == null)
                                throw new Exception("ไม่พบข้อมูลหมายเหตุเอกสาร");

                            val.DocumentType = input.DocumentType;
                            val.DocumentTypeName = input.DocumentTypeName;
                            val.DocumentText = input.DocumentText;
                            val.UpdateDate = input.UpdateDate;
                            val.UpdateBy = input.UpdateBy;
                            context.SaveChanges();
                        }

                        var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == input.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                        Log_Activity request = new Log_Activity();
                        request.BusinessID = input.BusinessID;
                        request.ActivityDate = DateTime.Now;
                        request.ActivityHeader = "ข้อมูลหมายเหตุเอกสาร";
                        request.ActivityTodo = "แก้ไขข้อมูลธุหมายเหตุเอกสาร";
                        request.ActivityDesc = "ทำการเพิ่ม/แก้ไขข้อมูลหมายเหตุเอกสาร ประเภท " + input.DocumentTypeName;
                        request.ActivityByName = user;
                        request.CreateOn = DateTime.Now;
                        request.CreateBy = input.UpdateBy;
                        Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                        Log_ActivityRepository.Save(request, context);
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Delete(string dockey)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var otable = context.Set<DocumentRemark>().Where(a => a.DocumentKey == Convert.ToInt32(dockey)).FirstOrDefault();
                        otable.isdelete = 0;
                        context.SaveChanges();

                        var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == otable.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                        Log_Activity request = new Log_Activity();
                        request.BusinessID = otable.BusinessID;
                        request.ActivityDate = DateTime.Now;
                        request.ActivityHeader = "ข้อมูลหมายเหตุเอกสาร";
                        request.ActivityTodo = "ลบข้อมูลธุหมายเหตุเอกสาร";
                        request.ActivityDesc = "ทำการลบข้อมูลหมายเหตุเอกสาร ประเภท " + otable.DocumentTypeName;
                        request.ActivityByName = user;
                        request.CreateOn = DateTime.Now;
                        request.CreateBy = otable.UpdateBy;
                        Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                        Log_ActivityRepository.Save(request, context);
                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
