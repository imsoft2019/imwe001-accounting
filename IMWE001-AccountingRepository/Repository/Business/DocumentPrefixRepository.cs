﻿using IMWE001_AccountingModels.Business;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;
using IMWE001_AccountingModels.Request;
using IMWE001_AccountingModels.EventLog;
using IMWE001_AccountingRepository.Repository.EventLog;
using IMWE001_AccountingModels.User;

namespace IMWE001_AccountingRepository.Repository.Business
{

    public class DocumentPrefixRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<DocumentPrefix, AccountingContext>
    {
        public void Save(DocumentPrefix input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        var Itable = context.Set<DocumentPrefix>();
                        var otable = Itable.Where(a => a.BusinessID == input.BusinessID && a.DocumentType == input.DocumentType && a.Suffix == input.Suffix).FirstOrDefault();
                        if (otable == null)
                        {
                            Itable.Add(input);
                        }
                        else
                        {
                            if (input.NextNumber > otable.NextNumber)
                            {
                                otable.NextNumber = input.NextNumber;
                                otable.UpdateBy = input.UpdateBy;
                                otable.UpdateDate = input.UpdateDate;
                            }
                        }

                        context.SaveChanges();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Save(List<DocumentPrefix> input)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
                {
                    using (AccountingContext context = new AccountingContext())
                    {
                        foreach (DocumentPrefix prefix in input)
                        {
                            var Itable = context.Set<DocumentPrefix>();
                            var otable = Itable.Where(a => a.BusinessID == prefix.BusinessID && a.DocumentType == prefix.DocumentType && a.Suffix == prefix.Suffix).FirstOrDefault();
                            if (otable == null)
                            {
                                Itable.Add(prefix);
                            }
                            else
                            {
                                if (prefix.NextNumber > otable.NextNumber)
                                {
                                    otable.NextNumber = prefix.NextNumber;
                                    otable.UpdateBy = prefix.UpdateBy;
                                    otable.UpdateDate = prefix.UpdateDate;
                                }
                            }
                            context.SaveChanges();

                            var user = context.Set<UserProfile>().Where(a => a.UID.ToString().ToUpper() == prefix.UpdateBy.ToUpper()).FirstOrDefault().EmployeeName;
                            Log_Activity request = new Log_Activity();
                            request.BusinessID = prefix.BusinessID.Value;
                            request.ActivityDate = DateTime.Now;
                            request.ActivityHeader = "เลขรันและหัวเอกสาร";
                            request.ActivityTodo = "แก้ไขข้อมูลเลขรันและหัวเอกสาร";
                            request.ActivityDesc = "แก้ไขเลขรันนิ่งเอกสาร ประเภท" + prefix.DocumentType + " เป็น " + prefix.NextNumber;
                            request.ActivityByName = user;
                            request.CreateOn = DateTime.Now;
                            request.CreateBy = prefix.UpdateBy;
                            Log_ActivityRepository Log_ActivityRepository = new Log_ActivityRepository();
                            Log_ActivityRepository.Save(request, context);
                            context.SaveChanges();
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public string GetStaticDocumentNo(GetDocumentNo request)
        {
            using (AccountingContext context = new AccountingContext())
                return GetDocumentNo(context, false, request);
        }

        public string GetDocumentNo(AccountingContext context, bool run, GetDocumentNo request)
        {
            string pk = string.Empty;
            try
            {
                BusinessParameter runingtype = new BusinessParameter();
                runingtype = context.Set<BusinessParameter>().Where(a => a.BusinessID == request.businessid && a.ParameterField == "RunningType").FirstOrDefault();

                string year = request.asofdate.Substring(6, 4).ToString();
                string month = request.asofdate.Substring(3, 2).ToString().PadLeft(2, '0');
                string suffix = year + (runingtype.ParameterValue == "YY" ? "" : "-" + month);

                DocumentPrefix prefix = new DocumentPrefix();
                prefix = context.Set<DocumentPrefix>().Where(a => a.DocumentType.Equals(request.documenttype) && a.Prefix.Equals(runingtype.ParameterValue) && a.Suffix.Equals(suffix)).FirstOrDefault();

                if (prefix != null)
                {
                    pk = string.Format("{0}{1}{2}", prefix.DocumentType, prefix.Suffix.Replace("-", ""), prefix.NextNumber.ToString().PadLeft(prefix.Len.Value, '0'));
                    prefix.NextNumber = prefix.NextNumber + 1;
                }
                else
                {
                    int len = (runingtype.ParameterValue == "YY") ? 6 : (runingtype.ParameterValue == "YM") ? 4 : 6;

                    pk = string.Format("{0}{1}{2}", request.documenttype, suffix.Replace("-", ""), "1".ToString().PadLeft(len, '0'));

                    var otable = context.Set<DocumentPrefix>();
                    DocumentPrefix DocPrefix = new DocumentPrefix()
                    {
                        BusinessID = request.businessid,
                        DocumentType = request.documenttype,
                        Prefix = runingtype.ParameterValue,
                        Suffix = suffix,
                        NextNumber = 2,
                        Len = len,
                        CreateDate = DateTime.Now,
                        CreateBy = "System",
                        UpdateDate = DateTime.Now,
                        UpdateBy = "System"
                    };
                    otable.Add(DocPrefix);
                }

                if (run)
                    context.SaveChanges();

                return pk;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
