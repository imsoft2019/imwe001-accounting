﻿using IMWE001_AccountingModels.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;


namespace IMWE001_AccountingRepository.Repository.User
{
    public class UserProfileRepository : IMauchlyCore.IMauchlyPostgreSQLRepository<UserProfile, AccountingContext>
    {
        public void Save(UserProfile input)
        {
            using (AccountingContext context = new AccountingContext())
            {
               var otable = context.Set<UserProfile>();

                var objUser = otable.Where(S => S.UID == input.UID && S.Email == input.Email).FirstOrDefault();

                if (objUser == null)
                {
                    var objValid = otable.Where(S =>  S.Email == input.Email).FirstOrDefault();
                    if (objValid != null)
                        throw new Exception("อีเมลซ้ำ กรุณาตรวจสอบอีกครั้ง");
                        input.UID = Guid.NewGuid();
                    input.CreateDate = input.UpdateDate = DateTime.Now;
                    input.IsDelete = 1;
                    otable.Add(input);
                   
                }
                else
                {
                    objUser.Name = input.Name;
                    objUser.Surname = input.Surname;
                    objUser.Position = input.Position;
                    objUser.Pwd = input.Pwd;
                    objUser.Password = input.Password;
                    objUser.SignaturePath = input.SignaturePath;
                    objUser.Telephone = input.Telephone;
                    objUser.ThumbnailSignature = input.ThumbnailSignature;
                    objUser.UpdateBy = input.UpdateBy;
                    objUser.UpdateDate = DateTime.Now;
                    objUser.IsActive = input.IsActive;
                    objUser.IsDelete = input.IsDelete;
                }
                context.SaveChanges();
            }
        }

        public UserProfile Get(string Email)
        {
            UserProfile result = null;
            using (AccountingContext context = new AccountingContext())
            {
                var otable = context.Set<UserProfile>();

                result = otable.Where(S => S.Email == Email).FirstOrDefault();
            }

            return result;
        }
    }
}

   
